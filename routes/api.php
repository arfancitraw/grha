<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

    
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::group(['namespace' => 'API'], function(){
        Route::group(['prefix' => 'master', 'namespace' => 'Master'], function(){
            Route::resource('/jabatan-dokter', 'JabatanDokterController');
            Route::resource('/poli', 'PoliController');
            Route::resource('/rumah-sakit', 'RumahSakitController');
            Route::resource('/spesialisasi', 'SpesialisasiController');
            Route::resource('/tipe-pendaftar', 'TipePendaftarController');
            Route::resource('/slider', 'SliderController');
            Route::resource('/layanan', 'LayananController');
            Route::resource('/direksi', 'DireksiController');
            Route::resource('/dewan', 'DewanController');
            Route::resource('/sertifikat', 'SertifikatController');
        });

        Route::group(['prefix' => 'dokter', 'namespace' => 'Dokter'], function(){
            Route::resource('/list-dokter', 'ListDokterController');
            Route::resource('/jadwal-dokter', 'JadwalDokterController');
            Route::resource('/tanya-jawab', 'TanyaJawabController');
        });
        // Route::group(['prefix' => 'mobile', 'namespace' => 'Mobile'], function(){
        //     Route::resource('/direksi', 'DireksiController');
        //     Route::resource('/dewan', 'DewanController');
        // });

        Route::group(['prefix' => 'karir', 'namespace' => 'Karir'], function(){
            Route::resource('/hasil-seleksi', 'HasilSeleksiController');
            Route::resource('/info-seleksi', 'InfoController');
        });

        Route::group(['prefix' => 'media-center', 'namespace' => 'MediaCenter'], function(){
            Route::resource('/pengumuman', 'PengumumanController');
            Route::resource('/artikel', 'ArtikelController');
            Route::resource('/pustaka/penyakit', 'PenyakitController');
            Route::resource('/pustaka/tindakan', 'TindakanController');
            Route::resource('/pustaka', 'PustakaController');
        }); 

        Route::resource('/rujukan-nasional', 'RujukanNasional\RujukanNasionalController');

        Route::group(['prefix' => 'kuesioner', 'namespace' => 'Kuesioner'], function(){
            Route::resource('/pertanyaan', 'KuesionerController');
            Route::post('/save-kuesioner', 'KuesionerController@nilaiKuesioner');
        });
    });

Route::group(['middleware' => 'auth:api'], function () {
});
