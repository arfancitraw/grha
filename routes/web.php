<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

        // Route::get('/', function () {
//     return view('welcome');
// });
use App\Mail\Pengaduan;
// Route::get('mail/send', function(){
//     Mail::to("ramdan.pragma@gmail.com")->send(new Pengaduan());
// });

Route::get('/translate-indonesia/', 'FrontEnd\TranslateController@index');//translate
Route::get('/translate-english/', 'FrontEnd\TranslateController@index');//translate

Route::post('/kuesioner/nilai', 'FrontEndController@nilaiKuesioner');
Route::post('/testimoni/nilai', 'FrontEndController@nilaiTestimoni');

Route::group(['namespace' => 'FrontEnd'], function(){
    Route::get('/', 'HomeController@index');
    Route::get('/sukaman', 'SukamanController@index');

    Route::post('/sukaman/kontak', 'SukamanController@contact');

    Route::get('/sukaman/dokter/{id}', 'SukamanController@dokter');
    Route::get('/sukaman/service/{id}', 'SukamanController@service');
    Route::get('/sukaman/info/{id}', 'SukamanController@info');
    Route::get('/infobed', 'HomeController@infobed');
    // Route::get('/profile/{page}', 'ProfileController@page');

    // Route::get('/pelayanan/{page}', 'Pelayanan\PelayananController@page');


    Route::get('/pelayanan/{slug}', 'PelayananController@detail');
    Route::get('/profil/{slug}', 'ProfileController@detail');
    Route::get('/informasi/{slug}', 'InformasiController@detail');

    Route::get('/tim-dokter/', 'TimDokterController@index');
    Route::get('tim-dokter/{id}/detail', 'TimDokterController@detail');

    Route::get('/rujukan-nasional/', 'RujukanNasionalController@index');
    Route::get('/rujukan-nasional/detail/{id}', 'RujukanNasionalController@detail');
    Route::get('/rujukan-nasional/{slug}', 'RujukanNasionalController@page');

    Route::get('/artikel/', 'ArtikelController@index');
    Route::get('/artikel/list/{bulan}', 'ArtikelController@month');
    Route::get('/artikel/{page}', 'ArtikelController@page');
    Route::get('/artikel/{slug}', 'ArtikelController@slug');

    Route::get('/kegiatan/', 'KegiatanController@index');
    Route::get('/kegiatan/list/{bulan}', 'KegiatanController@month');
    Route::get('/kegiatan/{page}', 'KegiatanController@page');
    Route::get('/kegiatan/{slug}', 'KegiatanController@slug');

    Route::get('/berita/', 'PengumumanController@index');
    Route::get('/berita/list/{bulan}', 'PengumumanController@month');
    Route::get('/berita/{page}', 'PengumumanController@page');
    Route::get('/berita/{slug}', 'PengumumanController@slug');

    Route::get('/pustaka/detail/{tipe}/{id}', 'Media\PustakaController@detail');
    Route::get('/pustaka/{key}', 'Media\PustakaController@daftar');

    Route::get('/diklat/', 'DiklatController@index');
    Route::get('/diklat/{id}', 'DiklatController@detail');
    Route::get('/litbang/', 'LitbangController@index');
    Route::get('/litbang/{id}', 'LitbangController@detail');

    Route::get('/mutu', 'MutuController@index');
    Route::post('/mutu', 'MutuController@show');

    Route::get('/jadwal-dokter/', 'JadwalDokterController@index');
    Route::post('/jadwal-dokter/', 'JadwalDokterController@index');

    Route::get('/pasien', 'PasienController@index');
    Route::post('/pasien', 'PasienController@index');
    Route::post('/pasien/perjanjian', 'PasienController@perjanjian');
    Route::post('/pasien/jadwal', 'PasienController@table');

    //Pasien Pengunjung
    Route::group(['prefix' => 'pasien', 'namespace' => 'Pasien'], function(){
        //Alur Pendaftaran
        Route::get('/alur-pendaftaran/', 'PasienController@index');
        Route::get('/index/{page}', 'PasienController@page');

        //Aturan Pengujung
        Route::get('/aturan-pengunjung/', 'PasienController@rules');
        Route::get('/index/{page}', 'PasienController@page');

    });
    
    Route::group(['prefix' => 'dokter', 'namespace' => 'InfoPasien'], function(){
        Route::get('tanya-jawab/detail/{id}', 'TanyaJawabController@detail');
        Route::resource('tanya-jawab', 'TanyaJawabController');
    });
    
    //Pasien Pengunjung
    Route::group(['prefix' => 'info-pasien', 'namespace' => 'InfoPasien'], function(){
        //Alur Pendaftaran
        Route::get('/testimoni/', 'TestimoniController@index');
        
        // Route::get('/pengaduan/', 'PengaduanController@index');
        // Route::post('/pengaduan/', 'PengaduanController@store');

        Route::get('/pengaduan/{tipe}', 'PengaduanController@index');
        Route::post('/pengaduan', 'PengaduanController@store');
        Route::post('/pengaduan/sponsor', 'PengaduanController@storeSponsor');
        Route::post('/pengaduan/gratifikasi', 'PengaduanController@storeGratifikasi');
        // Route::get('/pengaduan/masyarakat', 'PengaduanController@pengaduanMasyarakat');
        // Route::get('/pengaduan/whistleblowing', 'PengaduanController@whistleblowing');
        // Route::get('/pengaduan/etik-hukum', 'PengaduanController@etikHukum');
        // Route::get('/tanya-jawab/detail', 'TanyaJawabController@detail');
        // Route::get('/tanya-jawab/{id}', 'TanyaJawabController@id');

        Route::resource('/tambah-tanya-jawab/', 'TambahanTanyaJawabController');
    });

    Route::group(['prefix' => 'media', 'namespace' => 'Media'], function(){
    	Route::get('/kegiatan/', 'KegiatanController@index');
    	Route::get('/kegiatan/{page}', 'KegiatanController@page');

    	Route::get('/pengumuman', 'PengumumanController@index');
        Route::get('/pengumuman/detail/{id}', 'PengumumanController@detail');
    	//Route::get('/pengumuman/{page}', 'PengumumanController@page');

        Route::get('/pustaka/', 'PustakaController@index');
        Route::get('/pustaka/{page}', 'PustakaController@page');
    });
});

Auth::routes();


Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    
    Route::resource('/profile', 'Setting\ProfileController');

    Route::group(['prefix' => 'backend'], function(){

        Route::group(['prefix' => 'dokter', 'namespace' => 'Dokter'], function(){
            Route::post('list-dokter/sync', 'ListDokterController@sync');
            Route::post('list-dokter/grid', 'ListDokterController@grid');
            Route::resource('list-dokter', 'ListDokterController');

            Route::post('jadwal-dokter/grid', 'JadwalDokterController@grid');
            Route::resource('jadwal-dokter', 'JadwalDokterController');
        });

        Route::group(['prefix' => 'media-center', 'namespace' => 'MediaCenter'], function(){
            Route::post('artikel/grid', 'ArtikelController@grid');
            Route::resource('artikel', 'ArtikelController');
            Route::get('artikel/remove-image/{id}', 'ArtikelController@removeImage');


            Route::post('kegiatan/grid', 'KegiatanController@grid');
            Route::resource('kegiatan', 'KegiatanController');

            Route::post('pengumuman/grid', 'PengumumanController@grid');
            Route::resource('pengumuman', 'PengumumanController');

            Route::post('pustaka/penyakit/grid', 'PenyakitController@grid');
            Route::resource('pustaka/penyakit', 'PenyakitController');

            Route::post('pustaka/tindakan/grid', 'TindakanController@grid');
            Route::resource('pustaka/tindakan', 'TindakanController');
        });

        Route::group(['namespace' => 'RujukanNasional'], function(){ //'prefix' => 'rujukan-nasional',
            Route::post('rujukan-nasional/grid', 'RujukanNasionalController@grid');
            Route::get('rujukan-nasional/download/{id}', 'RujukanNasionalController@download');
            Route::get('rujukan-nasional/remove-image/{id}', 'RujukanNasionalController@removeImage');
            Route::resource('rujukan-nasional', 'RujukanNasionalController');
        });


        //Pasien Pengunjung
        Route::group(['prefix' => 'pasien-pengunjung', 'namespace' => 'PasienDanPengunjung'], function(){
            Route::post('alur-pendaftaran/grid', 'AlurPendaftaranController@grid');
            Route::resource('alur-pendaftaran', 'AlurPendaftaranController');

            Route::post('aturan-pengunjung/grid', 'AturanPengunjungController@grid');
            Route::resource('aturan-pengunjung', 'AturanPengunjungController');
        });

        //Info Pasien
        Route::group(['prefix' => 'info-pasien', 'namespace' => 'InfoPasien'], function(){
            Route::post('info/grid', 'InfoPasienController@grid');
            Route::resource('info', 'InfoPasienController');

            Route::post('testimoni/grid', 'TestimoniController@grid');
            Route::resource('testimoni', 'TestimoniController');

        });

        Route::group(['prefix' => 'pengaduan', 'namespace' => 'Pengaduan'], function(){
            Route::resource('narasi', 'NarasiController', ['only' => [
                'index', 'store'
            ]]);

            Route::post('info-pengaduan/grid', 'InfoPengaduanController@grid');
            Route::resource('info-pengaduan', 'InfoPengaduanController');

            Route::post('item-pengaduan/grid', 'ItemPengaduanController@grid');
            Route::resource('item-pengaduan', 'ItemPengaduanController');

            Route::post('sponsor/grid', 'PengaduanSponsorController@grid');
            Route::resource('sponsor', 'PengaduanSponsorController');

            Route::post('etik-hukum/grid', 'PengaduanEtikHukumController@grid');
            Route::resource('etik-hukum', 'PengaduanEtikHukumController');

            Route::post('masyarakat/grid', 'PengaduanMasyarakatController@grid');
            Route::resource('masyarakat', 'PengaduanMasyarakatController');

            Route::post('whistleblowing/grid', 'PengaduanWhistleblowingController@grid');
            Route::resource('whistleblowing', 'PengaduanWhistleblowingController');

            Route::post('gratifikasi/grid', 'PengaduanGratifikasiController@grid');
            Route::resource('gratifikasi', 'PengaduanGratifikasiController');
        });

        //Informasi
        Route::group(['namespace' => 'Informasi'], function(){
            Route::post('informasi/sort', 'InformasiController@sort');
            Route::post('informasi/grid', 'InformasiController@grid');
            Route::resource('informasi', 'InformasiController');
            Route::get('informasi/remove-image/{id}', 'InformasiController@removeImage');
        });

        //Profil
        Route::group(['namespace' => 'Profil'], function(){
            Route::post('profil/sort', 'ProfilController@sort');
            Route::post('profil/grid', 'ProfilController@grid');
            Route::resource('profil', 'ProfilController');
            Route::get('profil/remove-image/{id}', 'ProfilController@removeImage');
        });

        //Diklat
        Route::group(['prefix' => 'diklat', 'namespace' => 'Diklat'], function(){

            Route::post('info-diklat/grid', 'InfoDiklatController@grid');
            Route::resource('info-diklat', 'InfoDiklatController');

            Route::post('jadwal-diklat/grid', 'JadwalDiklatController@grid');
            Route::resource('jadwal-diklat', 'JadwalDiklatController');
        });

        //Kuesioner

        Route::group(['namespace' => 'Kuesioner'], function(){ //'prefix' => 'kuesioner',
            Route::get('kuesioner/{id}/monitoring', 'KuesionerController@monitoring');
            Route::post('kuesioner/{id}/activate', 'KuesionerController@activate');
            Route::post('kuesioner/grid', 'KuesionerController@grid');
            Route::resource('kuesioner', 'KuesionerController');

            // Route::post('kuesioner-jawaban/grid', 'KuesionerJawabanController@grid');
            // Route::resource('kuesioner-jawaban', 'KuesionerJawabanController');

            // Route::post('kuesioner-jawaban-detail/grid', 'KuesionerJawabanDetailController@grid');
            // Route::resource('kuesioner-jawaban-detail', 'KuesionerJawabanDetailController');
        });

        Route::group(['namespace' => 'Testimoni'], function(){
            Route::post('testimoni/{id}/activate', 'TestimoniController@activate');
            Route::post('testimoni/grid', 'TestimoniController@grid');
            Route::resource('testimoni', 'TestimoniController');
        });

        Route::group(['namespace' => 'Pertanyaan'], function(){
            Route::post('pertanyaan/{id}/publish', 'PertanyaanController@publish');
            Route::get('pertanyaan/{id}/detail', 'PertanyaanController@detail');
            Route::get('pertanyaan/{id}/details/jawaban/{ans}', 'PertanyaanController@jawaban');
            Route::post('pertanyaan/update-jawaban', 'PertanyaanController@updateJawaban');
            Route::get('pertanyaan/delete-jawaban/{id}', 'PertanyaanController@deleteJawaban');
            Route::post('pertanyaan/grid', 'PertanyaanController@grid');
            Route::resource('pertanyaan', 'PertanyaanController');
        });

        Route::group(['namespace' => 'Pengaduan'], function(){
            // Route::get('kuesioner/{id}/detail', 'KuesionerController@detail');
            Route::post('pengaduan/{id}/activate', 'PengaduanController@activate');
            Route::post('pengaduan/grid', 'PengaduanController@grid');
            Route::resource('pengaduan', 'PengaduanController');
        });


        //Litbang
        Route::group(['prefix' => 'litbang', 'namespace' => 'Litbang'], function(){
            Route::post('info-litbang/grid', 'InfoLitbangController@grid');
            Route::resource('info-litbang', 'InfoLitbangController');

            Route::post('list-publikasi/grid', 'ListPublikasiController@grid');
            Route::resource('list-publikasi', 'ListPublikasiController');

            Route::post('komite-etik/grid', 'KomiteEtikController@grid');
            Route::resource('komite-etik', 'KomiteEtikController');
        });

        //Mutu
        Route::group(['namespace' => 'Mutu'], function(){ //'prefix' => 'mutu',
            Route::post('mutu/grid', 'InfoMutuController@grid');
            Route::resource('mutu', 'InfoMutuController');
            Route::get('mutu/remove-image/{id}', 'InfoMutuController@removeImage');

        });
        //Pasien pelayanan
        Route::group(['namespace' => 'Layanan'], function(){ //'prefix' => 'layanan',
            Route::post('layanan/sort', 'LayananController@sort');
            Route::post('layanan/grid', 'LayananController@grid');
            Route::get('layanan/{id}/download', 'LayananController@downloadFile');
            Route::get('layanan/remove-image/{id}', 'LayananController@removeImage');
            Route::resource('layanan', 'LayananController');
        });

        //Karir
        Route::group(['prefix' => 'karir', 'namespace' => 'Karir'], function(){
            Route::post('info/grid', 'InfoController@grid');
            Route::resource('info', 'InfoController');

            Route::post('hasil-seleksi/grid', 'HasilSeleksiController@grid');
            Route::resource('hasil-seleksi', 'HasilSeleksiController');
        });


        //Profil
        // Route::group(['prefix' => 'profil', 'namespace' => 'Profil'], function(){
        //     Route::post('sejarah/grid', 'SejarahController@grid');
        //     Route::resource('sejarah', 'SejarahController');

        //     Route::post('visi-misi/grid', 'VisiMisiController@grid');
        //     Route::resource('visi-misi', 'VisiMisiController');

        //     Route::post('sambutan/grid', 'SambutanController@grid');
        //     Route::resource('sambutan', 'SambutanController');

        //     Route::post('dewan-pengawas/grid', 'DewanPengawasController@grid');
        //     Route::resource('dewan-pengawas', 'DewanPengawasController');

        //     Route::post('dewan-direksi/grid', 'DewanDireksiController@grid');
        //     Route::resource('dewan-direksi', 'DewanDireksiController');

        // //Penelitian
        //     Route::post('penelitian/grid', 'PenelitianController@grid');
        //     Route::resource('penelitian', 'PenelitianController');

        //     Route::post('struktur-organisasi/grid', 'StrukturOrganisasiController@grid');
        //     Route::resource('struktur-organisasi', 'StrukturOrganisasiController');

        //     Route::post('denah-lokasi/grid', 'DenahLokasiController@grid');
        //     Route::resource('denah-lokasi', 'DenahLokasiController');

        //     Route::post('profesi-keahlian/grid', 'ProfesiKeahlianController@grid');
        //     Route::resource('profesi-keahlian', 'ProfesiKeahlianController');

        //     Route::post('kerja-sama/grid', 'KerjaSamaController@grid');
        //     Route::resource('kerja-sama', 'KerjaSamaController');

        // });


        //Master
        Route::group(['prefix' => 'setting', 'namespace' => 'Setting'], function(){
            Route::post('user/grid', 'UserController@grid');
            Route::resource('user', 'UserController');

            Route::resource('jumbotron', 'JumbotronController');
            Route::resource('setting', 'SettingController');

            Route::post('menu/{id}/change', 'MenuController@change');
            Route::post('menu/save', 'MenuController@save');
            Route::get('menu/load', 'MenuController@load');
            Route::resource('menu', 'MenuController');
        });
        Route::group(['prefix' => 'sukaman', 'namespace' => 'Sukaman'], function(){
            Route::post('slider/grid', 'SliderController@grid');
            Route::resource('slider', 'SliderController');

            Route::post('pelayanan/grid', 'PelayananController@grid');
            Route::resource('pelayanan', 'PelayananController');

            Route::get('dokter/grid', 'DokterController@grid');
            Route::resource('dokter', 'DokterController');

            Route::post('fasilitas/grid', 'FasilitasController@grid');
            Route::resource('fasilitas', 'FasilitasController');

            Route::post('informasi/grid', 'InformasiController@grid');
            Route::resource('informasi', 'InformasiController');

            Route::post('setting/grid', 'SettingController@grid');
            Route::resource('setting', 'SettingController');

            // Route::resource('pricing', 'Tarif/TarifController');
            Route::post('pricing-kategori/salin-tarif', 'PricingKategoriController@salinTarif');
            Route::post('pricing-kategori/store-tarif', 'PricingKategoriController@storeTarif');
            Route::post('pricing-kategori/store-detail-tarif', 'PricingKategoriController@storeDetailTarif');
            Route::put('pricing-kategori/update-tarif', 'PricingKategoriController@updateTarif');
            Route::put('pricing-kategori/update-detail-tarif', 'PricingKategoriController@updateDetailTarif');
            Route::post('pricing-kategori/grid', 'PricingKategoriController@grid');
            Route::get('pricing-kategori/pricing', 'PricingKategoriController@pricing');
            Route::delete('pricing-kategori/{id}/delete-detail-tarif', 'PricingKategoriController@destroyDetail');
            Route::get('pricing-kategori/{id}/salin-tarif', 'PricingKategoriController@salin');
            Route::get('pricing-kategori/{id}/tambah-tarif', 'PricingKategoriController@tarif');
            Route::get('pricing-kategori/{id}/tambah-detail-tarif', 'PricingKategoriController@Detailtarif');
            Route::get('pricing-kategori/{id}/edit-tarif', 'PricingKategoriController@editTarif');
            Route::get('pricing-kategori/{id}/edit-detail-tarif', 'PricingKategoriController@editDetailTarif');
            Route::delete('pricing-kategori/{id}/delete-tarif', 'PricingKategoriController@destroyTarif');
            Route::resource('pricing-kategori', 'PricingKategoriController');

            Route::post('kontak/grid', 'KontakController@grid');
            Route::get('kontak/{id}/details', 'KontakController@details');
            Route::resource('kontak', 'KontakController');

        });
        Route::group(['prefix' => 'mobile', 'namespace' => 'Mobile'], function(){
            Route::post('direksi/grid', 'DireksiController@grid');
            Route::resource('direksi', 'DireksiController');

            Route::post('dewan/grid', 'DewanController@grid');
            Route::resource('dewan', 'DewanController');
        });

        Route::group(['prefix' => 'master', 'namespace' => 'Master'], function(){
//paviliun Sukaman

            Route::post('slider/grid', 'SliderController@grid');
            Route::resource('slider', 'SliderController');

            Route::post('kuesioner-periode/grid', 'KuesionerPeriodeController@grid');
            Route::resource('kuesioner-periode', 'KuesionerPeriodeController');

            Route::post('role-permission/change-permission', 'RoleController@changePermission');
            Route::post('role-permission/grid', 'RoleController@grid');
            Route::resource('role-permission', 'RoleController');

            Route::post('kuesioner-detail/grid', 'KuesionerDetailController@grid');
            Route::resource('kuesioner-detail', 'KuesionerDetailController');

            Route::post('footer/grid', 'FooterController@grid');
            Route::resource('footer', 'FooterController');

            Route::post('rumah-sakit/grid', 'RumahSakitController@grid');
            Route::resource('rumah-sakit', 'RumahSakitController');

            Route::post('jabatan-dokter/grid', 'JabatanDokterController@grid');
            Route::resource('jabatan-dokter', 'JabatanDokterController');

            Route::post('spesialisasi/grid', 'SpesialisasiController@grid');
            Route::resource('spesialisasi', 'SpesialisasiController');

            Route::post('sub-spesialisasi/grid', 'SubSpesialisasiController@grid');
            Route::resource('sub-spesialisasi', 'SubSpesialisasiController');

            Route::post('kategori-artikel/grid', 'KategoriArtikelController@grid');
            Route::resource('kategori-artikel', 'KategoriArtikelController');

            Route::post('kategori-pengumuman/grid', 'KategoriPengumumanController@grid');
            Route::resource('kategori-pengumuman', 'KategoriPengumumanController');

            Route::post('kategori-tanya-jawab/grid', 'KategoriTanyaJawabController@grid');
            Route::resource('kategori-tanya-jawab', 'KategoriTanyaJawabController');

            Route::post('tipe-media/grid', 'TipeMediaController@grid');
            Route::resource('tipe-media', 'TipeMediaController');

            Route::post('tipe-pendaftar/grid', 'TipePendaftarController@grid');
            Route::resource('tipe-pendaftar', 'TipePendaftarController');

            Route::post('poli/sync', 'PoliController@sync');
            Route::post('poli/grid', 'PoliController@grid');
            Route::resource('poli', 'PoliController');

            Route::post('jenis-mutu/grid', 'JenisMutuController@grid');
            Route::resource('jenis-mutu', 'JenisMutuController');

            Route::post('info-bed/grid', 'InfoBedController@grid');
            Route::resource('info-bed', 'InfoBedController');

            Route::post('link-footer/grid', 'LinkFooterController@grid');
            Route::resource('link-footer', 'LinkFooterController');

            Route::post('sertifikat-footer/grid', 'SertifikatFooterController@grid');
            Route::resource('sertifikat-footer', 'SertifikatFooterController');

            Route::post('media-sosial/grid', 'MediaSosialController@grid');
            Route::resource('media-sosial', 'MediaSosialController');

        });
    });


});
