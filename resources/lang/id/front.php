<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logout' => 'Keluar',
    'change_password' => 'Ubah Password',
    'edit_profile' => 'Edit Profil',
    'welcome' => 'Selamat Datang',
    'seeallnotofication' => 'Lihat semua notifikasi',
    'home' => 'Beranda',
    'register_training' => 'Register Training',
    'training' => 'Latihan',
    'result' => 'Hasil',
    'quesioner' => 'Kuesioner',
    'attendance' => 'Kehadiran',
    'score' => 'Skor',
    'elibrary' => 'E-Library',
    'tryouts' => 'Try Outs',
    'videolinks' => 'Tautan Video',
    'webbasedtraining' => 'Pelatihan berbasis Web',
    'directory' => 'Direktori',
    'venue' => 'Lokasi pelatihan',
    'contactUs' => 'Kontak Kami',
    'sertificate' => 'Sertifikat & Penghargaan',
    'service' => 'Pelayanan',
    'esatharkit' => 'Pelayanan unggulan di Pusat Jantung Nasional Harapan Kita',
    'ess' => 'Berbagai Layanan Unggulan Lainnya',
    'bedavailable' => 'Ketersediaan Tempat Tidur',
    'last' => 'Update Terakhir',
    'bedroom' => 'Ruang Perawatan',
    'class' => 'Kelas Perawatan',
    'sumabedroom' => 'Jumlah Tempat Tidur',
    'useroom' => 'Terpakai',
    'available' => 'Tersedia',
    'man' => 'Laki-laki',
    'woman' => 'Perempuan',
    'sum' => 'Jumlah',
    'videpprofile' => 'Video Profil',
    'cepattepatdanakurat' => 'Cepat, Tepat dan Akurat Dalam Memberikan Pelayanan Adalah Kami',
    'pusatmedia' => 'Pusat Media',
    'mediainformasiterkait' => 'Media Informasi terkait PJNHK untuk masyarakat',
    'news' => 'Berita',
    'kegiatan' => 'Kegiatan',
    'artikel' => 'Artikel',
    'pustakakesehatan' => 'Pustaka Kesehatan',
    'seeall' => 'Lihat semua',
    'seputarpenyakit' => 'Seputar Penyakit Jantung dan Tindakannya',
    'tidakadakegiatan' => 'Tidak ada kegiatan yang dapat ditampilkan',
    'accessibility' => 'Aksesibilitas RS',
    'untukdapatmengunjung' => 'Untuk dapat mengunjungi PJNHK, anda dapat menggunakan fasilitas pencarian kami berikut',
    'agreement' => 'Perjanjian',
    'makeagreement' => ' Buat Perjanjian',
    'doctorschedule' => ' Jadwal Dokter',
    'cekjadwal' => ' Cek Jadwal',
    'isikuesioner' => ' Isi Kuesioner',


];
