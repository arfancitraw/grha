<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logout' => 'Logout',
    'change_password' => 'Change Password',
    'edit_profile' => 'Edit Profile',
    'welcome' => 'Welcome',
    'seeallnotofication' => 'See All Notifications',
    'home' => 'Home',
    'register_training' => 'Training Registration',
    'training' => 'Training',
    'result' => 'Result',
    'quesioner' => 'Quesioner',
    'attendance' => 'Attendance',
    'score' => 'Score',
    'elibrary' => 'E-Library',
    'tryouts' => 'Try Outs',
    'videolinks' => 'Video Links',
    'webbasedtraining' => 'Pelatihan berbasis Web',
    'directory' => 'Directory',
    'venue' => 'Training Venues',
    'contactUs' => 'Contact Us',
    'sertificate' => 'Sertificate & Award',
    'service' => 'Service',
    'esatharkit' => 'Excellent service at Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita',
    'ess' => 'Other Superior Services',
    'bedavailable' => 'Available Beds',
    'last' => 'Last Updates',
    'bedroom' => 'Bedroom',
    'class' => 'Class',
    'sumabedroom' => 'Total Beds',
    'useroom' => 'Occupied',
    'available' => 'Available',
    'man' => 'Male',
    'woman' => 'Female',
    'sum' => 'Beds',
    'videpprofile' => 'Profile Video',
    'cepattepatdanakurat' => 'Fast, Precise and Accurate In Providing is Our Service Speciality',
    'pusatmedia' => 'Media Center',
    'mediainformasiterkait' => 'Related RSJPDHK Media information for you',
    'news' => 'News',
    'kegiatan' => 'Activity',
    'artikel' => 'Article',
    'pustakakesehatan' => 'Health Library',
    'seeall' => 'Read More',
    'seputarpenyakit' => 'Regarding Heart Disease and Its Action',
    'tidakadakegiatan' => 'No activity',
    'accessibility' => 'Hospital Accessibility',
    'untukdapatmengunjung' => 'To be able to visit RSJPDHK, you can use our following search facility',
    'agreement' => 'Appointment',
    'makeagreement' => 'Make Appointment',
    'doctorschedule' => 'Doctor Schedules',
    'quesioner' => 'Questionnaire',
    'cekjadwal' => 'Check Schedule',
    'isikuesioner' => 'Make Questionnaire',
];
