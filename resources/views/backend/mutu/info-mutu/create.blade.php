@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	<div class="ui attached top segment">
		{!! csrf_field() !!}
		<div class="ui grid">
			<div class="nine wide column">
				<div id="tab_info_mutu">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="info_mutu_id">ID</a>
						<a class="item" data-tab="info_mutu_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="info_mutu_id">
						<div class="field">
							<label>Nama </label>
							<input type="text" name="nama" placeholder="Nama">
						</div>
						<div class="field" >
							<label>Deskripsi</label>
							<textarea name="deskripsi" rows="5" class="editor"></textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="info_mutu_en">
						<div class="field">
							<label>Nama </label>
							<input type="text" name="nama_en" placeholder="Nama (EN)">
						</div>
						<div class="field" >
							<label>Deskripsi</label>
							<textarea name="deskripsi_en" rows="5" class="editor"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="seven wide column">
				<div class="two fields">
					<div class="date field">
						<label>Periode</label>
						<input type="text" class="five wide column" name="periode" placeholder="Periode">
					</div>
					<div class="field">
					<label for="nama">Jenis Mutu</label>
					<select name="jenis_mutu" class="ui search dropdown">
						{{-- <option value="">Pilih Jenis Mutu</option> --}}
						{!! \App\Models\Master\JenisMutu::options('nama') !!}
					</select>
					</div>
				</div>
				<div class="field image-container">
					<label>Gambar</label>
					<img class="image-preview" id="showAttachment" style="height: 27rem; object-fit: cover" src="http://fsm.undip.ac.id//assets/attachments/Images/default.jpg">
					<div class="ui fluid file input action">
						<input type="text" readonly="">
						<input type="file" class="ten wide column" id="attachment" name="gambar" autocomplete="off" multiple="">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '200px';
		tinymce.init(tinymceOpt);

		$('#tab_info_mutu .menu .item').tab();	

	$('.date').calendar({
		type: 'month',
		formatter: {
			date: function(date, setting) {
				var year = date.getFullYear();
				var month = date.getMonth() + 1;
				if (month < 10) {
					month = '0' + month;
				}
				return year + '-' + month
			}
		}
	})
	});

</script>
@endsection

