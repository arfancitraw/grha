@extends('layouts.grid')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('filters')
<div class="field">
	<input name="filter[nama]" placeholder="Nama" type="text">
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
<a class="ui blue button add-page">
	<i class="plus icon"></i> Tambah Data
</a>
@endif
@endsection

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).on('click', '.edit-tab.button', function(e){
		var id = $(this).data('id');
		var url = '{{ url($pageUrl) }}/'+id+'/edit';
		window.location = url;
	});
</script>
@endsection

@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
<a class="ui blue button" href="{{ url('backend/mutu/info-mutu/create') }}">
	<i class="plus icon"></i> Tambah Data
</a>
@endif
@endsection