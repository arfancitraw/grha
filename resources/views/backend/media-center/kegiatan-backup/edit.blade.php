@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
<div class="ui attached top segment">
{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	<input type="hidden" name="status" id="status" value="{{ $record->status }}">
	<div class="ui grid">
		<div class="sixteen wide column">
			<div class="field">
				<label for="nama">Nama Kegiatan </label>
				<input type="text" name="nama_kegiatan" value="{{$record->nama_kegiatan}}">
			</div>
			<div class="field">
				<label for="nama">Tipe Media</label>
				<select class="ui selection dropdown" name="tipe_media">
					{!! \App\Models\Master\TipeMedia::options('tipe_media','id',['selected'=> $record->tipe_media]) !!}
				</select>
			</div>
			<div class="field">
				<label for="">Deskripsi</label>
				<textarea name="deskripsi" rows="3">{{$record->deskripsi}}</textarea>
			</div>
		</div>
	</div>
	<div class="ui error message"></div>
</div>
<div class="ui bottom attached segment">
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui black labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<div class="ui right labeled green icon save as page button">
				<i class="save icon"></i>
				Simpan
			</div>
		</div>
	</div>
</div>
</form>

@endsection
@section('scripts')
<script type="text/javascript">
$('.editor').summernote({
	height: '155px',
	placeholder: "Konten..."
});

</script>
@endsection

