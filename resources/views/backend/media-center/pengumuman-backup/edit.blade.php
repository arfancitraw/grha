@extends('layouts.forms')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('scripts')
<script type="text/javascript">
	$('.editor').summernote({
			height: '200px'
		});

</script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	<div class="ui attached top segment">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<input type="hidden" name="status" id="status" value="{{ $record->status }}">
		
		<div class="ui grid">
			<div class="ten wide column">
				<div class="field">
					<label for="nama">Judul </label>
					<input type="text" name="judul" placeholder="Judul Pengumuman" value="{{$record->judul}}">
				</div>
				<div class="field">
					<label for="">Konten</label>
					<textarea name="konten" rows="3" class="editor">{{$record->konten}}</textarea>
				</div>
			</div>
			<div class="six wide column">
				<div class="field">
					<label>Unggah Berkas</label>
					<div class="ui fluid file input action">
						<input type="text" readonly="" >
						<input type="file" class="ten wide column" id="attachment" name="lampiran" autocomplete="off" accept="application/pdf" >
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				@if(isset($record->lampiran))
				<div class="field">
					<label for="">File Lampiran</label>
					<h1 class="text-center" style="font-size: 3rem"><i class="file excel icon"></i></h1>
				</div>
				@endif
				<div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" style="margin-top: 0px; margin-bottom: 0px; height: 193px;" rows="3">{{$record->keterangan}}</textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<button class="ui right labeled blue icon button">
					<i class="save icon"></i>
					Simpan
				</button>
			</div>
		</div>
	</div>
</form>
@endsection