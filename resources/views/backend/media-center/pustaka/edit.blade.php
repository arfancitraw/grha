@extends('layouts.forms')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		$('.menu .item').tab();	
		$('.editor').summernote({
			height: '200px'
		});
		$('.select2').select2();
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	<div class="ui attached top segment">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<input type="hidden" name="status" id="status" value="{{ $record->status }}">
		
		<div class="ui grid" style="margin-bottom: 0">
			<div class="ten wide column">
				<div class="field">
					<label for="nama">Judul </label>
					<input type="text" name="judul" placeholder="Judul Pengumuman" value="{{$record->judul}}">
				</div>
				{{-- <div class="field">
					<label for="">Konten</label>
					<textarea name="konten" rows="3" class="editor">{{$record->konten}}</textarea>
				</div> --}}
			</div>
			<div class="six wide column">
				<div class="field">
					<label>Unggah Berkas</label>
					<div class="ui fluid file input action">
						<input type="text" readonly="" >
						<input type="file" class="ten wide column" id="attachment" name="lampiran" autocomplete="off" accept="application/pdf" >
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				{{-- @if(isset($record->lampiran))
				<div class="field">
					<label for="">File Lampiran</label>
					<h1 class="text-center" style="font-size: 3rem"><i class="file excel icon"></i></h1>
				</div>
				@endif
				<div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" style="margin-top: 0px; margin-bottom: 0px; height: 193px;" rows="3">{{$record->keterangan}}</textarea>
				</div> --}}
			</div>
		</div>
	</div>

	<div class="ui attached segment">
		<div class="ui pointing secondary menu">
	        <a class="active item" data-tab="pengertian">Pengertian</a>
	        <a class="item" data-tab="gejala">Gejala</a>
	        <a class="item" data-tab="penyebab">Penyebab</a>
	        <a class="item" data-tab="penanganan">Penanganan & Pengobatan</a>
	        <a class="item" data-tab="pencegahan">Pencegahan</a>
		</div>

		<div class="ui active tab basic segment" data-tab="pengertian" style="padding:0">
			<p>Input data pengantar dalam bentuk html disini :</p>
			<textarea name="pengertian" class="editor">{!! $record->pengertian !!}</textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="gejala" style="padding:0">
			<p>Input data profil dalam bentuk html disini :</p>
			<textarea name="gejala" class="editor">{!! $record->gejala !!}</textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="penyebab" style="padding:0">
			<p>Input data produk dalam bentuk html disini :</p>
			<textarea name="penyebab" class="editor">{!! $record->penyebab !!}</textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="penanganan" style="padding:0">
			<p>Input data jejaring dalam bentuk html disini :</p>
			<textarea name="penanganan" class="editor">{!! $record->penanganan !!}</textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="pencegahan" style="padding:0">
			<p>Input data agenda dalam bentuk html disini :</p>
			<textarea name="pencegahan" class="editor">{!! $record->pencegahan !!}</textarea>
		</div>
	</div>

	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<button class="ui right labeled blue icon button">
					<i class="save icon"></i>
					Simpan
				</button>
			</div>
		</div>
	</div>
</form>
@endsection