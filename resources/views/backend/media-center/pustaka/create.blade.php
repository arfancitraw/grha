@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	<div class="ui attached top segment">
		{!! csrf_field() !!}

		<div class="ui grid" style="margin-bottom: 0">
			<div class="ten wide column">
				<div class="field">
					<label for="nama">Judul </label>
					<input type="text" name="judul" placeholder="Judul Pengumuman">
				</div>
				{{-- <div class="field">
					<label for="">Konten</label>
					<textarea name="konten" rows="3" class="editor"></textarea>
				</div> --}}
			</div>
			<div class="six wide column">
				<div class="field">
					<label>Unggah Berkas</label>
					<div class="ui fluid file input action">
						<input type="text" readonly="">
						<input type="file" class="ten wide column" accept="application/pdf" id="attachment" name="lampiran" autocomplete="off">
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				{{-- <div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" style="margin-top: 0px; margin-bottom: 0px; height: 244px;" rows="3"></textarea>
				</div> --}}
			</div>
		</div>
	</div>

	<div class="ui attached segment">
		<div class="ui pointing secondary menu">
	        <a class="active item" data-tab="pengertian">Pengertian</a>
	        <a class="item" data-tab="gejala">Gejala</a>
	        <a class="item" data-tab="penyebab">Penyebab</a>
	        <a class="item" data-tab="penanganan">Penanganan & Pengobatan</a>
	        <a class="item" data-tab="pencegahan">Pencegahan</a>
		</div>

		<div class="ui active tab basic segment" data-tab="pengertian" style="padding:0">
			<p>Input data pengantar dalam bentuk html disini :</p>
			<textarea name="pengertian" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="gejala" style="padding:0">
			<p>Input data profil dalam bentuk html disini :</p>
			<textarea name="gejala" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="penyebab" style="padding:0">
			<p>Input data produk dalam bentuk html disini :</p>
			<textarea name="penyebab" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="penanganan" style="padding:0">
			<p>Input data jejaring dalam bentuk html disini :</p>
			<textarea name="penanganan" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="pencegahan" style="padding:0">
			<p>Input data agenda dalam bentuk html disini :</p>
			<textarea name="pencegahan" class="editor"></textarea>
		</div>
	</div>

	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		$('.menu .item').tab();	
		$('.editor').summernote({
			height: '200px'
		});
		$('.select2').select2();
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@append