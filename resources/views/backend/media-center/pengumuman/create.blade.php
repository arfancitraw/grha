@extends('layouts.grid')



@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="ui attached top segment">
		<div class="ui grid">
			<div class="twelve wide column">
				<div class="field">
					<div class="ui small labeled input">
						<label for="amount" class="ui label">{{ url('berita') }}/</label>
						<input type="text" placeholder="Slug" name="slug" readonly>
					</div>
				</div>
				<div id="tab_translate">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="translate_id">ID</a>
						<a class="item" data-tab="translate_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="translate_id">
						<div class="field">
							<div class="ui big input">
								<input name="judul" placeholder="Judul Berita" type="text">
							</div>
						</div>
						<div class="field">
							<textarea name="konten" rows="3" class="editor"></textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="translate_en">
						<div class="field">
							<div class="ui big input">
								<input name="judul_en" placeholder="Judul Berita (EN)" type="text">
							</div>
						</div>
						<div class="field">
							<textarea name="konten_en" rows="3" class="editor"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="four wide column">
				<div class="field image-container">
					<img class="image-preview"  style="height: 120px" id="showAttachment" style="height: 27rem" src="http://fsm.undip.ac.id//assets/attachments/Images/default.jpg">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Gambar Berita">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" multiple="">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				<div class="date field">
					<label>Tanggal Berita</label>
					<div class="ui left icon input">
						<i class="calendar icon"></i>
						<input type="text" name="tanggal" placeholder="Tanggal Berita Terjadi">
					</div>
				</div>
				<div class="field">
					<label>Kategori</label>
					<select name="kategori_id" class="ui search dropdown">
						{!! \App\Models\Master\KategoriPengumuman::options('nama') !!}
					</select>
				</div>
				<div id="tab_deskripsi">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="deskripsi_id">ID</a>
						<a class="item" data-tab="deskripsi_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="deskripsi_id">
						<div class="field">
							<label for="">Deskripsi Singkat</label>
							<textarea name="keterangan" placeholder="Deskripsi Singkat" style="height: 95px; resize: none"></textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="deskripsi_en">
						<div class="field">
							<label for="">Deskripsi Singkat (EN)</label>
							<textarea name="keterangan_en" placeholder="Deskripsi Singkat (EN)" style="height: 95px; resize: none"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('#tab_translate .menu .item').tab();	
		$('#tab_deskripsi .menu .item').tab();
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});

	$(document).ready(function($) {
		$('[name=judul]').on('change, keyup', function(event) {
			var judul = $(this).val();
			$('[name=slug]').val(slugify(judul));
		});
		$('.date').calendar({
			type: 'date',
			formatter: {
				date: function(date, setting) {
					var year = date.getFullYear();
					var month = date.getMonth() + 1;
					var day = date.getDate();
					if (month < 10) {
						month = '0' + month;
					}
					if (day < 10) {
						day = '0' + day;
					}
					return year + '-' + month + '-' + day
				}
			}
		})
	});
</script>
@endsection

