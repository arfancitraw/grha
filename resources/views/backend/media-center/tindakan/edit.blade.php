@extends('layouts.grid')

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '200px';
		tinymce.init(tinymceOpt);

		$('#tab_tindakan .menu .item').tab();	
		$('#tab_id .menu .item').tab({
			context: 'parent'
		});
		$('#tab_en .menu .item').tab({
			context: 'parent'
		});
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});


	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	<div class="ui attached top segment">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<input type="hidden" name="status" id="status" value="{{ $record->status }}">
		
		<div id="inner2bottom" style="margin-bottom: 10px"></div>
		<div class="ui grid" style="margin-bottom: 0">
			<div class="ten wide column">
				<div id="tab_tindakan">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="tindakan_id">ID</a>
						<a class="item" data-tab="tindakan_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="tindakan_id">
						<div class="field">
							<label for="nama">Nama </label>
							<input type="text" name="nama" placeholder="Nama Tindakan" value="{{$record->nama}}">
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="tindakan_en">
						<div class="field">
							<label for="nama">Nama </label>
							<input type="text" name="nama_en" placeholder="Nama Tindakan (EN)" value="{{$record->nama_en}}">
						</div>
					</div>
				</div>
				{{-- <div class="field">
					<label for="">Konten</label>
					<textarea name="konten" rows="3" class="editor">{{$record->konten}}</textarea>
				</div> --}}
			</div>
			<div class="six wide column">
				<div class="field">
					<label>Unggah Berkas</label>
					<div class="ui fluid file input action">
						<input type="text" readonly="" >
						<input type="file" class="ten wide column" id="attachment" name="lampiran" autocomplete="off" accept="application/pdf" >
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				{{-- @if(isset($record->lampiran))
				<div class="field">
					<label for="">File Lampiran</label>
					<h1 class="text-center" style="font-size: 3rem"><i class="file excel icon"></i></h1>
				</div>
				@endif
				<div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" style="margin-top: 0px; margin-bottom: 0px; height: 193px;" rows="3">{{$record->keterangan}}</textarea>
				</div> --}}
			</div>
		</div>
	</div>

	<div class="ui attached segment">
			<div id="tab_tindakan">
				<div class="ui top attached tabular menu">
					<a class="active item" data-tab="tindakan_id">ID</a>
					<a class="item" data-tab="tindakan_en">EN</a>
				</div>
				<div class="ui bottom attached active tab segment" data-tab="tindakan_id">
					<div id="tab_id">
						<div class="ui pointing secondary menu">
							<a class="active item" data-tab="indikasi">Indikasi</a>
							<a class="item" data-tab="tujuan">Tujuan</a>
							<a class="item" data-tab="tatacara">Tatacara</a>
							<a class="item" data-tab="risiko">Risiko</a>
							<a class="item" data-tab="alternatif">Alternatif</a>
							<a class="item" data-tab="prognosis_dilakukan">Prognosis Dilakukan</a>
							<a class="item" data-tab="prognosis_tidak">Prognosis Tidak Dilakukan</a>
							<a class="item" data-tab="perluasan">Perluasan</a>
							<a class="item" data-tab="persiapan">Persiapan Khusus</a>
							<a class="item" data-tab="penjelasan">Penjelasan</a>
						</div>

						<div class="ui active tab basic segment" data-tab="indikasi" style="padding:0">
							<p>Input data Indikasi/temuan klinis/diagnosis sehingga memerlukan tindakan/pemeriksaan dalam bentuk html disini :</p>
							<textarea name="indikasi" class="editor">{!! $record->indikasi !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="tujuan" style="padding:0">
							<p>Input data tujuan tindakan dalam bentuk html disini :</p>
							<textarea name="tujuan" class="editor">{!! $record->tujuan !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="tatacara" style="padding:0">
							<p>Input data Tatacara pelaksanaan tindakan dalam bentuk html disini :</p>
							<textarea name="tatacara" class="editor">{!! $record->tatacara !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="risiko" style="padding:0">
							<p>Input data risiko dan komplikasi yang mungkin terjadi (yang sudah umum, jarang atau tidak dapat dibayangkan sebelumnya) dalam bentuk html disini :</p>
							<textarea name="risiko" class="editor">{!! $record->risiko !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="alternatif" style="padding:0">
							<p>Input data Alternatif lain dalam bentuk html disini :</p>
							<textarea name="alternatif" class="editor">{!! $record->alternatif !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="prognosis_dilakukan" style="padding:0">
							<p>Input data Prognosis bila dilakukan tindakan dalam bentuk html disini :</p>
							<textarea name="prognosis_dilakukan" class="editor">{!! $record->prognosis_dilakukan !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="prognosis_tidak" style="padding:0">
							<p>Input data Prognosis bila tidak dilakukan tindakan dalam bentuk html disini :</p>
							<textarea name="prognosis_tidak" class="editor">{!! $record->prognosis_tidak !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="perluasan" style="padding:0">
							<p>Input data Perluasan tindakan/ tindakan lain yang mungkin dilakukan dalam bentuk html disini :</p>
							<textarea name="perluasan" class="editor">{!! $record->perluasan !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="persiapan" style="padding:0">
							<p>Input data persiapan dalam bentuk html disini :</p>
							<textarea name="persiapan" class="editor">{!! $record->persiapan !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="penjelasan" style="padding:0">
							<p>Input data penjelasan dalam bentuk html disini :</p>
							<textarea name="penjelasan" class="editor">{!! $record->penjelasan !!}</textarea>
						</div>

					</div>
				</div>
				<div class="ui bottom attached tab segment" data-tab="tindakan_en">
					<div id="tab_en">
						<div class="ui pointing secondary menu">
							<a class="active item" data-tab="indikasi_en">Indikasi</a>
							<a class="item" data-tab="tujuan_en">Tujuan</a>
							<a class="item" data-tab="tatacara_en">Tatacara</a>
							<a class="item" data-tab="risiko_en">Risiko</a>
							<a class="item" data-tab="alternatif_en">Alternatif</a>
							<a class="item" data-tab="prognosis_dilakukan_en">Prognosis Dilakukan</a>
							<a class="item" data-tab="prognosis_tidak_en">Prognosis Tidak Dilakukan</a>
							<a class="item" data-tab="perluasan_en">Perluasan</a>
							<a class="item" data-tab="persiapan_en">Persiapan Khusus</a>
							<a class="item" data-tab="penjelasan_en">Penjelasan</a>
						</div>

						<div class="ui active tab basic segment" data-tab="indikasi_en" style="padding:0">
							<p>Input data Indikasi/temuan klinis/diagnosis sehingga memerlukan tindakan/pemeriksaan dalam bentuk html disini :</p>
							<textarea name="indikasi_en" class="editor">{!! $record->indikasi_en !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="tujuan_en" style="padding:0">
							<p>Input data tujuan tindakan dalam bentuk html disini :</p>
							<textarea name="tujuan_en" class="editor">{!! $record->tujuan_en !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="tatacara_en" style="padding:0">
							<p>Input data Tatacara pelaksanaan tindakan dalam bentuk html disini :</p>
							<textarea name="tatacara_en" class="editor">{!! $record->tatacara_en !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="risiko_en" style="padding:0">
							<p>Input data risiko dan komplikasi yang mungkin terjadi (yang sudah umum, jarang atau tidak dapat dibayangkan sebelumnya) dalam bentuk html disini :</p>
							<textarea name="risiko_en" class="editor">{!! $record->risiko_en !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="alternatif_en" style="padding:0">
							<p>Input data Alternatif lain dalam bentuk html disini :</p>
							<textarea name="alternatif_en" class="editor">{!! $record->alternatif_en !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="prognosis_dilakukan_en" style="padding:0">
							<p>Input data Prognosis bila dilakukan tindakan dalam bentuk html disini :</p>
							<textarea name="prognosis_dilakukan_en" class="editor">{!! $record->prognosis_dilakukan_en !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="prognosis_tidak_en" style="padding:0">
							<p>Input data Prognosis bila tidak dilakukan tindakan dalam bentuk html disini :</p>
							<textarea name="prognosis_tidak_en" class="editor">{!! $record->prognosis_tidak_en !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="perluasan_en" style="padding:0">
							<p>Input data Perluasan tindakan/ tindakan lain yang mungkin dilakukan dalam bentuk html disini :</p>
							<textarea name="perluasan_en" class="editor">{!! $record->perluasan_en !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="persiapan_en" style="padding:0">
							<p>Input data persiapan dalam bentuk html disini :</p>
							<textarea name="persiapan_en" class="editor">{!! $record->persiapan_en !!}</textarea>
						</div>
						<div class="ui tab basic segment" data-tab="penjelasan_en" style="padding:0">
							<p>Input data penjelasan dalam bentuk html disini :</p>
							<textarea name="penjelasan_en" class="editor">{!! $record->penjelasan_en !!}</textarea>
						</div>
					</div>
				</div>
			</div>
	</div>

	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection