@extends('layouts.grid')
@section('css')
@append

@section('js')
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	<div class="ui attached top segment">
		{!! csrf_field() !!}
		
		<div id="inner2bottom" style="margin-bottom: 10px"></div>
		<div class="ui grid" style="margin-bottom: 0">
			<div class="ten wide column">
				<div id="tab_penyakit">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="penyakit_id">ID</a>
						<a class="item" data-tab="penyakit_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="penyakit_id">
						<div class="field">
							<label for="nama">Nama </label>
							<input type="text" name="nama" placeholder="Nama Penyakit" required>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="penyakit_en">
						<div class="field">
							<label for="nama">Nama </label>
							<input type="text" name="nama_en" placeholder="Nama Penyakit (EN)" required>
						</div>
					</div>
				</div>
				{{-- <div class="field">
					<label for="">Konten</label>
					<textarea name="konten" rows="3" class="editor"></textarea>
				</div> --}}
			</div>
			<div class="six wide column">
				<div class="field">
					<label>Unggah Berkas</label>
					<div class="ui fluid file input action">
						<input type="text" readonly="">
						<input type="file" class="ten wide column" accept="application/pdf" id="attachment" name="lampiran" autocomplete="off">
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				{{-- <div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" style="margin-top: 0px; margin-bottom: 0px; height: 244px;" rows="3"></textarea>
				</div> --}}
			</div>
		</div>

	</div>

	<div class="ui attached segment">
			<div id="tab_penyakit">
				<div class="ui top attached tabular menu">
					<a class="active item" data-tab="penyakit_id">ID</a>
					<a class="item" data-tab="penyakit_en">EN</a>
				</div>
				<div class="ui bottom attached active tab segment" data-tab="penyakit_id">
					<div id="tab_id">
						<div class="ui pointing secondary menu">
					        <a class="active item" data-tab="pengertian">Pengertian</a>
					        <a class="item" data-tab="gejala">Gejala</a>
					        <a class="item" data-tab="penyebab">Penyebab</a>
					        <a class="item" data-tab="penanganan">Penanganan & Pengobatan</a>
					        <a class="item" data-tab="pencegahan">Pencegahan</a>
						</div>

						<div class="ui active tab basic segment" data-tab="pengertian" style="padding:0">
							<p>Input data pengertian dalam bentuk html disini :</p>
							<textarea name="pengertian" class="editor" required></textarea>
						</div>	
						<div class="ui tab basic segment" data-tab="gejala" style="padding:0">
							<p>Input data gejala dalam bentuk html disini :</p>
							<textarea name="gejala" class="editor" required></textarea>
						</div>	
						<div class="ui tab basic segment" data-tab="penyebab" style="padding:0">
							<p>Input data penyebab dalam bentuk html disini :</p>
							<textarea name="penyebab" class="editor" required></textarea>
						</div>	
						<div class="ui tab basic segment" data-tab="penanganan" style="padding:0">
							<p>Input data penanganan dalam bentuk html disini :</p>
							<textarea name="penanganan" class="editor" required></textarea>
						</div>	
						<div class="ui tab basic segment" data-tab="pencegahan" style="padding:0">
							<p>Input data pencegahan dalam bentuk html disini :</p>
							<textarea name="pencegahan" class="editor" required></textarea>
						</div>
					</div>
				</div>
				<div class="ui bottom attached tab segment" data-tab="penyakit_en">
					<div id="tab_en">
						<div class="ui pointing secondary menu">
					        <a class="active item" data-tab="pengertian_en">Pengertian</a>
					        <a class="item" data-tab="gejala_en">Gejala</a>
					        <a class="item" data-tab="penyebab_en">Penyebab</a>
					        <a class="item" data-tab="penanganan_en">Penanganan & Pengobatan</a>
					        <a class="item" data-tab="pencegahan_en">Pencegahan</a>
						</div>

						<div class="ui active tab basic segment" data-tab="pengertian_en" style="padding:0">
							<p>Input data pengertian dalam bentuk html disini :</p>
							<textarea name="pengertian_en" class="editor" required></textarea>
						</div>	
						<div class="ui tab basic segment" data-tab="gejala_en" style="padding:0">
							<p>Input data gejala dalam bentuk html disini :</p>
							<textarea name="gejala_en" class="editor" required></textarea>
						</div>	
						<div class="ui tab basic segment" data-tab="penyebab_en" style="padding:0">
							<p>Input data penyebab dalam bentuk html disini :</p>
							<textarea name="penyebab_en" class="editor" required></textarea>
						</div>	
						<div class="ui tab basic segment" data-tab="penanganan_en" style="padding:0">
							<p>Input data penanganan dalam bentuk html disini :</p>
							<textarea name="penanganan_en" class="editor" required></textarea>
						</div>	
						<div class="ui tab basic segment" data-tab="pencegahan_en" style="padding:0">
							<p>Input data pencegahan dalam bentuk html disini :</p>
							<textarea name="pencegahan_en" class="editor" required></textarea>
						</div>
					</div>
				</div>
			</div>
	</div>

	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '200px';
		tinymce.init(tinymceOpt);

		$('#tab_penyakit .menu .item').tab();	
		$('#tab_id .menu .item').tab({
			context: 'parent'
		});
		$('#tab_en .menu .item').tab({
			context: 'parent'
		});		
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@append