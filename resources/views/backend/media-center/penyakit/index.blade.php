@extends('layouts.grid')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('filters')
<div class="field">
	<input name="filter[nama]" placeholder="Nama Penyakit" type="text">
</div>
<div class="field">
	<input name="filter[pengertian]" placeholder="Pengertian" type="text">
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
<a class="ui blue button" href="{{ url($pageUrl.'create') }}">
	<i class="plus icon"></i> Tambah Data
</a>
@endif
@endsection

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
d.pengertian = $("input[name='filter[pengertian]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nama: 'empty',
		sub_nama: 'empty',
		url: 'url',
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
		$('.editor').summernote({
			height: '200px'
		});

		$(document).on('click', '.button.save.as.drafting', function(e){
			$('#status').val(0);
			$('#saveButton').trigger('click');
		});

		$(document).on('click', '.button.save.as.publicity', function(e){
			$('#status').val(1);
			$('#saveButton').trigger('click');
		});
	};
</script>
@endsection

@section('content-body')
@parent
@endsection