<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Alur Pendaftaran</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<input type="hidden" name="status" id="status" value="{{ $record->status }}">
		<div class="ui grid">


			<div class="six wide column">
				<div class="field image-container">
					<span class="image-preview" style="height: 20rem">
						@if($record->photo)
							<img src="{{ asset('storage') }}/{{ $record->photo }}" height="237px" width="235px">
						@else
							Pilih photo
						@endif
					</span>
					<div class="ui fluid file input action">
						<input type="text" readonly="">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" accept="image/*">
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</div>
			
			<div class="ten wide column">
				<div class="field">
					<label for="nama">Judul </label>
					<input type="text" name="judul" value="{{$record->judul}}">
				</div>
				<div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" rows="17">{{$record->keterangan}}</textarea>
				</div>
			</div>

		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>