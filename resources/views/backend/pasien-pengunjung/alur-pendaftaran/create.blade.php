<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Alur Pendaftaran</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="status" id="status">
		<div class="ui grid">
			<div class="six wide column">
				<div class="field image-container">
					<span class="image-preview" style="height: 27rem">Pilih Photo</span>
					<div class="ui fluid file input action">
						<input type="text" readonly="">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" accept="image/*">
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</div>
			<div class="ten wide column">
				<div class="field">
					<label for="nama">Judul </label>
					<input type="text" name="judul" placeholder="JUdul ">
				</div>
				<div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" rows="17"></textarea>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>