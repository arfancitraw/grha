<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Hasil Seleksi</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="status" id="status">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="nama">Karir</label>
					<select class="ui selection dropdown" name="karir_id">
						{!! \App\Models\Karir\Info::options('judul') !!}
					</select>
				</div>
				<div class="field">
					<label for="nama">File</label>
					<div class="ui fluid file input action">
						<input type="text" readonly="">
						<input type="file" class="ten wide column" id="attachment" name="lampiran" autocomplete="off" accept="application/msword, application/pdf">
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				<div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" rows="10"></textarea>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>