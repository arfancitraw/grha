<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Hasil Seleksi</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<input type="hidden" name="status" id="status" value="{{ $record->status }}">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="nama">Karir</label>
					<select class="ui selection dropdown" name="karir_id">
						{!! \App\Models\Karir\Info::options('judul','id',['selected'=> $record->karir_id]) !!}
					</select>
				</div>
				<div class="field">
					<label for="nama">File</label>
					<div class="ui fluid file input action">
						<input type="text" value="{{ $record->lampiran or '' }}" readonly="">
						<input type="file" class="ten wide column" id="attachment" name="lampiran" autocomplete="off" accept="application/msword, application/pdf">
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				<div class="field">
					<label for="">Keterangan</label>
					<textarea name="alamat" rows="10">{{$record->keterangan}}</textarea>
				</div>
			</div>

		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>