@extends('layouts.grid')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('filters')
<div class="field">
	<input name="filter[nama]" placeholder="Nama Pengirim" type="text">
</div>
<div class="field">
	<input name="filter[judul]" placeholder="Judul Petanyaan" type="text">
</div>
<div class="field">
	<select name="filter[kategori]" class="ui fluid dropdown">
		{!! \App\Models\Master\KategoriTanyaJawab::options('nama', 'id', [], 'Seluruh Kategori') !!}
	</select>
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
{{-- <a class="ui blue button add-page">
	<i class="plus icon"></i> Tambah Data
</a> --}}
@endif
@endsection

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
d.judul = $("input[name='filter[judul]']").val();
d.kategori = $("select[name='filter[kategori]']").val();
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).on('click', '.publish.button', function(e){
		var id = $(this).data('id');
		swal({
			title: 'Perhatian!',
			text: "Yakin ingin publish pertanyaan ini?",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Publish',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: '{{ url($pageUrl) }}/'+id+'/publish',
					type: 'POST',
					data: {_token: "{{ csrf_token() }}", status:1},
					success: function(resp){
						swal(
							'Terpublish!',
							'Data berhasil dipublish.',
							'success'
						).then(function(e){
							if(!postRequest()){
								dt.draw();
							}
						});
					},
					error : function(resp){
						var data = resp.responseJSON;
						console.log(data)
						swal(
							'Gagal!',
							Object.is(data.message, undefined) ? 'Data gagal dipublish, eror tidak diketahui' : data.message,
							'error'
						).then(function(e){
							dt.draw();
						});
					}
				});
			}
		})
	});

	$(document).on('click', '.unpublish.button', function(e){
		var id = $(this).data('id');
		swal({
			title: 'Perhatian!',
			text: "Yakin ingin menyembunyikan pertanyaan ini?",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sembunyikan',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: '{{ url($pageUrl) }}/'+id+'/publish',
					type: 'POST',
					data: {_token: "{{ csrf_token() }}", status:0},
					success: function(resp){
						swal(
							'Terpublish!',
							'Data berhasil disembunyikan.',
							'success'
						).then(function(e){
							if(!postRequest()){
								dt.draw();
							}
						});
					},
					error : function(resp){
						var data = resp.responseJSON;
						console.log(data)
						swal(
							'Gagal!',
							Object.is(data.message, undefined) ? 'Data gagal disembunyikan, eror tidak diketahui' : data.message,
							'error'
						).then(function(e){
							dt.draw();
						});
					}
				});
			}
		})
	});

	$(document).on('click', '.edit-tab.button', function(e){
		var id = $(this).data('id');
		var url = '{{ url($pageUrl) }}/'+id+'/edit';
		window.location = url;
	});
</script>
@endsection