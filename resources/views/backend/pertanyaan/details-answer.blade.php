<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Detail Jawaban</div>
<div class="content">
	<form class="ui data form" id="modalForm" action="{{ url($pageUrl.'update-jawaban/') }}" method="POST">
		{!! csrf_field() !!}

		<div class="field">
			<label>Referensi </label>
			<input type="text" name="referensi" value="{{ $jawaban->referensi }}">
			<input type="hidden" name="id_trans_tanya_jawab" value="{{ $record->id }}">
			<input type="hidden" name="id" value="{{ $jawaban->id }}">
		</div>
		<div class="field">
			<label>Jawaban yang diberikan</label>
			<textarea class="editor" name="jawaban" rows="5">{{ $jawaban->jawaban }}</textarea>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<button class="ui right labeled icon save-modal button green">
	  Simpan
	  <i class="checkmark icon"></i>
	</button>
</div>