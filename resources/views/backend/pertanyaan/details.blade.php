{{-- {{ dd($jawaban) }} --}}

@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@endsection

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@endsection

@section('styles')
<style>
.answer-title {
	text-align: center;
}
.answer-button-holder {
	text-align: center
}
.divided.list > .item{
	padding: 15px 10px;
	padding-bottom: 15px !important;
	border-radius: 2px;
}
.divided.list > .item .meta{
	font-size: .9em;
}
.divided.list > .active.item{
	background: rgb(33, 133, 208, .7); 
	color: #fff;
}
.divided.list > .muted.item{
	background: #f8f8f8;
	color: rgba(0,0,0,.4); 
	font-weight: bold;
	padding: 20px 10px !important;
}
.divided.list > .active.item .header{
	color: #fff;
}
</style>
@endsection

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	<div class="ui segment">
		{!! csrf_field() !!}
		<input type="hidden" name="id_tanya_jawab" value="{{ $record->id }}">
		{{-- <div class="ui error message"></div> --}}
		<div class="ui equal width grid">
			<div class="column">
				<div class="ui right floated small blue horizontal label header" style="margin-top: 3px">{{ $record->kategori->nama }}</div>
				<h3 class="ui left floated header" style="margin-bottom: 10px">
					Pertanyaan & Diskusi
				</h3>
				<div class="ui clearing divider"></div>
				<div class="ui top aligned divided list">
					<div class="item">
						<div class="ui grid content">
							<div class="eleven wide column">
								<h3 class="ui header">{{ $record->judul }}</h3>
								<p>{!! $record->konten !!}</p>
							</div>
							<div class="five wide column">
								<h5 class="ui header">{{ $record->nama }}</h5>
								<p class="meta" style="margin-bottom: 2px"><i class="mail icon"></i>{{ $record->email }}</p>
								<p class="meta" style="margin-bottom: 2px"><i class="calendar icon"></i>{{ $record->created_at->format('d/m/Y') }}</p>
							</div>
						</div>
					</div>
					@if($record->detail->count() > 0)
						@foreach($record->detail as $detail)
							<div class="@if($detail->internal) active @endif item">
								<div class="ui grid content">
									<div class="eleven wide column">
										<h3 class="ui header">{{ $detail->judul }}</h3>
										<p>{!! $detail->konten !!}</p>
										@if(!is_null($detail->referensi))
											<a href="{{ $detail->referensi }}">Referensi</a>
										@endif
									</div>
									<div class="five wide column">
										<h5 class="ui header">{{ $detail->nama }}</h5>
										<p class="meta" style="margin-bottom: 2px"><i class="mail icon"></i>{{ $detail->email }}</p>
										<p class="meta" style="margin-bottom: 2px"><i class="calendar icon"></i>{{ $detail->created_at->format('d/m/Y') }}</p>
									</div>
								</div>
							</div>
						@endforeach
					@else
						<div class="muted item">
							<div class="center aligned content">
								<i>Belum terdapat jawaban / diskusi pada pertanyaan ini</i>
							</div>
						</div>
					@endif
				</div>
			</div>
			<div class="column">
				<div class="fields">
					<div class="nine wide field">
						<label>Nama</label>
						<input type="text" name="nama" placeholder="Nama Dokter / Pemberi Jawaban">
					</div>
					<div class="seven wide field">
						<label>Email</label>
						<input type="email" name="email" placeholder="Alamat Email Dokter / Pemberi Jawaban">
					</div>
				</div>
				<div class="fields">
					<div class="nine wide field">
						<label>Judul</label>
						<input type="text" name="judul" placeholder="Judul Jawaban">
					</div>
					<div class="seven wide field">
						<label>Referensi</label>
						<input type="text" name="referensi" placeholder="Referensi jika Ada">
					</div>
				</div>
				<div class="field">
					<label>Jawaban</label>
					<textarea placeholder="Jawaban" name="konten"></textarea>
				</div>
			</div>
		</div>
		<div class="ui clearing divider"></div>
		<div class="ui labeled icon button" onclick="window.history.back()">
			<i class="chevron left icon"></i>
			Kembali
		</div>
		<button class="ui right floated right labeled blue icon save page button">
			<i class="save icon"></i>
			Simpan
		</button>
	</div>
</form>
	
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).on('click', '.answer.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/"+ {{ $record->id }} +"/details/jawaban/"+id;
		// alert(url)
		loadModal(url);
	});

	$(document).on('click', '.answer-delete.button', function(e){
		var id = $(this).data('id');

		swal({
			title: 'Apakah anda yakin?',
			text: "Jawaban yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Hapus',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: '{{ url($pageUrl) }}/delete-jawaban/'+id,
					type: 'GET',
					data: {_token: "{{ csrf_token() }}"},
					success: function(resp){
						swal(
							'Terhapus!',
							'Data berhasil dihapus.',
							'success'
						).then(function(e){
							location.reload();
						});
					},
					error : function(resp){
						var data = resp.responseJSON;
						console.log(data)
						swal(
							'Gagal!',
							Object.is(data.message, undefined) ? 'Data gagal dihapus, eror tidak diketahui' : data.message,
							'error'
							).then(function(e){
								location.reload();
							});
						}
				});
			}
		})
	});

	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '200px';
		tinymce.init(tinymceOpt);

		// $('.answer.button').popup();

		$('.date').calendar({
			type: 'month',
			formatter: {
				date: function(date, setting) {
					var year = date.getFullYear();
					var month = date.getMonth() + 1;
					if (month < 10) {
						month = '0' + month;
					}
					return year + '-' + month
				}
			}
		})
	});
</script>
@append

@section('init-modal')
<script>
	initModal = function(){
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '200px';
		tinymce.init(tinymceOpt);
	};
</script>
@endsection
