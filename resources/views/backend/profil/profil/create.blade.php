@extends('layouts.grid')
@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<input type="hidden" name="posisi" value="0">
	<div class="ui attached top segment">
		<div class="ui grid">
			<div class="twelve wide column">
				<div class="field">
					<div class="ui small labeled input">
						<label for="slug" class="ui label">{{ url('profil') }}/</label>
						<input type="text" placeholder="Slug" name="slug" readonly>
					</div>
				</div>
				<div id="tab_profil">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="profil_id">ID</a>
						<a class="item" data-tab="profil_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="profil_id">
						<div class="field">
							<div class="ui big input">
							  <input name="judul" placeholder="Judul Halaman Profil" type="text">
							</div>
						</div>
						<div class="field">
							<textarea name="konten" rows="3" class="editor"></textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="profil_en">
						<div class="field">
							<div class="ui big input">
							  <input name="judul_en" placeholder="Judul Halaman Profil (EN)" type="text">
							</div>
						</div>
						<div class="field">
							<textarea name="konten_en" rows="3" class="editor"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="four wide column">
				<div class="field image-container">
					<img class="image-preview"  style="height: 120px;" id="showAttachment" src="{{ asset('img/plc.png') }}">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Gambar Halaman Profil">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" multiple="">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				<div id="tab_deskripsi">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="deskripsi_id">ID</a>
						<a class="item" data-tab="deskripsi_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="deskripsi_id">
						<div class="field">
							<label for="">Deskripsi Singkat</label>
							<textarea name="keterangan" placeholder="Deskripsi Singkat" style="height: 95px; resize: none"></textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="deskripsi_en">
						<div class="field">
							<label for="">Deskripsi Singkat</label>
							<textarea name="keterangan_en" placeholder="Deskripsi Singkat (EN)" style="height: 95px; resize: none"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '320';
		tinymce.init(tinymceOpt);

		$('#tab_profil .menu .item').tab();	
		$('#tab_deskripsi .menu .item').tab();	

		$('[name=judul]').on('change, keyup', function(event) {
			var judul = $(this).val();
			$('[name=slug]').val(slugify(judul));
		});
	});
</script>
@endsection

