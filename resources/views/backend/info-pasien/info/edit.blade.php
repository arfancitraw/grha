@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">	
		<div class="ui attached top segment">
		
		<div class="ui grid">
			<div class="nine wide column">
				<div class="field">
					<label for="nama">Judul</label>
					<input type="text" name="judul" placeholder="Judul Profil" value="{{ $record->judul }}" onkeyup="menganu()">
				</div>
				
				<div class="field" >
					<label>Kontent</label>
					<textarea name="konten" rows="8" class="editor" placeholder="Konten Profil" style="height: 200px; resize:none;">{!! $record->konten !!}</textarea>
				</div>
			</div>
			<div class="seven wide column">
				<div class="field image-container">
					<label>Foto</label>
					<img class="image-preview"  style="height: 150px; object-fit: cover;" id="showAttachment" src="{{ asset('storage') }}/{{ $record->gambar}}">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Pilih foto sampul">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" accept="image/*">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				<div class="field">
					<label>Keterangan</label>
					<textarea name="keterangan" rows="4" placeholder="Keterangan" style="height: 100px; margin-top: 1rem; resize:none;">{!! $record->keterangan !!}</textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.editor').summernote({
		height: '168px'
	});
</script>
@endsection

