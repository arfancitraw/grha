@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	<div class="ui attached top segment">
		{!! csrf_field() !!}
		<input type="hidden" name="slug" readonly placeholder="Judul Pelayanan">
		<div class="ui grid">
			<div class="nine wide column">
				<div class="field">
					<label>Judul</label>
					<input type="text" name="judul" placeholder="Judul" onkeyup="menganu()">
				</div>
				
				<div class="field" >
					<label>Kontent</label>
					<textarea name="konten" rows="3" class="editor"></textarea>
				</div>
			</div>
			<div class="seven wide column">
				<div class="field image-container">
					<label>Foto</label>
					<img class="image-preview"  style="height: 180px" id="showAttachment" style="height: 27rem" src="http://fsm.undip.ac.id//assets/attachments/Images/default.jpg">
					<div class="ui fluid file input action">
						<input type="text" readonly="">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" accept="image/*">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				<div class="field">
					<label>Keterangan</label>
					<textarea name="keterangan" rows="3" placeholder="Keterangan"></textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.editor').summernote({
		height: '155px',
		 placeholder: "Konten..."
	});
</script>
@endsection

