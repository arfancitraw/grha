@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">	
	<div class="ui attached top segment">
		
		<div class="ui grid">
			<div class="five wide column">
				<div class="field">
					<label>Nama</label>
					<input type="text" name="nama" value="{{$record->nama}}" onkeyup="menganu()">
				</div>
				<div class="field">
					<label>Email</label>
					<input type="text" name="email" value="{{$record->email}}" onkeyup="menganu()">
				</div>
				<div class="field">
					<label for="nama">Layanan</label>
					<select name="layanan_yang_digunakan" class="ui selection search dropdown">
						<option value="">Pilih Layanan</option>
						{!! \App\Models\Layanan\Layanan::options('judul','id',['selected'=> $record->layanan_yang_digunakan]) !!}
					</select>
				</div>
				<div class="grouped inline fields">
					<label for="nama">Layanan</label>

					<div class="field">
						<div class="ui radio checkbox">
							<input type="radio" name="publik" value="1" <?php
							$x = $record->publik;
							if($x == '1'){
								$y = 'checked';
							}else{
								$y = ' ';
							}
							echo $y;
							?> >
							<label>Public</label>
						</div>
					</div>
					<div class="field">
						<div class="ui radio checkbox">
							<input type="radio" name="publik" value="0" <?php
							$x1 = $record->publik;
							if($x1 == '0'){
								$y1 = 'checked';
							}else{
								$y1 = ' ';
							}
							echo $y1;
							?> >
							<label>Privat</label>
						</div>
					</div>
				</div>
			</div>
			<div class="eleven wide column">
				<div class="field image-container">
					<div class="field" >
						<label>Testimoni</label>
						<textarea name="testimoni" rows="3" class="editor">{{$record->testimoni}}</textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.editor').summernote({
		height: '168px'
	});
</script>
@endsection

