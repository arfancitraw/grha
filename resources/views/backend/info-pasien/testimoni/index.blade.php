@extends('layouts.grid')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
	<style type="text/css" media="screen">
		.long-text {
	display: none
}	
	</style>
@append

@section('js')
	<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
	<script type="text/javascript">

		$(document).on('click','.show-more-button', function() {
			console.log('askld');
	// If text is shown less, then show complete
	if($(this).attr('data-more') == 0) {
		$(this).attr('data-more', 1);
		$(this).css('display', 'block');
		$(this).text('Read Less');

		$(this).prev().css('display', 'none');
		$(this).prev().prev().css('display', 'inline');
	}
	// If text is shown complete, then show less
	else if(this.getAttribute('data-more') == 1) {
		$(this).attr('data-more', 0);
		$(this).css('display', 'inline');
		$(this).text('Read More');

		$(this).prev().css('display', 'inline');
		$(this).prev().prev().css('display', 'none');
	}
});
	</script>
@append

@section('filters')
	<div class="field">
		<input name="filter[nama]" placeholder="nama" type="text">
	</div>
	<div class="field">
		<input name="filter[email]" placeholder="email" type="text">
	</div>
	<div class="field">
		<select name="filter[publik]" id="" class="ui dropdown selection">
			<option value="">Pilih status</option>
			<option value="0">Publik</option>
			<option value="1">Private</option>
		</select>
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
<a class="ui blue button add-page">
	<i class="plus icon"></i> Tambah Data
</a>
@endif
@endsection

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
    d.email = $("input[name='filter[email]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nama: 'empty',
		konten: 'empty',
		email: 'empty'
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
      $('.editor').summernote({
		  height: '200px',
		  placeholder: "Konten..."
	  });
	};
</script>
@endsection