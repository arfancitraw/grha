<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Tautan Footer</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
	    <input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<div class="field">
			<label for="nama">Nama</label>
			<input type="text" name="nama" placeholder="Nama" value="{{ $record->nama or '' }}">
		</div>
		<div class="field">
			<label for="url">Url</label>
			<input type="text" name="url" placeholder="Url" value="{{ $record->url or '' }}">
		</div>
		<div class="field">
			<label for="deskripsi">Deskripsi</label>
			<textarea name="deskripsi" rows="4" style="height: 100px; resize:none;">{!! $record->deskripsi or '' !!}</textarea>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>