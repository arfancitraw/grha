@extends('layouts.grid')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
	<style type="text/css">
		.img-container{
			width: 100%;
			max-height: 500px;
			overflow-y: auto;
		}
	</style>
@append

@section('js')
	<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
	<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js')}}"></script>
@append

@section('content-body')
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="ui top attached segment">
			<div class="ui two column grid">
				<div class="left aligned column">
					<div class="ui labeled icon button" onclick="window.history.back()">
						<i class="chevron left icon"></i>
						Kembali
					</div>
				</div>
				{{-- <div class="right aligned column">
					<button class="ui right labeled blue icon save as page button">
						<i class="save icon"></i>
						Simpan
					</button>
				</div> --}}
			</div>
		</div>
		<div class="ui attached segment">
			<div class="ui stackable grid">
				<div class="seven wide column">
					<table class="ui very basic table">
						<tbody>
							<tr>
								<td style="width:150px"><b>Nama Lengkap</b></td>
								<td>{{ $record->nama }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Telepon</b></td>
								<td>{{ $record->telepon }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Email</b></td>
								<td><a href="mailto:{{ $record->email }}">{{ $record->email }}</a></td>
							</tr>
							<tr>
								<td class="top aligned"><b>Perihal</b></td>
								<td>{{ $record->perihal }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Uraian</b></td>
								<td><div style="min-height: 100px">{!! e(nl2br($record->uraian)) !!}</div></td>
							</tr>
							<tr>
								<td class="top aligned"><b>Keterangan</b></td>
								<td><div style="min-height: 100px">{!! e(nl2br($record->keterangan)) !!}</div></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="nine wide column">
					@if (pathinfo($record->bukti, PATHINFO_EXTENSION) == 'pdf')
						<embed src="{{ url('storage/'.$record->bukti) }}" width="100%" height="500" type='application/pdf'>
					@else
						<div class="img-container">
							<img src="{{ url('storage/'.$record->bukti) }}" alt="{{ $record->perihal }}" style="width:100%; height: auto;">
						</div>
					@endif
				</div>
			</div>
		</div>
	</form>
@endsection