@extends('layouts.grid')

@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="ui stackable grid">
		{{-- Email Pengaduan --}}
			<!--div class="four wide column">
				<h2 class="ui header">
				  Email Pengaduan
				  <div class="sub header">Saat terdapat pengaduan tertentu, email akan dikirimkan kepada email terkait.</div>
				</h2>
			</div>
			<div class="twelve wide column">
				<div class="two fields">
					<div class="field">
						<label>Whistleblowing</label>
						<input type="email" name="setting[{{$settings['pengaduan-email-etik']->id}}][isi]" placeholder="Isikan dengan email yang valid" value="{{ $settings['pengaduan-email-etik']->isi }}">
					</div>
					<div class="field">
						<label>Whistleblowing</label>
						<input type="email" name="setting[{{$settings['pengaduan-email-wbs']->id}}][isi]" placeholder="Isikan dengan email yang valid" value="{{ $settings['pengaduan-email-wbs']->isi }}">
					</div>
				</div>
				<div class="two fields">
					<div class="field">
						<label>Pengaduan Masyarakat</label>
						<input type="email" name="setting[{{$settings['pengaduan-email-masyarakat']->id}}][isi]" placeholder="Isikan dengan email yang valid" value="{{ $settings['pengaduan-email-masyarakat']->isi }}">
					</div>
					<div class="field">
						<label>Gratifikasi</label>
						<input type="email" name="setting[{{$settings['pengaduan-email-gratifikasi']->id}}][isi]" placeholder="Isikan dengan email yang valid" value="{{ $settings['pengaduan-email-gratifikasi']->isi }}">
					</div>
				</div>
				<div class="two fields">
					<div class="field">
						<label>Sponsor</label>
						<input type="email" name="setting[{{$settings['pengaduan-email-sponsor']->id}}][isi]" placeholder="Isikan dengan email yang valid" value="{{ $settings['pengaduan-email-sponsor']->isi }}">
					</div>
					<div class="field">
					</div>
				</div>
			</div>
			<div class="sixteen wide column">
				<div class="ui fitted divider"></div>
			</div-->
		{{-- End Of Email Pengaduan --}}

		{{-- Etik & Hukum --}}
			<div class="four wide column">
				<h2 class="ui header">
				  Narasi Pengaduan <br>Etik & Hukum
				  <div class="sub header">Pengaturan narasi untuk pengaduan bagian Etik & Hukum.</div>
				</h2>
			</div>
			<div class="twelve wide column">
				<textarea name="setting[{{$settings['pengaduan-narasi-etik']->id}}][isi]" rows="3" class="editor">{!! $settings['pengaduan-narasi-etik']->isi !!}</textarea>
			</div>
			<div class="sixteen wide column">
				<div class="ui fitted divider"></div>
			</div>
		{{-- End Of Etik & Hukum --}}

		{{-- Whistleblowing --}}
			<div class="four wide column">
				<h2 class="ui header">
				  Narasi Pengaduan <br>Whistleblowing
				  <div class="sub header">Pengaturan narasi untuk pengaduan bagian Whistleblowing.</div>
				</h2>
			</div>
			<div class="twelve wide column">
				<textarea name="setting[{{$settings['pengaduan-narasi-wbs']->id}}][isi]" rows="3" class="editor">{!! $settings['pengaduan-narasi-wbs']->isi !!}</textarea>
			</div>
			<div class="sixteen wide column">
				<div class="ui fitted divider"></div>
			</div>
		{{-- End Of Whistleblowing --}}

		{{-- Pengaduan Masyarakat --}}
			<div class="four wide column">
				<h2 class="ui header">
				  Narasi Pengaduan <br>Masyarakat
				  <div class="sub header">Pengaturan narasi untuk pengaduan bagian Pengaduan Masyarakat.</div>
				</h2>
			</div>
			<div class="twelve wide column">
				<textarea name="setting[{{$settings['pengaduan-narasi-masyarakat']->id}}][isi]" rows="3" class="editor">{!! $settings['pengaduan-narasi-masyarakat']->isi !!}</textarea>
			</div>
			<div class="sixteen wide column">
				<div class="ui fitted divider"></div>
			</div>
		{{-- End Of Pengaduan Masyarakat --}}

		{{-- Pengaduan Gratifikasi --}}
			<div class="four wide column">
				<h2 class="ui header">
				  Narasi Pengaduan <br>Gratifikasi
				  <div class="sub header">Pengaturan narasi untuk pengaduan bagian Pengaduan Gratifikasi.</div>
				</h2>
			</div>
			<div class="twelve wide column">
				<textarea name="setting[{{$settings['pengaduan-narasi-gratifikasi']->id}}][isi]" rows="3" class="editor">{!! $settings['pengaduan-narasi-gratifikasi']->isi !!}</textarea>
			</div>
			<div class="sixteen wide column">
				<div class="ui fitted divider"></div>
			</div>
		{{-- End Of Pengaduan Gratifikasi --}}

		{{-- Pengaduan Sponsor --}}
			<div class="four wide column">
				<h2 class="ui header">
				  Narasi Pengaduan <br>Sponsor
				  <div class="sub header">Pengaturan narasi untuk pengaduan bagian Pengaduan Sponsor.</div>
				</h2>
			</div>
			<div class="twelve wide column">
				<textarea name="setting[{{$settings['pengaduan-narasi-sponsor']->id}}][isi]" rows="3" class="editor">{!! $settings['pengaduan-narasi-sponsor']->isi !!}</textarea>
			</div>
			<div class="sixteen wide column">
				<div class="ui fitted divider"></div>
			</div>
		{{-- End Of Pengaduan Sponsor --}}
	</div>
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui black labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<div class="ui right labeled green icon save as page button">
				<i class="save icon"></i>
				Simpan
			</div>
		</div>
	</div>

</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '200';
		tinymce.init(tinymceOpt);

		$('.menu .item').tab();	
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@endsection

