@extends('layouts.grid')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
	<style type="text/css">
		.img-container{
			width: 100%;
			max-height: 500px;
			overflow-y: auto;
		}
	</style>
@append

@section('js')
	<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
	<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js')}}"></script>
@append

@section('content-body')
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="ui top attached segment">
			<div class="ui two column grid">
				<div class="left aligned column">
					<div class="ui labeled icon button" onclick="window.history.back()">
						<i class="chevron left icon"></i>
						Kembali
					</div>
				</div>
				{{-- <div class="right aligned column">
					<button class="ui right labeled blue icon save as page button">
						<i class="save icon"></i>
						Simpan
					</button>
				</div> --}}
			</div>
		</div>
		<div class="ui attached segment">
			<div class="ui stackable grid">
				<div class="eight wide column">
					<table class="ui very basic table">
						<tbody>
							<tr>
								<td style="width:150px"><b>Nip / Nopeg</b></td>
								<td>{{ $record->nip_nopeg }}</td>
							</tr>
							<tr>
								<td style="width:150px"><b>Nama Lengkap</b></td>
								<td>{{ $record->nama }}</td>
							</tr>
							<tr>
								<td style="width:150px"><b>Jabatan</b></td>
								<td>{{ $record->jabatan }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Alamat</b></td>
								<td>{{ $record->alamat }}</td>
							</tr>
						</tbody>
					</table>
					@if(count($record->bukti)>0)
					@foreach($record->bukti as $bukti)
					@if (pathinfo($bukti->file, PATHINFO_EXTENSION) == 'pdf')
						<embed src="{{ url('storage/'.$bukti->file) }}" width="100%" height="500" type='application/pdf'>
					@else
						<div class="img-container">
							<img src="{{ url('storage/'.$bukti->file) }}" alt="{{ $record->nama_kegiatan }}" style="width:100%; height: auto;">
						</div>
					@endif
					@endforeach
					@endif
				</div>
				<div class="eight wide column">
					<table class="ui very basic table">
						<tbody>
							<tr>
								<td style="width:200px"><b>Nama Pemberi</b></td>
								<td>{{ $record->nama_pemberi }}</td>
							</tr>
							<tr>
								<td style="width:150px"><b>Nama Perusahaan</b></td>
								<td>{{ $record->nama_perusahaan }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Tanggal</b></td>
								<td>{{ \Carbon\Carbon::createFromFormat('Y-m-d',$record->tanggal)->format('d F Y') }}</td>
							</tr>
							<tr>
								<td style="width:150px"><b>Tempat Kegiatan</b></td>
								<td>{{ $record->tempat_kegiatan }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Peranan</b></td>
								<td>{{ $record->peranan }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Pemberi Sponsor</b></td>
								<td>{{ $record->pemberi_sponsor }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Registrasi</b></td>
								<td>{{ $record->registrasi }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Akomodasi</b></td>
								<td>{{ $record->akomodasi }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Transportasi</b></td>
								<td>{{ $record->transportasi }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Honor</b></td>
								<td>{{ $record->honor }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Total</b></td>
								<td>{{ $record->total }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</form>
@endsection