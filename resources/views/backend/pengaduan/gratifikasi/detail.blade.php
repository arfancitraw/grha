@extends('layouts.grid')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<style type="text/css">
.img-container{
	width: 100%;
	max-height: 500px;
	overflow-y: auto;
}
</style>
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js')}}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="ui top attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
				{{-- <div class="right aligned column">
					<button class="ui right labeled blue icon save as page button">
						<i class="save icon"></i>
						Simpan
					</button>
				</div> --}}
			</div>
		</div>
		<div class="ui attached segment">
			<div class="ui stackable grid">
				<div class="eight wide column">
					<table class="ui very basic table">
						<tbody>
							<tr>
								<td style="width:150px"><b>Nip / Nopeg</b></td>
								<td>{{ $record->nip_nopeg }}</td>
							</tr>
							<tr>
								<td style="width:150px"><b>Nama Lengkap</b></td>
								<td>{{ $record->nama }}</td>
							</tr>
							<tr>
								<td style="width:150px"><b>Jabatan / Unit Kerja</b></td>
								<td>{{ $record->jabatan_unit_kerja }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Alamat / Telepon / Email</b></td>
								<td>{{ $record->alamat_telepon_email }}</td>
							</tr>
						</tbody>
					</table>
					{{-- <table class="ui celled compact red responsive table" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>No</th>
								<th>File</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>File</td>
							</tr>
						</tbody>
					</table> --}}
					@if(count($record->bukti)>0)
					@foreach($record->bukti as $bukti)
					@if (pathinfo($bukti->file, PATHINFO_EXTENSION) == 'pdf')
					<embed src="{{ url('storage/'.$bukti->file) }}" width="100%" height="500" type='application/pdf'>
					@else
					<div class="img-container">
						<img src="{{ url('storage/'.$bukti->file) }}" alt="{{ $record->jenis_bentuk_penerimaan }}" style="width:100%; height: auto;">
					</div>
					@endif
					@endforeach
					@endif
				</div>
				<div class="eight wide column">
					<table class="ui very basic table">
						<tbody>
							<tr>
								<td style="width:200px"><b>Jenis / Bentuk Penerimaan</b></td>
								<td>{{ $record->jenis_bentuk_penerimaan }}</td>
							</tr>
							<tr>
								<td style="width:150px"><b>Harga / Nilai Nominal</b></td>
								<td>{{ $record->harga_nilai_nominal }}</td>
							</tr>
							<tr>
								<td style="width:150px"><b>Kaitan Peristiwa Penerimaan</b></td>
								<td>{{ $record->kaitan_peristiwa_penerimaan }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Tempat Penerimaan</b></td>
								<td>{{ $record->tempat_penerimaan }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Tanggal Penerimaan</b></td>
								<td>{{ \Carbon\Carbon::createFromFormat('Y-m-d',$record->tanggal_penerimaan)->format('d F Y') }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Nama Pemberi</b></td>
								<td>{{ $record->nama_pemberi }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Pekerjaan / Jabatan Pemberi</b></td>
								<td>{{ $record->pekerjaan_jabatan }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Hubungan Penerima</b></td>
								<td>{{ $record->hubungan_penerima }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Alasan Pemberian</b></td>
								<td>{{ $record->alasan_pemberian }}</td>
							</tr>
							<tr>
								<td class="top aligned"><b>Kronologi</b></td>
								<td><div style="min-height: 100px">{!! e(nl2br($record->kronologi)) !!}</div></td>
							</tr>
							<tr>
								<td class="top aligned"><b>Catatan</b></td>
								<td><div style="min-height: 100px">{!! e(nl2br($record->catatan)) !!}</div></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</form>
	@endsection