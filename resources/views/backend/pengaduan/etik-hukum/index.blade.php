@extends('layouts.grid')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
	<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('filters')
	<div class="field">
		<input name="filter[perihal]" placeholder="Perihal" type="text">
	</div>
	<div class="field">
		<input name="filter[nama]" placeholder="Nama" type="text">
	</div>
	<div class="field">
		<input name="filter[telepon]" placeholder="Telepon" type="text">
	</div>
	<div class="field">
		<input name="filter[email]" placeholder="Email" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))

@endif
@endsection

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
    d.perihal = $("input[name='filter[perihal]']").val();
    d.telepon = $("input[name='filter[telepon]']").val();
    d.email = $("input[name='filter[email]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nama: 'empty',
		jabatan: 'empty',
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
		$('.editor').summernote({
		  height: '200px',
		  placeholder: "Konten profil..."
		});
	};

</script>
@endsection