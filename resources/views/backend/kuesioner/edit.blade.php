@extends('layouts.grid')

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">	
	<div class="ui attached top segment">
		{{-- <div class="ui error message"></div> --}}
		<div class="sixteen wide column inner2bottom inner1 inner2" id="inner2bottom">

			<div class="fields">
				<div class="fourteen wide field">
					<label>Periode</label>
					<input name="periode" placeholder="Periode Kuisioner (contoh: Semester 1 - 2018)" type="text" value="{{ $record->periode or '' }}">
				</div>
				<div class="two wide field">
					<label>Status</label>
					<div class="ui toggle checkbox" style="margin-top:5px;">
						<input type="hidden" name="status" value="0">
						<input name="status" tabindex="0" class="hidden" type="checkbox" value="1" readonly @if($record->status) checked @endif>
						<label>Aktif</label>
					</div>
				</div>
			</div>
			<h4 class="ui dividing header">Pertanyaan</h4>
			<table class="ui celled padded table">
				<thead>
					<tr>
						<th class="center aligned">Pertanyaan</th>
						<th class="center aligned" style="width: 200px">Tipe</th>
						<th class="center aligned" style="width: 100px">Aksi</th>
					</tr>
				</thead>
				<tbody class="sub container">
					@if($record->detail->count() > 0)
					@php $i = 0; @endphp
					@foreach ($record->detail as $detail)
					<tr data-count="{{$i}}">
						<td>
							<input type="hidden" name="subid[]" value="{{ $detail->id }}">
							<input type="hidden" name="detail[{{$i}}][id]" value="{{ $detail->id }}">
							<div class="two fields">
								<div class="field">
									<div class="ui labeled small fluid input">
										<div class="ui label">
									    	ID
									  	</div>
										<input name="detail[{{$i}}][pertanyaan][id]" placeholder="Pertanyaan" type="text" value="{{ $detail->pertanyaan }}" required>
									</div>
								</div>
								<div class="field">
									<div class="ui labeled small fluid input">
										<div class="ui label">
									    	EN
									  	</div>
										<input name="detail[{{$i}}][pertanyaan][en]" placeholder="Pertanyaan" type="text" value="{{ $detail->pertanyaan_en }}" required>
									</div>
								</div>
							</div>
						</td>
						<td class="center aligned">
							<div class="inline fields" style="margin:0">
								<div class="field">
									<div class="ui radio checkbox">
										<input name="detail[{{$i}}][tipe]" checked="checked" type="radio" value="0" @if(!$detail->tipe) checked @endif>
										<label>Rating</label>
									</div>
								</div>
								<div class="field">
									<div class="ui radio checkbox">
										<input name="detail[{{$i}}][tipe]" type="radio" value="1" @if($detail->tipe) checked @endif>
										<label>Pernyataan</label>
									</div>
								</div>
							</div>
						</td>
						<td class="center aligned">
							@if($i == 0)
							<button type="button" class="ui green icon button add-sub" data-content="Tambah Pertanyaan">
								<i class="plus icon"></i>
							</button>
							@else
							<button type="button" class="ui red icon button remove-sub" data-content="Hapus Pertanyaan">
								<i class="delete icon"></i>
							</button>
							@endif
						</td>
					</tr>
					@php $i++; @endphp
					@endforeach
					@else
					<tr data-count="0">
						<td>
							<div class="two fields">
								<div class="field">
									<div class="ui labeled small fluid input">
										<div class="ui label">
									    	ID
									  	</div>
										<input name="detail[0][pertanyaan][id]" placeholder="Pertanyaan" type="text" required>
									</div>
								</div>
								<div class="field">
									<div class="ui labeled small fluid input">
										<div class="ui label">
									    	EN
									  	</div>
										<input name="detail[0][pertanyaan][en]" placeholder="Pertanyaan" type="text" required>
									</div>
								</div>
							</div>
						</td>
						<td class="center aligned">
							<div class="inline fields" style="margin:0">
								<div class="field">
									<div class="ui radio checkbox">
										<input name="detail[0][tipe]" checked="checked" type="radio" value="0">
										<label>Rating</label>
									</div>
								</div>
								<div class="field">
									<div class="ui radio checkbox">
										<input name="detail[0][tipe]" type="radio" value="1">
										<label>Pernyataan</label>
									</div>
								</div>
							</div>
						</td>
						<td class="center aligned">
							<button type="button" class="ui green icon button add-sub" data-content="Tambah Pertanyaan">
								<i class="plus icon"></i>
							</button>
						</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<script>
	$(document).ready(function($) {
		$('.ui.checkbox').checkbox();
		$('[data-content]').popup({
			hoverable: true,
			position : 'top center',
			delay: {
				show: 300,
				hide: 800
			}
		});
		
		$(document).on('click', '.remove-sub', function(event) {
			$(this).closest('tr').remove();
		});

		$('.add-sub').click(function(event) {
			var idx = parseInt($('.sub.container tr').last().data('count')) + 1;
			var html = `<tr data-count="`+idx+`">
			<td>
			<div class="two fields">
			<div class="field">
			<div class="ui labeled small fluid input">
			<div class="ui label">
			ID
			</div>
			<input name="detail[`+idx+`][pertanyaan][id]" placeholder="Pertanyaan" type="text" required>
			</div>
			</div>
			<div class="field">
			<div class="ui labeled small fluid input">
			<div class="ui label">
			EN
			</div>
			<input name="detail[`+idx+`][pertanyaan][en]" placeholder="Pertanyaan" type="text" required>
			</div>
			</div>
			</div>
			</td>
			<td class="center aligned">
			<div class="inline fields" style="margin:0">
			<div class="field">
			<div class="ui radio checkbox">
			<input name="detail[`+idx+`][tipe]" checked="checked" type="radio">
			<label>Rating</label>
			</div>
			</div>
			<div class="field">
			<div class="ui radio checkbox">
			<input name="detail[`+idx+`][tipe]" type="radio">
			<label>Pernyataan</label>
			</div>
			</div>
			</div>
			</td>
			<td class="center aligned">
			<button type="button" class="ui red icon button remove-sub" data-content="Hapus Pertanyaan">
			<i class="delete icon"></i>
			</button>
			</td>
			</tr>`;

			$('.sub.container').append(html);

			$('.ui.checkbox').checkbox();
			$('[data-content]').popup({
				hoverable: true,
				position : 'top center',
				delay: {
					show: 300,
					hide: 800
				}
			});
		});
	});	
</script>
@append