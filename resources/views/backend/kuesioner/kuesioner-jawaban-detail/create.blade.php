@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	<div class="ui attached top segment">
		{!! csrf_field() !!}
		<input type="hidden" name="slug" readonly placeholder="Judul Pelayanan">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="nama">Nama</label>
					<input type="text" name="nama">
				</div>
				<div class="field">
					<label for="email">Email</label>
					<input type="text" name="email">
				</div>
				<div class="field">
					<label for="nama">Kuesioner Jawaban</label>
					<select name="kuesioner_jawaban_id" class="ui search dropdown">
						{{-- <option value="">Pilih Kuesioner</option> --}}
						{!! \App\Models\Kuesioner\KuesionerJawaban::options('nama', 'id') !!}
					</select>
				</div>
				<div class="field">
					<label for="nama">Kuesioner detail</label>
					<select name="kuesioner_detail_id" class="ui search dropdown">
						{{-- <option value="">Pilih Kuesioner</option> --}}
						{!! \App\Models\Master\KuesionerDetail::options('pertanyaan', 'id') !!}
					</select>
				</div>
				<div class="field">
					<label for="">Jawaban</label>
						<div class="inline fields">
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="jawaban" value="1"  checked="checked">
									<label>satu</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="jawaban" value="2" >
									<label>dua</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="jawaban" value="3" >
									<label>tiga</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="jawaban" value="4" >
									<label>empat</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="jawaban" value="5" >
									<label>Lima</label>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.editor').summernote({
		height: '155px',
		placeholder: "Konten..."
	});
</script>
@endsection