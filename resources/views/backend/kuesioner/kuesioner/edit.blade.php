@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">	
	<div class="ui attached top segment">
		<div class="ui grid">
			<div class="eight wide column">
				<div class="field">
					<label for="nama">Nama</label>
					<input type="text" name="nama" value="{{$record->nama}}">
				</div>
				<div class="field">
					<label for="tahun">Email</label>
					<input type="email" name="email" value="{{$record->email}}">
				</div>
				<div class="field">
					<label for="">Profesi</label>
					<input type="text" name="profesi" value="{{$record->profesi}}">
				</div>
			</div>
			<div class="eight wide column">
				<div class="field">
					<label for="tahun">Kepuasan Layanan</label>
					<div class="ui star rating" data-rating="3"></div>
					<div class="ui form">
						<div class="inline fields">
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="kepuasan_layanan" value="1" {{ ($record->kepuasan_layanan == 1 ) ? 'checked' : '' }} {{ ($record->status == 0 ) ? '' : '' }}>
									<label>satu</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="kepuasan_layanan" value="2" {{ ($record->kepuasan_layanan == 2 ) ? 'checked' : '' }} {{ ($record->status == 0 ) ? '' : '' }}>
									<label>dua</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="kepuasan_layanan" value="3" {{ ($record->kepuasan_layanan == 3 ) ? 'checked' : '' }} {{ ($record->status == 0 ) ? '' : '' }}>
									<label>tiga</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="kepuasan_layanan" value="4" {{ ($record->kepuasan_layanan == 4 ) ? 'checked' : '' }} {{ ($record->status == 0 ) ? '' : '' }}>
									<label>empat</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="kepuasan_layanan" value="5" {{ ($record->kepuasan_layanan == 5 ) ? 'checked' : '' }} {{ ($record->status == 0 ) ? '' : '' }}>
									<label>Lima</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="field">
					<label for="tahun">Kepuasan Fasilitas Website</label>
					<div class="ui form">
						<div class="inline fields">
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="kepuasan_fasilitas_website" value="1" checked="checked" {{ ($record->kepuasan_fasilitas_website == 1 ) ? 'checked' : '' }} {{ ($record->status == 0 ) ? '' : '' }}>
									<label>satu</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="kepuasan_fasilitas_website" value="2" {{ ($record->kepuasan_fasilitas_website == 2 ) ? 'checked' : '' }} {{ ($record->status == 0 ) ? '' : '' }}>
									<label>dua</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="kepuasan_fasilitas_website" value="3" {{ ($record->kepuasan_fasilitas_website == 3 ) ? 'checked' : '' }} {{ ($record->status == 0 ) ? '' : '' }}>
									<label>tiga</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="kepuasan_fasilitas_website" value="4" {{ ($record->kepuasan_fasilitas_website == 4 ) ? 'checked' : '' }} {{ ($record->status == 0 ) ? '' : '' }}>
									<label>empat</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="kepuasan_fasilitas_website" value="5" {{ ($record->kepuasan_fasilitas_website == 5 ) ? 'checked' : '' }} {{ ($record->status == 0 ) ? '' : '' }}>
									<label>Lima</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="sixteen wide column">
				<div class="field">
					<label for="">Feedback</label>
					<textarea name="feedback" rows="5" class="editor" style="height: 301px; resize:none;">{{$record->feedback}}</textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.editor').summernote({
		height: '168px'
	});
	$('.date').calendar({
		type: 'date',
		// formatter: {
		// 	date: function(date, setting) {
		// 		var year = date.getFullYear();
		// 		var month = date.getMonth() + 1;
		// 		var day = date.getDate();
		// 		if (month < 10) {
		// 			month = '0' + month;
		// 		}
		// 		if (day < 10) {
		// 			day = '0' + day;
		// 		}
		// 		return day + '/' + month + '/' + year
		// 	}
		// }
	})
	$('.time').calendar({
		type: 'time',
		ampm: false
	})
</script>
@endsection