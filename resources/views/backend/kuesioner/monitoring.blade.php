@extends('layouts.grid')

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">	
	<div class="ui attached top segment">
		{{-- <div class="ui error message"></div> --}}
		<div class="sixteen wide column inner2bottom inner1 inner2" id="inner2bottom">

			<div class="fields">
				<div class="fourteen wide field">
					<label>Periode</label>
					<input name="periode" placeholder="Periode Kuisioner (contoh: Semester 1 - 2018)" type="text" value="{{ $record->periode or '' }}" readonly>
				</div>
				<div class="two wide field">
					<label>Status</label>
					<div class="ui toggle checkbox" style="margin-top:5px;">
						<input type="hidden" name="status" value="0">
						<input name="status" tabindex="0" class="hidden" type="checkbox" value="1" readonly @if($record->status) checked @endif>
						<label>Aktif</label>
					</div>
				</div>
			</div>
			<h4 class="ui dividing header">Pertanyaan</h4>
			<script src="{{ asset('plugins/highchart/highcharts.js') }}" type="text/javascript"></script>
			<div class="ui styled fluid accordion">
				@foreach($record->detail as $detail)
				<div class="title">
					<i class="dropdown icon"></i>
					{{ $detail->pertanyaan }}
				</div>
				<div class="content">
					<div class="transition">
						@if(count($detail->jawaban) == 0)
						<h4 style="color: #888"><i>Pertanyaan terkait belum memiliki Jawaban</i></h4>
						@else
							@if($detail->tipe)
								<table class="ui stripped table">
									<thead>										<tr>
											<th>No.</th>
											<th>Nama</th>
											<th>Email</th>
											<th>Jawaban</th>
										</tr>
									</thead>
									<tbody>
										@php $i=1; @endphp
										@foreach($detail->jawaban as $jawaban)
										<tr>
											<td>{{ $i++ }}</td>
											<td>{{ $jawaban->kuesionerjawaban->nama }}</td>
											<td>{{ $jawaban->kuesionerjawaban->email }}</td>
											<td>{{ $jawaban->jawaban }}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							@else
								@php
									$data = $detail->jawaban->groupBy('jawaban');
								@endphp
								<div id="container{{$detail->id}}" style="min-width: 310px; height: 300px; max-width: 600px; margin: 0 auto"></div>
								<script>							
									Highcharts.chart('container{{$detail->id}}', {
										chart: {
											plotBackgroundColor: null,
											plotBorderWidth: null,
											plotShadow: false,
											type: 'pie'
										},
										credits: {
											enabled: false
										},
									    title: {
									        text: null
									    },
										tooltip: {
											pointFormat: '{series.name}: <b>{point.percentage:.1f}% ({series.y})</b>'
										},
										plotOptions: {
											pie: {
												allowPointSelect: true,
												cursor: 'pointer',
												dataLabels: {
													enabled: true,
													format: '<b>{point.name}</b>: {point.percentage:.1f} %',
													style: {
														color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
													}
												}
											}
										},
										series: [{
											name: 'Jawaban',
											colorByPoint: true,
											data: [
												@foreach($data as $label => $point)
												{
													name: '{{ $label }} Bintang',
													y: {{ count($point) }}
												},
												@endforeach
											]
										}]
									});
								</script>
							@endif
						@endif
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<script>
	$(document).ready(function($) {
		$('.ui.checkbox').checkbox();
		$('[data-content]').popup({
			hoverable: true,
			position : 'top center',
			delay: {
				show: 300,
				hide: 800
			}
		});
		
		$(document).on('click', '.remove-sub', function(event) {
			$(this).closest('tr').remove();
		});

		$('.add-sub').click(function(event) {
			var idx = parseInt($('.sub.container tr').last().data('count')) + 1;
			var html = `<tr data-count="`+idx+`">
			<td>
			<div class="ui small fluid input">
			<input name="detail[`+idx+`][pertanyaan]" placeholder="Pertanyaan" type="text" required>
			</div>
			</td>
			<td class="center aligned">
			<div class="inline fields" style="margin:0">
			<div class="field">
			<div class="ui radio checkbox">
			<input name="detail[`+idx+`][tipe]" checked="checked" type="radio">
			<label>Rating</label>
			</div>
			</div>
			<div class="field">
			<div class="ui radio checkbox">
			<input name="detail[`+idx+`][tipe]" type="radio">
			<label>Pernyataan</label>
			</div>
			</div>
			</div>
			</td>
			<td class="center aligned">
			<button type="button" class="ui red icon button remove-sub" data-content="Hapus Pertanyaan">
			<i class="delete icon"></i>
			</button>
			</td>
			</tr>`;

			$('.sub.container').append(html);

			$('.ui.checkbox').checkbox();
			$('[data-content]').popup({
				hoverable: true,
				position : 'top center',
				delay: {
					show: 300,
					hide: 800
				}
			});
		});
	});	
</script>
@append