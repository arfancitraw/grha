@extends('layouts.grid')

@section('filters')
<div class="field">
	<input name="filter[nama]" placeholder="Nama" type="text">
</div>
<div class="field">
	<select name="filter[spesialisasi]" class="ui search dropdown">
		{!! \App\Models\Master\Spesialisasi::options('nama') !!}
	</select>
</div>
<div class="field">
	<select name="filter[jabatan]" class="ui search dropdown">
		{!! \App\Models\Master\JabatanDokter::options('jabatan') !!}
	</select>
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
d.spesialisasi = $("select[name='filter[spesialisasi]']").val();
d.jabatan = $("select[name='filter[jabatan]']").val();
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).on('click', '.edit-tab.button', function(e){
		var id = $(this).data('id');
		var url = '{{ url($pageUrl) }}/'+id+'/edit';
		window.location = url;
	});
	$(document).on('click', '.sync.dokter', function(e){
		swal({
			title: 'Sinkronisasi Data Dokter?',
			// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sinkronisasi',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$('.whole.dimmer').addClass('active');
				$.ajax({
					url: '{{ url($pageUrl.'sync') }}',
					type: 'POST',
					dataType: 'json',
					data: {_token:"{{csrf_token()}}"}
				})
				.done(function() {
					swal(
						'Sukses!',
						'Data berhasil disinkronisasi.',
						'success'
					).then((result) => {
						dt.draw();
						return true;
					});
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					$('.whole.dimmer').removeClass('active');
				});
				
			}
		})
	});
</script>
@endsection

@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-sync'))
<button type="button" class="ui green sync dokter button" data-url="{{ url('backend/dokter/list-dokter/sync') }}">
	<i class="refresh icon"></i> Sinkronisasi Data
</button>
@endif
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
<a class="ui blue button" href="{{ url('backend/dokter/list-dokter/create') }}">
	<i class="plus icon"></i> Tambah Data
</a>
@endif
@endsection