<div class="field">
	<div class="pendidikan container">
		@if($record->pendidikan->count() > 0)
		@php $i = 0; @endphp
		@foreach ($record->pendidikan as $pendidikan)
		<div class="fields" data-count="{{$i}}">
			<div class="fourteen wide field">
				<input type="text" name="pendidikan[{{$i}}][nama]" placeholder="Pendidikan" value="{{ $pendidikan->nama or '' }}">
			</div>
			<div class="two wide field">
				@if($i == 0)
				<button type="button" class="ui positive fluid icon button add-pendidikan" data-content="Tambah Pendidikan"><i class="plus icon"></i></button>
				@else
				<button type="button" class="ui negative fluid icon button remove-pendidikan" data-content="Hapus Pendidikan"><i class="remove icon"></i></button>
				@endif
			</div>
		</div>
		@php $i++; @endphp
		@endforeach
		@else
		<div class="field">
			<div class="pendidikan container">
				<div class="fields" data-count="0">
					<div class="fourteen wide field">
						<input type="text" name="pendidikan[0][nama]" placeholder="Pendidikan">
					</div>
					<div class="two wide field">
						<button type="button" class="ui positive fluid icon button add-pendidikan" data-content="Tambah Pendidikan"><i class="plus icon"></i></button>
					</div>
				</div>
			</div>
		</div>
		@endif
	</div>
</div>

@section('scripts')
<script type="text/javascript">
	$(document).on('click', '.remove-pendidikan', function(event) {
		$(this).closest('.fields').remove();
		calcTotal();
	});

	$('.add-pendidikan').click(function(event) {
		var idx = parseInt($('.pendidikan.container .fields').last().data('count')) + 1;
		var html = `<div class="fields" data-count="`+idx+`">
		<div class="fourteen wide field">
		<input type="text" name="pendidikan[`+idx+`][nama]" placeholder="Pendidikan">
		</div>
		<div class="two wide field">
		<button type="button" class="ui negative fluid icon button remove-pendidikan" data-content="Hapus Pendidikan"><i class="remove icon"></i></button>
		</div>
		</div>`;

		$('.pendidikan.container').append(html);
	});
</script>
@append