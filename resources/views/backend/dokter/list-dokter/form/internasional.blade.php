@php
	if(isset($pendidikan)){
		$gelar = $pendidikan->where('tipe', 0);
		$keanggotaan = $pendidikan->where('tipe', 1);
		$pelatihan = $pendidikan->where('tipe', 2);
	}
@endphp
<h4 class="ui dividing header">Pelatihan dan Internasional</h4>
<div class="field">
	<div class="gelar container">
	@if(!isset($gelar) || count($gelar) == 0)
		<div class="fields" data-count="0">
			<div class="thirteen wide field">
				<input type="text" name="gelar[0][nama]" placeholder="Pendidikan">
			</div>
			<div class="three wide field">
				<button type="button" class="ui positive fluid icon button add-gelar" data-content="Tambah Pendidikan"><i class="plus icon"></i></button>
			</div>
		</div>
	@else
	@php $i = 0; @endphp
		@foreach ($gelar as $sub)
			<div class="fields" data-count="{{$i}}">
				<div class="thirteen wide field">
					<input type="hidden" name="pendidikan[]" value="{{ $sub->id }}">
					<input type="hidden" name="gelar[{{$i}}][id]" value="{{ $sub->id }}">
					<input type="text" name="gelar[{{$i}}][nama]" placeholder="Pendidikan" value="{{ $sub->nama }}">
				</div>
				<div class="three wide field">
					@if($i == 0)
					<button type="button" class="ui positive fluid icon button add-gelar" data-content="Tambah Pendidikan"><i class="plus icon"></i></button>
					@else
					<button type="button" class="ui negative fluid icon button remove-gelar" data-content="Hapus Pendidikan"><i class="remove icon"></i></button>
					@endif
				</div>
			</div>
			@php $i++; @endphp
		@endforeach
	@endif
	</div>
</div>
<h4 class="ui dividing header">Pelatihan dan Fellowship</h4>
<div class="field">
	<div class="pelatihan container">
	@if(!isset($pelatihan) || count($pelatihan) == 0)
		<div class="fields" data-count="0">
			<div class="thirteen wide field">
				<input type="text" name="pelatihan[0][nama]" placeholder="Pelatihan dan Fellowship">
			</div>
			<div class="three wide field">
				<button type="button" class="ui positive fluid icon button add-pelatihan" data-content="Tambah Pelatihan dan Fellowship"><i class="plus icon"></i></button>
			</div>
		</div>
	@else
	@php $i = 0; @endphp
		@foreach ($pelatihan as $sub)
			<div class="fields" data-count="{{$i}}">
				<div class="thirteen wide field">
					<input type="hidden" name="pendidikan[]" value="{{ $sub->id }}">
					<input type="hidden" name="pelatihan[{{$i}}][id]" value="{{ $sub->id }}">
					<input type="text" name="pelatihan[{{$i}}][nama]" placeholder="Pelatihan dan Fellowship" value="{{ $sub->nama }}">
				</div>
				<div class="three wide field">
					@if($i == 0)
					<button type="button" class="ui positive fluid icon button add-pelatihan" data-content="Tambah Pelatihan dan Fellowship"><i class="plus icon"></i></button>
					@else
					<button type="button" class="ui negative fluid icon button remove-pelatihan" data-content="Hapus Pelatihan dan Fellowship"><i class="remove icon"></i></button>
					@endif
				</div>
			</div>
			@php $i++; @endphp
		@endforeach
	@endif
	</div>
</div>
<h4 class="ui dividing header">Keanggotaan Profesi</h4>
<div class="field">
	<div class="keanggotaan container">
	@if(!isset($keanggotaan) || count($keanggotaan) == 0)
		<div class="fields" data-count="0">
			<div class="thirteen wide field">
				<input type="text" name="keanggotaan[0][nama]" placeholder="Keanggotaan Profesi">
			</div>
			<div class="three wide field">
				<button type="button" class="ui positive fluid icon button add-keanggotaan" data-content="Tambah Keanggotaan Profesi"><i class="plus icon"></i></button>
			</div>
		</div>
	@else
	@php $i = 0; @endphp
		@foreach ($keanggotaan as $sub)
			<div class="fields" data-count="{{$i}}">
				<div class="thirteen wide field">
					<input type="hidden" name="pendidikan[]" value="{{ $sub->id }}">
					<input type="hidden" name="keanggotaan[{{$i}}][id]" value="{{ $sub->id }}">
					<input type="text" name="keanggotaan[{{$i}}][nama]" placeholder="Keanggotaan Profesi" value="{{ $sub->nama }}">
				</div>
				<div class="three wide field">
					@if($i == 0)
					<button type="button" class="ui positive fluid icon button add-keanggotaan" data-content="Tambah Keanggotaan Profesi"><i class="plus icon"></i></button>
					@else
					<button type="button" class="ui negative fluid icon button remove-keanggotaan" data-content="Hapus Keanggotaan Profesi"><i class="remove icon"></i></button>
					@endif
				</div>
			</div>
			@php $i++; @endphp
		@endforeach
	@endif
	</div>
</div>


@section('scripts')
<script type="text/javascript">
	$(document).on('click', '.remove-gelar', function(event) {
		$(this).closest('.fields').remove();
		calcTotal();
	});

	$('.add-gelar').click(function(event) {
		var idx = parseInt($('.gelar.container .fields').last().data('count')) + 1;
		var html = `<div class="fields" data-count="`+idx+`">
		<div class="thirteen wide field">
		<input type="text" name="gelar[`+idx+`][nama]" placeholder="Pendidikan">
		</div>
		<div class="three wide field">
		<button type="button" class="ui negative fluid icon button remove-gelar" data-content="Hapus Pendidikan"><i class="remove icon"></i></button>
		</div>
		</div>`;

		$('.gelar.container').append(html);
	});

	$(document).on('click', '.remove-pelatihan', function(event) {
		$(this).closest('.fields').remove();
		calcTotal();
	});

	$('.add-pelatihan').click(function(event) {
		var idx = parseInt($('.pelatihan.container .fields').last().data('count')) + 1;
		var html = `<div class="fields" data-count="`+idx+`">
		<div class="thirteen wide field">
		<input type="text" name="pelatihan[`+idx+`][nama]" placeholder="Pelatihan dan Fellowship">
		</div>
		<div class="three wide field">
		<button type="button" class="ui negative fluid icon button remove-pelatihan" data-content="Hapus Pelatihan dan Fellowship"><i class="remove icon"></i></button>
		</div>
		</div>`;

		$('.pelatihan.container').append(html);
	});

	$(document).on('click', '.remove-keanggotaan', function(event) {
		$(this).closest('.fields').remove();
		calcTotal();
	});

	$('.add-keanggotaan').click(function(event) {
		var idx = parseInt($('.keanggotaan.container .fields').last().data('count')) + 1;
		var html = `<div class="fields" data-count="`+idx+`">
		<div class="thirteen wide field">
		<input type="text" name="keanggotaan[`+idx+`][nama]" placeholder="Keanggotaan Profesi">
		</div>
		<div class="three wide field">
		<button type="button" class="ui negative fluid icon button remove-keanggotaan" data-content="Hapus Keanggotaan Profesi"><i class="remove icon"></i></button>
		</div>
		</div>`;

		$('.keanggotaan.container').append(html);
	});
</script>
@append