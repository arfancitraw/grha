@php
	if(isset($keahlian)){
		$spesialisasi = $keahlian->where('tipe', 0);
		$prosedur = $keahlian->where('tipe', 1);
	}
@endphp
<h4 class="ui dividing header">Spesialisasi</h4>
<div class="field">
	<div class="spesialisasi container">
	@if(!isset($spesialisasi) || count($spesialisasi) == 0)
		<div class="fields" data-count="0">
			<div class="thirteen wide field">
				<input type="text" name="spesialisasi[0][nama]" placeholder="Spesialisasi">
			</div>
			<div class="three wide field">
				<button type="button" class="ui positive fluid icon button add-spesialisasi" data-content="Tambah Spesialisasi"><i class="plus icon"></i></button>
			</div>
		</div>
	@else
	@php $i = 0; @endphp
		@foreach ($spesialisasi as $sub)
			<div class="fields" data-count="{{$i}}">
				<div class="thirteen wide field">
					<input type="hidden" name="keahlian[]" value="{{ $sub->id }}">
					<input type="hidden" name="spesialisasi[{{$i}}][id]" value="{{ $sub->id }}">
					<input type="text" name="spesialisasi[{{$i}}][nama]" placeholder="Spesialisasi" value="{{ $sub->nama }}">
				</div>
				<div class="three wide field">
					@if($i == 0)
					<button type="button" class="ui positive fluid icon button add-spesialisasi" data-content="Tambah Spesialisasi"><i class="plus icon"></i></button>
					@else
					<button type="button" class="ui negative fluid icon button remove-spesialisasi" data-content="Hapus Spesialisasi"><i class="remove icon"></i></button>
					@endif
				</div>
			</div>
			@php $i++; @endphp
		@endforeach
	@endif
	</div>
</div>
<h4 class="ui dividing header">Keahlian Khusus</h4>
<div class="field">
	<div class="prosedur container">
	@if(!isset($prosedur) || count($prosedur) == 0)
		<div class="fields" data-count="0">
			<div class="thirteen wide field">
				<input type="text" name="prosedur[0][nama]" placeholder="Keahlian Khusus">
			</div>
			<div class="three wide field">
				<button type="button" class="ui positive fluid icon button add-prosedur" data-content="Tambah Keahlian Khusus"><i class="plus icon"></i></button>
			</div>
		</div>
	@else
	@php $i = 0; @endphp
		@foreach ($prosedur as $sub)
			<div class="fields" data-count="{{$i}}">
				<div class="thirteen wide field">
					<input type="hidden" name="keahlian[]" value="{{ $sub->id }}">
					<input type="hidden" name="prosedur[{{$i}}][id]" value="{{ $sub->id }}">
					<input type="text" name="prosedur[{{$i}}][nama]" placeholder="Keahlian Khusus" value="{{ $sub->nama }}">
				</div>
				<div class="three wide field">
					@if($i == 0)
					<button type="button" class="ui positive fluid icon button add-prosedur" data-content="Tambah Keahlian Khusus"><i class="plus icon"></i></button>
					@else
					<button type="button" class="ui negative fluid icon button remove-prosedur" data-content="Hapus Keahlian Khusus"><i class="remove icon"></i></button>
					@endif
				</div>
			</div>
			@php $i++; @endphp
		@endforeach
	@endif
	</div>
</div>

@section('scripts')
<script type="text/javascript">
	$(document).on('click', '.remove-spesialisasi', function(event) {
		$(this).closest('.fields').remove();
		calcTotal();
	});

	$('.add-spesialisasi').click(function(event) {
		var idx = parseInt($('.spesialisasi.container .fields').last().data('count')) + 1;
		var html = `<div class="fields" data-count="`+idx+`">
		<div class="thirteen wide field">
		<input type="text" name="spesialisasi[`+idx+`][nama]" placeholder="Spesialisasi">
		</div>
		<div class="three wide field">
		<button type="button" class="ui negative fluid icon button remove-spesialisasi" data-content="Hapus Spesialisasi"><i class="remove icon"></i></button>
		</div>
		</div>`;

		$('.spesialisasi.container').append(html);
	});

	$(document).on('click', '.remove-prosedur', function(event) {
		$(this).closest('.fields').remove();
		calcTotal();
	});

	$('.add-prosedur').click(function(event) {
		var idx = parseInt($('.prosedur.container .fields').last().data('count')) + 1;
		var html = `<div class="fields" data-count="`+idx+`">
		<div class="thirteen wide field">
		<input type="text" name="prosedur[`+idx+`][nama]" placeholder="Keahlian Khusus">
		</div>
		<div class="three wide field">
		<button type="button" class="ui negative fluid icon button remove-prosedur" data-content="Hapus Keahlian Khusus"><i class="remove icon"></i></button>
		</div>
		</div>`;

		$('.prosedur.container').append(html);
	});
</script>
@append