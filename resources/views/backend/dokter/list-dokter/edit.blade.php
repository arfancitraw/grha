@extends('layouts.grid')

@section('scripts')
<script type="text/javascript">

	function readURL(input) {
		if(input.files && input.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#showAttachment').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).on('change', '#attachment', function () {
		readURL(this);
	});

	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '200px';
		tinymce.init(tinymceOpt);

		$('.menu .item').tab();	
	});

</script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	<div class="ui horizontal segments">
		<div class="ui segment" style="width: 30%">
			<h4 class="ui dividing header">Detail Dokter</h4>
			<div class="ui grid">
				<div class="six wide column image-container">
					<img class="image-preview"  style="height: 230px;object-fit: cover; object-position: top;" 
						 id="showAttachment" src="{{ ($record->photo) ? url('storage/'.$record->photo) : asset('img/default.jpg') }}">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Unggah Foto Dokter">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" multiple="">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				<div class="ten wide column">
					<div class="field">
						<label>Nama Dokter</label>
						<input type="text" name="nama" placeholder="Nama Dokter" value="{{ $record->nama or '' }}">
					</div>
					<div class="field">
						<label>Nama Lengkap Dokter</label>
						<input type="text" name="nama_lengkap" placeholder="Nama Lengkap Dokter (dengan gelar)" value="{{ $record->nama_lengkap or '' }}">
					</div>
					<div class="field">
						<label>Spesialisasi</label>
						<select name="spesialisasi_id" class="ui search dropdown">
							{{-- <option value="">Pilih Spesialisasi</option> --}}
							{!! \App\Models\Master\Spesialisasi::options('nama', 'id', ['selected' => $record->spesialisasi_id], 'Pilih Spesialisasi Dokter') !!}
						</select>
					</div>
					<div class="field">
						<label>Jabatan</label>
						<select name="jabatan_dokter_id" class="ui search dropdown">
							{{-- <option value="">Pilih Spesialisasi</option> --}}
							{!! \App\Models\Master\JabatanDokter::options('jabatan', 'id', ['selected' => $record->jabatan_dokter_id], 'Pilih Jabatan Dokter') !!}
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="ui segment">
			@include('backend.dokter.list-dokter.form.keahlian', ['keahlian' => $record->keahlian])
		</div>
		<div class="ui segment">
			@include('backend.dokter.list-dokter.form.pendidikan', ['pendidikan' => $record->pendidikan])
		</div>
	</div>
	<div class="ui segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<button class="ui right labeled blue icon save page button">
					<i class="save icon"></i>
					Simpan
				</button>
			</div>
		</div>
	</div>
</form>
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nama: 'empty',
		spesialisasi_id: 'empty',
		jabatan_dokter_id: 'empty',
		alamat: 'empty',
		no_tlpn: 'empty',
		spesialisasi_id: 'empty',
		jabatan_dokter_id: 'empty',
	};
</script>
@endsection