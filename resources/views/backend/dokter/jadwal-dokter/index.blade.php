@extends('layouts.grid')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/daterangepicker/moment.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
<div class="field">
	<select name="filter[dokter]" class="ui dropdown search">
		{!! \App\Models\Dokter\ListDokter::options('nama') !!}
	</select>
</div>
<div class="field">
	<select name="filter[hari_praktek]" class="ui dropdown search">
		<option value="">  (Pilih Hari)  </option>
		<option value="1">Senin</option>
		<option value="2">Selasa</option>
		<option value="3">Rabu</option>
		<option value="4">Kamis</option>
		<option value="5">Jumat</option>
		<option value="6">Sabtu</option>
		<option value="7">Minggu</option>
	</select>
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
d.dokter = $("select[name='filter[dokter]']").val();
d.hari_praktek = $("select[name='filter[hari_praktek]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		list_dokter_id: 'empty',
		hari_praktek: 'empty',
		jam_mulai_praktek: 'empty',
		jam_selesai_praktek: 'empty',
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
		$('.ui.clock.cawal').calendar({
			type: 'time',
			ampm: false, 
			disableMinute: true,

		});

		$('.ui.clock.cakhir').calendar({
			type: 'time',
			ampm: false, 
			disableMinute: true,
		});
	};
</script>
@endsection