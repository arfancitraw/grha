<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Jadwal Dokter</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="nama">Nama Dokter</label>
					<select name="list_dokter_id" class="ui dropdown search">
						{!! \App\Models\Dokter\ListDokter::options('nama','id',['selected'=> $record->list_dokter_id]) !!}
					</select>
				</div>
				<div class="field">
					<label for="">Hari Praktek</label>
					<select class="ui selection dropdown" name="hari_praktek">
						<option>  (Pilih Hari)  </option>
						<option value="1" {{ $record->hari_praktek==1 ? 'selected' : '' }}>Senin</option>
						<option value="2" {{ $record->hari_praktek==2 ? 'selected' : '' }}>Selasa</option>
						<option value="3" {{ $record->hari_praktek==3 ? 'selected' : '' }}>Rabu</option>
						<option value="4" {{ $record->hari_praktek==4 ? 'selected' : '' }}>Kamis</option>
						<option value="5" {{ $record->hari_praktek==5 ? 'selected' : '' }}>Jumat</option>
						<option value="6" {{ $record->hari_praktek==6 ? 'selected' : '' }}>Sabtu</option>
						<option value="7" {{ $record->hari_praktek==7 ? 'selected' : '' }}>Minggu</option>
					</select>
				</div>
				<div class="two fields">
					<div class="field">
						<label>Jam Mulai</label>
						<div class="ui clock cawal">
							<div class="ui input left icon">
								<i class="clock icon date"></i>
								<input name="jam_mulai_praktek" type="text" placeholder="Jam Mulai" value="{{ $record->jam_mulai_praktek or '' }}">
							</div>
						</div>
					</div>
					<div class="field">
						<label>Jam Selesai</label>
						<div class="ui clock cakhir">
							<div class="ui input left icon">
								<i class="clock icon date"></i>
								<input name="jam_selesai_praktek" type="text" placeholder="Jam Selesai" value="{{ $record->jam_selesai_praktek or '' }}">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>