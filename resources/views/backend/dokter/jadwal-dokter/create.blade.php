<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Jadwal Dokter</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="nama">Nama Dokter</label>
					<select name="list_dokter_id" class="ui dropdown search">
						{!! \App\Models\Dokter\ListDokter::options('nama') !!}
					</select>
				</div>
				<div class="field">
					<label for="">Hari Praktek</label>
					<select class="ui selection dropdown" name="hari_praktek">
						<option>  (Pilih Hari)  </option>
						<option value="1">Senin</option>
						<option value="2">Selasa</option>
						<option value="3">Rabu</option>
						<option value="4">Kamis</option>
						<option value="5">Jumat</option>
						<option value="6">Sabtu</option>
						<option value="7">Minggu</option>
					</select>
				</div>
				<div class="two fields">
					<div class="field">
						<label>Jam Mulai</label>
						<div class="ui clock cawal">
							<div class="ui input left icon">
								<i class="clock icon date"></i>
								<input name="jam_mulai_praktek" type="text" placeholder="Jam Mulai" value="{{ old('jam_mulai_praktek') }}">
							</div>
						</div>
					</div>
					<div class="field">
						<label>Jam Selesai</label>
						<div class="ui clock cakhir">
							<div class="ui input left icon">
								<i class="clock icon date"></i>
								<input name="jam_selesai_praktek" type="text" placeholder="Jam Selesai" value="{{ old('jam_selesai_praktek') }}">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>