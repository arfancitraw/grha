@extends('layouts.grid')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
	<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('filters')
	{{-- <div class="field">
		<input name="filter[kode]" placeholder="Kode" type="text">
	</div> --}}
	<div class="field">
		<input name="filter[nama]" placeholder="Nama" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('js-filters')
    {{-- d.kode = $("input[name='filter[kode]']").val(); --}}
    d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		deskripsi: 'empty',
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
      $('.editor').summernote({
		  height: '200px'
	  });
	};

	$(document).on('click', '.sync.poli', function(e){
		swal({
			title: 'Sinkronisasi Data Poli?',
			// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sinkronisasi',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$('.whole.dimmer').addClass('active');
				$.ajax({
					url: '{{ url($pageUrl.'sync') }}',
					type: 'POST',
					dataType: 'json',
					data: {_token:"{{csrf_token()}}"}
				})
				.done(function() {
					swal(
						'Sukses!',
						'Data berhasil disinkronisasi.',
						'success'
					).then((result) => {
						dt.draw();
						return true;
					});
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					$('.whole.dimmer').removeClass('active');
				});
				
			}
		})
	});
</script>
@endsection

@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-sync'))
<button type="button" class="ui green sync poli button">
	<i class="refresh icon"></i> Sinkronisasi Data Dengan Server
</button>
@endif
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
<button type="button" class="ui blue add button">
    <i class="plus icon"></i>
    Tambah Data
</button>
@endif
@endsection