<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Poli</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="">Kode</label>
					<input type="text" name="kode">
				</div>
				<div class="field">
					<label for="">Nama</label>
					<input type="text" name="nama">
				</div>
				<div class="field">
					<label for="">Deskripsi</label>
					<textarea name="deskripsi" id="" rows="3"></textarea>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>