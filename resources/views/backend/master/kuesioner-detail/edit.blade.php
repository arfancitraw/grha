@extends('layouts.grid')
@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">	
	<div class="ui attached top segment">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="nama">Kuesioner</label>
					<select name="kuesioner_id" class="ui search dropdown">
						{{-- <option value="">Pilih Kuesioner</option> --}}
						{!! \App\Models\Master\KuesionerPeriode::options('periode','id',['selected'=> $record->kuesioner_id]) !!}
					</select>
				</div>
				<div class="field">
					<label for="">Pertanyaan</label>
					<textarea name="pertanyaan" rows="5">{{$record->pertanyaan}}</textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection