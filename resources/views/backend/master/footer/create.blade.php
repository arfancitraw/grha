<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Tautan Footer</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="field">
			<label for="nama">Nama</label>
			<input type="text" name="nama" placeholder="Nama">
		</div>
		<div class="field">
			<label for="url">Url</label>
			<input type="text" name="url" placeholder="Url">
		</div>
		<div class="field">
			<label for="deskripsi">Deskripsi</label>
			<textarea name="deskripsi" rows="4" style="height: 100px; resize:none;"></textarea>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>