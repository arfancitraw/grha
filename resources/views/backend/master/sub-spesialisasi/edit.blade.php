<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Spesialisasi</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="nama">Nama</label>
					<input type="text" name="nama" value="{{$record->nama}}">
				</div>
				<div class="field">
					<label for="nama">Spesialisasi</label>
					<select name="spesialisasi_induk" class="ui search dropdown">
						{{-- <option value="">Pilih Spesialisasi</option> --}}
						{!! \App\Models\Master\Spesialisasi::options('nama','id',['selected'=> $record->spesialisasi_induk]) !!}
					</select>
				</div>
				<div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" id="" rows="3">{{$record->keterangan}}</textarea>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Update
		<i class="checkmark icon"></i>
	</div>
</div>