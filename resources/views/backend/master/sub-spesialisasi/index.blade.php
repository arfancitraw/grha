@extends('layouts.grid')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('filters')
<div class="field">
	<input name="filter[nama]" placeholder="Nama Sub Spesialisasi" type="text">
</div>
<div class="field">
	<select name="filter[spesialisasi_induk]" class="ui search dropdown">
		{!! \App\Models\Master\Spesialisasi::options('nama') !!}
	</select>
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
d.spesialisasi_induk = $("select[name='filter[spesialisasi_induk]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nama: 'empty',
		spesialisasi_induk: 'empty',
		keterangan: 'empty',
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
		$('.editor').summernote({
			height: '200px'
		});
	};
</script>
@endsection