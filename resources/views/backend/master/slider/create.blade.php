<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Home Slider</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="status" id="status">
		<input type="hidden" name="posisi" value="0">

		<div class="field image-container">
			<span class="image-preview" style="height: 15rem;">
				<img class="image-preview" id="showAttachment" style="height: 15rem" src="{{ asset('img/default.jpg') }}">
			</span>
			<div class="ui fluid file input action">
				<input type="text" readonly="">
				<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" accept="image/*">
				<div class="ui blue button file">
					Cari...
				</div>
			</div>
		</div>
		<div class="fields">
			<div class="twelve wide field">
				<label for="nama">Judul</label>
				<input type="text" name="judul" placeholder="Judul" value="">
			</div>
			<div class="two wide field">
				<label for="posisi">Urutan</label>
				<input type="number" name="posisi" placeholder="Urutan Slider" value="{{ \App\Models\Master\Slider::all()->count() + 1 }}">
			</div>
			<div class="two wide field" style="text-align: center">
				<label for="android">Android</label>
				<div class="ui fitted checkbox" style="margin-top: 8px;">
					<input type="hidden" name="android" value="0">
					<input type="checkbox" name="android" placeholder="Android" checked="">
					<label></label>
				</div>
			</div>
		</div>

		<div class="ui grid">
			<div class="seven wide column">
				<div class="field">
					<label for="nama">Subjudul</label>
					<input type="text" name="sub_judul" placeholder="Subjudul" value="">
				</div>
				<div class="field">
					<label for="nama">Link</label>
					<input type="text" name="url" placeholder="Link" value="">
				</div>
			</div>
			<div class="nine wide column">
				<div class="field">
					<label for="">Deskripsi</label>
					<textarea name="deskripsi" style="height: 97px; resize: none;"></textarea>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui buttons">
		<button type="button" class="ui button save as drafting orange">Draft</button>
		<div class="or"></div>
		<button type="button" class="ui button save as publicity teal">Publish</button>
	</div>
</div>