<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Home Slider</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<input type="hidden" name="status" id="status" value="{{ $record->status }}">
		<input type="hidden" name="posisi" value="0">

		<div class="field image-container">
			<span class="image-preview" style="height: 15rem;border:none">
				@if($record->photo)
					<img class="image-preview" id="showAttachment" style="height: 15rem;border:none" src="{{ asset('storage') }}/{{ $record->photo }}">
				@else
					<img class="image-preview" id="showAttachment" style="height: 15rem" src="{{ asset('img/default.jpg') }}">
				@endif
			</span>
			<div class="ui fluid file input action">
				<input type="text" readonly="" style="border-top-left-radius: 0">
				<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" accept="image/*">
				<div class="ui blue button file" style="border-top-right-radius: 0">
					Cari...
				</div>
			</div>
		</div>
		<div class="fields">
			<div class="twelve wide field">
				<label for="nama">Judul</label>
				<input type="text" name="judul" placeholder="Judul" value="{{ $record->judul or '' }}">
			</div>
			<div class="two wide field">
				<label for="posisi">Urutan</label>
				<input type="number" name="posisi" placeholder="Urutan Slider" value="{{ $record->posisi or '' }}">
			</div>
			<div class="two wide field" style="text-align: center">
				<label for="android">Android</label>
				<div class="ui fitted checkbox" style="margin-top: 8px;">
					<input type="hidden" name="android" value="0">
					<input type="checkbox" name="android" placeholder="Android" @if($record->android) checked="" @endif>
					<label></label>
				</div>
			</div>
		</div>

		<div class="ui grid">
			<div class="seven wide column">
				<div class="field">
					<label for="nama">Subjudul</label>
					<input type="text" name="sub_judul" placeholder="Subjudul" value="{{ $record->sub_judul or '' }}">
				</div>
				<div class="field">
					<label for="nama">Link</label>
					<input type="text" name="url" placeholder="Link" value="{{ $record->url or '' }}">
				</div>
			</div>
			<div class="nine wide column">
				<div class="field">
					<label for="">Deskripsi</label>
					<textarea name="deskripsi" style="height: 97px; resize: none;">{{ $record->deskripsi or '' }}</textarea>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui buttons">
		<button type="button" class="ui button save as drafting orange">Draft</button>
		<div class="or"></div>
		<button type="button" class="ui button save as publicity teal">Publish</button>
	</div>
</div>