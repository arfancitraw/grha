<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Tipe Pendaftar</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
	    <input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		<div class="field">
			<label for="tipe_pendaftar">Tipe Pendaftar</label>
			<input type="text" name="tipe_pendaftar" placeholder="Tipe Pendaftar" value="{{ $record->tipe_pendaftar or '' }}">
		</div>
		<div class="field">
			<label for="deskripsi">Deskripsi</label>
			<textarea name="deskripsi" rows="4" style="height: 100px; resize:none;">{!! $record->deskripsi or '' !!}</textarea>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui negative button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>