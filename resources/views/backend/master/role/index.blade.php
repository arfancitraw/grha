@extends('layouts.grid')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
	<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('filters')
	<div class="field">
		<input name="filter[jabatan]" placeholder="Nama Jabatan" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('js-filters')
    d.jabatan = $("input[name='filter[jabatan]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		jabatan: 'empty',
		deskripsi: 'empty',
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
      $('.editor').summernote({
		  height: '200px'
	  });
	};
</script>
@endsection