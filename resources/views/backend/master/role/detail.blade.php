@extends('layouts.forms')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.'change-permission/') }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<input type="hidden" name="id" value="{{ $record->id }}">

	<div class="ui attached top segment">
		<div class="ui grid">
      <div class="sixteen wide column">
        <table class="ui compact celled table">
          <thead>
            <tr>
              <th width="70%" colspan="3">Menu</th>
              <th class="center aligned" width="5%">View</th>
              <th class="center aligned" width="5%">Create</th>
              <th class="center aligned" width="5%">Edit</th>
              <th class="center aligned" width="5%">Delete</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="6"><h3>1. Media Center</h3></td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>1.1.</h4></td>
              <td colspan="2"><h4>Artikel</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['artikel']['view'] }}" {{ $record->getChecked('backend/media-center/artikel') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['artikel']['create'] }}" {{ $record->getChecked('backend/media-center/artikel/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['artikel']['edit'] }}" {{ $record->getChecked('backend/media-center/artikel/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['artikel']['delete'] }}" {{ $record->getChecked('backend/media-center/artikel/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td class="right aligned"><h4>1.2.</h4></td>
              <td colspan="2"><h4>Berita</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pengumuman']['view'] }}" {{ $record->getChecked('backend/media-center/pengumuman') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pengumuman']['create'] }}" {{ $record->getChecked('backend/media-center/pengumuman/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pengumuman']['edit'] }}" {{ $record->getChecked('backend/media-center/pengumuman/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pengumuman']['delete'] }}" {{ $record->getChecked('backend/media-center/pengumuman/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td class="right aligned"><h4>1.3.</h4></td>
              <td colspan="6"><h4>Pustaka</h4></td>
						</tr>
						<tr>
							<td></td>
              <td width="15%" class="right aligned"><h4>1.1.1.</h4></td>
              <td><h4>Penyakit</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pustaka']['penyakit']['view'] }}" {{ $record->getChecked('backend/media-center/pustaka/penyakit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pustaka']['penyakit']['create'] }}" {{ $record->getChecked('backend/media-center/pustaka/penyakit/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pustaka']['penyakit']['edit'] }}" {{ $record->getChecked('backend/media-center/pustaka/penyakit/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pustaka']['penyakit']['delete'] }}" {{ $record->getChecked('backend/media-center/pustaka/penyakit/delete') }}> <label></label>
                </div>
              </td>
            </tr>
						<tr>
							<td></td>
              <td width="15%" class="right aligned"><h4>1.1.2.</h4></td>
              <td><h4>Tindakan</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pustaka']['tindakan']['view'] }}" {{ $record->getChecked('backend/media-center/pustaka/tindakan') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pustaka']['tindakan']['create'] }}" {{ $record->getChecked('backend/media-center/pustaka/tindakan/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pustaka']['tindakan']['edit'] }}" {{ $record->getChecked('backend/media-center/pustaka/tindakan/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['media-center']['pustaka']['tindakan']['delete'] }}" {{ $record->getChecked('backend/media-center/pustaka/tindakan/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="3"><h3>2. Profil</h3></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['profil']['view'] }}" {{ $record->getChecked('backend/profil') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['profil']['create'] }}" {{ $record->getChecked('backend/profil/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['profil']['edit'] }}" {{ $record->getChecked('backend/profil/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['profil']['delete'] }}" {{ $record->getChecked('backend/profil/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="3"><h3>3. Pelayanan</h3></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['layanan']['view'] }}" {{ $record->getChecked('backend/layanan') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['layanan']['create'] }}" {{ $record->getChecked('backend/layanan/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['layanan']['edit'] }}" {{ $record->getChecked('backend/layanan/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['layanan']['delete'] }}" {{ $record->getChecked('backend/layanan/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="3"><h3>4. Dokter</h3></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['dokter']['list-dokter']['view'] }}" {{ $record->getChecked('backend/dokter/list-dokter') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['dokter']['list-dokter']['create'] }}" {{ $record->getChecked('backend/dokter/list-dokter/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['dokter']['list-dokter']['edit'] }}" {{ $record->getChecked('backend/dokter/list-dokter/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['dokter']['list-dokter']['delete'] }}" {{ $record->getChecked('backend/dokter/list-dokter/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="3"><h3>5. Rujukan Nasional</h3></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['rujukan-nasional']['view'] }}" {{ $record->getChecked('backend/rujukan-nasional') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['rujukan-nasional']['create'] }}" {{ $record->getChecked('backend/rujukan-nasional/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['rujukan-nasional']['edit'] }}" {{ $record->getChecked('backend/rujukan-nasional/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['rujukan-nasional']['delete'] }}" {{ $record->getChecked('backend/rujukan-nasional/delete') }}> <label></label>
                </div>
              </td>
            </tr>
						<tr>
              <td colspan="6"><h3>6. Diklat</h3></td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>6.1.</h4></td>
              <td colspan="2"><h4>Info Diklat</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['diklat']['info-diklat']['view'] }}" {{ $record->getChecked('backend/diklat/info-diklat') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['diklat']['info-diklat']['create'] }}" {{ $record->getChecked('backend/diklat/info-diklat/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['diklat']['info-diklat']['edit'] }}" {{ $record->getChecked('backend/diklat/info-diklat/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['diklat']['info-diklat']['delete'] }}" {{ $record->getChecked('backend/diklat/info-diklat/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>6.2.</h4></td>
              <td colspan="2"><h4>Jadwal Diklat</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['diklat']['jadwal-diklat']['view'] }}" {{ $record->getChecked('backend/diklat/jadwal-diklat') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['diklat']['jadwal-diklat']['create'] }}" {{ $record->getChecked('backend/diklat/jadwal-diklat/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['diklat']['jadwal-diklat']['edit'] }}" {{ $record->getChecked('backend/diklat/jadwal-diklat/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['diklat']['jadwal-diklat']['delete'] }}" {{ $record->getChecked('backend/diklat/jadwal-diklat/delete') }}> <label></label>
                </div>
              </td>
            </tr>
						<tr>
              <td colspan="6"><h3>7. Litbang</h3></td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>7.1.</h4></td>
              <td colspan="2"><h4>Info Litbang</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['info-litbang']['view'] }}" {{ $record->getChecked('backend/litbang/info-litbang') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['info-litbang']['create'] }}" {{ $record->getChecked('backend/litbang/info-litbang/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['info-litbang']['edit'] }}" {{ $record->getChecked('backend/litbang/info-litbang/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['info-litbang']['delete'] }}" {{ $record->getChecked('backend/litbang/info-litbang/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>7.2.</h4></td>
              <td colspan="2"><h4>Komite Etik</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['komite-etik']['view'] }}" {{ $record->getChecked('backend/litbang/komite-etik') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['komite-etik']['create'] }}" {{ $record->getChecked('backend/litbang/komite-etik/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['komite-etik']['edit'] }}" {{ $record->getChecked('backend/litbang/komite-etik/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['komite-etik']['delete'] }}" {{ $record->getChecked('backend/litbang/komite-etik/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>7.3.</h4></td>
              <td colspan="2"><h4>List Publikasi</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['list-publikasi']['view'] }}" {{ $record->getChecked('backend/litbang/list-publikasi') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['list-publikasi']['create'] }}" {{ $record->getChecked('backend/litbang/list-publikasi/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['list-publikasi']['edit'] }}" {{ $record->getChecked('backend/litbang/list-publikasi/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['litbang']['list-publikasi']['delete'] }}" {{ $record->getChecked('backend/litbang/list-publikasi/delete') }}> <label></label>
                </div>
              </td>
            </tr>
						<tr>
              <td colspan="3"><h3>8. Informasi</h3></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['informasi']['view'] }}" {{ $record->getChecked('backend/informasi') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['informasi']['create'] }}" {{ $record->getChecked('backend/informasi/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['informasi']['edit'] }}" {{ $record->getChecked('backend/informasi/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['informasi']['delete'] }}" {{ $record->getChecked('backend/informasi/delete') }}> <label></label>
                </div>
              </td>
            </tr>
						<tr>
              <td colspan="3"><h3>9. Mutu</h3></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mutu']['view'] }}" {{ $record->getChecked('backend/mutu') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mutu']['create'] }}" {{ $record->getChecked('backend/mutu/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mutu']['edit'] }}" {{ $record->getChecked('backend/mutu/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mutu']['delete'] }}" {{ $record->getChecked('backend/mutu/delete') }}> <label></label>
                </div>
              </td>
            </tr>
						<tr>
              <td colspan="3"><h3>10. Kuesioner</h3></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['kuesioner']['view'] }}" {{ $record->getChecked('backend/kuesioner') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['kuesioner']['create'] }}" {{ $record->getChecked('backend/kuesioner/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['kuesioner']['edit'] }}" {{ $record->getChecked('backend/kuesioner/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['kuesioner']['delete'] }}" {{ $record->getChecked('backend/kuesioner/delete') }}> <label></label>
                </div>
              </td>
            </tr>
						<tr>
              <td colspan="3"><h3>11. Testimoni</h3></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['testimoni']['view'] }}" {{ $record->getChecked('backend/testimoni') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['testimoni']['create'] }}" {{ $record->getChecked('backend/testimoni/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['testimoni']['edit'] }}" {{ $record->getChecked('backend/testimoni/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['testimoni']['delete'] }}" {{ $record->getChecked('backend/testimoni/delete') }}> <label></label>
                </div>
              </td>
            </tr>
						<tr>
              <td colspan="3"><h3>12. Pertanyaan</h3></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pertanyaan']['view'] }}" {{ $record->getChecked('backend/pertanyaan') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pertanyaan']['create'] }}" {{ $record->getChecked('backend/pertanyaan/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pertanyaan']['edit'] }}" {{ $record->getChecked('backend/pertanyaan/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pertanyaan']['delete'] }}" {{ $record->getChecked('backend/pertanyaan/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="6"><h3>13. Pengaduan</h3></td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>13.1.</h4></td>
              <td colspan="2"><h4>Etik & Hukum</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['etik-hukum']['view'] }}" {{ $record->getChecked('backend/pengaduan/etik-hukum') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['etik-hukum']['create'] }}" {{ $record->getChecked('backend/pengaduan/etik-hukum/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['etik-hukum']['edit'] }}" {{ $record->getChecked('backend/pengaduan/etik-hukum/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['etik-hukum']['delete'] }}" {{ $record->getChecked('backend/pengaduan/etik-hukum/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>13.2.</h4></td>
              <td colspan="2"><h4>Pengaduan Masyarakat</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['masyarakat']['view'] }}" {{ $record->getChecked('backend/pengaduan/masyarakat') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['masyarakat']['create'] }}" {{ $record->getChecked('backend/pengaduan/masyarakat/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['masyarakat']['edit'] }}" {{ $record->getChecked('backend/pengaduan/masyarakat/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['masyarakat']['delete'] }}" {{ $record->getChecked('backend/pengaduan/masyarakat/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>13.3.</h4></td>
              <td colspan="2"><h4>Whistleblowing</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['whistleblowing']['view'] }}" {{ $record->getChecked('backend/pengaduan/whistleblowing') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['whistleblowing']['create'] }}" {{ $record->getChecked('backend/pengaduan/whistleblowing/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['whistleblowing']['edit'] }}" {{ $record->getChecked('backend/pengaduan/whistleblowing/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['whistleblowing']['delete'] }}" {{ $record->getChecked('backend/pengaduan/whistleblowing/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>13.4.</h4></td>
              <td colspan="2"><h4>Gratifikasi</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['gratifikasi']['view'] }}" {{ $record->getChecked('backend/pengaduan/gratifikasi') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['gratifikasi']['create'] }}" {{ $record->getChecked('backend/pengaduan/gratifikasi/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['gratifikasi']['edit'] }}" {{ $record->getChecked('backend/pengaduan/gratifikasi/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['gratifikasi']['delete'] }}" {{ $record->getChecked('backend/pengaduan/gratifikasi/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>13.5.</h4></td>
              <td colspan="2"><h4>Sponsor</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['sponsor']['view'] }}" {{ $record->getChecked('backend/pengaduan/sponsor') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['sponsor']['create'] }}" {{ $record->getChecked('backend/pengaduan/sponsor/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['sponsor']['edit'] }}" {{ $record->getChecked('backend/pengaduan/sponsor/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaduan']['sponsor']['delete'] }}" {{ $record->getChecked('backend/pengaduan/sponsor/delete') }}> <label></label>
                </div>
              </td>
            </tr>
						<tr>
              <td colspan="6"><h3>14. Pengaturan Website</h3></td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>14.1.</h4></td>
              <td colspan="2"><h4>Home Slider</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['slider']['view'] }}" {{ $record->getChecked('backend/master/slider') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['slider']['create'] }}" {{ $record->getChecked('backend/master/slider/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['slider']['edit'] }}" {{ $record->getChecked('backend/master/slider/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['slider']['delete'] }}" {{ $record->getChecked('backend/master/slider/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>14.2.</h4></td>
              <td colspan="2"><h4>Pengaturan Gambar</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['jumbotron']['view'] }}" {{ $record->getChecked('backend/setting/jumbotron') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['jumbotron']['create'] }}" {{ $record->getChecked('backend/setting/jumbotron/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['jumbotron']['edit'] }}" {{ $record->getChecked('backend/setting/jumbotron/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['jumbotron']['delete'] }}" {{ $record->getChecked('backend/setting/jumbotron/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>14.3.</h4></td>
              <td colspan="2"><h4>Sertifikat</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['sertifikat-footer']['view'] }}" {{ $record->getChecked('backend/master/sertifikat-footer') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['sertifikat-footer']['create'] }}" {{ $record->getChecked('backend/master/sertifikat-footer/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['sertifikat-footer']['edit'] }}" {{ $record->getChecked('backend/master/sertifikat-footer/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['sertifikat-footer']['delete'] }}" {{ $record->getChecked('backend/master/sertifikat-footer/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>14.4.</h4></td>
              <td colspan="2"><h4>Media Sosial</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['media-sosial']['view'] }}" {{ $record->getChecked('backend/master/media-sosial') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['media-sosial']['create'] }}" {{ $record->getChecked('backend/master/media-sosial/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['media-sosial']['edit'] }}" {{ $record->getChecked('backend/master/media-sosial/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['media-sosial']['delete'] }}" {{ $record->getChecked('backend/master/media-sosial/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>14.5.</h4></td>
              <td colspan="2"><h4>Link Footer</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['link-footer']['view'] }}" {{ $record->getChecked('backend/master/link-footer') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['link-footer']['create'] }}" {{ $record->getChecked('backend/master/link-footer/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['link-footer']['edit'] }}" {{ $record->getChecked('backend/master/link-footer/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['link-footer']['delete'] }}" {{ $record->getChecked('backend/master/link-footer/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>14.6.</h4></td>
              <td colspan="2"><h4>Pengaturan Menu</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['menu']['view'] }}" {{ $record->getChecked('backend/setting/menu') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['menu']['create'] }}" {{ $record->getChecked('backend/setting/menu/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['menu']['edit'] }}" {{ $record->getChecked('backend/setting/menu/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['menu']['delete'] }}" {{ $record->getChecked('backend/setting/menu/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>14.7.</h4></td>
              <td colspan="2"><h4>Setting</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['setting']['view'] }}" {{ $record->getChecked('backend/setting/setting') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['setting']['create'] }}" {{ $record->getChecked('backend/setting/setting/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['setting']['edit'] }}" {{ $record->getChecked('backend/setting/setting/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['setting']['delete'] }}" {{ $record->getChecked('backend/setting/setting/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>14.8.</h4></td>
              <td colspan="2"><h4>Manajemen Pengguna</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['user']['view'] }}" {{ $record->getChecked('backend/setting/user') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['user']['create'] }}" {{ $record->getChecked('backend/setting/user/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['user']['edit'] }}" {{ $record->getChecked('backend/setting/user/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['setting']['user']['delete'] }}" {{ $record->getChecked('backend/setting/user/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>14.9.</h4></td>
              <td colspan="2"><h4>Role & Permission</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['role-permission']['view'] }}" {{ $record->getChecked('backend/master/role-permission') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['role-permission']['create'] }}" {{ $record->getChecked('backend/master/role-permission/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['role-permission']['edit'] }}" {{ $record->getChecked('backend/master/role-permission/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['pengaturan-website']['role-permission']['delete'] }}" {{ $record->getChecked('backend/master/role-permission/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="6"><h3>15. Pengaturan Mobile</h3></td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>15.1.</h4></td>
              <td colspan="2"><h4>Direksi</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mobile']['direksi']['view'] }}" {{ $record->getChecked('backend/mobile/direksi') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mobile']['direksi']['create'] }}" {{ $record->getChecked('backend/mobile/direksi/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mobile']['direksi']['edit'] }}" {{ $record->getChecked('backend/mobile/direksi/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mobile']['direksi']['delete'] }}" {{ $record->getChecked('backend/mobile/direksi/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>15.2.</h4></td>
              <td colspan="2"><h4>Dewan Pengawas</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mobile']['dewan']['view'] }}" {{ $record->getChecked('backend/mobile/dewan') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mobile']['dewan']['create'] }}" {{ $record->getChecked('backend/mobile/dewan/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mobile']['dewan']['edit'] }}" {{ $record->getChecked('backend/mobile/dewan/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['mobile']['dewan']['delete'] }}" {{ $record->getChecked('backend/mobile/dewan/delete') }}> <label></label>
                </div>
              </td>
            </tr>
						<tr>
              <td colspan="6"><h3>16. Master</h3></td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>16.1.</h4></td>
              <td colspan="2"><h4>Poli</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['poli']['view'] }}" {{ $record->getChecked('backend/master/poli') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['poli']['create'] }}" {{ $record->getChecked('backend/master/poli/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['poli']['edit'] }}" {{ $record->getChecked('backend/master/poli/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['poli']['delete'] }}" {{ $record->getChecked('backend/master/poli/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>16.2.</h4></td>
              <td colspan="2"><h4>Indikator Mutu</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['jenis-mutu']['view'] }}" {{ $record->getChecked('backend/master/jenis-mutu') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['jenis-mutu']['create'] }}" {{ $record->getChecked('backend/master/jenis-mutu/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['jenis-mutu']['edit'] }}" {{ $record->getChecked('backend/master/jenis-mutu/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['jenis-mutu']['delete'] }}" {{ $record->getChecked('backend/master/jenis-mutu/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>16.3.</h4></td>
              <td colspan="2"><h4>Jabatan Dokter</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['jabatan-dokter']['view'] }}" {{ $record->getChecked('backend/master/jabatan-dokter') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['jabatan-dokter']['create'] }}" {{ $record->getChecked('backend/master/jabatan-dokter/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['jabatan-dokter']['edit'] }}" {{ $record->getChecked('backend/master/jabatan-dokter/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['jabatan-dokter']['delete'] }}" {{ $record->getChecked('backend/master/jabatan-dokter/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>16.4.</h4></td>
              <td colspan="2"><h4>Spesialisasi</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['spesialisasi']['view'] }}" {{ $record->getChecked('backend/master/spesialisasi') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['spesialisasi']['create'] }}" {{ $record->getChecked('backend/master/spesialisasi/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['spesialisasi']['edit'] }}" {{ $record->getChecked('backend/master/spesialisasi/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['spesialisasi']['delete'] }}" {{ $record->getChecked('backend/master/spesialisasi/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>16.5.</h4></td>
              <td colspan="2"><h4>Sub Spesialisasi</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['sub-spesialisasi']['view'] }}" {{ $record->getChecked('backend/master/sub-spesialisasi') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['sub-spesialisasi']['create'] }}" {{ $record->getChecked('backend/master/sub-spesialisasi/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['sub-spesialisasi']['edit'] }}" {{ $record->getChecked('backend/master/sub-spesialisasi/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['sub-spesialisasi']['delete'] }}" {{ $record->getChecked('backend/master/sub-spesialisasi/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>16.6.</h4></td>
              <td colspan="2"><h4>Kategori Artikel</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-artikel']['view'] }}" {{ $record->getChecked('backend/master/kategori-artikel') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-artikel']['create'] }}" {{ $record->getChecked('backend/master/kategori-artikel/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-artikel']['edit'] }}" {{ $record->getChecked('backend/master/kategori-artikel/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-artikel']['delete'] }}" {{ $record->getChecked('backend/master/kategori-artikel/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>16.7.</h4></td>
              <td colspan="2"><h4>Kategori Berita</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-pengumuman']['view'] }}" {{ $record->getChecked('backend/master/kategori-pengumuman') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-pengumuman']['create'] }}" {{ $record->getChecked('backend/master/kategori-pengumuman/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-pengumuman']['edit'] }}" {{ $record->getChecked('backend/master/kategori-pengumuman/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-pengumuman']['delete'] }}" {{ $record->getChecked('backend/master/kategori-pengumuman/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>16.8.</h4></td>
              <td colspan="2"><h4>Kategori Tanya Jawab</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-tanya-jawab']['view'] }}" {{ $record->getChecked('backend/master/kategori-tanya-jawab') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-tanya-jawab']['create'] }}" {{ $record->getChecked('backend/master/kategori-tanya-jawab/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-tanya-jawab']['edit'] }}" {{ $record->getChecked('backend/master/kategori-tanya-jawab/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['kategori-tanya-jawab']['delete'] }}" {{ $record->getChecked('backend/master/kategori-tanya-jawab/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>16.9.</h4></td>
              <td colspan="2"><h4>Tipe Media</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['tipe-media']['view'] }}" {{ $record->getChecked('backend/master/tipe-media') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['tipe-media']['create'] }}" {{ $record->getChecked('backend/master/tipe-media/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['tipe-media']['edit'] }}" {{ $record->getChecked('backend/master/tipe-media/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['tipe-media']['delete'] }}" {{ $record->getChecked('backend/master/tipe-media/delete') }}> <label></label>
                </div>
              </td>
            </tr>
            <tr>
              <td width="15%" class="right aligned"><h4>16.10.</h4></td>
              <td colspan="2"><h4>Tipe Pendaftar</h4></td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['tipe-pendaftar']['view'] }}" {{ $record->getChecked('backend/master/tipe-pendaftar') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['tipe-pendaftar']['create'] }}" {{ $record->getChecked('backend/master/tipe-pendaftar/create') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['tipe-pendaftar']['edit'] }}" {{ $record->getChecked('backend/master/tipe-pendaftar/edit') }}> <label></label>
                </div>
              </td>
              <td>
                <div class="ui fitted toggle checkbox">
                  <input type="checkbox" name="permission[]" value="{{ $permission['master']['tipe-pendaftar']['delete'] }}" {{ $record->getChecked('backend/master/tipe-pendaftar/delete') }}> <label></label>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<button class="ui right labeled blue icon save as page button">
					<i class="save icon"></i>
					Simpan
				</button>
			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
</script>
@append
