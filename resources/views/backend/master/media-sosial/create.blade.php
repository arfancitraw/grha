<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Media Sosial</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui grid">
			<div class="sixteen wide column">

				<div class="fields">
					<div class="ten wide field">
						<div class="field">
							<label for="nama">Nama </label>
							<input type="text" name="nama" placeholder="Nama">
						</div>
						<div class="field">
							<label for="nama">Icon (Font Awesome) </label>
							<input type="text" name="icon" placeholder="Icon">
						</div>
						<div class="field">
							<label for="">Link</label>
							<input type="text" name="link" placeholder="Link">
						</div>
					</div>
				</div>



			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>