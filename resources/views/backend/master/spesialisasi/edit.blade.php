<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Spesialisasi</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="nama">Nama Spesialisasi</label>
					<input type="text" name="nama" value="{{$record->nama}}">
				</div>
				<div class="field">
					<label for="nama">Prioritas</label>
					<input type="text" name="prioritas" value="{{$record->prioritas}}">
				</div>
				<div class="fields">
					<div class="five wide field image-container">
						<label>Icon Spesialisasi</label>
						<img class="image-preview" style="height: 135px; padding: 10px" src="{{ !is_null($record->icon) ? url('storage/'.$record->icon) : asset('img/plc.png') }}">
						<div class="ui fluid file input action">
							<input type="text" readonly="" placeholder="Icon Spesialisasi">
							<input type="file" class="ten wide column attachment" name="icon" autocomplete="off" multiple="">

							<div class="ui blue button file">
								Cari...
							</div>
						</div>
					</div>
					<div class="eleven wide field">
						<label for="">Deskripsi</label>
						<textarea name="deskripsi" id="" rows="3" style="height: 167px; resize: none">{{$record->deskripsi}}</textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Update
		<i class="checkmark icon"></i>
	</div>
</div>