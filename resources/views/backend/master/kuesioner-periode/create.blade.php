@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	<div class="ui attached top segment">
		{!! csrf_field() !!}
		<input type="hidden" name="slug" readonly placeholder="Judul Pelayanan">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="">Periode</label>
					<textarea name="periode" rows="5"></textarea>
				</div>
			</div>
		</div>
		<br />
			<div class="sixteen wide column">
				<div class="field">
					<label for="tahun">Status</label>
					<div class="ui form">
						<div class="inline fields">
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="status" value="1" checked="checked">
									<label>Aktif</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="status" value="0">
									<label>Tidak Aktif</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.editor').summernote({
		height: '155px',
		placeholder: "Konten..."
	});
	$('.date').calendar({
		type: 'date',
		// formatter: {
		// 	date: function(date, setting) {
		// 		var year = date.getFullYear();
		// 		var month = date.getMonth() + 1;
		// 		var day = date.getDate();
		// 		if (month < 10) {
		// 			month = '0' + month;
		// 		}
		// 		if (day < 10) {
		// 			day = '0' + day;
		// 		}
		// 		return day + '/' + month + '/' + year
		// 	}
		// }
	})
	$('.time').calendar({
		type: 'time',
		ampm: false
	})
</script>
@endsection