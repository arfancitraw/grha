@extends('layouts.grid')
@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">	
	<div class="ui attached top segment">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="">Periode</label>
					<textarea name="periode" rows="5">{{$record->periode}}</textarea>
				</div>
			</div>
		</div>
		<br />
		<div class="sixteen wide column">
			<div class="field">
				<label for="tahun">Status</label>
				<div class="ui form">
					<div class="inline fields">
						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="status" value="1" {{ ($record->status == 1 ) ? 'checked' : '' }} {{ ($record->status == 0 ) ? '' : '' }}>
								<label>Aktif</label>
							</div>
						</div>
						<div class="field">
							<div class="ui radio checkbox">
								<input type="radio" name="status" value="0" {{ ($record->status == 0 ) ? 'checked' : '' }} {{ ($record->status == 1 ) ? '' : '' }}>
								<label>Tidak Aktif</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection