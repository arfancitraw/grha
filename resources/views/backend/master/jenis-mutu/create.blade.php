<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Jenis Mutu</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label for="jabatan">Nama Mutu</label>
					<input type="text" name="nama" placeholder="Nama Mutu">
				</div>
				<div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" id="" rows="3"></textarea>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>