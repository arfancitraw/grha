<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Kategori Tanya Jawab</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="fields">
					<div class="twelve wide field">
						<label for="nama">Nama Kategori</label>
						<input type="text" name="nama">
					</div>
					<div class="four wide field">
						<label for="pernyataan">Butuh Pernyataan?</label>
						<select name="pernyataan" class="ui fluid dropdown">
							<option value="0">Tidak</option>
							<option value="1">Ya</option>
						</select>
					</div>
				</div>
				<div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" id="" rows="3"></textarea>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>