<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Link Footer</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<div class="field">
			<label for="nama">Nama / Judul Sertifikat</label>
			<input type="text" name="nama" placeholder="Nama Sertifikat" value="{{ $record->nama }}">
		</div>
		<div class="ui grid">
			<div class="six wide column">
				<div class="field image-container">
					<img class="image-preview"  style="height: 200px; object-fit: contain;" id="showAttachment" src="{{ $record->gambar ? asset('storage/'.$record->gambar) : asset('img/default.jpg') }}">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Logo Sertifikat">
						<input type="file" class="ten wide column" id="attachment" name="gambar" autocomplete="off" multiple="">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</div>
			<div class="ten wide column">
				<div class="field">
					<label for="">Keterangan</label>
					<textarea name="keterangan" style="height: 150px; resize: none" placeholder="Keterangan Tertentu">{!! nl2br(e($record->keterangan)) !!}</textarea>
				</div>
				<div class="field">
					<label for="">Link</label>
					<input type="text" name="link" placeholder="Link / Tautan Web Sertifikasi" value="{{ $record->link }}">
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Update
		<i class="checkmark icon"></i>
	</div>
</div>