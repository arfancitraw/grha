@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('scripts')
<script type="text/javascript">

	function readURL(input) {
		if(input.files && input.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#showAttachment').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).on('change', '#attachment', function () {
		readURL(this);
	});

	$('.editor').summernote({
		height: '200px'
	});

</script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	<div class="ui attached top segment">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">
		{{-- <div class="ui error message"></div> --}}
		<div class="sixteen wide column inner2bottom inner1 inner2" id="inner2bottom">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="two fields">
					<div class="six wide field image-container">
						@if($record->gambar)
							<img class="image-preview"  style="height: 276px; object-fit: cover;" id="showAttachment" src="{{ asset('storage').'/'.$record->gambar }}">
						@else
							<img class="image-preview"  style="height: 276px; object-fit: cover;" id="showAttachment" src="{{ asset('img/default.jpg') }}">
						@endif
						<div class="ui fluid file input action">
							<input type="text" readonly="">
							<input type="file" class="ten wide column" id="attachment" name="gambar" autocomplete="off" multiple="">

							<div class="ui blue button file">
								Cari...
							</div>
						</div>
					</div>
					<div class="ten wide field">
					<div class="ten wide field">
						<div class="field">
							<label for="nama">Nama </label>
							<input type="text" name="nama" value="{{$record->nama}}">
						</div>
						<div class="field">
							<label for="nama">Jumlah Total </label>
							<input type="text" name="jumlah_total" value="{{$record->jumlah_total}}">
						</div>
						<div class="field">
							<label for="nama">Isi</label>
							<input type="text" name="isi" value="{{$record->isi}}">
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<div class="ui bottom attached segment">
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<button class="ui right labeled blue icon save page button">
				<i class="save icon"></i>
				Simpan
			</button>
		</div>
	</div>
</div>
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nama: 'empty',
		spesialisasi_id: 'empty',
		jabatan_dokter_id: 'empty',
		alamat: 'empty',
		no_tlpn: 'empty',
		spesialisasi_id: 'empty',
		jabatan_dokter_id: 'empty',
	};
</script>
@endsection