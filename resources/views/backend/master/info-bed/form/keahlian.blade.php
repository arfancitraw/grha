<div class="field">
	<div class="keahlian container">
		<div class="fields" data-count="0">
			<div class="fourteen wide field">
				<input type="text" name="keahlian[0][nama]" placeholder="Keahlian">
			</div>
			<div class="two wide field">
				<button type="button" class="ui positive fluid icon button add-keahlian" data-content="Tambah Keahlian"><i class="plus icon"></i></button>
			</div>
		</div>
	</div>
</div>

@section('scripts')
<script type="text/javascript">
	$(document).on('click', '.remove-keahlian', function(event) {
		$(this).closest('.fields').remove();
		calcTotal();
	});

	$('.add-keahlian').click(function(event) {
		var idx = parseInt($('.keahlian.container .fields').last().data('count')) + 1;
		var html = `<div class="fields" data-count="`+idx+`">
		<div class="fourteen wide field">
		<input type="text" name="keahlian[`+idx+`][nama]" placeholder="Keahlian">
		</div>
		<div class="two wide field">
		<button type="button" class="ui negative fluid icon button remove-keahlian" data-content="Hapus Keahlian"><i class="remove icon"></i></button>
		</div>
		</div>`;

		$('.keahlian.container').append(html);
	});
</script>
@append