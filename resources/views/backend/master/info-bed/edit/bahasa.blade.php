<div class="field">
	<div class="bahasa container">
		@if($record->bahasa->count() > 0)
		@php $i = 0; @endphp
		@foreach ($record->bahasa as $bahasa)
		<div class="fields" data-count="{{$i}}">
			<div class="fourteen wide field">
				<input type="text" name="bahasa[{{$i}}][nama]" placeholder="Bahasa" value="{{ $bahasa->nama or '' }}">
			</div>
			<div class="two wide field">
				@if($i == 0)
				<button type="button" class="ui positive fluid icon button add-bahasa" data-content="Tambah Bahasa"><i class="plus icon"></i></button>
				@else
				<button type="button" class="ui negative fluid icon button remove-bahasa" data-content="Hapus Bahasa"><i class="remove icon"></i></button>
				@endif
			</div>
		</div>
		@php $i++; @endphp
		@endforeach
		@else
		<div class="field">
			<div class="bahasa container">
				<div class="fields" data-count="0">
					<div class="fourteen wide field">
						<input type="text" name="bahasa[0][nama]" placeholder="Bahasa">
					</div>
					<div class="two wide field">
						<button type="button" class="ui positive fluid icon button add-bahasa" data-content="Tambah Bahasa"><i class="plus icon"></i></button>
					</div>
				</div>
			</div>
		</div>
		@endif
	</div>
</div>

@section('scripts')
<script type="text/javascript">
	$(document).on('click', '.remove-bahasa', function(event) {
		$(this).closest('.fields').remove();
		calcTotal();
	});

	$('.add-bahasa').click(function(event) {
		var idx = parseInt($('.bahasa.container .fields').last().data('count')) + 1;
		var html = `<div class="fields" data-count="`+idx+`">
		<div class="fourteen wide field">
		<input type="text" name="bahasa[`+idx+`][nama]" placeholder="Bahasa">
		</div>
		<div class="two wide field">
		<button type="button" class="ui negative fluid icon button remove-bahasa" data-content="Hapus Bahasa"><i class="remove icon"></i></button>
		</div>
		</div>`;

		$('.bahasa.container').append(html);
	});
</script>
@append