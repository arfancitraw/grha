@extends('layouts.grid')

@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	{{-- <div class="ui top attached tabular menu">
		<a class="item" data-tab="first">First</a>
		<a class="item" data-tab="second">Second</a>
		<a class="item active" data-tab="third">Third</a>
	</div> --}}
	<div class="ui segment">
		<div class="ui grid">
			<div class="four wide column">
				<div class="field image-container">
					<img class="image-preview"  style="height: 250px" id="showAttachment" src="{{ !is_null($record->photo) ? url('storage/'.$record->photo) : asset('img/noImage.png') }}">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Foto Profil">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" multiple="">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</div>
			<div class="twelve wide column">
				<h2 class="ui dividing header">
					<i class="user circle icon"></i>
					<div class="content">
						Edit Profile
						<div class="sub header">Untuk Rubah password, isi password lama terlebih dahulu</div>
					</div>
				</h2>
				<div class="ui equal width stackable grid">
					<div class="column">
						<div class="field">
							<label>Nama Pengguna</label>
							<input type="text" name="name" placeholder="Nama Pengguna" value="{{ $record->name }}" required>
						</div>
						<div class="field">
							<label>E-mail</label>
							<input type="email" name="email" placeholder="Email Pengguna" value="{{ $record->email }}" required>
						</div>
					</div>
					<div class="column">
						<div class="field">
							<label>Password Lama</label>
							<input type="password" name="old_password" placeholder="Password Lama">
						</div>
						<div class="two fields">
							<div class="field">
								<label>Password Baru</label>
								<input type="password" name="password" placeholder="Password Baru">
							</div>
							<div class="field">
								<label>Konfirmasi Password</label>
								<input type="password" name="password_confirmation" placeholder="Konfirmasi Password Baru">
							</div>
						</div>
					</div>
				</div>
				<div class="ui divider"></div>
				<div class="ui two column grid">
					<div class="left aligned column">
						<div class="ui black labeled icon button" onclick="window.history.back()">
							<i class="chevron left icon"></i>
							Kembali
						</div>
					</div>
					<div class="right aligned column">
						<div class="ui right labeled green icon save as page button" style="margin-right: 0">
							<i class="save icon"></i>
							Simpan
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- <div class="ui bottom attached tab segment" data-tab="second">
		Second
	</div>
	<div class="ui bottom attached tab segment active" data-tab="third">
		Third
	</div> --}}
</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('.menu .item').tab();	
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@endsection

