@extends('layouts.grid')

@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="ui stackable grid">
		@foreach($settings as $tron)
		<div class="three wide column">
			<h2 class="ui header">
			  {{ $tron['judul'] }}
			  <div class="sub header">Ditampilkan pada halaman dengan tema {{$tron['judul']}}</div>
			</h2>
			<div class="ui info message"><b>Saran Resolusi Gambar : <br>1600px x 400px (4:1)</b></div>
		</div>
		<div class="thirteen wide column">
			<div class="field image-container">
				<img class="image-preview"  style="height: 250px" src="{{ !is_null($tron->isi) ? url('storage/'.$tron->isi) : asset('img/plc.png') }}">
				<div class="ui fluid file input action">
					<input type="text" readonly="" placeholder="Gambar Artikel">
					<input type="file" class="ten wide column attachment" name="setting[{{$tron->id}}][isi]" autocomplete="off" multiple="">

					<div class="ui blue button file">
						Cari...
					</div>
				</div>
			</div>
		</div>
		@endforeach
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>
	</div>
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui black labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<div class="ui right labeled green icon save as page button">
				<i class="save icon"></i>
				Simpan
			</div>
		</div>
	</div>

</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('.menu .item').tab();	
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@endsection

