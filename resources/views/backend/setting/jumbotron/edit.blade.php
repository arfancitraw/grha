@extends('layouts.grid')
@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	<input type="hidden" name="status" id="status" value="{{ $record->status }}">
	
	<div class="ui attached top segment">
		<div class="field">
			<div class="ui big input">
				<input name="judul" value="{{ $record->judul or '' }}" placeholder="Judul Artikel" type="text">
			</div>
		</div>
		<div class="fields">
			<div class="two wide field image-container">
				<label>Icon Pelayanan</label>
				<img class="image-preview"  style="height: 135px; padding: 10px" id="showAttachment" src="{{ !is_null($record->icon) ? url('storage/'.$record->icon) : asset('img/plc.png') }}">
				<div class="ui fluid file input action">
					<input type="text" readonly="" placeholder="Icon Pengumuman">
					<input type="file" class="ten wide column" id="attachment" name="icon" autocomplete="off" multiple="">

					<div class="ui blue button file">
						Cari...
					</div>
				</div>
			</div>
			<div class="fourteen wide field">
				<label>Keterangan Pelayanan</label>
				<textarea name="konten" rows="3" style="height: 167px; resize: none;" placeholder="Masukan penjelasan singkat pelayanan disini">{!! e(nl2br($record->konten)) !!}</textarea>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<button class="ui right labeled blue icon save as page button">
					<i class="save icon"></i>
					Simpan
				</button>
			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('.menu .item').tab();	
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@append