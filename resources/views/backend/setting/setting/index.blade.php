@extends('layouts.grid')

@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="ui stackable grid">
		{{-- Contact --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Kontak
			  <div class="sub header">Merupakan pengaturan untuk akses dan kontak kepada RS Jantung dan Pembuluh Darah Harapan Kita.</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="ui equal width grid">
				<div class="column">
					<div class="two fields">
						<div class="field">
							<label>Call Center</label>
							<input type="text" name="setting[{{$settings['call_center']->id}}][isi]" placeholder="contoh: 1500 034" value="{{ $settings['call_center']->isi }}">
						</div>
						<div class="field">
							<label>Emergency Call</label>
							<input type="text" name="setting[{{$settings['emergency_call']->id}}][isi]" placeholder="contoh: (021) 568 2424" value="{{ $settings['emergency_call']->isi }}">
						</div>
					</div>
					<div class="field">
						<label>Email</label>
						<input type="text" name="setting[{{$settings['email']->id}}][isi]" placeholder="contoh: email@pjnhk.go.id" value="{{ $settings['email']->isi }}">
					</div>
				</div>
				<div class="column">
					<div class="field">
						<label>Alamat</label>
						<textarea name="setting[{{$settings['address']->id}}][isi]" style="height: 100px; resize: none;" placeholder="contoh: Lorem Ipsum dolor sit amet.">{!! nl2br($settings['address']->isi) !!}</textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>
		{{-- Video Profil --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Video Profil
			  <div class="sub header">Ditampilkan dihalaman depan, Video Profil berupa link dari penyedia fitur embed video seperti YouTube, Vimeo, d.l.l.</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="field">
				<input type="text" name="setting[{{$settings['profile-video']->id}}][judul]" placeholder="contoh: Tentang Paviliun Sukaman" value="{{ $settings['profile-video']->judul }}">
			</div>
			<div class="field">
				<textarea name="setting[{{$settings['profile-video']->id}}][isi]" style="height: 100px; resize: none;" placeholder="contoh: Lorem Ipsum dolor sit amet.">{!! nl2br($settings['profile-video']->isi) !!}</textarea>
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>
		{{-- Links --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Link Germas & GooglePlay
			  <div class="sub header">Ditampilkan dibagian bawah web, Kedua item ini dapat diatur link sesuai kebutuhan.</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="field">
				<label>Link Germas</label>
				<input type="text" name="setting[{{$settings['link-germas']->id}}][isi]" placeholder="contoh: Tentang Paviliun Sukaman" value="{{ $settings['link-germas']->isi }}">
			</div>
			<div class="field">
				<label>Link GooglePlay</label>
				<input type="text" name="setting[{{$settings['link-googleplay']->id}}][isi]" placeholder="contoh: Tentang Paviliun Sukaman" value="{{ $settings['link-googleplay']->isi }}">
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>
		{{-- Email Pengaduan --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Email Pengaduan
			  <div class="sub header">Saat terdapat pengaduan tertentu, email akan dikirimkan kepada email terkait.</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="two fields">
				<div class="field">
					<label>Etik & Hukum</label>
					<input type="email" name="setting[{{$settings['pengaduan-email-etik']->id}}][isi]" placeholder="Isikan dengan email yang valid" value="{{ $settings['pengaduan-email-etik']->isi }}">
				</div>
				<div class="field">
					<label>Whistleblowing</label>
					<input type="email" name="setting[{{$settings['pengaduan-email-wbs']->id}}][isi]" placeholder="Isikan dengan email yang valid" value="{{ $settings['pengaduan-email-wbs']->isi }}">
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>Pengaduan Masyarakat</label>
					<input type="email" name="setting[{{$settings['pengaduan-email-masyarakat']->id}}][isi]" placeholder="Isikan dengan email yang valid" value="{{ $settings['pengaduan-email-masyarakat']->isi }}">
				</div>
				<div class="field">
					<label>Gratifikasi</label>
					<input type="email" name="setting[{{$settings['pengaduan-email-gratifikasi']->id}}][isi]" placeholder="Isikan dengan email yang valid" value="{{ $settings['pengaduan-email-gratifikasi']->isi }}">
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>Sponsor</label>
					<input type="email" name="setting[{{$settings['pengaduan-email-sponsor']->id}}][isi]" placeholder="Isikan dengan email yang valid" value="{{ $settings['pengaduan-email-sponsor']->isi }}">
				</div>
				<div class="field">
				</div>
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>
	</div>
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui black labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<div class="ui right labeled green icon save as page button">
				<i class="save icon"></i>
				Simpan
			</div>
		</div>
	</div>

</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('.menu .item').tab();	
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@endsection

