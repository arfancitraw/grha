<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Pengguna</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<div class="three fields">
			<div class="field">
				<label for="nama">Nama Pengguna</label>
				<input type="text" name="name" placeholder="Nama Pengguna" value="{{ $record->name or '' }}">
			</div>
			<div class="field">
				<label for="email">Email Pengguna</label>
				<input type="email" name="email" placeholder="Email Pengguna" value="{{ $record->email or '' }}">
			</div>
			<div class="field">
				<label for="email">Role & Permission</label>
				<select class="ui selection dropdown" name="role_id">
					{!! \App\Models\Entrust\Role::options('display_name', 'id', [
						'selected' => !is_null($record->role()->first()) ? $record->role()->first()->id : '',
						]) !!}
				</select>
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				<label for="password">Password</label>
				<input type="password" name="password" placeholder="Password">
			</div>
			<div class="field">
				<label for="password">Konfirmasi Password</label>
				<input type="password" name="password_confirmation" placeholder="Konfirmasi Password">
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny left floated button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Update
		<i class="checkmark icon"></i>
	</div>
</div>
