<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Pengguna Baru</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="three fields">
			<div class="field">
				<label for="nama">Nama Pengguna</label>
				<input type="text" name="name" placeholder="Nama Pengguna">
			</div>
			<div class="field">
				<label for="email">Email Pengguna</label>
				<input type="email" name="email" placeholder="Email Pengguna">
			</div>
			<div class="field">
				<label for="email">Role & Permission</label>
				<select class="ui selection dropdown" name="role_id">
					{!! \App\Models\Entrust\Role::options('display_name') !!}
				</select>
			</div>
		</div>
		<div class="two fields">
			<div class="field">
				<label for="password">Password</label>
				<input type="password" name="password" placeholder="Password">
			</div>
			<div class="field">
				<label for="password">Konfirmasi Password</label>
				<input type="password" name="password_confirmation" placeholder="Konfirmasi Password">
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny left floated button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>
