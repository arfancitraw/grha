<div class="ui styled accordion" style="border-top-left-radius: 0; border-top-right-radius: 0;">
	@include('backend.setting.menu.side.spesial')
	@include('backend.setting.menu.side.profil')
	@include('backend.setting.menu.side.layanan')
	@include('backend.setting.menu.side.informasi')
	<div class="active title">
		<i class="dropdown icon"></i>
		Lainnya
	</div>
	<div class="active content">
		<form class="ui form" action="{{ url($pageUrl) }}" method="POST">
			{!! csrf_field() !!}
			<div class="field">
				<label for="">Judul :</label>
				<input type="text" name="judul" placeholder="Judul Menu">
			</div>
			<div class="field">
				<label for="tautan">Tautan :</label>
				<input type="text" name="tautan" placeholder="Tautan Menu">
			</div>
			<button type="button" class="ui left floated teal add-separator button">Tambah Separator</button>
			<button type="button" class="ui right floated blue add-menu button">Tambah Menu</button>
			<br>
		</form>
		<form id="formSeparator" action="{{ url($pageUrl) }}" method="POST">
			{!! csrf_field() !!}
			<input type="hidden" name="judul" value="------------------------------------------------------------------">
			<input type="hidden" name="tautan" value="separator">
		</form>
	</div>
</div>