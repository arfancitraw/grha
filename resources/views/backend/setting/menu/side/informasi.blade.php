<div class="title">
	<i class="dropdown icon"></i>
	Konten Informasi
</div>
<div class="content">
	<form class="ui form" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<div class="two fields">
			<div class="field">
				<label for="">Halaman :</label>
                <select class="ui search selection dropdown input-halaman" placeholder="Pilih Salah Satu" required>
                    <option value="">Pilih Salah Satu</option>
                    @foreach ($informasi as $item)
                        <option value="{{ $item->judul }}" data-url="{{ url('informasi/'.$item->slug) }}">{{$item->judul}}</option>
                    @endforeach
                </select>
			</div>
			<div class="field">
				<label for="">Judul :</label>
				<input type="text" name="judul" class="input-judul" placeholder="Judul Menu">
			</div>
		</div>
		<div class="field">
			<label for="tautan">Tautan :</label>
			<input type="text" name="tautan" class="input-tautan" placeholder="Tautan Menu" readonly style="background:#fafafa">
		</div>
		<button type="button" class="ui right floated blue add-menu button">Tambah Menu</button>
		<br>
	</form>
</div>