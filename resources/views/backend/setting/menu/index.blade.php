@extends('layouts.grid')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/nestable/jquery.nestable2.css') }}">
@append

@section('js')
	<script src="{{ asset('plugins/nestable/jquery.nestable2.js') }}"></script>
@append

@section('content-body')
<div class="ui grid">
	<div class="ui six wide column">
		<h3 class="ui top attached center aligned header">
			Sumber Menu
		</h3>
		@include('backend.setting.menu.side.index')
	</div>
	<div class="ui ten wide column">
		<h3 class="ui top attached center aligned header">
			Menu Website
      <div class="ui right floated tiny buttons" style="margin-top: -3px;" id="nestable-menu">
        <button type="button" class="ui teal button" data-action="expandAll"><i class="folder open icon"></i>Buka Semua</button>
        <button type="button" class="ui orange button" data-action="collapseAll"><i class="folder icon"></i>Tutup Semua</button>
      </div>
		</h3>
		<div class="ui attached segment" id="menu-list">
      @if(count($record) == 0)
        <div class="clear text-center">
            <i class="text-center">Belum terdapat menu dalam website</i>
        </div>
      @else
          <div ui-jq="nestable" class="dd" id="menu-container" style="position: relative;">
              @include('backend.setting.menu.list', ['data' => $record])
          </div>
      @endif
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script>
	$(document).ready(function($) {
        // $('.dd').nestable();
        // $('.chosen').chosen();
        reloadMenu();
    });

    $(document).on('change', '.input-halaman', function(e){
        var container = $(this).closest('.ui.form');
        var value = $(this).find('option:selected').val();
        var url = $(this).find('option:selected').data('url');

        container.find('.input-judul').val(value);
        container.find('.input-tautan').val(url);
    });

    $(document).on('click', '.add-separator', function(e){
        var url = "{{url($pageUrl)}}";

        $("#formSeparator").ajaxSubmit({
            url: url,
            type: 'POST',
            success: function(e){
                swal(
                  'Berhasil!',
                  'Data anda berhasil disimpan.',
                  'success'
                ).then(function(e){
                    reloadMenu();
                });
            },
            error: function(response) {
                console.log(response.responseJSON)
                if (response.status == '422') { // data tidak valid
                    // populateError('#form-data', response.responseJSON);
                } else{
                    swal(
                      'Gagal!',
                      response.responseJSON.errors.message,
                      'error'
                    ).then(function(response){
                        reloadMenu();
                    });
                }
            }
        });
    });
    $(document).on('click', '.add-menu', function(e){
        var container = $(this).closest('form');
        var url = "{{url($pageUrl)}}";

        container.ajaxSubmit({
            url: url,
            type: 'POST',
            success: function(e){
                swal(
                  'Berhasil!',
                  'Data anda berhasil disimpan.',
                  'success'
                ).then(function(e){
                    reloadMenu();
                });
            },
            error: function(response) {
                console.log(response.responseJSON)
                if (response.status == '422') { // data tidak valid
                    // populateError('#form-data', response.responseJSON);
                } else{
                    swal(
                      'Gagal!',
                      response.responseJSON.errors.message,
                      'error'
                    ).then(function(response){
                        reloadMenu();
                    });
                }
            }
        });
    });

    reloadMenu = function(){
        var container = $("#menu-container");

        $('#menu-list').addClass('loading')

        $.get("{{url($pageUrl).'/load'}}", function(response) {
            container.html(response);
            $('#menu-list').removeClass('loading');
            $('.dd').nestable({
                maxDepth: 3,
                expandBtnHTML: '<button type="button" data-action="expand" class="ui very basic tiny icon button" style="display:none"><i class="caret right icon"></i></button>',
                collapseBtnHTML: '<button type="button" data-action="collapse" class="ui very basic tiny icon button"><i class="caret down icon"></i></button>', 
                callback: function(l,e){
                    var json = $('.dd').nestable('serialize');
                    console.log(json);
                    $.ajax({
                        url: '{{url($pageUrl.'save')}}',
                        type: 'post',
                        data: { _token: "{{ csrf_token() }}", data:json },
                        success: function(e){
                            // reloadMenu();
                        },
                        error: function(e) {
                            swal(
                              'Gagal!',
                              e.responseJSON.error,
                              'error'
                            ).then(function(e){
                                reloadMenu();
                            });
                        }
                    });   
                }
            })
            // .nestable('collapseAll');
        });
    }

    changeMenu = function(id){
        swal({
          title: "Perhatian!",
          text: "Yakin ingin merubah tipe menu?",
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, Ubah Tipe',
          cancelButtonText: 'Batalkan',
          allowOutsideClick: false
        }).then((result) => {
            if (result){
                $.ajax({
                    url: '{{url($pageUrl)}}/'+id+'/change',
                    data: { _token: "{{ csrf_token() }}" },
                    method: 'post',
                    success: function(e){
                        swal(
                          'Berhasil!',
                          'Data anda telah diubah.',
                          'success'
                        ).then(function(e){
                            reloadMenu();
                        });
                    },
                    error: function(e) {
                        swal(
                          'Gagal!',
                          e.responseJSON.error,
                          'error'
                        ).then(function(e){
                            reloadMenu();
                        });
                    }
                });   
            }
        })
    }

    deleteMenu = function(id){
        swal({
          title: "Peringatan!",
          text: "Yakin ingin menghapus data?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, Hapus Data',
          cancelButtonText: 'Batalkan',
          allowOutsideClick: false
        }).then((result) => {
            if (result){
                $.ajax({
                    url: '{{url($pageUrl)}}/'+id,
                    data: { _token: "{{ csrf_token() }}" },
                    method: 'delete',
                    success: function(e){
                        swal(
                          'Berhasil!',
                          'Data anda telah terhapus.',
                          'success'
                        ).then(function(e){
                            reloadMenu();
                        });
                    },
                    error: function(e) {
                        swal(
                          'Gagal!',
                          e.responseJSON.error,
                          'error'
                        ).then(function(e){
                            reloadMenu();
                        });
                    }
                });   
            }
        })
    }

    $(document).on('click', '#nestable-menu .button', function(e){
      var action = $(this).data('action');
      $('.dd').nestable(action);
    });
	</script>
@append