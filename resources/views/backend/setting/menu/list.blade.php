<ol class="dd-list">
@foreach ($data as $row)
    <li class="item dd-item dd3-item" data-id="{{ $row['id'] }}">
            @if(array_key_exists('childs', $row))
            <button type="button" data-action="expand" class="ui very basic tiny icon button" style="display:none"><i class="caret right icon"></i></button>
            <button type="button" data-action="collapse" class="ui very basic tiny icon button"><i class="caret down icon"></i></button>
            @endif
        <div class="dd-handle dd3-handle">
            <div class="dd-content dd3-content">
                <div class="tools dd-nodrag">
                    @if(is_null($row['induk_id']))
                        @if($row['right'])
                            <button type="button" class="ui orange tiny button" onclick="changeMenu({{ $row['id'] }})">Kanan</button>
                        @else
                            <button type="button" class="ui green tiny button" onclick="changeMenu({{ $row['id'] }})">Kiri</button>
                        @endif
                    @endif
                    <button type="button" class="ui red tiny icon button" onclick="deleteMenu({{ $row['id'] }})"><i class="cancel icon"></i></button>
                </div>
                {{ $row['judul'] }}
            </div>
        </div>
        @if(array_key_exists('childs', $row))
            @include('backend.setting.menu.list', ['data' => $row['childs']])
        @endif
    </li>        
@endforeach
</ol>
