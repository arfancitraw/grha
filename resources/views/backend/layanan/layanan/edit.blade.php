@extends('layouts.grid')

@section('js')
<script type="text/javascript">
	function readURL(input, show) {
		if(input.files && input.files[0])
		{
			var reader = new FileReader();
			reader.onload = function (e) {
				show.attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).ready(function($) {
		$('.ui.checkbox').checkbox();
	});

	$(document).on('change', '.attachment', function () {
		var show = $(this).closest('.field').find('.showAttachment');
		readURL(this, show);
	});

	function menganu(){
		var slug = convertToSlug($('input[name="judul"]').val())
		console.log('slug',slug);
		$('input[name="slug"]').val(slug);
		console.log('asdas',$('input[name="slug"]').val(slug));
	}
	function convertToSlug(Text){
		return Text
		.toLowerCase()
		.replace(/ /g,'-')
		.replace(/[^\w-]+/g,'')
		;
	}

	$(document).on('click', '.icon.download.button', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/"+id+"/download";
		window.open(url,'_blank');
	});
</script>
@append
@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	<input type="hidden" name="status" id="status" value="{{ $record->status }}">
	<input type="hidden" name="posisi" value="{{ $record->posisi }}">
	<div class="ui attached top segment">
		{!! csrf_field() !!}
		<div class="ui grid">
			<div class="eleven wide column">
				<div class="field">
					<div class="ui small labeled input">
						<label for="slug" class="ui label">{{ url('layanan') }}/</label>
						<input type="text" placeholder="Slug" name="slug" value="{{ $record->slug or '' }}" readonly>
					</div>
				</div>
				<div id="tab_layanan">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="layanan_id">ID</a>
						<a class="item" data-tab="layanan_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="layanan_id">
						<div class="ui huge form field">
							<input type="text" name="judul" id="menganu" value="{{$record->judul}}" placeholder="Nama Pelayanan">
						</div>			
						<div class="field">
							<textarea name="konten" rows="3" class="editor" >{!!$record->konten!!}</textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="layanan_en">
						<div class="ui huge form field">
							<input type="text" name="judul_en" placeholder="Nama Pelayanan (EN)" value="{{$record->judul_en}}">
						</div>			
						<div class="field">
							<textarea name="konten_en" rows="3" class="editor" >{!!$record->konten_en!!}</textarea>
						</div>
					</div>
				</div>
				{{-- <div class="ui grid">
					<div class="four wide column">
						<div class="field image-container">
							<label for="">Gambar</label>
							<img class="image-preview showAttachment"  style="height: 150px; object-fit: contain;" src="@if($record->icon) {{asset('storage/'.$record->icon)}} @else {{ asset('img/no-images.png') }} @endif">
							<div class="ui fluid file input action">
								<input type="text" readonly="">
								<input type="file" class="ten wide column attachment" name="icon" autocomplete="off" multiple="">

								<div class="ui blue button file">
									Cari...
								</div>
							</div>
						</div>
					</div>
					<div class="twelve wide column">
						<div class="field">
							<label for="">Keterangan</label>
						</div>
					</div>
				</div> --}}
			</div>
			<div class="five wide column">
				<input type="hidden" name="layanan_unggulan" value="0">
				<input type="hidden" name="home" value="0">
				{{-- <div class="ui segment">
					<div class="field">
						<div class="ui checkbox">
							<input type="hidden" name="layanan_unggulan" value="0">
							<input type="checkbox" class="ui checkbox" value="1" name="layanan_unggulan" {{ $record->layanan_unggulan == 1 ? 'checked' : ''}}>
							<label>Layanan ini merupakan Layanan Unggulan</label>
						</div>
					</div>
				</div>
				<div class="ui segment">
					<div class="field">
						<div class="ui checkbox">
							<input type="hidden" name="home" value="0">
							<input type="checkbox" class="ui checkbox" value="1" name="home" {{ $record->home == 1 ? 'checked' : ''}}>
							<label>Layanan ini akan tampil di beranda</label>
						</div>
					</div>
				</div> --}}
				<div class="field image-container">
					{{-- <label for="">Gambar Cover</label> --}}
					@if(is_null($record->photo))
						<img class="image-preview showAttachment"  style="height: 15rem; object-fit: cover;" src="@if($record->photo) {{asset('storage/'.$record->photo)}} @else {{ asset('img/no-images.png') }} @endif">
						<div class="ui fluid file input action">
							<input type="text" placeholder="Gambar Cover" readonly="">
							<input type="file" class="ten wide column"  id="attachment" name="photo" autocomplete="off" multiple="">

							<div class="ui blue button file">
								Cari...
							</div>
						</div>
					@else
						<img class="image-preview showAttachment"  style="height: 15rem; object-fit: cover;" src="@if($record->photo) {{asset('storage/'.$record->photo)}} @else {{ asset('img/no-images.png') }} @endif">
						<div class="ui fluid file input action">
							<input type="text" placeholder="Gambar Cover" readonly="">
							<input type="file" class="ten wide column"  id="attachment" name="photo" autocomplete="off" multiple="">
							<div class="ui red button remove image" data-url="{{ url($pageUrl.'remove-image/'.$record->id) }}"><i class="trash icon"></i>
								Hapus
							</div>
							<div class="ui blue button file">
								Cari...
							</div>
						</div>
					@endif
				</div>
				<div class="field">
					<label for="">Berkas Terlampir</label>
					@if(isset($record->files))
					<div class="ui fluid file input action">
						<input type="text" placeholder="Berkas Terlampir (Optional)" readonly="" value="{{ str_replace('uploads/layanan/layanan/pdf/', '', $record->files) }}">
						<input type="file" class="ten wide column" name="files" autocomplete="off" accept=".pdf,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">

						<div class="ui blue icon button file" data-tooltip="Unggah berkas baru">
							<i class="upload icon"></i>
						</div>
						<div class="ui teal icon download button" data-id="{{$record->id}}" data-tooltip="Download berkas">
							<i class="download icon"></i>
						</div>
					</div>

					@else
					<div class="ui fluid file input action">
						<input type="text" placeholder="Berkas Terlampir (Optional)" readonly="">
						<input type="file" class="ten wide column" name="files" autocomplete="off" accept=".pdf,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
					@endif
				</div>
				<div id="tab_deskripsi">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="deskripsi_id">ID</a>
						<a class="item" data-tab="deskripsi_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="deskripsi_id">
						<div class="field">
							<label for="">Deskripsi Singkat</label>
							<textarea name="keterangan" style="margin-top: 0px; margin-bottom: 0px; height: 10rem;" rows="3">{!!$record->keterangan!!}</textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="deskripsi_en">
						<div class="field">
							<label for="">Deskripsi Singkat</label>
							<textarea name="keterangan_en" style="margin-top: 0px; margin-bottom: 0px; height: 10rem;" rows="3">{!!$record->keterangan_en!!}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<button class="ui right labeled blue icon save as page button">
					<i class="save icon"></i>
					Simpan
				</button>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('#tab_layanan .menu .item').tab();	
		$('#tab_deskripsi .menu .item').tab();

		$('[name=judul]').on('change, keyup', function(event) {
			var judul = $(this).val();
			$('[name=slug]').val(slugify(judul));
		});
	});
</script>
@endsection
