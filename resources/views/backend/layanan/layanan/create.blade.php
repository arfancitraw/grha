@extends('layouts.grid')

@section('js')
<script type="text/javascript">
	function readURL(input) {
		if(input.files && input.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#showAttachment').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).on('change', '#attachment', function () {
		readURL(this);
	});
	
	function menganu(){
		var slug = convertToSlug($('input[name="judul"]').val())
		console.log('slug',slug);
		$('input[name="slug"]').val(slug);
		console.log('asdas',$('input[name="slug"]').val(slug));
	}
	function convertToSlug(Text){
		return Text
		.toLowerCase()
		.replace(/ /g,'-')
		.replace(/[^\w-]+/g,'')
		;
	}
</script>
@append
@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	<div class="ui attached top segment">
		{!! csrf_field() !!}
		<input type="hidden" name="posisi" value="0">
		<div class="ui grid">
			<div class="eleven wide column">
				<div class="field">
					<div class="ui small labeled input">
						<label for="slug" class="ui label">{{ url('layanan') }}/</label>
						<input type="text" placeholder="Slug" name="slug" value="" readonly>
					</div>
				</div>
				<div id="tab_layanan">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="layanan_id">ID</a>
						<a class="item" data-tab="layanan_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="layanan_id">
						<div class="ui huge form field">
							<input type="text" name="judul" placeholder="Nama Pelayanan"  onkeyup="menganu()">
						</div>
						<div class="field">
							<textarea name="konten" rows="3" class="editor"></textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="layanan_en">
						<div class="ui huge form field">
							<input type="text" name="judul_en" placeholder="Nama Pelayanan (EN)">
						</div>
						<div class="field">
							<textarea name="konten_en" rows="3" class="editor"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="five wide column">
				<input type="hidden" name="layanan_unggulan" value="0">
				<input type="hidden" name="home" value="0">
				{{-- <div class="ui segment">
					<div class="field">
						<div class="ui checkbox">
							<input type="checkbox" class="ui checkbox" value="1" name="layanan_unggulan">
							<label>Layanan ini merupakan Layanan Unggulan</label>
						</div>
					</div>
				</div> --}}
				<div class="field image-container">
					{{-- <label for="">Gambar</label> --}}
					<img class="image-preview"  style="height: 15rem; object-fit: cover" id="showAttachment" src="http://fsm.undip.ac.id//assets/attachments/Images/default.jpg">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Gambar Cover">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" multiple="">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				<div class="field">
					<label for="">Berkas Terlampir</label>
					<div class="ui fluid file input action">
						<input type="text" placeholder="Klik 'Cari' untuk mengunggah berkas tambahan (Opsional)" readonly="">
						<input type="file" class="ten wide column" name="files" autocomplete="off" accept=".pdf,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				<div id="tab_deskripsi">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="deskripsi_id">ID</a>
						<a class="item" data-tab="deskripsi_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="deskripsi_id">
						<div class="field">
							<label for="">Deskripsi Singkat</label>
							<textarea name="keterangan" style="margin-top: 0px; margin-bottom: 0px; height: 10rem;" rows="3" placeholder="Keterangan"></textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="deskripsi_en">
						<div class="field">
							<label for="">Deskripsi Singkat</label>
							<textarea name="keterangan_en" style="margin-top: 0px; margin-bottom: 0px; height: 10rem;" rows="3" placeholder="Keterangan (EN)"></textarea>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<button class="ui right labeled blue icon save page button">
					<i class="save icon"></i>
					Simpan
				</button>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('#tab_layanan .menu .item').tab();	
		$('#tab_deskripsi .menu .item').tab();	


		$('[name=judul]').on('change, keyup', function(event) {
			var judul = $(this).val();
			$('[name=slug]').val(slugify(judul));
		});
	});
</script>
@endsection