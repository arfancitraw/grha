@extends('layouts.grid')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('filters')
<div class="field">
	<input name="filter[nama]" placeholder="nama" type="text">
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nama: 'empty',
		sub_nama: 'empty',
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
		$('.editor').summernote({
			height: '200px'
		});

		// $(document).on('click', '.button.drafting', function(e){
		// 	$('#status').val(0);
		// 	$('#saveButton').trigger('click');
		// });

		// $(document).on('click', '.button.publicity', function(e){
		// 	$('#status').val(1);
		// 	$('#saveButton').trigger('click');
		// });
	};
</script>
@endsection