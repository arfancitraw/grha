<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Direksi untuk Mobile</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="status" id="status">
		<div class="ui grid">
			<div class="six wide column">
				<div class="field image-container">
					<label for="nama">Photo</label>
					<span class="image-preview" style="height: 18rem;">
						<img class="image-preview" id="showAttachment" style="height: 18rem" src="{{ asset('img/default.jpg') }}">
					</span>
					<div class="ui fluid file input action">
						<input type="text" readonly="">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" accept="image/*">
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</div>
			<div class="ten wide column">
				<div class="ui big form">
					<div class="field">
						<label for="nama">Nama</label>
						<input type="text" name="nama" placeholder="Nama" value="">
					</div>
				</div>
				<br />
				<div class="two fields">
					<div class="field">
						<label>Jabatan</label>
						<input placeholder="Jabatan" name="jabatan" type="text">
					</div>
					<div class="field">
						<label>Urutan</label>
						<input name="urutan" type="number">
					</div>
				</div>
				<div class="field">
					<label for="">Deskripsi</label>
					<textarea name="deskripsi" style="height: 103px; resize: none;"></textarea>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui buttons">
		<button type="button" class="ui button save as publicity teal">Publish</button>
	</div>
</div>