<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Home Slider</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<input type="hidden" name="status" id="status" value="{{ $record->status }}">

		<div class="ui grid">
			<div class="six wide column">
				<div class="field image-container">
					<label for="nama">Photo</label>
					<span class="image-preview" style="height: 18rem;border:none">
						@if($record->photo)
						<img class="image-preview" id="showAttachment" style="height: 18rem;border:none" src="{{ asset('storage') }}/{{ $record->photo }}">
						@else
						<img class="image-preview" id="showAttachment" style="height: 18rem" src="{{ asset('img/default.jpg') }}">
						@endif
					</span>
					<div class="ui fluid file input action">
						<input type="text" readonly="">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" accept="image/*">
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</div>
			<div class="ten wide column">
				<div class="ui big form">
					<div class="field">
						<label for="nama">Nama</label>
						<input type="text" name="nama" value="{{$record->nama}}" value="">
					</div>
				</div>
				<br />
				<div class="two fields">
					<div class="field">
						<label>Jabatan</label>
						<input value="{{ $record->jabatan }}" name="jabatan" type="text">
					</div>
					<div class="field">
						<label>Urutan</label>
						<input name="urutan" value="{{ $record->urutan }}" type="number">
					</div>
				</div>
				<div class="field">
					<label for="">Deskripsi</label>
					<textarea name="deskripsi" style="height: 103px; resize: none;">{{$record->deskripsi}}</textarea>
				</div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui buttons">
		<button type="button" class="ui button save as publicity teal">Publish</button>
	</div>
</div>