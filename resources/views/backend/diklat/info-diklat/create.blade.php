@extends('layouts.grid')

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	
	<input type="hidden" name="slug" readonly placeholder="Judul Pelayanan">
	<div class="ui top attached segment" id="inner2bottom">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div id="tab_info_diklat">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="info_diklat_id">ID</a>
						<a class="item" data-tab="info_diklat_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="info_diklat_id">
						<div class="ui huge form field">
							<input type="text" name="judul" placeholder="Judul">
						</div>
						<div class="field" >
							<textarea name="konten" rows="3" class="editor"></textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="info_diklat_en">
						<div class="ui huge form field">
							<input type="text" name="judul_en" placeholder="Judul (EN)">
						</div>
						<div class="field" >
							<textarea name="konten_en" rows="3" class="editor"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '155px';
		tinymceOpt.placeholder = "Konten...";
		tinymce.init(tinymceOpt);

		$('#tab_info_diklat .menu .item').tab();	
	});
</script>
@endsection