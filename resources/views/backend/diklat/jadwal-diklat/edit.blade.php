@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
	    <input type="hidden" name="id" value="{{ $record->id }}">	
		<div class="ui attached top segment">
		
		<div class="ui grid">
			<div class="nine wide column">
				<div id="tab_jadwal_diklat">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="jadwal_diklat_id">ID</a>
						<a class="item" data-tab="jadwal_diklat_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="jadwal_diklat_id">
						<div class="field">
							<label for="nama">Judul</label>
							<input type="text" name="judul" placeholder="Judul" value="{{ $record->judul }}" onkeyup="menganu()">
						</div>
						<div class="field" >
							<label>Kontent</label>
							<textarea name="konten" rows="8" class="editor" placeholder="Konten" style="height: 301px; resize:none;">{!! $record->konten !!}</textarea>
						</div>
						<div class="field">
							<label>Keterangan</label>
							<textarea name="keterangan" rows="4" placeholder="Keterangan" style="height: 100px; margin-top: 1rem; resize:none;">{!! $record->keterangan !!}</textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="jadwal_diklat_en">
						<div class="field">
							<label for="nama">Judul</label>
							<input type="text" name="judul_en" placeholder="Judul (EN)" value="{{ $record->judul_en }}">
						</div>
						<div class="field" >
							<label>Kontent</label>
							<textarea name="konten_en" rows="8" class="editor" placeholder="Konten" style="height: 301px; resize:none;">{!! $record->konten_en !!}</textarea>
						</div>
						<div class="field">
							<label>Keterangan</label>
							<textarea name="keterangan_en" rows="4" placeholder="Keterangan" style="height: 100px; margin-top: 1rem; resize:none;">{!! $record->keterangan_en !!}</textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="seven wide column">
				<div class="two fields">
					<div class="date field">
						<label>Tanggal Diklat</label>
						<input type="text" class="five wide column" name="tanggal" value="{{$record->tanggal}}" >
					</div>
					<div class="time field">
						<label>&nbsp;</label>
						<input type="text" class="five wide column" name="jam" value="{{$record->jam}}" >
					</div>
				</div>
				<div class="field image-container">
					<label>Foto</label>
					<img class="image-preview" style="height: 371px; object-fit: cover;" id="showAttachment" src="{{ asset('storage') }}/{{ $record->gambar}}">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Pilih foto sampul">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" accept="image/*">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '168px';
		tinymce.init(tinymceOpt);
		$('#tab_jadwal_diklat .menu .item').tab();	

	});
	$('.date').calendar({
		type: 'date',
		formatter: {
			date: function(date, setting) {
				var year = date.getFullYear();
				var month = date.getMonth() + 1;
				var day = date.getDate();
				if (month < 10) {
					month = '0' + month;
				}
				if (day < 10) {
					day = '0' + day;
				}
				return year + '-' + month + '-' + day
			}
		}
	})
	$('.time').calendar({
		type: 'time',
		ampm: false
	})
</script>
@endsection

