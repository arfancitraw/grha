@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/select2/select2.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js')}}"></script>
<script src="{{ asset('plugins/select2/select2.js')}}"></script>

@append

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		$('.menu .item').tab();
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '200px';
		tinymce.init(tinymceOpt);
		$('.select2').select2();
	});


	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@append
@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="ui top attached segment" style="padding: 20px 15px 10px;">
		<div class="ui equal width grid">			
			<div class="column">
				<div class="field">
					<div class="ui big input">
					  <input name="judul" placeholder="Judul / Nama Rujukan Nasional" type="text">
					</div>
				</div>
				<div class="field image-container">
					<img class="image-preview"  style="height: 180px; object-fit: cover; background: #fff" style="height: 27rem" src="{{ asset('img/default.jpg') }}">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Cover Photo Rujukan Nasional">
						<input type="file" class="ten wide column attachment" name="photo" autocomplete="off" accept="image/*">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</div>
			<div class="column">
				<div class="field">
					<div class="ui top attached segment" style="padding: 10px;">
						<div class="ui checkbox">
							<input type="hidden" name="home" value="0">
							<input type="checkbox" class="ui checkbox" value="1" name="home" >
							<label>Layanan ini akan tampil di beranda <small style="font-weight: bold">(Termasuk Layanan Unggulan)</small></label>
						</div>
					</div>
				</div>
				<div class="fields">
					<div class="five wide field image-container">
						<label for="">Icon</label>
						<img class="image-preview"  style="height: 155px; object-fit: contain; background: #fff" style="height: 27rem" src="{{ asset('img/default.jpg') }}">
						<div class="ui fluid file input action">
							<input type="text" readonly="">
							<input type="file" class="ten wide column attachment" name="icon" autocomplete="off" accept="image/*">

							<div class="ui blue button file">
								Cari...
							</div>
						</div>
					</div>
					<div class="eleven wide field">
						<label for="">Deskiprsi Singkat</label>
						<textarea name="deskripsi" id="" class="short-desc" style="height: 187px; resize: none;" placeholder="Deskripsi Singkat"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="ui attached segment">
		<div class="ui pointing secondary menu">
	        <a class="active item" data-tab="pengantar">Pengantar</a>
	        <a class="item" data-tab="profil">Profil</a>
	        <a class="item" data-tab="produk">Produk Layanan</a>
	        <a class="item" data-tab="tim">Tim Layanan Unggulan</a>
	        <a class="item" data-tab="fasilitas">Fasilitas Pelayanan</a>
	        <a class="item" data-tab="penelitian">Penelitian & Publikasi</a>
	        <a class="item" data-tab="jejaring">Jejaring RS Mitra</a>
	        <a class="item" data-tab="agenda">Agenda Kegiatan Ilmiah</a>
	        <a class="item" data-tab="aksesibilitas">Aksesibilitas</a>
	        <a class="item" data-tab="cara-merujuk">Cara Merujuk</a>
		</div>

		<div class="ui active tab basic segment" data-tab="pengantar" style="padding:0">
			<p>Input data pengantar dalam bentuk html disini :</p>
			<textarea name="pengantar" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="profil" style="padding:0">
			<p>Input data profil dalam bentuk html disini :</p>
			<textarea name="profile" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="produk" style="padding:0">
			<p>Input data produk dalam bentuk html disini :</p>
			<textarea name="produk" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="tim" style="padding:0">
			@include('backend.rujukan-nasional.forms.tim')
		</div>	
		<div class="ui tab basic segment" data-tab="fasilitas" style="padding:0">
			<p>Input data fasilitas dalam bentuk html disini :</p>
			<textarea name="fasilitas" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="penelitian" style="padding:0">
			<p>Input data penelitian dalam bentuk html disini :</p>
			<textarea name="penelitian" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="jejaring" style="padding:0">
			<p>Input data jejaring dalam bentuk html disini :</p>
			<textarea name="jejaring" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="agenda" style="padding:0">
			<p>Input data agenda dalam bentuk html disini :</p>
			<textarea name="agenda" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="aksesibilitas" style="padding:0">
			<p>Input data aksesibilitas dalam bentuk html disini :</p>
			<textarea name="aksesibilitas" class="editor"></textarea>
		</div>	
		<div class="ui tab basic segment" data-tab="cara-merujuk" style="padding:0">
			<p>Input data cara merujuk dalam bentuk html disini :</p>
			<textarea name="cara_merujuk" class="editor"></textarea>
		</div>
	</div>

	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui buttons">
					<button type="button" class="ui button saves as drafting page orange">Draft</button>
					<div class="or"></div>
					<button type="button" class="ui button saves as publicity page teal">Publish</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('rules')
<script type="text/javascript">
	formRules = {
		judul: 'empty',
		deskripsi: 'empty',
		photo: 'empty',
	};
</script>
@endsection