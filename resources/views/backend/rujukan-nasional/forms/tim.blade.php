@section('js')
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.js') }}" type="text/javascript"></script>
@append

<div class="ui equal width grid">
	<div class="column">
	
		<div id="tab_tim">
			<div class="ui top attached tabular menu">
				<p>Input deskripsi tim dalam bentuk html disini :</p>
				<div class="right menu">
					<a class="item active" data-tab="tim_id">ID</a>
					<a class="item" data-tab="tim_en">EN</a>
				</div>
			</div>
			<div class="ui bottom attached tab segment active" data-tab="tim_id" style="margin-bottom:0">
				<textarea name="tim" class="editor">{!! $record->tim or '' !!}</textarea>
			</div>
			<div class="ui bottom attached tab segment" data-tab="tim_en" style="margin-bottom:0">
				<textarea name="tim_en" placeholder="Produk (EN)" class="editor">{!! $record->tim_en or '' !!}</textarea>
			</div>
		</div>
	</div>
	<div class="column">
		<h4 class="ui dividing header">Tim Dokter</h4>
		<div class="scroll-wrap">
			<table class="ui celled padded condensed table">
				<thead>	
					<tr>
						<th class="center aligned">Pilih Dokter</th>
						<th class="center aligned" style="width: 45%">Posisi</th>
						<th class="center aligned" style="width: 70px">Aksi</th>
					</tr>
				</thead>
				<tbody class="sub container">
					@if(isset($record) && count($record->dokter) > 0)
						@php $i = 0; @endphp
						@foreach ($record->dokter as $dokter)
						<tr data-count="{{$i}}">
							<td>
								<input type="hidden" name="subid[]" value="{{ $dokter->id }}">
								<input type="hidden" name="dokter[{{$i}}][id]" value="{{ $dokter->id }}">
								<select name="dokter[{{$i}}][id_dokter]" class="select2">
									{!! \App\Models\Dokter\ListDokter::options('nama_lengkap', 'id', ['selected' => $dokter->id]) !!}
								</select>
							</td>
							<td class="center aligned">
								<input type="text" name="dokter[{{$i}}][posisi]" placeholder="misal: Kepala Tim, Spesialis Saraf, d.l.l" value="{{ $dokter->pivot->posisi }}">
							</td>
							<td class="center aligned">
								@if($i == 0)
								<button type="button" class="ui green icon button add-sub" data-content="Tambah Tim Dokter">
									<i class="plus icon"></i>
								</button>
								@else
								<button type="button" class="ui red icon button remove-sub" data-content="Hapus Tim Dokter">
									<i class="delete icon"></i>
								</button>
								@endif
							</td>
						</tr>
						@php $i++; @endphp
						@endforeach
					@else
						<tr data-count="0">
							<td>
								<select name="dokter[0][id_dokter]" class="select2">
									{!! \App\Models\Dokter\ListDokter::options('nama_lengkap', 'id', []) !!}
								</select>
							</td>
							<td class="center aligned">
								<input type="text" name="dokter[0][posisi]" placeholder="misal: Kepala Tim, Spesialis Saraf, d.l.l">
							</td>
							<td class="center aligned">
								<button type="button" class="ui green icon button add-sub" data-content="Tambah Tim Dokter">
									<i class="plus icon"></i>
								</button>
							</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>

@section('scripts')
<script>
	$(document).ready(function($) {
		$('.ui.checkbox').checkbox();
		$('[data-content]').popup({
			hoverable: true,
			position : 'top center',
			delay: {
				show: 300,
				hide: 800
			}
		});

		$('.scroll-wrap').slimScroll({
			height: '290px',
		});
		
		$(document).on('click', '.remove-sub', function(event) {
			$(this).closest('tr').remove();
		});

		$('.add-sub').click(function(event) {
			var idx = parseInt($('.sub.container tr').last().data('count')) + 1;
			var html = `<tr data-count="`+idx+`">
				<td>
					<select name="dokter[`+idx+`][id_dokter]" class="select2">
						{!! \App\Models\Dokter\ListDokter::options('nama_lengkap', 'id', []) !!}
					</select>
				</td>
				<td class="center aligned">
					<input type="text" name="dokter[`+idx+`][posisi]" placeholder="misal: Kepala Tim, Spesialis Saraf, d.l.l">
				</td>
				<td class="center aligned">
					<button type="button" class="ui red icon button remove-sub" data-content="Hapus Tim Dokter">
						<i class="delete icon"></i>
					</button>
				</td>
			</tr>`;

			$('.sub.container').append(html);

			$('.ui.checkbox').checkbox();
			$('[data-content]').popup({
				hoverable: true,
				position : 'top center',
				delay: {
					show: 300,
					hide: 800
				}
			});

			$('.select2').select2();
		});
	});	
</script>
@append