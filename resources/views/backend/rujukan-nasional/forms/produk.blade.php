<table class="ui celled table listTable layanan">
	<thead class="center aligned">
		<tr>
			<th class="one wide column">No</th>
			<th class="six wide column">Detail</th>
			<th>Deskripsi</th>
			<th class="one wide column"><button type="button" data-content="Tambah Data" data-id="" class="ui mini green icon produk-layanan button"><i class="plus icon"></i></button></th>
		</tr>
	</thead>
	<tbody id="ListProdukLayanan">
		<tr>
			<td class="ui center aligned">
				<label class="ui center aligned header number-produk-layanan">1</label>
			</td>
			<td>
				<div class="ui grid field" style="width: 100%">
					<input type="text" name="produk[nama][]" placeholder="Nama" maxlength="65">
				</div>
				<div class="field image-container">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Pilih Gambar">
						<input type="file" class="ten wide column" id="attachments" placeholder="Pilih Gambar" name="produk[gambar][]" autocomplete="off" accept="image/*">
						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</td>
			<td>
				<textarea name="produk[keterangan][]" cols="100%" style="height: 60px !important; resize:none;"></textarea>
			</td>
			<td class="center aligned"><button type='button' data-content='Hapus Data' class='ui mini red icon modal-produk-delete button'><i class='trash icon'></i></button></td>
		</tr>
	</tbody>
</table>