@extends('layouts.grid')

@section('filters')
<div class="field">
	<input name="filter[nama]" placeholder="Nama" type="text">
</div>

<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection




@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
<a class="ui blue button add-page">
	<i class="plus icon"></i> Tambah Data
</a>
@endif
@endsection



@section('js-filters')
d.nama = $("input[name='filter[nama]']").val();
d.status = $("select[name='filter[status]']").val();
console.log(d.status)
@endsection

@section('init-modal')
<script>
	initModal = function(){
	  tinymceOpt.selector = '.editor';
	  tinymceOpt.height = '200px';
	  tinymceOpt.placeholder = "Konten profil...";
	  tinymce.init(tinymceOpt);
	};
</script>
@endsection