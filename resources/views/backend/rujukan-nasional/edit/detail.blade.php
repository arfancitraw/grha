
<div class="active title">
	<i class="dropdown icon"></i>
	Pengantar
</div>
<div class="active content">
	<div class="transition">
		<div class="ui field">
			<div class="ui big input">
				<textarea name="pengantar" id="" class="editor">{{$record->pengantar}}</textarea>
			</div>
		</div>
		
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Profil
</div>
<div class="content">
	<div class="transition hidden">
		<div class="ui field">
			<div class="ui big input">
				<textarea name="profile" id="" class="editor">{{$record->profile}}</textarea>
			</div>
		</div>
		
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Produk Layanan Unggulan
</div>
<div class="content">
	<div class="transition hidden">
		<table class="ui celled table listTable layanan">
			<thead class="center aligned">
				<tr>
					<th class="one wide column">No</th>
					<th class="six wide column">Detail</th>
					<th>Deskripsi</th>
					<th class="one wide column"><button type="button" data-content="Tambah Data" data-id="" class="ui mini green icon produk-layanan button"><i class="plus icon"></i></button></th>
				</tr>
			</thead>
			<tbody id="ListProdukLayanan">
				@foreach($record->Produk as $NilaiProduk => $ValueProduk)
				<tr>
					<td class="ui center aligned">
						<label class="ui center aligned header number-produk-layanan">{{$NilaiProduk+1}}</label>
					</td>
					<td>
						<div class="ui grid field" style="width: 100%">
							<input type="hidden" name="produk[id][]" placeholder="id" value="{{$ValueProduk->id}}" maxlength="65">
							<input type="text" name="produk[nama][]" placeholder="Nama" value="{{$ValueProduk->nama}}" maxlength="65">
						</div>
						<div class="field image-container">
							
							<div class="ui fluid file input action">
								<input type="text" readonly="" name="produk[gambar_asli][]" value="{{$ValueProduk->gambar}}" placeholder="Pilih Gambar">
								<input type="file" class="ten wide column" id="attachments" placeholder="Pilih Gambar" name="produk[gambar][]" autocomplete="off" value="{{$ValueProduk->gambar}}" accept="image/*">
								<div class="ui blue button file">
									Cari...
								</div>
							</div>
						</div>
					</td>
					<td>
						<div class="ui grid field" style="width: 100%">
							<textarea name="produk[keterangan][]" cols="100%" rows="5">{{$ValueProduk->keterangan}}</textarea>
						</div>
					</td>
					<td class="center aligned"><button type='button' data-content='Hapus Data' class='ui mini red icon modal-produk-delete button'><i class='trash icon'></i></button></td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
			</tfoot>
		</table>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Tim Layanan Unggulan
</div>
<div class="content">
	<div class="transition hidden">
		<table class="ui celled table listTable tim">
			<thead class="center aligned">
				<tr>
					<th class="one wide column">No</th>
					<th class="six wide column">Dokter</th>
					<th class="three wide column">Posisi</th>
					<th class="one wide column"><button type="button" data-content="Tambah Data" data-id="" class="ui mini green icon tim button"><i class="plus icon"></i></button></th>
				</tr>
			</thead>
			<tbody id="LayananTim">
				
				@foreach($record->Dokter as $NilaiDokter => $ValueDokter)

				<tr>
					<td class="ui center aligned">
						<label class="ui center aligned header number-tim-layanan">{{$NilaiDokter+1}}</label>
					</td>
					<td>
						<select name="tim[id_dokter][]" id="" class="ui sixten wide column search dropdown">
							{!! \App\Models\Dokter\ListDokter::options('nama', 'id', ['selected' => $ValueDokter->id ],"Pilih Dokter") !!}
						</select>
					</td>
					<td>
						<select name="tim[posisi][]" id="" class="ui sixten wide column search dropdown">
							<option value="">-- Pilih Posisi --</option>
							<option value="1" {{ $record->tim[$NilaiDokter]->posisi == '1' ? 'selected="selected"' : '' }}>Ketua</option>
							<option value="2" {{ $record->tim[$NilaiDokter]->posisi == '2' ? 'selected="selected"' : '' }}>Wakil Ketua</option>
							<option value="3" {{ $record->tim[$NilaiDokter]->posisi == '3' ? 'selected="selected"' : '' }}>Sekertaris / Kordinator</option>
							<option value="4" {{ $record->tim[$NilaiDokter]->posisi == '4' ? 'selected="selected"' : '' }}>Anggota</option>
						</select>
					</td>
					<td class="center aligned"><button type='button' data-content='Hapus Data' class='ui mini red icon modal-tim-delete button'><i class='trash icon'></i></button></td>
				</tr>
				
				@endforeach
				
			</tbody>
			<tfoot>
			</tfoot>
		</table>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Penelitian &amp; Publikasi
</div>
<div class="content">
	<div class="transition hidden">
		<textarea name="penelitian" id="" class="editor">{{$record->penelitian}}</textarea>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Jejaring RS Mitra
</div>
<div class="content">
	<div class="transition hidden">
		<textarea name="jejaring" id="" class="editor">{{$record->jejaring}}</textarea>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Agenda Kegiatan
</div>
<div class="content">
	<div class="transition hidden">
		<table class="ui celled table listTable agenda">
			<thead class="center aligned">
				<tr>
					<th class="one wide column">No</th>
					<th class="six wide column">Judul</th>
					<th>Deskripsi</th>
					<th class="one wide column"><button type="button" data-content="Tambah Data" data-id="" class="ui mini green icon agenda button"><i class="plus icon"></i></button></th>
				</tr>
			</thead>
			<tbody id="LayananAgenda">
				@foreach($record->Agenda as $NilaiAgenda => $ValueAgenda)
				<tr>
					<td class="ui center aligned">
						<label class="ui center aligned header number-agenda-layanan">{{$NilaiAgenda+1}}</label>

					</td>
					<td>
						<div class="ui grid field" style="width: 100%">
							<input type="hidden" name="agenda[id][]" placeholder="id" value="{{$ValueAgenda->id}}" maxlength="65">
							<input type="text" name="agenda[nama][]" placeholder="Nama Agenda" maxlength="65" value="{{$ValueAgenda->nama}}">
						</div>
						<div class="ui grid field year" style="width: 100%">
							<input type="text" name="agenda[tahun][]" value="{{$ValueAgenda->tahun}}" placeholder="Tahun Periode">
						</div>
					</td>
					<td><textarea name="agenda[keterangan][]" cols="100%" rows="5">{{$ValueAgenda->keterangan}}</textarea></td>
					<td class="center aligned"><button type='button' data-content='Hapus Data' data-id="1" class='ui mini red icon modal-agenda-delete button'><i class='trash icon'></i></button></td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
			</tfoot>
		</table>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Aksesibilitas
</div>
<div class="content">
	<div class="transition hidden">
		<div class="field">
			<input type="text" name="kordinat" value="{{$record->kordinat}}" placeholder="Koordinat">
		</div>
		<div class="field">
			<textarea name="alamat" cols="100%" rows="4" placeholder="Alamat">{{$record->alamat}}</textarea>
		</div>
		<div class="field">
			<input type="text" name="no_tlpn" value="{{$record->no_tlpn}}" placeholder="Telepon">
		</div>
		<div class="field">
			<input type="email" name="email" value="{{$record->email}}" placeholder="Email">
		</div>
		<div class="field">
			<textarea name="akses_deskripsi" id="" cols="30" rows="10" class="short-desc"  placeholder="Deskripsi Singkat">{{$record->deskripsi}}</textarea>
		</div>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Cara Merujuk
</div>
<div class="content">
	<div class="transition hidden">
		<textarea name="cara_merujuk" id="" class="editor">{{$record->cara_merujuk}}</textarea>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Fasilitas
</div>
<div class="content">
	<div class="transition hidden">
		<div class="field">
			<input type="text" value="{{$record->fasilitas_nama}}" name="fasilitas_nama" placeholder="Nama Pelayanan">
		</div>
		<div class="ui field">
			<div class="ui big input">
				<textarea name="fasilitas_pelayanan" placeholder="Persiapan" id="" class="editor">{{ $record->fasilitas_pelayanan }}</textarea>
			</div>
		</div>
		
	</div>
</div>


















