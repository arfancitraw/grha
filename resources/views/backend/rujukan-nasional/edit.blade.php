@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/select2/select2.css') }}">
@append

@section('styles')
	<style type="text/css">
	.select2-container--default .select2-selection--single{
		border-radius: .28571429rem !important;
		height: 32px !important;
	}
	</style>
@append

@section('js')
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js')}}"></script>
<script src="{{ asset('plugins/select2/select2.js')}}"></script>
@append

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		$('.menu .item').tab();	
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '200px';
		tinymce.init(tinymceOpt);
		$('.select2').select2();
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@append
@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="status" id="status">
	<input type="hidden" name="id" value="{{$record->id}}">
	<div class="ui top attached segment" style="padding: 20px 15px 10px;">
		<div class="ui equal width grid">			
			<div class="column">
				<div id="tab_judul" style="margin-bottom:10px">
					<div class="ui top attached tab segment active" data-tab="judul_id">		
						<div class="ui big transparent fluid input">
							<input name="judul" placeholder="Judul / Nama Rujukan Nasional" type="text" value="{{ $record->judul or '' }}">
						</div>
					</div>
					<div class="ui top attached tab segment" data-tab="judul_en" style="margin-top:0">		
						<div class="ui big transparent fluid input">
							<input name="judul_en" placeholder="Judul / Nama Rujukan Nasional (EN)" type="text" value="{{ $record->judul_en or '' }}">
						</div>
					</div>
					<div class="ui bottom attached tabular menu">
						<a class="item active" data-tab="judul_id">
							ID
						</a>
						<a class="item" data-tab="judul_en">
							EN
						</a>
						<div class="right item" style="padding-right:0">
							<b>Judul / Nama Rujukan Nasional</b>
						</div>
					</div>		
				</div>
				<div class="field image-container">
					<img class="image-preview"  style="height: 138px; object-fit: cover; background: #fff" 
						@if($photo = $record->photo)
						src="{{ url('storage/'.$photo) }}"
						@else
						src="{{ asset('img/default.jpg') }}"
						@endif>
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Cover Photo Rujukan Nasional">
						<input type="file" class="ten wide column attachment" name="photo" autocomplete="off" accept="image/*">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</div>
			<div class="column">
				<div class="field">
					<div class="ui top attached segment" style="padding: 10px;">
						<div class="ui checkbox">
							<input type="hidden" name="home" value="0">
							<input type="checkbox" class="ui checkbox" value="1" name="home" @if($record->home) checked @endif>
							<label>Layanan ini akan tampil di beranda <small style="font-weight: bold">(Termasuk Layanan Unggulan)</small></label>
						</div>
					</div>
				</div>
				<div class="fields">
					<div class="five wide field image-container">
						<label for="">Icon</label>
						<img class="image-preview"  style="height: 155px; object-fit: contain; background: #fff" style="height: 27rem" 
							@if($icon = $record->icon)
							src="{{ url('storage/'.$icon) }}"
							@else
							src="{{ asset('img/default.jpg') }}"
							@endif>
						<div class="ui fluid file input action">
							<input type="text" readonly="">
							<input type="file" class="ten wide column attachment" name="icon" autocomplete="off" accept="image/*">

							<div class="ui blue button file">
								Cari...
							</div>
						</div>
					</div>
					<div class="eleven wide field">
						<div id="tab_deskripsi">
							<div class="ui top attached tabular menu">
								<label style="font-weight:bold">Deskripsi Singkat</label>
								<div class="right menu">
									<a class="item active" data-tab="deskripsi_id">ID</a>
									<a class="item" data-tab="deskripsi_en">EN</a>
								</div>
							</div>
							<div class="ui bottom attached tab segment active" data-tab="deskripsi_id" style="margin-bottom:0">
								<div class="ui transparent input">
									<textarea name="deskripsi" placeholder="Deskripsi Singkat" style="height: 144px; padding:0; border:none; resize: none">{!! $record->deskripsi or '' !!}</textarea>
								</div>
							</div>
							<div class="ui bottom attached tab segment" data-tab="deskripsi_en" style="margin-bottom:0">
								<div class="ui transparent input">
									<textarea name="deskripsi_en" placeholder="Deskripsi Singkat (EN)" style="height: 144px; padding:0; border:none; resize: none">{!! $record->deskripsi_en or '' !!}</textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="ui attached segment">
		<div class="ui pointing secondary menu">
	        <a class="active item" data-tab="pengantar">Pengantar</a>
	        <a class="item" data-tab="profil">Profil</a>
	        <a class="item" data-tab="produk">Produk Layanan</a>
	        <a class="item" data-tab="tim">Tim Layanan Unggulan</a>
	        <a class="item" data-tab="fasilitas">Fasilitas Pelayanan</a>
	        <a class="item" data-tab="penelitian">Penelitian & Publikasi</a>
	        <a class="item" data-tab="jejaring">Jejaring RS Mitra</a>
	        <a class="item" data-tab="agenda">Agenda Kegiatan Ilmiah</a>
	        <a class="item" data-tab="aksesibilitas">Aksesibilitas</a>
	        <a class="item" data-tab="cara-merujuk">Cara Merujuk</a>
		</div>

		<div class="ui active tab basic segment" data-tab="pengantar" style="padding:0">
			<div id="tab_pengantar">
				<div class="ui top attached tabular menu">
					<p>Input data pengantar dalam bentuk html disini :</p>
					<div class="right menu">
						<a class="item active" data-tab="pengantar_id">ID</a>
						<a class="item" data-tab="pengantar_en">EN</a>
					</div>
				</div>
				<div class="ui bottom attached tab segment active" data-tab="pengantar_id" style="margin-bottom:0">
					<textarea name="pengantar" class="editor">{!! $record->pengantar or '' !!}</textarea>
				</div>
				<div class="ui bottom attached tab segment" data-tab="pengantar_en" style="margin-bottom:0">
					<textarea name="pengantar_en" class="editor">{!! $record->pengantar_en or '' !!}</textarea>
				</div>
			</div>
		</div>	
		<div class="ui tab basic segment" data-tab="profil" style="padding:0">
			<div id="tab_profil">
				<div class="ui top attached tabular menu">
					<p>Input data profil dalam bentuk html disini :</p>
					<div class="right menu">
						<a class="item active" data-tab="profil_id">ID</a>
						<a class="item" data-tab="profil_en">EN</a>
					</div>
				</div>
				<div class="ui bottom attached tab segment active" data-tab="profil_id" style="margin-bottom:0">
					<textarea name="profile" class="editor">{!! $record->profile or '' !!}</textarea>
				</div>
				<div class="ui bottom attached tab segment" data-tab="profil_en" style="margin-bottom:0">
					<textarea name="profile_en" class="editor">{!! $record->profile_en or '' !!}</textarea>
				</div>
			</div>
		</div>	
		<div class="ui tab basic segment" data-tab="produk" style="padding:0">
			<div id="tab_produk">
				<div class="ui top attached tabular menu">
					<p>Input data produk dalam bentuk html disini :</p>
					<div class="right menu">
						<a class="item active" data-tab="produk_id">ID</a>
						<a class="item" data-tab="produk_en">EN</a>
					</div>
				</div>
				<div class="ui bottom attached tab segment active" data-tab="produk_id" style="margin-bottom:0">
					<textarea name="produk" class="editor">{!! $record->produk or '' !!}</textarea>
				</div>
				<div class="ui bottom attached tab segment" data-tab="produk_en" style="margin-bottom:0">
					<textarea name="produk_en" class="editor">{!! $record->produk_en or '' !!}</textarea>
				</div>
			</div>
		</div>	
		<div class="ui tab basic segment" data-tab="tim" style="padding:0">
			@include('backend.rujukan-nasional.forms.tim')
		</div>
		<div class="ui tab basic segment" data-tab="fasilitas" style="padding:0">
			<div id="tab_fasilitas">
				<div class="ui top attached tabular menu">
					<p>Input data fasilitas dalam bentuk html disini :</p>
					<div class="right menu">
						<a class="item active" data-tab="fasilitas_id">ID</a>
						<a class="item" data-tab="fasilitas_en">EN</a>
					</div>
				</div>
				<div class="ui bottom attached tab segment active" data-tab="fasilitas_id" style="margin-bottom:0">
					<textarea name="fasilitas" class="editor">{!! $record->fasilitas or '' !!}</textarea>
				</div>
				<div class="ui bottom attached tab segment" data-tab="fasilitas_en" style="margin-bottom:0">
					<textarea name="fasilitas_en" class="editor">{!! $record->fasilitas_en or '' !!}</textarea>
				</div>
			</div>
		</div>	
		<div class="ui tab basic segment" data-tab="penelitian" style="padding:0">
			<div id="tab_penelitian">
				<div class="ui top attached tabular menu">
					<p>Input data penelitian dalam bentuk html disini :</p>
					<div class="right menu">
						<a class="item active" data-tab="penelitian_id">ID</a>
						<a class="item" data-tab="penelitian_en">EN</a>
					</div>
				</div>
				<div class="ui bottom attached tab segment active" data-tab="penelitian_id" style="margin-bottom:0">
					<textarea name="penelitian" class="editor">{!! $record->penelitian or '' !!}</textarea>
				</div>
				<div class="ui bottom attached tab segment" data-tab="penelitian_en" style="margin-bottom:0">
					<textarea name="penelitian_en" class="editor">{!! $record->penelitian_en or '' !!}</textarea>
				</div>
			</div>
		</div>	
		<div class="ui tab basic segment" data-tab="jejaring" style="padding:0">
			<div id="tab_jejaring">
				<div class="ui top attached tabular menu">
					<p>Input data jejaring dalam bentuk html disini :</p>
					<div class="right menu">
						<a class="item active" data-tab="jejaring_id">ID</a>
						<a class="item" data-tab="jejaring_en">EN</a>
					</div>
				</div>
				<div class="ui bottom attached tab segment active" data-tab="jejaring_id" style="margin-bottom:0">
					<textarea name="jejaring" class="editor">{!! $record->jejaring or '' !!}</textarea>
				</div>
				<div class="ui bottom attached tab segment" data-tab="jejaring_en" style="margin-bottom:0">
					<textarea name="jejaring_en" class="editor">{!! $record->jejaring_en or '' !!}</textarea>
				</div>
			</div>
		</div>	
		<div class="ui tab basic segment" data-tab="agenda" style="padding:0">
			<div id="tab_agenda">
				<div class="ui top attached tabular menu">
					<p>Input data agenda dalam bentuk html disini :</p>
					<div class="right menu">
						<a class="item active" data-tab="agenda_id">ID</a>
						<a class="item" data-tab="agenda_en">EN</a>
					</div>
				</div>
				<div class="ui bottom attached tab segment active" data-tab="agenda_id" style="margin-bottom:0">
					<textarea name="agenda" class="editor">{!! $record->agenda or '' !!}</textarea>
				</div>
				<div class="ui bottom attached tab segment" data-tab="agenda_en" style="margin-bottom:0">
					<textarea name="agenda_en" class="editor">{!! $record->agenda_en or '' !!}</textarea>
				</div>
			</div>
		</div>	
		<div class="ui tab basic segment" data-tab="aksesibilitas" style="padding:0">
			<div id="tab_aksesibilitas">
				<div class="ui top attached tabular menu">
					<p>Input data aksesibilitas dalam bentuk html disini :</p>
					<div class="right menu">
						<a class="item active" data-tab="aksesibilitas_id">ID</a>
						<a class="item" data-tab="aksesibilitas_en">EN</a>
					</div>
				</div>
				<div class="ui bottom attached tab segment active" data-tab="aksesibilitas_id" style="margin-bottom:0">
					<textarea name="aksesibilitas" class="editor">{!! $record->aksesibilitas or '' !!}</textarea>
				</div>
				<div class="ui bottom attached tab segment" data-tab="aksesibilitas_en" style="margin-bottom:0">
					<textarea name="aksesibilitas_en" class="editor">{!! $record->aksesibilitas_en or '' !!}</textarea>
				</div>
			</div>
		</div>	
		<div class="ui tab basic segment" data-tab="cara-merujuk" style="padding:0">
			<div id="tab_merujuk">
				<div class="ui top attached tabular menu">
					<p>Input data cara merujuk dalam bentuk html disini :</p>
					<div class="right menu">
						<a class="item active" data-tab="merujuk_id">ID</a>
						<a class="item" data-tab="merujuk_en">EN</a>
					</div>
				</div>
				<div class="ui bottom attached tab segment active" data-tab="merujuk_id" style="margin-bottom:0">
					<textarea name="cara_merujuk" class="editor">{!! $record->cara_merujuk or '' !!}</textarea>
				</div>
				<div class="ui bottom attached tab segment" data-tab="merujuk_en" style="margin-bottom:0">
					<textarea name="cara_merujuk_en" class="editor">{!! $record->cara_merujuk_en or '' !!}</textarea>
				</div>
			</div>
		</div>
	</div>

	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui buttons">
					<button type="button" class="ui button saves as drafting page orange">Draft</button>
					<div class="or"></div>
					<button type="button" class="ui button saves as publicity page teal">Publish</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('rules')
<script type="text/javascript">
	formRules = {
		judul: 'empty',
		deskripsi: 'empty',
		photo: 'empty',
	};
</script>
@endsection