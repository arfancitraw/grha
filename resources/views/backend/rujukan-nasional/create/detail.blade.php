<div class="title">
	<i class="dropdown icon"></i>
	Pengantar
</div>
<div class="content">
	<div class="transition hidden">
		<textarea name="pengantar" id="" class="editor"></textarea>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Profil
</div>
<div class="content">
	<div class="transition hidden">
		<textarea name="profile" id="" class="editor"></textarea>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Produk Layanan Unggulan
</div>
<div class="content">
	<div class="transition hidden">
		@include('backend.rujukan-nasional.forms.produk')
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Tim Layanan Unggulan
</div>
<div class="content">
	<div class="transition hidden">
		<table class="ui celled table listTable tim">
			<thead class="center aligned">
				<tr>
					<th class="one wide column">No</th>
					<th class="six wide column">Dokter</th>
					<th class="three wide column">Posisi</th>
					<th class="one wide column"><button type="button" data-content="Tambah Data" data-id="" class="ui mini green icon tim button"><i class="plus icon"></i></button></th>
				</tr>
			</thead>
			<tbody id="LayananTim">
				<tr>
					<td class="ui center aligned">
						<label class="ui center aligned header number-tim-layanan">1</label>
					</td>
					<td>
						<select name="tim[id_dokter][]" id="" class="ui sixten wide column search dropdown">
							{!! \App\Models\Dokter\ListDokter::options(function ($data) {
								return $data->nama;
							}, 'id',['not-empty' => ['nama']],"Pilih Dokter") !!}
						</select>
					</td>
					<td>
						<select name="tim[posisi][]" id="" class="ui sixten wide column search dropdown">
							<option value="">-- Pilih Posisi --</option>
							<option value="1">Ketua</option>
							<option value="2">Wakil Ketua</option>
							<option value="3">Sekertaris / Kordinator</option>
							<option value="4">Anggota</option>
						</select>
					</td>
					<td class="center aligned"><button type='button' data-content='Hapus Data' class='ui mini red icon modal-tim-delete button'><i class='trash icon'></i></button></td>
				</tr>
			</tbody>
			<tfoot>
			</tfoot>
		</table>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Penelitian &amp; Publikasi
</div>
<div class="content">
	<div class="transition hidden">
			<div class="ui field">
			<div class="ui big input">
				<textarea name="penelitian" id="penelitian" class="editor"></textarea>
			</div>
		</div>
		
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Jejaring RS Mitra
</div>
<div class="content">
	<div class="transition hidden">
		<textarea name="jejaring" id="" class="editor"></textarea>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Agenda Kegiatan
</div>
<div class="content">
	<div class="transition hidden">
		<table class="ui celled table listTable agenda">
			<thead class="center aligned">
				<tr>
					<th class="one wide column">No</th>
					<th class="six wide column">Judul</th>
					<th>Deskripsi</th>
					<th class="one wide column"><button type="button" data-content="Tambah Data" data-id="" class="ui mini green icon agenda button"><i class="plus icon"></i></button></th>
				</tr>
			</thead>
			<tbody id="LayananAgenda">
				<tr>
					<td class="ui center aligned">
						<label class="ui center aligned header number-agenda-layanan">1</label>

					</td>
					<td>
						<div class="ui grid field" style="width: 100%">
							<input type="text" name="agenda[nama][]" placeholder="Nama Agenda" maxlength="65">
						</div>
						<div class="ui grid field year" style="width: 100%">
							<input type="text" name="agenda[tahun][]" placeholder="Tahun Periode">
						</div>
					</td>
					<td><textarea name="agenda[keterangan][]" cols="100%" rows="5"></textarea></td>
					<td class="center aligned"><button type='button' data-content='Hapus Data' data-id="1" class='ui mini red icon modal-agenda-delete button'><i class='trash icon'></i></button></td>
				</tr>
			</tbody>
			<tfoot>
			</tfoot>
		</table>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Aksesibilitas
</div>
<div class="content">
	<div class="transition hidden">
		<div class="field">
			<input type="text" name="kordinat" placeholder="Koordinat">
		</div>
		<div class="field">
			<textarea name="alamat" cols="100%" rows="4" placeholder="Alamat"></textarea>
		</div>
		<div class="field">
			<input type="text" name="no_tlpn" placeholder="Telepon">
		</div>
		<div class="field">
			<input type="email" name="email" placeholder="Email">
		</div>
		<div class="field">
			<textarea name="akses_deskripsi" id="" cols="30" rows="10" class="short-desc"  placeholder="Deskripsi Singkat"></textarea>
		</div>
	</div>
</div>

<div class="title">
	<i class="dropdown icon"></i>
	Cara Merujuk
</div>
<div class="content">
	<div class="transition hidden">
		<div class="ui field">
			<div class="ui big input">
				<textarea name="cara_merujuk" id="" class="editor"></textarea>
			</div>
		</div>
		
	</div>
</div>
<div class="title">
	<i class="dropdown icon"></i>
	Fasilitas
</div>
<div class="content">
	<div class="transition hidden">
		<div class="field">
			<input type="text" name="fasilitas_nama" placeholder="Nama Pelayanan">
		</div>
		<div class="ui field">
			<div class="ui big input">
				<textarea name="fasilitas_pelayanan" placeholder="Persiapan" id="" class="editor"></textarea>
			</div>
		</div>
		
	</div>
</div>
