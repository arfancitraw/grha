@extends('layouts.grid')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('filters')
	<div class="field">
		<input name="filter[nama]" placeholder="Nama Komite Etik" type="text">
	</div>
	<div class="field">
		<select name="filter[jabatan]" class="ui search fluid dropdown">
			{!! \App\Models\Master\JabatanDokter::options('jabatan', 'id', [], 'Jabatan Komite') !!}
		</select>
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('js-filters')
    d.nama = $("input[name='filter[nama]']").val();
    d.jabatan = $("select[name='filter[jabatan]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nama: 'empty',
		konten: 'empty',
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
      $('.editor').summernote({
		  height: '200px'
	  });
	  $('.start.date').calendar({
			type: 'date',
			endCalendar: $('.end.date')
		});
	  $('.end.date').calendar({
			type: 'date',
			startCalendar: $('.start.date')
		});
	};
</script>
@endsection

@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
<a class="ui blue button add-page">
	<i class="plus icon"></i> Tambah Data
</a>
@endif
@endsection