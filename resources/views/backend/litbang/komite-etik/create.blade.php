@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	
	<input type="hidden" name="slug" readonly placeholder="Judul Pelayanan">
	<div class="ui top attached segment" id="inner2bottom">
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="field">
					<label>Name</label>
					<div class="two fields">
						<div class="field">
							<input type="text" name="nama" placeholder="Nama">
						</div>
						<div class="field">
							<select name="jabatan" class="ui search dropdown">
								{{-- <option value="">Pilih Jabatan</option> --}}
								{!! \App\Models\Master\JabatanDokter::options('jabatan') !!}
							</select>
						</div>
					</div>
				</div>
				<div class="field">
					<label for="">Departemen</label>
					<textarea name="departemen" id="" rows="3"></textarea>
				</div>
				<div class="two fields">
					<div class="start date field">
						<label>Tanggal Efektif Awal</label>
						<input type="text" class="five wide column" name="tanggal_efektif_awal" placeholder="Tanggal Efektif Awal">
					</div>
					<div class="end date field">
						<label>Tanggal Efektif Akhir</label>
						<input type="text" class="five wide column" name="tanggal_efektif_akhir" placeholder="Tanggal Efektif Akhir">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$('.editor').summernote({
		height: '155px',
		placeholder: "Konten..."
	});
	$('.start.date').calendar({
			type: 'date',
			formatter: {
				date: function(date, setting) {
					var year = date.getFullYear();
					var month = date.getMonth() + 1;
					var day = date.getDate();
					if (month < 10) {
						month = '0' + month;
					}
					if (day < 10) {
						day = '0' + day;
					}
					return year + '-' + month + '-' + day
				}
			},
			endCalendar: $('.end.date')
		});
	  $('.end.date').calendar({
			type: 'date',
			formatter: {
				date: function(date, setting) {
					var year = date.getFullYear();
					var month = date.getMonth() + 1;
					var day = date.getDate();
					if (month < 10) {
						month = '0' + month;
					}
					if (day < 10) {
						day = '0' + day;
					}
					return year + '-' + month + '-' + day
				}
			},
			startCalendar: $('.start.date')
		});
</script>
@endsection