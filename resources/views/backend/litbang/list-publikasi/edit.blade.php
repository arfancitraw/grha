@extends('layouts.grid')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/semanticui-calendar/calendar.min.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="id" value="{{ $record->id }}">	
	<div class="ui attached top segment">
		<div class="fields">
			<div class="fourteen wide field">
				<label>Judul</label>
				<input type="text" name="judul" placeholder="Judul Publikasi" value="{{ $record->judul or '' }}">
			</div>
			<div class="two wide year field">
				<label>Tahun</label>
				<input type="text" name="tahun" placeholder="Tahun Publikasi" value="{{ $record->tahun or '' }}">
			</div>
		</div>
		<div class="fields">
			<div class="four wide field">
				<label>Peneliti Utama</label>
				<input type="text" name="peneliti_utama" placeholder="Peneliti Utama" value="{{ $record->peneliti_utama or '' }}">
			</div>
			<div class="twelve wide field">
				<label>Peneliti Pendamping</label>
				<input type="text" name="peneliti_pendamping" placeholder="Peneliti Pendamping" value="{{ $record->peneliti_pendamping or '' }}">
			</div>
		</div>
		<div class="three fields">
			<div class="field">
				<label>Citasi Jurnal</label>
				<input type="text" name="citasi_jurnal" placeholder="Citasi Jurnal" value="{{ $record->citasi_jurnal or '' }}">
			</div>
			<div class="field">
				<label>Link Jurnal</label>
				<input type="text" name="link" placeholder="Link Jurnal" value="{{ $record->link or '' }}">
			</div>
			<div class="field">
				<label>File Jurnal</label>
				<div class="ui fluid file input action">
					<input type="text" readonly="" placeholder="File Jurnal" value="{{ $record->file or '' }}">
					<input type="file" class="ten wide column" id="attachment" name="file" autocomplete="off" multiple="">
					<div class="ui blue button file">
						Cari...
					</div>
				</div>
			</div>
		</div>
		<div class="field">
			<label for="">Deskripsi</label>
			<textarea name="deskripsi" rows="5" class="editor" style="height: 301px; resize:none;">{!! $record->deskripsi or '' !!}</textarea>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
	<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '200px';
		tinymce.init(tinymceOpt);

		$('.year').calendar({
			type: 'year'
		})
	});
	</script>
@endsection

