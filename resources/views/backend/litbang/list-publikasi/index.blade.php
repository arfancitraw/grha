@extends('layouts.grid')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
	<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('filters')
	<div class="field">
		<input name="filter[judul]" placeholder="Judul" type="text">
	</div>
	<div class="field">
		<input name="filter[tahun]" placeholder="Tahun" type="text">
	</div>
	<div class="field">
		<input name="filter[peneliti]" placeholder="Peneliti" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
<a class="ui blue button add-page">
	<i class="plus icon"></i> Tambah Data
</a>
@endif
@endsection


@section('js-filters')
    d.judul = $("input[name='filter[judul]']").val();
    d.tahun = $("input[name='filter[tahun]']").val();
    d.peneliti = $("input[name='filter[peneliti]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: 'empty',
		konten: 'empty',
		konten: 'empty'
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
      $('.editor').summernote({
		  height: '200px',
		  placeholder: "Konten profil..."
	  });
	};
</script>
@endsection