@extends('layouts.grid')

@section('content-body')
<form class="ui data large form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">	
	<input type="hidden" name="posisi" value="{{ $record->posisi }}">

	<div class="ui attached top segment">
		<div class="ui grid">
			<div class="twelve wide column">
				<div class="field">
					<div class="ui small labeled input">
						<label for="slug" class="ui label">{{ url('informasi') }}/</label>
						<input type="text" placeholder="Slug" name="slug" value="{{ $record->slug or '' }}" readonly>
					</div>
				</div>

				<div id="context1">
					<div class="ui pointing secondary menu">
				        <a class="active item" data-tab="konten">Konten</a>
				        <a class="item" data-tab="lampiran">File / Lampiran</a>
					</div>
					<div class="ui active tab basic segment" data-tab="konten" style="padding:0">
						<div id="tab_konten">
							<div class="ui top attached tabular menu">
								<a class="active item" data-tab="konten_id">ID</a>
								<a class="item" data-tab="konten_en">EN</a>
							</div>
							<div class="ui bottom attached active tab segment" data-tab="konten_id">
								<div class="field">
									<div class="ui big input">
									  <input name="judul" placeholder="Judul Halaman Informasi" type="text" value="{{ $record->judul or '' }}">
									</div>
								</div>
								<textarea name="konten" rows="3" class="editor">{!! $record->konten or '' !!}</textarea>
							</div>
							<div class="ui bottom attached tab segment" data-tab="konten_en">
								<div class="field">
									<div class="ui big input">
									  <input name="judul_en" placeholder="Judul Halaman Informasi (EN)" type="text" value="{{ $record->judul_en or '' }}">
									</div>
								</div>
								<textarea name="konten_en" rows="3" class="editor">{!! $record->konten_en or '' !!}</textarea>
							</div>
						</div>
					</div>	
					<div class="ui tab basic segment" data-tab="lampiran" style="padding:0">
						<table class="ui celled padded table">
							<thead>
								<tr>
									<th class="center aligned">Judul Lampiran</th>
									<th class="center aligned">File Lampiran</th>
									<th class="center aligned" style="width: 100px">
										<button type="button" class="ui green icon button add-sub" data-content="Tambah Lampiran">
											<i class="plus icon"></i>
										</button>
									</th>
								</tr>
							</thead>
							<tbody class="sub container">
								@if($record->lampiran->count() > 0)
									@php $i = 0; @endphp
									@foreach ($record->lampiran as $lampiran)
										<tr data-count="{{$i}}">
											<td>
												<input type="hidden" name="subid[]" value="{{ $lampiran->id }}">
												<input type="hidden" name="lampiran[{{$i}}][id]" value="{{ $lampiran->id }}">
												<div class="ui small fluid input">
													<input name="lampiran[{{$i}}][judul]" placeholder="Judul Lampiran" type="text" value="{{ $lampiran->judul }}" required>
												</div>
											</td>
											<td class="center aligned">
												<div class="ui small fluid file input action">
													<input type="text" readonly="" placeholder="File Lampiran" value="{{ $lampiran->file }}">
													<input type="file" class="ten wide column" name="lampiran[{{$i}}][file]" autocomplete="off" accept="image/x-png,image/jpeg,application/pdf">

													<div class="ui blue button file">
														Cari...
													</div>
												</div>
											</td>
											<td class="center aligned">
												<button type="button" class="ui red icon button remove-sub" data-content="Hapus Lampiran">
													<i class="delete icon"></i>
												</button>
											</td>
										</tr>
									@php $i++; @endphp
									@endforeach
								@else
								<tr data-count="0">
									<td>
										<div class="ui small fluid input">
											<input name="lampiran[0][judul]" placeholder="Judul Lampiran" type="text" required>
										</div>
									</td>
									<td class="center aligned">
										<div class="ui small fluid file input action">
											<input type="text" readonly="" placeholder="File Lampiran">
											<input type="file" class="ten wide column" name="lampiran[0][file]" autocomplete="off" accept="image/x-png,image/jpeg,application/pdf">

											<div class="ui blue button file">
												Cari...
											</div>
										</div>
									</td>
									<td class="center aligned">
										<button type="button" class="ui red icon button remove-sub" data-content="Hapus Lampiran">
											<i class="delete icon"></i>
										</button>
									</td>
								</tr>
								@endif
							</tbody>
						</table>
					</div>
				</div>	
			</div>
			<div class="four wide column">
				<div class="field image-container">
					<img class="image-preview"  style="height: 120px; object-fit: cover" id="showAttachment" src="{{ ($p = $record->gambar) ? url('storage/'.$p) : asset('img/plc.png') }}">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Gambar Halaman Informasi">
						<input type="file" class="ten wide column" id="attachment" name="photo" autocomplete="off" accept="image/x-png,image/jpeg,application/pdf">
						
						@if(!is_null($record->gambar))
						<div class="ui red button remove image" data-url="{{ url($pageUrl.'remove-image/'.$record->id) }}"><i class="trash icon"></i>
							Hapus
						</div>
						@endif

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
				<div id="tab_deskripsi">
					<div class="ui top attached tabular menu">
						<a class="active item" data-tab="deskripsi_id">ID</a>
						<a class="item" data-tab="deskripsi_en">EN</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="deskripsi_id">
						<div class="field">
							<label for="">Deskripsi Singkat</label>
							<textarea name="keterangan" placeholder="Deskripsi Singkat" style="height: 95px; resize: none">{{ nl2br(e($record->keterangan)) }}</textarea>
						</div>
					</div>
					<div class="ui bottom attached tab segment" data-tab="deskripsi_en">
						<div class="field">
							<label for="">Deskripsi Singkat</label>
							<textarea name="keterangan_en" placeholder="Deskripsi Singkat (EN)" style="height: 95px; resize: none">{{ nl2br(e($record->keterangan_en)) }}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		$('#context1 .menu .item').tab({
		    context: $('#context1')
		  });

		$('#tab_deskripsi .menu .item').tab();
		$('#tab_konten .menu .item').tab({
			context: 'parent'
		});

		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '200px';
		tinymce.init(tinymceOpt);

		$('[name=judul]').on('change, keyup', function(event) {
			var judul = $(this).val();
			$('[name=slug]').val(slugify(judul));
		});
		
		$(document).on('click', '.remove-sub', function(event) {
			$(this).closest('tr').remove();
		});

		$('.add-sub').click(function(event) {
			var idx = parseInt($('.sub.container tr').last().data('count')) + 1;
			var html = `<tr data-count="`+idx+`">
							<td>
								<div class="ui small fluid input">
									<input name="lampiran[`+idx+`][judul]" placeholder="Judul Lampiran" type="text" required>
								</div>
							</td>
							<td class="center aligned">
								<div class="ui small fluid file input action">
									<input type="text" readonly="" placeholder="File Lampiran">
									<input type="file" class="ten wide column" name="lampiran[`+idx+`][file]" autocomplete="off" accept="image/x-png,image/jpeg,application/pdf">

									<div class="ui blue button file">
										Cari...
									</div>
								</div>
							</td>
							<td class="center aligned">
								<button type="button" class="ui red icon button remove-sub" data-content="Hapus Lampiran">
									<i class="delete icon"></i>
								</button>
							</td>
						</tr>`;

			$('.sub.container').append(html);

			$('.ui.checkbox').checkbox();
			$('[data-content]').popup({
				hoverable: true,
				position : 'top center',
				delay: {
					show: 300,
					hide: 800
				}
			});
		});
	});
</script>
@endsection

