@extends('layouts.grid')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
	{{-- <link rel="stylesheet" type="text/css" href="{{ asset('plugins/jQuery/jquery-sortable.css') }}"> --}}
@append

@section('js')
	<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
	<script src="{{ asset('plugins/sortable/Sortable.js') }}"></script>
@append

@section('filters')
	<div class="field">
		<input name="filter[judul]" placeholder="Judul" type="text">
	</div>
	<div class="field">
		<input name="filter[keterangan]" placeholder="Keterangan" type="text">
	</div>
	<button type="button" class="ui teal icon filter button" data-content="Cari Data">
		<i class="search icon"></i>
	</button>
	<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
		<i class="refresh icon"></i>
	</button>
@endsection

@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
<a class="ui teal sorting button">
	<i class="list icon"></i> Urutkan Menu
</a>
<a class="ui blue button add-page">
	<i class="plus icon"></i> Tambah Data
</a>
@endif
@endsection

@section('js-filters')
    d.judul = $("input[name='filter[judul]']").val();
    d.keterangan = $("input[name='filter[keterangan]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		judul: 'empty',
		konten: 'empty',
		keterangan: 'empty'
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
      $('.editor').summernote({
		  height: '200px',
		  placeholder: "Konten informasi..."
	  });
	};

	$('#sortingModal').modal({
		observeChanges: true,
		closable: false,
		detachable: false, 
		autofocus: false,
		onShow: function(e){
			var el = document.getElementById('sortingItems');
			var sortable = Sortable.create(el, {
				animation: 150,
				onSort: function (e) {
					// same properties as onEnd
					$("#sortingModal .loading").show();
					var items = e.to.children;
					var result = [];
					for (var i = 0; i < items.length; i++) {
						result.push($(items[i]).data('key'));
					}
					$.post('{{ url($pageUrl.'sort') }}', {_token: '{{ csrf_token() }}', sorting: result}, function(data, textStatus, xhr) {
						$("#sortingModal .loading").hide();
						dt.draw();
					});
				},
			});
		}
	});

	$('.sorting.button').click(function(event) {
		$('#sortingModal').modal('show');
	});
</script>
@endsection

@section('modals')
<div class="ui tiny modal" id="sortingModal">
	<div class="ui inverted loading dimmer">
		<div class="ui text loader">Loading</div>
	</div>
	<div class="header">Urutkan Menu Informasi</div>
	<div class="content">
		<div class="ui info message">
			Silahkan drag & drop untuk mengatur urutan menu informasi pada website
		</div>
		<div class="ui vertical fluid sortable menu" id="sortingItems">
			@if($data->count() > 0)
			@foreach($data as $item)
			<div class="link item" data-key="{{ $item->id }}">
				{{ $item->judul }}
			</div>
			@endforeach
			@else
			<div class="item" style="text-align: center;"><i>Mohon input konten terlebih dahulu</i></div>
			@endif
		</div>
	</div>
	<div class="actions">
		<div class="ui deny button">
			Tutup
		</div>
	</div>
</div>
@append