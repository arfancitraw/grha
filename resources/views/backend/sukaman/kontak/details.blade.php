@extends('layouts.grid')
@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	<input type="hidden" name="status" id="status" value="{{ $record->status }}">
	
	<div class="ui attached top segment">
		<div class="ui grid">
			<div class="eleven wide column">
				<div class="ui raised segment">
						<table class="ui definition table">
							<tbody>
								<tr>
									<td>Pesan</td>
									<td>{{$record->pesan}}</td>
								</tr>
							</tbody>
						</table>
					</div>
			</div>
			<div class="five wide column">
				<div class="column">
					<div class="ui raised segment">
						<center>
						<h3>{{$record->perihal}}</h3>
						</center>
							@if ($record->status == false)
								<a class="ui red ribbon label">Tidak Aktif</a>
								@else
								<a class="ui blue ribbon label">Aktif</a>
							@endif
						<hr />
						<table class="ui definition table">
							<tbody>
								<tr>
									<td>Nama</td>
									<td>{{$record->nama}}</td>
								</tr>
								<tr>
									<td>Email</td>
									<td>{{$record->email}}</td>
								</tr>
							</tbody>
						</table>
						<p></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">

			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('.menu .item').tab();	
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@append