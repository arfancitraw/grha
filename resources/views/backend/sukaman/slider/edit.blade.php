<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Home Slider</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<input type="hidden" name="status" id="status" value="{{ $record->status }}">

		<div class="field image-container">
			<span class="image-preview" style="height: 15rem;border:none">
				@if($record->gambar)
					<img class="image-preview" id="showAttachment" style="height: 15rem;border:none" src="{{ asset('storage') }}/{{ $record->gambar }}">
				@else
					<img class="image-preview" id="showAttachment" style="height: 15rem" src="{{ asset('img/default.jpg') }}">
				@endif
			</span>
			<div class="ui fluid file input action">
				<input type="text" readonly="" style="border-top-left-radius: 0">
				<input type="file" class="ten wide column" id="attachment" name="gambar" autocomplete="off" accept="image/*">
				<div class="ui blue button file" style="border-top-right-radius: 0">
					Cari...
				</div>
			</div>
		</div>
		<div class="fields">
			<div class="fourteen wide field">
				<label for="nama">Judul</label>
				<input type="text" name="judul" placeholder="Judul" value="{{ $record->judul or '' }}">
			</div>
			<div class="two wide field">
				<label for="urutan">Urutan</label>
				<input type="number" name="urutan" placeholder="Urutan Slider" value="{{ $record->urutan or '' }}">
			</div>
		</div>
		<div class="field">
			<label for="">Deskripsi</label>
			<textarea name="konten" style="height: 97px; resize: none;">{{ $record->konten or '' }}</textarea>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui buttons">
		<button type="button" class="ui button save as drafting orange">Draft</button>
		<div class="or"></div>
		<button type="button" class="ui button save as publicity teal">Publish</button>
	</div>
</div>