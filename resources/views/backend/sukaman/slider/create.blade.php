<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Slider di Paviliun Sukaman</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
		<input type="hidden" name="status" id="status">

		<div class="field image-container">
			<span class="image-preview" style="height: 15rem;">
				<img class="image-preview" id="showAttachment" style="height: 15rem" src="{{ asset('img/default.jpg') }}">
			</span>
			<div class="ui fluid file input action">
				<input type="text" readonly="">
				<input type="file" class="ten wide column" id="attachment" name="gambar" autocomplete="off" accept="image/*">
				<div class="ui blue button file">
					Cari...
				</div>
			</div>
		</div>
		<div class="fields">
			<div class="fourteen wide field">
				<label for="nama">Judul</label>
				<input type="text" name="judul" placeholder="Judul" value="">
			</div>
			<div class="two wide field">
				<label for="urutan">Urutan</label>
				<input type="number" name="urutan" placeholder="Urutan Slider" value="{{ \App\Models\Sukaman\Slider::all()->count() + 1 }}">
			</div>
		</div>

		<div class="field">
			<label for="">Deskripsi</label>
			<textarea name="konten" style="height: 97px; resize: none;"></textarea>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui buttons">
		<button type="button" class="ui button save as drafting orange">Draft</button>
		<div class="or"></div>
		<button type="button" class="ui button save as publicity teal">Publish</button>
	</div>
</div>