@extends('layouts.grid')

@section('css')
@append

@section('js')
@append

@section('init-modal')
    <script type="text/javascript">
    	postRequest = function(){
            $('.grid-container').load("{{ url($pageUrl.'grid') }}" ,function() {
                $('.special.cards .dimmable').dimmer({
                  on: 'hover'
                });
            });
    	};

    	initModal = function(){
			dt = $('#dokterTable').DataTable({
		        dom: 'rt<"bottom"ip><"clear">',
				responsive: true,
				autoWidth: false,
				processing: true,
				lengthChange: false,
				pageLength: 5,
				sorting: [],
				language: {
					url: "{{ asset('plugins/datatables/Indonesian.json') }}"
				},
				drawCallback: function() {
		            $('.ui.checkbox').checkbox();
				}
			});

    		$('#modal-search').keyup(function(){
			      dt.search($(this).val()).draw() ;
			})
        };

        $(document).ready(function($) {
            $('.grid-container').load("{{ url($pageUrl.'grid') }}" ,function() {
                $('.special.cards .dimmable').dimmer({
                  on: 'hover'
                });
            });
        });

        $(document).on('click', '.filter.button', function(){
            var search = $("input[name='filter[nama]']").val();
            $('.grid-container').load("{{ url($pageUrl.'grid') }}?search="+search, function() {
                $('.special.cards .dimmable').dimmer({
                  on: 'hover'
                });
            });
        });
    </script>
@append

@section('content-body')
	<div class="ui grid">
	    <div class="four wide column">
	        <div class="ui right action fluid input">
	            <input type="text" name="filter[nama]" placeholder="Cari Nama Dokter">
	            <button type="button" class="ui teal icon filter button" data-content="Cari Nama Dokter">
	                <i class="search icon"></i>
	            </button>
	        </div>
	    </div>
	</div>
	<div class="grid-container">    
	    
	</div>
@endsection