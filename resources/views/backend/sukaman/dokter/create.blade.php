<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Dokter di Paviliun Sukaman</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
		{!! csrf_field() !!}
        <div class="ui icon fluid input">
			<input id="modal-search" placeholder="Cari nama dokter" type="text">
			<i class="search icon"></i>
		</div><br>
		<table id="dokterTable" class="ui bordered red table">
			<thead>
				<tr>
					<th style="width: 120px" class="center aligned">Foto</th>
					<th class="center aligned">Nama</th>
					<th class="center aligned">Spesialisasi</th>
					<th class="center aligned"></th>
				</tr>
			</thead>
			<tbody>
				@if(count($records) > 0)
				@foreach($records as $row)
				<tr>
					<td class="center aligned">
						<div class="img-container" style="width: 100%; height: 100px; overflow: hidden;">
							<img src="{{ !is_null($row->photo) ? url('storage/'.$row->photo) : asset('img/foto_placeholder2.png') }}" alt="{{ $row->nama }}" width="120px">
						</div>
					</td>
					<td>{{ $row->nama_lengkap }}</td>
					<td>{{ $row->spesialisasi->nama }}</td>
					<td class="center aligned">
						<div class="ui checkbox">
						  <input name="dokter[]" type="checkbox" value="{{$row->id}}">
						</div>
					</td>
				</tr>
				@endforeach
				@endif
			</tbody>
		</table>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>