<div class="ui four stackable special cards">
    @if(count($data) > 0)
    @foreach($data as $row)
    <div class="card">
        <div class="blurring dimmable image">
            <div class="ui dimmer">
                <div class="content">
                    <div class="center">
                        <button type="button" class="ui inverted red delete button" data-id="{{ $row->id }}">Hapus Dokter</button>
                    </div>
                </div>
            </div>
            @if(!is_null($row->detail->photo))
            <img src="{{ asset('storage/'.$row->detail->photo) }}" alt="{{ $row->detail->nama }}">
            @else
            <img src="{{ asset('img/foto_placeholder2.png') }}" alt="Dokter">
            @endif
        </div>
        <div class="content">
            <a class="header">{{ $row->detail->nama_lengkap }}</a>
            <div class="meta">
                <span class="date">{{ $row->detail->spesialisasi->nama }}</span>
            </div>
        </div>
    </div>
    @endforeach
    @endif
    <a class="add card" style="min-height: 200px">
        <div class="blurring dimmable content center" style="background: #fff;">
            <div class="ui dimmer">
                <div class="content">
                    <div class="center">
                        <div class="ui inverted blue add button">Tambah Dokter</div>
                    </div>
                </div>
            </div>
            <img src="{{ asset('img/icons/icon_accordion_show.png') }}">
        </div>
    </a>
</div>