@extends('layouts.grid')

@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="ui stackable grid">

		{{-- Judul Utama --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Judul Utama
			  <div class="sub header">Ditampilkan diawal halaman Paviliun Sukaman. Menjadi overlay dari slider web khusus Paviliun Sukaman</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="field">
				<input type="text" name="setting[{{$settings['title']->id}}][judul]" placeholder="contoh: Tentang Paviliun Sukaman" value="{{ $settings['title']->judul }}">
			</div>
			<div class="field">
				<textarea name="setting[{{$settings['title']->id}}][isi]" style="height: 100px; resize: none;" placeholder="contoh: Lorem Ipsum dolor sit amet.">{!! nl2br($settings['title']->isi) !!}</textarea>
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>

		{{-- Tentang Pav. Sukaman --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Tentang Paviliun Sukaman
			  <div class="sub header">Ditampilkan diawal halaman Paviliun Sukaman. Merupakan penjelasan singkat tentang Paviliun Sukaman</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="field">
				<input type="text" name="setting[{{$settings['about']->id}}][judul]" placeholder="contoh: Tentang Paviliun Sukaman" value="{{ $settings['about']->judul }}">
			</div>
			<div class="field">
				<textarea name="setting[{{$settings['about']->id}}][isi]" style="height: 100px; resize: none;" placeholder="contoh: Lorem Ipsum dolor sit amet.">{!! nl2br($settings['about']->isi) !!}</textarea>
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>

		{{-- Emergency --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Perhatian Darurat
			  <div class="sub header">Ditampilkan setelah penjelasan singkat Paviliun Sukaman. Merupakan pemberitahuan singkat terkait informasi darurat dan tombol buat perjanjian.</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="field">
				<input type="text" name="setting[{{$settings['emergency']->id}}][judul]" placeholder="contoh: Tentang Paviliun Sukaman" value="{{ $settings['emergency']->judul }}">
			</div>
			<div class="field">
				<textarea name="setting[{{$settings['emergency']->id}}][isi]" style="height: 100px; resize: none;" placeholder="contoh: Lorem Ipsum dolor sit amet.">{!! nl2br($settings['emergency']->isi) !!}</textarea>
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>

		{{-- Services --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Pelayanan
			  <div class="sub header">Ditampilkan setelah perhatian darurat. Merupakan penjelasan singkat terkait pelayanan unggulan yang ada pada Paviliun Sukaman.</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="field">
				<input type="text" name="setting[{{$settings['service']->id}}][judul]" placeholder="contoh: Tentang Paviliun Sukaman" value="{{ $settings['service']->judul }}">
			</div>
			<div class="field">
				<textarea name="setting[{{$settings['service']->id}}][isi]" style="height: 100px; resize: none;" placeholder="contoh: Lorem Ipsum dolor sit amet.">{!! nl2br($settings['service']->isi) !!}</textarea>
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>

		{{-- Doctors --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Tim Dokter
			  <div class="sub header">Ditampilkan setelah detail pelayanan. Merupakan pengantar untuk tampilan tim dokter yang ada di Paviliun Sukaman.</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="field">
				<input type="text" name="setting[{{$settings['doctors']->id}}][judul]" placeholder="contoh: Tentang Paviliun Sukaman" value="{{ $settings['doctors']->judul }}">
			</div>
			<div class="field">
				<textarea name="setting[{{$settings['doctors']->id}}][isi]" style="height: 100px; resize: none;" placeholder="contoh: Lorem Ipsum dolor sit amet.">{!! nl2br($settings['doctors']->isi) !!}</textarea>
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>

		{{-- Facilities --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Fasilitas
			  <div class="sub header">Ditampilkan setelah detail tim dokter. Merupakan pengantar untuk detail fasilitas yang dimiliki Paviliun Sukaman.</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="field">
				<input type="text" name="setting[{{$settings['facility']->id}}][judul]" placeholder="contoh: Tentang Paviliun Sukaman" value="{{ $settings['facility']->judul }}">
			</div>
			<div class="field">
				<textarea name="setting[{{$settings['facility']->id}}][isi]" style="height: 100px; resize: none;" placeholder="contoh: Lorem Ipsum dolor sit amet.">{!! nl2br($settings['facility']->isi) !!}</textarea>
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>

		{{-- Information --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Informasi Pasien
			  <div class="sub header">Ditampilkan setelah detail fasilitas. Merupakan pengantar untuk detail informasi yang dibutuhkan oleh pasien.</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="field">
				<input type="text" name="setting[{{$settings['information']->id}}][judul]" placeholder="contoh: Tentang Paviliun Sukaman" value="{{ $settings['information']->judul }}">
			</div>
			<div class="field">
				<textarea name="setting[{{$settings['information']->id}}][isi]" style="height: 100px; resize: none;" placeholder="contoh: Lorem Ipsum dolor sit amet.">{!! nl2br($settings['information']->isi) !!}</textarea>
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>

		{{-- Contact --}}
		<div class="four wide column">
			<h2 class="ui header">
			  Kontak
			  <div class="sub header">Merupakan pengaturan untuk akses dan kontak kepada RS Jantung dan Pembuluh Darah Harapan Kita terutama departemen Paviliun Sukaman.</div>
			</h2>
		</div>
		<div class="twelve wide column">
			<div class="ui equal width grid">
				<div class="column">
					<div class="two fields">
						<div class="field">
							<label>Call Center</label>
							<input type="text" name="setting[{{$settings['call_center']->id}}][judul]" placeholder="contoh: 1500 034" value="{{ $settings['call_center']->judul }}">
						</div>
						<div class="field">
							<label>Emergency Call</label>
							<input type="text" name="setting[{{$settings['emergency_call']->id}}][judul]" placeholder="contoh: (021) 568 2424" value="{{ $settings['emergency_call']->judul }}">
						</div>
					</div>
					<div class="field">
						<label>Email</label>
						<input type="text" name="setting[{{$settings['email']->id}}][judul]" placeholder="contoh: email@pjnhk.go.id" value="{{ $settings['email']->judul }}">
					</div>
				</div>
				<div class="column">
					<div class="field">
						<label>Alamat</label>
						<textarea name="setting[{{$settings['address']->id}}][judul]" style="height: 100px; resize: none;" placeholder="contoh: Lorem Ipsum dolor sit amet.">{!! nl2br($settings['address']->judul) !!}</textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="sixteen wide column">
			<div class="ui fitted divider"></div>
		</div>
	</div>
	<div class="ui two column grid">
		<div class="left aligned column">
			<div class="ui black labeled icon button" onclick="window.history.back()">
				<i class="chevron left icon"></i>
				Kembali
			</div>
		</div>
		<div class="right aligned column">
			<div class="ui right labeled green icon save as page button">
				<i class="save icon"></i>
				Simpan
			</div>
		</div>
	</div>

</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('.menu .item').tab();	
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@endsection

