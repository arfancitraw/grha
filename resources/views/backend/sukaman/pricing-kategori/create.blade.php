<div class="ui inverted loading dimmer">
    <div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Kategori Pricing</div>
<div class="content">
    <form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST">
        {!! csrf_field() !!}
        <div class="field">
            <div class="ui large input">
                <input type="text" name="nama" placeholder="Nama Kategori Tarif">
            </div>
      </div>
      <div class="ui grid">
        <div class="six wide column">
            <div class="field">
                <label for="">Prioritas</label>
                <input type="number" name="prioritas">
            </div>
            <div class="field">
                <label for="">Status</label>
                <select name="status" id="" class="ui dropdown">
                    <option value="1">Aktif</option>
                    <option value="0">Non-aktif</option>
                </select>
            </div>
        </div>
        <div class="ten wide column">
            <div class="field">
                <label>Deskripsi Kategori</label>
                <textarea name="deskripsi" style="height: 100px; resize: none;" placeholder="Masukan penjelasan singkat pelayanan disini"></textarea>
            </div>
        </div>
    </div>
    <div class="ui error message"></div>
</form>
</div>
<div class="actions">
    <div class="ui black deny button">
        Batal
    </div>
    <div class="ui positive right labeled icon save button">
        Simpan
        <i class="checkmark icon"></i>
    </div>
</div>