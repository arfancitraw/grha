<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Tarif</div>
<div class="content">
	<form class="ui data form" id="tarifForm" action="{{ url($pageUrl.'store-tarif') }}" method="POST">
		{!! csrf_field() !!}
        <input type="hidden" name="id_kategori" value="{{ $record->id }}">
        <div class="field">
            <div class="ui large input">
                <input type="text" name="nama" placeholder="Judul Tarif">
            </div>
        </div>
		<div class="ui grid">
			<div class="seven wide column">
                <div class="field">
                    <label for="">Harga</label>
                    <div class="ui labeled fluid input">
                        <div class="ui label">
                            Rp.
                        </div>
                        <input type="number" name="harga" style="text-align: right;">
                    </div>
                </div>
                <div class="field">
                    <label for="">Warna</label>
                    <select name="warna">
                        <option value="default">Abu-abu</option>
                        <option value="blue">Biru</option>
                    </select>
                </div>
            </div>
            <div class="nine wide column">
                <div class="field">
                    <label>Deskripsi Tarif</label>
                    <textarea name="deskripsi" rows="3" style="height: 100px; resize: none;" placeholder="Masukan penjelasan singkat pelayanan disini"></textarea>
                </div>
            </div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui green right labeled icon save-tarif button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>