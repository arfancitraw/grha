<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Edit Data Tarif</div>
<div class="content">
	<form class="ui data form" id="tarifFormEdit" action="{{ url($pageUrl.'update-tarif') }}" method="POST">
{!! csrf_field() !!}
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="field">
        <div class="ui large input">
            <input type="text" name="nama" placeholder="Judul Tarif" value="{{$record->nama}}">
        </div>
    </div>
    <div class="ui grid">
        <div class="seven wide column">
            <div class="field">
                <label for="">Harga</label>
                <div class="ui labeled fluid input">
                    <div class="ui label">
                        Rp.
                    </div>
                    <input type="number" name="harga" style="text-align: right;" value="{{$record->harga}}">
                </div>
            </div>
            <div class="field">
                <label for="">Warna</label>
                <select name="warna">
                    <option value="default" @if($record->warna == 'default') selected @endif>Abu-abu</option>
                    <option value="blue" @if($record->warna == 'blue') selected @endif>Biru</option>
                </select>
            </div>
        </div>
        <div class="nine wide column">
            <div class="field">
                <label>Deskripsi Tarif</label>
                <textarea name="deskripsi" rows="3" style="height: 100px; resize: none;" placeholder="Masukan penjelasan singkat pelayanan disini">{!! nl2br($record->deskripsi) !!}</textarea>
            </div>
        </div>
    </div>
    <div class="ui error message"></div>
</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui green right labeled icon update-tarif button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>