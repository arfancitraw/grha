<div class="ui three stackable special cards">
	@foreach ($record->tarif as $value)
	<form class="card formUang" data-id="1" method="POST" action="http://tokosurya.me/uang/1/save">
		<input type="hidden" name="_token" value="A0mz4pMU4Xn0t9QppWOhu1nYbwbzXeDQ8ZXGPEvQ">
		<div class="dwarf content">
			<span class="header">
				{{$value->nama}}
				<div class="right floated" style="margin-top: -3px">
					<button type="button" onclick="editTarifModal({{$value->id}})" class="ui mini orange edit-pecahan icon button visible" data-content="Ubah Tarif" data-id="1"><i class="edit icon"></i></button>
					<button type="button" onclick="deleteTarif({{$value->id}})" class="ui mini red icon button visible" data-content="Hapus Tarif" data-id="1"><i class="delete icon"></i></button>
				</div>
			</span>
		</div>
		<div class="compact content">
			<table class="ui small very basic table">
				<thead>
					<tr>
						<th class="center aligned">Nama</th>
						<th class="center aligned">Checklist</th>
						<th class="center aligned">
					<button type="button" onclick="tambahDetailTarifModal({{$value->id}})" class="ui mini green edit-pecahan icon button visible" data-content="Tambah Item Tarif" data-id="1"><i class="plus icon"></i></button>
						</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($value->detail as $row)
						
					<tr>
						<td>{{$row->nama}}</td>
						<td class="center aligned">
							@if($row->check)
								<i class="checkmark box icon"></i>
							@else
								<i class="square outline icon"></i>
							@endif
						</td>
						<td class="center aligned">
							<button type="button" onclick="editDetailTarifModal({{$row->id}})" class="ui mini orange edit-pecahan icon button visible" data-content="Ubah Item Tarif" data-id="1"><i class="edit icon"></i></button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="ui bottom attached teal salin tarif button" data-id="{{ $value->id }}">
            <i class="clone icon"></i>
            Salin Tarif
        </div>
	</form>
	@endforeach
	<a class="add card add tarif" style="min-height: 200px">
		<div class="blurring dimmable content center" style="background: #fff;">
			<img width="100px" src="https://static1.squarespace.com/static/550446e4e4b0595113edfea0/t/57023e533c44d887e798dba6/1459764830622/Plus+Icon.png?format=2500w">
		</div>
	</a>
</div>