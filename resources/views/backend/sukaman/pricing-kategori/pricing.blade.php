@extends('layouts.grid')

@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<div class="grid-container"><div class="ui three stackable special cards">
    <form class="card formUang" data-id="1" method="POST" action="http://tokosurya.me/uang/1/save">
        <input type="hidden" name="_token" value="A0mz4pMU4Xn0t9QppWOhu1nYbwbzXeDQ8ZXGPEvQ">
        <div class="dwarf content">
            <span class="header">
                Kategori 1 
                <div class="right floated" style="margin-top: -3px">
                    <button class="ui mini orange edit icon button" data-content="Ubah Mata Uang" data-id="1"><i class="edit icon"></i></button>
                    <button class="ui mini red delete icon button" data-content="Hapus Mata Uang" data-id="1"><i class="delete icon"></i></button>
                </div>
            </span>
        </div>
        <div class="compact content">
            <table class="ui small very basic table">
                <thead>
                    <tr>
                        <th class="center aligned">Nama Pecahan</th>
                        <th class="center aligned">Nilai</th>
                        <th class="center aligned">Harga Beli</th>
                        <th class="center aligned">Harga Jual</th>
                        <th class="center aligned">
                            <button type="button" class="ui mini green add-pecahan icon button" data-content="Tambah Pecahan" data-id="1"><i class="plus icon"></i></button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Satuan</td>
                        <td class="center aligned">1-3</td>
                        <td class="right aligned">
                            <div class="ui semi transparent fluid input">
                                <input class="right aligned harga pecahan" placeholder="Harga Beli" name="harga[1][harga_beli]" type="text" value="13200">
                            </div>
                        </td>
                        <td class="right aligned">
                            <div class="ui semi transparent fluid input">
                                <input class="right aligned harga pecahan" placeholder="Harga Jual" name="harga[1][harga_jual]" type="text" value="14200">
                            </div>
                        </td>
                        <td class="center aligned">
                            <button type="button" class="ui mini orange edit-pecahan icon button" data-content="Ubah Pecahan" data-id="1"><i class="edit icon"></i></button>
                        </td>
                    </tr>
                    <tr>
                        <td>Puluhan</td>
                        <td class="center aligned">4-10</td>
                        <td class="right aligned">
                            <div class="ui semi transparent fluid input">
                                <input class="right aligned harga pecahan" placeholder="Harga Beli" name="harga[2][harga_beli]" type="text" value="14100">
                            </div>
                        </td>
                        <td class="right aligned">
                            <div class="ui semi transparent fluid input">
                                <input class="right aligned harga pecahan" placeholder="Harga Jual" name="harga[2][harga_jual]" type="text" value="14700">
                            </div>
                        </td>
                        <td class="center aligned">
                            <button type="button" class="ui mini orange edit-pecahan icon button" data-content="Ubah Pecahan" data-id="2"><i class="edit icon"></i></button>
                        </td>
                    </tr>
                    <tr>
                        <td>Ratusan</td>
                        <td class="center aligned">11-100</td>
                        <td class="right aligned">
                            <div class="ui semi transparent fluid input">
                                <input class="right aligned harga pecahan" placeholder="Harga Beli" name="harga[5][harga_beli]" type="text" value="15000">
                            </div>
                        </td>
                        <td class="right aligned">
                            <div class="ui semi transparent fluid input">
                                <input class="right aligned harga pecahan" placeholder="Harga Jual" name="harga[5][harga_jual]" type="text" value="15300">
                            </div>
                        </td>
                        <td class="center aligned">
                            <button type="button" class="ui mini orange edit-pecahan icon button" data-content="Ubah Pecahan" data-id="5"><i class="edit icon"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="ui bottom attached disabled save-pecahan green button" data-id="1">
            <i class="save icon"></i>
            Simpan Harga
        </div>
    </form>
    <a class="add card" style="min-height: 200px">
        <div class="blurring dimmable content center" style="background: #fff;">
            <div class="ui dimmer transition hidden">
                <div class="content">
                    <div class="center">
                        <div class="ui inverted add button">Tambah Mata Uang</div>
                    </div>
                </div>
            </div>
            <img src="http://tokosurya.me/img/icons/icon_accordion_show.png">
        </div>
    </a>
</div></div>
@endsection

@section('scripts')
<script type="text/javascript">

	/* Tanpa Rupiah */
	var tanpa_rupiah = document.getElementById('tanpa-rupiah');
	tanpa_rupiah.addEventListener('keyup', function(e)
	{
		tanpa_rupiah.value = formatRupiah(this.value);
	});

	/* Fungsi */
	function formatRupiah(angka, prefix)
	{
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split	= number_string.split(','),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);
			
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}
		
		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}
</script>
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('.menu .item').tab();	
		$(".numeral").inputmask("integer", {
		    alias: 'decimal', 
		    groupSeparator: '.', 
		    autoGroup: true, 
		    placeholder: '0',
		    autoUnmask: true,
		    unmaskAsNumber: true,
		    removeMaskOnSubmit:true
		});
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@endsection

