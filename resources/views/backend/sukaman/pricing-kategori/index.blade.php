@extends('layouts.grid')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-lite.css') }}">
@append

@section('js')
<script src="{{ asset('plugins/summernote/summernote-lite.js') }}"></script>
@append

@section('filters')
<div class="field">
	<input name="filter[judul]" placeholder="Judul" type="text">
</div>
<button type="button" class="ui teal icon filter button" data-content="Cari Data">
	<i class="search icon"></i>
</button>
<button type="reset" class="ui icon reset button" data-content="Bersihkan Pencarian">
	<i class="refresh icon"></i>
</button>
@endsection

@section('js-filters')
d.judul = $("input[name='filter[judul]']").val();
@endsection

@section('rules')
<script type="text/javascript">
	formRules = {
		nama: 'empty',
		prioritas: 'empty',
		status: 'empty',
	};
</script>
@endsection

@section('init-modal')
<script>
	initModal = function(){
		$('.editor').summernote({
			height: '200px'
		});

		$(document).on('click', '.button.save.as.drafting', function(e){
			$('#status').val(0);
			$('#saveButton').trigger('click');
		});

		$(document).on('click', '.button.save.as.publicity', function(e){
			$('#status').val(1);
			$('#saveButton').trigger('click');
		});
	};
</script>

@endsection
@section('toolbars')
@if($pagePerms == '' || auth()->user()->can($pagePerms.'-add'))
<button type="button" class="ui blue add button">
    <i class="plus icon"></i>
    Tambah Data
</button>
@endif
@endsection