<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Tambah Detail Tarif</div>
<div class="content">
	<form class="ui data form" id="detilTarifForm" action="{{ url($pageUrl.'store-detail-tarif') }}" method="POST">
		{!! csrf_field() !!}
        <input type="hidden" name="id_tarif" value="{{ $record->id }}">
        <div class="field">
            <label for="">Judul Detail</label>
            <input type="text" name="nama" placeholder="Judul Detail Tarif">
        </div>
        <div class="two fields">
            <div class="field">
                <label for="">Checklist</label>
                <div class="ui checkbox" style="margin-top: 10px">
                    <input type="hidden" name="check" value="0">
                    <input type="checkbox" name="check" value="1">
                    <label>Ter-checklist</label>
              </div>
            </div>
            <div class="field">
                <label for="">Urutan</label>
                <input type="number" name="urutan" placeholder="Kosongkan untuk default">
            </div>
        </div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui green right labeled icon save-detail button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>