<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">
    Edit Detail Tarif
</div>
<div class="content">
	<form class="ui data form" id="tarifFormEditDetail" action="{{ url($pageUrl.'update-detail-tarif') }}" method="POST">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="id" value="{{ $record->id }}">
        <div class="field">
            <label for="">Judul Detail</label>
            <input type="text" name="nama" placeholder="Judul Detail Tarif" value="{{ $record->nama }}">
        </div>
        <div class="two fields">
            <div class="field">
                <label for="">Checklist</label>
                <div class="ui checkbox" style="margin-top: 10px">
                    <input type="hidden" name="check" value="0">
                    <input type="checkbox" name="check" value="1" @if($record->check) checked @endif>
                    <label>Ter-checklist</label>
              </div>
            </div>
            <div class="field">
                <label for="">Urutan</label>
                <input type="number" name="urutan" placeholder="Kosongkan untuk default" value="{{ $record->urutan }}">
            </div>
        </div>
    </div>
</form>
</div>
<div class="actions">
    <button type="button" onclick="deleteDetailTarif({{$record->id}})" class="ui left floated red icon button" data-content="Hapus Item Tarif">
        <i class="trash icon"></i>
    </button>
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui green right labeled icon update-detail-tarif button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>