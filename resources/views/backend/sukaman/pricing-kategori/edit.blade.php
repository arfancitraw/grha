@extends('layouts.grid')

@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl.$record->id) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="id" value="{{ $record->id }}">
	<input type="hidden" name="status" id="status" value="{{ $record->status }}">
	
	<div class="ui attached top segment">
		<div class="field">
		    <div class="ui large input">
		        <input type="text" name="nama" placeholder="Nama Kategori Tarif" value="{{ $record->nama }}">
		    </div>
		</div>
		<div class="ui grid">
			<div class="six wide column">
				<div class="field">
					<label for="">Prioritas</label>
					<input type="text" name="prioritas" value="{{ $record->prioritas }}">
				</div>
				<div class="field">
					<label for="">Status</label>
					<select name="status" class="ui dropdown">
						<option value="1" @if($record->status) selected @endif>Aktif</option>
						<option value="0" @if(!$record->status) selected @endif>Not Aktif</option>
					</select>
				</div>
			</div>
			<div class="ten wide column">
				<div class="field">
					<label>Deskripsi Kategori</label>
					<textarea name="deskripsi" rows="3" style="height: 100px; resize: none;" placeholder="Masukan penjelasan singkat pelayanan disini">{{$record->deskripsi}}</textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">

			</div>
			<div class="right aligned column">
				<button class="ui right labeled blue icon save as page button">
					<i class="save icon"></i>
					Simpan
				</button>
			</div>
		</div>
	</div>
</form>
<div class="grid-container">
	@include('backend.sukaman.pricing-kategori.tarif.grid')
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	function editTarifModal(id) {
		var url = "{{ url($pageUrl) }}/"+id+"/edit-tarif";
		if($('#formModal').hasClass('mini')){
			$('#formModal').removeClass('mini');
			$('#formModal').addClass('tiny');
		}
		loadModal(url);
	}
	function editDetailTarifModal(id) {
		var url = "{{ url($pageUrl) }}/"+id+"/edit-detail-tarif";
		if($('#formModal').hasClass('tiny')){
			$('#formModal').removeClass('tiny');
			$('#formModal').addClass('mini');
		}
		loadModal(url);
	}
	function tambahDetailTarifModal(id) {
		var url = "{{ url($pageUrl) }}/"+id+"/tambah-detail-tarif";
		if($('#formModal').hasClass('tiny')){
			$('#formModal').removeClass('tiny');
			$('#formModal').addClass('mini');
		}
		loadModal(url);
	}
	function deleteTarif(id) {
		swal({
			title: 'Apakah anda yakin?',
			text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Hapus',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: '{{ url($pageUrl) }}/'+id+'/delete-tarif',
					type: 'POST',
					data: {_token: "{{ csrf_token() }}", _method: "delete"},
					success: function(resp){
						swal(
							'Terhapus!',
							'Data berhasil dihapus.',
							'success'
							).then(function(e){
								if(!postRequest()){
									dt.draw();
								}
							});location.reload();
						},
						error : function(resp){
							var data = resp.responseJSON;
							console.log(data)
							swal(
								'Gagal!',
								Object.is(data.message, undefined) ? 'Data gagal dihapus, eror tidak diketahui' : data.message,
								'error'
								).then(function(e){
									dt.draw();
								});
							}
						});
				}
			})
		}
	function deleteDetailTarif(id) {
		swal({
			title: 'Apakah anda yakin?',
			text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Hapus',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$.ajax({
					url: '{{ url($pageUrl) }}/'+id+'/delete-detail-tarif',
					type: 'POST',
					data: {_token: "{{ csrf_token() }}", _method: "delete"},
					success: function(resp){
						swal(
							'Terhapus!',
							'Data berhasil dihapus.',
							'success'
							).then(function(e){
								if(!postRequest()){
									dt.draw();
								}
							});location.reload();
						},
						error : function(resp){
							var data = resp.responseJSON;
							console.log(data)
							swal(
								'Gagal!',
								Object.is(data.message, undefined) ? 'Data gagal dihapus, eror tidak diketahui' : data.message,
								'error'
								).then(function(e){
									dt.draw();
								});
							}
						});
				}
			})
		}

	$(document).on('click', '.salin.tarif', function(e){
		var id = $(this).data('id');
		var url = "{{ url($pageUrl) }}/"+id+"/salin-tarif";

		if($('#formModal').hasClass('mini')){
			$('#formModal').removeClass('mini');
			$('#formModal').addClass('tiny');
		}

		loadModal(url);
	});
	
	$(document).on('click', '.add.tarif', function(e){
		var url = "{{ url($pageUrl) }}/{{ $record->id }}/tambah-tarif";

		if($('#formModal').hasClass('mini')){
			$('#formModal').removeClass('mini');
			$('#formModal').addClass('tiny');
		}

		loadModal(url);
	});

	$(document).on('click', '.save-tarif', function(e){
		$("#tarifForm").form('validate form');
		
		if($("#tarifForm").form('is valid')){
			$('#formModal').find('.loading.dimmer').addClass('active');
			$("#tarifForm").ajaxSubmit({
				beforeSend: function(f){
					$(".numeral").inputmask('unmask');
				},
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							location.reload();

							if(!postRequest()){
								dt.draw();
							}
							return true;
						})
					},
					error: function(resp){
						$('#formModal').find('.loading.dimmer').removeClass('active');
						var error = $('<ul class="list"></ul>');
						console.log(resp.responseJSON);
						$.each(resp.responseJSON, function(index, val) {
							error.append('<li>'+val+'</li>');
						});

						$('#formModal').find('.ui.error.message').html(error).show();
					}
				});	
		}});
	
	$(document).on('click', '.save-detail', function(e){
		$("#detilTarifForm").form('validate form');
		
		if($("#detilTarifForm").form('is valid')){
			$('#formModal').find('.loading.dimmer').addClass('active');
			$("#detilTarifForm").ajaxSubmit({
				beforeSend: function(f){
					$(".numeral").inputmask('unmask');
				},
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							location.reload();

							if(!postRequest()){
								dt.draw();
							}
							return true;
						})
					},
					error: function(resp){
						$('#formModal').find('.loading.dimmer').removeClass('active');
						var error = $('<ul class="list"></ul>');
						console.log(resp.responseJSON);
						$.each(resp.responseJSON, function(index, val) {
							error.append('<li>'+val+'</li>');
						});

						$('#formModal').find('.ui.error.message').html(error).show();
					}
				});	
		}});
	$(document).on('click', '.update-tarif', function(e){
		$("#tarifFormEdit").form('validate form');
		if($("#tarifFormEdit").form('is valid')){
			$('#formModal').find('.loading.dimmer').addClass('active');
			$("#tarifFormEdit").ajaxSubmit({
				beforeSend: function(f){
					$(".numeral").inputmask('unmask');
				},
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil Di Ubah.',
						'success'
						).then((result) => {
							location.reload();
							if(!postRequest()){
								dt.draw();
							}
							return true;
						})
					},
					error: function(resp){
						$('#formModal').find('.loading.dimmer').removeClass('active');
						var error = $('<ul class="list"></ul>');
						console.log(resp.responseJSON);
						$.each(resp.responseJSON, function(index, val) {
							error.append('<li>'+val+'</li>');
						});

						$('#formModal').find('.ui.error.message').html(error).show();
					}
				});	
		}});
	$(document).on('click', '.update-detail-tarif', function(e){
		$("#tarifFormEditDetail").form('validate form');
		if($("#tarifFormEditDetail").form('is valid')){
			$('#formModal').find('.loading.dimmer').addClass('active');
			$("#tarifFormEditDetail").ajaxSubmit({
				beforeSend: function(f){
					$(".numeral").inputmask('unmask');
				},
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil Di Ubah.',
						'success'
						).then((result) => {
							location.reload();
							if(!postRequest()){
								dt.draw();
							}
							return true;
						})
					},
					error: function(resp){
						$('#formModal').find('.loading.dimmer').removeClass('active');
						var error = $('<ul class="list"></ul>');
						console.log(resp.responseJSON);
						$.each(resp.responseJSON, function(index, val) {
							error.append('<li>'+val+'</li>');
						});

						$('#formModal').find('.ui.error.message').html(error).show();
					}
				});	
		}});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}});
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('.menu .item').tab();	
	});
</script>
@append