<div class="ui inverted loading dimmer">
	<div class="ui text loader">Loading</div>
</div>
<div class="header">Buat Data Tarif</div>
<div class="content">
	<form class="ui data form" id="dataForm" action="{{ url($pageUrl).'/store-tarif' }}" method="POST">
		{!! csrf_field() !!}
		<div class="ui grid">
			<div class="sixteen wide column">
				<div class="fields">
                <div class="five wide field image-container">
                    <div class="field">
                        <label for="">Nama</label>
                        <input type="text" name="nama">
                    </div>
                    <div class="field">
                        <label for="">Harga</label>
                        <input type="text" name="harga">
                    </div>
                    <div class="field">
                        <label for="">Warna</label>
                        <input type="text" name="warna">
                    </div>
                </div>
                <div class="eleven wide field">
                    <label>Deskripsi Tarif</label>
                    <textarea name="deskripsi" rows="3" style="height: 135px; resize: none;" placeholder="Masukan penjelasan singkat pelayanan disini"></textarea>
                </div>
            </div>
			</div>
		</div>
		<div class="ui error message"></div>
	</form>
</div>
<div class="actions">
	<div class="ui black deny button">
		Batal
	</div>
	<div class="ui positive right labeled icon save button">
		Simpan
		<i class="checkmark icon"></i>
	</div>
</div>