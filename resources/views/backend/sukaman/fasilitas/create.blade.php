@extends('layouts.grid')

@section('js')
<script src="{{ asset('js/slugify.js') }}"></script>
<script src="{{ asset('plugins/semanticui-calendar/calendar.min.js') }}"></script>
@append

@section('content-body')
<form class="ui data form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="ui attached top segment">
		<div class="ui grid">
			<div class="twelve wide column">
				<div class="field">
					<div class="ui big input">
					  <input name="judul" placeholder="Judul Pelayanan" type="text">
					</div>
				</div>
				<div class="field">
					<textarea name="deskripsi" rows="3" class="editor"></textarea>
				</div>
			</div>
			<div class="four wide column">
				<div class="field image-container">
					<img class="image-preview"  style="height: 120px" id="showAttachment" style="height: 27rem" src="http://fsm.undip.ac.id//assets/attachments/Images/default.jpg">
					<div class="ui fluid file input action">
						<input type="text" readonly="" placeholder="Gambar Fasilitas">
						<input type="file" class="ten wide column" id="attachment" name="gambar" autocomplete="off" multiple="">

						<div class="ui blue button file">
							Cari...
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui bottom attached segment">
		<div class="ui two column grid">
			<div class="left aligned column">
				<div class="ui black labeled icon button" onclick="window.history.back()">
					<i class="chevron left icon"></i>
					Kembali
				</div>
			</div>
			<div class="right aligned column">
				<div class="ui right labeled green icon save as page button">
					<i class="save icon"></i>
					Simpan
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function($) {
		tinymceOpt.selector = '.editor';
		tinymceOpt.height = '300';
		tinymce.init(tinymceOpt);

		$('.menu .item').tab();	
	});
	$(document).on('change', '.attachment', function () {
		var thumb = $(this).closest('.image-container').find('.image-preview');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@endsection

