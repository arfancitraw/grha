@extends('layouts.scaffold')

@section('content')
    @section('content-header')
        <div class="title-container">
            <div class="ui breadcrumb">
                <a href="#" class="section">
                    <i class="home icon"></i>
                </a>
                <?php $i=1; $last=count($breadcrumb);?>
                @foreach ($breadcrumb as $name => $link)
                    <i class="right chevron icon divider"></i>
                    @if($i++ != $last)
                        <a href="{{ $link }}" class="section">{{ $name }}</a>
                    @else
                        <div class="active section">{{ $name }}</div>
                    @endif
                @endforeach
            </div>
            <h2 class="ui header">
                <div class="content">
                    {!! $title or '-' !!}
                    {{-- <div class="sub header">{!! $subtitle or ' ' !!}</div> --}}
                </div>
            </h2>
        </div>
    @show

    <div class="ui clearing divider" style="border-top: none !important; margin:10px"></div>

    @yield('content-body')
@endsection
