<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Pusat Jantung Nasional Harapan Kita</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap/dist/css/bootstrap.min.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/font-awesome/css/fontawesome-all.min.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/select2/select2.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/sweetalert/sweetalert2.min.css') }} ">
  {{-- <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datepicker/datepicker.min.css') }} "> --}}
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/colorbox/colorbox.css') }} ">
  
  <style type="text/css">
    .datepicker{
      padding:.375rem .75rem !important;
    }

    .content-main img{
      cursor:zoom-in;
    }
  </style>
  @yield('css')
  
  <link rel="stylesheet" type="text/css" href="{{ asset('css/front.css') }} ">
  <style type="text/css">
    .jumbotron.karir:before {
      background: url('{{ url('storage/'.$settings['jumbotron-litbang']['isi']) }}') top center no-repeat transparent !important;
      background-size: cover !important;
      background-attachment: fixed !important;
    }
    .jumbotron.service:before {
      background: url('{{ url('storage/'.$settings['jumbotron-service']['isi']) }}') top center no-repeat transparent !important;
      background-size: cover !important;
      background-attachment: fixed !important;
    }
    .jumbotron.rujukan:before {
      background: url('{{ url('storage/'.$settings['jumbotron-rujukan']['isi']) }}') top center no-repeat transparent !important;
      background-size: cover !important;
      background-attachment: fixed !important;
    }
    .jumbotron.pasien:before {
      background: url('{{ url('storage/'.$settings['jumbotron-informasi']['isi']) }}') top center no-repeat transparent !important;
      background-size: cover !important;
      background-attachment: fixed !important;
    }
    .jumbotron.tim-dokter:before {
      background: url('{{ url('storage/'.$settings['jumbotron-dokter']['isi']) }}') top center no-repeat transparent !important;
      background-size: cover !important;
      background-attachment: fixed !important;
    }
    .jumbotron.whistleblowing:before {
      background: url('{{ url('storage/'.$settings['jumbotron-whistleblowing']['isi']) }}') top center no-repeat transparent !important;
      background-size: cover !important;
      background-attachment: fixed !important;
    }
    .jumbotron.media:before {
      background: url('{{ url('storage/'.$settings['jumbotron-media']['isi']) }}') top center no-repeat transparent !important;
      background-size: cover !important;
      background-attachment: fixed !important;
    }
    .jumbotron.profil:before {
      background: url('{{ url('storage/'.$settings['jumbotron-profil']['isi']) }}') top center no-repeat transparent !important;
      background-size: cover !important;
      background-attachment: fixed !important;
    }
    .jumbotron.mutu:before {
      background: url('{{ url('storage/'.$settings['jumbotron-mutu']['isi']) }}') top center no-repeat transparent !important;
      background-size: cover !important;
      background-attachment: fixed !important;
    }
    /* google translate */
    /*.goog-te-banner, .goog-te-gadget{
      display: none !important;
    }*/
  </style>
  @yield('styles')
</head>

<body id="app">
  @include('partials.frontend.global-form')

  @yield('body')

  @include('partials.frontend.modal-kuesioner')
  
  @include('partials.frontend.modal-janji')

  @if(session('lang') == 'english')
  <div id="google_translate_element"></div>
  @endif

  @yield('modals')

  <div id="zoomer-modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-zoomer">
      <img id="zoomer" src="" alt="" style="width:100%; display:block">
    </div>
  </div>

  <script src="{{ asset('js/es6-promise.auto.min.js') }}"></script>

  <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
  <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
  <script src="{{ asset('plugins/daterangepicker/moment.js') }}"></script>
  <script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
  <script src="{{ asset('plugins/sweetalert/sweetalert2.min.js') }}"></script>
  <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
  <script src="{{ asset('plugins/daterangepicker/moment.js') }}"></script>
  <script src="{{ asset('plugins/jQuery/jquery.form.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
  {{-- <script src="{{ asset('plugins/zoom/jquery.zoom.js') }}"></script> --}}
  {{-- <script src="{{ asset('plugins/colorbox/jquery.colorbox.js') }}"></script> --}}
  {{-- <script src="{{ asset('plugins/timepicker/lib/bootstrap-datepicker.js') }}"></script> --}}
  <script src="{{ asset('js/axios.min.js') }}"></script>
  @yield('js')
  
  <script src="{{ asset('js/front.js') }}"></script>
  <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  
  <script>
    @if(session('lang') == 'english')
    function googleTranslateElementInit() {
      $.when(
        new google.translate.TranslateElement({
          pageLanguage: 'id', 
          includedLanguages: 'en', 
          autoDisplay: true
        }, 'google_translate_element')
      ).done(function(){
          // var select = document.getElementsByClassName('goog-te-combo')[0];
          // select.selectedIndex = 1;
          // select.addEventListener('click', function () {
          //     select.dispatchEvent(new Event('change'));
          // });
          // select.click();

          setTimeout(function(e){
            var a = document.querySelector("#google_translate_element select");
            a.selectedIndex=1;
            a.dispatchEvent(new Event('change'));
            $('#google_translate_element select').val('en');
          console.log(a);
            $('.goog-te-button button').trigger('click');
            $('#navbarCollapse').css('font-size', '.9rem');
            $('#app').css('top', '0 !important');
          }, 2000)
      });
    }
    @endif

    $(document).ready(function($) {
        $('.datepicker').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy'
        })
        $('.select2').select2({
          minimumResultsForSearch: -1
        })
        $('.select2-drawer').select2({
          minimumResultsForSearch: -1,
          containerCssClass: "drop",
          dropdownCssClass: "drawer-selector"
        })

        $('.content-main img').click(function(event) {
          $('#zoomer').attr('src', this.src)
                      .attr('alt', this.alt);
          if(this.width > this.height){
            $('#zoomer-modal .modal-dialog').width(($(window).width() - 100)+"px");
          }else{
            $('#zoomer-modal .modal-dialog').width("800px");
          }

          $('#zoomer-modal').modal('show');
        });
        
        // $('.select2-drawer').select2().data('select2').$dropdown.addClass('drawer-selector');
    });

    // submit
    $('.kuesioner .btn-submit').on('click', function(e) {

      var parent_fieldset = $(this).parents('fieldset');
      var ok = true;
      
      // fields validation
      parent_fieldset.find('input[type="text"], input[type="password"], input[type="email"], textarea').each(function() {
        if( $(this).val() == "" ) {
          $(this).addClass('input-error');
          ok = false
        }
        else {
          $(this).removeClass('input-error');
        }
      });
      // fields validation

      if(ok){
        //Ngambil data dari form
        var formData = $('#kuesionerForm').serializeArray();
        formData.push({ name: "_token", value: "{{ csrf_token() }}" });
        // bikin request
        $.ajax({
          url: "{{ url('kuesioner/nilai') }}",
          dataType : 'json',
          type: 'POST',
          data: formData,
          success:function(response) {
            swal(
              'Terkirim',
              'Terima kasih telah mengikuti kuesioner singkat yang kami sediakan.',
              'success'
              ).then(
              $('#kuesionerModal').modal('hide')
              )
            }
          });
      }
    });

    //testimoni
    $('.testimoni .btn-submit').on('click', function(e) {

      var parent_fieldset = $(this).parents('fieldset');
      var ok = true;
      
      // fields validation
      parent_fieldset.find('input[type="text"], input[type="password"], input[type="email"], textarea').each(function() {
        if( $(this).val() == "" ) {
          $(this).addClass('input-error');
          ok = false
        }
        else {
          $(this).removeClass('input-error');
        }
      });
      // fields validation

      if(ok){
        //Ngambil data dari form
        var formData = $('#testimoniForm').serializeArray();
        formData.push({ name: "_token", value: "{{ csrf_token() }}" });
        // bikin request
        $.ajax({
          url: "{{ url('testimoni/nilai') }}",
          dataType : 'json',
          type: 'POST',
          data: formData,
          success:function(response) {
            swal(
              'Terkirim',
              'Terima kasih telah mengikuti testimoni singkat yang kami sediakan.',
              'success'
              ).then(
              $('#testimoniModal').modal('hide')
              )
            }
          });
      }
    });

    $(document).on('change','.custom-file-input',function(){
        var fileName = $(this).val();
        var string = fileName.split('\\');
        $(this).next('.custom-file-label').addClass("selected").html(string[string.length - 1]);
    });

    var myApp;
    myApp = myApp || (function () {
        var pleaseWaitDiv = $(`<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header"><h1>Processing...</h1></div>
              <div class="modal-body">
                <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>`);
        
        return {
            showPleaseWait: function() {
                pleaseWaitDiv.modal();
            },
            hidePleaseWait: function () {
                pleaseWaitDiv.modal('hide');
            },

        };
    })();
  </script>
  @yield('scripts')
</body>
</html>
