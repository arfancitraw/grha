<script type="text/javascript">
	function menganu(){
		var slug = convertToSlug($('input[name="judul"]').val())
		console.log('slug',slug);
		$('input[name="slug"]').val(slug);
		console.log('asdas',$('input[name="slug"]').val(slug));
	}
	function convertToSlug(Text){
		return Text
		.toLowerCase()
		.replace(/ /g,'-')
		.replace(/[^\w-]+/g,'')
		;
	}
	function loadModal(url) {
		$('#formModal').modal({
				// inverted: true,
				observeChanges: true,
				closable: false,
				detachable: false, 
				autofocus: false,
				onApprove : function() {
					// self.modal('refresh');
					$("#dataForm").form('validate form');
					
					if($("#dataForm").form('is valid')){
						$('#formModal').find('.loading.dimmer').addClass('active');
						$("#dataForm").ajaxSubmit({
							beforeSend: function(f){
								$(".numeral").inputmask('unmask');
							},
							success: function(resp){
								$("#formModal").modal('hide');
								swal(
								'Tersimpan!',
								'Data berhasil disimpan.',
								'success'
								).then((result) => {
									if(!postRequest()){
										dt.draw();
									}
									return true;
								})
							},
							error: function(resp){
								$('#formModal').find('.loading.dimmer').removeClass('active');
								var error = $('<ul class="list"></ul>');
								console.log(resp.responseJSON);
								$.each(resp.responseJSON, function(index, val) {
									error.append('<li>'+val+'</li>');
								});
								
								$('#formModal').find('.ui.error.message').html(error).show();
							}
						});	
					}
					return false;
				},
				onShow: function(){
					$('#formModal').find('.loading.dimmer').addClass('active');
					
					$(this).draggable({
						cancel: ".note-editable, input, select, textarea, .ui.calendar, .ui.radio"
					});
					
					$.get(url, { _token: "{{ csrf_token() }}", @yield('filter_detail') } )
					.done(function( response ) {
						$('#formModal').html(response);
						$('#dataForm').form({
							inline: true,
							fields: formRules
						});
						
						$('.ui.dropdown').dropdown();
						initModal();
					});
				},
				onHidden: function(){
					$('#formModal').html(`<div class="ui inverted loading dimmer">
						<div class="ui text loader">Loading</div>
					</div>`);
					unModal();
				}
			}).modal('show');
	}
	function formatRupiah($val=0){
		var bilangan = $val;
		var	number_string = bilangan.toString(),
		sisa 	= number_string.length % 3,
		rupiah 	= number_string.substr(0, sisa),
		ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
		
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}
		return rupiah;
	}
	function postNewTab(url, param){
		var form = document.createElement("form");
		form.setAttribute("method", 'POST');
		form.setAttribute("action", url);
		form.setAttribute("target", "_blank");

		$.each(param, function(key, val) {
			var inputan = document.createElement("input");
			inputan.setAttribute("type", "hidden");
			inputan.setAttribute("name", key);
			inputan.setAttribute("value", val);
			form.appendChild(inputan);
		});

		document.body.appendChild(form);
		form.submit();

		document.body.removeChild(form);
	}
	function saveForm(container = '#formModal'){
		$("#dataForm").form('validate form');
					
		if($("#dataForm").form('is valid')){
			$('#cover').show();
			$("#dataForm").ajaxSubmit({
				beforeSend: function(f){
					$(".numeral").inputmask('unmask');
				},
				success: function(resp){
					$(container).modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							$('#cover').hide();
							dt.draw();
							return true;
						})
					},
					error: function(resp){
						$(container).find('.loading.dimmer').removeClass('active');
						var error = $('<ul class="list"></ul>');
						// console.log(resp.responseJSON);
						$.each(resp.responseJSON, function(index, val) {
							error.append('<li>'+val+'</li>');
						});

						$(container).find('.ui.error.message').html(error).show();
					}
				});
		}
	}
	function saveModalForm(container = '#formModal'){
		$("#modalForm").form('validate form');
					
		if($("#modalForm").form('is valid')){
			$('#cover').show();
			$("#modalForm").ajaxSubmit({
				beforeSend: function(f){
					$(".numeral").inputmask('unmask');
				},
				success: function(resp){
					$(container).modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
						).then((result) => {
							$('#cover').hide();
							location.reload();
							return true;
						})
					},
					error: function(resp){
						$(container).find('.loading.dimmer').removeClass('active');
						var error = $('<ul class="list"></ul>');
						// console.log(resp.responseJSON);
						$.each(resp.responseJSON, function(index, val) {
							error.append('<li>'+val+'</li>');
						});

						$(container).find('.ui.error.message').html(error).show();
					}
				});
		}
	}
	function saveFormPage(){
		$('.ui.negative').hide();
		$('.negative.message').remove();
		$('.pointing.prompt.label.transition.visible').remove();
		$('.pointing.prompt.label.transition.visible').hide();
		$('.error').each(function (index, val) {
			$(val).removeClass('error');
		});
		$("#dataForm").form('validate form');
		if($("#dataForm").form('is valid')){
			$('#cover').show();
			$("#dataForm").ajaxSubmit({
				beforeSend: function(f){
					$(".numeral").inputmask('unmask');
				},
				success: function(resp){
					$("#formModal").modal('hide');
					swal(
						'Tersimpan!',
						'Data berhasil disimpan.',
						'success'
					).then((result) => {
						$('#cover').hide();
						window.location = "{{ url($pageUrl) }}";
						return true;
					})
				},
				error: function(resp){
					console.log(resp);
					var html = `<div class="ui negative message">
						<i class="close icon" id="close"></i>
						<div class="header">
						Data harus dilengkapi terlebih dahulu
						</div>
						<p>Semua data mohon diisi dan dilengkapi terlebih dahulu, Dan pastikan isi data Lainnya.
						</p></div>`;
						$('#cover').hide();
						$('.ui.error.message').prepend(html);
						$('#inner2bottom').prepend(html);
					$.each(resp.responseJSON, function(index, val) {
						clearFormError(index,val);
						showFormError(index,val);
					});
					time = 5;
					interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.ui.negative').hide();
								$('.negative.message').remove();
								$('.pointing.prompt.label.transition.visible').remove();
								$('.error').each(function (index, val) {
									$(val).removeClass('error');
								});
							}
					},1000)
				}
			});
		}
	}
	function readURL(input) {
		if(input.files[0].size > 5242880){
			swal(
				"Gagal",
				"Ukuran file terlalu besar, silahkan upload file dengan ukuran dibawah 5MB",
				"error"
			)
			$("#attachment").val("");
		}else{
			if(input.files && input.files[0])
			{
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#showAttachment').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}
	}

$(document).ready(function() {
		function readURL(input) {
			if 
				(input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('#showPic').attr('src', e.target.result);
					}

					reader.readAsDataURL(input.files[0]);
				}
			}
			var lengthAttachment = $('input[name^="attachment"]').length;

			function getAttachmentInput(length) {
				$('input[name="attachment['+length+']"]').click()
			}

			function multiAppendUrl(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$('img[name="img['+lengthAttachment+']"]').parent().parent().show();
						$('img[name="img['+lengthAttachment+']"]').attr('src', e.target.result);
					}
					lengthAttachment = lengthAttachment + 1;
					var html = `<div class="card">
					<div class="image">
					<img class="ui image" src="" name="img[`+lengthAttachment+`]">
					<input type="file" name="attachment[`+lengthAttachment+`]" style="display:none;" accept="image/*">
					</div>
					</div>`;
					$('#multiShowPic').append(html);
					reader.readAsDataURL(input.files[0]);

					$('input[name="attachment['+lengthAttachment+']"]').change(function () {
						multiAppendUrl(this);
					});
				}
			}

			$(document)
			.on('click', '.ui.teal.button.safety-file', function(e) {
				getAttachmentInput(lengthAttachment);
			});

			$("#browsePic").change(function () {
				readURL(this);
			});

			$('input[name="attachment['+lengthAttachment+']"]').change(function () {
				multiAppendUrl(this);
			});

			clearError = function(key, value)
			{
				if(key.includes("."))
				{
					res = key.split('.');
					key = res[0] + '[' + res[1] + ']';
					if(res[1] == 0)
					{
						key = res[0] + '\\[\\]';
					}
					console.log(key);
				}
				var elm = $('#formModal' + ' [name^=' + key + ']').closest('.field');
				$(elm).removeClass('error');

				var showerror = $('#formModal' + ' [name^=' + key + ']').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
			}

			showError = function(key, value)
			{
				if(key.includes("."))
				{
					res = key.split('.');
					key = res[0] + '[' + res[1] + ']';
					if(res[1] == 0)
					{
						key = res[0] + '\\[\\]';
					}
					console.log(key);
				}

				var elm = $('#formModal' + ' [name^=' + key + ']').closest('.field');
				$(elm).addClass('error');
				var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;

				var showerror = $('#formModal' + ' [name^=' + key + ']').closest('.field');
				$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
			}

			clearElementError = function(key, value, element)
			{
				if(key.includes("."))
				{
					res = key.split('.');
					key = res[0] + '[' + res[1] + ']';
					if(res[1] == 0)
					{
						key = res[0] + '\\[\\]';
					}
					console.log(key);
				}
				var elm = $(element + ' [name^=' + key + ']').closest('.field');
				$(elm).removeClass('error');

				var showerror = $(element + ' [name^=' + key + ']').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
			}

			showElementError = function(key, value, element)
			{
				if(key.includes("."))
				{
					res = key.split('.');
					key = res[0] + '[' + res[1] + ']';
					if(res[1] == 0)
					{
						key = res[0] + '\\[\\]';
					}
					console.log(key);
				}

				var elm = $(element + ' [name^=' + key + ']').closest('.field');
				$(elm).addClass('error');
				var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;

				var showerror = $(element + ' [name^=' + key + ']').closest('.field');
				$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
			}

			showFormError = function(key, value)
			{
				if(key.includes("."))
				{
					res = key.split('.');
					key = res[0] + '[' + res[1] + ']';
					if(res[1] == 0)
					{
						key = res[0] + '\\[\\]';
					}
					console.log(key);
				}
				var elm = $('#dataForm' + ' [name=' + key + ']').closest('.field');
				$(elm).addClass('error');
				var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;

				var showerror = $('#dataForm' + ' [name=' + key + ']').closest('.field');
				$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
			}

			clearFormError = function(key, value)
			{
				// if(key.includes("."))
				// {
				// 	res = key.split('.');
				// 	key = res[0] + '[' + res[1] + ']';
				// 	if(res[1] == 0)
				// 	{
				// 		key = res[0] + '\\[\\]';
				// 	}
				// 	console.log(key);
				// }
				// var elm = $('#dataForm' + ' [name=' + key + ']').closest('.field');
				// $(elm).removeClass('error');

				// var showerror = $('#dataForm' + ' [name=' + key + ']').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
			}

			showFormErrorSafety = function(key, value)
			{
				if(key.includes("."))
				{
					res = key.split('.');
					key = res[0] + '[' + res[1] + ']';
					if(res[1] == 0)
					{
						key = res[0] + '\\[\\]';
					}
					console.log(key);
				}
				var elm = $('#dataForm' + ' [name=' + key + ']').closest('.field');
				$(elm).addClass('error');
				var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;

				var showerror = $('#dataForm' + ' [name=' + key + ']').closest('.field');
				$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
			}

			clearFormErrorSafety = function(key, value)
			{
				if(key.includes("."))
				{
					res = key.split('.');
					key = res[0] + '[' + res[1] + ']' + '[]';
					if(res[1] == 0)
					{
						key = res[0] + '\\[\\]';
					}
					console.log('clearFormErrorSafety',key);
				}
				var elm = $('#dataForm' + ' input[name=' + key + ']').closest('.field');
				$(elm).removeClass('error');

				var showerror = $('#dataForm' + ' [name=' + key + ']').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
			}

			showFormErrorEquip = function(key, value)
			{
				if(key.includes("."))
				{
					res = key.split('.');
					key = res[0] + '[' + res[1] + ']' + '[]';
					if(res[1] == 0)
					{
						key = res[0] + '\\[\\]';
					}
					console.log('showFormErrorEquip',key);
				}
				var elm = $('#dataFormss' + ' input[name=' + key + ']').closest('.field');
				$(elm).addClass('error');
				var message = `<div class="ui basic red pointing prompt label transition visible">`+ value +`</div>`;

				var showerror = $('#dataFormss' + ' [name=' + key + ']').closest('.field');
				$(showerror).append('<div class="ui basic red pointing prompt label transition visible">' + value + '</div>');
			}

			clearFormErrorEquip = function(key, value)
			{
				if(key.includes("."))
				{
					res = key.split('.');
					key = res[0] + '[' + res[1] + ']';
					if(res[1] == 0)
					{
						key = res[0] + '\\[\\]';
					}
					console.log(key);
				}
				var elm = $('#dataFormss' + ' [name=' + key + ']').closest('.field');
				$(elm).removeClass('error');

				var showerror = $('#dataFormss' + ' [name=' + key + ']').closest('.field').find('.ui.basic.red.pointing.prompt.label.transition.visible').remove();
			}

		});

$(document).on('submit', '#dataForm', function(e){
	return false;
});

$(document).on('submit', '#modalForm', function(e){
	return false;
});

$(document).on('keydown', '.field input', function(e){
	if(e.keyCode == 13){
		e.preventDefault();
		$( ".save.button" ).trigger( "click" );
	}
});

$(document).on('click', '.add.button', function(e){
	var url = "{{ url($pageUrl) }}/create";

	loadModal(url);
});

// $(document).on('click', '.save.button', function(e){
// 	saveForm();
// });

$(document).on('click', '.save-modal.button', function(e){
	saveModalForm();
});

$(document).on('click', '.add.button.uom', function(e) {
	addUom();
});

$(document).on('click', '.remove.button.uom', function(e) {
	removeUom();
});

$(document).on('click', '.add.button.uomp', function(e) {
	addUomp();
});

$(document).on('click', '.remove.button.uomp', function(e) {
	removeUomp();
});

$(document).on('click', '.edit.button', function(e){
	var id = $(this).data('id');
	var url = "{{ url($pageUrl) }}/"+id+"/edit";

	loadModal(url);
});

$(document).on('click', '.add-page.button', function(e){
	var url = "{{ url($pageUrl) }}/create";
	window.location = url;
});

$(document).on('click', '.edit-page.button', function(e){
	var id = $(this).data('id');
	var url = "{{ url($pageUrl) }}/"+id+"/edit";
	window.location = url;
});

$(document).on('click', '.details-page.button', function(e){
	var id = $(this).data('id');
	var url = "{{ url($pageUrl) }}/"+id+"/details";
	window.location = url;
});

$(document).on('click', '.delete.button', function(e){
	var id = $(this).data('id');

	swal({
		title: 'Apakah anda yakin?',
		text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Hapus',
		cancelButtonText: 'Batal'
	}).then((result) => {
		if (result) {
			$.ajax({
				url: '{{ url($pageUrl) }}/'+id,
				type: 'POST',
				data: {_token: "{{ csrf_token() }}", _method: "delete"},
				success: function(resp){
					swal(
						'Terhapus!',
						'Data berhasil dihapus.',
						'success'
					).then(function(e){
						if(!postRequest()){
							dt.draw();
						}
					});
				},
				error : function(resp){
					var data = resp.responseJSON;
					console.log(data)
					swal(
						'Gagal!',
						Object.is(data.message, undefined) ? 'Data gagal dihapus, eror tidak diketahui' : data.message,
						'error'
						).then(function(e){
							dt.draw();
						});
					}
				});

		}
	})
});

$(document).on('click', '.activate.button', function(e){
	var id = $(this).data('id');

	swal({
		title: 'Apakah anda yakin?',
		text: "Data lain akan dinonaktifkan jika anda mengaktifkan salah satu data!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Aktifkan',
		cancelButtonText: 'Batal'
	}).then((result) => {
		if (result) {
			$.ajax({
				url: '{{ url($pageUrl) }}/'+id+'/activate',
				type: 'POST',
				data: {_token: "{{ csrf_token() }}"},
				success: function(resp){
					swal(
						'Aktivasi!',
						'Data berhasil diaktivasi.',
						'success'
						).then(function(e){
							dt.draw();
						});
					},
					error : function(resp){
						swal(
							'Gagal!',
							'Data gagal diaktivasi.',
							'error'
							).then(function(e){
								dt.draw();
							});
						}
					});

		}
	})
});

$(document).on('change', '#attachment, .attachment', function () {
	readURL(this);
});

$(document).on('click', '.terima.button', function(e){
	var id = $(this).data('id');

	swal({
		title: 'Apakah Data Sudah Benar?',
		// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Terima',
		cancelButtonText: 'Batal'
	}).then((result) => {
		if (result) {
			$.ajax({
				url: '{{ url($pageUrl) }}/'+id,
				type: 'POST',
				data: {_token: "{{ csrf_token() }}", _method: "delete"},
				success: function(resp){
					swal(
					'Berhasil!',
					'Kartu Telah Diterima',
					'success'
					).then(function(e){
						dt.draw();
					});
				},
				error : function(resp){
					swal(
					'Gagal!',
					'error'
					).then(function(e){
						dt.draw();
					});
				}
			});
			
		}
	})
});

$(document).on('click', '.save.as.drafting', function(e){
	$('#dataForm').find('input[name="status"]').val("0");
	saveForm();
});

$(document).on('click', '.save.page', function(e){
	swal({
		title: 'Apakah Anda Yakin?',
		// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Simpan',
		cancelButtonText: 'Batal'
	}).then((result) => {
		if (result) {
			saveFormPage();
		}
	})
});

$(document).on('click', '.saves.as.drafting.page', function(e){
	swal({
		title: 'Apakah Anda Yakin?',
			// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Simpan',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$('#dataForm').find('input[name="status"]').val("0");
				saveFormPage();
			}
		})
	});

$(document).on('click', '.saves.as.publicity.page', function(e){
	// console.log('sadas');
	swal({
		title: 'Apakah Anda Yakin?',
			// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Simpan',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result) {
				$('#dataForm').find('input[name="status"]').val("1");
				saveFormPage();
			}
		})
	});

	$(document).on('click', '.close', function(e){
		$('.ui.negative').hide();
	})

$(document).on('click', '.save.as.publicity', function(e){
	swal({
		title: 'Apakah Anda Yakin Sudah Siap Publish?',
		// text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Publish',
		cancelButtonText: 'Batal'
	}).then((result) => {
		if (result) {
			$('#dataForm').find('input[name="status"]').val("1");
			saveForm();
		}
	})
});

$(document).on('click','.remove.image',function(event) {
	var link = $(this).data('url');
	swal({
		title: 'Apakah Anda Yakin?',
		text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Hapus',
		cancelButtonText: 'Batal'
	}).then((result) => {
		if (result) {
			$.get(link, function(e){
				swal(
					'Terhapus!',
					'Data berhasil dihapus.',
					'success'
					).then((result) => {
						location.reload();
						return true;
					})
			});
		}
	})
});

$(document).on('keyup', '.filter.form input', function(event) {
	/* Act on the event */
    event.preventDefault();
    if (event.keyCode === 13) {
        $('.filter.button').click();
    }
});
</script>