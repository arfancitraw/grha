<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'GRHA KEDOYA') }}</title>

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/fontawesome-all.min.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('css/login-style.css') }}">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=zXr4R5YArk">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=zXr4R5YArk">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=zXr4R5YArk">
    <link rel="manifest" href="/manifest.json?v=zXr4R5YArk">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=zXr4R5YArk" color="#5bbad5">
    <link rel="shortcut icon" href="/favicon.ico?v=zXr4R5YArk">
    <meta name="theme-color" content="#ffffff">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body>
    <!-- Top content -->
    <div class="top-content">

        {{-- <div class="inner-bg"> --}}
            <div class="container">
                <div class="row">
                    <div class="col-md-6 d-flex p-md-5 justify-content-center align-items-center">
                        {{-- <div class="description logo">
                            <h1>HSE PORTAL MANAGEMENT<br><small>AP2B Kalbar</small></h1>
                        </div> --}}
                        <div>
                            <img src="{{url('img/grha-icon.png')}}" class="baru" alt="Logo"/>
                            {{-- <div class="description d-none d-sm-block">
                                <h1><small>pjnhk.go.id</small></h1>
                            </div> --}}
                        </div>
                    </div>
                    <div class="col-md-6 d-flex p-md-5 justify-content-center align-items-center form-box">
                        <div>
                            <img src="{{url('img/login-icon.png')}}" alt="Logo"/>
                            <h2 class="title my-4">{{ config('app.longname', 'GRHA KEDOYA') }}</h2>
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>

        {{-- </div> --}}

    </body>

    </html>
