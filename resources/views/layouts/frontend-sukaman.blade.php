<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>{{ config('app.name') }} | Paviliun Sukaman</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

  <link href="{{ asset('plugins/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/font-awesome/css/fontawesome-all.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/magnific-popup/magnific-popup.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/sweetalert/sweetalert2.min.css') }} ">

  <link href="{{ asset('sk/css/animate.css') }}" rel="stylesheet">
  @yield('css')
  
  <link href="{{ asset('sk/css/style.css') }}" rel="stylesheet">
  @yield('styles')
</head>

<body class="home" data-spy="scroll" data-target=".navbar" data-offset="144">

  <div id="loader">
    <img src="{{ asset('sk/img/icon-long2.png') }}" class="d-block" width="280" alt="">
  </div>
  <main role="main">
    @include('partials.sukaman.header')
    @yield('content')
    @include('partials.sukaman.footer')
  </main>

  @yield('modals')

  <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
  <script src="{{ asset('plugins/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('plugins/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{ asset('plugins/sweetalert/sweetalert2.min.js') }}"></script>
  <script src="{{ asset('sk/js/wow.min.js')}}"></script>
  
  @yield('js')
  <script src="{{ asset('sk/js/front.js')}}"></script>
  @yield('scripts')
</body>
</html>
