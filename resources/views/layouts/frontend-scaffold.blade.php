@extends('layouts.frontend')

@section('body')
<header class="fixed-top">
	@include('partials.frontend.header')
</header>

<main role="main">
	@yield('content')
</main>

<footer class="footer">
	@include('partials.frontend.footer')
</footer>
@endsection
