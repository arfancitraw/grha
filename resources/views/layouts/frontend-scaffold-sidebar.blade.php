@extends('layouts.frontend-scaffold')

@section('content')

@yield('jumbotron')

<div class="container">
	<div class="row mb-4">
		<div class="d-none d-md-block col-md-3 d-md-block content-sidebar">
			<aside>
				@yield('content-sidebar')
			</aside>
		</div>

		<div class="col-12 col-md-9 content-main">
			@yield('breadcrumb')
			@yield('content-main')
		</div>
	</div>
</div>
@endsection
