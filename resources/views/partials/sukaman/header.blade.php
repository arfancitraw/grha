<header>
  <div class="header-spacing header-contacts wow fadeInDown" data-wow-delay="1.0s" style="background: #fff;">
    <div class="container d-flex justify-content-between ">      
      <div class="text-left d-none d-md-flex justify-content-start align-items-center">
        <strong>{{ date('l, d F Y') }}&nbsp;&nbsp;<span class="clock"></span></strong>
      </div>
      <div class="text-right d-flex hotline justify-content-end">
        <div class="d-flex align-items-start contact">
          <img src="{{ asset('img/icons/icon_phone.png') }}" class="mr-2 d-none d-md-inline-block" height="40" alt="">
          <span class="d-inline-block">
            <small class="d-inline-block">Call Center:</small><br>1500 034
          </span>
        </div>
        <div class="d-flex align-items-start contact ml-3">
          <img src="{{ asset('img/icons/icon_emergency.png') }}" class="mr-2 d-none d-md-inline-block" height="40" alt="">
          <span class="d-inline-block">
            <small>Emergency Call:</small><br>(021) 568 2424
          </span>
        </div>
      </div>
    </div>
  </div>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top text-white wow fadeInDown" data-wow-delay="1.5s">
    <div class="container">
      <a href="{{ url('/') }}" class="navbar-brand"> 
        <img src="{{ asset('/img/icon-long-white.png') }}" class="d-block" width="280" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="#about">
              <span class="d-none d-md-inline-block">Tentang</span>
              <span class="d-inline-block d-md-none">Tentang Kami <span class="sr-only">(current)</span></span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#service">
              <span class="d-none d-md-inline-block">Layanan</span>
              <span class="d-inline-block d-md-none">Layanan</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#doctor">
              <span class="d-none d-md-inline-block">Dokter</span>
              <span class="d-inline-block d-md-none">Dokter</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#facility">
              <span class="d-none d-md-inline-block">Fasilitas</span>
              <span class="d-inline-block d-md-none">Fasilitas</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#info">
              <span class="d-none d-md-inline-block">Info</span>
              <span class="d-inline-block d-md-none">Informasi Pasien</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#pricing">
              <span class="d-none d-md-inline-block">Tarif</span>
              <span class="d-inline-block d-md-none">Tarif</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#contact">
              <span class="d-none d-md-inline-block">Kontak</span>
              <span class="d-inline-block d-md-none">Hubungi Kami</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>