<footer class="footer">
	<div class="useful-links d-none d-md-block">
		<div class="container py-4">
			<div class="row align-items-top">
				<div class="col-12 col-md-3 justify-content-center text-center">
					<img src="{{ asset('/img/germas.png') }}" class="img-thumbnail mx-auto mb-3 d-block" width="144" alt="">
					<span>PJNHK Mobile</span>

					<a href="#"><img src="{{ asset('/img/mobile-icon.png') }}" class="img mx-auto d-block" width="168" alt="Mobile version"></a>
				</div>
				<div class="col-12 col-md-3">
					<ul class="list-unstyled mb-0">
						@foreach($linkFooter as $lin)
						<li class="mb-2"><a href="{{ url($lin->link) }}">{{ $lin->nama }}</a></li>
						@endforeach
					</ul>
				</div>
				<div class="col-12 col-md-3 justify-content-center">
					<dl class="footer-contact mb-1">
						<h5 class="text-white">@lang('front.contactUs')</h5>
						<dt>Pusat Jantung Nasional Harapan Kita</dt>
						<dd>Jl. Letjen S. Parman Kav 87 Slipi<br>Jakarta Barat 11420</dd>
						<dt>Telepon</dt>  
						<dd>
							<span class="d-block my-1">Call Center: 021-5684090</span>
							<span class="d-block my-1">Emergency Contact: 021-5684090 (Hunting)</span>
						</dd>
						<dt>Email</dt>
						<dd>info@pjnhk.go.id</dd>
					</dl>

				</div>
				<div class="col-12 col-md-3">
					<h5 class="text-white">@lang('front.sertificate')</h5>
					<div class="d-flex my-3">
						@foreach($sertifikatFooter as $sin)
						<a href="{{ $sin->link or '#' }}" class="sertifikat text-center" target="_blank">
							<img src="{{ url('/storage/'.$sin->gambar) }}" alt="{{ $sin->nama }}" width="64" height="64" class="image-cover mr-4">
							<div class="sertifikat-title mr-4">{{ $sin->nama }}</div>
						</a>
						@endforeach
					</div>


					<div id="histats_counter" class="d-flex justify-content-center"></div>
					<script type="text/javascript">var _Hasync= _Hasync|| [];
					_Hasync.push(['Histats.start', '1,4078325,4,1034,150,25,00011111']);
					_Hasync.push(['Histats.fasi', '1']);
					_Hasync.push(['Histats.track_hits', '']);
					(function() {
						var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
						hs.src = ('//s10.histats.com/js15_as.js');
						(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
					})();</script>
					<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4078325&101" alt="site hit counter" border="0"></a></noscript>
				</div>
			</div>
		</div>
	</div>

	<div class="real-footer">
		<div class="container">
			<div class="copyright-text py-2 text-center">
				<span class="d-block d-md-none d-lg-none d-xl-none">© PJNHK 2018</span>
				<span class=" d-none d-md-block">© Copyright PJNHK 2018. All Rights Reserved.</span>
			</div>
		</div>
	</div>
</footer>