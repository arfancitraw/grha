<div class="ui fixed blue inverted menu">
    <a href="{{ url('/home') }}" class="header item">
        <img class="logo" src="{{ asset('img/icon-white.png')}}" style="width:2.2em; margin-right: 5px">&nbsp;&nbsp;
        {{ config('app.shortname') }}
    </a>
    <div class="menu">
        <a href="#" class="toggler icon item" onclick="toggleSidebar()">
            <i class="sidebar icon"></i>
        </a>
    </div>
    {{-- <div class="ui stackable dark blue inverted domain menu">
      @include('partials.backend.menu', ['items' => $mainMenu->roots()])
    </div> --}}
    
    <div class="right domain menu">
        <a class="item" href="{{ url('/') }}" target="_blank">
            <span class="popup" data-tooltip="Lihat halaman depan" data-inverted data-position="bottom center">
                <i class="desktop icon" style="margin-right: 0"></i>
            </span>
        </a>

        @if(auth()->guest())
            @if(!Request::is('auth/login'))
                <a class="item" href="{{ url('/auth/login') }}">Login</a>
            @endif
            @if(!Request::is('auth/register'))
                <a class="item" href="{{ url('/auth/register') }}">Register</a>
            @endif
        @else
            {{-- <div class="ui pointing dropdown toggler item" tabindex="0">
                <div class="floating ui red small label">22</div>
                <i class="ui bell icon" style="margin-right: 0"></i>
                <div class="menu transition hidden" tabindex="-1">
                    <a class="item" href="#">Item Notif 1</a>
                    <a class="item" href="#">Item Notif 2</a>
                    <a class="item" href="#">Item Notif 3</a>
                </div>
            </div> --}}
            <div class="ui pointing dropdown item" tabindex="0">
                <img class="ui avatar image" src="{{ ($photo = auth()->user()->photo) ? url('storage/'.$photo) : asset('img/noImage.png') }}"> &nbsp;<span>{{ auth()->user()->name }}</span> <i class="dropdown icon"></i>
                <div class="menu transition hidden" tabindex="-1">
                    <a class="item" href="{{ url('profile') }}"><i class="user icon"></i> Profil</a>
                    <a class="item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="sign out icon"></i> Logout</a>
                </div>
            </div>
        @endif
        {{-- <div class="ui pointing dropdown item" tabindex="0">
            <img src="{{ isset(auth()->user()->fotos->first()->path)?url(auth()->user()->fotos->first()->path):url('img/profile.png') }}" id="home-photo" style="border-radius: 50%; margin-right: 6px"> 
            {{ auth()->user()->name }} <i class="dropdown icon"></i>
            <div class="menu transition hidden" tabindex="-1">
            	<a href="{{ url('settings/profile') }}" class="item"><i class="user icon"></i> Profil</a>
                <a href="javascript: void(0)" onclick="event.preventDefault(); $('#logout-form').submit();" class="item"><i class="sign out icon"></i>Sign Out</a>
            </div>
        </div> --}}
    </div>
</div>

<form id="logout-form" action="{{ url('/logout') }}" method="POST">
    {{ csrf_field() }}
</form>