<nav class="navbar navbar-expand-md navbar-dark">
    <div class="container">
        <a class="navbar-brand d-block d-md-none" href="{{ url('/') }}">PJNHK</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        {{-- {{ dd($mainMenu) }} --}}

        <div class="collapse navbar-collapse" id="navbarCollapse">
            @php 
                $leftMenu = array_filter($mainMenu, function($item){
                    return !$item['right'];
                });
                $rightMenu = array_filter($mainMenu, function($item){
                    return $item['right'];
                });
            @endphp
            @if(count($leftMenu) > 0)
            <ul class="navbar-nav justify-content-between ml-0 w-lg-80">
                <li class="nav-item d-none d-lg-inline-block">
                    <a class="nav-link" href="{{ url('/') }}"><i class="fa fa-home"></i></a>
                </li>
                @foreach($leftMenu as $menu)
                <li class="nav-item  @if(array_key_exists('childs', $menu) || $menu['tautan'] == 'pengaduan') dropdown @endif">
                    <a class="nav-link @if(array_key_exists('childs', $menu) || $menu['tautan'] == 'pengaduan') dropdown-toggle @endif" 
                        href="{{  $menu['tautan'] == 'pengaduan' ? '#' : $menu['tautan'] }}" 
                        role="button" 
                        id="{{ str_slug($menu['judul']) }}" 
                        @if(array_key_exists('childs', $menu) || $menu['tautan'] == 'pengaduan')
                        data-toggle="dropdown" 
                        aria-haspopup="true" 
                        aria-expanded="false"
                        @endif
                    >
                        {{ $menu['judul'] }}
                    </a>
                    @if($menu['tautan'] == 'pengaduan')
                        <ul class="dropdown-menu" aria-labelledby="{{ str_slug($menu['judul']) }}">
                            <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/etik-hukum') }}">Etik &amp; Hukum</a></li>
                            <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/whistleblowing') }}">Whistleblowing</a></li>
                            <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/masyarakat') }}">Pengaduan Masyarakat</a></li>
                            <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/gratifikasi') }}">Gratifikasi</a></li>
                            <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/sponsor') }}">Sponsor</a></li>
                            

                        </ul>
                    @elseif(array_key_exists('childs', $menu))
                        <ul class="dropdown-menu" aria-labelledby="{{ str_slug($menu['judul']) }}">
                        @foreach($menu['childs'] as $child)
                            @if($child['tautan'] == 'layanan-unggulan')
                                @if(isset($layananUnggulan) && count($layananUnggulan) > 0 )
                                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Layanan Unggulan&ensp;</a>
                                    <ul class="dropdown-menu">
                                        @foreach($layananUnggulan as $lu)
                                        <li><a class="dropdown-item" href="{{ url('rujukan-nasional/detail/'.$lu->id) }}">{{ $lu->judul }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endif
                            @elseif($child['tautan'] == 'pengaduan')
                                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Pengaduan&ensp;</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/etik-hukum') }}">Etik &amp; Hukum</a></li>
                                        <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/whistleblowing') }}">Whistleblowing</a></li>
                                        <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/masyarakat') }}">Pengaduan Masyarakat</a></li>
                                        <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/gratifikasi') }}">Gratifikasi</a></li>
                                        <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/sponsor') }}">Sponsor</a></li>
                                        

                                    </ul>
                                </li>
                            @elseif($child['tautan'] == 'separator')
                                <div class="dropdown-divider"></div>
                            @elseif(array_key_exists('childs', $child))
                                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">{{ $child['judul'] }}&ensp;</a>
                                    <ul class="dropdown-menu">
                                        @foreach($child['childs'] as $grandchild)
                                        @if($grandchild['tautan'] == 'separator')
                                            <div class="dropdown-divider"></div>
                                        @else
                                            <li><a class="dropdown-item" href="{{ $grandchild['tautan'] }}">{{ $grandchild['judul'] }}</a></li>
                                        @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li><a class="dropdown-item" href="{{ $child['tautan'] }}">{{ $child['judul'] }}</a></li>
                            @endif
                        @endforeach
                        </ul>
                    @endif
                </li>
                @endforeach
            </ul>
            <ul class="navbar-nav justify-content-between ml-auto mr-0">
                @foreach($rightMenu as $menu)
                <li class="nav-item  @if(array_key_exists('childs', $menu) || $menu['tautan'] == 'pengaduan') dropdown @endif">
                    <a class="nav-link @if(array_key_exists('childs', $menu) || $menu['tautan'] == 'pengaduan') dropdown-toggle @endif" 
                        href="{{  $menu['tautan'] == 'pengaduan' ? '#' : $menu['tautan'] }}" 
                        role="button" 
                        id="{{ str_slug($menu['judul']) }}" 
                        @if(array_key_exists('childs', $menu) || $menu['tautan'] == 'pengaduan')
                        data-toggle="dropdown" 
                        aria-haspopup="true" 
                        aria-expanded="false"
                        @endif
                    >
                        {{ $menu['judul'] }}
                    </a>
                    @if($menu['tautan'] == 'pengaduan')
                        <ul class="dropdown-menu" aria-labelledby="{{ str_slug($menu['judul']) }}">
                            <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/etik-hukum') }}">Etik &amp; Hukum</a></li>
                            <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/whistleblowing') }}">Whistleblowing</a></li>
                            <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/masyarakat') }}">Pengaduan Masyarakat</a></li>
                            <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/gratifikasi') }}">Gratifikasi</a></li>
                                        <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/sponsor') }}">Sponsor</a></li>
                            

                        </ul>
                    @elseif(array_key_exists('childs', $menu))
                        <ul class="dropdown-menu" aria-labelledby="{{ str_slug($menu['judul']) }}">
                        @foreach($menu['childs'] as $child)
                            @if($child['tautan'] == 'layanan-unggulan')
                                @if(isset($layananUnggulan) && count($layananUnggulan) > 0 )
                                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Layanan Unggulan&ensp;</a>
                                    <ul class="dropdown-menu">
                                        @foreach($layananUnggulan as $lu)
                                        <li><a class="dropdown-item" href="{{ url('rujukan-nasional/detail/'.$lu->id) }}">{{ $lu->judul }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endif
                            @elseif($child['tautan'] == 'pengaduan')
                                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Pengaduan&ensp;</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/etik-hukum') }}">Etik &amp; Hukum</a></li>
                                        <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/whistleblowing') }}">Whistleblowing</a></li>
                                        <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/masyarakat') }}">Pengaduan Masyarakat</a></li>
                            <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/gratifikasi') }}">Gratifikasi</a></li>
                                        <li><a class="dropdown-item" href="{{ url('info-pasien/pengaduan/sponsor') }}">Sponsor</a></li>
                            

                                    </ul>
                                </li>
                            @elseif($child['tautan'] == 'separator')
                                <div class="dropdown-divider"></div>
                            @elseif(array_key_exists('childs', $child))
                                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">{{ $child['judul'] }}&ensp;</a>
                                    <ul class="dropdown-menu">
                                        @foreach($child['childs'] as $grandchild)
                                        @if($grandchild['tautan'] == 'separator')
                                            <div class="dropdown-divider"></div>
                                        @else
                                            <li><a class="dropdown-item" href="{{ $grandchild['tautan'] }}">{{ $grandchild['judul'] }}</a></li>
                                        @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li><a class="dropdown-item" href="{{ $child['tautan'] }}">{{ $child['judul'] }}</a></li>
                            @endif
                        @endforeach
                        </ul>
                    @endif
                </li>
                @endforeach
            </ul>
            @else
                <ul class="navbar-nav justify-content-between ml-0 w-lg-80">
                    <li class="nav-item d-none d-lg-inline-block">
                        <a class="nav-link" href="{{ url('/') }}"><i class="fa fa-home"></i></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" id="profile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Profil
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="profile">
                            @if(isset($profil) && count($profil) > 0 )
                            @foreach($profil as $pr)
                            <li><a class="dropdown-item" href="{{ url('profil/'.$pr->slug) }}">{{ $pr->judul }}</a></li>
                            @endforeach
                            @endif
                        </ul>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Pelayanan
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            @if(isset($layananUnggulan) && count($layananUnggulan) > 0 )
                            <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Layanan Unggulan</a>
                                <ul class="dropdown-menu">
                                    @foreach($layananUnggulan as $lu)
                                    <li><a class="dropdown-item" href="{{ url('rujukan-nasional/detail/'.$lu->id) }}">{{ $lu->judul }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <div class="dropdown-divider"></div>
                            @endif

                            @if(isset($layanan) && count($layanan) > 0 )
                            @foreach($layanan as $lu)
                            <li><a class="dropdown-item" href="{{ url('pelayanan/'.$lu->slug) }}">{{ $lu->judul }}</a></li>
                            @endforeach
                            @endif

                            <div class="dropdown-divider"></div>
                            <li>
                                <a class="dropdown-item" href="{{ url('mutu') }}">Indikator Mutu</a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" id="menu-dokter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dokter
                        </a>

                        <div class="dropdown-menu" aria-labelledby="menu-dokter">
                            <a class="dropdown-item" href="{{ url('tim-dokter') }}">Profil Dokter</a>
                            <a class="dropdown-item" href="{{ url('jadwal-dokter') }}">Jadwal Dokter</a>
                            <a class="dropdown-item" href="{{ url('info-pasien/tanya-jawab') }}">Tanya Jawab</a>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('rujukan-nasional') }}">Rujukan <span class="d-md-none d-lg-inline">Nasional</span></a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('diklat') }}">Diklat</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('litbang') }}">Litbang</a>
                    </li>

                    @if(count($informasi) > 0)
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" id="info-pasien" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Informasi
                        </a>

                        <div class="dropdown-menu" aria-labelledby="info-pasien">
                            {{-- <a class="dropdown-item" href="{{ url('jadwal-dokter') }}">Jadwal Dokter</a> --}}
                            {{-- <a class="dropdown-item btn-riwayat" href="#">Riwayat Pasien</a> --}}
                            {{-- <div class="dropdown-divider"></div> --}}
                            @if(count($informasi) > 0)
                                @foreach($informasi as $info)
                                <a class="dropdown-item" href="{{ url('informasi/'.$info->slug) }}">{{ $info->judul }}</a>
                                @endforeach
                                {{-- <div class="dropdown-divider"></div> --}}
                            @endif
                            {{-- <a class="dropdown-item" href="{{ url('info-pasien/testimoni') }}">Testimoni</a> --}}
                            {{-- <a class="dropdown-item" href="{{ url('info-pasien/tanya-jawab') }}">Tanya Jawab</a> --}}
                        </div>
                    </li>
                    @endif
                </ul>
                <ul class="navbar-nav justify-content-between ml-auto mr-0">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/info-pasien/pengaduan') }}">Pengaduan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/karir/info') }}">Karir</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link login d-inline-block" href="{{url('login')}}"><img src="{{ asset('/img/icons/icon_login.png') }}" height="24" alt=""></a>
                    </li> --}}
                </ul>
            @endif
        </div>
    </div>
</nav>