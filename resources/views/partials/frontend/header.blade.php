@section('scripts')
    <script type="text/javascript">
        $(document).on('click', '.bahasa.indonesia', function(event){
            key = 'indonesia';
            $.get('{{ url('translate-indonesia') }}', {key:key}, function(data, textStatus, xhr) {
                if(!data.status){
                    swal('Gagal!', data.message, 'warning');
                }else{
                    $('#table-container').html(data.html);
                    location.reload();
                }
            });
        });

        $(document).on('click', '.bahasa.inggris', function(event){
            key = 'english';
            $.get('{{ url('translate-english') }}', {key:key}, function(data, textStatus, xhr) {
                if(!data.status){
                    swal('Gagal!', data.message, 'warning');
                }else{
                    $('#table-container').html(data.html);
                    location.reload();
                }
            });
        });
    </script>
@append

<div class="fixed-top">
<div class="container h-100 d-none d-md-block">
    <div class="row d-none d-md-flex align-items-center py-1 justify-content-between h-100">
        <div class="col-6 d-flex h-100 align-self-between">
            <!-- <img src="../img/icon-long.png" class="main-logo" alt="logo heading"> -->
            <a href="{{ url('/') }}"><img src="{{ asset('/img/icon-long.png') }}" class="main-logo" alt="logo heading"></a>

            <a href="{{ url('/sukaman') }}" class="d-flex justify-content-around" target="_blank">
              <img src="{{ asset('img/sukaman-logo.png') }}" alt="logo sukaman" class="main-logo py-2">
            </a>
        </div>
        {{-- <div class="col-2 h-100 align-self-between">
          
        </div> --}}
        <div class="col-6 h-100 header-contacts d-flex justify-content-end align-items-end">
            {{-- <a class="nav-link login mr-4" href="{{url('login')}}"><img src="{{ asset('/img/icons/icon_login.png') }}" height="32" alt=""></a> --}}
            <div class="row w-100">
                <div class="col-12">
                    <div class="language d-flex justify-content-end align-items-center skiptranslate ignore">
                        <a class="nav-link d-inline-block bahasa indonesia" href="#">
                            <img src="{{ asset('/img/icons/lang_id.png') }}" height="10" alt=""> ID
                        </a>
                        |
                        <a class="nav-link d-inline-block bahasa inggris" href="#">
                            <img  src="{{ asset('/img/icons/lang_en.png') }}" height="10" alt=""> EN
                        </a>
                    </div>
                    <div class="text-right d-flex hotline justify-content-end">
                        {{-- <span class="d-block">Jl. Letjen S. Parman, Kav 87 Slipi, Jakarta Barat 11420, Indonesia</span> --}}
                        {{-- <a class="nav-link login d-inline-block mr-1" href="{{url('login')}}"><img src="{{ asset('/img/icons/icon_login.png') }}" height="32" alt=""></a> --}}

                        {{-- <span class="d-inline-block"><img src="{{ asset('/img/icons/icon_phone.png') }}" class="mr-2" height="32" alt="">1500 034</span>
                        <span class="d-inline-block ml-3"><img src="{{ asset('/img/icons/icon_emergency.png') }}" class="mr-2" height="32" alt="">(021) 568 2424</span> --}}

                        <div class="d-flex align-items-start contact">
                            <img src="{{ asset('/img/icons/icon_phone.png') }}" class="mr-2 d-none d-xl-inline-block" height="40" alt="">
                            <span class="d-inline-block">
                                <small class="d-inline-block">Contact Center:</small><br>{{$settings['call_center']['isi']}}
                            </span>
                        </div>
                        <div class="d-flex align-items-start contact mx-3">
                            <img src="{{ asset('/img/icons/icon_emergency.png') }}" class="mr-2 d-none d-xl-inline-block" height="40" alt="">
                            <span class="d-inline-block">
                                <small>Emergency Call:</small><br>{{$settings['emergency_call']['isi']}}
                            </span>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-3 text-right px-0 d-none d-lg-block">
                    <img src="{{ asset('/img/germas.png') }}" class=" d-inline-block" height="72" alt="">
                </div> --}}
            </div>
        </div>
    </div>
</div>

@include('partials.frontend.menu')
</div>