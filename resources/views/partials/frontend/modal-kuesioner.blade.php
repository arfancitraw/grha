<div class="modal fade" id="kuesionerModal" tabindex="-1" role="dialog" aria-labelledby="kuesionerModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">

    <form class="form" id="kuesionerForm">
      {!! csrf_field() !!}
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">{{ $kuesionerDetail->periode }}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          {{-- <p>Bantu kami memperbaiki sistem pelayanan PJNHK dengan mengisi kuesioner yang telah kami sediakan.</p> --}}
          <div class="kuesioner">
            <div class="kuesioner-steps">
              <div class="kuesioner-progress">
                <div class="kuesioner-progress-line" data-now-value="16.66" data-number-of-steps="2" style="width: 33.3%;"></div>
              </div>
              <div class="kuesioner-step active">
                <div class="kuesioner-step-icon"><i class="fa fa-user"></i></div>
              </div>
              <div class="kuesioner-step">
                <div class="kuesioner-step-icon"><i class="fa fa-star"></i></div>
              </div>
            </div>

            <fieldset>
              <div class="form-group">
                <input type="text" name="nama" class="form-control" placeholder="Nama">
              </div>
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email">
              </div>
              <div class="kuesioner-buttons d-flex justify-content-end">
                <button type="button" class="btn btn-next">Selanjutnya</button>
              </div>
            </fieldset>
            <fieldset>
              @if(!is_null($kuesionerDetail))
              @foreach($kuesionerDetail->detail as $row)
                <input type="hidden" name="kuesioner_id" value="{{ $row->kuesioner_id }}">
                @if($row->tipe)
                <div class="form-row my-2">
                  <label>{{ $row->pertanyaan }}</label>
                  <textarea name="kepuasan-layanan[{{$row->id}}]" class="form-control" style="resize: none;"></textarea>
                </div>
                @else
                <div class="form-row my-2">
                  <div class="col-8">
                    <label for="">{{ $row->pertanyaan }}</label>
                  </div>
                  <div class="col-4 rating">
                    <input type="radio" id="star{{$row->id}}5" name="kepuasan-layanan[{{$row->id}}]" value="5" /><label for="star{{$row->id}}5" title="Rocks!">5 stars</label>
                    <input type="radio" id="star{{$row->id}}4" name="kepuasan-layanan[{{$row->id}}]" value="4" /><label for="star{{$row->id}}4" title="Pretty good">4 stars</label>
                    <input type="radio" id="star{{$row->id}}3" name="kepuasan-layanan[{{$row->id}}]" value="3" /><label for="star{{$row->id}}3" title="Meh">3 stars</label>
                    <input type="radio" id="star{{$row->id}}2" name="kepuasan-layanan[{{$row->id}}]" value="2" /><label for="star{{$row->id}}2" title="Kinda bad">2 stars</label>
                    <input type="radio" id="star{{$row->id}}1" name="kepuasan-layanan[{{$row->id}}]" value="1" /><label for="star{{$row->id}}1" title="Sucks big time">1 star</label>
                  </div>
                </div>
                @endif
              @endforeach
              @endif
              <div class="kuesioner-buttons d-flex justify-content-between">
                <button type="button" class="btn btn-previous">Kembali</button>
                <button type="button" class="btn btn-submit"><i class="fa fa-paper-plane"></i> Kirim feedback</button>
              </div>
            </fieldset>

            {{-- <fieldset>
              <div class="form-group">
                <textarea type="text" name="feedback" class="form-control" rows="4" placeholder="Opsional: Feedback Anda mengenai fasilitas dan pelayanan kami"></textarea>
              </div>
              <div class="kuesioner-buttons d-flex justify-content-between">
                <button type="button" class="btn btn-previous">Kembali</button>
                <button type="button" class="btn btn-submit"><i class="fa fa-paper-plane"></i> Kirim feedback</button>
              </div>
            </fieldset> --}}

          </div>

        </div>
        {{-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div> --}}
      </div>
    </form>
  </div>
</div>