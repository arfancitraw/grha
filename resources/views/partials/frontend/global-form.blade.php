<div class="drawer-container">
    <div class="drawer emergency">
        <span class="title">@lang('front.agreement')</span>
        <div class="d-flex align-items-center drawer-content">
            <form action="{{ url('pasien') }}" class="form d-block w-100" method="POST">
                {!! csrf_field() !!}
                <div class="form-group">
                    <input type="text" name="nomr" class="form-control" placeholder="Nomor MR">
                </div>
                <div class="form-group">
                    <input type="text" name="tgllahir" class="form-control datepicker" placeholder="Tgl Lahir (dd/mm/yyyy)">
                </div>
                <button class="btn btn-appointment btn-block"><i class="fa fa-send"></i>@lang('front.makeagreement')</button>
            </form>
        </div>
    </div>
    <div class="drawer find-doctor">
        <span class="title">@lang('front.doctorschedule')</span>
        <div class="d-flex align-items-center drawer-content">
            <form action="{{ url('jadwal-dokter') }}" method="POST" class="form d-block w-100">
                {!! csrf_field() !!}
                <div class="form-group">
                  <label for="poli" class="sr-only">Poli</label>
                  <select name="poli" id="" class="form-control select2-drawer">
                    <option value="">Semua Poli</option>
                    @foreach($pilihanPoli as $kode => $nama)
                    <option value="{{ $kode }}">{{$nama}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="hari" class="sr-only">Hari</label>
                  <select name="hari" id="" class="form-control select2-drawer">
                    <option value="">Semua hari</option>
                    <option value="1">Senin</option>
                    <option value="2">Selasa</option>
                    <option value="3">Rabu</option>
                    <option value="4">Kamis</option>
                    <option value="5">Jumat</option>
                    <option value="6">Sabtu</option>
                    <option value="7">Minggu</option>
                  </select>
                </div>
                <button class="btn btn-appointment btn-block"><i class="fa fa-search"></i>@lang('front.cekjadwal')</button>
            </form>
        </div>
    </div>

    <div class="drawer questionnaire">
        <span class="title">@lang('front.quesioner')</span>
        <div class="d-block align-items-center drawer-content">
            <span class="d-block text-justify mb-2" style="line-height: 1.5">Bantu kami memperbaiki sistem pelayanan PJNHK dengan mengisi kuesioner yang telah kami sediakan.</span>
            <button class="btn btn-appointment btn-block" data-toggle="modal" data-target="#kuesionerModal"><i class="fa fa-paper-plane"></i>@lang('front.isikuesioner')</button>
        </div>
    </div>
</div>

{{-- <div class="testimoni-container">
    <a href="{{ url('info-pasien/testimoni') }}" class="btn btn-lg" data-toggle="tooltip" data-placement="top" title="Testimoni">
        <i class="fa fa-comments"></i>
    </a>
</div> --}}

{{-- <a href="{{ url('info-pasien/testimoni') }}" class="testimoni-bottom">
    Testimoni   
</a> --}}