<a href="{{ url('info-pasien/testimoni') }}" class="testimoni-bottom">
  Testimoni   
</a>

<div class="useful-links">
  <div class="container py-4" style="position: relative;">
    <div class="row align-items-top">
      <div class="col-12 d-none d-md-block col-md-3 justify-content-center text-center">
        <a href="{{$settings['link-germas']['isi']}}">
          <img src="{{ asset('/img/germas.png') }}" class="img-thumbnail mx-auto mb-3 d-block" width="144" alt="">
        </a>
        <span>PJNHK Mobile</span>

        <a href="{{$settings['link-googleplay']['isi']}}"><img src="{{ asset('/img/mobile-icon.png') }}" class="img mx-auto d-block" width="168" alt="Mobile version"></a>
        {{-- <span>pjnhk.go.id</span> --}}
      </div>
      <div class="col-12 d-none d-md-block col-md-3">
        <ul class="list-unstyled mb-0">
          @foreach($linkFooter as $lin)
          <li class="mb-2"><a href="{{ url($lin->link) }}">{{ $lin->nama }}</a></li>
          @endforeach
        </ul>
      </div>
      <div class="col-12 d-none d-md-block col-md-3 justify-content-center">
        {{-- <span class="footer-contact telp">
          021-5684090
        </span>
        <span class="footer-contact email">
          cd@pjnhk.go.id
        </span>
        <span class="footer-contact address">
          Jl. Letjen S. Parman Kav 87 Slipi<br>Jakarta Barat 11420
        </span> --}}

        <dl class="footer-contact mb-1">
          <h5 class="text-white">@lang('front.contactUs')</h5>
          <dt>Pusat Jantung Nasional Harapan Kita</dt>
          <dd>{!! $settings['address']['isi'] !!}</dd>
          <dt>Telepon</dt>  
          <dd>
            <span class="d-block my-1">Contact Center: {{$settings['call_center']['isi']}}</span>
            {{-- <span class="d-block my-1">SPGDT: 021-5684090</span> --}}
            <span class="d-block my-1">Emergency Contact: {{$settings['emergency_call']['isi']}} (Hunting)</span>
          </dd>
          <dt>Email</dt>
          <dd>{{$settings['email']['isi']}}</dd>
        </dl>
      </div>
      <div class="col-12 col-md-3 text-center">
        <h5 class="text-white">@lang('front.sertificate')</h5>
        <div class="d-flex my-3" style="justify-content: center;">
          @foreach($sertifikatFooter as $sin)
          <a href="{{ $sin->link or '#' }}" class="sertifikat text-center" target="_blank">
            <img src="{{ url('/storage/'.$sin->gambar) }}" alt="{{ $sin->nama }}" width="64" height="64" class="image-cover mr-4">
            <div class="sertifikat-title mr-4">{{ $sin->nama }}</div>
          </a>
          @endforeach
        </div>

        <!-- Histats.com  (div with counter) --><div id="histats_counter" class="d-flex justify-content-center"></div>
        <!-- Histats.com  START  (aync)-->
        <script type="text/javascript">var _Hasync= _Hasync|| [];
        _Hasync.push(['Histats.start', '1,4078325,4,1034,150,25,00011111']);
        _Hasync.push(['Histats.fasi', '1']);
        _Hasync.push(['Histats.track_hits', '']);
        (function() {
          var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
          hs.src = ('//s10.histats.com/js15_as.js');
          (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
        })();</script>
        <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4078325&101" alt="site hit counter" border="0"></a></noscript>
        <!-- Histats.com  END  -->

        <!--<div id="histats_counter" class="d-flex justify-content-center"></div>
        <script type="text/javascript">var _Hasync= _Hasync|| [];
        _Hasync.push(['Histats.start', '1,4176684,4,1034,150,25,00011111']);
        _Hasync.push(['Histats.fasi', '1']);
        _Hasync.push(['Histats.track_hits', '']);
        (function() {
        var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
        hs.src = ('//s10.histats.com/js15_as.js');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
        })();</script>
        <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4176684&101" alt="javascript hit counter" border="0"></a></noscript>
         -->
      </div>
    </div>
  </div>
</div>

<div class="real-footer">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-6 copyright-text">
        <span class="d-block d-md-none d-lg-none d-xl-none">&copy; PJNHK 2018</span>
        <span class="text-muted d-none d-md-block">&copy; Copyright PJNHK 2018. All Rights Reserved.</span>
      </div>
      <div class="col-6 text-right">
        <div class="footer-contacts d-flex justify-content-end align-items-center">
          @foreach($mediaSosial as $ms)
          <a href="{{ $ms->link }}" class="footer-items icon {{$ms->icon}}"></a>
          @endforeach

        </div>
      </div>
    </div>
  </div>
</div>
