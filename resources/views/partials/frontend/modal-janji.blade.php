<div class="modal fade" id="janjiModal" tabindex="-1" role="dialog" aria-labelledby="janjiModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <form class="form" id="janjiForm" action="{{ url('pasien') }}" method="POST">
            {!! csrf_field() !!}
            <input type="hidden" name="kd_dr" value="">
            <input type="hidden" name="kd_dep" value="">
            <input type="hidden" name="hari" value="">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalJanjiTitle">Buat Perjanjian</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nomor MR</label>
                        <input type="text" name="nomr" class="form-control" placeholder="Nomor MR" required>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="text" name="tgllahir" class="form-control datepicker" placeholder="Tgl Lahir (mm/dd/yyyy)" required>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" id="modalJanjiSubmit" class="btn btn-primary">Buat Perjanjian</button>
                </div>
            </div>
        </form>
    </div>
</div>