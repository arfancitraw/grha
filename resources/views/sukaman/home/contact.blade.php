<div class="container wow fadeInDown" data-wow-delay=".5s">
    <div class="row">
        <div class="col-md-4 col-sm-4">
            <h3 class="headline">Contact Info</h3>
            <div class="space"></div>
            <table>
                <tr>
                    <td style="vertical-align: top"><i class="fa fa-map-marker fa-fw fa-2x mr-2"></i></td>
                    <td style="vertical-align: top">{{$setting['address']->judul}}</td>
                </tr>
                <tr>
                    <td style="vertical-align: top"><i class="fa fa-envelope fa-fw fa-2x mr-2"></i></td>
                    <td style="vertical-align: top">{{$setting['email']->judul}}</td>
                </tr>
                <tr>
                    <td style="vertical-align: top"><i class="fa fa-phone fa-fw fa-2x mr-2"></i></td>
                    <td style="vertical-align: top">{{$setting['emergency_call']->judul}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-8 col-sm-8 marb20">
            <div class="contact-info">
                <h3 class="headline">Memiliki pertanyaan seputar Paviliun Sukaman?</h3>

                <div class="space"></div>
                <form id="formKontak" action="{{ url('sukaman/kontak') }}" method="POST">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" required="" name="nama" class="form-control br-radius-zero" id="name" placeholder="Nama Anda" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
                                <div class="validation"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" class="form-control br-radius-zero" required="" name="email" id="email" placeholder="Email Anda" data-rule="email" data-msg="Please enter a valid email">
                                <div class="validation"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control br-radius-zero" required="" name="perihal" id="subject" placeholder="Perihal pesan anda" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control br-radius-zero" required="" name="pesan" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Tuliskan pesan anda disini"></textarea>
                        <div class="validation"></div>
                    </div>

                    <div class="form-action">
                <a class="btn btn-info btn-lg kirim kontak" href="#">Kirim Pesan</a>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>