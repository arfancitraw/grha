@extends('layouts.frontend-sukaman')

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick-theme.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.css') }} ">
  {{-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css"> --}}
@append

@section('js')
  <script src="{{ asset('plugins/slick/slick.min.js') }}"></script>
  <script src="{{ asset('plugins/jQuery/jquery.form.min.js') }}"></script>
  <script src="{{ asset('plugins/daterangepicker/moment.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script> --}}
@append

@section('styles')
  <style type="text/css">
  .datepicker{
    padding:.375rem .75rem !important;
  }
  .list-dokter {
    color: #000;
    font-weight: 300;
    padding-left: 15px;
    font-size: 1.2em;
  }
  .not-found{
    color: #000;
    font-weight: 300;
    padding-left: 15px;
    font-size: 1.2em;
    font-style: normal;
  }
  h1, h2, h3, h4, h5, h6 {
    color: #3b9ca5;
    font-weight: 200;
  }
  .nav-pills .nav-link {
    margin-left: .25rem;
    margin-right: .25rem;
    background: rgba(0,0,0,.1);
    padding: .75rem 1.5rem;
  }
  .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    background: rgb(59, 156, 165);
    box-shadow: inset 0 0 6px rgba(0,0,0,0.35);
    transition: .15s all ease-in-out;
  }
  .mfp-bottom-bar {
    top: auto;
    bottom: 40px;
    left: 0;
    padding: 1rem;
    background: rgba(0,0,0,0.35);
  }
  .mfp-bottom-bar p {margin-bottom: 0}
  .modal-cover {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: #fff;
    z-index: 100;
  }
  </style>
@append

@section('scripts')
<script type="text/javascript">
  $(document).on('click', '.kirim.kontak', function(event){
    nama = $("input[name=nama]").val();
    email = $("input[name=email]").val();
    perihal = $("input[name=perihal]").val();
    pesan = $("input[name=pesan]").val();
    swal({
      title: 'Apaka Anda Yakin ?',
      text: "akan mengirimkan pesan ! "+nama,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya Tentu',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result) {
        $("#formKontak").ajaxSubmit({
          success: function(resp){
            if(!resp.status){
              swal(
                'Gagal!',
                resp.message,
                'warning'
                );
            }else{
              swal(
                'Success',
                resp.message,
                'success'
                );
              location.reload();
            }
          },
          error: function(resp){
            var error = $('<ul class="list"></ul>');
            console.log(resp.responseJSON);
            $.each(resp.responseJSON, function(index, val) {
              error.append('<li>'+val+'</li>');
            });

            $('#formModal').find('.ui.error.message').html(error).show();
          }
        }); 
      }
    });
    
  });
</script>

<script type="text/javascript">
  $(document).ready(function($) {
      $('#home-slider #slider').slick({
        dots: false,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
        speed: 500,
        fade: true,
        pauseOnHover: false,
        pauseOnFocus: false,
        arrows: false,
        cssEase: 'linear'
      });

      $('#dokterTab .nav-item:first-child .nav-link').tab('show')

      startTime();

      $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
      })
  });

  function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    $('.clock').html(h + ":" + m + ":" + s);
    var t = setTimeout(startTime, 500);
  }
  function checkTime(i) {
    if (i < 10) {i = "0" + i};  
    return i;
  }

  $('a[data-target="#infoModal"]').on('click', function(event) {
    event.preventDefault();

    var id = $(this).attr('data-id');
    $.get("{{ url()->current() . '/info' }}/" + id, function(data){
      setTimeout(function(){
        $('#infoModal .modal-cover').fadeOut(200, function(){
          $(this).css('visibility', 'hidden')
        });
      }, 200)
      $('#infoModal .modal-title').html(data.judul);
      if (data.gambar) {
        $('#infoModal .modal-gambar').html(`<img src="{{ asset('storage') }}/`+data.gambar+`" alt="gambar" width="100%">`);
      }else{
        $('#infoModal .modal-gambar').html(``);
      }
      $('#infoModal .modal-body').html(data.konten);
    })
  });

  $('a[data-target="#serviceModal"]').on('click', function(event) {
    event.preventDefault();

    var id = $(this).attr('data-id');
    $.get("{{ url('sukaman/service') }}/" + id, function(data){
      setTimeout(function(){
        $('#serviceModal .modal-cover').fadeOut(200, function(){
          $(this).css('visibility', 'hidden')
        });
      }, 200)
      $('#serviceModal .modal-title').html(data.judul);
      if (data.gambar) {
        $('#serviceModal .modal-gambar').html(`<img src="{{ asset('storage') }}/`+data.gambar+`" alt="gambar" width="100%">`);
      }else{
        $('#serviceModal .modal-gambar').html(``);
      }
      $('#serviceModal .modal-body').html(data.konten);
    })
  });

  $(document).on('hide.bs.modal', '#infoModal, #serviceModal', function(e){
    $('.modal-cover').fadeIn(20, function(){
      $(this).css('visibility', 'visible')
    });
  })

</script>
@append

@section('content')
<section class="mb-3" id="home"> 
  @include('sukaman.home.slider')
</section>

<section class="section" id="about">
  @include('sukaman.home.about')
</section>

<section class="special-bar" style="background: #3b9ca522">
  @include('sukaman.home.emergency')
</section>

<section class="section wow fadeInUp" id="service">
  @include('sukaman.home.service')
</section>

<section class="section wow fadeInUp" id="doctor" style="background: rgba(0,0,0,.1)">
  @include('sukaman.home.dokter')
</section>

<section class="section wow" id="facility">
  @include('sukaman.home.facilities')
</section>

<section class="section filler" id="info" style="background: rgba(0,0,0,.1)">
  @include('sukaman.home.informasi')
</section>

<section class="section wow slideInRight" id="pricing" style="background: rgba(0,0,0,.05)">
  @include('sukaman.home.pricing')
</section>

<section class="section" id="contact">
  @include('sukaman.home.contact')
</section>
@endsection

@section('modals')
  @include('sukaman.home.modals.service')
  @include('sukaman.home.modals.info')

  <div class="modal fade" id="dokterModal" tabindex="-1" role="dialog" aria-labelledby="dokterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
      <div class="modal-cover"></div>
      <div class="modal-content"> 
      </div>
    </div>
  </div>
  
  @include('partials.frontend.modal-janji')
@endsection
