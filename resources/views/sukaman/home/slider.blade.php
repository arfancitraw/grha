<div id="home-slider">
  <div id="slider">
    @if(count($slider) > 0)
      @foreach($slider as $slide)
        <a href="{{ $slide->url or '#' }}" 
           class="slider-wrap" 
           style="background-image:url('{{ url('storage/'.$slide->gambar) }}'); background-size: cover; height:481px; background-position: center center;">
        </a>
      @endforeach
    @else
      <div class="slider-wrap" style="background-image:url('{{ asset('sk/img/backgrounds/bg-login.jpg') }}')">
        {{-- <div class="slider-content d-none d-xl-block container">
          <h1>Medical Service <b>that you can trust.</b></h1>
          <span>nobis dicta quis blanditiis tempora eum dolor, voluptatem fugit dignissimos accusantium!</span>
        </div> --}}
      </div>
    @endif
  </div>
</div>
{{-- <div class="home-slider">
  <div style="background-image: url('{{ asset('sk/img/backgrounds/bg-login.jpg') }}')"></div>
</div> --}}
<div class="container content">
  <h1 class="headline wow fadeInDown" data-wow-delay="1.2s">{{$setting['title']->judul}}</h1>
  <span class="h5 d-block wow fadeInDown" data-wow-delay="1.5s">{{$setting['title']->isi}}</span>
</div>

<div class="container walkmethru">
  <div class="wow fadeInDown" data-wow-delay="2.5s">
    <a href="#" onclick="scrollToSegment('#about')" class="text-white"><i class="fa fa-lg fa-chevron-down"></i></a>
  </div>
</div>