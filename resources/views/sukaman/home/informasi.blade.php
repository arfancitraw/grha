<div class="wow fadeInDown" data-wow-delay=".2s">
  <div class="container wow fadeInDown" data-wow-delay=".5s">
    <div class="row">
      <div class="col-12">
        <h2 class="headline">{{$setting['information']->judul}}</h2>
        <span class="h5">{{$setting['information']->isi}}</span>
      </div>
    </div>

    <div class="mt-4">
      <div class="row useful-info justify-content-start">
        @foreach ($info as $i => $in)
        <div class="col-12 col-md-6 col-lg-4 my-3">
          <div class="card h-100">
            <div class="card-body">
              <h5 class="card-title">{{ $in->judul }}</h5>
              <p class="card-text">{!! str_limit(strip_tags($in->konten), 80, $end = '... <span class="d-block w-100 mt-3 text-right font-italic"><a class="text-white" href="javascript:void(0)" data-toggle="modal" data-target="#infoModal" data-id="' . $in->id . '">read more <i class="fa fa-chevron-right ml-1"></i></a></span>') !!}</p>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>