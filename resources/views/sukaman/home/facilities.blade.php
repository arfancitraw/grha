<div class="container wow fadeInRight">
  <div class="row">
    <div class="col-12">
      <h2 class="headline">{{$setting['facility']->judul}}</h2>
      <span class="h5">{{$setting['facility']->isi}}</span>
    </div>
  </div>

  <div class="masonry mt-5">
    @foreach ($fasilitas as $fas)
      <a class="fasilitas-item masonry-brick" href="{{asset('storage').'/'.$fas->gambar}}" title="{{ $fas->deskripsi }}">
        <img src="{{asset('storage').'/'.$fas->gambar}}" alt="item">
      </a>
    @endforeach
  </div>
</div>