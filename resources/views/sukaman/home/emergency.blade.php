<div class="container wow slideInUp" data-wow-delay="0.2s">
  <div class="row d-flex align-items-center">
    <div class="col-12 col-md-9">
      <h3>{{$setting['emergency']->judul}}</h3>
      <p>{{$setting['emergency']->isi}}</p>
    </div>
    <div class="col-12 col-md-3">
    	<button type="button" class="btn btn-primary btn-block btn-lg btn-janji" data-poli="{{ $poli }}">Buat Perjanjian</button>
      {{-- <a href="#" class="btn btn-block btn-lg btn-primary">Buat Perjanjian</a> --}}
    </div>
  </div>
</div>