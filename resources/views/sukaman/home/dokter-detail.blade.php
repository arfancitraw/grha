<div class="col-12 content-main">
	<div class="row mb-3">
		<div class="col-12 col-md-3">
			<img class="card-img-top cover" style="object-position: top center; object-fit: cover;" src="@if(!is_null($record->photo)) {{ asset('storage/' . $record->photo ) }} @else {{ asset('img/noImage.png' ) }} @endif" alt="Foto" height="300">
		</div>
		<div class="col-12 col-md-9">
			<div class="dokter-title mb-2">
				<h2>{{ $record->nama_lengkap }}</h2>
				{{-- <span>{{ $record->spesialisasi->nama }}</span> --}}<span>{{ ($record->jabatandokter) ? $record->jabatandokter->jabatan : '' }}</span>
			</div>
			<ul class="nav nav-pills nav-fill my-3 nav-details" id="detail" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="keahlian-tab" data-toggle="tab" href="#keahlian" role="tab" aria-controls="keahlian" aria-selected="true">Keahlian</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="pendidikan-tab" data-toggle="tab" href="#pendidikan" role="tab" aria-controls="pendidikan" aria-selected="false">Kualifikasi</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="jadwal-tab" data-toggle="tab" href="#jadwal" role="tab" aria-controls="jadwal" aria-selected="false">Jadwal Praktek</a>
				</li>
			</ul>
			<div class="tab-content" id="detailContent">
				<div class="tab-pane fade show active" id="keahlian" role="tabpanel" aria-labelledby="keahlian-tab">
					@php
					if(count($record->keahlian) > 0){
						$spesialisasi = $record->keahlian->where('tipe', 0);
						$prosedur = $record->keahlian->where('tipe', 1);
					}
					@endphp
					<h4>Spesialisasi</h4>
					@if(isset($spesialisasi) && count($spesialisasi) > 0)
					@foreach($spesialisasi as $row)
					<h5 class="list-dokter">
						<i class="fa fa-angle-right"></i>
						{{ $row->nama }}
					</h5>
					@endforeach
					@else
					<h5 class="not-found"><i>Data Spesialisasi belum dilengkapi</i></h5>
					@endif

					<hr>

					<h4>Penugasan Klinis</h4>
					@if(isset($prosedur) && count($prosedur) > 0)
					@foreach($prosedur as $row)
					<h5 class="list-dokter">
						<i class="fa fa-angle-right"></i>
						{{ $row->nama }}
					</h5>
					@endforeach
					@else
					<h5 class="not-found"><i>Data Penugasan Klinis belum dilengkapi</i></h5>
					@endif

				</div>
				<div class="tab-pane fade" id="pendidikan" role="tabpanel" aria-labelledby="pendidikan-tab">
					@php
					if(count($record->pendidikan) > 0){
						$gelar = $record->pendidikan->where('tipe', 0);
						$pelatihan = $record->pendidikan->where('tipe', 2);
						$keanggotaan = $record->pendidikan->where('tipe', 1);
						$internasional = $record->pendidikan->where('tipe', 3);
					}
					@endphp
					<h4>Pendidikan</h4>
					@if(isset($gelar) && count($gelar) > 0)
					@foreach($gelar as $row)
					<h5 class="list-dokter">
						<i class="fa fa-angle-right"></i>
						{{ $row->nama }}
					</h5>
					@endforeach
					@else
					<h5 class="not-found"><i>Data Pendidikan belum dilengkapi</i></h5>
					@endif
					<hr>

					<h4>Pelatihan dan Fellowship</h4>
					@if(isset($pelatihan) && count($pelatihan) > 0)
					@foreach($pelatihan as $row)
					<h5 class="list-dokter">
						<i class="fa fa-angle-right"></i>
						{{ $row->nama }}
					</h5>
					@endforeach
					@else
					<h5 class="not-found"><i>Data Pelatihan dan Fellowship belum dilengkapi</i></h5>
					@endif

					<hr>

					<h4>Kualifikasi Internasional</h4>
					@if(isset($internasional) && count($internasional) > 0)
					@foreach($internasional as $row)
					<h5 class="list-dokter">
						<i class="fa fa-angle-right"></i>
						{{ $row->nama }}
					</h5>
					@endforeach
					@else
					<h5 class="not-found"><i>Data Kualifikasi Internasional belum dilengkapi</i></h5>
					@endif	


					<hr>

					<h4>Keanggotaan Profesi</h4>
					@if(isset($keanggotaan) && count($keanggotaan) > 0)
					@foreach($keanggotaan as $row)
					<h5 class="list-dokter">
						<i class="fa fa-angle-right"></i>
						{{ $row->nama }}
					</h5>
					@endforeach
					@else
					<h5 class="not-found"><i>Data Keanggotaan Profesi belum dilengkapi</i></h5>
					@endif



				</div>
				<div class="tab-pane fade" id="jadwal" role="tabpanel" aria-labelledby="jadwal-tab">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									<th class="text-center">Hari</th>
									<th class="text-center">Waktu</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>SENIN</td>
									<td class="text-center">
										@if($senin = $poli->jadwal->where('hari_praktek', 0)) @foreach($senin as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="1" data-poli="{{ $poli->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
								</tr>
								<tr>
									<td>SELASA</td>
									<td class="text-center">
										@if($selasa = $poli->jadwal->where('hari_praktek', 1)) @foreach($selasa as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="2" data-poli="{{ $poli->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
								</tr>
								<tr>
									<td>RABU</td>
									<td class="text-center">
										@if($rabu = $poli->jadwal->where('hari_praktek', 2)) @foreach($rabu as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="3" data-poli="{{ $poli->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
								</tr>
								<tr>
									<td>KAMIS</td>
									<td class="text-center">
										@if($rabu = $poli->jadwal->where('hari_praktek', 2)) @foreach($rabu as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="3" data-poli="{{ $poli->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
								</tr>
								<tr>
									<td>KAMIS</td>
									<td class="text-center">
										@if($kamis = $poli->jadwal->where('hari_praktek', 3)) @foreach($kamis as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}"data-hari="4"data-poli="{{ $poli->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
								</tr>
								<tr>
									<td>JUMAT</td>
									<td class="text-center">
										@if($jumat = $poli->jadwal->where('hari_praktek', 4)) @foreach($jumat as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="5" data-poli="{{ $poli->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
								</tr>
								<tr>
									<td>SABTU</td>
									<td class="text-center">
										@if($sabtu = $poli->jadwal->where('hari_praktek', 5)) @foreach($sabtu as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="6" data-poli="{{ $poli->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
								</tr>
								<tr>
									<td>MINGGU</td>
									<td class="text-center">
										@if($minggu = $poli->jadwal->where('hari_praktek', 6)) @foreach($minggu as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="7" data-poli="{{ $poli->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				{{-- <div class="tab-pane fade" id="jadwal" role="tabpanel" aria-labelledby="jadwal-tab">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									<th class="text-center">Senin</th>
									<th class="text-center">Selasa</th>
									<th class="text-center">Rabu</th>
									<th class="text-center">Kamis</th>
									<th class="text-center">Jumat</th>
									<th class="text-center">Sabtu</th>
									<th class="text-center">Minggu</th>
								</tr>
							</thead>
							<tbody>
								@php $counter = 0; @endphp
								@foreach($poli as $p)
								<tr>
									<th>{{ $p->nama }}</th>
									@if($p->jadwal->count() > 0)
									@php $counter++; @endphp 
									<td class="text-center">
										@if($senin = $p->jadwal->where('hari_praktek', 0)) @foreach($senin as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="1" data-poli="{{ $p->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
									<td class="text-center">
										@if($selasa = $p->jadwal->where('hari_praktek', 1)) @foreach($selasa as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="2" data-poli="{{ $p->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
									<td class="text-center">
										@if($rabu = $p->jadwal->where('hari_praktek', 2)) @foreach($rabu as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="3" data-poli="{{ $p->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
									<td class="text-center">
										@if($kamis = $p->jadwal->where('hari_praktek', 3)) @foreach($kamis as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}"data-hari="4"data-poli="{{ $p->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
									<td class="text-center">
										@if($jumat = $p->jadwal->where('hari_praktek', 4)) @foreach($jumat as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="5" data-poli="{{ $p->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
									<td class="text-center">
										@if($sabtu = $p->jadwal->where('hari_praktek', 5)) @foreach($sabtu as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="6" data-poli="{{ $p->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
									<td class="text-center">
										@if($minggu = $p->jadwal->where('hari_praktek', 6)) @foreach($minggu as $j)
										<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
										<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="7" data-poli="{{ $p->kode }}">BUAT JANJI
										</button>
										@endforeach @endif
									</td>
									@else
									<td colspan="7" class="text-center"><i>Tidak ada jadwal pada departemen terkait</i></td>
									@endif
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div> --}}

			</div>
		</div>
	</div>