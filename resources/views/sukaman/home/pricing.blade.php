@section('scripts')
<script>
  $(document).ready(function() {
    var options = {
      slidesPerView: 3,
      spaceBetween: 20,
      observer: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      breakpoints: {
        480: {
          slidesPerView: 1
        },
        800: {
          slidesPerView: 2
        },
        1120: {
          slidesPerView: 3
        }
      }
    }
    var pricingSwiper = new Swiper ('.pricing-container', options) 

    $('#pricingTab a[data-toggle="tab"]').on('hide.bs.tab', function (e) {
      pricingSwiper.destroy
    });
    $('#pricingTab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      pricingSwiper = new Swiper ('.pricing-container', options) 
    });
  });


  </script>
@append
@section('styles')
<style type="text/css">
.pricing-container {
  position: relative;
  width: 100%;
  height: 100%;
  overflow: hidden
}
#pricingTab .nav-link {
  margin-left: .25rem;
  margin-right: .25rem;
  background: rgba(0,0,0,.1);
  padding: .75rem 1.5rem;
}
#pricingTab .nav-link.active, #pricingTab .show>.nav-link {
  background: rgb(59, 156, 165);
  box-shadow: inset 0 0 6px rgba(0,0,0,0.35);
  transition: .15s all ease-in-out;
}
.pricing-container .swiper-wrapper.centered {
  justify-content: center;
}
</style>
@append

<section id="pricing" class="section bg-gray">
  <div class="container">
    <div class="row mb-4">
      <div class="col-12">
        <h2 class="headline">Informasi Tarif Paviliun Sukaman</h2>
        {{-- <span class="h5">{{$setting6->isi}}</span> --}}
      </div>
    </div>
  </div>
  <div class="container mb-3">
    <ul class="nav nav-pills justify-content-center mb-4" id="pricingTab" role="tablist">
      @foreach ($pricing as $idx => $kategori)
      <li class="nav-item">
        <a class="h6 nav-link @if($idx == 0) active @endif" id="{{$kategori->id}}-tab" data-toggle="tab" href="#{{$kategori->id}}-content" role="tab" aria-controls="{{$kategori->id}}" aria-selected="true">{{$kategori->nama}}</a>
      </li>
      @endforeach
    </ul>
    <div class="tab-content">
      @foreach($pricing as $idx => $kategori)
      <div class="tab-pane fade show @if($idx == 0) active @endif" id="{{$kategori->id}}-content" role="tabpanel" aria-labelledby="{{$kategori->id}}-tab">
        <div class="pricing-container wow fadeInUp" data-wow-delay="0.1s">
          <div class="swiper-wrapper mx-auto {{ sizeOf($kategori->tarif) <= 3 ? 'centered' : '' }}">
            @foreach($kategori->tarif as $tarif)
              <div class="pricing-content swiper-slide general {{ $tarif->warna == 'default' ? '' : 'featured' }} h-100">
                <h2>{{ $tarif->nama }}</h2>
                <span class="d-block px-4 mt-3">
                  <b>Detail Tarif:</b>
                </span>
                <ul class="list-unstyled mt-0">
                  @foreach($tarif->detail as $detail)
                  <li>
                    {{ $detail->nama }}
                    @if($detail->check)
                    <i class="fa fa-check icon-success"></i>
                    @else
                    <i class="fa fa-times icon-danger"></i>
                    @endif
                  </li>
                  @endforeach
                </ul>

                <div class="text-center px-4">
                  <p>{{ $tarif->deskripsi }}</p>
                </div>

                <div class="price-bottom">
                  <a href="#" class="btn btn-info btn-lg">Rp. {{ number_format($tarif->harga, 0, ',', '.') }},-</a>
                </div>
              </div>
            {{-- </div> --}}
            @endforeach
          </div>

          <div class="swiper-button-next {{ sizeOf($kategori->tarif) <= 3 ? 'd-none' : '' }}"></div>
          <div class="swiper-button-prev {{ sizeOf($kategori->tarif) <= 3 ? 'd-none' : '' }}"></div>
        </div>

      </div>
      @endforeach
    </div>
  </div>
</section>