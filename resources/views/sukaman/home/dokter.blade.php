@section('css')
<link href="{{ asset('sk/plugins/swiper/css/swiper.min.css') }}" rel="stylesheet">
@append
@section('js')
<script src="{{ asset('sk/plugins/swiper/js/swiper.min.js') }}"></script>
@append
@section('scripts')
<script>
  $(document).ready(function() {
    var mySwiper = new Swiper ('.dokter-container', {
      slidesPerView: 5,
      slidesPerColumn: 2,
      slidesPerGroup: 5,
      spaceBetween: 5,
      observer: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      breakpoints: {
        480: {
          slidesPerView: 1
        },
        800: {
          slidesPerView: 2
        },
        1120: {
          slidesPerView: 3
        }
      }
    }) 
  });

  $('#dokterTab .nav-link[data-toggle="tab"]').on('hide.bs.tab', function (e) {
    $('.dokter-container').hide()
  });
  $('#dokterTab .nav-link[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $('.dokter-container').show()
  });

  $('.dokter-tabs .dokter').on('click', function (e) {
    $id = $(this).attr('data-id');
    $('#dokterModal').modal('show')
    // TODO: removing the delay on show
    $('#dokterModal .modal-content').load("{{ url('sukaman/dokter') }}/" + $id, function(response, status, xhr){
      if(status=="success"){
        setTimeout(function(){
          $('#dokterModal .modal-cover').fadeOut(200, function(){
            $(this).css('visibility', 'hidden')
          });
        }, 200)
      }else{
        var msg = "Terdapat kesalahan: ";
        console.log( msg + xhr.status + " " + xhr.statusText );
      }
    });
  });
  $('#dokterModal').on('hide.bs.modal', function(e){
    $('#dokterModal .modal-cover').fadeIn(20, function(){
      $(this).css('visibility', 'visible')
    });
  })
</script>
@append
@section('styles')
<style type="text/css">
.swiper-button-next {
  right: 1rem;
}
.swiper-button-prev {
  left: 1rem;
}

.dokter-container {
  position: relative;
  width: 100%;
  height: 100%;
  overflow: hidden
}
#dokterTab .nav-link {
  margin-left: .25rem;
  margin-right: .25rem;
  background: rgba(0,0,0,.1);
  padding: .75rem 1.5rem;
}
#dokterTab .nav-link.active, #dokterTab .show>.nav-link {
  background: rgb(59, 156, 165);
  box-shadow: inset 0 0 6px rgba(0,0,0,0.35);
  transition: .15s all ease-in-out;
}
.dokter-tabs, .dokter-container {
  min-height: 22rem;
}
.dokter {
  display: flex;
  justify-content: start;
  align-items: center;
}
.dokter .card {
  cursor: pointer;
  margin: .5rem 1rem;
  width: 100%;
}
.dokter .card img {
  height: 11rem;
  object-fit: cover;
}
.dokter .card .card-title {
  color: #3b9ca5;
  font-size: .8em;
  font-weight: 500;
  min-height: 2rem;
  line-height: 1.25rem;
  display: block;
}
.dokter .card .card-text {
  font-size: .8em;
}
/*.dokter .card .card-photo{
  width: 40%;
}
.dokter .card .card-body {
  width: 60%;
  line-height: 1.25rem;
}*/
.dokter .card .card-body {
  /*width: 60%;*/
  display: block;
  line-height: 1.25rem;
}
.dokter .card .cover{
  display: block;
  max-height: 19rem;
  object-fit: cover;
  object-position: top;
}
#dokterModal .modal-cover {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: #fff;
  z-index: 100;
}
</style>
@append

<div class="container">
  <div class="row">
    <div class="col-12">
      <h2 class="headline">{{$setting['doctors']->judul}}</h2>
      <span class="h5">{{$setting['doctors']->isi}}</span>
    </div>
  </div>

  <div class="container wow slideInUp" data-wow-delay=".5s">
    <div class="row">
      <div class="col-lg-12">
        <ul class="nav nav-pills my-4 d-flex justify-content-center" id="dokterTab" role="tablist">
          @foreach ($dokter as $row)
          <li class="nav-item">
            <a class="h6 nav-link" id="{{ str_replace( [':', '\\', '/', '*', ' ', '(', ')'], '-', strtolower($row->nama)) }}-tab" data-toggle="tab" href="#{{ str_replace( [':', '\\', '/', '*', ' ', '(', ')'], '-', strtolower($row->nama)) }}" role="tab" aria-controls="{{ str_replace( [':', '\\', '/', '*', ' ', '(', ')'], '-', strtolower($row->nama)) }}" aria-selected="true">{{ $row->nama }}</a>
          </li>
          @endforeach
        </ul>
        <div class="tab-content dokter-tabs">
          @foreach ($dokter as $dok)
            <div class="tab-pane fade in" id="{{ str_replace( [':', '\\', '/', '*', ' ', '(', ')'], '-', strtolower($dok->nama)) }}" role="tabpanel" aria-labelledby="{{ str_replace( [':', '\\', '/', '*', ' ', '(', ')'], '-', strtolower($dok->nama)) }}-tab">
              <div class="dokter-container">
                <div class="swiper-wrapper justify-content-start align-item-center mx-auto">
                  @foreach ($dok->dokter as $z)
                    <div class="dokter swiper-slide" data-groups='["{{str_replace( [':', '\\', '/', '*', ' ', '(', ')'], '-', strtolower($dok->nama))}}"]' data-title="{{$z->nama_lengkap}}" data-id="{{ $z->id }}">
                      <div class="card">
                        @if ($z->photo)
                        <img class="card-img-top cover" src="{{asset('storage').'/'.$z->photo }}" alt="Card image cap">
                        @else
                        <img class="card-img-top cover" src="{{ asset ('/img/foto_placeholder2.png') }}" alt="Card image cap">
                        @endif
                        <div class="card-body text-center">
                          <span class="card-title">{{$z->nama_lengkap}}</span>
                          <p class="card-text text-muted text-center">{{$dok->nama}}</p>
                        </div>
                      </div>
                    </div>
                  @endforeach
                </div>

                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>

              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>