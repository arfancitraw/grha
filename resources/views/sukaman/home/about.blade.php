<div class="container wow fadeInUp" data-wow-delay="2.5s">
	<div class="row wow fadeInUp" data-wow-delay="0.2s">
		<div class="col-12">
			<h2 class="headline">{{$setting['about']->judul}}</h2>
			{!! $setting['about']->isi !!}
		</div>
	</div>
</div>