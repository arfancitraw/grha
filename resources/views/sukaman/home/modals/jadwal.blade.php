<table class="table table-striped">
	<thead>
		<tr>
			<th class="text-center">Hari</th>
			<th class="text-center">Waktu</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th class="text-center">SENIN</th>
			<td class="text-center">
				@if($senin = $poli->jadwal->where('hari_praktek', 0)) @foreach($senin as $j)
				<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
				<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="1" data-poli="{{ $poli->kode }}">BUAT JANJI
				</button>
				@endforeach @endif
			</td>
		</tr>
		<tr>
			<th class="text-center">SELASA</th>
			<td class="text-center">
				@if($selasa = $poli->jadwal->where('hari_praktek', 1)) @foreach($selasa as $j)
				<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
				<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="2" data-poli="{{ $poli->kode }}">BUAT JANJI
				</button>
				@endforeach @endif
			</td>
		</tr>
		<tr>
			<th class="text-center">RABU</th>
			<td class="text-center">
				@if($rabu = $poli->jadwal->where('hari_praktek', 2)) @foreach($rabu as $j)
				<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
				<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="3" data-poli="{{ $poli->kode }}">BUAT JANJI
				</button>
				@endforeach @endif
			</td>
		</tr>
		<tr>
			<th class="text-center">KAMIS</th>
			<td class="text-center">
				@if($rabu = $poli->jadwal->where('hari_praktek', 3)) @foreach($rabu as $j)
				<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
				<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="4" data-poli="{{ $poli->kode }}">BUAT JANJI
				</button>
				@endforeach @endif
			</td>
		</tr>
		<tr>
			<th class="text-center">KAMIS</th>
			<td class="text-center">
				@if($kamis = $poli->jadwal->where('hari_praktek', 4)) @foreach($kamis as $j)
				<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
				<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}"data-hari="5" data-poli="{{ $poli->kode }}">BUAT JANJI
				</button>
				@endforeach @endif
			</td>
		</tr>
		<tr>
			<th class="text-center">JUMAT</th>
			<td class="text-center">
				@if($jumat = $poli->jadwal->where('hari_praktek', 5)) @foreach($jumat as $j)
				<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
				<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="6" data-poli="{{ $poli->kode }}">BUAT JANJI
				</button>
				@endforeach @endif
			</td>
		</tr>
		<tr>
			<th class="text-center">SABTU</th>
			<td class="text-center">
				@if($sabtu = $poli->jadwal->where('hari_praktek', 6)) @foreach($sabtu as $j)
				<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
				<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="7" data-poli="{{ $poli->kode }}">BUAT JANJI
				</button>
				@endforeach @endif
			</td>
		</tr>
		<tr>
			<th class="text-center">MINGGU</th>
			<td class="text-center">
				@if($minggu = $poli->jadwal->where('hari_praktek', 7)) @foreach($minggu as $j)
				<span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
				<button type="button" class="btn btn-info btn-xs btn-janji" data-dokter="{{ $record->kode }}" data-hari="8" data-poli="{{ $poli->kode }}">BUAT JANJI
				</button>
				@endforeach @endif
			</td>
		</tr>
	</tbody>
</table>