<div class="modal-header">
  <h5 class="modal-title" id="dokterModalLabel"></h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span class="h6" aria-hidden="true">Tutup info dokter &times;</span>
  </button>
</div>
<div class="modal-body">
  <div class="row mb-3">
    <div class="col-12 col-md-4 photo-holder">
      @if(isset($record))
      <img class="card-img-top cover" style="object-position: top center; object-fit: cover; border-radius: 8px;"  src="@if(!is_null($record->photo)) {{ asset('storage/' . $record->photo ) }} @else {{ asset('img/noImage.png' ) }} @endif" alt="Foto" width="100%">
      @endif
    </div>
    <div class="col-12 col-md-8">
      <div class="dokter-title mb-2 name-holder">
        <h2>{{ $record->nama_lengkap }}</h2>
        <span>{{ ($record->jabatandokter) ? $record->jabatandokter->jabatan : '' }}</span>
      </div>
      <ul class="nav nav-pills nav-fill my-3 nav-details" id="detail" role="tablist">
          <li class="nav-item">
              <a class="h6 nav-link active" id="keahlian-tab" data-toggle="tab" href="#keahlian" role="tab" aria-controls="keahlian" aria-selected="true">Keahlian</a>
          </li>
          <li class="nav-item">
              <a class="h6 nav-link" id="pendidikan-tab" data-toggle="tab" href="#pendidikan" role="tab" aria-controls="pendidikan" aria-selected="false">Kualifikasi</a>
          </li>
          <li class="nav-item">
              <a class="h6 nav-link" id="jadwal-tab" data-toggle="tab" href="#jadwal" role="tab" aria-controls="jadwal" aria-selected="false">Jadwal Praktek</a>
          </li>
      </ul>
      <div class="tab-content" id="detailContent">
          <div class="tab-pane fade show active" id="keahlian" role="tabpanel" aria-labelledby="keahlian-tab">
              @include('frontend.tim-dokter.detail.keahlian')
          </div>
          <div class="tab-pane fade" id="pendidikan" role="tabpanel" aria-labelledby="pendidikan-tab">
              @include('frontend.tim-dokter.detail.pendidikan')
          </div>
          <div class="tab-pane fade" id="jadwal" role="tabpanel" aria-labelledby="jadwal-tab">
              @include('sukaman.home.modals.jadwal')
          </div>
      </div>
    </div>
  </div>
</div>