<div class="container">
  <div class="row">
    <div class="col-12 col-lg-4 wow fadeInUp">
      <div class="my-2">
        <h2 class="headline">{{$setting['service']->judul}}</h2>
        <span class="h5">{{$setting['service']->isi}}</span>
      </div>
    </div>
    <div class="col-12 col-lg-8">
      <div class="row">

        @foreach ($pelayanan as $element)
        <div class="col-12 col-md-6">
          <div class="card no-border wow fadeInUp" data-wow-delay="0.5s">
            <div class="card-body p-0 my-2">
              <div class="row">
                <div class="col-3">
                  <img src="{{asset('storage').'/'.$element->icon}}" alt="icon" width="100%">
                </div>
                <div class="col-9">
                  <h5>{{$element->judul}}</h5>
                  @if(strlen($element->konten) > 90)
                  <p>{!! str_limit($element->konten, 90) !!}</p>
                  <span class="d-block w-100 text-right font-italic">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#serviceModal" data-id="{{ $element->id }}">
                      read more <i class="fa fa-chevron-right ml-1"></i>
                    </a>
                  </span>
                  @else
                  <p>{!! $element->konten !!}</p>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>        
        @endforeach
      </div>
    </div>
  </div>
</div>