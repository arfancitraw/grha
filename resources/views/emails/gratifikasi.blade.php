@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<img src="{{ url('img/icon-long.png') }}" height="70px" width="auto">
@endcomponent
@endslot

{{-- Body --}}
## Terdapat pengaduan gratifikasi baru dengan detail sebagai berikut: 

<div class="table">
	<table>
		<thead>
			<tr>
				<th colspan="2">Detail Pengaduan</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Nama</td>
				<td>{{ $parameter->nama }}</td>
			</tr>
			<tr>
				<td>NIP/Nopeg</td>
				<td>{{ $parameter->nip_nopeg }}</td>
			</tr>
			<tr>
				<td>Jabatan/Unit Kerja</td>
				<td>{{ $parameter->jabatan_unit_kerja }}</td>
			</tr>
			<tr>
				<td>Jenis/Bentuk Penerimaan</td>
				<td>{{ $parameter->jenis_bentuk_penerimaan }}</td>
			</tr>
			<tr>
				<td>Harga/Nilai Nominal</td>
				<td>{{ $parameter->harga_nilai_nominal }}</td>
			</tr>
			<tr>
				<td>Kaitan Peristiwa Penerimaan</td>
				<td>{{ $parameter->kaitan_peristiwa_penerimaan }}</td>
			</tr>
			<tr>
				<td>Tempat & Tanggal Penerimaan</td>
				<td>{{ $parameter->tempat_penerimaan }}, {{ $parameter->tanggal_penerimaan->format('d/m/Y') }}</td>
			</tr>
			<tr>
				<td>Nama Pemberi</td>
				<td>{{ $parameter->nama_pemberi }}</td>
			</tr>
			<tr>
				<td>Pekerjaan dan Jabatan</td>
				<td>{{ $parameter->pekerjaan_jabatan }}</td>
			</tr>
			<tr>
				<td>Alamat/Telepon/Email</td>
				<td>{{ $parameter->alamat_telepon_email }}</td>
			</tr>
			<tr>
				<td>Hubungan Dengan Penerima</td>
				<td>{{ $parameter->hubungan_penerima }}</td>
			</tr>
			<tr>
				<td>Alasan Pemberian</td>
				<td>{{ $parameter->alasan_pemberian }}</td>
			</tr>
			<tr>
				<td>Kronologi</td>
				<td>{{ $parameter->kronologi }}</td>
			</tr>
			<tr>
				<td>Catatan</td>
				<td>{{ $parameter->catatan }}</td>
			</tr>
		</tbody>
	</table>
</div>

@component('mail::button', ['url' =>  url('storage/'.$parameter->bukti->first()->file), 'color' => 'green'])
Download Bukti Pendukung
@endcomponent

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
@endcomponent
@endslot
@endcomponent