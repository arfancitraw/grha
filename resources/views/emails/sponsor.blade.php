@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<img src="{{ url('img/icon-long.png') }}" height="70px" width="auto">
@endcomponent
@endslot

{{-- Body --}}
## Terdapat pengaduan sponsor baru dengan detail sebagai berikut: 

<div class="table">
	<table>
		<thead>
			<tr>
				<th colspan="2">Detail Pengaduan</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Nama</td>
				<td>{{ $parameter->nama }}</td>
			</tr>
			<tr>
				<td>NIP / Nopeg</td>
				<td>{{ $parameter->nip_nopeg }}</td>
			</tr>
			<tr>
				<td>Jabatan</td>
				<td>{{ $parameter->jabatan }}</td>
			</tr>
			<tr>
				<td>Nama Kegiatan</td>
				<td>{{ $parameter->nama_kegiatan }}</td>
			</tr>
			<tr>
				<td>Tempat/Tanggal Kegiatan</td>
				<td>{{ $parameter->tempat_kegiatan }}, {{ $parameter->tanggal->format('d/m/Y') }}</td>
			</tr>
			<tr>
				<td>Peranan</td>
				<td>{{ $parameter->peranan }}</td>
			</tr>
			<tr>
				<td>Nama Perusahaan</td>
				<td>{{ $parameter->nama_perusahaan }}</td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>{{ $parameter->alamat }}</td>
			</tr>
			<tr>
				<td>Pemberi Sponsor</td>
				<td>{{ $parameter->pemberi_sponsor }}</td>
			</tr>
			<tr>
				<td>Registrasi</td>
				<td>Rp. {{ number_format($parameter->registrasi, 0, ',', '.') }},-</td>
			</tr>
			<tr>
				<td>Akomodasi</td>
				<td>Rp. {{ number_format($parameter->akomodasi, 0, ',', '.') }},-</td>
			</tr>
			<tr>
				<td>Transportasi</td>
				<td>Rp. {{ number_format($parameter->transportasi, 0, ',', '.') }},-</td>
			</tr>
			<tr>
				<td>Honor</td>
				<td>Rp. {{ number_format($parameter->honor, 0, ',', '.') }},-</td>
			</tr>
			<tr>
				<td>Total</td>
				<td>Rp. {{ number_format($parameter->total, 0, ',', '.') }},-</td>
			</tr>
		</tbody>
	</table>
</div>

@component('mail::button', ['url' =>  url('storage/'.$parameter->bukti->first()->file), 'color' => 'green'])
Download Bukti Pendukung
@endcomponent

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
@endcomponent
@endslot
@endcomponent