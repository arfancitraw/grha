@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<img src="{{ url('img/icon-long.png') }}" height="70px" width="auto">
@endcomponent
@endslot

{{-- Body --}}
## Terdapat pengaduan {{ $parameter->tipe_label }} baru dengan detail sebagai berikut: 

<div class="table">
	<table>
		<thead>
			<tr>
				<th colspan="2">Detail Pengaduan</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><b>Nama Lengkap</b></td>
				<td>{{ $parameter->nama }}</td>
			</tr>
			<tr>
				@if($parameter->tipe == 2)
				<td><b>Status</b></td>
				<td>{{ $parameter->status ? 'Pelanggan RSJPDHK' : 'Masyarakat' }}</td>
				@endif
			</tr>
			<tr>
				<td><b>Telepon</b></td>
				<td>{{ $parameter->telepon }}</td>
			</tr>
			<tr>
				<td><b>Email</b></td>
				<td>{{ $parameter->email }}</td>
			</tr>
			<tr>
				<td><b>Perihal</b></td>
				<td>{{ $parameter->perihal }}</td>
			</tr>
			<tr>
				<td><b>Uraian</b></td>
				<td>{{ $parameter->uraian }} </td>
			</tr>
			<tr>
				<td><b>Keterangan</b></td>
				<td>{{ $parameter->keterangan }}</td>
			</tr>
		</tbody>
	</table>
</div>

@component('mail::button', ['url' =>  url('storage/'.$parameter->bukti), 'color' => 'green'])
Download Bukti Pendukung
@endcomponent

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
@endcomponent
@endslot
@endcomponent