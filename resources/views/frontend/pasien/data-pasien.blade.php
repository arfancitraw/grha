<div class="card mb-3">
    <h5 class="text-center mb-3 mt-3">Data Pasien</h5>
    <table class="table m-0">
        <tr>
            <th class="text-left" style="width: 40%">Nama Pasien</th>
            <td class="text-left">{{ $pasien->nama }}</td>
        </tr>
        <tr>
            <th class="text-left">Nomor MR</th>
            <td class="text-left">{{ $pasien->nomr }}</td>
        </tr>
        <tr>
            <th class="text-left">Tanggal Lahir</th>
            <td class="text-left">{{ $tanggal_lahir->format('j F Y') }}</td>
        </tr>
        <tr>
            <th class="text-left">Alamat</th>
            <td class="text-left">{{ $pasien->alamat }}</td>
        </tr>
    </table>
</div>