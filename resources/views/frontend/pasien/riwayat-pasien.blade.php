<div class="card mb-3">
    <h5 class="text-center mb-3 mt-3">Riwayat Perjanjian Terakhir</h5>
    <div class="list-group">
        @if(!is_null($pasien) && count($pasien->riwayat) > 0)
            @foreach($pasien->riwayat as $riwayat)
            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">{{ $riwayat->nama_dokter }}</h5>
                    @php 
                        $tanggal = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $riwayat->tanggal_janji);
                    @endphp
                    <small class="text-muted">{{ $tanggal->format('d M Y') }}</small>
                </div>
                <div class="d-flex w-100 justify-content-between">
                    <p class="mb-1">{{ $riwayat->departemen }}</p>
                    <small class="text-muted">
                    @if($riwayat->datang != "N")
                        <span class="badge badge-success">Hadir</span>
                    @elseif($riwayat->datang == "N" && $tanggal->isFuture())
                        <span class="badge badge-warning text-white">Belum Hadir</danger>
                    @else
                        <span class="badge badge-danger">Tidak Hadir</danger>
                    @endif
                    </small>
                </div>
                <div class="d-flex w-100 justify-content-between">
                    <small class="mb-1">INISIAL  : {{ $riwayat->inisial }}</small>
                    <small class="mb-1">NO. URUT : {{ $riwayat->no_urut }}</small>
                </div>
            </a>
            @endforeach
        @else
            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="text-center my-3">
                    <h6><i>Belum ada riwayat</i></h6>
                </div>
            </a>
        @endif
    </div>
</div>