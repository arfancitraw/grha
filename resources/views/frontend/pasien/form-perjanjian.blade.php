
      <form class="form perjanjian" id="dataForm" action="{{ url($pageUrl.'/perjanjian') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        @php
          $born = strtotime($pasien->tgl_lahir)
        @endphp
        <input type="hidden" name="nomr" value="{{ $pasien->nomr }}">
        <input type="hidden" name="tgllahir" value="{{ date('m/d/Y', $born) }}">
        <input type="hidden" name="kd_dr">
        <input type="hidden" name="sts_jam">
        <h4 class="text-center">Proses Perjanjian</h4>
        <div class="perjanjian-steps">
          <div class="perjanjian-progress">
            <div class="perjanjian-progress-line" data-now-value="33.33" data-number-of-steps="2" style="width: 33.33%;"></div>
          </div>
          <div class="perjanjian-step active">
            <div class="perjanjian-step-icon"><i class="fa fa-calendar"></i></div>
          </div>
          <div class="perjanjian-step">
            <div class="perjanjian-step-icon"><i class="fa fa-user"></i></div>
          </div>
        </div>

        <div class="step step-one">
          <div id="catatanPerjanjian" class="alert alert-primary" role="alert" style="display: none">
            @php
              $dkt = \App\Models\Dokter\ListDokter::where('kode', $dokter)->first();
            @endphp
            Anda akan membuat perjanjian dengan dokter <b>{{ $dkt->nama or '' }}</b> pada hari <b>{{$hari}}</b>, silahkan memilih tanggal yang sesuai dengan hari tersebut
          </div>
          <div class="form-row align-items-center">
            <div class="col-auto mb-3" style="display: none">
              <label class="mr-sm-2 mb-0" for="p-dokter">Dokter</label>
              <select name="kd_dokter" id="p-dokter" class="form-control select2">
                  <option value="">Semua Dokter</option>
                  @foreach($pilihanDokter as $kode => $nama)
                  <option value="{{ $kode }}" @if(isset($dokter) && $dokter == $kode) selected @endif>{{$nama}}</option>
                  @endforeach
              </select>
            </div>
            <div class="col-auto mb-3">
              <label class="mr-sm-2 mb-0" for="poli">Poli</label>
              <select name="kd_dep" id="poli" class="form-control select2">
                  {{-- <option value="">Semua Poli</option> --}}
                  @foreach($pilihanPoli as $kode => $nama)
                  <option value="{{ $kode }}" @if(isset($poli) && $poli == $kode) selected @endif>{{$nama}}</option>
                  @endforeach
              </select>
            </div>
            <div class="col-auto mb-3">
              <label class="mr-sm-2 mb-0" for="tanggal">Tanggal</label>
              <input type="text" class="form-control tgl_janji" name="tgl_janji" id="tanggal">
            </div>
            <div class="col-auto mb-3">
              <label class="mr-sm-2 mb-0" for="">&nbsp;</label>
              <button type="button" class="btn btn-primary btn-search" style="display: block;">Cek Kuota</button>
            </div>
          </div>
          <div class="table-perjanjian">
            @include('frontend.pasien.table')
          </div>
        </div>
        <div class="step step-two col-12" style="display:none">
            <h4 class="text-center my-4">Konfirmasi Data</h4>
            <div class="form-group row mb-2 align-items-start">
                <label class="col-4 justify-content-start">Nama Lengkap Pasien</label>
                <div class="col-8">
                    <input class="form-control" type="text" value="{{ $pasien->nama }}" readonly>
                </div>
            </div>
            <div class="form-group row mb-2 align-items-start">
                <label class="col-4 justify-content-start">Nomor MR</label>
                <div class="col-8">
                    <input class="form-control" type="text" value="{{ $pasien->nomr }}" readonly>
                </div>
            </div>
            <div class="form-group row mb-2 align-items-start">
                <label class="col-4 justify-content-start">Tanggal Lahir</label>
                <div class="col-8">
                    <input class="form-control" type="text" value="{{ date('d-m-Y', $born) }}" readonly>
                </div>
            </div>
            <div class="form-group row mb-2 align-items-start">
                <label class="col-4 justify-content-start">Tanggal Perjanjian</label>
                <div class="col-8">
                    <input class="form-control tanggal_janji" type="text" readonly>
                </div>
            </div>
            <div class="form-group row mb-2 align-items-start">
                <label class="col-4 justify-content-start">Poliklinik</label>
                <div class="col-8">
                    <input class="form-control poliklinik" type="text" readonly>
                </div>
            </div>
            <div class="form-group row mb-2 align-items-start">
                <label class="col-4 justify-content-start">Dokter Tujuan</label>
                <div class="col-8">
                    <input class="form-control dokter" type="text" readonly>
                </div>
            </div>
            <div class="form-group row mb-2 align-items-start">
                <label class="col-4 justify-content-start">Waktu Pelayanan</label>
                <div class="col-8">
                    <input class="form-control waktu" type="text" readonly>
                </div>
            </div>
            <hr>
            <div class="form-group row mb-2 align-items-start">
                <label class="col-4 justify-content-start">Cara Bayar</label>
                <div class="col-8">
                    <input type="hidden" name="cara_bayar" value="P" disabled>
                    <select name="cara_bayar" class="form-control">
                        <option value="B">BPJS</option>
                        <option value="P">NON BPJS</option>
                        {{-- <option value="P">Pribadi</option>
                        <option value="A">Asuransi Lainnya</option> --}}
                    </select>
                </div>
            </div>
            <div class="form-group row mb-2 align-items-start">
                <label class="col-4 justify-content-start">Nomor Kontak</label>
                <div class="col-8">
                    <input class="form-control" type="text" name="hp" placeholder="Masukkan Nomor HP anda untuk konfirmasi perjanjian ini" required>
                </div>
            </div>

            <div class="pengaduan-buttons d-flex justify-content-between mt-4 px-0">
                <button type="button" class="btn btn-default btn-back">Kembali</button>
                <button type="button" class="btn btn-primary btn-save">Buat Perjanjian</button>
            </div>
        </div>

      </form>