<h5 class="text-center mb-3">Info Jadwal Dokter</h5>
<form id="jadwalForm" class="form row justify-content-center mb-4" action="{{ url($pageUrl) }}" method="POST">
  <div class="form-group col-sm-3 px-0 mb-0">
    <label for="dokter" class="sr-only">Dokter</label>
    <input type="text" name="dokter" class="form-control" placeholder="Nama Dokter">
  </div>
  <div class="form-group col-sm-3 pr-0 mb-0">
    <label for="hari" class="sr-only">Hari</label>
    <select name="hari" id="" class="form-control">
      <option value="">Semua hari</option>
      <option value="0">Senin</option>
      <option value="1">Selasa</option>
      <option value="2">Rabu</option>
      <option value="3">Kamis</option>
      <option value="4">Jumat</option>
      <option value="5">Sabtu</option>
      <option value="6">Minggu</option>
    </select>
  </div>
  <div class="form-group col-sm-3 mb-0">
    <label for="hari" class="sr-only">Spesialisasi</label>
    <select name="hari" id="" class="form-control">
      <option value="">Semua spesialisasi</option>
      <option value="0">Kardiologi</option>
      <option value="1">Anestesi</option>
    </select>
  </div>
  <div class="form-group col-12 col-sm-2 p-0 mr-3 mb-0">
    <button class="btn btn-outline-info" type="submit"><i class="fa fa-filter"></i></button>
    <button class="btn btn-outline-secondary btn-reset" type="button"><i class="fa fa-sync"></i></button>
  </div>
</form>

<table class="table table-sm table-striped table-bordered">
  <thead>
    <tr>
      <th>Dokter</th>
      <th>Spesialisasi</th>
      <th>Hari</th>
      <th>Jam</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td rowspan="3">Dokter A</td>
      <td rowspan="3">Kardiologi</td>
      <td>Senin</td>
      <td>12:00 - 14:00</td>
    </tr>
    <tr>
      <td>Selasa</td>
      <td>15:00 - 18:00</td>
    </tr>
    <tr>
      <td>Kamis</td>
      <td>14:00 - 18:00</td>
    </tr>
  </tbody>
</table>