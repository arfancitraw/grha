@extends('layouts.frontend-scaffold')

@php
  $born = null;
  if(!is_null($pasien)){
      $born = \Carbon\Carbon::createFromFormat('Y-m-d', $pasien->tgl_lahir);
  }
@endphp

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/select2/select2.min.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/timepicker/jquery.timepicker.min.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/timepicker/lib/bootstrap-datepicker.css') }} ">
@endsection

@section('js')
  <script src="{{ asset('plugins/daterangepicker/moment.js') }}"></script>
  <script src="{{ asset('js/moment-id.js') }}"></script>
  <script src="{{ asset('plugins/datepicker/datepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
  <script src="{{ asset('plugins/timepicker/jquery.timepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/jQuery/jquery.redirect.js') }}"></script>
@endsection

@section('styles')
  <style>
    input.form-control[readonly]{
      background: #f0f0f0;
    }
    table th {
      text-align: center
    }
    .perjanjian {
      padding: 1rem;
      margin: 0 auto;
    }
    .perjanjian h3 { 
      margin-top: 0;
      margin-bottom: 5px;
      text-transform: uppercase;
    }

    .perjanjian-steps { 
      overflow: hidden;
      position: relative;
      margin: 0 0 1rem;
      display: flex;
      justify-content: center
    }

    .perjanjian-progress { 
      position: absolute;
      top: 24px;
      left: 0;
      width: 100%;
      height: 1px;
      background: #ddd;
    }
    .perjanjian-progress-line { 
      position: absolute;
      top: 0;
      left: 0;
      height: 1px;
      background: #17646b;
    }

    .perjanjian-step { 
      position: relative;
      width: 33.333333%;
      padding: 0 5px;
      text-align: center;
    }

    .perjanjian-step-icon {
      display: inline-block;
      width: 40px;
      height: 40px;
      margin-top: 4px;
      background: #ddd;
      font-size: 16px;
      color: #fff;
      line-height: 40px;
      -moz-border-radius: 50%;
      -webkit-border-radius: 50%;
      border-radius: 50%;
    }
    .perjanjian-step.activated .perjanjian-step-icon {
      background: #fff;
      border: 1px solid #17646b;
      color: #17646b;
      line-height: 38px;
    }
    .perjanjian-step.active .perjanjian-step-icon {
      width: 48px;
      height: 48px;
      margin-top: 0;
      background: #17646b;
      font-size: 22px;
      line-height: 48px;
    }

    .perjanjian-buttons .btn {
      background: #17646b;
      color: #fff;
    }
    .perjanjian-step p { 
      color: #ccc;
    }
    .perjanjian-step.activated p { 
      color: #17646b;
    }
    .perjanjian-step.active p { 
      color: #17646b;
    }

    .perjanjian fieldset { 
      display: none;
      text-align: left;
    }

    .perjanjian .input-error { 
      border-color: #f35b3f;
    }
  </style>
  <style>
    .table tr, .table td {
      vertical-align: middle!important;
      text-align: center
    }
    .upsized {
      font-size: 1.4rem;
      font-weight: 500;
      line-height: 1.2;
    }
    .nav-pills .nav-link.active {
      background: transparent;
      color: #3b9ca5;
      border-bottom: 2px solid #3b9ca5;
      border-radius: 0
    }
    .btn-question {
      border-radius: 0;
      text-align: left;
      font-size: 1.2rem;
      line-height: 1.1;
      font-weight: 500;
      border-color: transparent;
    }
    .btn-question small {
      font-weight: 300;
    }
    .form-appointment {
      border: 1px solid #3b9ca5;
      border-radius: 4px;
      padding-top: 1rem;
    }
    .form-group.row{
      padding-top: .5rem;
      padding-bottom: .5rem
    }
    .form-group.row:nth-child(even) {
      background: #3b9ca522
    }
    .badge-lg {
      font-size: 2rem
    }
    .badge-block {
      width: 100%;
      font-size: .8rem;
      padding: .5rem .75rem;
    }
    .list-group-item{
      border-right: none;
      border-left: none;
    }
    .list-group-item:first-child {
        border-top-left-radius: 0 !important;
        border-top-right-radius: 0 !important;
    }
    .select2-container{
      display: block !important;
    }
    .select2-container .select2-selection--single{
      height:33px !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow{
      height:30px !important;
    }
  </style>
@endsection

@section('scripts')
<script>
  function kirimPertanyaan() {
    swal('Terkirim!','Pertanyaan Anda akan dijawab oleh dokter kami.','success').then($('#tanyajawabModal').modal('hide'))
  }

  $(document).ready(function($) {
    $('#cover').hide('slow');
    $('.select2').select2()

    @if(!is_null($hari))
        $('#catatanPerjanjian').slideDown('slow');
    @endif  

    $('.tgl_janji').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        startDate: '+1d',
        @if(!is_null($disable_days))
        daysOfWeekDisabled: "{{ $disable_days }}"
        @endif
    })

    $('.btn-reset').click(function(e){
        $('[name=dokter]').val('');
        $('[name=poli]').val('');
        $('[name=hari]').val('');
        $('#jadwalForm').submit();
    })
    $('.time').timepicker({ 
        timeFormat: 'H:i:s',
        useSelect: true,
        step: 15,
        minTime: '7:00',
        maxTime: '17:00',
        className: 'form-control'
    });

    $('.btn-search').click(function(event) {
      if($('#tanggal').val() == ''){
        swal(
          'Gagal!',
          'Mohon pilih tanggal terlebih dahulu.',
          'error'
        ).then((result) => {
          return true;
        })
      }else{
        $('#cover').show('fast');
        $.post('{{ url('pasien/jadwal') }}', {
            _token: '{{ csrf_token() }}', 
            dokter:$('#p-dokter').val(), 
            poli:$('#poli').val(), 
            tanggal:$('#tanggal').val()
        }, function(resp) {
            $('.table-perjanjian').html(resp);
            $('#cover').hide('fast');
        });
      }
    });
  });
  $(document).on("change", "#p-dokter, #poli",function(){
      $('#catatanPerjanjian').slideUp('slow');
      $('.tgl_janji').datepicker('remove');
      $('.tgl_janji').datepicker({
          format: "dd/mm/yyyy",
          autoclose: true,
          startDate: '+1d'
      });
  });

  $(document).on('click', '.btn-pick', function(e){
      var kd_dr = $(this).data('dr');
      var sts_jam = $(this).data('sts');
      var row = $(this).closest('tr');
      var dokter = row.find('.p_dokter').html();
      var waktu = row.find('.p_jam_dtg').html() + " - " + row.find('.p_jam_plg').html();
      // var tanggal = $('[name=tgl_janji]').val();
      var date = moment($('[name=tgl_janji]').val(), 'DD/MM/YYYY');


      $('[name=kd_dr]').val(kd_dr);
      $('[name=sts_jam]').val(sts_jam);
      $('.poliklinik').val($('[name=kd_dep] option:selected').text());
      $('.tanggal_janji').val(date.lang("id").format('dddd, DD-MM-YYYY'));
      $('.dokter').val(dokter);
      $('.waktu').val(waktu);

      if($('[name=kd_dep]').val() == '20012'){
        $('select[name="cara_bayar"]').val('P')
                                      .prop('disabled', true);
        $('input[name="cara_bayar"]').prop('disabled', false);
      }else{
        $('select[name="cara_bayar"]').val('B')
                                      .prop('disabled', false);
        $('input[name="cara_bayar"]').prop('disabled', true);
      }

      var parent = $(this).closest('.step');
      var next_step = true;
      var current_active_step = $(this).closest('.perjanjian').find('.perjanjian-step.active');
      var progress_line = $(this).closest('.perjanjian').find('.perjanjian-progress-line');

      // console.log(parent);

      if( next_step ) {
        parent.fadeOut(200, function() {
            current_active_step.removeClass('active')
                               .addClass('activated')
                               .next()
                               .addClass('active');

            bar_progress(progress_line, 'right');
            $(this).next().fadeIn();
        });
      }
  });

  $(document).on('click', '.btn-back', function(e){
      var current_active_step = $(this).parents('.perjanjian').find('.perjanjian-step.active');
      var progress_line = $(this).parents('.perjanjian').find('.perjanjian-progress-line');

      $(this).parents('.step').fadeOut(100, function() {
        current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
        bar_progress(progress_line, 'left');
        $(this).prev().fadeIn();
      });
  });

  $(document).on('click', '.btn-save', function(e){
    if($('[name=hp]').val() == ''){
        swal(
          'Gagal!',
          'Mohon isi data dengan lengkap terlebih dahulu.',
          'error'
        ).then((result) => {
          return true;
        })
    }else{
      swal({
        title: 'Apakah Data Anda Sudah Benar?',
        // text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Buat Perjanjian',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result) {
          $('#cover').show('fast');
          $(".form.perjanjian").ajaxSubmit({
            success: function(resp){
              $('#cover').hide('fast');
              // $("#formModal").modal('hide');
              swal(
                'Berhasil!',
                'Perjanjian Berhasil Dilakukan.',
                'success'
              ).then((result) => {
                // window.location = "{{ url('/') }}";
                $.redirect('{{ url($pageUrl) }}', {'_token': '{{ csrf_token() }}', 'nomr': '{{ !is_null($pasien) ? $pasien->nomr : '' }}', 'tgllahir': '{{ !is_null($born) ? $born->format('d/m/Y') : '' }}'});
                return true;
              })
            },
            error: function(resp){
              $('#cover').hide('fast');
              var data = (resp.responseJSON);
              swal(
                'Gagal!',
                data.message,
                'error'
              ).then((result) => {
                return true;
              })
            }
          });
        }
      })

    }
  });
</script>
@endsection

@section('content')
<div class="jumbotron pasien">
  <div class="container d-flex">
    {{-- <div class="col-12 col-lg-6 d-none d-lg-block">
      <h1 class="display-3"><span class="text-danger">Rujukan</span> Nasional</h1>
      <p>Quos perferendis unde incidunt suscipit nostrum, vitae aliquam quasi illum reiciendis, obcaecati iste aut molestias culpa quae minima natus? Doloribus, sint, provident.</p>
    </div> --}}

  </div>
</div>

<div class="mx-5 my-2">
  <div id="cover"></div>
  <div class="row mb-4">
    <div class="col-12 content-main">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item active" aria-current="page">Detail Pasien</li>
        </ol>
      </nav>

      @if(!is_null($pasien))
      <div class="row">
        <div class="col-12 col-lg-3">
          @include('frontend.pasien.data-pasien')
          {{-- <hr>
          @include('frontend.pasien.info-dokter') --}}
          @include('frontend.pasien.riwayat-pasien')
        </div>

        <div class="col-12 col-lg-9">          
          @include('frontend.pasien.form-perjanjian')
        </div>
      </div>
      @else
      <h2 class="text-center"> Data Pasien Tidak Ditemukan </h2>
      <p class="text-center"> Mohon periksa kembali <b>Nomor MR</b> dan <b>Tanggal Lahir</b> yang anda inputkan apakah sudah sesuai? </p>
      @endif

    </div>
  </div>
</div>

@endsection