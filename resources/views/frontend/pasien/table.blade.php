<style type="text/css" media="screen">
  .table thead tr th {
    vertical-align: middle;
  }
</style>

<table class="table table-bordered table-sm table-striped table-hover">
  <thead>
    <tr class="bg-primary text-white">
      <th rowspan="2">Dokter</th>
      <th colspan="2">Jam Praktek</th>
      <th rowspan="2">Quota<br>Tersedia</th>
      <th rowspan="2"></th>
    </tr>
    <tr class="bg-primary text-white">
      <th>Jam Mulai</th>
      <th>Jam Selesai</th>
    </tr>
  </thead>
  <tbody>
    @if(isset($jadwal))
    <input type="hidden" name="hari" value="{{ $hari }}">
    @foreach($jadwal as $row)
    <tr>
      <td class="text-left p_dokter" style="padding:0 15px;">{{ $row->nama_dr }}</td>
      <td class="p_jam_dtg">{{ substr($row->jam_dtg, 0, 5) }}</td>
      <td class="p_jam_plg">{{ substr($row->jam_plg, 0, 5) }}</td>
      <td>{{ $quota = ($row->batas_quota - $row->quota_terisi) }}</td>
      <td>@if($quota > 0)
          <button type="button" class="btn btn-primary btn-sm btn-pick" data-dr="{{ $row->kd_dr }}" data-sts="{{ $row->sts_jam }}">Buat Perjanjian</button>
      @endif</td>
    </tr>
    @endforeach
    @else
    <tr>
      <td colspan="5" class="text-center"><i>Jadwal Tidak Ditemukan</i></td>
    </tr>
    @endif
  </tbody>
</table>