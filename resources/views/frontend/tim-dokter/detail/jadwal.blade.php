<table class="table table-striped">
    <thead>
        <tr>
            <th>
            <th class="text-center">Senin</th>
            <th class="text-center">Selasa</th>
            <th class="text-center">Rabu</th>
            <th class="text-center">Kamis</th>
            <th class="text-center">Jumat</th>
            <th class="text-center">Sabtu</th>
            <th class="text-center">Minggu</th>
        </tr>
    </thead>
    <tbody>
        @php $counter = 0; @endphp
        @foreach($poli as $p)
        <tr>
            <th>{{ $p->nama }}</th>
            @if($p->jadwal->count() > 0)
            @php $counter++; @endphp 
            <td class="text-center">
                @if($senin = $p->jadwal->where('hari_praktek', 0)) @foreach($senin as $j)
                    <span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
                    <button type="button" class="btn btn-info btn-xs btn-janji" 
                            data-dokter="{{ $record->kode }}"
                            data-hari="1"
                            data-poli="{{ $p->kode }}">BUAT JANJI
                    </button>
                @endforeach @endif
            </td>
            <td class="text-center">
                @if($selasa = $p->jadwal->where('hari_praktek', 1)) @foreach($selasa as $j)
                    <span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
                    <button type="button" class="btn btn-info btn-xs btn-janji" 
                            data-dokter="{{ $record->kode }}"
                            data-hari="2"
                            data-poli="{{ $p->kode }}">BUAT JANJI
                    </button>
                @endforeach @endif
            </td>
            <td class="text-center">
                @if($rabu = $p->jadwal->where('hari_praktek', 2)) @foreach($rabu as $j)
                    <span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
                    <button type="button" class="btn btn-info btn-xs btn-janji" 
                            data-dokter="{{ $record->kode }}"
                            data-hari="3"
                            data-poli="{{ $p->kode }}">BUAT JANJI
                    </button>
                @endforeach @endif
            </td>
            <td class="text-center">
                @if($kamis = $p->jadwal->where('hari_praktek', 3)) @foreach($kamis as $j)
                    <span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
                    <button type="button" class="btn btn-info btn-xs btn-janji" 
                            data-dokter="{{ $record->kode }}"
                            data-hari="4"
                            data-poli="{{ $p->kode }}">BUAT JANJI
                    </button>
                @endforeach @endif
            </td>
            <td class="text-center">
                @if($jumat = $p->jadwal->where('hari_praktek', 4)) @foreach($jumat as $j)
                    <span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
                    <button type="button" class="btn btn-info btn-xs btn-janji" 
                            data-dokter="{{ $record->kode }}"
                            data-hari="5"
                            data-poli="{{ $p->kode }}">BUAT JANJI
                    </button>
                @endforeach @endif
            </td>
            <td class="text-center">
                @if($sabtu = $p->jadwal->where('hari_praktek', 5)) @foreach($sabtu as $j)
                    <span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
                    <button type="button" class="btn btn-info btn-xs btn-janji" 
                            data-dokter="{{ $record->kode }}"
                            data-hari="6"
                            data-poli="{{ $p->kode }}">BUAT JANJI
                    </button>
                @endforeach @endif
            </td>
            <td class="text-center">
                @if($minggu = $p->jadwal->where('hari_praktek', 6)) @foreach($minggu as $j)
                    <span class="time badge badge-lg badge-info">{{ substr($j->jam_mulai_praktek, 0, 5) . '-' . substr($j->jam_selesai_praktek, 0, 5) }}</span>
                    <button type="button" class="btn btn-info btn-xs btn-janji" 
                            data-dokter="{{ $record->kode }}"
                            data-hari="7"
                            data-poli="{{ $p->kode }}">BUAT JANJI
                    </button>
                @endforeach @endif
            </td>
            @else
            <td colspan="7" class="text-center"><i>Tidak ada jadwal pada departemen terkait</i></td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>