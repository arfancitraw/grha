@php
	if(count($record->keahlian) > 0){
		$spesialisasi = $record->keahlian->where('tipe', 0);
		$prosedur = $record->keahlian->where('tipe', 1);
	}
@endphp
<h4>Spesialisasi</h4>
@if(isset($spesialisasi) && count($spesialisasi) > 0)
	@foreach($spesialisasi as $row)
	<h5 class="list-dokter">
		<i class="fa fa-angle-right"></i>
		{{ $row->nama }}
	</h5>
	@endforeach
@else
	<h5 class="not-found"><i>Data Spesialisasi belum dilengkapi</i></h5>
@endif
	
<hr>

<h4>Keahlian Khusus</h4>
@if(isset($prosedur) && count($prosedur) > 0)
	@foreach($prosedur as $row)
	<h5 class="list-dokter">
		<i class="fa fa-angle-right"></i>
		{{ $row->nama }}
	</h5>
	@endforeach
@else
	<h5 class="not-found"><i>Data Keahlian Khusus belum dilengkapi</i></h5>
@endif
 