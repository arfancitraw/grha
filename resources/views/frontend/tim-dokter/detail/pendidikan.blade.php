@php
	if(count($record->pendidikan) > 0){
		$gelar = $record->pendidikan->where('tipe', 0);
		$pelatihan = $record->pendidikan->where('tipe', 2);
		$keanggotaan = $record->pendidikan->where('tipe', 1);
		$internasional = $record->pendidikan->where('tipe', 3);
	}
@endphp
<h4>Pendidikan</h4>
@if(isset($gelar) && count($gelar) > 0)
	@foreach($gelar as $row)
	<h5 class="list-dokter">
		<i class="fa fa-angle-right"></i>
		{{ $row->nama }}
	</h5>
	@endforeach
@else
	<h5 class="not-found"><i>Data Pendidikan belum dilengkapi</i></h5>
@endif
<hr>

<h4>Pelatihan dan Fellowship</h4>
@if(isset($pelatihan) && count($pelatihan) > 0)
	@foreach($pelatihan as $row)
	<h5 class="list-dokter">
		<i class="fa fa-angle-right"></i>
		{{ $row->nama }}
	</h5>
	@endforeach
@else
	<h5 class="not-found"><i>Data Pelatihan dan Fellowship belum dilengkapi</i></h5>
@endif

<hr>

<h4>Kualifikasi Internasional</h4>
@if(isset($internasional) && count($internasional) > 0)
	@foreach($internasional as $row)
	<h5 class="list-dokter">
		<i class="fa fa-angle-right"></i>
		{{ $row->nama }}
	</h5>
	@endforeach
@else
	<h5 class="not-found"><i>Data Kualifikasi Internasional belum dilengkapi</i></h5>
@endif	


<hr>

<h4>Keanggotaan Profesi</h4>
@if(isset($keanggotaan) && count($keanggotaan) > 0)
	@foreach($keanggotaan as $row)
	<h5 class="list-dokter">
		<i class="fa fa-angle-right"></i>
		{{ $row->nama }}
	</h5>
	@endforeach
@else
	<h5 class="not-found"><i>Data Keanggotaan Profesi belum dilengkapi</i></h5>
@endif


