@extends('layouts.frontend-scaffold')

@section('css')
<style>
.cover {
  max-height: 19rem;
  object-fit: cover;
}
.card-body {
  padding-left: 0;
  padding-right: 0;
}
</style>
@endsection
@section('js')
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        $('body').on('click', '.btn-search', function(e) {
            getDoctors("{{$pageUrl}}");
        });
        
        $('body').on('click', '.pagination a', function(e) {
            e.preventDefault();

            var url = $(this).attr('href');  
            getDoctors(url);
            // window.history.pushState("", "", url);
        });

        function getDoctors(url) {
            $('.dokter-container a').css('color', '#dfecf6');
            $('.dokter-container').append('<img style="position: absolute; left: 50%; top: 50%; z-index: 100000;" src="{{asset('img/loading.gif')}}" />');

            $.ajax({
                url : url,
                data : {
                  spesialisasi: $('select[name=spesialisasi]').val(),
                  nama: $('input[name=nama]').val()
                }
            }).done(function (data) {
                $('.dokter-container').html(data);  
            }).fail(function () {
                alert('Data dokter gagal diload.');
            });
        }
    });
</script>
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-dokter']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="row pb-4">
    <div class="col-12 content-main">
      <div class="row justify-content-between mt-4">
        <nav aria-label="breadcrumb" class="col-12 col-md-4">
          <ol class="breadcrumb mb-0 p-0">
            <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tim Dokter</li>
          </ol>
        </nav>

        {{-- {{ $sspesialisasi }} --}}

        <div class="col-12 col-md-8 pr-0 mb-4">
          <div class="form form-inline justify-content-start justify-content-lg-end" method="GET">
            <div class="form-group mr-3 mb-2">
              <label for="dokter" class="sr-only">Spesialisasi</label>
              <select name="spesialisasi" id="" class="form-control select2">
                <option value="">--Pilih spesialisasi--</option>
                
                @foreach($sspesialisasi as $ss)
                  <option value="{{ $ss->id }}">{{ $ss->nama }}</option>
                @endforeach

              </select>
            </div>
            <div class="form-group mr-3 mb-2">
              <label for="dokter" class="sr-only">Nama Dokter</label>
              <input type="text" name="nama" class="form-control" placeholder="Nama Dokter" aria-label="Nama Dokter" aria-describedby="filter-dokter" value="{{ $nama }}">
            </div>
            <button type="button" class="btn btn-outline-info mr-3 mb-2 btn-search">Cari</button>
          </div>
        </div>
      </div>
      
      <div class="dokter-container">
        @if(count($record) > 0)
          @include('frontend.tim-dokter.list')
        @endif
      </div>
    </div>
  </div>
</div>
@endsection