@extends('layouts.frontend-scaffold')

@section('styles')
<style>
.card-img-top{
    border:1px solid #3b9ca5;
    border-radius: .2rem;
}
.nav-details {
  border: 1px solid #3b9ca5;
}
.nav-details .nav-link.active {
  background: #3b9ca5;
  border-radius: 0;
}
table tr th{
    vertical-align: middle;
}
.not-found{
    color:#888;
    font-weight: 300;
    font-size: 1.2em;
}
.list-dokter{
    color:#000;
    font-weight: 300;
    padding-left: 15px;
    font-size: 1.2em;
}
.list-dokter .fa{
    color:#3b9ca5;
    margin-right: 5px;
}
.badge-lg{
    font-size: 1em;
}
.btn-xs{
    font-size: .8em;
    display: block;
    margin: auto;
    font-weight: bold;
    padding: 2px 5px;
}
</style>
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-dokter']['isi']) }}" width="100%"></div>
</div>

<div class="container">
    <div class="row mb-4">
        <div class="col-12 content-main">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tim Dokter</li>
                </ol>
            </nav>

            @if(isset($record))
            <div class="row mb-3">
                <div class="col-12 col-md-3">
                    <img class="card-img-top cover" style="object-position: top center; object-fit: cover;" src="@if(!is_null($record->photo)) {{ asset('storage/' . $record->photo ) }} @else {{ asset('img/noImage.png' ) }} @endif" alt="Foto" height="300">
                </div>
                <div class="col-12 col-md-9">
                    <div class="dokter-title mb-2">
                        <h2>{{ $record->nama_lengkap }}</h2>
                        {{-- <span>{{ $record->spesialisasi->nama }}</span> --}}<span>{{ ($record->jabatandokter) ? $record->jabatandokter->jabatan : '' }}</span>
                    </div>
                    <ul class="nav nav-pills nav-fill my-3 nav-details" id="detail" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="keahlian-tab" data-toggle="tab" href="#keahlian" role="tab" aria-controls="keahlian" aria-selected="true">Keahlian</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pendidikan-tab" data-toggle="tab" href="#pendidikan" role="tab" aria-controls="pendidikan" aria-selected="false">Kualifikasi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="jadwal-tab" data-toggle="tab" href="#jadwal" role="tab" aria-controls="jadwal" aria-selected="false">Jadwal Praktek</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="detailContent">
                        <div class="tab-pane fade show active" id="keahlian" role="tabpanel" aria-labelledby="keahlian-tab">
                            @include('frontend.tim-dokter.detail.keahlian')
                        </div>
                        <div class="tab-pane fade" id="pendidikan" role="tabpanel" aria-labelledby="pendidikan-tab">
                            @include('frontend.tim-dokter.detail.pendidikan')
                        </div>
                        <div class="tab-pane fade" id="jadwal" role="tabpanel" aria-labelledby="jadwal-tab">
                            @include('frontend.tim-dokter.detail.jadwal')
                        </div>
                    </div>

                </div>
            </div>
            @else
            <h2>Tidak ada data</h2>
            @endif
        </div>
    </div>
</div>
@endsection