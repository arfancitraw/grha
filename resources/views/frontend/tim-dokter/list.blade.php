<div class="row cards justify-content-start">
    @if(!is_null($record))
        @foreach($record as $data)
        <a href="{{ url('/tim-dokter') }}/{{$data->id}}/detail" class="card border-light text-center col-12 col-md-6 col-lg-2">
            <img class="card-img-top cover mt-3" style="object-position: top" src="@if(!is_null($data->photo)) {{ asset('storage/' . $data->photo ) }} @else {{ asset('img/noImage.png' ) }} @endif" alt="Foto" height="180">
            <div class="card-body">
                <h6 class="card-title mb-0">{{ $data->nama_lengkap }}</h6>
                <small class="card-text text-center">{{ $data->spesialisasi->nama }}</small>
            </div>
        </a>
        @endforeach
    @else
        <h2>Data Dokter Tidak Ditemukan</h2>
    @endif
</div>

<div class="mt-5 justify-content-center">
    {{ $record->links() }}
</div>