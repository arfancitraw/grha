@extends('layouts.frontend-scaffold-sidebar')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datepicker/datepicker.min.css') }}">
@append

@section('scripts')
  <script src="{{ asset('plugins/datepicker/datepicker.min.js') }}" type="text/javascript"></script>
  <script>
    $(document).ready(function($) {
      var active = $('.mutu.menu.active').data('id');
      displayMutu(active);
      // alert(active);
      $(".periode").datepicker( {
        format: "mm-yyyy",
          viewMode: "months", 
          minViewMode: "months",
          autoclose: true
      });
    });

    $(document).on('click', '.mutu.menu', function(e){
      $('.mutu.menu').removeClass('active');
      $(this).addClass('active');
      var active = $(this).data('id');
      displayMutu(active);
    });

    $(document).on('click', '.btn-cari', function(e){
      var active = $('.mutu.menu.active').data('id');
      displayMutu(active);
    });

    function displayMutu(id) {
      var periode = $('[name=periode]').val();

      $.ajax({
        url: '{{ $pageUrl }}',
        type: 'POST',
        data: {_token: '{{ csrf_token() }}', periode:periode, id:id},
      })
      .done(function(resp) {
        $('.indikator-container').html(resp);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    }
  </script>
@append

@section('jumbotron')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-informasi']['isi']) }}" width="100%"></div>
</div>
@endsection

@section('content-sidebar')
<div class="p-3 pt-4">
  <h2>Indikator Mutu</h2>
  {{-- <p>Doloremque deleniti! Reprehenderit perspiciatis facilis fugiat, fuga eaque quisquam, harum.</p> --}}
</div>
@include('frontend.mutu.menu')
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Indikator Mutu</a></li>
    {{-- <li class="breadcrumb-item active" aria-current="page">{{ $record->judul }}</li> --}}
  </ol>
</nav>
@endsection

@section('content-main')
<div class="row mb-4">
  <h2 class="col-8">Indikator Mutu</h2>
  
  <div class="col-4 pr-0">
    {{-- <form class="form form-inline justify-content-end" action="{{ url($pageUrl) }}" method="GET">
      <label for="dokter" class="sr-only">Periode</label>
      <input type="text" name="periode" class="form-control periode" placeholder="Periode Mutu">
      <button class="btn btn-outline-info btn-cari ml-2" type="button">Cari</button>
    </form> --}}
  </div>
</div>

<div class="indikator-container">
  <div class="text-center">
    Belum terdapat data indikator mutu yang diinputkan ke sistem.
  </div>
</div>

@endsection