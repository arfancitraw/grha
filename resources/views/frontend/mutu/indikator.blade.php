@foreach($indikator as $mutu)
<h6 class="text-center">{{ $mutu->nama }}</h6>
<div class="text-center my-3"> 
	<img src="{{ url('storage/'.$mutu->gambar) }}" alt="sample" class="img-responsive img-thumbnail">
</div>

<p>{!! $mutu->deskripsi !!}</p>
@endforeach