@extends('layouts.frontend-scaffold-sidebar')

@section('styles')
@endsection

@section('jumbotron')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-informasi']['isi']) }}" width="100%"></div>
</div>
@endsection

@section('content-sidebar')
  @include('frontend.informasi.menu')
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Informasi</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $record->judul }}</li>
  </ol>
</nav>
@endsection

@section('content-main')
<h2>{{ $record->judul }}</h2>

<div class="text-center my-3"> 
@if($record->gambar != "")
<img src="{{ url('/storage/'.$record->gambar) }}" alt="Featured image" class="img-fluid rounded">
@endif
</div>

{!! $record->konten !!}

@if(count($record->lampiran) > 0)
  <ul class="list-group">
    @foreach($record->lampiran as $lampiran)
    <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
      {{ $lampiran->judul }}
      <a href="{{ url('storage/'.$lampiran->file) }}" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Unduh</a>
    </li>
    @endforeach
  </ul>
@endif
@endsection