@extends('layouts.frontend-scaffold-sidebar')

@section('jumbotron')
<div class="jumbotron pasien">
  <div class="container d-flex">
    {{-- <div class="col-12 col-lg-6 d-none d-lg-block">
      <h1 class="display-3"><span class="text-danger">Pasien</span> &amp; Pengunjung</h1>
      <p>Quos perferendis unde incidunt suscipit nostrum, vitae aliquam quasi illum reiciendis, obcaecati iste aut molestias culpa quae minima natus? Doloribus, sint, provident.</p>
    </div> --}}
     
  </div>
</div>
@endsection

@section('content-sidebar')
  <div class="p-3 pt-4">
    <h2>Pasien dan Pengunjung</h2>
    {{-- <p>Doloremque deleniti! Reprehenderit perspiciatis facilis fugiat, fuga eaque quisquam, harum.</p> --}}
  </div>
  <div class="menu">
    <a href="{{ url('pasien/alur-pendaftaran') }}" class="active">Alur Pendaftaran</a>
    <a href="{{ url('pasien/aturan-pengunjung') }}">Aturan Pengunjung</a>
  </div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Pasien</a></li>
    <li class="breadcrumb-item active" aria-current="page">Alur Pendaftaran</li>
  </ol>
</nav>
@endsection

@section('content-main')

@foreach($record as $raw)
<h2>{{ $raw->judul }}</h2>
    <div class="text-center">
      <img class="img-fluid" src="{{ asset('storage') }}/{{$raw->photo}}">
    </div>
@endforeach
@endsection