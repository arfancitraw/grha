@extends('layouts.frontend-scaffold')

@section('styles')
<style>
blockquote {
  border-left: none;
  font-style: italic;
}

.ratings i {
  color: grey
}
.ratings i.orange {
  color: orange
}

.sender {
  line-height: 1.3
}
</style>
@endsection

@section('content')
<div class="jumbotron pasien">
  <div class="container d-flex">     
  </div>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col-12 text-center content-main">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item">Info Pasien</li>
          <li class="breadcrumb-item active" aria-current="page">Testimoni</li>
        </ol>
      </nav>

      <h2 class="mb-4">Testimoni</h2>

      <div class="list-testimoni">
        <div class="row justify-content-center align-items-end">
          @if (count($record) < 1)
          <blockquote>"Belum ada Testimoni"</blockquote>
          
          @elseif (count($record) >= 1)
          @foreach($record as $data)
          <div class="col-12 col-md-6 col-lg-4 text-left mb-4">
            <blockquote>"{{ $data->testimoni }}"</blockquote>
            <div class="row">
             <div class="col-6 ratings">
              @for ($i = 0; $i < 5; $i++)
              <i class="fa fa-star @if($i < $data->rating) orange @endif"></i>
              @endfor
            </div>
            <div class="col-6 sender">
              <span class="d-block w-100 text-right">{{$data->nama}}<br>{{$data->pekerjaan}}</span>
            </div>
          </div>
        </div>        
        @endforeach
        @endif

      </div>
    </div>
    {{ $record->links() }}

    <div class="justify-content-center mt-5">
      <a class="btn btn-info btn-lg text-white" data-toggle="modal" data-target="#testimoniModal"><i class="fa fa-send"></i> Buat Testimoni</a>
    </div>

  </div>
</div>
</div>

<div class="modal fade" id="testimoniModal" tabindex="-1" role="dialog" aria-labelledby="testimoniModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">

    <form class="form" id="testimoniForm" >
      {!! csrf_field() !!}
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Survey Testimoni Pelanggan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          {{-- <p>Bantu kami memperbaiki sistem pelayanan PJNHK dengan mengisi testimoni yang telah kami sediakan.</p> --}}
          <div class="testimoni">
            <fieldset>
              <div class="form-group">
                <input type="text" name="nama" class="form-control" placeholder="Nama">
              </div>
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email">
              </div>
              <div class="form-group">
                <input type="text" name="pekerjaan" class="form-control" placeholder="Pekerjaan">
              </div>
              <div class="testimoni-buttons d-flex justify-content-end">
              </div>
              <div class="form-group">
                <div class="rating">
                  <input type="radio" id="5" name="rating" value="5" /><label for="5" title="Rocks!">5 stars</label>
                  <input type="radio" id="4" name="rating" value="4" /><label for="4" title="Pretty good">4 stars</label>
                  <input type="radio" id="3" name="rating" value="3" /><label for="3" title="Meh">3 stars</label>
                  <input type="radio" id="2" name="rating" value="2" /><label for="2" title="Kinda bad">2 stars</label>
                  <input type="radio" id="1" name="rating" value="1" /><label for="1" title="Sucks big time">1 star</label>
                </div>
              </div>
            </fieldset>
            <fieldset>
              <div class="form-group">
                <textarea type="text" name="testimoni" class="form-control" rows="4" placeholder="Testimoni Anda sangat membantu kami"></textarea>
              </div>


              <div class="testimoni-buttons d-flex justify-content-between">
                <button type="button" class="btn btn-previous">Kembali</button>
                <button type="button" class="btn btn-submit"><i class="fa fa-paper-plane"></i> Kirim feedback</button>
              </div>
            </fieldset>

          </div>

        </div>
        {{-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div> --}}
      </div>
    </form>
  </div>
</div>

@endsection