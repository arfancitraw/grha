@extends('layouts.frontend-scaffold-sidebar')

@section('jumbotron')
<div class="jumbotron rujukan">
  <div class="container d-flex">
    <div class="col-12 col-lg-6 d-none d-lg-block">
      <h1 class="display-3"><span class="text-danger">Rujukan</span> Nasional</h1>
      <p>Quos perferendis unde incidunt suscipit nostrum, vitae aliquam quasi illum reiciendis, obcaecati iste aut molestias culpa quae minima natus? Doloribus, sint, provident.</p>
    </div>
     
  </div>
</div>
@endsection

@section('content-sidebar')
  <div class="nav menu flex-column" id="v-pills-tab" role="tablist" aria-orientation="vertical">
    <a class="nav-link active" id="pengantar-tab" data-toggle="pill" href="#pengantar" role="tab" aria-controls="pengantar" aria-selected="true">Pengantar</a>
    <a class="nav-link" id="profil-tab" data-toggle="pill" href="#profil" role="tab" aria-controls="profil" aria-selected="false">Profil</a>
    <a class="nav-link" id="produk-tab" data-toggle="pill" href="#produk" role="tab" aria-controls="produk" aria-selected="false">Produk Layanan</a>
    <a class="nav-link" id="tim-tab" data-toggle="pill" href="#tim" role="tab" aria-controls="tim" aria-selected="false">Tim Layanan Unggulan</a>
    <a class="nav-link" id="fasilitas-tab" data-toggle="pill" href="#fasilitas" role="tab" aria-controls="fasilitas" aria-selected="false">Fasilitas Pelayanan</a>
    <a class="nav-link" id="penelitian-tab" data-toggle="pill" href="#penelitian" role="tab" aria-controls="penelitian" aria-selected="false">Penelitian &amp; Publikasi</a>
    <a class="nav-link" id="jejaring-tab" data-toggle="pill" href="#jejaring" role="tab" aria-controls="jejaring" aria-selected="false">Jejaring RS Mitra</a>
    <a class="nav-link" id="agenda-tab" data-toggle="pill" href="#agenda" role="tab" aria-controls="agenda" aria-selected="false">Agenda Kegiatan Ilmiah Mendatan</a>
    <a class="nav-link" id="aksesibilitas-tab" data-toggle="pill" href="#aksesibilitas" role="tab" aria-controls="aksesibilitas" aria-selected="false">Aksesibilitas</a>
    <a class="nav-link" id="cara-rujuk-tab" data-toggle="pill" href="#cara-rujuk" role="tab" aria-controls="cara-rujuk" aria-selected="false">Cara Merujuk</a>
  </div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Rujukan Nasional</a></li>
    <li class="breadcrumb-item active" aria-current="page">Transplantasi</li>
  </ol>
</nav>
@endsection

@section('content-main')
<h2>Transplantasi</h2>

<div class="tab-content" id="v-pills-tabContent">
  <div class="tab-pane fade show active" id="pengantar" role="tabpanel" aria-labelledby="pengantar-tab">
    <img src="{{ asset('/img/backgrounds/sample2.jpg') }}" alt="nani?" class="img-fluid my-3 rounded" width="100%">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque animi, quibusdam alias temporibus ab perferendis ullam laboriosam asperiores nulla quis, doloribus obcaecati dolores accusamus suscipit vel libero error vero sit.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque animi, quibusdam alias temporibus ab perferendis ullam laboriosam asperiores nulla quis, doloribus obcaecati dolores accusamus suscipit vel libero error vero sit.</p>
  </div>
  <div class="tab-pane fade" id="profil" role="tabpanel" aria-labelledby="profil-tab">...</div>
  <div class="tab-pane fade" id="produk" role="tabpanel" aria-labelledby="produk-tab">...</div>
  <div class="tab-pane fade" id="tim" role="tabpanel" aria-labelledby="tim-tab">...</div>
  <div class="tab-pane fade" id="fasilitas" role="tabpanel" aria-labelledby="fasilitas-tab">...</div>
  <div class="tab-pane fade" id="penelitian" role="tabpanel" aria-labelledby="penelitian-tab">...</div>
  <div class="tab-pane fade" id="jejaring" role="tabpanel" aria-labelledby="jejaring-tab">...</div>
  <div class="tab-pane fade" id="agenda" role="tabpanel" aria-labelledby="agenda-tab">...</div>
  <div class="tab-pane fade" id="aksesibilitas" role="tabpanel" aria-labelledby="aksesibilitas-tab">...</div>
  <div class="tab-pane fade" id="cara-rujuk" role="tabpanel" aria-labelledby="cara-rujuk-tab">...</div>
</div>
@endsection