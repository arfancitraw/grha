@extends('layouts.frontend-scaffold')

@section('styles')
<style>
.featured-image {
    width: 100%;
    height: 100%;
    object-fit: cover;
    min-height: 15rem;
}

@media (min-width: 768px) {
    .featured-image {
        width: 100%;
        height: auto;
        max-height: 25rem;
    }
}
</style>
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-rujukan']['isi']) }}" width="100%"></div>
</div>

<div class="container">
    <div class="row mb-4">
        <div class="col-12 text-center content-main">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Rujukan Nasional</li>
                </ol>
            </nav>

            <h2>Rujukan Nasional</h2>
        </div>
    </div>
</div>

<div class="list-rujukan">
    <div class="mt-5 justify-content-center">

        @if(isset($record) and count($record) > 0)
        @foreach($record as $key => $row)
        <div class="d-block d-md-flex {{ $key % 2 == 1 ? 'flex-row-reverse' : 'flex-row' }}">
            <div class="d-blocks col-12 col-md-6 p-0">
                <img src="{{ url('storage/'.$row->photo) }}" alt="icon" class="featured-image">
            </div>
            <div class="d-block col-12 col-md-6 p-0">
                <div class="p-5">
                    <h3 class="d-block">{{ session('lang') == 'english' ? (!is_null($row->judul_en) ? $row->judul_en : $row->judul) : $row->judul }}</h3>
                    <span>{{ str_limit(session('lang') == 'english' ? (!is_null($row->deskripsi_en) ? $row->deskripsi_en : $row->deskripsi) : $row->deskripsi, 300) }}</span>
                    <a class="btn btn-block btn-outline-info" href="{{ url('rujukan-nasional/'.str_slug($row->judul)) }}" style="margin-top:5em">Selengkapnya <i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
        </div>
        @endforeach
        @endif

    </div>
</div>
@endsection