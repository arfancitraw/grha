@extends('layouts.frontend-scaffold-sidebar')

@section('jumbotron')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-rujukan']['isi']) }}" width="100%"></div>
</div>
@endsection

@section('content-sidebar')
<div class="p-3 pt-4">
  <h2>{{ session('lang') == 'english' ? (!is_null($record->judul_en) ? $record->judul_en : $record->judul) : $record->judul }}</h2>
</div>
<div class="nav menu flex-column" id="v-pills-tab" role="tablist" aria-orientation="vertical">
  <a class="nav-link active" id="pengantar-tab" data-toggle="pill" href="#pengantar" role="tab" aria-controls="pengantar" aria-selected="true">Pengantar</a>
  <a class="nav-link" id="profil-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="profil" aria-selected="false">Profil</a>
  <a class="nav-link" id="produk-tab" data-toggle="pill" href="#produk" role="tab" aria-controls="produk" aria-selected="false">Produk Layanan</a>
  <a class="nav-link" id="tim-tab" data-toggle="pill" href="#tim" role="tab" aria-controls="tim" aria-selected="false">Tim Layanan Unggulan</a>
  <a class="nav-link" id="fasilitas-tab" data-toggle="pill" href="#fasilitas" role="tab" aria-controls="fasilitas" aria-selected="false">Fasilitas Pelayanan</a>
  <a class="nav-link" id="penelitian-tab" data-toggle="pill" href="#penelitian" role="tab" aria-controls="penelitian" aria-selected="false">Penelitian &amp; Publikasi</a>
  <a class="nav-link" id="jejaring-tab" data-toggle="pill" href="#jejaring" role="tab" aria-controls="jejaring" aria-selected="false">Jejaring RS Mitra</a>
  <a class="nav-link" id="agenda-tab" data-toggle="pill" href="#agenda" role="tab" aria-controls="agenda" aria-selected="false">Agenda Kegiatan Ilmiah</a>
  <a class="nav-link" id="aksesibilitas-tab" data-toggle="pill" href="#aksesibilitas" role="tab" aria-controls="aksesibilitas" aria-selected="false">Aksesibilitas</a>
  <a class="nav-link" id="cara-rujuk-tab" data-toggle="pill" href="#cara-rujuk" role="tab" aria-controls="cara-rujuk" aria-selected="false">Cara Merujuk</a>
</div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Rujukan Nasional</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $record->judul }}</li>
  </ol>
</nav>
@endsection

@section('content-main')
<div class="tab-content d-none d-md-block" id="v-pills-tabContent">
  <div class="tab-pane fade show active" id="pengantar" role="tabpanel" aria-labelledby="pengantar-tab">
    <h2 class="mb-3">Pengantar</h2>
    {!! session('lang') == 'english' ? (!is_null($record->pengantar_en) ? $record->pengantar_en : $record->pengantar) : $record->pengantar !!}
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profil-tab">
    <h2 class="mb-3">Profil</h2>
    {!! session('lang') == 'english' ? (!is_null($record->profile_en) ? $record->profile_en : $record->profile) : $record->profile !!}
  </div>
  <div class="tab-pane fade" id="produk" role="tabpanel" aria-labelledby="produk-tab">
    <h2 class="mb-3">Produk Layanan Unggulan</h2>
    {!! session('lang') == 'english' ? (!is_null($record->produk_en) ? $record->produk_en : $record->produk) : $record->produk !!}
  </div>
  <div class="tab-pane fade" id="tim" role="tabpanel" aria-labelledby="tim-tab">
    <h2 class="mb-3">Tim Layanan Unggulan</h2>
    {!! session('lang') == 'english' ? (!is_null($record->tim_en) ? $record->tim_en : $record->tim) : $record->tim !!}
    @if(count($record->dokter) > 0)
      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>No.</th>
            <th>Dokter</th>
            <th>Posisi</th>
          </tr>
        </thead>
        <tbody>
          @php $i = 1; @endphp
          @foreach($record->dokter as $dokter)
            <tr>
              <td>{{ $i++ }}</td>
              <td><b>{{ $dokter->nama_lengkap }}</b></td>
              <td>{{ $dokter->pivot->posisi }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif
  </div>
  <div class="tab-pane fade" id="fasilitas" role="tabpanel" aria-labelledby="fasilitas-tab">
    <h2 class="mb-3">Fasilitas Layanan Unggulan</h2>
    {!! session('lang') == 'english' ? (!is_null($record->fasilitas_en) ? $record->fasilitas_en : $record->fasilitas) : $record->fasilitas !!}
  </div>
  <div class="tab-pane fade" id="penelitian" role="tabpanel" aria-labelledby="penelitian-tab">
    <h2 class="mb-3">Penelitian &amp; Publikasi</h2>
    {!! session('lang') == 'english' ? (!is_null($record->penelitian_en) ? $record->penelitian_en : $record->penelitian) : $record->penelitian !!}
  </div>
  <div class="tab-pane fade" id="jejaring" role="tabpanel" aria-labelledby="jejaring-tab">
    <h2 class="mb-3">Jejaring RS Mitra</h2>
    {!! session('lang') == 'english' ? (!is_null($record->jejaring_en) ? $record->jejaring_en : $record->jejaring) : $record->jejaring !!}
  </div>
  <div class="tab-pane fade" id="agenda" role="tabpanel" aria-labelledby="agenda-tab">
    <h2 class="mb-3">Agenda Kegiatan Ilmiah</h2>
    {!! session('lang') == 'english' ? (!is_null($record->agenda_en) ? $record->agenda_en : $record->agenda) : $record->agenda !!}
  </div>
  <div class="tab-pane fade" id="aksesibilitas" role="tabpanel" aria-labelledby="aksesibilitas-tab">
    <h2 class="mb-3">Aksesibilitas</h2>
    {!! session('lang') == 'english' ? (!is_null($record->aksesibilitas_en) ? $record->aksesibilitas_en : $record->aksesibilitas) : $record->aksesibilitas !!}
  </div>
  <div class="tab-pane fade" id="cara-rujuk" role="tabpanel" aria-labelledby="cara-rujuk-tab">
    <h2 class="mb-3">Cara Merujuk</h2>
    {!! session('lang') == 'english' ? (!is_null($record->cara_merujuk_en) ? $record->cara_merujuk_en : $record->cara_merujuk) : $record->cara_merujuk !!}
  </div>
</div>
@endsection