@extends('layouts.frontend-scaffold-sidebar')

@section('jumbotron')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>
@endsection

@section('content-sidebar')
  <div class="p-3 pt-4">
    <h2>Artikel</h2>
  </div>
  <div class="menu less-padding">
    @if(isset($bulan))
    <ol class="list-unstyled mb-0">
      <li><a href="{{url('artikel')}}">Semua Artikel</a></li>
      @foreach($bulan as $key => $month)
      <li><a href="{{ url('artikel/list/'.$month->format('Y-m')) }}" class="py-1">{{ $month->format('F Y') }}</a></li>
      @endforeach
    </ol>
    @endif
  </div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item active" aria-current="page">Artikel</li>
  </ol>
</nav>
@endsection

@section('content-main')
@if(count($record) > 0)
  @foreach($record as $rec)
  <div class="blog-post">
    <div class="row">
      <div class="col-12 col-md-4 col-lg-3">
        <img src="{{ $rec->photo ? url('storage/'.$rec->photo) : asset('img/plc.png') }}" alt="icon" class="featured-image" width="200px">
      </div>
      <div class="col-12 col-md-8 col-lg-9">
      <a href="{{ url('artikel/'.$rec->slug) }}"><h5 class="blog-post-title mb-0">{{ session('lang') == 'english' ? (!is_null($rec->judul_en) ? $rec->judul_en : $rec->judul) : $rec->judul }}</h5></a>
      <small class="blog-post-meta text-muted">
        <i class="fa fa-user"></i> {{ $rec->creator->name or '' }} | 
        <i class="fa fa-calendar"></i> {{ $rec->tanggal ? $rec->tanggal->format('d F Y') : $rec->created_at->format('d F Y') }}
      </small>
      <p>{!! str_limit(strip_tags( session('lang') == 'english' ? (!is_null($rec->konten_en) ? $rec->konten_en : $rec->konten) : $rec->konten ), 190) !!}</p>
      <a class="btn btn-block btn-sm btn-outline-info mt-4" href="{{ url('artikel/'.$rec->slug) }}">Selengkapnya <i class="fa fa-chevron-right"></i></a>
      </div>
    </div>
  </div>
  @endforeach
  {{ $record->render() }}
@else
    <h5 class="text-center"><i>Belum terdapat Artikel yang di publish</i></h5>
@endif
@endsection