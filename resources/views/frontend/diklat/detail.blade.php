@extends('layouts.frontend-scaffold')

@section('styles')
  <style type="text/css" media="screen">
    .pop{
      cursor: pointer;
    }
  </style>
@append

@section('scripts')
<script>
  $(function() {
      $('.pop').on('click', function() {
        $('.imagepreview').attr('src', $(this).attr('src'));
        $('#imagemodal').modal('show');   
      });   
  });
</script>
@append

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col-12 content-main">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item"><a href="#">Diklat</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{ session('lang') == 'english' ? (!is_null($diklat->judul_en) ? $diklat->judul_en : $diklat->judul) : $diklat->judul }}</li>
        </ol>
      </nav>

      <h2>{{ session('lang') == 'english' ? (!is_null($diklat->judul_en) ? $diklat->judul_en : $diklat->judul) : $diklat->judul }}</h2>

      <div class="row">
        <div class="col-12 col-md-4 text-center mb-4"> 
          <img src="{{ url('storage/'.$diklat->gambar) }}" class="img-responsive img-thumbnail pop" width="100%" alt="{{ $diklat->judul }}">
        </div>
        
        
        <div class="col-12 col-md-8">
          <ul class="list-group">
            <li class="list-group-item">{!! session('lang') == 'english' ? (!is_null($diklat->konten_en) ? $diklat->konten_en : $diklat->konten) : $diklat->konten !!}</li>
            <li class="list-group-item">{!! nl2br(e( session('lang') == 'english' ? (!is_null($diklat->keterangan_en) ? $diklat->keterangan_en : $diklat->keterangan) : $diklat->keterangan )) !!}</li>
            <li class="list-group-item">
              <small class="row">
                <div class="col text-left">Tanggal : {{ $diklat->tanggal->format('d F Y') }}</div>
                <div class="col text-right">Jam Pelaksanaan : {{ $diklat->jam }}</div>
              </small>
            </li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection

@section('modals')
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">              
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <img src="" class="imagepreview" style="width: 100%;" >
          </div>
        </div>
      </div>
    </div>
@append