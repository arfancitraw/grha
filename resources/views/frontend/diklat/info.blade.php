@if(isset($jadwal) && count($jadwal) > 0)
<div class="col-12 my-3">
	<div class="card bg-info">
		<h5 class="card-header text-white">Jadwal Diklat</h5>
		<div class="card-body bg-white p-0">
			<table class="table table-bordered m-0">
				<thead>
					<tr>
						<th width="150px">Tanggal</th>
						<th>Judul</th>
						<th width="150px">Jam Pelaksanaan</th>
					</tr>
				</thead>
				<tbody>
				@foreach($jadwal as $row)
					<tr>
						<td style="text-align:right">{{ $row->tanggal->format('d F Y') }}</td>
						<td style="text-align:left"><a href="{{ url('diklat/'.$row->id) }}">{{ session('lang') == 'english' ? (!is_null($row->judul_en) ? $row->judul_en : $row->judul) : $row->judul }}</a></td>
						<td>{{ $row->jam }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endif