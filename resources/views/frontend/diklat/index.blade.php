@extends('layouts.frontend-scaffold')

@section('styles')
<style>
  table th, table td {
    text-align: center;
  }
</style>
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col-12 content-main">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item active" aria-current="page">Diklat</li>
        </ol>
      </nav>
    
      <h2>Info Diklat</h2>
      
      @foreach($diklat as $row)
          <blockquote> {{ session('lang') == 'english' ? (!is_null($row->judul_en) ? $row->judul_en : $row->judul) : $row->judul }} </blockquote>
          <p>{!! session('lang') == 'english' ? (!is_null($row->konten_en) ? $row->konten_en : $row->konten) : $row->konten !!}</p>
      @endforeach
       


      <div class="row mb-4">
        @include('frontend.diklat.info')
      </div>

    </div>
  </div>
</div>
@endsection