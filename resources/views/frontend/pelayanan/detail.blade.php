@extends('layouts.frontend-scaffold')

@section('css')
@endsection
@section('js')
@endsection
@section('scripts')
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-service']['isi']) }}" width="100%"></div>
</div>
<div class="container">
  <div class="row mb-4">
    <div class="d-none d-md-block col-md-3 d-md-block content-sidebar">
      <aside>
        <div class="p-3 pt-4">
          <h2>{{ isset($menu->first()->tautan) ? $menu->first()->parent->judul : 'Pelayanan' }}</h2>
          {{-- <p>Doloremque deleniti! Reprehenderit perspiciatis facilis fugiat, fuga eaque quisquam, harum.</p> --}}
        </div>
        <div class="menu" id="menu-sidebar">
          @foreach($menu as $m)
          @if(isset($m->tautan))
            @if($m->tautan != 'separator' && $m->tautan != 'layanan-unggulan' && count($m->childs) == 0)
              <a class="{{ $m->tautan == Request::url() ? 'active' : '' }}" href="{{ $m->tautan }}">{{ $m->judul }}</a>
            @elseif(count($m->childs) > 0)
              <div class="btn-group dropright w-100">
                <a class="btn btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{ $m->judul }}
                </a>
                <div class="dropdown-menu">
                  @foreach($m->childs as $c)
                  <a href="{{ $c->tautan }}">{{ $c->judul }}</a>
                  @endforeach
                </div>
              </div>
            @endif
          @else
          <a class="{{ $m->slug == $record->slug ? 'active' : '' }}" href="{{ url('pelayanan/'.$m->slug) }}">{{ $m->judul }}</a>
          @endif
          @endforeach
        </div>
      </aside>
    </div>

    <div class="col-12 col-md-9 content-main">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item active" aria-current="page">Pelayanan</li>
        </ol>
      </nav>

      <h2>{{ $record->judul }}</h2>
      @if(!is_null($record->photo))
      <div class="text-center my-3"> 
        <img src="{{ url('/storage/'.$record->photo) }}" alt="Featured image" class="img-fluid rounded">
      </div>
      @endif

      {!! $record->konten !!}

    </div>
  </div>
</div>
@endsection