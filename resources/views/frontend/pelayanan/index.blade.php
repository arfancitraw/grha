@extends('layouts.frontend-scaffold')

@section('css')
@endsection
@section('js')
@endsection
@section('scripts')
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-service']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col-12 text-center content-main">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item"><a href="#">Pelayanan</a></li>
          <li class="breadcrumb-item active" aria-current="page">Item Pelayanan</li>
        </ol>
      </nav>

      <h2>Item Pelayanan</h2>

      <div class="list-rujukan">
        <div class="row my-5">
          <div class="col-12 col-md-3">
            <a href="{{ url('pelayanan/umum/detail') }}">
              <img src="{{ asset('img/rujukan/icon_rujukan_ginjal.png') }}" alt="icon" height="120">
              <span class="d-block mt-4">Transplantasi Ginjal</span>
            </a>
          </div>
          <div class="col-12 col-md-3">
            <a href="{{ url('pelayanan/umum/detail') }}">
              <img src="{{ asset('img/rujukan/icon_rujukan_pelayanan_terpadu.png') }}" alt="icon" height="120">
              <span class="d-block mt-4">Pelayanan Terpadu</span>
            </a>
          </div>
          <div class="col-12 col-md-3">
            <a href="{{ url('pelayanan/umum/detail') }}">
              <img src="{{ asset('img/rujukan/icon_rujukan_hati.png') }}" alt="icon" height="120">
              <span class="d-block mt-4">Transplantasi Hati</span>
            </a>
          </div>
          <div class="col-12 col-md-3">
            <a href="{{ url('pelayanan/umum/detail') }}">
              <img src="{{ asset('img/rujukan/icon_rujukan_ginjal.png') }}" alt="icon" height="120">
              <span class="d-block mt-4">Transplantasi Ginjal</span>
            </a>
          </div>
          <div class="col-12 col-md-3">
            <a href="{{ url('pelayanan/umum/detail') }}">
              <img src="{{ asset('img/rujukan/icon_rujukan_pelayanan_terpadu.png') }}" alt="icon" height="120">
              <span class="d-block mt-4">Pelayanan Terpadu</span>
            </a>
          </div>
          <div class="col-12 col-md-3">
            <a href="{{ url('pelayanan/umum/detail') }}">
              <img src="{{ asset('img/rujukan/icon_rujukan_hati.png') }}" alt="icon" height="120">
              <span class="d-block mt-4">Transplantasi Hati</span>
            </a>
          </div>
          <div class="col-12 col-md-3">
            <a href="{{ url('pelayanan/umum/detail') }}">
              <img src="{{ asset('img/rujukan/icon_rujukan_ginjal.png') }}" alt="icon" height="120">
              <span class="d-block mt-4">Transplantasi Ginjal</span>
            </a>
          </div>
          <div class="col-12 col-md-3">
            <a href="{{ url('pelayanan/umum/detail') }}">
              <img src="{{ asset('img/rujukan/icon_rujukan_pelayanan_terpadu.png') }}" alt="icon" height="120">
              <span class="d-block mt-4">Pelayanan Terpadu</span>
            </a>
          </div>
          <div class="col-12 col-md-3">
            <a href="{{ url('pelayanan/umum/detail') }}">
              <img src="{{ asset('img/rujukan/icon_rujukan_hati.png') }}" alt="icon" height="120">
              <span class="d-block mt-4">Transplantasi Hati</span>
            </a>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection