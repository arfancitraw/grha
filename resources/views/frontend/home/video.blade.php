<div id="news" class="content text-center">
	<div class="container">
		<div class="mb-5">
			<h2 class="mb-1">@lang('front.videpprofile')</h2>
			<small class="subtitle">"@lang('front.cepattepatdanakurat')"</small>
		</div>
		{!! $settings['profile-video']['isi'] !!}
	</div>
</div>