<div id="testimonies" class="content text-center">
	<div class="container">
		<h2 class="mb-5">Sertifikat &amp; Penghargaan</h2>
		<div class="testimonies-slider">
			@foreach($sertifikatFooter as $l)
			{{-- <a href="{{ url('pelayanan/'.$l->slug) }}" class="service-item text-left d-flex align-items-start my-3"> --}}
				<div class="d-flex justify-content-center align-items-center">
					<img src="{{ url('/storage/'.$l->gambar) }}" alt="{{ $l->nama }}" width="96" height="96" class="image-cover">
				</div>

				{{-- <div class="ml-3">
					<h4>{{ $l->judul }}</h4>
					<span>{{ $l->keterangan }}</span>
				</div> --}}
			{{-- </a> --}}
			@endforeach
		</div>
	</div>
</div>