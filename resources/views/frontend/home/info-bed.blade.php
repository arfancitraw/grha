@section('js')
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.js') }}" type="text/javascript"></script>
@append

@section('scripts')
<script>
	$(document).ready(function($) {
		var now = moment().format('DD/MMMM/YYYY HH:mm:ss');
		$('#info-bed .last-update').html(now);
		// axios.get('{{ url('infobed') }}')
		// 	 .then(function(response) {
		// 	  	console.log(response);
		// 	 })
		// 	 .catch(function(error) {
		// 	 	console.log(error);
		// 	 });
		@if(!is_null($infobed))
		$('.scroll-wrap').slimScroll({
			height: '300px',
		});
		@endif

		$("[data-toggle=collapse]").click(function() {
			$(this).find('i').toggle();
		});
	});
</script>
@append

<style>
.article-content{
   overflow: hidden;
   text-overflow: ellipsis;
   display: -webkit-box;
   line-height: 20px;     /* fallback */
   max-height: 60px;      /* fallback */
   -webkit-line-clamp: 3; /* number of lines to show */
   -webkit-box-orient: vertical;
}
table.table th{
	vertical-align: middle;
	/*border: none;*/
	background: #3b9ca5;
	color: #fff;
	font-weight: 400
}
table.info thead, table.info tbody, table.info tfoot { display: block; width: 100%; }
/*
table.info tbody {
    height: 300px;       
    overflow-y: auto;    
    overflow-x: hidden;  
}*/

table.info thead, table.info tfoot, table.info tbody tr{
    display:table;
    width:100%;
    table-layout:fixed;/* even columns width , fix width of table too*/
}
</style>
<div id="info-bed" class="filler text-center">
	<div class="container content">
		<div class="mb-5">
			<h2 class="text-white mb-1">Berita & Artikel Terbaru</h2>
			<small class="subtitle text-white">Berbagai berita, artikel dan kegiatan seputar PJNHK terbaru</small>
		</div>

		<div class="row d-flex justify-content-center">
			@if(count($news) > 0)
			@foreach($news as $new)
			<div class="col-md-4">
				<div class="card text-left">
					<div class="card-img-top" style="max-height: 150px; overflow: hidden;">
						<img class="card-img-top text-center" src="{{ url('storage/'.$new->photo) }}" alt="{{ session('lang') == 'english' ? (!is_null($new->judul_en) ? $new->judul_en : $new->judul) : $new->judul }}">
					</div>
					<div class="card-body">
						<h5 class="card-title">{{ session('lang') == 'english' ? (!is_null($new->judul_en) ? $new->judul_en : $new->judul) : $new->judul }}</h5>
						<p class="card-meta text-muted" style="font-size: .8em">
						    <i class="fa fa-user"></i> {{ $new->creator->name }} | 
						    <i class="fa fa-calendar"></i> {{ $new->tanggal ? $new->tanggal->format('d F Y') : $new->created_at->format('d F Y') }}
						</p>
						<p class="card-text article-content">{!! str_limit(strip_tags( (session('lang') == 'english' ? (!is_null($new->konten_en) ? $new->konten_en : $new->konten) : $new->konten) ), 130) !!}</p>
		                <a class="btn btn-block btn-sm btn-outline-info mt-4" href="{{ $new->tautan }}">Selengkapnya <i class="fa fa-chevron-right"></i></a>
					</div>
				</div>
			</div>
			@endforeach
			@endif
		</div>
		
		<div class="row dividing">
			<a class="btn btn-dividing" data-toggle="collapse" href="#bedTable" role="button" aria-expanded="false" aria-controls="bedTable">Tampilkan/Sembunyikan Informasi Tempat Tidur</a>
		</div>

		<div class="card no-border mt-5 collapse" id="bedTable">
			<div class="card-body px-0 pb-0">
				<div class="mb-3">
					<h2 class="mb-1">
						@lang('front.bedavailable')
					</h2>
					<small class="subtitle">@lang('front.last') : <strong class="last-update">{{ date('d/F/Y H:i:s') }}</strong></small>
				</div>

				<div class="table-responsive">
					<table class="table table-striped table-bordered table-sm mb-0 info">
						<thead>
							<tr>
								<th rowspan="2" style="width:50px">No</th>
								<th rowspan="2" style="width:250px">@lang('front.bedroom')</th>
								<th rowspan="2" style="width:130px">@lang('front.class')</th>
								<th rowspan="2" style="width:120px">@lang('front.sumabedroom')</th>
								<th colspan="3">@lang('front.useroom')</th>
								<th colspan="3">@lang('front.available')</th>
								{{-- <th rowspan="2">Waiting List</th> --}}
							</tr>
							<tr>
								<th>@lang('front.man')</th>
								<th>@lang('front.woman')</th>
								<th>@lang('front.sum')</th>
								<th>@lang('front.man')</th>
								<th>@lang('front.woman')</th>
								<th>@lang('front.sum')</th>
							</tr>
						</thead>
						<tbody class="data-table scroll-wrap">
							{{-- <div class="antiscroll-inner"></div> --}}
							@php
								$jumlah = 0;
								$isimale = 0;
								$isifemale = 0;
								$isi = 0;
								$tersedia = 0;
							@endphp
							@if(!is_null($infobed))
								@foreach($infobed as $bed)
								{{-- @if($bed->ds_dep != '-') --}}
									<tr>
										<td style="width:50px">{{ $bed->no }}</td>
										<td style="width:250px">{{ $bed->ds_dep }}</td>  
										<td style="width:130px">{{ $bed->ds_klas }}</td>
										<td align="center" style="width:120px">{{ $bed->jumlah }}</td>
										<td align="center">{{ $bed->isimale }}</td>
										<td align="center">{{ $bed->isifemale }}</td>
										<td align="center">{{ $bed->isi }}</td>
										<td align="center">-</td>
										<td align="center">-</td>
										<td align="center">
											@if($bed->tersedia == 0)
												<font color="red" style="font-weight: bolder">{{ $bed->tersedia }}</font>
											@else
												{{ $bed->tersedia }}
											@endif
										</td>	
										{{-- <td align="center">N/A</td>				 --}}
									</tr>
								{{-- @endif --}}
									@php 
										$jumlah += (int) $bed->jumlah;
										$isimale += (int) $bed->isimale;
										$isifemale += (int) $bed->isifemale;
										$isi += (int) $bed->isi;
										$tersedia += (int) $bed->tersedia;
									@endphp
								@endforeach
							@else
								<tr>
									<td colspan="10" class="text-center"><i>Gagal konek ke server, info tidak dapat diakses</i></td>
								</tr>
							@endif
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3" style="width: 430px"><strong>@lang('front.sumabedroom')</strong></td>
								<td align="center" style="width:120px"><strong>{{ $jumlah }}</strong></td>
								<td align="center"><strong>{{ $isimale }}</strong></td>
								<td align="center"><strong>{{ $isifemale }}</strong></td>
								<td align="center"><strong>{{ $isi }}</strong></td>
								<td align="center"><strong>0</strong></td>
								<td align="center"><strong>0</strong></td>
								<td align="center"><strong>{{ $tersedia }}</strong></td>
							</tr>
						</tfoot>
					</table>
					<div class="my-2">
						<i>Informasi ini berdasarkan data dari sistem komputer</i>
					</div>
				</div>

		{{-- <div class="card text-wite">
			<h5 class="card-header bg-info text-white"><i class="fa fa-search"></i> Info Ketersediaan Tempat Tidur</h5>
			<table class="table table-responsive table-sm table-striped table-bordered bg-white text-dark m-0">
				<thead>
					<tr>
						<th rowspan="2">No</th>
						<th rowspan="2">Ruang Perawatan</th>
						<th rowspan="2">Kelas Perawatan</th>
						<th rowspan="2">Jumlah Tempat Tidur</th>
						<th colspan="3">Terpakai</th>
						<th colspan="3">Kosong</th>
						<th rowspan="2">Waiting List</th>
						<th rowspan="2">Tanggal Update</th>
					</tr>
					<tr>
						<th>Laki-Laki</th>
						<th>Perempuan</th>
						<th>Jumlah</th>
						<th>Laki-Laki</th>
						<th>Perempuan</th>
						<th>Jumlah</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td rowspan="1">1</td>
						<td rowspan="1">Umum</td>  
						<td></td>
						<td align="right">0</td>
						<td align="right">0</td>
						<td align="right">0</td>
						<td align="right">0</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="font-weight:bold;color:red">0</td>	
						<td align="right">N/A</td>				
						<td align="right">17/May/2017 14:50:03</td>
					</tr>
					<tr>
						<td rowspan="5">2</td>
						<td rowspan="5">Anak</td>
						<td>VIP</td>
						<td align="right">2</td>
						<td align="right">0</td>
						<td align="right">0</td>
						<td align="right">0</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="background:">2</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr><td>Kelas 1</td>
						<td align="right">12</td>
						<td align="right">2</td>
						<td align="right">6</td>
						<td align="right">8</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="background:">4</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr><td>Kelas 2</td>
						<td align="right">12</td>
						<td align="right">4</td>
						<td align="right">3</td>
						<td align="right">7</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="background:">5</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr><td>Kelas 3</td>
						<td align="right">12</td>
						<td align="right">4</td>
						<td align="right">4</td>
						<td align="right">8</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="background:">4</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr><td>Intermediate</td>
						<td align="right">2</td>
						<td align="right">1</td>
						<td align="right">1</td>
						<td align="right">2</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="font-weight:bold;color:red">0</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr>
						<td rowspan="1">3</td>
						<td rowspan="1">Bedah</td>
						<td>Rawat Khusus</td>
						<td align="right">25</td>
						<td align="right">17</td>
						<td align="right">8</td>
						<td align="right">25</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="font-weight:bold;color:red">0</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr>
						<td rowspan="7">4</td>
						<td rowspan="7">Jantung</td>  
						<td></td>
						<td align="right">41</td>
						<td align="right">20</td>
						<td align="right">21</td>
						<td align="right">41</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="font-weight:bold;color:red">0</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr><td>super VIP</td>
						<td align="right">6</td>
						<td align="right">0</td>
						<td align="right">0</td>
						<td align="right">0</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="background:">6</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr><td>VIP</td>
						<td align="right">13</td>
						<td align="right">5</td>
						<td align="right">3</td>
						<td align="right">8</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="background:">5</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr><td>Kelas 1</td>
						<td align="right">53</td>
						<td align="right">12</td>
						<td align="right">4</td>
						<td align="right">16</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="background:">37</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr><td>Kelas 2</td>
						<td align="right">22</td>
						<td align="right">6</td>
						<td align="right">5</td>
						<td align="right">11</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="background:">11</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr><td>Kelas 3</td>
						<td align="right">30</td>
						<td align="right">8</td>
						<td align="right">5</td>
						<td align="right">13</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="background:">17</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr><td>Intermediate</td>
						<td align="right">45</td>
						<td align="right">22</td>
						<td align="right">12</td>
						<td align="right">34</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="background:">11</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr>
						<td rowspan="1">5</td>
						<td rowspan="1">ICU</td>  
						<td>Rawat Khusus</td>
						<td align="right">32</td>
						<td align="right">16</td>
						<td align="right">16</td>
						<td align="right">32</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="font-weight:bold;color:red">0</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr>
						<td rowspan="1">6</td>
						<td rowspan="1">CVCU/ICCU</td>  
						<td>Rawat Khusus</td>
						<td align="right">24</td>
						<td align="right">16</td>
						<td align="right">5</td>
						<td align="right">21</td>
						<td align="right">-</td>
						<td align="right">-</td>
						<td align="right" style="background:">3</td>	
						<td align="right">N/A</td>				
					</tr>
					<tr>
						<td colspan="3"><strong>Jumlah Tempat Tidur</strong></td>
						<td align="right"><strong>331</strong></td>
						<td align="right"><strong>133</strong></td>
						<td align="right"><strong>93</strong></td>
						<td align="right"><strong>226</strong></td>
						<td align="right"><strong>0</strong></td>
						<td align="right"><strong>0</strong></td>
						<td align="right"><strong>105</strong></td>
						<td align="center"><strong>0</strong></td>
					</tr>
				</tbody>
			</table>
		</div> --}}
	</div>
</div>
</div>
</div>
