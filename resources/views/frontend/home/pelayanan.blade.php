<div id="services" class="content text-center">
	<div class="container">
		<div class="mb-5">
			<h2>@lang('front.service')</h2>
			<small class="subtitle">@lang('front.esatharkit')</small>
		</div>
		<div class="row d-flex justify-content-center my-4">
			@if(isset($homeLayanan) && count($homeLayanan) > 0)
				@foreach($homeLayanan as $l)
				<div class="col-12 col-lg-4 service-item text-left">
					<a href="{{ url('rujukan-nasional/detail/'.str_slug($l->id)) }}">
						@if($l->icon)
						<img src="{{ url('/storage/'.$l->icon) }}" width="100px" alt="{{ session('lang') == 'english' ? (!is_null($l->judul_en) ? $l->judul_en : $l->judul) : $l->judul }}" />
						@endif
						<div class="detail">
							<h4 class="title">{{ session('lang') == 'english' ? (!is_null($l->judul_en) ? $l->judul_en : $l->judul) : $l->judul }}</h4>
							<span class="description">{{ str_limit(session('lang') == 'english' ? (!is_null($l->deskripsi_en) ? $l->deskripsi_en : $l->deskripsi) : $l->deskripsi, 60) }}</span>
						</div>
					</a>
				</div>
				@endforeach
			@else
			{{-- <h4>--Tidak ada layanan yang dapat ditampilkan--</h4> --}}
			@endif
		</div>
		<div class="row dividing">
			<a href="{{ url('rujukan-nasional') }}" class="btn btn-dividing">@lang('front.ess')</a>
		</div>
	</div>
</div>