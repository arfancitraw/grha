<div id="news" class="content odd-section text-center">
	<div class="container">
		<div class="mb-5">
			<h2 class="mb-1">@lang('front.pusatmedia')</h2>
			<small class="subtitle">@lang('front.mediainformasiterkait')</small>
		</div>
		<div class="row">
			<div class="col-12 col-lg-3">
				<div class="card bg-info mb-4">
					<h5 class="card-header text-white"><i class="fa fa-bullhorn"></i> @lang('front.news')</h5>
					<div class="card-body p-0">
						@if(isset($pengumuman) && count($pengumuman) > 0)
						<table class="table bg-white table-sm mb-0">
							<tbody>
								@foreach($pengumuman as $d)
								<tr>
									<td style="width: 50px">
										@php $date = $d->tanggal ?: $d->created_at; @endphp
										<time datetime="2014-09-20" class="icon">
										  <strong>{{ $date->format('M') }}</strong>
										  <span>{{ $date->format('d') }}</span>
										</time>
									</td>{{-- {{ ($d->tanggal) ? $d->tanggal->format('d/m/Y') : $d->created_at->format('d/m/Y') }}</td> --}}
									<td class="text-left"><a href="{{ url('berita/'.$d->slug) }}" title="{{ (session('lang') == 'english' ? (!is_null($d->judul_en) ? $d->judul_en : $d->judul) : $d->judul) }}">
										{{ str_limit( (session('lang') == 'english' ? (!is_null($d->judul_en) ? $d->judul_en : $d->judul) : $d->judul) , 40) }}</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@else
						<h6 class="text-white my-3">@lang('front.tidakadakegiatan')</h6>
						@endif
					</div>
					@if(isset($pengumuman) && count($pengumuman) > 0)
					<div class="card-footer p-1 bg-white">
						<a href="{{ url('berita') }}">@lang('front.seeall') @lang('front.news')</a>
					</div>
					@endif
				</div>
			</div>
			<div class="col-12 col-lg-3">
				<div class="card bg-info mb-4">
					<h5 class="card-header text-white"><i class="fa fa-calendar"></i> @lang('front.kegiatan')</h5>
					<div class="card-body p-0">
						@if(isset($kegiatan) && count($kegiatan) > 0)
						<table class="table bg-white table-sm mb-0">
							<tbody>
								@foreach($kegiatan as $d)
								<tr>
									<td style="width: 50px">
										@php $date = $d->tanggal ?: $d->created_at; @endphp
										<time datetime="2014-09-20" class="icon">
										  <strong>{{ $date->format('M') }}</strong>
										  <span>{{ $date->format('d') }}</span>
										</time>
									</td>
									<td class="text-left"><a href="{{ url('kegiatan/'.$d->slug) }}" title="{{ (session('lang') == 'english' ? (!is_null($d->judul_en) ? $d->judul_en : $d->judul) : $d->judul) }}">
										{{ str_limit( (session('lang') == 'english' ? (!is_null($d->judul_en) ? $d->judul_en : $d->judul) : $d->judul) , 40) }}</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@else
						<h6 class="text-white my-3">@lang('front.tidakadakegiatan')</h6>
						@endif
					</div>
					@if(isset($kegiatan) && count($kegiatan) > 0)
					<div class="card-footer p-1 bg-white">
						<a href="{{ url('kegiatan') }}">@lang('front.seeall') @lang('front.kegiatan')</a>
					</div>
					@endif
				</div>
			</div>
			<div class="col-12 col-lg-3">
				<div class="card bg-info mb-4">
					<h5 class="card-header text-white"><i class="fa fa-newspaper"></i> @lang('front.artikel')</h5>
					<div class="card-body p-0">
						@if(isset($artikel) && count($artikel) > 0)
						<table class="table bg-white table-sm mb-0">
							<tbody>
								@foreach($artikel as $d)
								<tr>
									<td style="width: 50px">
										@php $date = $d->tanggal ?: $d->created_at; @endphp
										<time datetime="2014-09-20" class="icon">
										  <strong>{{ $date->format('M') }}</strong>
										  <span>{{ $date->format('d') }}</span>
										</time>
									</td>
									<td class="text-left"><a href="{{ url('artikel/'.$d->slug) }}" title="{{ (session('lang') == 'english' ? (!is_null($d->judul_en) ? $d->judul_en : $d->judul) : $d->judul) }}">{{ str_limit( (session('lang') == 'english' ? (!is_null($d->judul_en) ? $d->judul_en : $d->judul) : $d->judul) , 40) }}</a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@else
						<h6 class="text-white my-3">@lang('front.tidakadakegiatan')</h6>
						@endif
					</div>
					@if(isset($artikel) && count($artikel) > 0)
					<div class="card-footer p-1 bg-white">
						<a href="{{ url('artikel') }}">@lang('front.seeall') @lang('front.artikel')</a>
					</div>
					@endif
				</div>
			</div>
			<div class="col-12 col-lg-3">
				<div class="card bg-info mb-4">
					<h5 class="card-header text-white"><i class="fa fa-plus-square"></i> Pustaka Kesehatan</h5>
					<div class="card-body bg-white pt-1" style="border-radius:0 0 calc(.25rem - 1px) calc(.25rem - 1px)">
						<small style="font-weight: 300">@lang('front.seputarpenyakit')</small>
						<nav aria-label="Page navigation example">
							<ul class="alphabets">
								<li><a href="{{ url('pustaka/A') }}">A</a></li>
								<li><a href="{{ url('pustaka/B') }}">B</a></li>
								<li><a href="{{ url('pustaka/C') }}">C</a></li>
								<li><a href="{{ url('pustaka/D') }}">D</a></li>
								<li><a href="{{ url('pustaka/E') }}">E</a></li>
								<li><a href="{{ url('pustaka/F') }}">F</a></li>
								<li><a href="{{ url('pustaka/G') }}">G</a></li>
								<li><a href="{{ url('pustaka/H') }}">H</a></li>
								<li><a href="{{ url('pustaka/I') }}">I</a></li>
								<li><a href="{{ url('pustaka/J') }}">J</a></li>
								<li><a href="{{ url('pustaka/K') }}">K</a></li>
								<li><a href="{{ url('pustaka/L') }}">L</a></li>
								<li><a href="{{ url('pustaka/M') }}">M</a></li>
								<li><a href="{{ url('pustaka/N') }}">N</a></li>
								<li><a href="{{ url('pustaka/O') }}">O</a></li>
								<li><a href="{{ url('pustaka/P') }}">P</a></li>
								<li><a href="{{ url('pustaka/Q') }}">Q</a></li>
								<li><a href="{{ url('pustaka/R') }}">R</a></li>
								<li><a href="{{ url('pustaka/S') }}">S</a></li>
								<li><a href="{{ url('pustaka/T') }}">T</a></li>
								<li><a href="{{ url('pustaka/U') }}">U</a></li>
								<li><a href="{{ url('pustaka/V') }}">V</a></li>
								<li><a href="{{ url('pustaka/W') }}">W</a></li>
								<li><a href="{{ url('pustaka/X') }}">X</a></li>
								<li><a href="{{ url('pustaka/Y') }}">Y</a></li>
								<li><a href="{{ url('pustaka/Z') }}">Z</a></li>
								<li><a href="{{ url('pustaka/0') }}">#</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>