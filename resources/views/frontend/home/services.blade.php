<div id="services" class="content text-center">
	<div class="container">
		{{-- <div class="row dividing mb-5"> --}}
			<h2 class="mb-5">Layanan Unggulan</h2>
		{{-- </div> --}}
		<div class="my-4">
			@if(isset($homeLayanan) && count($homeLayanan) > 0)
			<div class="service-slider">
				@foreach($homeLayanan as $l)
				<a href="{{ url('pelayanan/'.$l->slug) }}" class="service-item text-left d-flex align-items-start my-3">
					<img src="{{ url('/storage/'.$l->photo) }}" alt="{{ $l->judul }}" width="96" height="100%" class="image-cover">
					<div class="ml-3">
						<h4>{{ $l->judul }}</h4>
						<span>{{ str_limit($l->keterangan, 100) }}</span>
					</div>
				</a>
				@endforeach
			</div>
			@else
			<h4>--Tidak ada layanan yang dapat ditampilkan--</h4>
			@endif
		</div>
	</div>
</div>