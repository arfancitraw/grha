<div id="location" class="mt-5 text-center">
	<div class="container mb-1">
		<h2>@lang('front.accessibility')</h2>
		<small class="subtitle">@lang('front.untukdapatmengunjung')</small>
	</div>
	<div class="mv-gd-widget-20" data-width="100%" data-height="640"></div>
</div> 