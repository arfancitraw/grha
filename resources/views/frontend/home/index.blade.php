@extends('layouts.frontend-scaffold')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick.css') }} ">
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick-theme.css') }} ">
@endsection

@section('js')
	<script src="{{ asset('plugins/slick/slick.min.js') }}"></script>
@endsection

@section('styles')
	<style>
	.slick-dots {
		margin-bottom: -40px
	}
	.cari-dokter-container {
		position: absolute;
		top: 50%;
		right: 0;
		transform: translateY(-50%);
	}
	.home-cari-dokter {
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}
	.service-item {
		color: #333;
		text-decoration: none!important
	}
	.service-item:hover {
		color: #444
	}
	#location {
		min-height: 80vh
	}
	#awards .image-cover {
		/*width: 96px!important*/
	}
	</style>
@endsection

@section('scripts')
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "https://widgets.moovit.com/ws/6C4BAE9FA3022D13E0530100007F43F3/1166810";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'moovit-jsw'));

		$(document).on('scroll', '.map-container', function(e){
			e.preventDefault();
			alert('dor');
		});

		$(document).ready(function(){
			var slider = $('#home-slider'),
			    h = slider.width() / 4;
			// if(h < 240){ h = 240 }
			slider.height(h);

			$('#home-slider #slider').slick({
				dots: false,
				autoplay: true,
				autoplaySpeed: 5000,
				infinite: true,
				speed: 500,
				fade: true,
				pauseOnHover: false,
				pauseOnFocus: false,
				cssEase: 'linear'
			});

			$('.service-slider').slick({
				dots: true,
				infinite: true,
				speed: 500,
				pauseOnHover: false,
				pauseOnFocus: false,
				slidesToShow: 2,
				slidesToScroll: 2,
				rows: 2,
				responsive: [
				{
					breakpoint: 920,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						rows: 1
					}
				}
				]
			});

			$('.awards-slider').slick({
				dots: true,
				infinite: true,
				speed: 500,
				pauseOnHover: false,
				pauseOnFocus: false,
				slidesToShow: 4,
				slidesToScroll: 1,
				responsive: [
				{
					breakpoint: 920,
					settings: {
						slidesToShow: 2
					}
				},{
					breakpoint: 480,
					settings: {
						slidesToShow: 1
					}
				}
				]
			});
		});
	</script>
@endsection

@section('content')
<div id="home-slider">
	<div id="slider">
		@if(count($slider) > 0)
			@foreach($slider as $slide)
				<a href="{{ $slide->url or '#' }}" class="slider-wrap" style="background-image:url('{{ url('storage/'.$slide->photo) }}')">
				</a>
			@endforeach
		@else
			<div class="slider-wrap" style="background-image:url('{{ asset('img/backgrounds/bg-login.jpg') }}')">
				{{-- <div class="slider-content d-none d-xl-block container">
					<h1>Medical Service <b>that you can trust.</b></h1>
					<span>nobis dicta quis blanditiis tempora eum dolor, voluptatem fugit dignissimos accusantium!</span>
				</div> --}}
			</div>
		@endif
	</div>
</div>

@include('frontend.home.pelayanan')
@include('frontend.home.info-bed')
@include('frontend.home.video')
@include('frontend.home.articles')
{{-- @include('frontend.home.testimonies') --}}
@include('frontend.home.location')

{{-- <div id="filler" class="filler text-center"> --}}
	{{-- <div class="content">
		<h2>Corporis tempora nihil!</h2>
		<span>Iste facere corrupti ipsa. Recusandae quis rerum at beatae, itaque aliquam aspernatur debitis tempore, quasi repudiandae laudantium quae! Similique magni, minus ea.</span>
	</div> --}}
{{-- </div> --}}
@endsection