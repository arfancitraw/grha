<h2 class="text-center my-3">Formulir Pelaporan Gratifikasi</h2>

<form class="form pengaduan" id="dataForm" action="{{ url($pageUrl.'gratifikasi') }}" method="POST" enctype="multipart/form-data">
  {!! csrf_field() !!}
  <input type="hidden" name="tipe" value="0"> 
  <fieldset class="">
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">1. Nama Lengkap <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap pelapor" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">2. NIP/Nopeg <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="nip_nopeg" class="form-control" placeholder="NIP/Nopeg" maxlength="15" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">3. Jabatan/Unit Kerja <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="jabatan_unit_kerja" class="form-control" placeholder="Jabatan/Unit Kerja" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">4. Jenis/Bentuk Penerimaan <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="jenis_bentuk_penerimaan" class="form-control" placeholder="Jenis/Bentuk Penerimaan" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">5. Harga/Nilai Nominal <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="number" name="harga_nilai_nominal" class="form-control" placeholder="Harga/Nilai Nominal" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">6. Kaitan Peristiwa Penerimaan <span class="text-danger">*</span></label>
      <div class="col-8">
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="kaitan_peristiwa_penerimaan" id="pernikahan" value="Pernikahan" checked required>
          <label class="form-check-label" for="pernikahan">Pernikahan</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="kaitan_peristiwa_penerimaan" id="pisah_sambut" value="Pisah Sambut" required>
          <label class="form-check-label" for="pisah_sambut">Pisah Sambut</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="kaitan_peristiwa_penerimaan" id="promosi" value="Promosi" required>
          <label class="form-check-label" for="promosi">Promosi</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="kaitan_peristiwa_penerimaan" id="lainnya" value="Lainnya" required>
          <label class="form-check-label" for="lainnya">Lainnya</label>
        </div>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">7. Tempat & Tanggal Penerimaan <span class="text-danger">*</span></label>
      <div class="col-8">
          <div class="row">
              <div class="col-md-6">
                  <input type="text" name="tempat_penerimaan" class="form-control" placeholder="Tempat Penerimaan" required>
              </div>
              <div class="col-md-6">
                  <input type="text" name="tanggal_penerimaan" class="form-control datepicker2" placeholder="Tanggal Penerimaan" required>
              </div>
          </div>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">8. Nama Pemberi <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="nama_pemberi" class="form-control" placeholder="Nama Pemberi" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">9. Pekerjaan dan Jabatan <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="pekerjaan_jabatan" class="form-control" placeholder="Pekerjaan dan Jabatan" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">10. Alamat/Telepon/Email <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="alamat_telepon_email" class="form-control" placeholder="Alamat/Telepon/Email" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">11. Hubungan dengan Penerima <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="hubungan_penerima" class="form-control" placeholder="Hubungan dengan Penerima" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">12. Alasan Pemberian <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="alasan_pemberian" class="form-control" placeholder="Alasan Pemberian" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">13. Kronologi Pemberian <span class="text-danger">*</span></label>
      <div class="col-8">
        <textarea name="kronologi" class="form-control w-100" rows="3" required></textarea>
      </div>
    </div>

    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">14. Bukti Pendukung <span class="text-danger">*</span></label>
      <div class="col-8">
        <div class="input-group mb-3">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="input-bukti" name="bukti[]" accept="application/pdf,image/*" multiple="">
            <label class="custom-file-label" for="input-bukti">Pilih File Bukti</label>
          </div>
        </div>
      </div>
    </div>


    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">Catatan Tambahan</label>
      <div class="col-8">
        <textarea name="catatan" class="form-control w-100" rows="2"></textarea>
      </div>
    </div>

    <div class="form-group my-2">
      <span class="text-danger">* = wajib diisi</span>
    </div>

    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start"></label>
      <div class="col-8">
        <div class="row">
          <div class="col-md-1">
            <input type="checkbox" name="setuju" value="1">
          </div>
          <div class="col-md-11">
            Laporan Gratifikasi ini saya sampaikan dengan sebenar-benarnya. Apabila ada yang sengaja tidak saya laporkan atau saya laporkan ke UPG/KPK secara tidak benar, maka saya  bersedia mempertanggungjawabkannya secara hukum sesuai dengan peraturan perundang-undangan yang berlaku dan saya  bersedia memberikan keterangan selanjutnya.
          </div>
        </div>
      </div>
    </div>
  </fieldset>

  <div class="d-flex justify-content-between mt-4">
    <button type="button" class="btn btn-lg btn-secondary btn-previous"><i class="fa fa-arrow-left"></i> Kembali</button>
    <div class="btn-group">
      <button type="button" class="btn btn-lg btn-info"><i class="fa fa-sync-alt"></i> Reset</button>
      <button type="button" class="btn btn-lg btn-primary btn-submit"><i class="fa fa-paper-plane"></i> Kirim laporan</button>
    </div>
  </div>
</form>