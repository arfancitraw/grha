<h2 class="text-center mb-5">
  Pelaporan Etik dan Hukum
</h2>

{!! $settings['pengaduan-narasi-etik']->isi !!}

<div class="row my-5 justify-content-center">
  <div class="col-12 col-md-3">
    <a href="javascript:void(0)" class="btn btn-primary btn-block btn-adu btn-next my-4">
      <i class="fa fa-edit"></i> &nbsp;Isi Formulir
    </a>
  </div>
</div>
