<h2 class="text-center my-3">Formulir Whistleblowing</h2>

<form class="form pengaduan" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
  {!! csrf_field() !!}
  <input type="hidden" name="tipe" value="1"> 
  <fieldset class="">
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">1. Nama Lengkap <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap pelapor" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">2. Telepon <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="telepon" class="form-control" placeholder="Nomor telepon" maxlength="15" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">3. Email <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="email" name="email" class="form-control" placeholder="Email" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">4. Perihal <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="perihal" class="form-control" placeholder="Perihal laporan" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">5. Uraian <span class="text-danger">*</span></label>
      <div class="col-8">
        <textarea name="uraian" class="form-control w-100" rows="3" required></textarea>
      </div>
    </div>

    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">6. Bukti Pendukung <span class="text-danger">*</span></label>
      <div class="col-8">
        <div class="input-group mb-3">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="input-bukti" name="bukti" accept="application/pdf,image/*">
            <label class="custom-file-label" for="input-bukti">Pilih File Bukti</label>
          </div>
        </div>
      </div>
    </div>


    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">7. Keterangan</label>
      <div class="col-8">
        <textarea name="keterangan" class="form-control w-100" rows="2"></textarea>
      </div>
    </div>

    <div class="form-group my-2">
      <span class="text-danger">* = wajib diisi</span>
    </div>
  </fieldset>

  <div class="d-flex justify-content-between mt-4">
    <button type="button" class="btn btn-lg btn-secondary btn-previous"><i class="fa fa-arrow-left"></i> Kembali</button>
    <div class="btn-group">
      <button type="button" class="btn btn-lg btn-info"><i class="fa fa-sync-alt"></i> Reset</button>
      <button type="button" class="btn btn-lg btn-primary btn-submit"><i class="fa fa-paper-plane"></i> Kirim laporan</button>
    </div>
  </div>
</form>