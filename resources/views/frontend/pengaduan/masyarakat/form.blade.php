<h2 class="text-center my-3">Formulir Pengaduan Masyarakat</h2>

<form class="form pengaduan" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
  {!! csrf_field() !!}
  <input type="hidden" name="tipe" value="2"> 
  <fieldset class="">
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">1. Nama Lengkap <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap pelapor" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">2. Status <span class="text-danger">*</span></label>
      <div class="col-8">
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="status" id="pelanggan" value="0" checked required>
          <label class="form-check-label" for="pelanggan">Pelanggan RSPJPDHK</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="status" id="masyarakat" value="1" required>
          <label class="form-check-label" for="masyarakat">Masyarakat</label>
        </div>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">3. Telepon <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="telepon" class="form-control" placeholder="Nomor telepon" maxlength="15" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">4. Email <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="email" name="email" class="form-control" placeholder="Email" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">5. Perihal <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="perihal" class="form-control" placeholder="Perihal laporan" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">6. Uraian <span class="text-danger">*</span></label>
      <div class="col-8">
        <textarea name="uraian" class="form-control w-100" rows="3" required></textarea>
      </div>
    </div>

    {{-- <table class="table table-bordered bukti">
      <thead>
        <tr>
          <th>Jenis / Nama Bukti</th>
          <th>Jumlah</th>
          <th>Berkas</th>
          <th><a href="javascript:void(0)" class="btn btn-sm btn-success add-bukti"><i class="fa fa-plus"></i></a></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width: 30%"><input type="text" name="bukti[0][nama]" class="form-control" placeholder="Nama berkas"></td>
          <td style="width: 10%"><input type="number" name="bukti[0][jumlah]" class="form-control" placeholder="0"></td>
          <td><input type="file" name="bukti[0][file]" class="form-control"></td>
          <td class="text-center"><a href="javascript:void(0)" class="btn btn-sm btn-danger remove-bukti"><i class="fa fa-minus"></i></a></td>
        </tr>
      </tbody>
    </table> --}}

    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">7. Bukti Pendukung <span class="text-danger">*</span></label>
      <div class="col-8">
        <div class="input-group mb-3">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="input-bukti" name="bukti" accept="application/pdf,image/*">
            <label class="custom-file-label" for="input-bukti">Pilih File Bukti</label>
          </div>
        </div>
      </div>
    </div>


    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">8. Keterangan</label>
      <div class="col-8">
        <textarea name="keterangan" class="form-control w-100" rows="2"></textarea>
      </div>
    </div>

    <div class="form-group my-2">
      <span class="text-danger">* = wajib diisi</span>
    </div>
  </fieldset>

  <div class="d-flex justify-content-between mt-4">
    <button type="button" class="btn btn-lg btn-secondary btn-previous"><i class="fa fa-arrow-left"></i> Kembali</button>
    <div class="btn-group">
      <button type="button" class="btn btn-lg btn-info"><i class="fa fa-sync-alt"></i> Reset</button>
      <button type="button" class="btn btn-lg btn-primary btn-submit"><i class="fa fa-paper-plane"></i> Kirim laporan</button>
    </div>
  </div>
</form>