<h2 class="text-center my-3">Whistleblowing dan Pengaduan Masyarakat</h2>

<h3>Kriteria Pelaporan</h3>
<ol>
  <li>Ada penyimpangan kasus yang dilaporkan</li>
  <li>Menjelaskan Dimana, Kapan kasus tersebut dilakukan</li>
  <li>Siapa pejabat/pegawai RS Pusat Jantung dan Pembuluh Darah Harapan Kita yang melakukan atau terlibat</li>
  <li>Bagaimana cara perbuatan tersebut dilakukan</li>
  <li>Dilengkapi dengan bukti permulaan (data, dokumen, gambar dan rekaman) yang mendukung/menjelaskan adanya dugaan Tindak Pidana Korupsi.</li>
</ol>

<div class="list text-center">
  <div class="row my-5">
    <div class="col-12 col-md-4">
      <h5 class="my-2">Etika &amp; Hukum</h5>
      <img class="my-2" src="{{asset('/img/whistleblowing/icon_hukum.png')}}" alt="icon" height="120">
      <span class="d-block mt-4">
        Segala bentuk pengaduan terkait<br>
        etika dan hukum, segera email ke<br>
        <a href="#" class="text-link">etikhukum.wbs@pjnhk.go.id</a>
      </span>
    </div>
    <div class="col-12 col-md-4">
      <h5 class="my-2">Pelayanan Pelanggan</h5>
      <img class="my-2" src="{{asset('/img/whistleblowing/icon_pelayanan.png')}}" alt="icon" height="120">
      <span class="d-block mt-4">
        Segala bentuk pengaduan terkait<br>
        pelayanan pelanggan, segera email ke<br>
        <a href="#" class="text-link">Yangan.wbs@pjnhk.go.id</a>
      </span>
    </div>
    <div class="col-12 col-md-4">
      <h5 class="my-2">Satuan Pemeriksaan Internal (SPI)</h5>
      <img class="my-2" src="{{asset('/img/whistleblowing/icon_spi.png')}}" alt="icon" height="120">
      <span class="d-block mt-4">
        Segala bentuk pengaduan terkait<br>
        satuan pemeriksaan internal, segera email ke<br>
        <a href="#" class="text-link">SPI.wbs@pjnhk.go.id</a>
      </span>
    </div>
  </div>
</div>

<div class="row justify-content-center">
  <div class="col-12 col-md-6">
    <a href="javascript:void(0)" class="btn btn-lg btn-primary btn-block btn-adu btn-next my-4">Buat Pengaduan</a>
  </div>
</div>
