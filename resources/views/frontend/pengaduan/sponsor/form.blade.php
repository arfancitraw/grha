<h2 class="text-center my-3">Formulir Pelaporan Sponsor</h2>

<form class="form pengaduan" id="dataForm" action="{{ url($pageUrl.'sponsor') }}" method="POST" enctype="multipart/form-data">
  {!! csrf_field() !!}
  <input type="hidden" name="tipe" value="0"> 
  <fieldset class="">
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">1. Nama Lengkap <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap pelapor" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">2. NIP/Nopeg <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="nip_nopeg" class="form-control" placeholder="NIP/Nopeg" maxlength="15" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">3. Jabatan <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="jabatan" class="form-control" placeholder="Jabatan" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">4. Nama Kegiatan <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="nama_kegiatan" class="form-control" placeholder="Nama Kegiatan" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">5. Tempat Kegiatan <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="tempat_kegiatan" class="form-control" placeholder="Tempat Kegiatan" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">6. Tanggal <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="tanggal" class="form-control datepicker2" placeholder="Tanggal" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">7. Peranan <span class="text-danger">*</span></label>
      <div class="col-8">
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="peranan" id="peserta" value="Peserta" checked required>
          <label class="form-check-label" for="peserta">Peserta</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="peranan" id="pembicara" value="Pembicara" required>
          <label class="form-check-label" for="pembicara">Pembicara</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="peranan" id="moderator" value="Moderator" required>
          <label class="form-check-label" for="moderator">Moderator</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="peranan" id="lainnya" value="Lainnya" required>
          <label class="form-check-label" for="lainnya">Lainnya</label>
        </div>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">8. Penyelenggara Kegiatan</label>
      <div class="col-8">
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start" style="padding-left: 40px;">a. Nama Perusahaan <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="nama_perusahaan" class="form-control" placeholder="Nama Perusahaan" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start" style="padding-left: 40px;">b. Alamat <span class="text-danger">*</span></label>
      <div class="col-8">
        <textarea name="alamat" class="form-control w-100" rows="3" required></textarea>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">9. Pemberi Sponsor <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="text" name="pemberi_sponsor" class="form-control" placeholder="Pemberi Sponsor" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">10. Registrasi <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="number" name="registrasi" class="form-control sum" placeholder="Registrasi" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">11. Akomodasi <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="number" name="akomodasi" class="form-control sum" placeholder="Akomodasi" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">12. Transportasi <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="number" name="transportasi" class="form-control sum" placeholder="Transportasi" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">13. Honor <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="number" name="honor" class="form-control sum" placeholder="Honor" required>
      </div>
    </div>
    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">14. Total <span class="text-danger">*</span></label>
      <div class="col-8">
        <input type="number" name="total" class="form-control total" placeholder="Total" required readonly>
      </div>
    </div>

    <div class="form-group row mb-2 align-items-start">
      <label class="col-4 justify-content-start">15. Bukti Pendukung <span class="text-danger">*</span></label>
      <div class="col-8">
        <div class="input-group mb-3">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="input-bukti" name="bukti[]" accept="application/pdf,image/*" multiple="">
            <label class="custom-file-label" for="input-bukti">Pilih File Bukti</label>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group my-2">
      <span class="text-danger">* = wajib diisi</span>
    </div>
  </fieldset>

  <div class="d-flex justify-content-between mt-4">
    <button type="button" class="btn btn-lg btn-secondary btn-previous"><i class="fa fa-arrow-left"></i> Kembali</button>
    <div class="btn-group">
      <button type="button" class="btn btn-lg btn-info"><i class="fa fa-sync-alt"></i> Reset</button>
      <button type="button" class="btn btn-lg btn-primary btn-submit"><i class="fa fa-paper-plane"></i> Kirim laporan</button>
    </div>
  </div>
</form>