@extends('layouts.frontend-scaffold')

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/gijgo-timepicker/css/gijgo.min.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datepicker/datepicker.min.css') }} ">
@endsection
@section('js')
  <script src="{{ asset('plugins/gijgo-timepicker/js/gijgo.min.js') }} "></script>
  <script src="{{ asset('plugins/datepicker/datepicker.min.js') }} "></script>
@endsection

@section('styles')
  <style>
  .btn-adu {
    font-size: 1rem
  }

  .fade {
    transition: all .75s ease-in-out;
  }

  table th {
    text-align: center
  }
  .pengaduan {
    padding: 1rem;
    max-width: 800px;
    margin: 0 auto;
  }
  .pengaduan h3 { 
    margin-top: 0;
    margin-bottom: 5px;
    text-transform: uppercase;
  }

  .pengaduan-steps { 
    overflow: hidden;
    position: relative;
    margin: 0 0 1rem;
    display: flex;
    justify-content: center
  }

  .pengaduan-progress { 
    position: absolute;
    top: 24px;
    left: 0;
    width: 100%;
    height: 1px;
    background: #ddd;
  }
  .pengaduan-progress-line { 
    position: absolute;
    top: 0;
    left: 0;
    height: 1px;
    background: #17646b;
  }

  .pengaduan-step { 
    position: relative;
    width: 33.333333%;
    padding: 0 5px;
    text-align: center;
  }

  .pengaduan-step-icon {
    display: inline-block;
    width: 40px;
    height: 40px;
    margin-top: 4px;
    background: #ddd;
    font-size: 16px;
    color: #fff;
    line-height: 40px;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
  }
  .pengaduan-step.activated .pengaduan-step-icon {
    background: #fff;
    border: 1px solid #17646b;
    color: #17646b;
    line-height: 38px;
  }
  .pengaduan-step.active .pengaduan-step-icon {
    width: 48px;
    height: 48px;
    margin-top: 0;
    background: #17646b;
    font-size: 22px;
    line-height: 48px;
  }

  .pengaduan-buttons .btn {
    background: #17646b;
    color: #fff;
  }
  .pengaduan-step p { 
    color: #ccc;
  }
  .pengaduan-step.activated p { 
    color: #17646b;
  }
  .pengaduan-step.active p { 
    color: #17646b;
  }

  .pengaduan fieldset { 
    display: none;
    text-align: left;
  }

  .pengaduan .input-error { 
    border-color: #f35b3f;
  }
  </style>
@endsection

@section('scripts')
  <script>
    $('.btn-next').click(function(){
        $('#info').css('opacity', '0');
        $('html, body').animate({
            scrollTop: $('.content-main').offset().top - 140
        }, 750);
        $('#form-tab').trigger('click');
        setTimeout(function(){ 
            $('#form').css('opacity', '1');
        }, 1000);
    });
    $('.btn-previous').click(function(){
        $('#form').css('opacity', '0');
        $('html, body').animate({
            scrollTop: $('.content-main').offset().top - 140
        }, 750);
        $('#info-tab').trigger('click');
        setTimeout(function(){ 
            $('#info').css('opacity', '1');
        }, 1000);
    });

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        startDate: '-3d'
    });
    $('.datepicker2').datepicker({
        format: 'yyyy-mm-dd',
    });
    $('[name=pukul_awal]').timepicker({
        uiLibrary: 'bootstrap4',
    });
    $('[name=pukul_akhir]').timepicker({
        uiLibrary: 'bootstrap4',
    });
    $('.timepicker').focus(function(event) {
        $(this).blur();
        $(this).closest('.gj-timepicker').find('.btn').trigger('click');
    });
    // $('.datepicker').datepicker();
    $('.pengaduan fieldset:first').fadeIn('fast');
    $('.pengaduan input[type="text"], .pengaduan input[type="password"], .pengaduan textarea').on('focus', function() {
        $(this).removeClass('input-error');
    });

    function calcTotal(){
      var val = 0;
      $('.sum').each(function (){
        var value = $(this).val();
        if($(this).val() == '')
        {
          value = 0;
        }
        val += parseInt(value);
      });
      $('.total').val(val);
    }

    $(document).on('change', '.sum', function() {
      calcTotal();
    });

    $(document).on('keyup', '.sum', function() {
      calcTotal(); 
    });

    $(document).on('click', '.pengaduan .btn-submit', function(e) {
        swal({
            title: 'Perhatian!',
            text: "Data yang anda isikan sudah sesuai?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Kirim',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result) {
                if($('#dataForm')[0].checkValidity()){
                    myApp.showPleaseWait();

                    $("#dataForm").ajaxSubmit({
                        done: function(){
                            myApp.hidePleaseWait();
                        },
                        success: function(resp){
                            swal(
                                'Terkirim',
                                'Terima kasih telah mengirimkan laporan pengaduan. Kami akan menghubungi Anda sebagai konfirmasi lanjutan.',
                                'success'
                            ).then((result) => {
                                location.reload();
                                return true;
                            })
                        },
                        error: function(resp){
                            var error = $('<ul class="list"></ul>');
                            console.log(resp.responseJSON);
                            $.each(resp.responseJSON, function(index, val) {
                                error.append('<li>'+val+'</li>');
                            });
                            $('#formModal').find('.ui.error.message').html(error).show();
                        }
                    });
                }else{
                    swal(
                        'Gagal',
                        'Mohon periksa kembali, form isian anda belum sesuai.',
                        'error'
                    ).then((result) => {
                        return true;
                    })
                }
            }
        });
    });
  </script>
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-whistleblowing']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col-12 content-main">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item"><a href="#">Pengaduan</a></li>
          <li class="breadcrumb-item active" aria-current="page">Sponsor</li>
        </ol>
      </nav>

      <ul class="nav nav-tabs d-none" id="pengaduan" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">info</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="form-tab" data-toggle="tab" href="#form" role="tab" aria-controls="form" aria-selected="false">form</a>
        </li>
      </ul>
      <div class="tab-content" id="pengaduanContent">
        <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">
          @include('frontend.pengaduan.sponsor.redaksi')
        </div>
        <div class="tab-pane fade" id="form" role="tabpanel" aria-labelledby="form-tab">
          @include('frontend.pengaduan.sponsor.form')
        </div>
      </div>

    </div>
  </div>
</div>
@endsection