<div class="p-3 pt-4">
  <h2>{{ isset($menu->first()->tautan) ? $menu->first()->parent->judul : 'Profil' }}</h2>
  {{-- <p>Doloremque deleniti! Reprehenderit perspiciatis facilis fugiat, fuga eaque quisquam, harum.</p> --}}
</div>
<div class="menu" id="menu-sidebar">
  @foreach($menu as $m)
  @if(isset($m->tautan))
    @if($m->tautan != 'separator' && $m->tautan != 'layanan-unggulan' && count($m->childs) == 0)
      <a class="{{ $m->tautan == Request::url() ? 'active' : '' }}" href="{{ $m->tautan }}">{{ $m->judul }}</a>
    @elseif(count($m->childs) > 0)
      <div class="btn-group dropright w-100">
        <a class="btn btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{ $m->judul }}
        </a>
        <div class="dropdown-menu">
          @foreach($m->childs as $c)
          <a href="{{ $c->tautan }}">{{ $c->judul }}</a>
          @endforeach
        </div>
      </div>
    @endif
  @else
  <a class="{{ $m->slug == $record->slug ? 'active' : '' }}" href="{{ url('profil/'.$m->slug) }}">{{ $m->judul }}</a>
  @endif
  @endforeach
</div>
