@extends('layouts.frontend-scaffold-sidebar')

@section('styles')
@endsection

@section('jumbotron')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-profil']['isi']) }}" width="100%"></div>
</div>
@endsection

@section('content-sidebar')
  @include('frontend.profile.menu')
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Profil</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ session('lang') == 'english' ? (!is_null($record->judul_en) ? $record->judul_en : $record->judul) : $record->judul }}</li>
  </ol>
</nav>
@endsection

@section('content-main')
<h2>{{ session('lang') == 'english' ? (!is_null($record->judul_en) ? $record->judul_en : $record->judul) : $record->judul }}</h2>

<div class="text-center my-3"> 
@if($record->gambar != "")
<img src="{{ url('/storage/'.$record->gambar) }}" alt="Featured image" class="img-fluid rounded">
@endif
</div>

{!! session('lang') == 'english' ? (!is_null($record->konten_en) ? $record->konten_en : $record->konten) : $record->konten !!}

@endsection