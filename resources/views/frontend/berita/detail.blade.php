@extends('layouts.frontend-scaffold-sidebar')

@section('jumbotron')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>
@endsection

@section('content-sidebar')
  <div class="p-3 pt-4">
    <h2>Berita</h2>
  </div>
  <div class="menu less-padding">
    @if(isset($bulan))
    <ol class="list-unstyled mb-0">
      <li><a href="{{url('berita')}}">Semua Berita</a></li>
      @foreach($bulan as $key => $month)
      <li><a href="{{ url('berita/list/'.$month->format('Y-m')) }}" class="py-1">{{ $month->format('F Y') }}</a></li>
      @endforeach
    </ol>
    @endif
  </div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item active" aria-current="page">Berita</li>
  </ol>
</nav>
@endsection

@section('content-main')
<div class="blog-post">
  <h2 class="blog-post-title">{{ session('lang') == 'english' ? (!is_null($record->judul_en) ? $record->judul_en : $record->judul) : $record->judul }}</h2>
  <p class="blog-post-meta text-muted">
    <i class="fa fa-user"></i> {{ $record->creator->name }} | 
    <i class="fa fa-calendar"></i> {{ $record->tanggal ? $record->tanggal->format('d F Y') : $record->created_at->format('d F Y') }}
  </p>
  <img src="{{ url('storage/'.$record->photo) }}" alt="{{ $record->judul }}" style="width: 100%" class="img-fluid rounded">
  {!! session('lang') == 'english' ? (!is_null($record->konten_en) ? $record->konten_en : $record->konten) : $record->konten !!}
</div>
@endsection