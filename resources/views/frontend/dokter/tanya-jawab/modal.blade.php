<div class="modal fade" id="kategoriModal" tabindex="-1" role="dialog" aria-labelledby="kategoriModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="kategoriModalTitle">Pilih Kategori Pertanyaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @foreach($kategori as $kat)
                    <button type="button" class="btn btn-outline-info btn-sm btn-block btn-tanya" data-value="{{ $kat->id }}" data-disclaimer="{{ $kat->pernyataan }}">{{ $kat->nama }}</button>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tanyajawabModal" tabindex="-1" role="dialog" aria-labelledby="tanyajawabModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form class="form" id="dataForm" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tanyajawabModalTitle">Buat pertanyaan baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <fieldset>
                        <div class="alert alert-danger" role="alert" style="display: none">
                            <h5 class="alert-heading">Terdapat kesalahan!</h5>
                            <ul class="error list"></ul>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="nama" placeholder="Nama Anda" required="">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Alamat Email Anda" required="">
                            <small class="d-block text-muted mt-1 p-1">Kami membutuhkan alamat email anda untuk keperluan notifikasi; alamat email Anda akan kami rahasiakan.</small>
                            <div class="custom-control custom-checkbox mr-sm-2">
                              <input type="hidden" name="rahasia" value="1">
                              <input type="checkbox" class="custom-control-input" id="customControlInline" name="rahasia" value="0">
                              <label class="custom-control-label" for="customControlInline">Saya bersedia nama saya diketahui oleh publik</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <select name="id_kategori" class="form-control" required="" style="display: none">
                                {!! \App\Models\Master\KategoriTanyaJawab::options('nama') !!}
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="judul" placeholder="Judul Pertanyaan" required="">
                        </div>
                        <div class="form-group">
                            <textarea name="konten"" rows="5" class="form-control" placeholder="Tuliskan pertanyaan Anda di sini." required=""></textarea>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-info" onclick="kirimPertanyaan()"><i class="fa fa-paper-plane"></i> Kirim pertanyaan</button>
                </div>
            </div>
        </form>
    </div>
</div>