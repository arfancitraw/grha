@extends('layouts.frontend-scaffold')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/sweetalert/sweetalert2.min.css') }}"/>
@append

@section('js')
<script src="{{ asset('plugins/sweetalert/sweetalert2.min.js') }}"></script>
@append

@section('styles')
  <style>
  .table tr, .table td {
    vertical-align: middle!important;
  }
  .table td:first-child {
    width: 65%
  }
  .table td:last-child {
    width: 15%;
  }
  .upsized {
    font-size: 1.4rem;
    font-weight: 500;
    line-height: 1.2;
  }
  .nav-pills .nav-link.active {
    background: transparent;
    color: #3b9ca5;
    border-bottom: 2px solid #3b9ca5;
    border-radius: 0
  }
  .btn-question {
    border-radius: 0;
    text-align: left;
    font-size: 1.2rem;
    line-height: 1.1;
    font-weight: 500;
    border-color: transparent;
  }
  .btn-question small {
    font-weight: 300;
  }
  .custom-control-label::before, .custom-control-label::after{
    top: .5rem !important;
  }
  </style>
@endsection

@section('scripts')
  <script>
    $('.btn-question').click(function(event) {
        $("#kategoriModal").modal('show');
    });
    $('.btn-tanya').click(function(event) {
      var kategori = $(this).data('value');
      var pernyataan = $(this).data('disclaimer');

      $('select[name=id_kategori]').val(kategori);
      $("#kategoriModal").modal('hide');
      
      if(pernyataan == "1"){
        swal({
          title: 'Perhatian!',
          text: "Jawaban yang diberikan adalah bersifat umum, bukan merupakan petunjuk medis dan mungkin identitas anda akan diketahui publik.",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Saya Mengerti, Buat Pertanyaan',
          cancelButtonText: 'Batal'
        }).then((result) => {
          if (result) {
            $('#tanyajawabModal').modal('show');
          }
        });
      }else{
        $('#tanyajawabModal').modal('show');
      }
    });
    function kirimPertanyaan(){
      $("#dataForm").ajaxSubmit({
        success: function(resp){
          swal(
            'Terkirim',
            'Terimakasih, Pertanyaan anda akan kami review dan akan dijawab oleh dokter kami.',
            'success'
            ).then((result) => {
              location.reload();
              return true;
            })
          },
          error: function(resp){
            $('#dataForm')[0].reportValidity();
            var error = $('ul.error.list');
            // console.log(resp.responseJSON);
            error.html('');
            $.each(resp.responseJSON, function(index, val) {
              error.append('<li>'+val+'</li>');
            });

            $('.alert-danger').slideDown('fast');
            setTimeout(function(e){
              $('.alert-danger').slideUp('slow');
            }, 2000);
          }
        });
    }
  </script>
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-dokter']['isi']) }}" width="100%"></div>
</div>

<div class="container">
    <div class="text-center content-main pb-5">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ url('/') }}">PJNHK</a></li>
          <li class="breadcrumb-item"><a href="{{ url('tim-dokter') }}">Dokter</a></li>
          <li class="breadcrumb-item active" aria-current="page"><a href="{{ url('dokter/tanya-jawab') }}">Tanya Jawab</a></li>
        </ol>
      </nav>

      <ul class="nav nav-pills" id="myTab" role="tablist">
        @foreach($kategori as $i => $tab)
        <li class="nav-item">
          <a class="nav-link @if($i==0) active @endif" data-toggle="tab" href="#kategori-{{$tab->id}}" role="tab" aria-selected="{{ $i==0 ? 'true' : 'false' }}">{{ $tab->nama }}</a>
        </li>
        @endforeach
      </ul>

      <div class="card my-3">
        <div class="card-body p-0">
          <button type="button" class="btn btn-lg btn-block btn-outline-info btn-question">
            Ada pertanyaan? <br>
            <small>Klik disini untuk membuat pertanyaan</small>
          </button>
        </div>
      </div>

      <div class="tab-content" id="myTabContent">
        @foreach($kategori as $i => $tab)
        <div class="tab-pane fade @if($i==0) show active @endif" id="kategori-{{$tab->id}}" role="tabpanel">
          @include('frontend.dokter.tanya-jawab.list', ['data' => $tab->pertanyaan])
        </div>
        @endforeach
      </div>
    </div>
</div>

@include('frontend.dokter.tanya-jawab.modal')
@endsection