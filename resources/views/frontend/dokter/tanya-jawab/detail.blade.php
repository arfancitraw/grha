@extends('layouts.frontend-scaffold')

@section('styles')
  <style>
    .emphasis {
      padding: 1rem 1rem;
      margin-left: -1rem;
      margin-right: -1rem;
      margin-bottom: 2rem;
      background: #fafafa;

      border-radius: 10px;
    }

    .emphasis.respond{
      background: rgb(59, 156, 165, .8);
    }
    .emphasis.respond .text-muted{
      color: #fff !important;
    }

    .emphasis .sender {
      border-left: none
    }

    .sender-smol {
      font-size: 90%;
      line-height: 1.25
    }

    .list-group-flush:last-child .list-group-item:last-child {
      border-bottom: 1px solid rgba(0,0,0,.125)!important;
    }

    @media (min-width: 768px) {
      .emphasis .responder {
        border-left: 1px solid #fff
      }

      .emphasis .sender {
        border-right: 1px solid #999
      }
    }
  </style>
@endsection

@section('scripts')
  <script>
    $('.btn-question').click(function(event) {
      swal({
        title: 'Perhatian!',
        text: "Jawaban yang diberikan adalah bersifat umum dan bukan merupakan petunjuk tindakan medis.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Saya Mengerti, Buat Pertanyaan',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result) {
          $('#tanyajawabModal').modal('show');
        }
      });
    });

    function kirimPertanyaan(){
      $("#dataForm").ajaxSubmit({
        success: function(resp){
          swal(
            'Terkirim',
            'Terimakasih, Tanggapan anda akan kami review dan akan dijawab oleh dokter kami.',
            'success'
            ).then((result) => {
              location.reload();
              return true;
            })
          },
          error: function(resp){
            $('#dataForm')[0].reportValidity();
            var error = $('ul.error.list');
            // console.log(resp.responseJSON);
            error.html('');
            $.each(resp.responseJSON, function(index, val) {
              error.append('<li>'+val+'</li>');
            });

            $('.alert-danger').slideDown('fast');
            setTimeout(function(e){
              $('.alert-danger').slideUp('slow');
            }, 2000);
          }
        });
    }
  </script>
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-dokter']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="content-main pb-5">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">PJNHK</a></li>
        <li class="breadcrumb-item"><a href="{{ url('tim-dokter') }}">Dokter</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{ url('dokter/tanya-jawab') }}">Tanya Jawab</a></li>
      </ol>
    </nav>

    <h3>
      <small class="float-right">
        <span class="badge badge-primary">{{ $record->kategori->nama }}</span>
      </small>
      {{ $record->judul }}
    </h3>
    <hr>

    <div class="emphasis mb-3">
      <div class="row d-flex align-items-stretch">
        <div class="col-12 col-md-6 col-lg-3 sender">
          <div class="info-dokter d-flex">
            <div class="info-dokter-holder">
              <img src="{{ asset('img/foto_placeholder.png') }}" alt="Foto dokter" class="img rounded" width="48" height="48">
            </div>
            <div class="info-dokter-holder ml-3">
              <h5 class="d-block text-muted mb-0">{{ !$record->rahasia ? $record->nama : 'Dirahasiakan' }}</h5>
              <small class="text-muted"><i class="fa fa-calendar"></i> {{ $record->created_at ? $record->created_at->format('d F Y') : $record->created_at->format('d F Y') }}
              </small>
            </div>
          </div>
        </div>  
        <div class="col-12 col-md-6 col-lg-9">
          <h5>Pertanyaan:</h5>
          <p class="text-muted mb-0">{{ $record->konten }}</p>
        </div>
      </div>
    </div>

    @if($record->detail->count() == 0)
      <div class="emphasis mb-3 text-center">
        <h5 class="text-muted"><i>Belum ada Jawaban terkait pertanyaan ini.</i></h5>
      </div>
    @else
      @foreach($record->detail as $detail)
        @if($detail->internal)
          <div class="emphasis respond mb-3">
            <div class="row d-flex align-items-stretch">
              <div class="col-12 col-md-6 col-lg-9">
                <h5 class="text-white">{{ $detail->judul }}</h5>
                <p class="text-muted mb-0">{{ $detail->konten }}</p>
                @if($detail->referensi)
                <a href="{{$detail->referensi}}">Referensi</a>
                @endif
              </div>
              <div class="col-12 col-md-6 col-lg-3 responder">
                <div class="info-dokter d-flex">
                  <div class="info-dokter-holder">
                    <img src="{{ asset('img/foto_placeholder.png') }}" alt="Foto dokter" class="img rounded" width="48" height="48">
                  </div>
                  <div class="info-dokter-holder ml-3">
                    <h5 class="d-block text-muted mb-0">{{ !$detail->rahasia ? $detail->nama : 'Dirahasiakan' }}</h5>
                    <small class="text-muted"><i class="fa fa-calendar"></i> {{ $detail->created_at ? $detail->created_at->format('d F Y') : $detail->created_at->format('d F Y') }}
                    </small>
                  </div>
                </div>
              </div>  
            </div>
          </div>
        @else
          <div class="emphasis mb-3">
            <div class="row d-flex align-items-stretch">
              <div class="col-12 col-md-6 col-lg-3 sender">
                <div class="info-dokter d-flex">
                  <div class="info-dokter-holder">
                    <img src="{{ asset('img/foto_placeholder.png') }}" alt="Foto dokter" class="img rounded" width="48" height="48">
                  </div>
                  <div class="info-dokter-holder ml-3">
                    <h5 class="d-block text-muted mb-0">{{ !$detail->rahasia ? $detail->nama : 'Dirahasiakan' }}</h5>
                    <small class="text-muted"><i class="fa fa-calendar"></i> {{ $detail->created_at ? $detail->created_at->format('d F Y') : $detail->created_at->format('d F Y') }}
                    </small>
                  </div>
                </div>
              </div>  
              <div class="col-12 col-md-6 col-lg-9">
                <h5>{{ $detail->judul }}</h5>
                <p class="text-muted mb-0">{{ $detail->konten }}</p>
              </div>
            </div>
          </div>
        @endif
      @endforeach
    @endif

    <div class="row dividing">
      <button type="button" class="btn btn-dividing btn-question">Punya tanggapan seputar topik ini?</button>
    </div>
  </div>
</div>

@include('frontend.dokter.tanya-jawab.modal-secondary')
@endsection