<table class="table table-bordered bg-white">
  <tbody>
    @if($data->isNotEmpty())
      @foreach($data as $row)
        <tr>
          <td class="text-left">
            <a href="{{ url('dokter/tanya-jawab/detail').'/'.str_slug($row->judul) }}" class="d-block"><h5>{{ $row->judul }}</h5></a>
            <span class="text-muted"><b>{{ $row->kategori->nama }}</b> | <b>{{ !$row->rahasia ? $row->nama : 'Dirahasiakan' }}</b>, {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->formatLocalized('%d %B %Y') }}</span>
          </td>
          <td class="text-left d-none d-md-table-cell">
            <span class="upsized text-muted d-block">Diskusi: {{ $row->detail->count() }}</span>
            <span class="text-muted">Dilihat: {{ $row->access->count() }} kali</span>
          </td>
        </tr>
      @endforeach
    @else
      <tr>
        <td colspan="2" style="text-align: center">
          <h5>Tidak Ada Pertanyaan</h5>
        </td>
      </tr>
    @endif
  </tbody>
</table>