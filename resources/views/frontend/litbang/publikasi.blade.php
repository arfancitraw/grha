<div class="col-12 my-3">
	<div class="card bg-info">
		<h5 class="card-header text-white">List Publikasi</h5>
		<div class="card-body bg-white p-0">
			<table class="table table-bordered m-0">
				<thead>
					<tr>
						<th width="30%">Judul</th>
						<th class="text-center" width="5%">Tahun</th>
						<th width="25%" class="text-center">Peneliti Utama</th>
						<th width="25%" class="text-center">Peneliti Pendamping</th>
						<th class="text-center" style="width: 20px !important">Tautan</th>
					</tr>
				</thead>
				<tbody>
					@if(isset($listPublikasi) && count($listPublikasi) > 0)
					@foreach($listPublikasi as $in)
					<tr>
						<td>{{ $in->judul }}</td>
						<td>{{ $in->tahun }}</td>
						<td>{{ $in->peneliti_utama }}</td>
						<td>{{ $in->peneliti_pendamping }}</td>
						<td class="text-center">
							@if(is_null($in->file))
								@if($in->link)
									<a target="_blank" href="{{ $in->link }}">Buka Tautan</a>
								@endif 
							@else

							<a target="_blank" href="{{ url('storage/'.$in->file) }}">Download</a>
							@endif 
						</td>
					</tr>
					@endforeach
					@else
					<p class="text-center mb-0">Belum ada data publikasi</p>
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>