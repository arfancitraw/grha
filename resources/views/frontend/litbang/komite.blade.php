<div class="col-12 my-3">
	<div class="card bg-info">
		<h5 class="card-header text-white">Komite Etik Litbang</h5>
		<div class="card-footer table-responsive bg-white p-0">
			<table class="table table-bordered table-hover mb-0">
				<thead>
					<tr>
						<th class="text-center">Nama</th>
						<th class="text-center">Jabatan</th>
						<th class="text-center">Departemen</th>
						<th class="text-center">Tanggal Efektif</th>
					</tr>
				</thead>
				<tbody>
					@if(isset($komiteEtik) && count($komiteEtik) > 0)
					@foreach($komiteEtik as $in)
					<tr>
						<td class="text-center">{{ $in->nama }}</td>
						<td class="text-center">{{ $in->jabatandokter->jabatan }}</td>
						<td class="text-center">{{ $in->departemen }}</td>
						<td class="text-center">{{ $in->tanggal_efektif_awal->format('d F Y') . ' s/d ' . $in->tanggal_efektif_akhir->format('d F Y') }}</td>
					</tr>
					@endforeach
					@else
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>