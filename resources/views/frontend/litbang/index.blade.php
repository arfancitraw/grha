@extends('layouts.frontend-scaffold')

@section('styles')
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-litbang']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col-12 content-main">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item active" aria-current="page">Litbang</li>
        </ol>
      </nav>

      <h2>Penelitian dan Pengembangan</h2>
      @if(isset($info) && count($info) > 0)
        @foreach($info as $in)
          <blockquote>{{ session('lang') == 'english' ? (!is_null($in->judul_en) ? $in->judul_en : $in->judul) : $in->judul }}</blockquote>
          <p>{!! session('lang') == 'english' ? (!is_null($in->konten_en) ? $in->konten_en : $in->konten) : $in->konten !!}</p>
        @endforeach
      @endif

      <div class="row mb-4">
        @include('frontend.litbang.komite')
        @include('frontend.litbang.publikasi')
      </div>

    </div>
  </div>
</div>
@endsection