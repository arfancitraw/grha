@extends('layouts.frontend-scaffold')

@section('styles')
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-litbang']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col-12 content-main">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item"><a href="#">Diklat</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{ $listPublikasi->judul }}</li>
        </ol>
      </nav>
      <h2>{{ $listPublikasi->judul }}</h2>     
      <ul class="list-group">
        <li class="list-group-item">{{ $listPublikasi->tahun }}</li>
        <li class="list-group-item"><small>
          Tanggal : {{ $listPublikasi>tanggal->tahun }}<div class="float-right">Jam Pelaksanaan : {{ $listPublikasi->tahun }}</div>
        </small></li>
      </ul>

    </div>
  </div>
</div>
@endsection