@extends('layouts.frontend-scaffold')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/timepicker/jquery.timepicker.min.css') }} ">
@endsection
@section('js')
<script src="{{ asset('plugins/timepicker/jquery.timepicker.min.js') }}"></script>
@endsection

@section('styles')
<style>
.table tr, .table td {
  vertical-align: middle!important;
}
.table th:first-child,
.table td:first-child {
  width: 50%;
  text-align: left;
}
.table td:last-child {
  width: 15%;
}
.upsized {
  font-size: 1.4rem;
  font-weight: 500;
  line-height: 1.2;
}
.nav-pills .nav-link.active {
  background: transparent;
  color: #3b9ca5;
  border-bottom: 2px solid #3b9ca5;
  border-radius: 0
}
.btn-question {
  border-radius: 0;
  text-align: left;
  font-size: 1.2rem;
  line-height: 1.1;
  font-weight: 500;
  border-color: transparent;
}
.btn-question small {
  font-weight: 300;
}
</style>
@endsection

@section('scripts')
<script>
  function kirimPertanyaan() {
    swal(
      'Terkirim!',
      'Pertanyaan Anda akan dijawab oleh dokter kami.',
      'success'
    ).then(
      $('#tanyajawabModal').modal('hide')
    )
  }

  $(document).ready(function($) {
      $('.time').timepicker({ 
        timeFormat: 'H:i:s',
        useSelect: true,
        step: 15,
        minTime: '7:00',
        maxTime: '17:00',
        className: 'form-control'
      });

      $('.btn-reset').click(function(e){
        $('[name=dokter]').val('');
        $('[name=poli]').val('');
        $('[name=hari]').val('');
        $('#jadwalForm').submit();
      })
  });
</script>
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-dokter']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col-12 text-center content-main">

      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item active" aria-current="page">Jadwal Dokter</li>
        </ol>
      </nav>

      <form id="jadwalForm" class="form row justify-content-center mb-4" action="{{ url($pageUrl) }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group col-12 col-md-2 pr-0 mb-0">
          <label for="dokter" class="sr-only">Dokter</label>
          <input type="text" name="dokter" class="form-control" placeholder="Nama Dokter" value="{{ $selected['dokter'] }}">
        </div>
        <div class="form-group col-12 col-md-2 pr-0 mb-0">
          <label for="poli" class="sr-only">Poli</label>
          <select name="poli" id="" class="form-control select2">
            <option value="">Semua Poli</option>
            @foreach($poli as $p)
            <option value="{{ $p->kode_departemen }}" @if($p->kode_departemen == $selected['poli']) selected @endif>{{$p->nama_departemen}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group col-12 col-md-2 pr-0 mb-0">
          <label for="hari" class="sr-only">Hari</label>
          <select name="hari" id="" class="form-control select2">
            <option value="">Semua hari</option>
            <option value="1" @if($selected['hari'] == 1) selected @endif>Senin</option>
            <option value="2" @if($selected['hari'] == 2) selected @endif>Selasa</option>
            <option value="3" @if($selected['hari'] == 3) selected @endif>Rabu</option>
            <option value="4" @if($selected['hari'] == 4) selected @endif>Kamis</option>
            <option value="5" @if($selected['hari'] == 5) selected @endif>Jumat</option>
            <option value="6" @if($selected['hari'] == 6) selected @endif>Sabtu</option>
            <option value="7" @if($selected['hari'] == 7) selected @endif>Minggu</option>
          </select>
        </div>
        <div class="form-group col-12 col-md-1 p-0 mr-3 mb-0">
          <button class="btn btn-outline-info" type="submit"><i class="fa fa-search"></i></button>
          <button class="btn btn-outline-secondary btn-reset" type="button"><i class="fa fa-sync"></i></button>
        </div>
      </form>

      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>Dokter</th>
            <th>Poli</th>
            <th>Hari</th>
            <th>Jam Praktek</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @php 
            $days = ['SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU', 'MINGGU'];
          @endphp
          @foreach($jadwal as $row)
          <tr>
            <td>{{ $row->listdokter->nama }}</td>
            <td>{{ $row->nama_departemen }}</td>
            <td>{{ $row->display_hari }}</td>
            <td>{{ substr($row->jam_mulai_praktek, 0, 5) }} - {{ substr($row->jam_selesai_praktek, 0, 5) }}</td>
            <td class="text-center">
              @if($row->poli && $row->poli->aktif)
              <button type="button" class="btn btn-primary btn-sm btn-janji"
                      data-dokter="{{ $row->listdokter->kode }}"
                      data-hari="{{ array_search($row->display_hari, $days) + 1 }}"
                      data-poli="{{ $row->kode_departemen }}">Buat Perjanjian</button>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection