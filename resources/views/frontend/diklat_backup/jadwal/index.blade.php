@extends('layouts.frontend-scaffold-sidebar')

@section('styles')
<style>
  dl {
    display: flex
  }
  dt {
    width: 20%;
  }
  dd {
    width: 80%;
  }
</style>
@append

@section('jumbotron')
<div class="jumbotron karir">
  <div class="container d-flex">
    {{-- <div class="col-12 col-lg-6 d-none d-lg-block">
      <h1 class="display-3"><span class="text-danger">Diklat</span> PJNHK</h1>
      <p>Quos perferendis unde incidunt suscipit nostrum, vitae aliquam quasi illum reiciendis, obcaecati iste aut molestias culpa quae minima natus? Doloribus, sint, provident.</p>
    </div>
      --}}
  </div>
</div>
@endsection

@section('content-sidebar')
  <div class="p-3 pt-4">
    <h2>Diklat</h2>
    {{-- <p>Doloremque deleniti! Reprehenderit perspiciatis facilis fugiat, fuga eaque quisquam, harum.</p> --}}
  </div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Diklat</a></li>
    <li class="breadcrumb-item active" aria-current="page">Jadwal Diklat</li>
  </ol>
</nav>
@endsection

@section('content-main')

<h2>Jadwal Diklat</h2>

<div id="karir-list" class="accordion icon">
  @foreach($record as $key => $raw)
  <div class="item mt-3">
    <div class="item-header" id="item-{{ $key }}">
      <button class="btn btn-link" data-toggle="collapse" data-target="#{{ $key }}" aria-expanded="false" aria-controls="{{ $key }}">
        {!! $raw->judul !!}
      </button>
    </div>

    <div id="{{ $key }}" class="collapse" aria-labelledby="item-{{ $key }}" data-parent="#karir-list">
      <div class="item-body">
        <dl>
          <dt>Tanggal:</dt>
          <dd>{{ $raw->tanggal }}</dd>
          <dt>Jam:</dt>
          <dd>{{ $raw->jam }}</dd>
        </dl>

        {!! $raw->konten !!}
      </div>
    </div>
  </div> 
  @endforeach
</div>
@endsection