@extends('layouts.frontend-scaffold-sidebar')

@section('jumbotron')
<div class="jumbotron karir">
  <div class="container d-flex">
    {{-- <div class="col-12 col-lg-6 d-none d-lg-block">
      <h1 class="display-3"><span class="text-danger">Karir</span> PJNHK</h1>
      <p>Quos perferendis unde incidunt suscipit nostrum, vitae aliquam quasi illum reiciendis, obcaecati iste aut molestias culpa quae minima natus? Doloribus, sint, provident.</p>
    </div> --}}
     
  </div>
</div>
@endsection

@section('content-sidebar')
  <div class="p-3 pt-4">
    <h2>Pusat Karir</h2>
    {{-- <p>Doloremque deleniti! Reprehenderit perspiciatis facilis fugiat, fuga eaque quisquam, harum.</p> --}}
  </div>
  <div class="menu">
    <a href="{{ url('karir/info') }}">Info Lowongan</a>
    <a class="active" href="{{ url('karir/hasil') }}">Hasil Seleksi</a>
  </div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Karir</a></li>
    <li class="breadcrumb-item active" aria-current="page">Info Lowongan</li>
  </ol>
</nav>
@endsection

@section('content-main')
<h2>Hasil Seleksi Lowongan Kerja</h2>
{{-- {{ dd($record) }} --}}
<table class="table table-striped text-center my-5">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Posisi</th>
      <th scope="col">Seleksi</th>
      <th scope="col">Tanggal Seleksi</th>
      <th scope="col">Download Berkas</th>
    </tr>
  </thead>
  <tbody>
    @foreach($record as $key => $row)
    <tr>
      <th scope="row">{{ $key + 1 }}</th>
      <td>{{ $row->karir->judul }}</td>
      <td>{{ $row->keterangan }}</td>
      <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('d-F-Y') }}</td>
      <td><a href="{{asset('storage') }}/{{ $row->lampiran }}" class="btn btn-link download"></a></td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection