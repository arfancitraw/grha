@extends('layouts.frontend-scaffold-sidebar')

@section('jumbotron')
<div class="jumbotron media">
  <div class="container d-flex">
    {{-- <div class="col-12 col-lg-6 d-none d-lg-block">
      <h1 class="display-3"><span class="text-danger">Media</span> PJNHK</h1>
      <p>Quos perferendis unde incidunt suscipit nostrum, vitae aliquam quasi illum reiciendis, obcaecati iste aut molestias culpa quae minima natus? Doloribus, sint, provident.</p>
    </div> --}}
     
  </div>
</div>
@endsection

@section('content-sidebar')
<div class="p-3 pt-4">
  <h2>Media</h2>
  {{-- <p>Doloremque deleniti! Reprehenderit perspiciatis facilis fugiat, fuga eaque quisquam, harum.</p> --}}
</div>
<div class="menu">
  <a href="{{ url('media/pengumuman') }}">Pengumuman</a>
  <a class="active" href="{{ url('media/kegiatan') }}">Artikel</a>
  <a href="{{ url('media/pustaka') }}"">Pustaka Kesehatan</a>
</div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Media</a></li>
    <li class="breadcrumb-item active" aria-current="page">Artikel</li>
  </ol>
</nav>
@endsection

@section('content-main')
<h2>Artikel</h2>

<div class="list-kegiatan">
  <div class="row my-5">
    <div class="col-12 col-md-4">
      <a href="{{ url('media/kegiatan/detail') }}">
        <img src="{{ asset('img/icons/icon_folder.png') }}" alt="icon" height="120">
        <span class="d-block mt-4 text-center">Album Artikel</span>
      </a>
    </div>
    <div class="col-12 col-md-4">
      <a href="{{ url('media/kegiatan/detail') }}">
        <img src="{{ asset('img/icons/icon_folder.png') }}" alt="icon" height="120">
        <span class="d-block mt-4 text-center">Album Artikel</span>
      </a>
    </div>
    <div class="col-12 col-md-4">
      <a href="{{ url('media/kegiatan/detail') }}">
        <img src="{{ asset('img/icons/icon_folder.png') }}" alt="icon" height="120">
        <span class="d-block mt-4 text-center">Album Artikel</span>
      </a>
    </div>
  </div>
  <div class="row my-5">
    <div class="col-12 col-md-4">
      <a href="{{ url('media/kegiatan/detail') }}">
        <img src="{{ asset('img/icons/icon_folder.png') }}" alt="icon" height="120">
        <span class="d-block mt-4 text-center">Album Artikel</span>
      </a>
    </div>
    <div class="col-12 col-md-4">
      <a href="{{ url('media/kegiatan/detail') }}">
        <img src="{{ asset('img/icons/icon_folder.png') }}" alt="icon" height="120">
        <span class="d-block mt-4 text-center">Album Artikel</span>
      </a>
    </div>
    <div class="col-12 col-md-4">
      <a href="{{ url('media/kegiatan/detail') }}">
        <img src="{{ asset('img/icons/icon_folder.png') }}" alt="icon" height="120">
        <span class="d-block mt-4 text-center">Album Artikel</span>
      </a>
    </div>
  </div>
</div>
@endsection