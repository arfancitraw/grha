@extends('layouts.frontend-scaffold-sidebar')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/magnific-popup/magnific-popup.css') }}">
@endsection
@section('js')
<script src="{{ asset('plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
@endsection
@section('scripts')
<script>
  $(document).ready(function() {
    $('.image-gallery').magnificPopup({
      type:'image',
      mainClass: 'mfp-with-zoom',
      zoom: {
        enabled: true,
        duration: 300,
        easing: 'ease-in-out',
        opener: function(openerElement) {
          return openerElement.is('img') ? openerElement : openerElement.find('img');
        }
      }
    });
  });
</script>
@endsection

@section('jumbotron')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>
@endsection

@section('content-sidebar')
<div class="p-3 pt-4">
  <h2>Media</h2>
  {{-- <p>Doloremque deleniti! Reprehenderit perspiciatis facilis fugiat, fuga eaque quisquam, harum.</p> --}}
</div>
<div class="menu">
  <a href="{{ url('media/pengumuman') }}">Pengumuman</a>
  <a class="active" href="{{ url('media/kegiatan') }}">Artikel</a>
  <a href="{{ url('media/pustaka') }}"">Pustaka Kesehatan</a>
</div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Media</a></li>
    <li class="breadcrumb-item active" aria-current="page">Artikel</li>
  </ol>
</nav>
@endsection

@section('content-main')
<h2>Artikel</h2>

<div class="gallery">
  <a href="{{ asset('img/backgrounds/sample1.jpg') }}" class="image-gallery" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua.">
    <img src="{{ asset('img/backgrounds/sample1.jpg') }}" alt="yes">
  </a>
  <a href="{{ asset('img/backgrounds/sample3.jpg') }}" class="image-gallery" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua.">
    <img src="{{ asset('img/backgrounds/sample3.jpg') }}" alt="yes">
  </a>
  <a href="{{ asset('img/backgrounds/karir.jpg') }}" class="image-gallery" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua.">
    <img src="{{ asset('img/backgrounds/karir.jpg') }}" alt="yes">
  </a>
  <a href="{{ asset('img/backgrounds/media.jpg') }}" class="image-gallery" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua.">
    <img src="{{ asset('img/backgrounds/media.jpg') }}" alt="yes">
  </a>
  <a href="{{ asset('img/backgrounds/sample2.jpg') }}" class="image-gallery" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua.">
    <img src="{{ asset('img/backgrounds/sample2.jpg') }}" alt="yes">
  </a>
  <a href="{{ asset('img/backgrounds/pelayanan.jpg') }}" class="image-gallery" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua.">
    <img src="{{ asset('img/backgrounds/pelayanan.jpg') }}" alt="yes">
  </a>

</div>
@endsection