@extends('layouts.frontend-scaffold')

@section('styles')
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col-12 content-main">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item"><a href="#">Media</a></li>
          <li class="breadcrumb-item"><a href="#">Pustaka</a></li>
          <li class="breadcrumb-item"><a href="#">Tindakan</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{ $pustaka->nama }}</li>
        </ol>
      </nav>
      <div class="row">
        <div class="col-md-10">
          <h2 class="mb-4">{{ $pustaka->nama }}</h2>
        </div>
        <div class="col-md-2 text-right">
          @if(is_null($pustaka->lampiran))
          {{-- tidak ada lampiran di temukan --}}
          @else
          <a target="_blank" href="{{ url('storage/'.$pustaka->lampiran) }}"><i class="fa fa-print"></i> Download</a>
          @endif 
        </div>
      </div>
      

      <div class="row">
        <div class="col-md-12"> 
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingIndikasi">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#indikasi" aria-expanded="true" aria-controls="indikasi">
                                Indikasi / Temuan klinis / Diagnosis sehingga memerlukan Tindakan / Pemeriksaan
                            </button>
                        </h5>
                    </div>

                    <div id="indikasi" class="collapse show" aria-labelledby="headingIndikasi" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->indikasi !!}
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingtujuan">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#tujuan" aria-expanded="true" aria-controls="tujuan">
                                Tujuan Tindakan
                            </button>
                        </h5>
                    </div>

                    <div id="tujuan" class="collapse" aria-labelledby="headingtujuan" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->tujuan !!}
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingtatacara">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#tatacara" aria-expanded="true" aria-controls="tatacara">
                                Tatacara pelaksanaan tindakan
                            </button>
                        </h5>
                    </div>

                    <div id="tatacara" class="collapse" aria-labelledby="headingtatacara" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->tatacara !!}
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingrisiko">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#risiko" aria-expanded="true" aria-controls="risiko">
                                Risiko dan komplikasi yang mungkin terjadi (yang sudah umum, jarang atau tidak dapat dibayangkan sebelumnya)
                            </button>
                        </h5>
                    </div>

                    <div id="risiko" class="collapse" aria-labelledby="headingrisiko" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->risiko !!}
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingalternatif">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#alternatif" aria-expanded="true" aria-controls="alternatif">
                                Alternatif lain
                            </button>
                        </h5>
                    </div>

                    <div id="alternatif" class="collapse" aria-labelledby="headingalternatif" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->alternatif !!}
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingPrognosisDilakukan">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#prognosis_dilakukan" aria-expanded="true" aria-controls="prognosis_dilakukan">
                                Prognosis bila dilakukan tindakan
                            </button>
                        </h5>
                    </div>

                    <div id="prognosis_dilakukan" class="collapse" aria-labelledby="headingPrognosisDilakukan" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->prognosis_dilakukan !!}
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingPrognosisTidak">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#prognosis_tidak" aria-expanded="true" aria-controls="prognosis_tidak">
                            Prognosis bila tidak dilakukan tindakan             
                            </button>
                        </h5>
                    </div>

                    <div id="prognosis_tidak" class="collapse" aria-labelledby="headingPrognosisTidak" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->prognosis_tidak !!}
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingperluasan">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#perluasan" aria-expanded="true" aria-controls="perluasan">
                                Perluasan tindakan / tindakan lain yang mungkin dilakukan
                            </button>
                        </h5>
                    </div>

                    <div id="perluasan" class="collapse" aria-labelledby="headingperluasan" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->perluasan !!}
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingpersiapan">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#persiapan" aria-expanded="true" aria-controls="persiapan">
                                Persiapan khusus
                            </button>
                        </h5>
                    </div>

                    <div id="persiapan" class="collapse" aria-labelledby="headingpersiapan" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->persiapan !!}
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingpenjelasan">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#penjelasan" aria-expanded="true" aria-controls="penjelasan">
                                Penjelasan tambahan
                            </button>
                        </h5>
                    </div>

                    <div id="penjelasan" class="collapse" aria-labelledby="headingpenjelasan" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->penjelasan !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
  </div>
</div>
@endsection