@extends('layouts.frontend-scaffold')

@section('jumbotron')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>
@endsection


@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Media</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pustaka Kesehatan</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col-12 content-main">
      <div class="row justify-content-between mt-4">
        <nav aria-label="breadcrumb" class="col-12 col-md-4">
          <ol class="breadcrumb mb-3 p-0">
            <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
            <li class="breadcrumb-item"><a href="#">Media</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pustaka Kesehatan</li>
          </ol>
        </nav>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="card bg-info mb-4">
            <h5 class="card-header text-white"><i class="fa fa-plus-square"></i> Pustaka Kesehatan</h5>
            <div class="card-body bg-white">
              <div class="mt-0">Seputar Penyakit Jantung dan Tindakannya</div>
              <nav aria-label="Page navigation example">
                <ul class="alphabets">
                  <li @if($keyword == 'A') class="active" @endif><a href="{{ url('pustaka/A') }}">A</a></li>
                  <li @if($keyword == 'B') class="active" @endif><a href="{{ url('pustaka/B') }}">B</a></li>
                  <li @if($keyword == 'C') class="active" @endif><a href="{{ url('pustaka/C') }}">C</a></li>
                  <li @if($keyword == 'D') class="active" @endif><a href="{{ url('pustaka/D') }}">D</a></li>
                  <li @if($keyword == 'E') class="active" @endif><a href="{{ url('pustaka/E') }}">E</a></li>
                  <li @if($keyword == 'F') class="active" @endif><a href="{{ url('pustaka/F') }}">F</a></li>
                  <li @if($keyword == 'G') class="active" @endif><a href="{{ url('pustaka/G') }}">G</a></li>
                  <li @if($keyword == 'H') class="active" @endif><a href="{{ url('pustaka/H') }}">H</a></li>
                  <li @if($keyword == 'I') class="active" @endif><a href="{{ url('pustaka/I') }}">I</a></li>
                  <li @if($keyword == 'J') class="active" @endif><a href="{{ url('pustaka/J') }}">J</a></li>
                  <li @if($keyword == 'K') class="active" @endif><a href="{{ url('pustaka/K') }}">K</a></li>
                  <li @if($keyword == 'L') class="active" @endif><a href="{{ url('pustaka/L') }}">L</a></li>
                  <li @if($keyword == 'M') class="active" @endif><a href="{{ url('pustaka/M') }}">M</a></li>
                  <li @if($keyword == 'N') class="active" @endif><a href="{{ url('pustaka/N') }}">N</a></li>
                  <li @if($keyword == 'O') class="active" @endif><a href="{{ url('pustaka/O') }}">O</a></li>
                  <li @if($keyword == 'P') class="active" @endif><a href="{{ url('pustaka/P') }}">P</a></li>
                  <li @if($keyword == 'Q') class="active" @endif><a href="{{ url('pustaka/Q') }}">Q</a></li>
                  <li @if($keyword == 'R') class="active" @endif><a href="{{ url('pustaka/R') }}">R</a></li>
                  <li @if($keyword == 'S') class="active" @endif><a href="{{ url('pustaka/S') }}">S</a></li>
                  <li @if($keyword == 'T') class="active" @endif><a href="{{ url('pustaka/T') }}">T</a></li>
                  <li @if($keyword == 'U') class="active" @endif><a href="{{ url('pustaka/U') }}">U</a></li>
                  <li @if($keyword == 'V') class="active" @endif><a href="{{ url('pustaka/V') }}">V</a></li>
                  <li @if($keyword == 'W') class="active" @endif><a href="{{ url('pustaka/W') }}">W</a></li>
                  <li @if($keyword == 'X') class="active" @endif><a href="{{ url('pustaka/X') }}">X</a></li>
                  <li @if($keyword == 'Y') class="active" @endif><a href="{{ url('pustaka/Y') }}">Y</a></li>
                  <li @if($keyword == 'Z') class="active" @endif><a href="{{ url('pustaka/Z') }}">Z</a></li>
                  <li @if($keyword == '0') class="active" @endif><a href="{{ url('pustaka/0') }}">#</a></li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="card bg-info mb-4">
            <h5 class="card-header text-white"><i class="fa fa-book"></i> {{ $keyword }}</h5>
            <div class="card-body bg-white">
              <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" id="nav-penyakit-tab" data-toggle="tab" href="#nav-penyakit" role="tab" aria-controls="nav-penyakit" aria-selected="true">
                    Penyakit
                  </a>
                  <a class="nav-item nav-link" id="nav-tindakan-tab" data-toggle="tab" href="#nav-tindakan" role="tab" aria-controls="nav-tindakan" aria-selected="false">
                    Tindakan
                  </a>
                </div>
              </nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-penyakit" role="tabpanel" aria-labelledby="nav-penyakit-tab">
                  <div class="list-group mt-3">
                  @if($penyakit->count() > 0)
                      @foreach($penyakit as $item)
                        <a href="{{ url('pustaka/detail/penyakit/'.$item->id) }}" class="list-group-item list-group-item-action">
                          {{ $item->nama }}
                        </a>
                      @endforeach
                  @else
                    <button type="button" class="list-group-item">Data Pustaka Penyakit Tidak Ditemukan</button>
                  @endif
                  </div>
                </div>
                <div class="tab-pane fade" id="nav-tindakan" role="tabpanel" aria-labelledby="nav-tindakan-tab">
                  <div class="list-group mt-3">
                  @if($tindakan->count() > 0)
                      @foreach($tindakan as $item)
                        <a href="{{ url('pustaka/detail/tindakan/'.$item->id) }}" class="list-group-item list-group-item-action">
                          {{ $item->nama }}
                        </a>
                      @endforeach
                  @else
                    <button type="button" class="list-group-item">Data Pustaka Tindakan Tidak Ditemukan</button>
                  @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row cards justify-content-center">
        <div class="mt-5">

        </div>
      </div>
    </div>
  </div>
</div>
@endsection