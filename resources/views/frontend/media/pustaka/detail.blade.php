@extends('layouts.frontend-scaffold')

@section('styles')
@endsection

@section('content')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col-12 content-main">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
          <li class="breadcrumb-item"><a href="#">Media</a></li>
          <li class="breadcrumb-item"><a href="#">Pustaka</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{ $pustaka->judul }}</li>
        </ol>
      </nav>
      <div class="row">
        <div class="col-md-10">
          <h2 class="mb-4">{{ $pustaka->judul }}</h2>
        </div>
        <div class="col-md-2 text-right">
          @if(is_null($pustaka->lampiran))
          {{-- tidak ada lampiran di temukan --}}
          @else
          <a target="_blank" href="{{ url('storage/'.$pustaka->lampiran) }}"><i class="fa fa-print"></i> Download</a>
          @endif 
        </div>
      </div>
      

      <div class="row">
        <div class="col-md-12"> 
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingPengertian">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#pengertian" aria-expanded="true" aria-controls="pengertian">
                                Pengertian
                            </button>
                        </h5>
                    </div>

                    <div id="pengertian" class="collapse show" aria-labelledby="headingPengertian" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->pengertian !!}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingGejala">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#gejala" aria-expanded="true" aria-controls="gejala">
                                Gejala
                            </button>
                        </h5>
                    </div>

                    <div id="gejala" class="collapse" aria-labelledby="headingGejala" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->gejala !!}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingPenyebab">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#penyebab" aria-expanded="true" aria-controls="penyebab">
                                Penyebab
                            </button>
                        </h5>
                    </div>

                    <div id="penyebab" class="collapse" aria-labelledby="headingPenyebab" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->penyebab !!}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingPenanganan">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#penanganan" aria-expanded="true" aria-controls="penanganan">
                                Penanganan & Pengobatan
                            </button>
                        </h5>
                    </div>

                    <div id="penanganan" class="collapse" aria-labelledby="headingPenanganan" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->penanganan !!}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingPencegahan">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#pencegahan" aria-expanded="true" aria-controls="pencegahan">
                                Pencegahan
                            </button>
                        </h5>
                    </div>

                    <div id="pencegahan" class="collapse" aria-labelledby="headingPencegahan" data-parent="#accordion">
                        <div class="card-body">
                            {!! $pustaka->pencegahan !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
  </div>
</div>
@endsection