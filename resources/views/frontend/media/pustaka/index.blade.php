@extends('layouts.frontend-scaffold-sidebar')

@section('jumbotron')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>
@endsection

@section('content-sidebar')
<div class="p-3 pt-4">
  <h2>Media</h2>
  {{-- <p>Doloremque deleniti! Reprehenderit perspiciatis facilis fugiat, fuga eaque quisquam, harum.</p> --}}
</div>
<div class="menu">
  <a href="{{ url('media/pengumuman/') }}">Pengumuman</a>
  <a href="{{ url('media/kegiatan/') }}">Kegiatan</a>
  <a class="active" href="{{ url('media/pustaka/') }}"">Pustaka Kesehatan</a>
</div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Media</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pustaka Kesehatan</li>
  </ol>
</nav>
@endsection

@section('content-main')
<div class="row my-4">
  <div class="col">
    <h2>Pustaka Kesehatan</h2>
  </div>
  <div class="col text-right">
    <div class="form-inline justify-content-end">
      <select name="" id="" class="form-control">
        <option value="az">Sort A-Z</option>
        <option value="az">Sort Z-A</option>
      </select>
      <ul class="nav nav-pills ml-4" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-th"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><i class="fas fa-th-list"></i></a>
        </li>
      </ul>
    </div>
  </div>
</div>

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

    <div class="list-pustaka">
      <div class="row my-5">
        <div class="col-12 col-md-2">
          <a href="{{ url('media/kegiatan/detail') }}">
            <h4 class="text-center display-3"><i class="fas fa-level-up-alt"></i></h4>
            <span class="d-block mt-4 text-center">..</span>
          </a>
        </div>
      </div>
      <div class="row my-5">
        <div class="col-12 col-md-2">
          <a href="{{ url('media/kegiatan/detail') }}">
            <h4 class="text-center display-3"><i class="fas fa-folder"></i></h4>
            <span class="d-block mt-4 text-center">Folder Pustaka</span>
          </a>
        </div>
        <div class="col-12 col-md-2">
          <a href="{{ url('media/kegiatan/detail') }}">
            <h4 class="text-center display-3"><i class="fas fa-folder"></i></h4>
            <span class="d-block mt-4 text-center">Folder Pustaka</span>
          </a>
        </div>
      </div>
      <div class="row my-5">
        <div class="col-12 col-md-2">
          <a href="{{ url('media/kegiatan/detail') }}">
            <h4 class="text-center display-3"><i class="fas fa-file-pdf"></i></h4>
            <span class="d-block mt-4 text-center">Item Pustaka</span>
          </a>
        </div>
        <div class="col-12 col-md-2">
          <a href="{{ url('media/kegiatan/detail') }}">
            <h4 class="text-center display-3"><i class="fas fa-file-pdf"></i></h4>
            <span class="d-block mt-4 text-center">Item Pustaka</span>
          </a>
        </div>
        <div class="col-12 col-md-2">
          <a href="{{ url('media/kegiatan/detail') }}">
            <h4 class="text-center display-3"><i class="fa fa-file-excel"></i></h4>
            <span class="d-block mt-4 text-center">Item Pustaka</span>
          </a>
        </div>
      </div>
    </div>

  </div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">

    <table class="table table-hover">
      <thead>
        <tr>
          <th>Judul</th>
          <th class="text-center">Tanggal</th>
          <th class="text-center"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="3"><a href="#"><i class="fas fa-level-up-alt"></i> ..</a></td>
        </tr>
        <tr>
          <td><a href="#"><i class="fas fa-folder"></i> Fugiat dolorum eligendi magni modi.</a></td>
          <td class="text-center">28 Februari 2018</td>
          <td class="text-center"></td>
        </tr>
        <tr>
          <td><a href="#"><i class="fas fa-file-pdf"></i> Qui assumenda eos ab sint. Cumque, facilis, quos!</a></td>
          <td class="text-center">28 Februari 2018</td>
          <td class="text-center"><a href="#" class="btn btn-link download"></a></td>
        </tr>
        <tr>
          <td><a href="#"><i class="fas fa-file-pdf"></i> Nemo deleniti nostrum laborum. Nam iusto, illo veniam libero voluptatum.</a></td>
          <td class="text-center">28 Februari 2018</td>
          <td class="text-center"><a href="#" class="btn btn-link download"></a></td>
        </tr>
      </tbody>
    </table>

  </div>
</div>
@endsection