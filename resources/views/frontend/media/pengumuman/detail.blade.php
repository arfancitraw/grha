@extends('layouts.frontend-scaffold-sidebar')

@section('jumbotron')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>
@endsection

@section('content-sidebar')
<div class="p-3 pt-4">
  <h2>Media</h2>
  {{-- <p>Doloremque deleniti! Reprehenderit perspiciatis facilis fugiat, fuga eaque quisquam, harum.</p> --}}
</div>
<div class="menu">
  <a class="active" href="{{ url('media/pengumuman') }}">Pengumuman</a>
  <a href="{{ url('media/kegiatan') }}">Kegiatan</a>
  <a href="{{ url('media/pustaka') }}"">Pustaka Kesehatan</a>
</div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Media</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pengumuman</li>
  </ol>
</nav>
@endsection

@section('content-main')
<h2>{{$record->judul}}</h2>
<span class="text-muted">Tanggal: {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $record->created_at)->format('d-F-Y') }}</span> | <a href="{{asset('storage') }}/{{ $record->lampiran }}" class="btn btn-xs btn-info "><i class="fa fa-file-pdf"></i> Unduh PDF</a>

<p>{!! $record->konten !!}</p>

{{-- <a href="#" class="btn btn-xs btn-info"><i class="fa fa-file-pdf"></i> Unduh PDF</a> --}}
@endsection