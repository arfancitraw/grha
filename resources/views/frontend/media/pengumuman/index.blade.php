@extends('layouts.frontend-scaffold-sidebar')

@section('jumbotron')
<div class="jumbotron">
  <img src="{{ url('storage/'.$settings['jumbotron-media']['isi']) }}" width="100%"></div>
</div>
@endsection

@section('content-sidebar')
<div class="p-3 pt-4">
  <h2>Media</h2>
  {{-- <p>Doloremque deleniti! Reprehenderit perspiciatis facilis fugiat, fuga eaque quisquam, harum.</p> --}}
</div>
<div class="menu">
  <a class="active" href="{{ url('media/pengumuman') }}">Pengumuman</a>
  <a href="{{ url('media/kegiatan') }}">Kegiatan</a>
  <a href="{{ url('media/pustaka') }}"">Pustaka Kesehatan</a>
</div>
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">PJNHK</a></li>
    <li class="breadcrumb-item"><a href="#">Media</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pengumuman</li>
  </ol>
</nav>
@endsection

@section('content-main')
<h2>Pengumuman</h2>

<table class="table table-hover">
  <thead>
    <tr>
      <th>Judul</th>
      <th class="text-center">Tanggal</th>
      <th class="text-center"></th>
    </tr>
  </thead>
  <tbody>
    @if(isset($record))
    @foreach($record as $item)
    <tr>
      <td><a href="{{ url('media/pengumuman/detail/'.$item->id) }}">{{ $item->judul }}</a></td>
      <td class="text-center">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-F-Y') }}</td>
      <td class="text-center"><a href="{{asset('storage') }}/{{ $item->lampiran }}" class="btn btn-link download"></a></td>
    </tr>
    @endforeach
    @endif
  </tbody>
  <tfoot>
    <tr>
      <td colspan="3" class="justify-content-end">{{ $record->render() }}</td>
    </tr>
  </tfoot>
</table>
@endsection