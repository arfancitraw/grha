@extends('layouts.auth')

@section('content')
<div class="form-bottom">
	<div class="">
		<div class="wrapper text-center">
			<h4>LOGIN</h4>
		</div>
		@if (count($errors) > 0)
		<div class="alert alert-warning alert-dismissible fade show" role="alert">
			<strong>Mohon Maaf, </strong>Terjadi Kesalahan<br>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		<form role="form" method="POST" action="{{ route('password.request') }}" class="login-form clearfix">
			{!! csrf_field() !!}
			<input type="hidden" name="token" value="{{ $token }}">
			<div class="form-group">
				<input type="email" name="email" placeholder="Email..." class="form-username form-control no-border" id="form-username" value="" pattern=".{2,40}" required title="2 to 40 characters" maxlength="40" value="{{ old('email') }}">
			</div>
			<div class="form-group">
				<input type="password" name="password" placeholder="Kata Sandi baru..." class="form-password form-control no-border" id="form-password" pattern=".{2,30}" required title="2 to 30 characters" maxlength="30">
			</div>
			<div class="form-group">
				<input type="password" name="password_confirmation" placeholder="Konfirmasi Kata Sandi..." class="form-password form-control no-border" id="form-password" pattern=".{2,30}" required title="2 to 30 characters" maxlength="30">
			</div>
			<button type="submit" class="btn btn-lg btn-primary btn-block">Reset Password</button>
		</form>
	</div>
</div>
@endsection

@section('js')
@endsection