@extends('layouts.auth')

@section('content')
<div class="form-bottom">
    <div class="">
        <div class="wrapper text-center">
            <h4>LOGIN</h4>
        </div>
        @if (count($errors) > 0)
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Mohon Maaf, </strong>Terjadi Kesalahan<br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <form role="form" method="POST" action="{{ route('password.email') }}" class="login-form clearfix">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="email" name="email" placeholder="Email..." class="form-username form-control no-border" id="form-username" value="" pattern=".{2,40}" required title="2 to 40 characters" maxlength="40" value="{{ old('email') }}">
            </div>
            <button type="submit" class="btn btn-lg btn-primary btn-block">Kirim Link Reset Password</button>

            <div class="text-center m-t m-b"><a href="{{ url('login') }}" class="forget margin">Login</a></div>
        </form>
    </div>
</div>
@endsection

@section('js')
@endsection