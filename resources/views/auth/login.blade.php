@extends('layouts.auth')

@section('content')

<div class="form-bottom">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        <span>{{ $error }}</span>
        @endforeach
    </div>
    @endif
    <form role="form" action="{{ url('/login') }}" method="post" class="login-form clearfix">
        {!! csrf_field() !!}

        <div class="input-group mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text"><img src="{{ asset('img/icon-user.png') }}" alt="icon"></span>
            </div>
            <input type="text" name="email" placeholder="Email..." class="form-username form-control no-border" id="form-username" value="" pattern=".{2,40}" required title="2 to 40 characters" maxlength="40">
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><img src="{{ asset('img/icon-password.png') }}" alt="icon"></span>
            </div>
            <input type="password" name="password" placeholder="Kata Sandi..." class="form-password form-control no-border" id="form-password" pattern=".{2,30}" required title="2 to 30 characters" maxlength="30">
        </div>

        <div class="row login-tools">
            <div class="col-lg-6">
                <label class="i-checks">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="form-remember">
                        <label class="custom-control-label" for="form-remember">Ingat saya</label>
                    </div>
                </label>
            </div>
            <div class="col-lg-6">
                <a href="{{ url('password/reset') }}">Lupa password?</a>
            </div>
        </div>
        <button type="submit" class="btn btn-lg btn-primary btn-block mt-2 mt-md-4">LOGIN</button>
        <div class="text-center m-t m-b"><a href="{{url('password/reset')}}" class="forget margin"></a></div>
    </form>
</div>
@endsection
