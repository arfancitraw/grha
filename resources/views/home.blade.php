@extends('layouts.grid')

@section('js')
<script src="{{ asset('plugins/highchart/highcharts.js') }}" type="text/javascript"></script>
@append

@section('styles')
    <style type="text/css">
        .ui.divided.accordion > .title{
            border-top: 1px solid rgba(0,0,0,.1);
            clear: both;
            position: relative;
        }
        .ui.divided.accordion > .title:first-child{
            border-top: none;
        }
        .ui.divided.accordion > .title .icon{
            float: left;
            height: 100%;
            line-height: 20px;
        }
        .ui.divided.accordion > .title span{
            float: left;
            width: 95%;
        }
        .clear{
            clear: both;
        }
    </style>
@append

@section('scripts')
    <script>
        $('.dashboard-helper').slick({
            slidesToShow: 4,
            slidesToScroll: 1
        }); 
    </script>
@append

@section('content-body')
    <div class="dashboard-helper" style="margin-bottom: 15px; margin-top: -20px">
        <div class="ui blue inverted segment center aligned">
            <div class="ui statistics">
                <div class="statistic">
                    <div class="value">
                        <i class="user icon"></i> {{ $counts['perjanjian'] }}
                    </div>
                    <div class="label">
                        Perjanjian Baru
                    </div>
                </div>
            </div>
        </div>
        <div class="ui teal inverted segment center aligned">
            <div class="ui statistics">
                <div class="statistic">
                    <div class="value">
                        <i class="circle question icon"></i> {{ $counts['tanya-jawab'] }}
                    </div>
                    <div class="label">
                        Tanya Jawab Baru
                    </div>
                </div>
            </div>
        </div>
        <div class="ui orange inverted segment center aligned">
            <div class="ui statistics">
                <div class="statistic">
                    <div class="value">
                        <i class="pie chart icon"></i> {{ $counts['kuesioner'] }}
                    </div>
                    <div class="label">
                        Responden Kuesioner Baru
                    </div>
                </div>
            </div>
        </div>
        <div class="ui red inverted segment center aligned">
            <div class="ui statistics">
                <div class="statistic">
                    <div class="value">
                        <i class="law icon"></i> {{ $counts['pengaduan'] }}
                    </div>
                    <div class="label">
                        Pengaduan Baru
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ui grid">
        <div class="eight wide column">
            <div id="pertanyaanWidget" class="ui segments">
                <h4 class="ui top attached header segment">
                    5 Pertanyaan Terbaru
                </h4>
                <div class="ui attached segment" style="padding: 0;">
                    <table class="ui celled basic small compact table" style="border-radius: 0; border:none">
                        <thead>
                            <tr>
                                <th class="center aligned">#</th>
                                <th class="center aligned">Tanggal</th>
                                <th class="center aligned">Kategori</th>
                                <th class="center aligned">Nama</th>
                                <th class="center aligned">Judul</th>
                                <th class="center aligned">Pertanyan</th>
                                <th class="center aligned">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!is_null($tanyajawab))
                                @foreach($tanyajawab->take(5) as $i => $row)
                                <tr>
                                    <td class="center aligned">{{ $i+1 }}</td>
                                    <td class="center aligned">{{ $row->created_at->format('d/m') }}</td>
                                    <td>{{ $row->kategori->nama }}</td>
                                    <td>{{ $row->rahasia ? 'Dirahasiakan' : $row->nama }}</td>
                                    <td style="word-wrap: nowrap">{{ $row->judul }}</td>
                                    <td>{{ str_limit($row->konten, 20) }}</td>
                                    <td class="center aligned">
                                        <a href="{{ url("backend/pertanyaan/{$row->id}/detail") }}" class="ui teal mini icon button" data-content="Detail Tanya Jawab"><i class="file text icon"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7" class="center aligned"><i>Belum ada Pertanyaan</i></td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="ui bottom attached center aligned segment" style="padding: 8px 0">
                    <a href="{{ url('backend/pertanyaan') }}">Selengkapnya <i class="right angle icon"></i></a>
                </div>
            </div>
            <div id="pengaduanWidget" class="ui segments">
                <h4 class="ui top attached header segment">
                    5 Pengaduan Terbaru
                </h4>
                <div class="ui attached segment" style="padding: 0;">
                    <table class="ui celled basic small compact table" style="border-radius: 0; border:none">
                        <thead>
                            <tr>
                                <th class="center aligned">#</th>
                                <th class="center aligned">Tanggal</th>
                                <th class="center aligned">Kategori</th>
                                <th class="center aligned">Nama</th>
                                <th class="center aligned">Detail</th>
                                <th class="center aligned">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!is_null($pengaduan))
                                @php 
                                    $i=1; 
                                    $tipe = ['Etik & Hukum', 'Pengaduan Masyarakat', 'Whistleblowing'];
                                    $tipeUrl = ['etik-hukum', 'masyarakat', 'whistleblowing'];
                                @endphp
                                @foreach($pengaduan->take(5) as $row)
                                <tr>
                                    <td class="center aligned">{{ $i++ }}</td>
                                    <td class="center aligned">{{ $row->created_at->format('d/m') }}</td>
                                    <td>{{ ($row->kronologi) ? 'Gratifikasi' : ($row->nama_kegiatan ? 'Sponsor' : $tipe[$row->tipe]) }}</td>
                                    <td>{{ $row->nama }}</td>
                                    <td style="word-wrap: nowrap">
                                        @if($row->kronologi)
                                            Kronologi: {{ str_limit($row->kronologi, 20) }}
                                        @elseif($row->nama_kegiatan)
                                            Kegiatan: {{ str_limit($row->nama_kegiatan, 20) }}
                                        @else
                                            Perihal: {{ $row->perihal }}
                                        @endif
                                    </td>
                                    <td class="center aligned">
                                        @php
                                            $purl = ($row->kronologi) ? 'gratifikasi' : ($row->nama_kegiatan ? 'sponsor' : $tipeUrl[$row->tipe])
                                        @endphp
                                        <a href="{{ url("backend/pengaduan/{$purl}/{$row->id}") }}" class="ui teal mini icon button" data-content="Detail Pengaduan"><i class="file text icon"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7" class="center aligned"><i>Belum ada Pertanyaan</i></td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="ui bottom attached center aligned segment">
                    <div class="ui equal width grid">
                        <div class="column"><a href="{{ url('backend/pengaduan/etik-hukum') }}">Etik & Hukum<i class="right angle icon"></i></a></div>
                        <div class="column"><a href="{{ url('backend/pengaduan/masyarakat') }}">Masyarakat<i class="right angle icon"></i></a></div>
                        <div class="column"><a href="{{ url('backend/pengaduan/whistleblowing') }}">WBS<i class="right angle icon"></i></a></div>
                        <div class="column"><a href="{{ url('backend/pengaduan/gratifikasi') }}">Gratifikasi<i class="right angle icon"></i></a></div>
                        <div class="column"><a href="{{ url('backend/pengaduan/sponsor') }}">Sponsor<i class="right angle icon"></i></a></div>
                    </div>
                </div>
            </div>
            <div id="kunjunganWidget" class="ui segments">
                <h4 class="ui top attached header segment" style="line-height: 25px">
                    <!-- Histats.com  (div with counter) --><div id="histats_counter" style="height: 25px; float: right;"></div>
                    <!-- Histats.com  START  (aync)-->
                    <script type="text/javascript">var _Hasync= _Hasync|| [];
                    _Hasync.push(['Histats.start', '1,4078325,4,1034,150,25,00011111']);
                    _Hasync.push(['Histats.fasi', '1']);
                    _Hasync.push(['Histats.track_hits', '']);
                    (function() {
                      var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
                      hs.src = ('//s10.histats.com/js15_as.js');
                      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
                    })();</script>
                    <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4078325&101" alt="site hit counter" border="0"></a></noscript>
                    <!-- Histats.com  END  -->
                    Klik Widget untuk melihat detail kunjungan website
                </h4>
            </div>
        </div>
        <div class="eight wide column">
            <div id="kuesionerWidget" class="ui segments">
                <h5 class="ui top attached header">
                    Grafik Kuesioner
                </h5>
                <div class="ui bottom attached segment">
                    <script src="{{ asset('plugins/highchart/highcharts.js') }}" type="text/javascript"></script>
                    <div class="ui divided fluid accordion">
                        @if($kuesioner )
                        @foreach($kuesioner->detail as $i => $detail)
                            <div class="title @if($i==0) active @endif">
                                <i class="dropdown icon"></i>
                                <span>{{ $detail->pertanyaan }}</span>
                                <div class="clear"></div>
                            </div>
                            <div class="content @if($i==0) active @endif">
                                <div class="transition">
                                    @if(count($detail->jawaban) == 0)
                                    <h4 style="color: #888"><i>Pertanyaan terkait belum memiliki Jawaban</i></h4>
                                    @else
                                        @if($detail->tipe)
                                            <table class="ui stripped small compact table">
                                                <thead><tr>
                                                    <th>No.</th>
                                                    <th>Nama</th>
                                                    <th>Email</th>
                                                    <th>Jawaban</th>
                                                </tr></thead>
                                                <tbody>
                                                    @php $i=1; @endphp
                                                    @foreach($detail->jawaban as $jawaban)
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $jawaban->kuesionerjawaban->nama }}</td>
                                                        <td>{{ $jawaban->kuesionerjawaban->email }}</td>
                                                        <td>{{ $jawaban->jawaban }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            @php
                                                $data = $detail->jawaban->groupBy('jawaban');
                                            @endphp
                                            <div id="container{{$detail->id}}" style="min-width: 310px; height: 300px; max-width: 600px; margin: 0 auto"></div>
                                            <script>                            
                                                Highcharts.chart('container{{$detail->id}}', {
                                                    chart: {
                                                        plotBackgroundColor: null,
                                                        plotBorderWidth: null,
                                                        plotShadow: false,
                                                        type: 'pie'
                                                    },
                                                    credits: {
                                                        enabled: false
                                                    },
                                                    title: {
                                                        text: null
                                                    },
                                                    tooltip: {
                                                        pointFormat: '{series.name}: <b>{point.percentage:.1f}% ({point.y} dari {point.total} Tanggapan)</b>'
                                                    },
                                                    plotOptions: {
                                                        pie: {
                                                            allowPointSelect: true,
                                                            cursor: 'pointer',
                                                            dataLabels: {
                                                                enabled: true,
                                                                format: '<b>{point.name}</b>: {point.percentage:.1f}% <br/>({point.y} dari {point.total} Tanggapan)',
                                                                style: {
                                                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                                }
                                                            }
                                                        }
                                                    },
                                                    series: [{
                                                        name: 'Jawaban',
                                                        colorByPoint: true,
                                                        data: [
                                                            @foreach($data as $label => $point)
                                                            {
                                                                name: '{{ $label }} Bintang',
                                                                y: {{ count($point) }}
                                                            },
                                                            @endforeach
                                                        ]
                                                    }]
                                                });
                                            </script>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection