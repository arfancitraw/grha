// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 1;
var navbarHeight = $('header').outerHeight();
var st = $(this).scrollTop();


function isHome(){
	if($('body').hasClass('home')){
		if ($(this).scrollTop() > $(window).height() - 56){
			$('header .navbar').css({
				'background-color': '#fff'})
			$('header .navbar li a').css({
				'color': '#444'})
			$('header .navbar-brand').css({
				'color': '#444'})
		}else{
			$('header .navbar').css({
				'background-color': 'transparent'})
			$('header .navbar li a').css({
				'color': '#fff'})
			$('header .navbar-brand').css({
				'color': '#fff'})
		}
	}
}

function hasScrolled() {
	var st = $(this).scrollTop();

 	if(lastScrollTop > 56) {
 		$('header .header-spacing').css({
 			'padding-top': '.25rem',
 			'padding-bottom': '.25rem'
 		})
 		$('header .navbar .navbar-brand img').css({
 			'filter': 'none'
 		})
 		$('header .navbar').css({
 			'background-color': '#3b9ca5',
 			'padding-top': 0,
 			'padding-bottom': 0,
 			'top': '3.25rem'
 		})
 	}else{
 		$('header .header-spacing').css({
 			'padding-top': '.5rem',
 			'padding-bottom': '.5rem'
 		})
 		$('header .navbar .navbar-brand img').css({
 			'filter': 'brightness(0) invert(1)'
 		})
 		$('header .navbar').css({
 			'background-color': 'transparent',
 			'padding-top': '1rem',
 			'padding-bottom': '1rem',
 			'top': '3.75rem'
 		})
 	}

    lastScrollTop = st;
}

function scrollToSegment(segment){
	$('html, body').animate({
        // scrollTop: $(segment).offset().top - 98
        scrollTop: $(segment).offset().top - 80
    }, 1000);
}

$(" header a.nav-link[href^='#']").on('click', function(e) {
	e.preventDefault();

	// var hash = this.hash;
	scrollToSegment(this.hash)
});

$(window).scroll(function(event){
	// didScroll = true;
	hasScrolled();
});

$(window).load(function() {
	hasScrolled();

	$('html, body').animate({
        // scrollTop: $(segment).offset().top - 98
        scrollTop: $('body').offset().top
  	}, 100);

	$('#loader img').fadeOut(500, function(){
		$(this).css('visibility', 'hidden')
	});
	setTimeout(function(){
		$('#loader').fadeOut(500);
	}, 500)

	new WOW().init();

	$('.fasilitas-item').magnificPopup({
		type: 'image',
		mainClass: 'mfp-with-zoom',

		zoom: {
			enabled: true, 
			duration: 300, 
			easing: 'ease-in-out', 
			opener: function(openerElement) {
				return openerElement.is('img') ? openerElement : openerElement.find('img');
			}
		}
	});
});

$(document).on('click', '.btn-janji', function($q){
	var container = $('#janjiModal');
	var dokter = $(this).data('dokter');
	var poli = $(this).data('poli');
	var hari = $(this).data('hari');
	container.find('[name=kd_dr]').val(dokter);
	container.find('[name=kd_dep]').val(poli);
	container.find('[name=hari]').val(hari);
	container.find('#modalJanjiTitle').html('Buat Perjanjian');
	container.find('#modalJanjiSubmit').html('Buat Perjanjian');

	container.modal('show');
});