<!DOCTYPE html>
<html lang="en" dir="ltr">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="noindex">
<meta name="referrer" content="origin-when-crossorigin">
<title>Export: travel - Adminer</title>
<link rel="stylesheet" type="text/css" href="?file=default.css&amp;version=4.3.1">
<script type="text/javascript" src="?file=functions.js&amp;version=4.3.1"></script>
<link rel="shortcut icon" type="image/x-icon" href="?file=favicon.ico&amp;version=4.3.1">
<link rel="apple-touch-icon" href="?file=favicon.ico&amp;version=4.3.1">
<link rel="stylesheet" type="text/css" href="adminer.css">

<body class="ltr nojs" onkeydown="bodyKeydown(event);" onclick="bodyClick(event);">
<script type="text/javascript">
document.body.className = document.body.className.replace(/ nojs/, ' js');
var offlineMessage = 'You are offline.';
</script>

<div id="help" class="jush-sql jsonly hidden" onmouseover="helpOpen = 1;" onmouseout="helpMouseout(this, event);"></div>

<div id="content">
<p id="breadcrumb"><a href=".">MySQL</a> &raquo; <a href='?username=root' accesskey='1' title='Alt+Shift+1'>Server</a> &raquo; <a href="?username=root&amp;db=travel">travel</a> &raquo; Export
<h2>Export: travel</h2>
<div id='ajaxstatus' class='jsonly hidden'></div>

<form action="" method="post">
<table cellspacing="0">
<tr><th>Output<td><label><input type='radio' name='output' value='text' checked>open</label><label><input type='radio' name='output' value='file'>save</label><label><input type='radio' name='output' value='gz'>gzip</label>
<tr><th>Format<td><label><input type='radio' name='format' value='sql' checked>SQL</label><label><input type='radio' name='format' value='csv'>CSV,</label><label><input type='radio' name='format' value='csv;'>CSV;</label><label><input type='radio' name='format' value='tsv'>TSV</label>
<tr><th>Database<td><select name='db_style'><option selected><option>USE<option>DROP+CREATE<option>CREATE</select><label><input type='checkbox' name='routines' value='1' checked>Routines</label><label><input type='checkbox' name='events' value='1' checked>Events</label><tr><th>Tables<td><select name='table_style'><option><option selected>DROP+CREATE<option>CREATE</select><label><input type='checkbox' name='auto_increment' value='1'>Auto Increment</label><label><input type='checkbox' name='triggers' value='1' checked>Triggers</label><tr><th>Data<td><select name='data_style'><option><option>TRUNCATE+INSERT<option selected>INSERT<option>INSERT+UPDATE</select></table>
<p><input type="submit" value="Export">
<input type="hidden" name="token" value="218251:175751">

<table cellspacing="0">
<thead><tr><th style='text-align: left;'><label class='block'><input type='checkbox' id='check-tables' checked onclick='formCheck(this, /^tables\[/);'>Tables</label><th style='text-align: right;'><label class='block'>Data<input type='checkbox' id='check-data' checked onclick='formCheck(this, /^data\[/);'></label></thead>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='indonesia_cities' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">indonesia_cities</label><td align='right'><label class='block'><span id='Rows-indonesia_cities'></span><input type='checkbox' name='data[]' value='indonesia_cities' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='indonesia_districts' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">indonesia_districts</label><td align='right'><label class='block'><span id='Rows-indonesia_districts'></span><input type='checkbox' name='data[]' value='indonesia_districts' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='indonesia_provinces' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">indonesia_provinces</label><td align='right'><label class='block'><span id='Rows-indonesia_provinces'></span><input type='checkbox' name='data[]' value='indonesia_provinces' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='indonesia_villages' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">indonesia_villages</label><td align='right'><label class='block'><span id='Rows-indonesia_villages'></span><input type='checkbox' name='data[]' value='indonesia_villages' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='oauth_access_tokens' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">oauth_access_tokens</label><td align='right'><label class='block'><span id='Rows-oauth_access_tokens'></span><input type='checkbox' name='data[]' value='oauth_access_tokens' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='oauth_auth_codes' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">oauth_auth_codes</label><td align='right'><label class='block'><span id='Rows-oauth_auth_codes'></span><input type='checkbox' name='data[]' value='oauth_auth_codes' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='oauth_clients' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">oauth_clients</label><td align='right'><label class='block'><span id='Rows-oauth_clients'></span><input type='checkbox' name='data[]' value='oauth_clients' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='oauth_personal_access_clients' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">oauth_personal_access_clients</label><td align='right'><label class='block'><span id='Rows-oauth_personal_access_clients'></span><input type='checkbox' name='data[]' value='oauth_personal_access_clients' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='oauth_refresh_tokens' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">oauth_refresh_tokens</label><td align='right'><label class='block'><span id='Rows-oauth_refresh_tokens'></span><input type='checkbox' name='data[]' value='oauth_refresh_tokens' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='ref_keberangkatan' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">ref_keberangkatan</label><td align='right'><label class='block'><span id='Rows-ref_keberangkatan'></span><input type='checkbox' name='data[]' value='ref_keberangkatan' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='ref_peserta' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">ref_peserta</label><td align='right'><label class='block'><span id='Rows-ref_peserta'></span><input type='checkbox' name='data[]' value='ref_peserta' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='sys_migrations' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">sys_migrations</label><td align='right'><label class='block'><span id='Rows-sys_migrations'></span><input type='checkbox' name='data[]' value='sys_migrations' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='sys_password_resets' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">sys_password_resets</label><td align='right'><label class='block'><span id='Rows-sys_password_resets'></span><input type='checkbox' name='data[]' value='sys_password_resets' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='sys_users' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">sys_users</label><td align='right'><label class='block'><span id='Rows-sys_users'></span><input type='checkbox' name='data[]' value='sys_users' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='trans_paket' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">trans_paket</label><td align='right'><label class='block'><span id='Rows-trans_paket'></span><input type='checkbox' name='data[]' value='trans_paket' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='trans_paket_detail' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">trans_paket_detail</label><td align='right'><label class='block'><span id='Rows-trans_paket_detail'></span><input type='checkbox' name='data[]' value='trans_paket_detail' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<tr><td><label class='block'><input type='checkbox' name='tables[]' value='trans_travel' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-tables&#039;);">trans_travel</label><td align='right'><label class='block'><span id='Rows-trans_travel'></span><input type='checkbox' name='data[]' value='trans_travel' checked onclick="checkboxClick(event, this); formUncheck(&#039;check-data&#039;);"></label>
<script type='text/javascript'>ajaxSetHtml('?username=root&db=travel&script=db');</script>
</table>
</form>
<p><a href='?username=root&amp;db=travel&amp;dump=indonesia%25'>indonesia</a> <a href='?username=root&amp;db=travel&amp;dump=oauth%25'>oauth</a> <a href='?username=root&amp;db=travel&amp;dump=ref%25'>ref</a> <a href='?username=root&amp;db=travel&amp;dump=sys%25'>sys</a> <a href='?username=root&amp;db=travel&amp;dump=trans%25'>trans</a></div>

<form action="" method="post">
<p class="logout">
<input type="submit" name="logout" value="Logout" id="logout">
<input type="hidden" name="token" value="218251:175751">
</p>
</form>
<div id="menu">
<h1>
<a href='https://www.adminer.org/' target='_blank' id='h1'>Adminer</a> <span class="version">4.3.1</span>
<a href="https://www.adminer.org/#download" target="_blank" id="version">4.6.2</a>
</h1>
<script type="text/javascript" src="?file=jush.js&amp;version=4.3.1"></script>
<script type="text/javascript">
var jushLinks = { sql: [ '?username=root&db=travel&table=$&', /\b(indonesia_cities|indonesia_districts|indonesia_provinces|indonesia_villages|oauth_access_tokens|oauth_auth_codes|oauth_clients|oauth_personal_access_clients|oauth_refresh_tokens|ref_keberangkatan|ref_peserta|sys_migrations|sys_password_resets|sys_users|trans_paket|trans_paket_detail|trans_travel)\b/g ] };
jushLinks.bac = jushLinks.sql;
jushLinks.bra = jushLinks.sql;
jushLinks.sqlite_quo = jushLinks.sql;
jushLinks.mssql_bra = jushLinks.sql;
bodyLoad('5.7');
</script>
<form action="">
<p id="dbs">
<input type="hidden" name="username" value="root"><span title='database'>DB</span>: <select name='db' onmousedown='dbMouseDown(event, this);' onchange='dbChange(this);'><option value=""><option>information_schema<option>a<option>apd-riau<option>kecamatan<option>kubikel<option>margie<option>mysql<option>nindya<option>onlineshop<option>padangbersama<option>performance_schema<option>pettycash<option>shop<option>singleline<option>sld<option>sys<option>tracker<option selected>travel</select><input type='submit' value='Use' class='hidden'>
<input type="hidden" name="dump" value=""></p></form>
<p class='links'><a href='?username=root&amp;db=travel&amp;sql='>SQL command</a>
<a href='?username=root&amp;db=travel&amp;import='>Import</a>
<a href='?username=root&amp;db=travel&amp;dump=' id='dump' class='active '>Export</a>
<a href="?username=root&amp;db=travel&amp;create=">Create table</a>
<ul id='tables' onmouseover='menuOver(this, event);' onmouseout='menuOut(this);'>
<li><a href="?username=root&amp;db=travel&amp;select=indonesia_cities" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=indonesia_cities" class='structure' title='Show structure'>indonesia_cities</a>
<li><a href="?username=root&amp;db=travel&amp;select=indonesia_districts" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=indonesia_districts" class='structure' title='Show structure'>indonesia_districts</a>
<li><a href="?username=root&amp;db=travel&amp;select=indonesia_provinces" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=indonesia_provinces" class='structure' title='Show structure'>indonesia_provinces</a>
<li><a href="?username=root&amp;db=travel&amp;select=indonesia_villages" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=indonesia_villages" class='structure' title='Show structure'>indonesia_villages</a>
<li><a href="?username=root&amp;db=travel&amp;select=oauth_access_tokens" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=oauth_access_tokens" class='structure' title='Show structure'>oauth_access_tokens</a>
<li><a href="?username=root&amp;db=travel&amp;select=oauth_auth_codes" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=oauth_auth_codes" class='structure' title='Show structure'>oauth_auth_codes</a>
<li><a href="?username=root&amp;db=travel&amp;select=oauth_clients" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=oauth_clients" class='structure' title='Show structure'>oauth_clients</a>
<li><a href="?username=root&amp;db=travel&amp;select=oauth_personal_access_clients" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=oauth_personal_access_clients" class='structure' title='Show structure'>oauth_personal_access_clients</a>
<li><a href="?username=root&amp;db=travel&amp;select=oauth_refresh_tokens" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=oauth_refresh_tokens" class='structure' title='Show structure'>oauth_refresh_tokens</a>
<li><a href="?username=root&amp;db=travel&amp;select=ref_keberangkatan" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=ref_keberangkatan" class='structure' title='Show structure'>ref_keberangkatan</a>
<li><a href="?username=root&amp;db=travel&amp;select=ref_peserta" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=ref_peserta" class='structure' title='Show structure'>ref_peserta</a>
<li><a href="?username=root&amp;db=travel&amp;select=sys_migrations" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=sys_migrations" class='structure' title='Show structure'>sys_migrations</a>
<li><a href="?username=root&amp;db=travel&amp;select=sys_password_resets" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=sys_password_resets" class='structure' title='Show structure'>sys_password_resets</a>
<li><a href="?username=root&amp;db=travel&amp;select=sys_users" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=sys_users" class='structure' title='Show structure'>sys_users</a>
<li><a href="?username=root&amp;db=travel&amp;select=trans_paket" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=trans_paket" class='structure' title='Show structure'>trans_paket</a>
<li><a href="?username=root&amp;db=travel&amp;select=trans_paket_detail" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=trans_paket_detail" class='structure' title='Show structure'>trans_paket_detail</a>
<li><a href="?username=root&amp;db=travel&amp;select=trans_travel" class='select'>select</a> <a href="?username=root&amp;db=travel&amp;table=trans_travel" class='structure' title='Show structure'>trans_travel</a>
</ul>
</div>
<script type="text/javascript">setupSubmitHighlight(document);</script>
