// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 1;
var navbarHeight = $('header').outerHeight();

$(window).scroll(function(event){
	didScroll = true;

	var st = $(this).scrollTop(),
	    bh = $('body').height(),
	    wh = $(this).height(),
	    an = $('.real-footer').height() + 14;
	if(st > bh - wh - an) {
		$('.testimoni-bottom').css('bottom', an)
	}else{
		$('.testimoni-bottom').css('bottom', 0)
	}
});

$(window).load(function() {
	isHome()
	setInterval(function() {
		if (didScroll) {
			hasScrolled();
			didScroll = false;
		}
	}, 250);

	function isHome(){
		if($('body').hasClass('home')){
			if ($(this).scrollTop() > $(window).height() - 56){
				$('header .navbar').css({
					'background-color': '#fff'})
				$('header .navbar li a').css({
					'color': '#444'})
				$('header .navbar-brand').css({
					'color': '#444'})
			}else{
				$('header .navbar').css({
					'background-color': 'transparent'})
				$('header .navbar li a').css({
					'color': '#fff'})
				$('header .navbar-brand').css({
					'color': '#fff'})
			}
		}
	}

	function hasScrolled() {
		var st = $(this).scrollTop();

		if(Math.abs(lastScrollTop - st) <= delta)
			return;
		if (st > lastScrollTop && st > navbarHeight){
			$('header').removeClass('nav-down').addClass('nav-up');
		} else {
			if(st + $(window).height() < $(document).height()) {
				$('header').removeClass('nav-up').addClass('nav-down');
			}
		}
		lastScrollTop = st;
		isHome()
	}	
});

$(document).ready(function() {

	$('[data-toggle="tooltip"]').tooltip()

	$('.navbar .dropdown').on('show.bs.dropdown', function() {
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown(250);
	});

	$('.navbar .dropdown').on('hide.bs.dropdown', function() {
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp(250);
	});

	$('.drawer .title').click(function(event) {
		event.preventDefault();
		if($(this).parent().hasClass('show')){
			$(this).parent().removeClass('show');
		}else{
			$(this).parent().parent().find('.drawer').removeClass('show');
			$(this).parent().toggleClass('show');
		}
	});

	$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
		if (!$(this).next().hasClass('show')) {
			$(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
		}
		var $subMenu = $(this).next(".dropdown-menu");
		$subMenu.toggleClass('show');

		$(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
			$('.dropdown-submenu .show').removeClass("show");
		});
		return false;
	});

	$('.select2').select2({ width: '100%' });

	$('.date').daterangepicker({
		singleDatePicker: true,
		timePicker: false,
		autoUpdateInput: false,
	});
	$('.datetime').daterangepicker({
		singleDatePicker: true,
		timePicker: true,
		autoUpdateInput: false,
		timePicker24Hour: true,
		timePickerIncrement: 15,
		locale: {
			format: 'DD/MM/YYYY HH:mm'
		}
	});
	$('.date, .datetime').on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('MM/DD/YYYY'));
	});

	$('.date, .datetime').on('cancel.daterangepicker', function(ev, picker) {
		$(this).val('');
	});

	$('.cari-dokter .btn-sidebar').click(function (e) {
		$(this).toggleClass('active');
		if ($(this).hasClass('active')) {
			$('.cari-dokter').css('bottom', '0');
		} else {
			$('.cari-dokter').css('bottom', '-250px');
		}
	});


	$('.kuesioner fieldset:first').fadeIn('fast');
	$('.kuesioner input[type="text"], .kuesioner input[type="password"], .kuesioner textarea').on('focus', function() {
		$(this).removeClass('input-error');
	});

    // next step
    $('.kuesioner .btn-next').on('click', function() {
    	var parent_fieldset = $(this).parents('fieldset');
    	var next_step = true;
    	// navigation steps / progress steps
    	var current_active_step = $(this).parents('.kuesioner').find('.kuesioner-step.active');
    	var progress_line = $(this).parents('.kuesioner').find('.kuesioner-progress-line');
    	
    	// fields validation
    	parent_fieldset.find('input[type="text"], input[type="password"], input[type="email"], textarea').each(function() {
    		if( $(this).val() == "" ) {
    			$(this).addClass('input-error');
    			next_step = false;
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	// fields validation
    	
    	if( next_step ) {
    		parent_fieldset.fadeOut(100, function() {
    			// change icons
    			current_active_step.removeClass('active').addClass('activated').next().addClass('active');
    			// progress bar
    			bar_progress(progress_line, 'right');
    			// show next step
    			$(this).next().fadeIn();
	    		// scroll window to beginning of the form
	    		// scroll_to_class( $('.kuesioner'), 20 );
	    	});
    	}
    	
    });
    
    // previous step
    $('.kuesioner .btn-previous').on('click', function() {
    	// navigation steps / progress steps
    	var current_active_step = $(this).parents('.kuesioner').find('.kuesioner-step.active');
    	var progress_line = $(this).parents('.kuesioner').find('.kuesioner-progress-line');
    	
    	$(this).parents('fieldset').fadeOut(100, function() {
    		// change icons
    		current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
    		// progress bar
    		bar_progress(progress_line, 'left');
    		// show previous step
    		$(this).prev().fadeIn();
    		// scroll window to beginning of the form
    		// scroll_to_class( $('.kuesioner'), 20 );
    	});
    });
    
    // submit
    // $('.kuesioner').on('submit', function(e) {

    // 	console.log('nganu')

    // 	// fields validation
    // 	$(this).find('input[type="text"], input[type="password"], input[type="email"], textarea').each(function() {
    // 		if( $(this).val() == "" ) {
    // 			e.preventDefault();
    // 			$(this).addClass('input-error');
    // 		}
    // 		else {
    // 			$(this).removeClass('input-error');
    // 		}
    // 	});
    // 	// fields validation

    // });
});

$(document).on('click', '.btn-janji', function($q){
	var container = $('#janjiModal');
	var dokter = $(this).data('dokter');
	var poli = $(this).data('poli');
	var hari = $(this).data('hari');
	container.find('[name=kd_dr]').val(dokter);
	container.find('[name=kd_dep]').val(poli);
	container.find('[name=hari]').val(hari);
	container.find('#modalJanjiTitle').html('Buat Perjanjian');
	container.find('#modalJanjiSubmit').html('Buat Perjanjian');

	container.modal('show');
});

$(document).on('click', '.btn-riwayat', function($q){
	var container = $('#janjiModal');
	// var dokter = $(this).data('dokter');
	// var poli = $(this).data('poli');
	// var hari = $(this).data('hari');
	// container.find('[name=kd_dr]').val(dokter);
	// container.find('[name=kd_dep]').val(poli);
	// container.find('[name=hari]').val(hari);
	container.find('#modalJanjiTitle').html('Riwayat Perjanjian');
	container.find('#modalJanjiSubmit').html('Tampilkan Riwayat');

	container.modal('show');
});

function scroll_to_class(element_class, removed_height) {
	var scroll_to = $(element_class).offset().top - removed_height;
	if($(window).scrollTop() != scroll_to) {
		$('html, body').stop().animate({scrollTop: scroll_to}, 0);
	}
}

function bar_progress(progress_line_object, direction) {
	var number_of_steps = progress_line_object.data('number-of-steps');
	var now_value = progress_line_object.data('now-value');
	var new_value = 0;
	if(direction == 'right') {
		new_value = now_value + ( 100 / number_of_steps );
	}
	else if(direction == 'left') {
		new_value = now_value - ( 100 / number_of_steps );
	}
	progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
}
