<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRefPoliTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_poli', function (Blueprint $table) {
            $table->string('kode');
            $table->string('nama');
            $table->boolean('aktif')->default(0);
            $table->text('deskripsi')->nullable()->change();
        });
        Schema::table('log_poli', function (Blueprint $table) {
            $table->string('kode');
            $table->string('nama');
            $table->boolean('aktif')->default(0);
            $table->text('deskripsi')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_poli', function (Blueprint $table) {
            $table->dropColumn('kode');
            $table->dropColumn('nama');
            $table->dropColumn('aktif');
            $table->text('deskripsi')->change();
        });
        Schema::table('log_poli', function (Blueprint $table) {
            $table->dropColumn('kode');
            $table->dropColumn('nama');
            $table->dropColumn('aktif');
            $table->text('deskripsi')->change();
        });
    }
}
