<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropThreeTableRujukan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('log_trans_rujukan_agenda');
        Schema::dropIfExists('trans_rujukan_agenda');

        Schema::dropIfExists('log_trans_rujukan_tim');
        Schema::dropIfExists('trans_rujukan_tim');

        Schema::dropIfExists('log_trans_rujukan_produk');
        Schema::dropIfExists('trans_rujukan_produk');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('trans_rujukan_agenda', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_rujukan_nasional')->references('id')->on('trans_rujukan')->onDelete('cascade');
            $table->string('nama');
            $table->mediumtext('keterangan')->nullable();
            $table->string('tahun')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
         
        Schema::create('log_trans_rujukan_agenda', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_agenda')->unsigned();
            $table->integer('id_rujukan_nasional')->references('id')->on('trans_rujukan')->onDelete('cascade');
            $table->string('nama');
            $table->mediumtext('keterangan')->nullable();
            $table->string('tahun')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_rujukan_tim', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_rujukan_nasional')->references('id')->on('trans_rujukan')->onDelete('cascade');
            $table->integer('id_dokter')->unsigned();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
         
        Schema::create('log_trans_rujukan_tim', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_tim')->unsigned();
            $table->integer('id_rujukan_nasional')->references('id')->on('trans_rujukan')->onDelete('cascade');
            $table->integer('id_dokter')->unsigned();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_rujukan_produk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_rujukan_nasional')->references('id')->on('trans_rujukan')->onDelete('cascade');
            $table->string('nama');
            $table->mediumtext('keterangan')->nullable();
            $table->string('gambar');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
         
        Schema::create('log_trans_rujukan_produk', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_produk')->unsigned();
            $table->integer('id_rujukan_nasional')->references('id')->on('trans_rujukan')->onDelete('cascade');
            $table->string('nama');
            $table->mediumtext('keterangan')->nullable();
            $table->string('gambar');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }
}
