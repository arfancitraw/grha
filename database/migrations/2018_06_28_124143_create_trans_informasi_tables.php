<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransInformasiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_informasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->string('slug');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('gambar')->nullable();
            $table->integer('posisi');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_trans_informasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_informasi');
            $table->string('judul');
            $table->string('slug');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('gambar')->nullable();
            $table->integer('posisi');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_informasi');
        Schema::dropIfExists('trans_informasi');
    }
}
