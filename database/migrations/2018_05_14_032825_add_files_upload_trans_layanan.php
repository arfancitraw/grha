<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFilesUploadTransLayanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_layanan', function (Blueprint $table) {
            $table->string('files')->nullable();
        });
        Schema::table('log_trans_layanan', function (Blueprint $table) {
            $table->string('files')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_layanan', function (Blueprint $table) {
            $table->dropColumn('files')->nullable();
        });
        Schema::table('trans_layanan', function (Blueprint $table) {
            $table->dropColumn('files')->nullable();
        });
    }
}
