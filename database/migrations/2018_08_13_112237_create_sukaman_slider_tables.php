<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSukamanSliderTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_sukaman_slider', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->string('gambar');
            $table->integer('urutan')->default(0);
            $table->boolean('status')->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_trans_sukaman_slider', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_slider')->unsigned();
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->string('gambar');
            $table->integer('urutan')->default(0);
            $table->boolean('status')->default(0);
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_sukaman_slider');
        Schema::dropIfExists('log_trans_sukaman_slider');
    }
}
