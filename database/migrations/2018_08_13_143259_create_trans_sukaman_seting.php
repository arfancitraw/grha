<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransSukamanSeting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_sukaman_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kunci');
            $table->string('judul');
            $table->text('isi')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();;
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_trans_sukaman_setting', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_setting')->unsigned();
            $table->string('judul');
            $table->text('isi')->nullable();
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();;
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_sukaman_setting');
        Schema::dropIfExists('log_trans_sukaman_setting');
    }
}
