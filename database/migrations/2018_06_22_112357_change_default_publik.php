<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultPublik extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_trans_testimoni', function(Blueprint $table){
            $table->integer('publik')->default(0)->change();
            $table->integer('rating')->default(5);
            $table->string('pekerjaan')->nullable();
        });

        Schema::table('trans_testimoni', function (Blueprint $table) {
            $table->integer('publik')->default(0)->change();
            $table->integer('rating')->default(5);
            $table->string('pekerjaan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_testimoni', function(Blueprint $table){
            $table->dropColumn('rating');
            $table->dropColumn('pekerjaan');
        });

        Schema::table('trans_testimoni', function (Blueprint $table) {
            $table->dropColumn('rating')->default(5);
            $table->dropColumn('pekerjaan');
        });
    }
}
