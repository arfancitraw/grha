<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransTambahanJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
Schema::create('trans_tambahan_tanya_jawab', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('email');
            $table->integer('id_pertanyaan')->unsigned();
            $table->string('judul');
            $table->text('konten');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_pertanyaan')->references('id')->on('trans_tanya_jawab');
        });
        Schema::create('log_trans_tambahan_tanya_jawab', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_tambahan_tanya_jawab')->unsigned();
            $table->string('nama');
            $table->string('email');
            $table->integer('id_pertanyaan')->unsigned();
            $table->string('judul');
            $table->text('konten');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_tambahan_tanya_jawab');
        Schema::dropIfExists('log_trans_tambahan_tanya_jawab');
    }
}
