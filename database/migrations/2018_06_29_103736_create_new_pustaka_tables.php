<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewPustakaTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pustaka_penyakit', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->text('pengertian')->nullable();
            $table->text('gejala')->nullable();
            $table->text('penyebab')->nullable();
            $table->text('penanganan')->nullable();
            $table->text('pencegahan')->nullable();
            $table->string('lampiran')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_trans_pustaka_penyakit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_penyakit')->unsigned();
            $table->string('nama');
            $table->text('pengertian')->nullable();
            $table->text('gejala')->nullable();
            $table->text('penyebab')->nullable();
            $table->text('penanganan')->nullable();
            $table->text('pencegahan')->nullable();
            $table->string('lampiran')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_pustaka_tindakan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->text('indikasi')->nullable();
            $table->text('tujuan')->nullable();
            $table->text('tatacara')->nullable();
            $table->text('risiko')->nullable();
            $table->text('alternatif')->nullable();
            $table->text('prognosis_dilakukan')->nullable();
            $table->text('prognosis_tidak')->nullable();
            $table->text('perluasan')->nullable();
            $table->text('persiapan')->nullable();
            $table->text('penjelasan')->nullable();
            $table->string('lampiran')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_trans_pustaka_tindakan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tindakan')->unsigned();
            $table->string('nama');
            $table->text('indikasi')->nullable();
            $table->text('tujuan')->nullable();
            $table->text('tatacara')->nullable();
            $table->text('risiko')->nullable();
            $table->text('alternatif')->nullable();
            $table->text('prognosis_dilakukan')->nullable();
            $table->text('prognosis_tidak')->nullable();
            $table->text('perluasan')->nullable();
            $table->text('persiapan')->nullable();
            $table->text('penjelasan')->nullable();
            $table->string('lampiran')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_pustaka_tindakan');
        Schema::dropIfExists('trans_pustaka_tindakan');

        Schema::dropIfExists('log_trans_pustaka_penyakit');
        Schema::dropIfExists('trans_pustaka_penyakit');
    }
}
