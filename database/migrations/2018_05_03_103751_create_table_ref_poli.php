<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefPoli extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_poli', function (Blueprint $table) {
            $table->increments('id');
            $table->text('deskripsi');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_poli', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_poli')->unsigned();
            $table->text('deskripsi');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_poli');
        Schema::dropIfExists('ref_poli');
    }
}
