<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKontenEnKegiatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('trans_kegiatan', function (Blueprint $table) {
            $table->string('judul_en',191)->after('judul')->nullable();
            $table->text('konten_en')->after('konten')->nullable();
            $table->text('keterangan_en')->after('keterangan')->nullable();
        });

        Schema::table('log_trans_kegiatan', function (Blueprint $table) {
            $table->string('judul_en',191)->after('judul')->nullable();
            $table->text('konten_en')->after('konten')->nullable();
            $table->text('keterangan_en')->after('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trans_kegiatan', function (Blueprint $table) {
            $table->dropColumn(['judul_en','konten_en','keterangan_en']);
        });

        Schema::table('log_trans_kegiatan', function (Blueprint $table) {
            $table->dropColumn(['judul_en','konten_en','keterangan_en']);
        });
    }
}
