<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAndroidFlagOnSlider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_slider', function (Blueprint $table) {
            $table->boolean('android')->default(1)->after('status');
        });
        Schema::table('log_slider', function (Blueprint $table) {
            $table->boolean('android')->default(1)->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_slider', function (Blueprint $table) {
            $table->dropColumn('android');
        });
        Schema::table('log_slider', function (Blueprint $table) {
            $table->dropColumn('android');
        });

    }
}
