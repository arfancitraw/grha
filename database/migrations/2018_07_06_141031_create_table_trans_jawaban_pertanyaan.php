<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransJawabanPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('trans_jawaban_pertanyaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_trans_tanya_jawab')->unsigned();
            $table->string('referensi');
            $table->text('jawaban');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
            $table->foreign('id_trans_tanya_jawab')->references('id')->on('trans_tanya_jawab');
        });
        Schema::create('log_trans_jawaban_pertanyaan', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_tanya_jawab')->unsigned();
            $table->string('referensi');
            $table->text('jawaban');
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_jawaban_pertanyaan');
        Schema::dropIfExists('log_trans_jawaban_pertanyaan');
    }
}
