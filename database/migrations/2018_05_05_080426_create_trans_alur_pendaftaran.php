<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransAlurPendaftaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
  Schema::create('trans_alur_pendaftaran', function (Blueprint $table) {
        $table->increments('id');
        $table->string('judul',100);
        $table->string('photo',100)->nullable();
        $table->text('keterangan');

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();
    });
       Schema::create('log_trans_alur_pendaftaran', function(Blueprint $table){
        $table->increments('id');
        $table->integer('id_alur_pendaftaran')->unsigned();
        $table->string('judul',100);
        $table->string('photo',100)->nullable();
        $table->text('keterangan');

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_alur_pendaftaran');
        Schema::dropIfExists('log_trans_alur_pendaftaran');
    }
}
