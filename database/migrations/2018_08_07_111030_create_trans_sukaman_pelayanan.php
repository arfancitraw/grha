<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransSukamanPelayanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_sukaman_pelayanan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->text('konten')->nullabel();
            $table->string('icon');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
        Schema::create('log_trans_sukaman_pelayanan', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_sukaman_pelayanan')->unsigned();
            $table->string('judul');
            $table->text('konten')->nullabel();
            $table->string('icon');
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_sukaman_pelayanan');
        Schema::dropIfExists('log_trans_sukaman_pelayanan');
    }
}
