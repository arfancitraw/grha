<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeKeteranganToNullOnKarir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('trans_karir', function (Blueprint $table) {
            $table->text('keterangan')->nullable()->change();
        });
       Schema::table('log_trans_karir', function(Blueprint $table){
            $table->text('keterangan')->nullable()->change();
        });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_karir', function (Blueprint $table) {
            $table->text('keterangan')->nullable()->change();
        });
       Schema::table('log_trans_karir', function(Blueprint $table){
            $table->text('keterangan')->change();
        });
    }
}
