<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTanyaJawab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_tanya_jawab', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('email');
            $table->integer('id_kategori_tanya_jawab')->unsigned();
            $table->string('judul');
            $table->text('konten');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_trans_tanya_jawab', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_tanya_jawab')->unsigned();
            $table->string('nama');
            $table->string('email');
            $table->integer('id_kategori_tanya_jawab')->unsigned();
            $table->string('judul');
            $table->text('konten');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_tanya_jawab');
        Schema::dropIfExists('log_trans_tanya_jawab');
    }
}
