<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranslateEnInfoMutu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('trans_info_mutu', function (Blueprint $table) {
            $table->string('nama_en',191)->after('nama')->nullable();
            $table->text('deskripsi_en')->after('deskripsi')->nullable();
        });

        Schema::table('log_trans_info_mutu', function (Blueprint $table) {
            $table->string('nama_en',191)->after('nama')->nullable();
            $table->text('deskripsi_en')->after('deskripsi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trans_info_mutu', function (Blueprint $table) {
            $table->dropColumn(['nama_en','deskripsi_en']);
        });

        Schema::table('log_trans_info_mutu', function (Blueprint $table) {
            $table->dropColumn(['nama_en','deskripsi_en']);
        });
    }
}
