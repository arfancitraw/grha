<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTransRujukanNasionalNullableAll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->string('no_tlpn',15)->nullable()->change();
            $table->string('email',100)->nullable()->change();
        });
        Schema::table('log_trans_rujukan_nasional', function (Blueprint $table) {
            $table->string('no_tlpn',15)->nullable()->change();
            $table->string('email',100)->nullable()->change();
        });
    }
    public function down()
    {
        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->string('no_tlpn',15)->change();
            $table->string('email',100)->change();
        });
        Schema::table('log_trans_rujukan_nasional', function (Blueprint $table) {
            $table->string('no_tlpn',15)->change();
            $table->string('email',100)->change();
        });
    }
}
