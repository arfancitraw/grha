<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranslateEnRujukanNasionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->text('judul_en')->nullable();
            $table->text('pengantar_en')->nullable();
            $table->text('profile_en')->nullable();
            $table->text('cara_merujuk_en')->nullable();
            $table->text('penelitian_en')->nullable();
            $table->text('jejaring_en')->nullable();
            $table->text('prosedur_en')->nullable();
            $table->text('deskripsi_en')->nullable();
            $table->text('produk_en')->nullable();
            $table->text('tim_en')->nullable();
            $table->text('fasilitas_en')->nullable();
            $table->text('agenda_en')->nullable();
            $table->text('aksesibilitas_en')->nullable();
        });

        Schema::table('log_trans_rujukan_nasional', function (Blueprint $table) {
            $table->text('judul_en')->nullable();
            $table->text('pengantar_en')->nullable();
            $table->text('profile_en')->nullable();
            $table->text('cara_merujuk_en')->nullable();
            $table->text('penelitian_en')->nullable();
            $table->text('jejaring_en')->nullable();
            $table->text('prosedur_en')->nullable();
            $table->text('deskripsi_en')->nullable();
            $table->text('produk_en')->nullable();
            $table->text('tim_en')->nullable();
            $table->text('fasilitas_en')->nullable();
            $table->text('agenda_en')->nullable();
            $table->text('aksesibilitas_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_rujukan_nasional', function (Blueprint $table) {
            $table->dropColumn('judul_en');
            $table->dropColumn('pengantar_en');
            $table->dropColumn('profile_en');
            $table->dropColumn('cara_merujuk_en');
            $table->dropColumn('penelitian_en');
            $table->dropColumn('jejaring_en');
            $table->dropColumn('prosedur_en');
            $table->dropColumn('deskripsi_en');
            $table->dropColumn('produk_en');
            $table->dropColumn('tim_en');
            $table->dropColumn('fasilitas_en');
            $table->dropColumn('agenda_en');
            $table->dropColumn('aksesibilitas_en');
        });

        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->dropColumn('judul_en');
            $table->dropColumn('pengantar_en');
            $table->dropColumn('profile_en');
            $table->dropColumn('cara_merujuk_en');
            $table->dropColumn('penelitian_en');
            $table->dropColumn('jejaring_en');
            $table->dropColumn('prosedur_en');
            $table->dropColumn('deskripsi_en');
            $table->dropColumn('produk_en');
            $table->dropColumn('tim_en');
            $table->dropColumn('fasilitas_en');
            $table->dropColumn('agenda_en');
            $table->dropColumn('aksesibilitas_en');
        });
    }
}
