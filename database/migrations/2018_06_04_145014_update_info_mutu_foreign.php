<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInfoMutuForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_info_mutu', function (Blueprint $table) {
            $table->dropForeign(['jenis_mutu']);
        });
        Schema::table('trans_info_mutu', function (Blueprint $table) {
            $table->foreign('jenis_mutu')->references('id')->on('ref_jenis_mutu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_info_mutu', function (Blueprint $table) {
            $table->dropForeign(['jenis_mutu']);
        });
        Schema::table('trans_info_mutu', function (Blueprint $table) {
            $table->foreign('jenis_mutu')->references('id')->on('ref_spesialisasi');
        });
    }
}
