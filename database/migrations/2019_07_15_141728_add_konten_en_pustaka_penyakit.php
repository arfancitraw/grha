<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKontenEnPustakaPenyakit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('trans_pustaka_penyakit', function (Blueprint $table) {
            $table->string('nama_en',191)->after('nama')->nullable();
            $table->text('pengertian_en')->after('pengertian')->nullable();
            $table->text('gejala_en')->after('gejala')->nullable();
            $table->text('penyebab_en')->after('penyebab')->nullable();
            $table->text('penanganan_en')->after('penanganan')->nullable();
            $table->text('pencegahan_en')->after('pencegahan')->nullable();
        });

        Schema::table('log_trans_pustaka_penyakit', function (Blueprint $table) {
            $table->string('nama_en',191)->after('nama')->nullable();
            $table->text('pengertian_en')->after('pengertian')->nullable();
            $table->text('gejala_en')->after('gejala')->nullable();
            $table->text('penyebab_en')->after('penyebab')->nullable();
            $table->text('penanganan_en')->after('penanganan')->nullable();
            $table->text('pencegahan_en')->after('pencegahan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trans_pustaka_penyakit', function (Blueprint $table) {
            $table->dropColumn(['nama_en','pengertian_en','gejala_en','penyebab_en','penanganan_en','pencegahan_en']);
        });

        Schema::table('log_trans_pustaka_penyakit', function (Blueprint $table) {
            $table->dropColumn(['nama_en','pengertian_en','gejala_en','penyebab_en','penanganan_en','pencegahan_en']);
        });
    }
}
