<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransInfoLampiranTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_informasi_lampiran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('informasi_id')->unsigned();
            $table->string('judul');
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('informasi_id')->references('id')->on('trans_informasi');
        });

        Schema::create('log_trans_informasi_lampiran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lampiran_id')->unsigned();
            $table->integer('informasi_id')->unsigned();
            $table->string('judul');
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_informasi_lampiran');
        Schema::dropIfExists('trans_informasi_lampiran');
    }
}
