<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugChangeKeteranganLampiranOnTransListArtikel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_list_artikel', function (Blueprint $table) {
            $table->string('slug',100)->after('judul')->nullable();
         $table->dropColumn('keterangan');
         $table->dropColumn('lampiran');
         $table->text('deskripsi')->after('konten')->nullable();
         $table->string('photo',100)->after('konten')->nullable();
     });
        Schema::table('log_trans_list_artikel', function(Blueprint $table){
            $table->string('slug',100)->after('judul')->nullable();
            $table->dropColumn('keterangan');
            $table->dropColumn('lampiran');
            $table->text('deskripsi')->after('konten')->nullable();
            $table->string('photo',100)->after('konten')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_list_artikel', function (Blueprint $table) {
            $table->dropColumn('slug');
            $table->dropColumn('deskripsi');
            $table->dropColumn('photo');
            $table->text('keterangan')->after('konten')->nullable();
            $table->string('lampiran',100)->after('konten')->nullable();
        });
        Schema::table('log_trans_list_artikel', function(Blueprint $table){
           $table->dropColumn('slug');
           $table->dropColumn('deskripsi');
           $table->dropColumn('photo');
           $table->text('keterangan')->after('konten')->nullable();
           $table->string('lampiran',100)->after('konten')->nullable();


       });
    }
}
