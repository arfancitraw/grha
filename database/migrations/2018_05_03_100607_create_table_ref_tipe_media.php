<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefTipeMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
{
        Schema::create('ref_tipe_media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipe_media', 100);
            $table->text('deskripsi')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_tipe_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipe_media')->unsigned();
            $table->string('tipe_media', 100);
            $table->text('deskripsi')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_tipe_media');
        Schema::dropIfExists('ref_tipe_media');
    }
}
