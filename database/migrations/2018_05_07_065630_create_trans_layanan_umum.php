<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransLayananUmum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_layanan_umum', function (Blueprint $table) {
        $table->increments('id');
        $table->string('judul');
        $table->text('konten')->nullable();
        $table->text('keterangan')->nullable();
        $table->string('photo')->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();

    });
        Schema::create('log_trans_layanan_umum', function(Blueprint $table){
        $table->increments('id');
        $table->integer('id_layanan_umum')->unsigned();
        $table->string('judul');
        $table->text('konten')->nullable();
        $table->text('keterangan')->nullable();
        $table->string('photo')->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_layanan_umum');
        Schema::dropIfExists('log_trans_layanan_umum');
    }
}
