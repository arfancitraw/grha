<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugifyFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("CREATE OR REPLACE FUNCTION slugify(str text) RETURNS text AS $$
                        BEGIN
                          RETURN lower(translate(str, 
                            'äëïöüáéíóúâêîûôåãõàèìòùřšěčůńýśćłęąĄŃÝŚĆŁĘÄËÏÖÜÁÉÍÓÚÂÊÎÛÔÅÃÕÀÈÌÒÙŘŠĚČŮß :;-²ø®(),.\/\"’`''',
                            'aeiouaeiouaeiouaaoaeioursecunyscleaANYSCLEAEIOUAEIOUAEIOUAAOAEIOURSECUs----2dR' -- missing chars will be removed
                          ));
                        END
                        $$ LANGUAGE plpgsql;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP FUNCTION IF EXISTS slugify(str text);");
    }
}
