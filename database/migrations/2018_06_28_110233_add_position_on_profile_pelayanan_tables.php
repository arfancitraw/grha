<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionOnProfilePelayananTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_profile', function (Blueprint $table) {
            $table->integer('posisi')->default(0);
        });
        Schema::table('log_trans_profile', function(Blueprint $table){
            $table->integer('posisi')->default(0);
        });

        Schema::table('trans_layanan', function (Blueprint $table) {
            $table->integer('posisi')->default(0);
        });
        Schema::table('log_trans_layanan', function(Blueprint $table){
            $table->integer('posisi')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_layanan', function(Blueprint $table){
            $table->dropColumn('posisi');
        });
        Schema::table('trans_layanan', function (Blueprint $table) {
            $table->dropColumn('posisi');
        });

        Schema::table('log_trans_profile', function(Blueprint $table){
            $table->dropColumn('posisi');
        });
        Schema::table('trans_profile', function (Blueprint $table) {
            $table->dropColumn('posisi');
        });
    }
}
