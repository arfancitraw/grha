<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditInformasiTransSukaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_sukaman_informasi', function (Blueprint $table) {
            $table->string('gambar')->nullable()->change();
        });
         
        Schema::table('log_trans_sukaman_informasi', function(Blueprint $table){
            $table->string('gambar')->nullable()->change();
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_sukaman_informasi', function (Blueprint $table) {
            $table->string('gambar')->change();
        });
         
        Schema::table('log_trans_sukaman_informasi', function(Blueprint $table){
            $table->string('gambar')->change();
        });
    }
}
