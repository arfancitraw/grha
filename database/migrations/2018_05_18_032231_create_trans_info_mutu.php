<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransInfoMutu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
 Schema::create('trans_info_mutu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',150);
            $table->integer('jenis_mutu')->unsigned();
            $table->text('deskripsi')->nullable();
            $table->string('gambar')->nullable();
            $table->string('periode');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('jenis_mutu')->references('id')->on('ref_spesialisasi');
        });
        Schema::create('log_trans_info_mutu', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_info_mutu')->unsigned();
            $table->string('nama',150);
            $table->integer('jenis_mutu')->unsigned();
            $table->text('deskripsi')->nullable();
            $table->string('gambar')->nullable();
            $table->string('periode');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_info_mutu');
        Schema::dropIfExists('log_trans_info_mutu');
    }
}
