<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePengaduanSponsorNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('trans_pengaduan_sponsor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('nip_nopeg');
            $table->string('jabatan');
            $table->string('nama_kegiatan');
            $table->string('tempat_kegiatan');
            $table->date('tanggal');
            $table->string('peranan');
            $table->string('nama_perusahaan');
            $table->text('alamat');
            $table->string('pemberi_sponsor');
            $table->decimal('registrasi',20,2);
            $table->decimal('akomodasi',20,2);
            $table->decimal('transportasi',20,2);
            $table->decimal('honor',20,2);
            $table->decimal('total',20,2);
            

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
        Schema::create('log_trans_pengaduan_sponsor', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan_sponsor')->unsigned();
            $table->string('nama');
            $table->string('nip_nopeg');
            $table->string('jabatan');
            $table->string('nama_kegiatan');
            $table->string('tempat_kegiatan');
            $table->date('tanggal');
            $table->string('peranan');
            $table->string('nama_perusahaan');
            $table->text('alamat');
            $table->string('pemberi_sponsor');
            $table->decimal('registrasi',20,2);
            $table->decimal('akomodasi',20,2);
            $table->decimal('transportasi',20,2);
            $table->decimal('honor',20,2);
            $table->decimal('total',20,2);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_pengaduan_sponsor_bukti', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengaduan_sponsor')->unsigned();
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_pengaduan_sponsor')->references('id')->on('trans_pengaduan_sponsor');
        });
        Schema::create('log_trans_pengaduan_sponsor_bukti', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan_sponsor_bukti')->unsigned();
            $table->integer('id_pengaduan_sponsor')->unsigned();
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('trans_pengaduan_sponsor_bukti');
        Schema::dropIfExists('log_trans_pengaduan_sponsor_bukti');
        Schema::dropIfExists('trans_pengaduan_sponsor');
        Schema::dropIfExists('log_trans_pengaduan_sponsor');
    }
}
