<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagOnDetailDokter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_dokter_keahlian', function (Blueprint $table) {
            $table->integer('tipe')->default(0)->comment('0:spesialisasi, 1:prosedur');
        });

        Schema::table('trans_dokter_pendidikan', function (Blueprint $table) {
            $table->integer('tipe')->default(0)->comment('0:gelar, 1:keanggotaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_dokter_keahlian', function (Blueprint $table) {
            $table->dropColumn('tipe');
        });

        Schema::table('trans_dokter_pendidikan', function (Blueprint $table) {
            $table->dropColumn('tipe');
        });
    }
}
