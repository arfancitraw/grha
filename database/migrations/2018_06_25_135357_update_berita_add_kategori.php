<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBeritaAddKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengumuman', function (Blueprint $table) {
            $table->date('tanggal')->nullable();
            $table->integer('kategori_id')->unsigned()->nullable();
            $table->foreign('kategori_id')
                  ->references('id')
                  ->on('ref_kategori_pengumuman');
        });

        Schema::table('log_trans_pengumuman', function(Blueprint $table){
            $table->date('tanggal')->nullable();
            $table->integer('kategori_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_pengumuman', function (Blueprint $table) {
            $table->dropForeign(['kategori_id']);
            $table->dropColumn('kategori_id');
            $table->dropColumn('tanggal');
        });

        Schema::table('log_trans_pengumuman', function(Blueprint $table){
            $table->dropColumn('kategori_id');
            $table->dropColumn('tanggal');
        });
    }
}
