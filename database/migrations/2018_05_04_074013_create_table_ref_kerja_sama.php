<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefKerjaSama extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('ref_kerja_sama', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
         
        Schema::create('log_kerja_sama', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_kerja_sama')->unsigned();
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_kerja_sama');
        Schema::dropIfExists('log_kerja_sama');
    }
}
