<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDropTestimoniFieldLayananYgDigunakan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_trans_testimoni', function(Blueprint $table){
            $table->dropColumn('layanan_yang_digunakan')->nullable();
            $table->integer('created_by')->nullable()->change();
            $table->integer('publik')->nullable()->change();
        });

        Schema::table('trans_testimoni', function (Blueprint $table) {
            $table->dropColumn('layanan_yang_digunakan')->nullable();
            $table->integer('created_by')->nullable()->change();
            $table->integer('publik')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_testimoni', function (Blueprint $table) {
            $table->integer('layanan_yang_digunakan')->nullable();
        });
         
        Schema::table('log_trans_testimoni', function(Blueprint $table){
            $table->integer('layanan_yang_digunakan')->nullable();
        });
    }
}
