<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransTestimoni extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
Schema::create('trans_testimoni', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('email');
            $table->integer('layanan_yang_digunakan')->unsigned();
            $table->text('testimoni');
            $table->integer('publik');
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('layanan_yang_digunakan')->references('id')->on('trans_layanan');
            
        });
         
        Schema::create('log_trans_testimoni', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_testimoni')->unsigned();
            $table->string('nama');
            $table->string('email');
            $table->integer('layanan_yang_digunakan')->unsigned();
            $table->text('testimoni');
            $table->integer('publik');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_testimoni');
        Schema::dropIfExists('trans_testimoni');
    }
}
