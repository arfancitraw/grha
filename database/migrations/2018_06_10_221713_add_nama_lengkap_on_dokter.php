<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNamaLengkapOnDokter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_list_dokter', function (Blueprint $table) {
            $table->string('nama_lengkap')->nullable();
        });
        Schema::table('log_trans_list_dokter', function (Blueprint $table) {
            $table->string('nama_lengkap')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_list_dokter', function (Blueprint $table) {
            $table->dropColumn('nama_lengkap');
        });
        Schema::table('log_trans_list_dokter', function (Blueprint $table) {
            $table->dropColumn('nama_lengkap');
        });
    }
}
