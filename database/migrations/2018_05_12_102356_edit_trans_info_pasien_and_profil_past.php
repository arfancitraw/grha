<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTransInfoPasienAndProfilPast extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_profile', function(Blueprint $table){
             $table->string('gambar')->nullable();
        });

        Schema::table('log_trans_profile', function (Blueprint $table) {
             $table->string('gambar')->nullable();
        });

        Schema::table('trans_info_pasien', function(Blueprint $table){
             $table->string('gambar')->nullable();
        });

        Schema::table('log_trans_info_pasien', function (Blueprint $table) {
             $table->string('gambar')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_profile', function(Blueprint $table){
            $table->dropColumn('gambar');
        });

        Schema::table('trans_profile', function (Blueprint $table) {
            $table->dropColumn('gambar');
        });

        Schema::table('log_trans_info_pasien', function(Blueprint $table){
            $table->dropColumn('gambar');
        });

        Schema::table('trans_info_pasien', function (Blueprint $table) {
            $table->dropColumn('gambar');
        });
    }
}
