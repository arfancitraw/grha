<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransInfoWhistle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('trans_info_whistle', function (Blueprint $table) {
        $table->increments('id');
        $table->string('judul');
        $table->text('konten')->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();

    });
      Schema::create('log_trans_info_whistle', function(Blueprint $table){
        $table->increments('id');
        $table->integer('id_info_whistle')->unsigned();
        $table->string('judul');
        $table->text('konten')->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_info_whistle');
        Schema::dropIfExists('log_trans_info_whistle');
    }
}
