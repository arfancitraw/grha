<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSliderAddNullableUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_slider', function (Blueprint $table) {
            $table->string('url')->nullable()->change();
            $table->string('sub_judul')->nullable()->change();
        });

        Schema::table('log_slider', function(Blueprint $table){
            $table->string('url')->nullable()->change();
            $table->string('sub_judul')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_slider', function(Blueprint $table){
            $table->string('url', 100)->change();
            $table->string('sub_judul', 100)->change();
        });
        Schema::table('ref_slider', function (Blueprint $table) {
            $table->string('url', 100)->change();
            $table->string('sub_judul', 100)->change();
        });
    }
}
