<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeKuesionerDetailForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_kuesioner_detail', function (Blueprint $table) {
            $table->dropForeign(['kuesioner_id']);
        });
        Schema::table('ref_kuesioner_detail', function (Blueprint $table) {
            $table->foreign('kuesioner_id')
                  ->references('id')
                  ->on('ref_kuesioner_periode')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_kuesioner_detail', function (Blueprint $table) {
            $table->dropForeign(['kuesioner_id']);
        });
        Schema::table('ref_kuesioner_detail', function (Blueprint $table) {
            $table->foreign('kuesioner_id')
                  ->references('id')
                  ->on('ref_kuesioner_periode')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }
}
