<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdTanyaJawabPnTransTanyaJawab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_tanya_jawab', function (Blueprint $table) {
            $table->integer('id_tanya_jawab_pertanyaan')->nullable();

            $table->foreign('id_tanya_jawab_pertanyaan')->references('id')->on('trans_tanya_jawab');
        });
        Schema::table('log_trans_tanya_jawab', function(Blueprint $table){
            $table->integer('id_tanya_jawab_pertanyaan')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_tanya_jawab', function(Blueprint $table){
            $table->dropColumn('id_tanya_jawab_pertanyaan');
        });
        Schema::table('trans_tanya_jawab', function (Blueprint $table) {
            $table->dropColumn('id_tanya_jawab_pertanyaan');
        });

    }
}
