<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIconSpesialisasiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_spesialisasi', function (Blueprint $table) {
            $table->string('icon')->nullable()->after('deskripsi');
        });
        Schema::table('log_spesialisasi', function (Blueprint $table) {
            $table->string('icon')->nullable()->after('deskripsi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_spesialisasi', function (Blueprint $table) {
            $table->dropColumn('icon');
        });
        Schema::table('log_spesialisasi', function (Blueprint $table) {
            $table->dropColumn('icon');
        });
    }
}
