<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTransRujukanNasional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_rujukan_nasional', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul',100);
            $table->mediumtext('pengantar')->nullable();
            $table->mediumtext('profile')->nullable();
            $table->mediumtext('cara_merujuk')->nullable();
            $table->mediumtext('penelitian')->nullable();
            $table->mediumtext('jejaring')->nullable();
            $table->mediumtext('prosedur')->nullable();
            $table->string('aksesibilitas',100);
            $table->mediumtext('alamat')->nullable();
            $table->string('no_tlpn',15);
            $table->string('email',100);
            $table->mediumtext('deskripsi')->nullable();
            $table->string('photo')->nullable();
            $table->integer('status')->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_trans_rujukan_nasional', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_trans_rujukan_nasional')->unsigned();
            $table->string('judul',100);
            $table->mediumtext('pengantar')->nullable();
            $table->mediumtext('profile')->nullable();
            $table->mediumtext('cara_merujuk')->nullable();
            $table->mediumtext('penelitian')->nullable();
            $table->mediumtext('jejaring')->nullable();
            $table->mediumtext('prosedur')->nullable();
            $table->string('aksesibilitas',100);
            $table->mediumtext('alamat')->nullable();
            $table->string('no_tlpn',15);
            $table->string('email',100);
            $table->mediumtext('deskripsi')->nullable();
            $table->string('photo')->nullable();
            $table->integer('status')->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('log_trans_rujukan_nasional');
        Schema::dropIfExists('trans_rujukan_nasional');
    }
}
