<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransListDokter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_list_dokter', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',150);
            $table->integer('spesialisasi_id')->unsigned();
            $table->text('alamat');
            $table->string('no_tlpn',20);
            $table->text('deskripsi')->nullable();
            $table->string('photo')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('spesialisasi_id')->references('id')->on('ref_spesialisasi');
        });
        Schema::create('log_trans_list_dokter', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_list_dokter')->unsigned();
            $table->string('nama',150);
            $table->integer('spesialisasi_id')->unsigned();
            $table->text('alamat');
            $table->string('no_tlpn',20);
            $table->text('deskripsi')->nullable();
            $table->string('photo')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_list_dokter');
        Schema::dropIfExists('log_trans_list_dokter');
    }
}
