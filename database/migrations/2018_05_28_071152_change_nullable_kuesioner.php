<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNullableKuesioner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('trans_kuesioner_jawaban', function (Blueprint $table) {
            $table->integer('created_by')->nullable()->change();
        });
       Schema::table('log_trans_kuesioner_jawaban', function(Blueprint $table){
            $table->integer('created_by')->nullable()->change();
        });

       Schema::table('trans_kuesioner_jawaban_detail', function (Blueprint $table) {
            $table->integer('created_by')->nullable()->change();
        });
       Schema::table('log_trans_kuesioner_jawaban_detail', function(Blueprint $table){
            $table->integer('created_by')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_kuesioner_jawaban', function (Blueprint $table) {
            $table->integer('created_by')->nullable()->change();
        });
       Schema::table('log_trans_kuesioner_jawaban', function(Blueprint $table){
            $table->integer('created_by')->change();
        });

        Schema::table('trans_kuesioner_jawaban_detail', function (Blueprint $table) {
            $table->integer('created_by')->nullable()->change();
        });
       Schema::table('log_trans_kuesioner_jawaban_detail', function(Blueprint $table){
            $table->integer('created_by')->change();
        });
    }
}
