<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChoiceLayananTransLayananUnggulan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_layanan_unggulan', function (Blueprint $table) {
            $table->integer('layanan_unggulan')->default(0);
        });
        Schema::table('log_trans_layanan_unggulan', function (Blueprint $table) {
            $table->integer('layanan_unggulan')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_layanan_unggulan', function (Blueprint $table) {
            $table->dropColumn('layanan_unggulan')->nullable();
        });
        Schema::table('trans_layanan_unggulan', function (Blueprint $table) {
            $table->dropColumn('layanan_unggulan')->nullable();
        });
    }
}
