<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTransRujukanTimAttributs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_trans_rujukan_tim', function(Blueprint $table){
            $table->dropColumn('id_dokter')->nullable();
        });

        Schema::table('trans_rujukan_tim', function (Blueprint $table) {
            $table->dropColumn('id_dokter')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_rujukan_tim', function (Blueprint $table) {
            $table->integer('id_dokter')->nullable();
        });
         
        Schema::table('log_trans_rujukan_tim', function(Blueprint $table){
            $table->integer('id_dokter')->nullable();
        });
    }
}
