<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransKuesionerJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('trans_kuesioner_jawaban', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',50);
            $table->string('email',50);
            $table->integer('kuesioner_id')->unsigned();
            $table->text('feedback');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('kuesioner_id')->references('id')->on('ref_kuesioner_periode');
        });
        Schema::create('log_trans_kuesioner_jawaban', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_kuesioner_jawaban')->unsigned();
            $table->string('nama',50);
            $table->string('email',50);
            $table->integer('kuesioner_id')->unsigned();
            $table->text('feedback');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_kuesioner_jawaban');
        Schema::dropIfExists('log_trans_kuesioner_jawaban');
    }
}
