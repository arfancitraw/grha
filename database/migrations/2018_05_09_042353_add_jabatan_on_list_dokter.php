<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJabatanOnListDokter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_list_dokter', function (Blueprint $table) {
            $table->integer('jabatan_dokter_id')->unsigned()->nullable();

            $table->foreign('jabatan_dokter_id')->references('id')->on('ref_jabatan_dokter');
        });
        Schema::table('log_trans_list_dokter', function(Blueprint $table){
            $table->integer('jabatan_dokter_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_list_dokter', function (Blueprint $table) {
            $table->dropForeign('trans_list_dokter_jabatan_dokter_id_foreign');
            $table->dropColumn('jabatan_dokter_id')->unsigned();
        });
        Schema::table('log_trans_list_dokter', function(Blueprint $table){
            $table->dropColumn('jabatan_dokter_id');
        });
    }
}
