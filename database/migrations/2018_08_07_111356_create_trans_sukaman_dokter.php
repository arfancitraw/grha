<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransSukamanDokter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_sukaman_dokter', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dokter')->unsigned();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_dokter')->references('id')->on('trans_list_dokter');
            
        });
         
        Schema::create('log_trans_sukaman_dokter', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_sukaman_dokter')->unsigned();
            $table->integer('id_dokter')->unsigned();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_sukaman_dokter');
        Schema::dropIfExists('trans_sukaman_dokter');
    }
}
