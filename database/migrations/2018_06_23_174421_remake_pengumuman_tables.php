<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemakePengumumanTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pengumuman', function (Blueprint $table) {
            $table->text('keterangan')->nullable()->change();
            $table->string('slug')->nullable();
            $table->string('photo')->nullable();
        });

        Schema::table('log_trans_pengumuman', function(Blueprint $table){
            $table->text('keterangan')->nullable()->change();
            $table->string('slug')->nullable();
            $table->string('photo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_pengumuman', function(Blueprint $table){
            $table->text('keterangan')->change();
            $table->dropColumn('slug');
            $table->dropColumn('photo');
        });

        Schema::table('trans_pengumuman', function (Blueprint $table) {
            $table->text('keterangan')->change();
            $table->dropColumn('slug');
            $table->dropColumn('photo');
        });
    }
}
