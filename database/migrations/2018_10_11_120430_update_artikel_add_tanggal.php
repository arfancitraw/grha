<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateArtikelAddTanggal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_list_artikel', function (Blueprint $table) {
            $table->date('tanggal')->nullable();
        });

        Schema::table('log_trans_list_artikel', function(Blueprint $table){
            $table->date('tanggal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_list_artikel', function(Blueprint $table){
            $table->dropColumn('tanggal');
        });

        Schema::table('trans_list_artikel', function (Blueprint $table) {
            $table->dropColumn('tanggal');
        });
    }
}
