<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefSpesialisasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_spesialisasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',100);
            $table->text('deskripsi')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_spesialisasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_spesialisasi')->unsigned();
            $table->string('nama',100);
            $table->text('deskripsi')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_spesialisasi');
        Schema::dropIfExists('log_spesialisasi');
    }
}
