<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRightFlagOnMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_menu', function (Blueprint $table) {
            $table->boolean('right')->default(0);
        });
        Schema::table('log_trans_menu', function (Blueprint $table) {
            $table->boolean('right')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_menu', function (Blueprint $table) {
            $table->dropColumn('right');
        });
        Schema::table('trans_menu', function (Blueprint $table) {
            $table->dropColumn('right');
        });
    }
}
