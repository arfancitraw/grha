<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableToIndukId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_menu', function (Blueprint $table) {
            $table->integer('induk_id')->unsigned()->nullable()->change();
        });
        Schema::table('log_trans_menu', function (Blueprint $table) {
            $table->integer('induk_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_menu', function (Blueprint $table) {
            $table->integer('induk_id')->unsigned()->change();
        });
        Schema::table('log_trans_menu', function (Blueprint $table) {
            $table->integer('induk_id')->unsigned()->change();
        });
    }
}
