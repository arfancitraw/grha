<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTransRujukanNasional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->dropColumn('kinerja')->nullable();
        });
         
        Schema::table('log_trans_rujukan_nasional', function(Blueprint $table){
            $table->dropColumn('kinerja')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->text('kinerja')->nullable();
        });
         
        Schema::table('log_trans_rujukan_nasional', function(Blueprint $table){
            $table->text('kinerja')->nullable();
        });
    }
}
