<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefTipePendaftar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_tipe_pendaftar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipe_pendaftar', 100);
            $table->text('deskripsi')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_tipe_pendaftar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipe_pendaftar')->unsigned();
            $table->string('tipe_pendaftar', 100);
            $table->text('deskripsi')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_tipe_pendaftar');
        Schema::dropIfExists('ref_tipe_pendaftar');
    }
}
