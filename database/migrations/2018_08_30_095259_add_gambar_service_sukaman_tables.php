<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGambarServiceSukamanTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_sukaman_pelayanan', function (Blueprint $table) {
            $table->string('gambar')->nullable()->after('icon');
        });
        Schema::table('log_trans_sukaman_pelayanan', function (Blueprint $table) {
            $table->string('gambar')->nullable()->after('icon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_sukaman_pelayanan', function (Blueprint $table) {
            $table->dropColumn('gambar');
        });
        Schema::table('trans_sukaman_pelayanan', function (Blueprint $table) {
            $table->dropColumn('gambar');
        });
    }
}
