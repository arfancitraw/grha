<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefDewanDireksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_dewan_direksi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',100);
            $table->string('jabatan',100);
            $table->string('email',100);
            $table->string('no_tlpn',15);
            $table->text('biografi')->nullable();
            $table->string('photo',100)->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_dewan_direksi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dewan_direksi')->unsigned();
            $table->string('nama',100);
            $table->string('jabatan',100);
            $table->string('email',100);
            $table->string('no_tlpn',15);
            $table->text('biografi')->nullable();
            $table->string('photo',100)->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_dewan_direksi');
        Schema::dropIfExists('ref_dewan_direksi');
    }
}
