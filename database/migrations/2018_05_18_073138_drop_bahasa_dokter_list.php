<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropBahasaDokterList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('trans_dokter_bahasa');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::create('trans_dokter_bahasa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('list_dokter_id')->unsigned();
            $table->string('nama');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('list_dokter_id')->references('id')->on('trans_list_dokter');


        });
    }
}
