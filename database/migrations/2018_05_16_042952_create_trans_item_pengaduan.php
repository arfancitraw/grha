<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransItemPengaduan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_item_pengaduan', function (Blueprint $table) {
        $table->increments('id');
        $table->string('judul');
        $table->text('keterangan')->nullable();
        $table->string('photo')->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();

    });
        Schema::create('log_trans_item_pengaduan', function(Blueprint $table){
        $table->increments('id');
        $table->integer('id_item_pengaduan')->unsigned();
        $table->string('judul');
        $table->text('keterangan')->nullable();
        $table->string('photo')->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_item_pengaduan');
        Schema::dropIfExists('log_trans_item_pengaduan');
    }
}
