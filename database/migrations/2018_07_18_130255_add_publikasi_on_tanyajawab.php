<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPublikasiOnTanyajawab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_tanya_jawab', function (Blueprint $table) {
            $table->boolean('publikasi')->default(0);
        });
        Schema::table('log_trans_tanya_jawab', function (Blueprint $table) {
            $table->boolean('publikasi')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_tanya_jawab', function (Blueprint $table) {
            $table->dropColumn('publikasi');
        });
        Schema::table('log_trans_tanya_jawab', function (Blueprint $table) {
            $table->dropColumn('publikasi');
        });
    }
}
