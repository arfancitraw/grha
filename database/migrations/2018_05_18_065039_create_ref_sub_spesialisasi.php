<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefSubSpesialisasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
 Schema::create('ref_sub_spesialisasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',150);
            $table->integer('spesialisasi_induk')->unsigned();
            $table->text('keterangan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('spesialisasi_induk')->references('id')->on('ref_spesialisasi');
        });
        Schema::create('log_ref_sub_spesialisasi', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_sub_spesialisasi')->unsigned();
            $table->string('nama',150);
            $table->integer('spesialisasi_induk')->unsigned();
            $table->text('keterangan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_sub_spesialisasi');
        Schema::dropIfExists('log_ref_sub_spesialisasi');
    }
}
