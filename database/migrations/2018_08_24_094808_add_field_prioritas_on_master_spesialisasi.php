<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPrioritasOnMasterSpesialisasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_spesialisasi', function (Blueprint $table) {
            $table->integer('prioritas')->default(999);
        });
        Schema::table('log_spesialisasi', function (Blueprint $table) {
            $table->integer('prioritas')->default(999);
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_spesialisasi', function (Blueprint $table) {
            $table->dropColumn('prioritas')->unsigned(999);
        });
        Schema::table('log_spesialisasi', function (Blueprint $table) {
            $table->dropColumn('prioritas')->unsigned(999);
        });
    }
}
