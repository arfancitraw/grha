<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefDewanPengawas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('ref_dewan_pengawas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jabatan_dokter_id')->unsigned();
            $table->string('nama');
            $table->string('email');
            $table->string('no_tlpn');
            $table->string('photo')->nullable();
            $table->mediumtext('biografi')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('jabatan_dokter_id')->references('id')->on('ref_jabatan_dokter');
        });
        Schema::create('log_dewan_pengawas', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_dewan_pengawas')->unsigned();
            $table->integer('jabatan_dokter_id')->unsigned();
            $table->string('nama');
            $table->string('email');
            $table->string('no_tlpn');
            $table->string('photo')->nullable();
            $table->mediumtext('biografi')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_dewan_pengawas');
        Schema::dropIfExists('log_dewan_pengawas');
    }
}
