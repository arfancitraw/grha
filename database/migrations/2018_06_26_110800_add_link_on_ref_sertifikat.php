<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLinkOnRefSertifikat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_sertifikat_footer', function (Blueprint $table) {
            $table->string('link')->nullable();
        });

        Schema::table('log_ref_sertifikat_footer', function(Blueprint $table){
            $table->string('link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_sertifikat_footer', function(Blueprint $table){
            $table->dropColumn('link');
        });

        Schema::table('ref_sertifikat_footer', function (Blueprint $table) {
            $table->dropColumn('link');
        });
    }
}
