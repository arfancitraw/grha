<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefTautanFooter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_tautan_footer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 50);
            $table->string('url',50);
            $table->text('deskripsi');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_tautan_footer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tautan_footer')->unsigned();
            $table->string('nama', 50);
            $table->string('url',50);
            $table->text('deskripsi');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_tautan_footer');
        Schema::dropIfExists('ref_tautan_footer');
    }
}
