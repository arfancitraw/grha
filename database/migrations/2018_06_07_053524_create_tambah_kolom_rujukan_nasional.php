<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTambahKolomRujukanNasional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->boolean('home')->nullable();
            $table->string('icon',100)->nullable();
        });
        Schema::table('log_trans_rujukan_nasional', function (Blueprint $table) {
            $table->boolean('home')->nullable();
            $table->string('icon',100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->dropColumn('home');
            $table->dropColumn('icon');
        });
        Schema::table('log_trans_rujukan_nasional', function (Blueprint $table) {
            $table->dropColumn('home');
            $table->dropColumn('icon');
        });
    }
}
