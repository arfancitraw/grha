<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransInfoBed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
 Schema::create('trans_info_bed', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',150);
            $table->string('gambar')->nullable();
            $table->integer('jumlah_total');
            $table->integer('isi');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_trans_info_bed', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_info_bed')->unsigned();
            $table->string('nama',150);
            $table->string('gambar')->nullable();
            $table->integer('jumlah_total');
            $table->integer('isi');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_info_bed');
        Schema::dropIfExists('log_trans_info_bed');
    }
}
