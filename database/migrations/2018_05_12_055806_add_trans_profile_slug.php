<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransProfileSlug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('trans_profile', function(Blueprint $table){
             $table->string('slug');
        });

        Schema::table('log_trans_profile', function (Blueprint $table) {
             $table->string('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_profile', function(Blueprint $table){
            $table->dropColumn('slug');
        });

        Schema::table('trans_profile', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
