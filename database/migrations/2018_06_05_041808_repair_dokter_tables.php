<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RepairDokterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_jadwal_dokter', function (Blueprint $table) {
            $table->string('kode_departemen')->default('');
            $table->string('nama_departemen')->default('');
            $table->string('status_jam')->default('');
            $table->string('status_ue')->default('');
            $table->string('quota_janji')->default('');
        });
        Schema::table('log_trans_jadwal_dokter', function (Blueprint $table) {
            $table->string('kode_departemen')->default('');
            $table->string('nama_departemen')->default('');
            $table->string('status_jam')->default('');
            $table->string('status_ue')->default('');
            $table->string('quota_janji')->default('');
        });

        Schema::table('trans_list_dokter', function (Blueprint $table) {
            $table->text('alamat')->nullable()->change();
            $table->string('no_tlpn', 20)->nullable()->change();
            $table->string('kode')->default('');
        });
        Schema::table('log_trans_list_dokter', function (Blueprint $table) {
            $table->text('alamat')->nullable()->change();
            $table->string('no_tlpn', 20)->nullable()->change();
            $table->string('kode')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_jadwal_dokter', function (Blueprint $table) {
            $table->dropColumn('kode_departemen');
            $table->dropColumn('nama_departemen');
            $table->dropColumn('status_jam');
            $table->dropColumn('status_ue');
            $table->dropColumn('quota_janji');
        });
        Schema::table('log_trans_jadwal_dokter', function (Blueprint $table) {
            $table->dropColumn('kode_departemen');
            $table->dropColumn('nama_departemen');
            $table->dropColumn('status_jam');
            $table->dropColumn('status_ue');
            $table->dropColumn('quota_janji');
        });

        Schema::table('trans_list_dokter', function (Blueprint $table) {
            $table->text('alamat')->change();
            $table->string('no_tlpn', 20)->change();
            $table->dropColumn('kode');
        });
        Schema::table('log_trans_list_dokter', function (Blueprint $table) {
            $table->text('alamat')->change();
            $table->string('no_tlpn', 20)->change();
            $table->dropColumn('kode');
        });
    }
}
