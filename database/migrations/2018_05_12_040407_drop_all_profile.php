<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAllProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ref_kerja_sama');
        Schema::dropIfExists('log_kerja_sama');

        Schema::dropIfExists('log_profesi_keahlian');
        Schema::dropIfExists('ref_profesi_keahlian');

        Schema::dropIfExists('ref_penelitian');
        Schema::dropIfExists('log_penelitian');

        Schema::dropIfExists('log_denah_lokasi');
        Schema::dropIfExists('ref_denah_lokasi');

        Schema::dropIfExists('log_struktur_organisasi');
        Schema::dropIfExists('ref_struktur_organisasi');

        Schema::dropIfExists('log_dewan_direksi');
        Schema::dropIfExists('ref_dewan_direksi');

        Schema::dropIfExists('ref_dewan_pengawas');
        Schema::dropIfExists('log_dewan_pengawas');

        Schema::dropIfExists('ref_sambutan');
        Schema::dropIfExists('log_sambutan');

        Schema::dropIfExists('ref_visi_misi');
        Schema::dropIfExists('log_visi_misi');

        Schema::dropIfExists('log_sejarah');
        Schema::dropIfExists('ref_sejarah');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('ref_kerja_sama', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
         
        Schema::create('log_kerja_sama', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_kerja_sama')->unsigned();
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

         Schema::create('ref_profesi_keahlian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('photo',100)->nullable();
            $table->integer('status');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_profesi_keahlian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_profesi_keahlian')->unsigned();
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('photo',100)->nullable();
            $table->integer('status');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('ref_penelitian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('photo',100)->nullable();
            $table->integer('status')->default(0);

            $table->integer('created_by')->unsigned();
            $table->timestamp('created_at');
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('log_penelitian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_penelitian')->unsigned();
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('photo',100)->nullable();
            $table->integer('status');

            $table->integer('created_by')->unsigned();
            $table->timestamp('created_at');
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('ref_denah_lokasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('photo',100)->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_denah_lokasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_denah_lokasi')->unsigned();
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('photo',100)->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('ref_struktur_organisasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('photo',100)->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_struktur_organisasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_struktur_organisasi')->unsigned();
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('photo',100)->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('ref_dewan_direksi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',100);
            $table->string('jabatan',100);
            $table->string('email',100);
            $table->string('no_tlpn',15);
            $table->text('biografi')->nullable();
            $table->string('photo',100)->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_dewan_direksi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dewan_direksi')->unsigned();
            $table->string('nama',100);
            $table->string('jabatan',100);
            $table->string('email',100);
            $table->string('no_tlpn',15);
            $table->text('biografi')->nullable();
            $table->string('photo',100)->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('ref_dewan_pengawas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jabatan_dokter_id')->unsigned();
            $table->string('nama');
            $table->string('email');
            $table->string('no_tlpn');
            $table->string('photo')->nullable();
            $table->mediumtext('biografi')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('jabatan_dokter_id')->references('id')->on('ref_jabatan_dokter');
        });
        Schema::create('log_dewan_pengawas', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_dewan_pengawas')->unsigned();
            $table->integer('jabatan_dokter_id')->unsigned();
            $table->string('nama');
            $table->string('email');
            $table->string('no_tlpn');
            $table->string('photo')->nullable();
            $table->mediumtext('biografi')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('ref_sambutan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
        Schema::create('log_sambutan', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_sambutan')->unsigned();
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('ref_visi_misi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_visi_misi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_visi_misi')->unsigned();
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('ref_sejarah', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_sejarah', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_sejarah')->unsigned();
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }
}
