<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateKuisionerAddEssayFeature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_kuesioner_detail', function (Blueprint $table) {
            $table->boolean('tipe')->default(0)->after('pertanyaan')->comment('0: Rating, 1: Essay');
        });
        Schema::table('log_ref_kuesioner_detail', function (Blueprint $table) {
            $table->boolean('tipe')->default(0)->after('pertanyaan')->comment('0: Rating, 1: Essay');
        });
        Schema::table('trans_kuesioner_jawaban_detail', function (Blueprint $table) {
            $table->text('jawaban')->nullable()->change();
        });
        Schema::table('log_trans_kuesioner_jawaban_detail', function (Blueprint $table) {
            $table->text('jawaban')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_kuesioner_detail', function (Blueprint $table) {
            $table->dropColumn('tipe');
        });
        Schema::table('log_ref_kuesioner_detail', function (Blueprint $table) {
            $table->dropColumn('tipe');
        });
        Schema::table('trans_kuesioner_jawaban_detail', function (Blueprint $table) {
            $table->integer('jawaban')->change();
        });
        Schema::table('log_trans_kuesioner_jawaban_detail', function (Blueprint $table) {
            $table->integer('jawaban')->change();
        });
    }
}
