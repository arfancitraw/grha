<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePengaduanSaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('trans_pengaduan_saksi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengaduan')->unsigned();
            $table->string('nama');
            $table->text('alamat');
            $table->text('telp');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_pengaduan')->references('id')->on('trans_pengaduan');
        });
        Schema::create('log_trans_pengaduan_saksi', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan_saksi')->unsigned();
            $table->integer('id_pengaduan')->unsigned();
            $table->string('nama');
            $table->text('alamat');
            $table->text('telp');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_pengaduan_saksi');
        Schema::dropIfExists('log_trans_pengaduan_saksi');
    }
}
