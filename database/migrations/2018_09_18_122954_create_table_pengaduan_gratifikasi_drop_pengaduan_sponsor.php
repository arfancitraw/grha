<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePengaduanGratifikasiDropPengaduanSponsor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('trans_pengaduan_sponsor_bukti');
        Schema::dropIfExists('log_trans_pengaduan_sponsor_bukti');
        Schema::dropIfExists('trans_pengaduan_sponsor');
        Schema::dropIfExists('log_trans_pengaduan_sponsor');

        Schema::create('trans_pengaduan_gratifikasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('nip_nopeg');
            $table->string('jabatan_unit_kerja');
            $table->string('jenis_bentuk_penerimaan');
            $table->decimal('harga_nilai_nominal',20,2);
            $table->string('kaitan_peristiwa_penerimaan');
            $table->string('tempat_penerimaan');
            $table->date('tanggal_penerimaan');
            $table->string('nama_pemberi');
            $table->string('pekerjaan_jabatan');
            $table->string('alamat_telepon_email');
            $table->string('hubungan_penerima');
            $table->string('alasan_pemberian');
            $table->text('kronologi');
            $table->text('catatan')->nullable();
            

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
        Schema::create('log_trans_pengaduan_gratifikasi', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan_gratifikasi')->unsigned();
            $table->string('nama');
            $table->string('nip_nopeg');
            $table->string('jabatan_unit_kerja');
            $table->string('jenis_bentuk_penerimaan');
            $table->decimal('harga_nilai_nominal',20,2);
            $table->string('kaitan_peristiwa_penerimaan');
            $table->string('tempat_penerimaan');
            $table->date('tanggal_penerimaan');
            $table->string('nama_pemberi');
            $table->string('pekerjaan_jabatan');
            $table->string('alamat_telepon_email');
            $table->string('hubungan_penerima');
            $table->string('alasan_pemberian');
            $table->text('kronologi');
            $table->text('catatan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_pengaduan_gratifikasi_bukti', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengaduan_gratifikasi')->unsigned();
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_pengaduan_gratifikasi')->references('id')->on('trans_pengaduan_gratifikasi');
        });
        Schema::create('log_trans_pengaduan_gratifikasi_bukti', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan_gratifikasi_bukti')->unsigned();
            $table->integer('id_pengaduan_gratifikasi')->unsigned();
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::create('trans_pengaduan_sponsor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('nip_nopeg');
            $table->string('jabatan_unit_kerja');
            $table->string('jenis_bentuk_penerimaan');
            $table->decimal('harga_nilai_nominal',20,2);
            $table->string('kaitan_peristiwa_penerimaan');
            $table->string('tempat_penerimaan');
            $table->date('tanggal_penerimaan');
            $table->string('nama_pemberi');
            $table->string('pekerjaan_jabatan');
            $table->string('alamat_telepon_email');
            $table->string('hubungan_penerima');
            $table->string('alasan_pemberian');
            $table->text('kronologi');
            $table->text('catatan')->nullable();
            

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
        Schema::create('log_trans_pengaduan_sponsor', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan_sponsor')->unsigned();
            $table->string('nama');
            $table->string('nip_nopeg');
            $table->string('jabatan_unit_kerja');
            $table->string('jenis_bentuk_penerimaan');
            $table->decimal('harga_nilai_nominal',20,2);
            $table->string('kaitan_peristiwa_penerimaan');
            $table->string('tempat_penerimaan');
            $table->date('tanggal_penerimaan');
            $table->string('nama_pemberi');
            $table->string('pekerjaan_jabatan');
            $table->string('alamat_telepon_email');
            $table->string('hubungan_penerima');
            $table->string('alasan_pemberian');
            $table->text('kronologi');
            $table->text('catatan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_pengaduan_sponsor_bukti', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengaduan_sponsor')->unsigned();
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_pengaduan_sponsor')->references('id')->on('trans_pengaduan_sponsor');
        });
        Schema::create('log_trans_pengaduan_sponsor_bukti', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan_sponsor_bukti')->unsigned();
            $table->integer('id_pengaduan_sponsor')->unsigned();
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::dropIfExists('trans_pengaduan_gratifikasi_bukti');
        Schema::dropIfExists('log_trans_pengaduan_gratifikasi_bukti');
        Schema::dropIfExists('trans_pengaduan_gratifikasi');
        Schema::dropIfExists('log_trans_pengaduan_gratifikasi');


    }
}
