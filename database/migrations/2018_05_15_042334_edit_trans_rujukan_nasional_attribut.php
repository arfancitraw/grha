<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTransRujukanNasionalAttribut extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->text('cara_merujuk')->nullable();
        });
         
        Schema::table('log_trans_rujukan_nasional', function(Blueprint $table){
            $table->text('cara_merujuk')->nullable();
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->dropColumn('cara_merujuk')->nullable();
        });
         
        Schema::table('log_trans_rujukan_nasional', function(Blueprint $table){
            $table->dropColumn('cara_merujuk')->nullable();
        });
    }
}
