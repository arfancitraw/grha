<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveOnListDokterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_list_dokter', function (Blueprint $table) {
            $table->boolean('aktif')->default(0);
        });
        Schema::table('log_trans_list_dokter', function (Blueprint $table) {
            $table->boolean('aktif')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_list_dokter', function (Blueprint $table) {
            $table->dropColumn('aktif');
        });
        Schema::table('log_trans_list_dokter', function (Blueprint $table) {
            $table->dropColumn('aktif');
        });
    }
}
