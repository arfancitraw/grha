<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateKategoriTanyaJawabTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_kategori_tanya_jawab', function (Blueprint $table) {
            $table->boolean('pernyataan')->default(0);
        });
        Schema::table('log_ref_kategori_tanya_jawab', function (Blueprint $table) {
            $table->boolean('pernyataan')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_kategori_tanya_jawab', function (Blueprint $table) {
            $table->dropColumn('pernyataan');
        });
        Schema::table('log_ref_kategori_tanya_jawab', function (Blueprint $table) {
            $table->dropColumn('pernyataan');
        });
    }
}
