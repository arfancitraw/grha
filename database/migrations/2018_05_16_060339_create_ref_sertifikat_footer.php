<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefSertifikatFooter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_sertifikat_footer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',150);
            $table->string('gambar',150);
            $table->text('keterangan');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
        Schema::create('log_ref_sertifikat_footer', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_sertifikat_footer')->unsigned();
            $table->string('nama',150);
            $table->string('gambar',150);
            $table->text('keterangan');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_sertifikat_footer');
        Schema::dropIfExists('log_ref_sertifikat_footer');
    }
}
