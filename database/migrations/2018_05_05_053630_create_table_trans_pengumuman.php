<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransPengumuman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pengumuman', function (Blueprint $table) {
        $table->increments('id');
        $table->string('judul',100);
        $table->text('konten');
        $table->text('keterangan');
        $table->string('lampiran',100)->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();
    });
       Schema::create('log_trans_pengumuman', function(Blueprint $table){
        $table->increments('id');
        $table->integer('id_pengumuman')->unsigned();
        $table->string('judul',100);
        $table->text('konten');
        $table->text('keterangan');
        $table->string('lampiran',100)->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_pengumuman');
        Schema::dropIfExists('log_trans_pengumuman');
    }
}
