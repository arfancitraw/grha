<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePengaduanBukti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('trans_pengaduan_bukti', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengaduan')->unsigned();
            $table->string('nama');
            $table->integer('jumlah');
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_pengaduan')->references('id')->on('trans_pengaduan');
        });
        Schema::create('log_trans_pengaduan_bukti', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan_bukti')->unsigned();
            $table->integer('id_pengaduan')->unsigned();
            $table->string('nama');
            $table->integer('jumlah');
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_pengaduan_bukti');
        Schema::dropIfExists('log_trans_pengaduan_bukti');
    }
}
