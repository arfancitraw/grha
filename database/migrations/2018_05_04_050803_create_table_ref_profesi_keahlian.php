<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefProfesiKeahlian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_profesi_keahlian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('photo',100)->nullable();
            $table->integer('status');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_profesi_keahlian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_profesi_keahlian')->unsigned();
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('photo',100)->nullable();
            $table->integer('status');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_profesi_keahlian');
        Schema::dropIfExists('ref_profesi_keahlian');
    }
}
