<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransSukamanFasilitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_sukaman_fasilitas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->string('gambar');
            $table->text('deskripsi')->nullabel();
            $table->string('icon');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
        Schema::create('log_trans_sukaman_fasilitas', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_sukaman_fasilitas')->unsigned();
            $table->string('judul');
            $table->string('gambar');
            $table->text('deskripsi')->nullabel();
            $table->string('icon');
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('trans_sukaman_fasilitas');
        Schema::dropIfExists('log_trans_sukaman_fasilitas');
    }
}
