<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInformasiLampiranForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_informasi_lampiran', function (Blueprint $table) {
            $table->dropForeign(['informasi_id']);
        });
        Schema::table('trans_informasi_lampiran', function (Blueprint $table) {
            $table->foreign('informasi_id')
                  ->references('id')
                  ->on('trans_informasi')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_informasi_lampiran', function (Blueprint $table) {
            $table->dropForeign(['informasi_id']);
        });
        Schema::table('trans_informasi_lampiran', function (Blueprint $table) {
            $table->foreign('informasi_id')
                  ->references('id')
                  ->on('trans_informasi')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }
}
