<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefMediaSosial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_media_sosial', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',150);
            $table->string('icon',150);
            $table->text('link');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
        Schema::create('log_ref_media_sosial', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_media_sosial')->unsigned();
            $table->string('nama',150);
            $table->string('icon',150);
            $table->text('link');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_media_sosial');
        Schema::dropIfExists('log_ref_media_sosial');
    }
}
