<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemakePengaduanTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('trans_pengaduan_saksi');
        Schema::dropIfExists('log_trans_pengaduan_saksi');
        
        Schema::dropIfExists('trans_pengaduan_bukti');
        Schema::dropIfExists('log_trans_pengaduan_bukti');
        
        Schema::dropIfExists('trans_pengaduan');
        Schema::dropIfExists('log_trans_pengaduan');
        
        Schema::create('trans_pengaduan', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('tipe')->default(0)->comment('0: Etik, 1:WBS, 2:Masyarakat');
            $table->string('nama');
            $table->boolean('status')->default(0);
            $table->string('telepon');
            $table->string('email');
            $table->string('perihal');
            $table->text('uraian');
            $table->string('bukti');
            $table->text('keterangan')->nullable();

            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();
        });
        Schema::create('log_trans_pengaduan', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan')->unsigned();
            $table->tinyInteger('tipe')->default(0)->comment('0: Etik, 1:WBS, 2:Masyarakat');
            $table->string('nama');
            $table->boolean('status')->default(0);
            $table->string('telepon');
            $table->string('email');
            $table->string('perihal');
            $table->text('uraian');
            $table->string('bukti');
            $table->text('keterangan')->nullable();

            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_pengaduan');
        Schema::dropIfExists('log_trans_pengaduan');
        Schema::create('trans_pengaduan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->text('alamat');
            $table->string('telp_rumah')->nullable();
            $table->string('telp_kantor')->nullable();
            $table->string('telp_seluler')->nullable();
            $table->string('email');
            $table->integer('hari');
            $table->string('pukul_awal');
            $table->string('pukul_akhir');
            $table->string('no_id')->nullable();
            $table->string('pangkat')->nullable();
            $table->string('jabatan')->nullable();
            $table->date('tgl_kerja')->nullable();
            $table->string('divisi')->nullable();
            $table->string('no_sk')->nullable();
            $table->integer('rahasia');
            $table->text('alasan_rahasia')->nullable();
            $table->text('kronologi')->nullable();
            $table->text('pihak_terkait')->nullable();
            $table->string('lokasi_kejadian')->nullable();
            $table->date('tanggal_kejadian');
            $table->text('penyebab')->nullable();
            $table->text('dampak')->nullable();
            $table->text('alasan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_trans_pengaduan', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan')->unsigned();
            $table->string('nama');
            $table->text('alamat');
            $table->string('telp_rumah')->nullable();
            $table->string('telp_kantor')->nullable();
            $table->string('telp_seluler')->nullable();
            $table->string('email');
            $table->integer('hari');
            $table->string('pukul_awal');
            $table->string('pukul_akhir');
            $table->string('no_id')->nullable();
            $table->string('pangkat')->nullable();
            $table->string('jabatan')->nullable();
            $table->date('tgl_kerja')->nullable();
            $table->string('divisi')->nullable();
            $table->string('no_sk')->nullable();
            $table->integer('rahasia');
            $table->text('alasan_rahasia')->nullable();
            $table->text('kronologi')->nullable();
            $table->text('pihak_terkait')->nullable();
            $table->string('lokasi_kejadian')->nullable();
            $table->date('tanggal_kejadian');
            $table->text('penyebab')->nullable();
            $table->text('dampak')->nullable();
            $table->text('alasan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });


        Schema::create('trans_pengaduan_bukti', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengaduan')->unsigned();
            $table->string('nama');
            $table->integer('jumlah');
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_pengaduan')->references('id')->on('trans_pengaduan');
        });
        Schema::create('log_trans_pengaduan_bukti', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan_bukti')->unsigned();
            $table->integer('id_pengaduan')->unsigned();
            $table->string('nama');
            $table->integer('jumlah');
            $table->string('file');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        
        Schema::create('trans_pengaduan_saksi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengaduan')->unsigned();
            $table->string('nama');
            $table->text('alamat');
            $table->text('telp');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_pengaduan')->references('id')->on('trans_pengaduan');
        });
        Schema::create('log_trans_pengaduan_saksi', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan_saksi')->unsigned();
            $table->integer('id_pengaduan')->unsigned();
            $table->string('nama');
            $table->text('alamat');
            $table->text('telp');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }
}
