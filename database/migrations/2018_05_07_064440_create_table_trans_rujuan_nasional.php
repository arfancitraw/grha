<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransRujuanNasional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_rujukan_nasional', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul',100);
            $table->text('pengantar')->nullable();
            $table->text('profile')->nullable();
            $table->text('kinerja')->nullable();
            $table->text('penelitian')->nullable();
            $table->text('jejaring')->nullable();
            $table->text('prosedur')->nullable();
            $table->string('aksesibilitas',100);
            $table->text('alamat')->nullable();
            $table->string('no_tlpn',10);
            $table->string('email',100);
            $table->text('deskripsi')->nullable();
            $table->string('photo',100)->nullable();
            $table->integer('status')->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_trans_rujukan_nasional', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_trans_rujukan_nasional')->unsigned();
            $table->string('judul',100);
            $table->text('pengantar')->nullable();
            $table->text('profile')->nullable();
            $table->text('kinerja')->nullable();
            $table->text('penelitian')->nullable();
            $table->text('jejaring')->nullable();
            $table->text('prosedur')->nullable();
            $table->string('aksesibilitas',100);
            $table->text('alamat')->nullable();
            $table->string('no_tlpn',10);
            $table->string('email',100);
            $table->text('deskripsi')->nullable();
            $table->string('photo',100)->nullable();
            $table->integer('status')->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_rujukan_nasional');
        Schema::dropIfExists('log_trans_rujukan_nasional');
    }
}
