<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransRujukanNasionalForAgendaRujukan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
   {
         Schema::create('trans_rujukan_agenda', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_rujukan_nasional')->references('id')->on('trans_rujukan')->onDelete('cascade');
            $table->string('nama');
            $table->mediumtext('keterangan')->nullable();
            $table->string('tahun')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
         
        Schema::create('log_trans_rujukan_agenda', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_agenda')->unsigned();
            $table->integer('id_rujukan_nasional')->references('id')->on('trans_rujukan')->onDelete('cascade');
            $table->string('nama');
            $table->mediumtext('keterangan')->nullable();
            $table->string('tahun')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_rujukan_agenda');
        Schema::dropIfExists('trans_rujukan_agenda');
    }
}
