<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransJadwalDokter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_jadwal_dokter', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('list_dokter_id')->unsigned();
        $table->integer('hari_praktek');
        $table->string('jam_mulai_praktek',10);
        $table->string('jam_selesai_praktek',10);

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();

        $table->foreign('list_dokter_id')->references('id')->on('trans_list_dokter');
    });
       Schema::create('log_trans_jadwal_dokter', function(Blueprint $table){
        $table->increments('id');
        $table->integer('id_jadwal_dokter')->unsigned();
        $table->integer('list_dokter_id')->unsigned();
        $table->integer('hari_praktek');
        $table->string('jam_mulai_praktek',10);
        $table->string('jam_selesai_praktek',10);

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_jadwal_dokter');
        Schema::dropIfExists('log_trans_jadwal_dokter');
    }
}
