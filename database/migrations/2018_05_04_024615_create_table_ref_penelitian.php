<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefPenelitian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_penelitian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('photo',100)->nullable();
            $table->integer('status')->default(0);

            $table->integer('created_by')->unsigned();
            $table->timestamp('created_at');
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('log_penelitian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_penelitian')->unsigned();
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('photo',100)->nullable();
            $table->integer('status');

            $table->integer('created_by')->unsigned();
            $table->timestamp('created_at');
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_penelitian');
        Schema::dropIfExists('log_penelitian');
    }
}
