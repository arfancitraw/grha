<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableLayananAddHomeFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_layanan', function (Blueprint $table) {
            $table->string('icon')->nullable();
            $table->boolean('home')->default(0)->after('files');
        });
        Schema::table('log_trans_layanan', function (Blueprint $table) {
            $table->string('icon')->nullable();
            $table->boolean('home')->default(0)->after('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_layanan', function (Blueprint $table) {
            $table->dropColumn('icon');
            $table->dropColumn('home');
        });
        Schema::table('trans_layanan', function (Blueprint $table) {
            $table->dropColumn('icon');
            $table->dropColumn('home');
        });
    }
}
