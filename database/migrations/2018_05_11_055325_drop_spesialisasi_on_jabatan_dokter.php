<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropSpesialisasiOnJabatanDokter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('ref_jabatan_dokter', function (Blueprint $table) {
            $table->dropForeign('ref_jabatan_dokter_spesialisasi_id_foreign');
            $table->dropColumn('spesialisasi_id');
        });
        Schema::table('log_jabatan_dokter', function(Blueprint $table){
            $table->dropColumn('spesialisasi_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('ref_jabatan_dokter', function (Blueprint $table) {
            $table->integer('spesialisasi_id')->unsigned()->nullable()->after('id');
            $table->foreign('spesialisasi_id')->references('id')->on('ref_spesialisasi');
        });
        Schema::table('log_jabatan_dokter', function(Blueprint $table){
            $table->integer('spesialisasi_id')->nullable()->after('id_jabatan_dokter');
        });
    }
}
