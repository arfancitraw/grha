<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransKomiteEtik extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_komite_etik', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->integer('jabatan')->unsigned();
            $table->string('departemen');
            $table->date('tanggal_efektif_awal')->nullabel();
            $table->date('tanggal_efektif_akhir')->nullabel();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('jabatan')->references('id')->on('ref_jabatan_dokter');

        });
        Schema::create('log_trans_komite_etik', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_komite_etik')->unsigned();
            $table->string('nama');
            $table->integer('jabatan')->unsigned();
            $table->string('departemen');
            $table->date('tanggal_efektif_awal')->nullabel();
            $table->date('tanggal_efektif_akhir')->nullabel();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_komite_etik');
        Schema::dropIfExists('log_trans_komite_etik');
    }
}
