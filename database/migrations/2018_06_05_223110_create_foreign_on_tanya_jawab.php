<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignOnTanyaJawab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('trans_tanya_jawab', function (Blueprint $table) {
            $table->foreign('id_kategori_tanya_jawab')
                  ->references('id')
                  ->on('ref_kategori_tanya_jawab');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trans_tanya_jawab', function (Blueprint $table) {
            $table->dropForeign(['id_kategori_tanya_jawab']);
        });
    }
}
