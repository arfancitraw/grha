<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefJabatanDokter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('ref_jabatan_dokter', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('spesialisasi_id')->unsigned();
            $table->string('jabatan');
            $table->text('deskripsi')->nullabel();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('spesialisasi_id')->references('id')->on('ref_spesialisasi');
        });
        Schema::create('log_jabatan_dokter', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_jabatan_dokter')->unsigned();
            $table->integer('spesialisasi_id');
            $table->string('jabatan');
            $table->text('deskripsi')->nullabel();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_jabatan_dokter');
        Schema::dropIfExists('log_jabatan_dokter');
    }
}
