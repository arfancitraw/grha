<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransJadwalDiklatTambahKolomJam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('log_trans_jadwal_diklat');
        Schema::dropIfExists('trans_jadwal_diklat');


        Schema::create('trans_jadwal_diklat', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('gambar')->nullable();
            $table->date('tanggal')->nullable();
            $table->time('jam')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
         
        Schema::create('log_trans_jadwal_diklat', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_jadwal_diklat')->unsigned();
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('gambar')->nullable();
            $table->date('tanggal')->nullable();
            $table->time('jam')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_jadwal_diklat');
        Schema::dropIfExists('trans_jadwal_diklat');

        Schema::create('trans_jadwal_diklat', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('gambar')->nullable();
            $table->datetime('tanggal')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
         
        Schema::create('log_trans_jadwal_diklat', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_jadwal_diklat')->unsigned();
            $table->string('judul');
            $table->mediumtext('konten')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('gambar')->nullable();
            $table->datetime('tanggal')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }
}
