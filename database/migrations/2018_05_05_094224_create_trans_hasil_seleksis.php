<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransHasilSeleksis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('trans_hasil_seleksi', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('karir_id')->unsigned();
        $table->text('keterangan')->nullable();
        $table->string('lampiran',150)->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();

        $table->foreign('karir_id')->references('id')->on('trans_karir');
    });
      Schema::create('log_trans_hasil_seleksi', function(Blueprint $table){
        $table->increments('id');
        $table->integer('id_hasil_seleksi')->unsigned();
        $table->integer('karir_id')->unsigned();
        $table->text('keterangan')->nullable();
        $table->string('lampiran',150)->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();
    });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_hasil_seleksi');
        Schema::dropIfExists('log_trans_hasil_seleksi');
    }
}
