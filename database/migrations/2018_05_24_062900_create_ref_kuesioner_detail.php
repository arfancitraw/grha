<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefKuesionerDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
   Schema::create('ref_kuesioner_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kuesioner_id')->unsigned();
            $table->text('pertanyaan');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('kuesioner_id')->references('id')->on('ref_kuesioner_periode');
        });
        Schema::create('log_ref_kuesioner_detail', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_kuesioner_detail')->unsigned();
            $table->integer('kuesioner_id')->unsigned();
            $table->text('pertanyaan');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_kuesioner_detail');
        Schema::dropIfExists('log_ref_kuesioner_detail');
    }
}
