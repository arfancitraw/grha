<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreatePublikasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('trans_list_publikasi');
        Schema::dropIfExists('log_trans_list_publikasi');

        Schema::create('trans_list_publikasi', function (Blueprint $table) {
            $table->increments('id');
            $table->text('judul');
            $table->integer('tahun');
            $table->text('peneliti_utama');
            $table->text('peneliti_pendamping');
            $table->text('citasi_jurnal');
            $table->text('link')->nullable();
            $table->text('file')->nullable();
            $table->text('deskripsi')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('log_trans_list_publikasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_list_publikasi')->unsigned();
            $table->text('judul');
            $table->integer('tahun');
            $table->text('peneliti_utama');
            $table->text('peneliti_pendamping');
            $table->text('citasi_jurnal');
            $table->text('link')->nullable();
            $table->text('file')->nullable();
            $table->text('deskripsi')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_list_publikasi');
        Schema::dropIfExists('log_trans_list_publikasi');
        
        Schema::create('trans_list_publikasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('tahun');
            $table->string('link');
            $table->text('deskripsi')->nullabel();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
        Schema::create('log_trans_list_publikasi', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_list_publikasi')->unsigned();
            $table->string('nama');
            $table->string('tahun');
            $table->string('link');
            $table->text('deskripsi')->nullabel();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }
}
