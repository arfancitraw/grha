<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranslateEnInfoLitbang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('trans_info_litbang', function (Blueprint $table) {
            $table->string('judul_en',191)->after('judul')->nullable();
            $table->text('konten_en')->after('konten')->nullable();
        });

        Schema::table('log_trans_info_litbang', function (Blueprint $table) {
            $table->string('judul_en',191)->after('judul')->nullable();
            $table->text('konten_en')->after('konten')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trans_info_litbang', function (Blueprint $table) {
            $table->dropColumn(['judul_en','konten_en']);
        });

        Schema::table('log_trans_info_litbang', function (Blueprint $table) {
            $table->dropColumn(['judul_en','konten_en']);
        });
    }
}
