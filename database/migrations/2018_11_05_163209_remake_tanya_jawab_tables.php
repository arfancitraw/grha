<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemakeTanyaJawabTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('log_trans_jawaban_pertanyaan');
        Schema::dropIfExists('trans_jawaban_pertanyaan');

        Schema::dropIfExists('log_trans_tambahan_tanya_jawab');
        Schema::dropIfExists('trans_tambahan_tanya_jawab');

        Schema::dropIfExists('log_trans_tanya_jawab');
        Schema::dropIfExists('trans_tanya_jawab');

        Schema::create('trans_tanya_jawab', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kategori')->unsigned();

            $table->string('nama');
            $table->string('email');
            $table->string('judul');
            $table->text('konten');
            $table->boolean('rahasia')->default(0);
            $table->boolean('publish')->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_kategori')
                  ->references('id')
                  ->on('ref_kategori_tanya_jawab');
        });

        Schema::create('trans_tanya_jawab_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tanya_jawab')->unsigned();

            $table->string('nama');
            $table->string('email');
            $table->string('referensi')->nullable();
            $table->string('judul');
            $table->text('konten');
            $table->boolean('internal')->default(0);
            $table->boolean('rahasia')->default(0);
            $table->boolean('publish')->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_tanya_jawab')
                  ->references('id')
                  ->on('trans_tanya_jawab')
                  ->onDelete('cascade');
        });

        Schema::create('trans_tanya_jawab_access', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tanya_jawab')->unsigned();

            $table->string('ip');

            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();

            $table->foreign('id_tanya_jawab')
                  ->references('id')
                  ->on('trans_tanya_jawab')
                  ->onDelete('cascade');
        });

        Schema::create('log_trans_tanya_jawab', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tanya_jawab')->unsigned();
            $table->integer('id_kategori')->unsigned();

            $table->string('nama');
            $table->string('email');
            $table->string('judul');
            $table->text('konten');
            $table->boolean('rahasia')->default(0);
            $table->boolean('publish')->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('log_trans_tanya_jawab_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tanya_jawab_detail')->unsigned();
            $table->integer('id_tanya_jawab')->unsigned();

            $table->string('nama');
            $table->string('email');
            $table->string('referensi')->nullable();
            $table->string('judul');
            $table->text('konten');
            $table->boolean('internal')->default(0);
            $table->boolean('rahasia')->default(0);
            $table->boolean('publish')->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_tanya_jawab_access');

        Schema::dropIfExists('log_trans_tanya_jawab_detail');
        Schema::dropIfExists('trans_tanya_jawab_detail');

        Schema::dropIfExists('log_trans_tanya_jawab');
        Schema::dropIfExists('trans_tanya_jawab');

        Schema::create('trans_tanya_jawab', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('email');
            $table->integer('id_kategori_tanya_jawab')->unsigned();
            $table->string('judul');
            $table->text('konten');
            $table->integer('id_tanya_jawab_pertanyaan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_kategori_tanya_jawab')->references('id')->on('ref_kategori_tanya_jawab');
            $table->foreign('id_tanya_jawab_pertanyaan')->references('id')->on('trans_tanya_jawab');
        });

        Schema::create('log_trans_tanya_jawab', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_tanya_jawab')->unsigned();
            $table->string('nama');
            $table->string('email');
            $table->integer('id_kategori_tanya_jawab')->unsigned();
            $table->string('judul');
            $table->text('konten');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_jawaban_pertanyaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_trans_tanya_jawab')->unsigned();
            $table->string('referensi');
            $table->text('jawaban');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
            $table->foreign('id_trans_tanya_jawab')->references('id')->on('trans_tanya_jawab');
        });
        Schema::create('log_trans_jawaban_pertanyaan', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_tanya_jawab')->unsigned();
            $table->string('referensi');
            $table->text('jawaban');
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        
        Schema::create('trans_tambahan_tanya_jawab', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('email');
            $table->integer('id_pertanyaan')->unsigned();
            $table->string('judul');
            $table->text('konten');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_pertanyaan')->references('id')->on('trans_tanya_jawab');
        });
        Schema::create('log_trans_tambahan_tanya_jawab', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_tambahan_tanya_jawab')->unsigned();
            $table->string('nama');
            $table->string('email');
            $table->integer('id_pertanyaan')->unsigned();
            $table->string('judul');
            $table->text('konten');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }
}
