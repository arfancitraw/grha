<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsForRujukanTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->text('produk')->nullable();
            $table->text('tim')->nullable();
            $table->text('fasilitas')->nullable();
            $table->text('agenda')->nullable();
            $table->text('aksesibilitas')->nullable();
        });
        Schema::table('log_trans_rujukan_nasional', function (Blueprint $table) {
            $table->text('produk')->nullable();
            $table->text('tim')->nullable();
            $table->text('fasilitas')->nullable();
            $table->text('agenda')->nullable();
            $table->text('aksesibilitas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_rujukan_nasional', function (Blueprint $table) {
            $table->dropColumn('produk');
            $table->dropColumn('tim');
            $table->dropColumn('fasilitas');
            $table->dropColumn('agenda');
            $table->dropColumn('aksesibilitas');
        });
        Schema::table('trans_rujukan_nasional', function (Blueprint $table) {
            $table->dropColumn('produk');
            $table->dropColumn('tim');
            $table->dropColumn('fasilitas');
            $table->dropColumn('agenda');
            $table->dropColumn('aksesibilitas');
        });
    }
}
