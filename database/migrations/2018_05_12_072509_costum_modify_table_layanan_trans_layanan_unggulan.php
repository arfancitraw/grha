<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

class CostumModifyTableLayananTransLayananUnggulan extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::rename('log_trans_layanan_unggulan', 'log_trans_layanan');
        Schema::rename('trans_layanan_unggulan', 'trans_layanan');
        DB::table('trans_layanan')->truncate();
        DB::table('log_trans_layanan')->truncate();
        Schema::table('trans_layanan', function(Blueprint $table){
            $table->string('slug');
        });
        Schema::table('log_trans_layanan', function(Blueprint $table){
            $table->string('slug');
            $table->renameColumn('id_layanan_unggulan', 'id_layanan');
        });
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::table('trans_layanan', function(Blueprint $table){
            $table->dropColumn('slug');
        });
        Schema::table('log_trans_layanan', function(Blueprint $table){
            $table->dropColumn('slug');
            $table->renameColumn('id_layanan', 'id_layanan_unggulan');            
        });
        Schema::rename('log_trans_layanan', 'log_trans_layanan_unggulan');
        Schema::rename('trans_layanan', 'trans_layanan_unggulan');
    }
}
