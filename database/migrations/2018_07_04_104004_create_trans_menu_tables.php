<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransMenuTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->string('tautan');
            $table->integer('urutan');
            $table->integer('induk_id')->unsigned();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('induk_id')->references('id')->on('trans_menu');
        });
        Schema::create('log_trans_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_menu')->unsigned();
            $table->string('judul');
            $table->string('tautan');
            $table->integer('urutan');
            $table->integer('induk_id')->unsigned();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_menu');
        Schema::dropIfExists('trans_menu');
    }
}
