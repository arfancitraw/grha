<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignDokterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_dokter_keahlian', function (Blueprint $table) {
            $table->dropForeign(['list_dokter_id']);
        });
        Schema::table('trans_dokter_keahlian', function (Blueprint $table) {
            $table->foreign('list_dokter_id')
                  ->references('id')
                  ->on('trans_list_dokter')
                  ->onDelete('cascade');
        });
        
        Schema::table('trans_dokter_pendidikan', function (Blueprint $table) {
            $table->dropForeign(['list_dokter_id']);
        });
        Schema::table('trans_dokter_pendidikan', function (Blueprint $table) {
            $table->foreign('list_dokter_id')
                  ->references('id')
                  ->on('trans_list_dokter')
                  ->onDelete('cascade');
        });

        Schema::table('trans_jadwal_dokter', function (Blueprint $table) {
            $table->dropForeign(['list_dokter_id']);
        });
        Schema::table('trans_jadwal_dokter', function (Blueprint $table) {
            $table->foreign('list_dokter_id')
                  ->references('id')
                  ->on('trans_list_dokter')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_dokter_keahlian', function (Blueprint $table) {
            $table->dropForeign(['list_dokter_id']);
        });
        Schema::table('trans_dokter_keahlian', function (Blueprint $table) {
            $table->foreign('list_dokter_id')
                  ->references('id')
                  ->on('trans_list_dokter');
        });
        
        Schema::table('trans_dokter_pendidikan', function (Blueprint $table) {
            $table->dropForeign(['list_dokter_id']);
        });
        Schema::table('trans_dokter_pendidikan', function (Blueprint $table) {
            $table->foreign('list_dokter_id')
                  ->references('id')
                  ->on('trans_list_dokter');
        });

        Schema::table('trans_jadwal_dokter', function (Blueprint $table) {
            $table->dropForeign(['list_dokter_id']);
        });
        Schema::table('trans_jadwal_dokter', function (Blueprint $table) {
            $table->foreign('list_dokter_id')
                  ->references('id')
                  ->on('trans_list_dokter');
        });
    }
}
