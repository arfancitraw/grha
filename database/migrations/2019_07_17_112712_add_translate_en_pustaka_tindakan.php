<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranslateEnPustakaTindakan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('trans_pustaka_tindakan', function (Blueprint $table) {
            $table->string('nama_en',191)->after('nama')->nullable();
            $table->text('indikasi_en')->after('indikasi')->nullable();
            $table->text('tujuan_en')->after('tujuan')->nullable();
            $table->text('tatacara_en')->after('tatacara')->nullable();
            $table->text('risiko_en')->after('risiko')->nullable();
            $table->text('alternatif_en')->after('alternatif')->nullable();
            $table->text('prognosis_dilakukan_en')->after('prognosis_dilakukan')->nullable();
            $table->text('prognosis_tidak_en')->after('prognosis_tidak')->nullable();
            $table->text('perluasan_en')->after('perluasan')->nullable();
            $table->text('persiapan_en')->after('persiapan')->nullable();
            $table->text('penjelasan_en')->after('penjelasan')->nullable();
        });

        Schema::table('log_trans_pustaka_tindakan', function (Blueprint $table) {
            $table->string('nama_en',191)->after('nama')->nullable();
            $table->text('indikasi_en')->after('indikasi')->nullable();
            $table->text('tujuan_en')->after('tujuan')->nullable();
            $table->text('tatacara_en')->after('tatacara')->nullable();
            $table->text('risiko_en')->after('risiko')->nullable();
            $table->text('alternatif_en')->after('alternatif')->nullable();
            $table->text('prognosis_dilakukan_en')->after('prognosis_dilakukan')->nullable();
            $table->text('prognosis_tidak_en')->after('prognosis_tidak')->nullable();
            $table->text('perluasan_en')->after('perluasan')->nullable();
            $table->text('persiapan_en')->after('persiapan')->nullable();
            $table->text('penjelasan_en')->after('penjelasan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trans_pustaka_tindakan', function (Blueprint $table) {
            $table->dropColumn(['nama_en','indikasi_en','tujuan_en','tatacara_en','risiko_en','alternatif_en','prognosis_tidak_en','prognosis_dilakukan_en','perluasan_en','persiapan_en','penjelasan_en']);
        });

        Schema::table('log_trans_pustaka_tindakan', function (Blueprint $table) {
            $table->dropColumn(['nama_en','indikasi_en','tujuan_en','tatacara_en','risiko_en','alternatif_en','prognosis_tidak_en','prognosis_dilakukan_en','perluasan_en','persiapan_en','penjelasan_en']);
        });
    }
}
