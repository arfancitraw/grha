<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKontenEnListArtikel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('trans_list_artikel', function (Blueprint $table) {
            $table->string('judul_en',100)->after('judul')->nullable();
            $table->text('konten_en')->after('konten')->nullable();
            $table->text('deskripsi_en')->after('deskripsi')->nullable();
        });

        Schema::table('log_trans_list_artikel', function (Blueprint $table) {
            $table->string('judul_en',100)->after('judul')->nullable();
            $table->text('konten_en')->after('konten')->nullable();
            $table->text('deskripsi_en')->after('deskripsi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('trans_list_artikel', function (Blueprint $table) {
            $table->dropColumn(['judul_en','konten_en','deskripsi_en']);
        });

        Schema::table('log_trans_list_artikel', function (Blueprint $table) {
            $table->dropColumn(['judul_en','konten_en','deskripsi_en']);
        });
    }
}
