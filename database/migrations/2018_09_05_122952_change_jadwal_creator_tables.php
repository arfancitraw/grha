<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeJadwalCreatorTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_jadwal_dokter', function (Blueprint $table) {
            $table->integer('created_by')
                  ->unsigned()
                  ->nullable()
                  ->change();
        });
        Schema::table('log_trans_jadwal_dokter', function (Blueprint $table) {
            $table->integer('created_by')
                  ->unsigned()
                  ->nullable()
                  ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_jadwal_dokter', function (Blueprint $table) {
            $table->integer('created_by')
                  ->unsigned()
                  ->change();
        });
        Schema::table('log_trans_jadwal_dokter', function (Blueprint $table) {
            $table->integer('created_by')
                  ->unsigned()
                  ->change();
        });
    }
}
