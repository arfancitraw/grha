<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefSlider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_slider', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul', 100);
            $table->string('sub_judul');
            $table->text('deskripsi')->nullable();
            $table->string('url', 100);
            $table->string('photo', 100)->nullable();
            $table->integer('posisi');
            $table->integer('status');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_slider', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_slider')->unsigned();
            $table->string('judul', 100);
            $table->string('sub_judul');
            $table->text('deskripsi')->nullable();
            $table->string('url', 100);
            $table->string('photo', 100)->nullable();
            $table->integer('posisi');
            $table->integer('status');

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_slider');
        Schema::dropIfExists('ref_slider');
    }
}
