<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThreeTableRujukan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_rujukan_produk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_rujukan_nasional')->unsigned();
            $table->string('nama')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('gambar')->nullable();

            $table->foreign('id_rujukan_nasional')->references('id')->on('trans_rujukan_nasional')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
         
        Schema::create('log_trans_rujukan_produk', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_produk')->unsigned();
            $table->integer('id_rujukan_nasional')->unsigned();
            $table->string('nama')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('gambar')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_rujukan_agenda', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_rujukan_nasional')->unsigned();
            $table->string('nama')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('tahun')->nullable();

            $table->foreign('id_rujukan_nasional')->references('id')->on('trans_rujukan_nasional')->onDelete('cascade')->onUpdate('cascade');


            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
         
        Schema::create('log_trans_rujukan_agenda', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_agenda')->unsigned();
            $table->integer('id_rujukan_nasional')->unsigned();
            $table->string('nama')->nullable();
            $table->mediumtext('keterangan')->nullable();
            $table->string('tahun')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_rujukan_tim', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_rujukan_nasional')->unsigned();
            $table->integer('id_dokter')->unsigned()->nullable();
            $table->string('posisi')->nullable();

            $table->foreign('id_rujukan_nasional')->references('id')->on('trans_rujukan_nasional')->onDelete('cascade')->onUpdate('cascade');


            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
        });
         
        Schema::create('log_trans_rujukan_tim', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_trans_tim')->unsigned();
            $table->integer('id_rujukan_nasional')->unsigned();
            $table->integer('id_dokter')->unsigned();
            $table->string('posisi')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::table('trans_rujukan_nasional', function(Blueprint $table){
            $table->dropColumn('aksesibilitas')->nullable();
            $table->mediumtext('akses_deskripsi')->nullable();
            $table->string('kordinat')->nullable();

            $table->string('fasilitas_nama')->nullable();
            $table->string('fasilitas_pelayanan')->nullable();
        });

        Schema::table('log_trans_rujukan_nasional', function(Blueprint $table){
            $table->dropColumn('aksesibilitas')->nullable();
            $table->mediumtext('akses_deskripsi')->nullable();
            $table->string('kordinat')->nullable();

            $table->string('fasilitas_nama')->nullable();
            $table->string('fasilitas_pelayanan')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_rujukan_produk');
        Schema::dropIfExists('trans_rujukan_produk');

        Schema::dropIfExists('log_trans_rujukan_agenda');
        Schema::dropIfExists('trans_rujukan_agenda');

        Schema::dropIfExists('log_trans_rujukan_tim');
        Schema::dropIfExists('trans_rujukan_tim');

        Schema::table('trans_rujukan_nasional', function(Blueprint $table){
            $table->string('aksesibilitas')->nullable();
            $table->dropColumn('akses_deskripsi')->nullable();
            $table->dropColumn('kordinat')->nullable();

            $table->dropColumn('fasilitas_nama')->nullable();
            $table->dropColumn('fasilitas_pelayanan')->nullable();
        });

        Schema::table('log_trans_rujukan_nasional', function(Blueprint $table){
            $table->string('aksesibilitas')->nullable();
            $table->dropColumn('akses_deskripsi')->nullable();
            $table->dropColumn('kordinat')->nullable();

            $table->dropColumn('fasilitas_nama')->nullable();
            $table->dropColumn('fasilitas_pelayanan')->nullable();
        });
    }
}
