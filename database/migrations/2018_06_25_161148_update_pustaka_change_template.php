<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePustakaChangeTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_pustaka', function (Blueprint $table) {
            $table->text('pengertian')->nullable();
            $table->text('gejala')->nullable();
            $table->text('penyebab')->nullable();
            $table->text('penanganan')->nullable();
            $table->text('pencegahan')->nullable();
            $table->text('konten')->nullable()->change();
            $table->text('keterangan')->nullable()->change();
        });

        Schema::table('log_trans_pustaka', function(Blueprint $table){
            $table->text('pengertian')->nullable();
            $table->text('gejala')->nullable();
            $table->text('penyebab')->nullable();
            $table->text('penanganan')->nullable();
            $table->text('pencegahan')->nullable();
            $table->text('konten')->nullable()->change();
            $table->text('keterangan')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_pustaka', function(Blueprint $table){
            $table->dropColumn('pengertian');
            $table->dropColumn('gejala');
            $table->dropColumn('penyebab');
            $table->dropColumn('penanganan');
            $table->dropColumn('pencegahan');
            $table->text('konten')->change();
            $table->text('keterangan')->change();
        });

        Schema::table('trans_pustaka', function (Blueprint $table) {
            $table->dropColumn('pengertian');
            $table->dropColumn('gejala');
            $table->dropColumn('penyebab');
            $table->dropColumn('penanganan');
            $table->dropColumn('pencegahan');
            $table->text('konten')->change();
            $table->text('keterangan')->change();
        });
    }
}
