<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatLogTransAtrtikel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_trans_list_artikel', function (Blueprint $table) {
            $table->integer('kategori_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_list_artikel', function (Blueprint $table) {
            $table->dropColumn('kategori_id')->unsigned()->nullable();
        });
    }
}
