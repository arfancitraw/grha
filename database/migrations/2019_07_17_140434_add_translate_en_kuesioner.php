<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranslateEnKuesioner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('ref_kuesioner_detail', function (Blueprint $table) {
            $table->text('pertanyaan_en')->after('pertanyaan')->nullable();
        });

        Schema::table('log_ref_kuesioner_detail', function (Blueprint $table) {
            $table->text('pertanyaan_en')->after('pertanyaan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('ref_kuesioner_detail', function (Blueprint $table) {
            $table->dropColumn(['pertanyaan_en']);
        });

        Schema::table('log_ref_kuesioner_detail', function (Blueprint $table) {
            $table->dropColumn(['pertanyaan_en']);
        });
    }
}
