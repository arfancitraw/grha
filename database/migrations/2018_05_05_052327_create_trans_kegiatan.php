<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransKegiatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_kegiatan', function (Blueprint $table) {
        $table->increments('id');
        $table->string('nama_kegiatan',150);
        $table->integer('tipe_media')->unsigned();
        $table->text('deskripsi')->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();

        $table->foreign('tipe_media')->references('id')->on('ref_tipe_media');
    });
       Schema::create('log_trans_kegiatan', function(Blueprint $table){
        $table->increments('id');
        $table->integer('id_kegiatan')->unsigned();
        $table->string('nama_kegiatan',150);
        $table->integer('tipe_media')->unsigned();
        $table->text('deskripsi')->nullable();

        $table->timestamp('created_at');
        $table->integer('created_by')->unsigned();
        $table->timestamp('updated_at')->nullable();
        $table->integer('updated_by')->unsigned()->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_kegiatan');
        Schema::dropIfExists('log_trans_kegiatan');
    }
}
