<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransKuesioner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_kuesioner', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('email');
            $table->string('profesi');
            $table->integer('kepuasan_layanan');
            $table->integer('kepuasan_fasilitas_website');
            $table->text('feedback')->nullabel();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
        Schema::create('log_trans_kuesioner', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_kuesioner')->unsigned();
            $table->string('nama');
            $table->string('email');
            $table->string('profesi');
            $table->integer('kepuasan_layanan');
            $table->integer('kepuasan_fasilitas_website');
            $table->text('feedback')->nullabel();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_kuesioner');
        Schema::dropIfExists('log_trans_kuesioner');
    }
}
