<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateKuesionerNullableFeedback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_kuesioner_jawaban', function (Blueprint $table) {
            $table->text('feedback')->nullable()->change();
        });

        Schema::table('log_trans_kuesioner_jawaban', function(Blueprint $table){
            $table->text('feedback')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_kuesioner_jawaban', function(Blueprint $table){
            $table->text('feedback')->change();
        });

        Schema::table('trans_kuesioner_jawaban', function (Blueprint $table) {
            $table->text('feedback')->change();
        });
    }
}
