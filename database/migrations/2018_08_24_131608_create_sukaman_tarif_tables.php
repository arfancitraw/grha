<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSukamanTarifTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_sukaman_kategori_tarif', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->text('deskripsi')->nullable();
            $table->integer('prioritas')->default(999);
            $table->boolean('status')->default(0);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_trans_sukaman_kategori_tarif', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_kategori')->unsigned();
            $table->string('nama');
            $table->text('deskripsi')->nullable();
            $table->integer('prioritas')->default(999);
            $table->boolean('status')->default(0);
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_sukaman_tarif', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kategori')->unsigned();
            $table->string('nama');
            $table->text('deskripsi')->nullable();
            $table->integer('harga')->default(0);
            $table->string('warna')->default('default');
            $table->integer('prioritas')->default(999);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_kategori')->references('id')->on('trans_sukaman_kategori_tarif');
        });
        Schema::create('log_trans_sukaman_tarif', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_tarif')->unsigned();
            $table->integer('id_kategori')->unsigned();
            $table->string('nama');
            $table->text('deskripsi')->nullable();
            $table->integer('harga')->default(0);
            $table->string('warna')->default('default');
            $table->integer('prioritas')->default(999);
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        Schema::create('trans_sukaman_tarif_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tarif')->unsigned();
            $table->string('nama');
            $table->integer('check')->default(0);
            $table->integer('urutan')->default(999);

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('id_tarif')->references('id')->on('trans_sukaman_tarif');
        });
        Schema::create('log_trans_sukaman_tarif_detail', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_tarif_detail')->unsigned();
            $table->integer('id_tarif')->unsigned();
            $table->string('nama');
            $table->integer('check')->default(0);
            $table->integer('urutan')->default(999);
            
            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_sukaman_tarif_detail');
        Schema::dropIfExists('trans_sukaman_tarif_detail');

        Schema::dropIfExists('log_trans_sukaman_tarif');
        Schema::dropIfExists('trans_sukaman_tarif');
        
        Schema::dropIfExists('log_trans_sukaman_kategori_tarif');
        Schema::dropIfExists('trans_sukaman_kategori_tarif');
    }
}
