<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePengaduan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_pengaduan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->text('alamat');
            $table->string('telp_rumah')->nullable();
            $table->string('telp_kantor')->nullable();
            $table->string('telp_seluler')->nullable();
            $table->string('email');
            $table->integer('hari');
            $table->string('pukul_awal');
            $table->string('pukul_akhir');
            $table->string('no_id')->nullable();
            $table->string('pangkat')->nullable();
            $table->string('jabatan')->nullable();
            $table->date('tgl_kerja')->nullable();
            $table->string('divisi')->nullable();
            $table->string('no_sk')->nullable();
            $table->integer('rahasia');
            $table->text('alasan_rahasia')->nullable();
            $table->text('kronologi')->nullable();
            $table->text('pihak_terkait')->nullable();
            $table->string('lokasi_kejadian')->nullable();
            $table->date('tanggal_kejadian');
            $table->text('penyebab')->nullable();
            $table->text('dampak')->nullable();
            $table->text('alasan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

        });
        Schema::create('log_trans_pengaduan', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_pengaduan')->unsigned();
            $table->string('nama');
            $table->text('alamat');
            $table->string('telp_rumah')->nullable();
            $table->string('telp_kantor')->nullable();
            $table->string('telp_seluler')->nullable();
            $table->string('email');
            $table->integer('hari');
            $table->string('pukul_awal');
            $table->string('pukul_akhir');
            $table->string('no_id')->nullable();
            $table->string('pangkat')->nullable();
            $table->string('jabatan')->nullable();
            $table->date('tgl_kerja')->nullable();
            $table->string('divisi')->nullable();
            $table->string('no_sk')->nullable();
            $table->integer('rahasia');
            $table->text('alasan_rahasia')->nullable();
            $table->text('kronologi')->nullable();
            $table->text('pihak_terkait')->nullable();
            $table->string('lokasi_kejadian')->nullable();
            $table->date('tanggal_kejadian');
            $table->text('penyebab')->nullable();
            $table->text('dampak')->nullable();
            $table->text('alasan')->nullable();

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_pengaduan');
        Schema::dropIfExists('log_trans_pengaduan');
    }
}
