<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefKuesionerPeriode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_kuesioner_periode',function(Blueprint $table){
            $table->increments('id');
            $table->text('periode');
            $table->integer('status');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_ref_kuesioner_periode', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_kuesioner_periode')->unsigned();
            $table->text('periode');
            $table->integer('status');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_kuesioner_periode');
        Schema::dropIfExists('log_ref_kuesioner_periode');
    }
}
