<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransKuesionerJawabanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('trans_kuesioner_jawaban_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kuesioner_jawaban_id')->unsigned();
            $table->integer('kuesioner_detail_id')->unsigned();
            $table->integer('jawaban');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('kuesioner_jawaban_id')->references('id')->on('trans_kuesioner_jawaban');
            $table->foreign('kuesioner_detail_id')->references('id')->on('ref_kuesioner_detail');
        });
        Schema::create('log_trans_kuesioner_jawaban_detail', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_kuesioner_jawaban_detail')->unsigned();
            $table->integer('kuesioner_jawaban_id')->unsigned();
            $table->integer('kuesioner_detail_id')->unsigned();
            $table->integer('jawaban');

            $table->timestamp('created_at');
            $table->integer('created_by')->unsigned();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_kuesioner_jawaban_detail');
        Schema::dropIfExists('log_trans_kuesioner_jawaban_detail');
    }
}
