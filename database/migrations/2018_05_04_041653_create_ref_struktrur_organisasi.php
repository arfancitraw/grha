<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefStruktrurOrganisasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_struktur_organisasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('photo',100)->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
        Schema::create('log_struktur_organisasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_struktur_organisasi')->unsigned();
            $table->string('judul');
            $table->text('konten')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('photo',100)->nullable();

            $table->timestamp('created_at')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_struktur_organisasi');
        Schema::dropIfExists('ref_struktur_organisasi');
    }
}
