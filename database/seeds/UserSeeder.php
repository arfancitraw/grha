<?php

use App\Models\Entrust\Role;
use App\Models\Setting\Setting;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('ref_setting')->truncate();
        $admin = new Role();
        $admin->fill([
            'name' => 'admin',
            'display_name'=> 'admin',
            'description' => 'admin'
        ]);
        $admin->save();

        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@email.com',
            'password' => bcrypt('password'),
        ]);
        $user->attachRole($admin);
    }
}
