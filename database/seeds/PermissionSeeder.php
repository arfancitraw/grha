<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Models\Entrust\Permission;
use App\Models\Entrust\Role;
use App\Models\User;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement('TRUNCATE TABLE sys_permission_role CASCADE');
        DB::statement('TRUNCATE TABLE sys_permissions CASCADE');

        $perms = [
            'media-center' => [
              'artikel' => [
                'Artikel',
              ],
              'kegiatan' => [
                'Kegiatan',
              ],
              'pengumuman' => [
                'Berita',
              ],
              'pustaka' => [
                'penyakit' => [
                    'Penyakit',
                  ],
                'tindakan' => [
                    'Tindakan',
                  ],
              ],
            ],

            'profil' => [
              'Profil',
            ],

            'layanan' => [
              'Pelayanan',
            ],

            'dokter' => [
              'list-dokter' => [
                'Tim Dokter',
              ],
            ],

            'rujukan-nasional' => [
              'Rujukan Nasional',
            ],

            'diklat' => [
              'info-diklat' => [
                'Info Diklat',
              ],
              'jadwal-diklat' => [
                'Jadwal Diklat',
              ],
            ],

            'litbang' => [
              'info-litbang' => [
                'Info Litbang',
              ],
              'komite-etik' => [
                'Komite Etik',
              ],
              'list-publikasi' => [
                'List Publikasi',
              ],
            ],

            'informasi' => [
              'Informasi',
            ],

            'mutu' => [
              'Mutu',
            ],

            'kuesioner' => [
              'Kuesioner',
            ],

            'testimoni' => [
              'Testimoni',
            ],

            'pertanyaan' => [
              'Pertanyaan',
            ],

            'pengaduan' => [
                'etik-hukum' => ['Etik & Hukum'],
                'masyarakat' => ['Pengaduan Masyarakat'],
                'whistleblowing' => ['Whistleblowing'],
                'gratifikasi' => ['Gratifikasi'],
                'sponsor' => ['Sponsor'],
            ],

            'sukaman' => [
                'slider' => ['Slider'],
                'pelayanan' => ['Pelayanan'],
                'dokter' => ['Dokter'],
                'fasilitas' => ['Fasilitas'],
                'informasi' => ['Informasi'],
                'setting' => ['Setting'],
                'pricing-kategori' => ['Pricing'],
                'kontak' => ['Kontak'],
            ],

            'setting' => [
                'jumbotron' => ['Pengaturan Gambar'],
                'menu' => ['Pengaturan Menu'],
                'setting' => ['Setting'],
                'user' => ['Manajemen Pengguna'],
            ],

            'mobile' => [
                'direksi' => ['Direksi'],
                'dewan' => ['Dewan'],
            ],

            'master' => [
                'slider' => [
                  'Home Slider',
                ],
                'sertifikat-footer' => [
                  'Sertifikat',
                ],
                'media-sosial' => [
                  'Media Sosial',
                ],
                'link-footer' => [
                  'Link Footer',
                ],
                'role-permission' => [
                  'Role & Permission',
                ],
                'poli' => [
                  'Poli',
                ],
                'jenis-mutu' => [
                  'Indikator Mutu',
                ],
                'jabatan-dokter' => [
                  'Jabatan Dokter',
                ],
                'spesialisasi' => [
                  'Spesialisasi',
                ],
                'sub-spesialisasi' => [
                  'Sub Spesialisasi',
                ],
                'kategori-artikel' => [
                  'Kategori Artikel',
                ],
                'kategori-pengumuman' => [
                  'Kategori Pengumuman',
                ],
                'kategori-tanya-jawab' => [
                  'Kategori Tanya Jawab',
                ],
                'tipe-media' => [
                  'Tipe Media',
                ],
                'tipe-pendaftar' => [
                  'Tipe Pendaftar',
                ],
            ],
        ];

        $this->createPermission($perms);
        Model::reguard();

        // $this->call(UsersTableSeeder::class);
    }



    public function createPermission($perms)
    {
        foreach($perms as $i => $menu)
        {
          foreach($menu as $ii => $modul)
          {
              if(is_array($modul))
              {
                foreach($modul as $iii => $submodul)
                {
                  if(is_array($submodul))
                  {
                    foreach($submodul as $iv => $subsubmodul)
                    {
                        $view = new Permission;
                        $view->name = 'backend/'.$i.'/'.$ii.'/'.$iii;
                        $view->display_name = $subsubmodul.' View';
                        $view->save();

                        $add = new Permission;
                        $add->name = 'backend/'.$i.'/'.$ii.'/'.$iii.'/create';
                        $add->display_name = $subsubmodul.' Create';
                        $add->save();

                        $edit = new Permission;
                        $edit->name = 'backend/'.$i.'/'.$ii.'/'.$iii.'/edit';
                        $edit->display_name = $subsubmodul.' Edit';
                        $edit->save();

                        $delete = new Permission;
                        $delete->name = 'backend/'.$i.'/'.$ii.'/'.$iii.'/delete';
                        $delete->display_name = $subsubmodul.' Delete';
                        $delete->save();

                        $roles = Role::all();

                        foreach($roles as $role)
                        {
                            $role->attachPermission($view);
                            $role->attachPermission($add);
                            $role->attachPermission($edit);
                            $role->attachPermission($delete);
                        }

                    }
                  }else{
                    $view = new Permission;
                    $view->name = 'backend/'.$i.'/'.$ii;
                    $view->display_name = $submodul.' View';
                    $view->save();

                    $add = new Permission;
                    $add->name = 'backend/'.$i.'/'.$ii.'/create';
                    $add->display_name = $submodul.' Create';
                    $add->save();

                    $edit = new Permission;
                    $edit->name = 'backend/'.$i.'/'.$ii.'/edit';
                    $edit->display_name = $submodul.' Edit';
                    $edit->save();

                    $delete = new Permission;
                    $delete->name = 'backend/'.$i.'/'.$ii.'/delete';
                    $delete->display_name = $submodul.' Delete';
                    $delete->save();

                    $roles = Role::all();

                    foreach($roles as $role)
                    {
                        $role->attachPermission($view);
                        $role->attachPermission($add);
                        $role->attachPermission($edit);
                        $role->attachPermission($delete);
                    }
                  }
                }
              }else{
                  $view = new Permission;
                  $view->name = 'backend/'.$i;
                  $view->display_name = $modul.' View';
                  $view->save();

                  $add = new Permission;
                  $add->name = 'backend/'.$i.'/create';
                  $add->display_name = $modul.' Create';
                  $add->save();

                  $edit = new Permission;
                  $edit->name = 'backend/'.$i.'/edit';
                  $edit->display_name = $modul.' Edit';
                  $edit->save();

                  $delete = new Permission;
                  $delete->name = 'backend/'.$i.'/delete';
                  $delete->display_name = $modul.' Delete';
                  $delete->save();

                  $roles = Role::all();

                  foreach($roles as $role)
                  {
                      $role->attachPermission($view);
                      $role->attachPermission($add);
                      $role->attachPermission($edit);
                      $role->attachPermission($delete);
                  }
              }
          }
        }
    }

}
