<?php

use Illuminate\Database\Seeder;

class SukamanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trans_sukaman_setting')->truncate();

        DB::table('trans_sukaman_setting')->insert([
            [
                'kunci' => 'title',
                'judul' => 'Paviliun Sukaman',
                'isi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et fermentum dui.',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'about',
                'judul' => 'Tentang Paviliun Sukaman',
                'created_at' => date('Y-m-d H:i:s'),
                'isi' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, nemo, aliquid. Eos quis delectus incidunt ipsam id inventore ipsum qui optio unde, tenetur, excepturi aliquid quibusdam porro non exercitationem, dolorem!',

            ],
            [
                'kunci' => 'emergency',
                'judul' => 'In an emergency? Need help now?',
                'created_at' => date('Y-m-d H:i:s'),
                'isi' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quis quisquam culpa nostrum fugit. Totam excepturi fugiat impedit perferendis nostrum aliquid ex consequuntur eum, tenetur quas numquam unde itaque delectus.',

            ],
            [
                'kunci' => 'service',
                'judul' => 'Pelayanan Kami',
                'isi' => 'Tempore hic maxime eos soluta esse, accusantium quisquam nostrum non.',
                'created_at' => date('Y-m-d H:i:s'),
            
            ],
            [
                'kunci' => 'doctors',
                'judul' => 'Tim Dokter Paviliun Sukaman',
                'isi' => 'Tempore hic maxime eos soluta esse, accusantium quisquam nostrum non.',
                'created_at' => date('Y-m-d H:i:s'),
            
            ],
            [
                'kunci' => 'facility',
                'judul' => 'Fasilitas',
                'created_at' => date('Y-m-d H:i:s'),
                'isi' => 'Nobis ullam pariatur hic nam nesciunt deleniti impedit corporis ut in tenetur. Dicta repudiandae, vitae quam officia aliquid quidem, quod enim! Recusandae',

            ],
            [
                'kunci' => 'information',
                'judul' => 'Informasi Pasien',
                'isi' => 'Tempore hic maxime eos soluta esse, accusantium quisquam nostrum non.',
                'created_at' => date('Y-m-d H:i:s'),
            
            ],
            [
                'kunci' => 'address',
                'judul' => '321 Awesome Street',
                'isi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et fermentum dui.',
                'created_at' => date('Y-m-d H:i:s'),
            
            ],
            [
                'kunci' => 'call_center',
                'judul' => '+1 800 123 1234',
                'isi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et fermentum dui.',
                'created_at' => date('Y-m-d H:i:s'),
            
            ],
            [
                'kunci' => 'emergency_call',
                'judul' => '+1 800 123 1234',
                'isi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et fermentum dui.',
                'created_at' => date('Y-m-d H:i:s'),
            
            ],
            [
                'kunci' => 'email',
                'judul' => 'info@companyname.com',
                'isi' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et fermentum dui.',
                'created_at' => date('Y-m-d H:i:s'),
            
            ]
        ]);
    }
}
