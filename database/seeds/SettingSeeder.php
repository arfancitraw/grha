<?php

use Illuminate\Database\Seeder;
use App\Models\Setting\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('ref_setting')->truncate();
        $settings = [
            [
                'kunci' => 'jumbotron-litbang',
                'judul' => 'Jumbotron Litbang',
                'isi' => 'https://www.dphealthnow.com/wp-content/uploads/2016/05/blue-medical-background.jpg',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'jumbotron-service',
                'judul' => 'Jumbotron Service',
                'isi' => 'https://www.dphealthnow.com/wp-content/uploads/2016/05/blue-medical-background.jpg',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'jumbotron-rujukan',
                'judul' => 'Jumbotron Rujukan',
                'isi' => 'https://www.dphealthnow.com/wp-content/uploads/2016/05/blue-medical-background.jpg',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'jumbotron-informasi',
                'judul' => 'Jumbotron Informasi',
                'isi' => 'img/backgrounds/pasien.jpg',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'jumbotron-dokter',
                'judul' => 'Jumbotron Dokter',
                'isi' => 'img/backgrounds/tim-dokter.jpg',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'jumbotron-whistleblowing',
                'judul' => 'Jjumbotron Whistleblowing',
                'isi' => 'img/backgrounds/whistleblowing.jpg',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'jumbotron-media',
                'judul' => 'Jumbotron Media',
                'isi' => 'img/backgrounds/media.jpg',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'jumbotron-profil',
                'judul' => 'Jumbotron Profil',
                'isi' => 'img/backgrounds/profil.jpg',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'jumbotron-mutu',
                'judul' => 'Jumbotron Mutu',
                'isi' => 'img/backgrounds/media.jpg',
                'created_at' => date('Y-m-d H:i:s'),
            ],

            [
                'kunci' => 'call_center',
                'judul' => 'Call Center',
                'isi' => '1500 034',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'emergency_call',
                'judul' => 'Emergency Call',
                'isi' => '(021) 568 2424',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'email',
                'judul' => 'E-Mail',
                'isi' => 'info@pjnhk.go.id',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'address',
                'judul' => 'Address',
                'isi' => 'Jl. Letjen S. Parman Kav 87 Slipi<br/>Jakarta Barat 11420',
                'created_at' => date('Y-m-d H:i:s'),
            ],

            [
                'kunci' => 'profile-video',
                'judul' => 'Video Profil',
                'isi' => '<iframe width="100%" height="650" src="https://www.youtube.com/embed/zsNLdbM-P5w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>',
                'created_at' => date('Y-m-d H:i:s'),
            ],

            [
                'kunci' => 'link-germas',
                'judul' => 'Link Germas',
                'isi' => 'http://www.depkes.go.id/article/view/16111500002/germas-wujudkan-indonesia-sehat.html',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'link-googleplay',
                'judul' => 'Link GooglePlay',
                'isi' => '#',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'pengaduan-email-etik',
                'judul' => 'Email Etik & Hukum',
                'isi' => 'etik.hukum@pjnhk.go.id',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'pengaduan-email-wbs',
                'judul' => 'Email Whistleblowing',
                'isi' => 'spi.wbs@pjnhk.go.id',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'pengaduan-email-masyarakat',
                'judul' => 'Email Pengaduan Masyarakat',
                'isi' => 'dumas@pjnhk.go.id',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'pengaduan-email-gratifikasi',
                'judul' => 'Email Gratifikasi',
                'isi' => 'budiha44@gmail.com',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'pengaduan-email-sponsor',
                'judul' => 'Email Sponsor',
                'isi' => 'budiha44@gmail.com',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'pengaduan-narasi-etik',
                'judul' => 'Narasi Pengaduan Etik & Hukum',
                'isi' => '
                    <p><strong>Selamat datang di Pelaporan Etik dan Hukum Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita. (RSJPDHK)</strong></p>
                    <p>Pelaporan Etik dan Hukum merupakan pelaporan dugaan pelanggaran kode etik dan disiplin pegawai di Rumah Sakit Jantung Dan Pembuluh Darah Harapan Kita.</p>
                    <p>Pelaporan Etik dan Hukum merupakan bagian dari sistem penanganan pengaduan pegawai dan masyarakat terpadu yang memfokuskan pada penanganan dugaan pelanggaran kode etik dan disiplin pegawai.<br />Pengaduan dari pegawai dan masyarakat adalah salah satu bentuk peran serta dalam pengawasan terhadap RS Jantung dan Pembuluh Darah Harapan Kita. Dengan adanya pelanggan kode etik dan disipllin perlu mendapatkan tanggapan yang cepat, tepat dan dapat dipertanggung jawabkan.</p>
                    <h3>Kriteria Pelaporan Etik dan Hukum</h3>
                    <ol>
                    <li>Ada dugaan pelanggaran kode etik dan disiplin pegawai.</li>
                    <li>Dimana dan Kapan kasus tersebut dilakukan</li>
                    <li>Siapa pejabat/pegawai RSJPDHK yang melakukan atau terlibat</li>
                    <li>Bagaimana cara perbuatan tersebut dilakukan</li>
                    <li>Dilengkapi dengan bukti permulaan (data, dokumen, gambar dan rekaman) yang mendukung/menjelaskan adanya dugaan pelanggaran kode etik dan disiplin pegawai.</li>
                    </ol>
                    <p>Anda melihat atau mengetahui dugaan pelanggaran kode etik dan disiplin yang dilakukan pegawai di lingkungan RS Jantung Dan Pembuluh Darah Rumah Sakit Harapan Kita. Silahkan melapor ke Komite Etik dan Hukum RSJPDHK.</p>
                    <p>Jika laporan anda memenuhi syarat / kriteria, maka akan diproses lebih lanjut.<br />Silahkan melapor ke Komite Etik dan Hukum RSJPDHK di telepon (021) 568 4085 ext. 1540 atau klik link dibawah ini.</p>
                ',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'pengaduan-narasi-wbs',
                'judul' => 'Narasi Pengaduan Whistleblowing',
                'isi' => '
                    <p><strong>Selamat datang di Whistleblowing Systems (WBS) Rumah Sakit Jantung dan Pembuluh DarahHarapan Kita (RSJPDHK).</strong></p>
                    <p class="h5">Definisi Whistleblowing:</p>
                    <p>Seseorang yang melaporkan perbuatan dugaan tindak pidana korupsi yang terjadi di Rumah Sakit Jantung Dan Pembuluh DarahHarapan Kita, atau pihak terkait lainnya yang memiliki akses informasi yang memadai atas terjadinya dugaan tindak pidana korupsi tersebut. Pelapor dijamin serta diberikan perlindungan oleh Pimpinan RS.</p>
                    <p>WBS merupakan bagian dari sistem penanganan pengaduan pegawai dan masyarakat terpadu yang memfokuskan pada penanganan dugaan tindak pidana korupsi.</p>
                    <p>Pengaduan dari pegawai dan masyarakat adalah salah satu bentuk peran serta dalam pengawasan terhadap RS Jantung Dan Pembuluh Darah Rumah Sakit Harapan Kita yang perlu mendapatkan tanggapan dengan cepat, tepat dan dapat dipertanggungjawabkan oleh RS Jantung dan Pembuluh Darah Harapan Kita.</p>
                    <h3>Kriteria Whistleblower</h3>
                    <ol>
                    <li>Ada penyimpangan kasus yang dilaporkan</li>
                    <li>Menjelaskan dimana, kapan kasus tersebut dilakukan</li>
                    <li>Siapa pejabat/pegawai RSJPDHK yang melakukan atau terlibat</li>
                    <li>Bagaimana cara perbuatan tersebut dilakukan</li>
                    <li>Dilengkapi dengan bukti permulaan (data, dokumen, video, gambar dan rekaman) yang mendukung/menjelaskan adanya dugaan Tindak Pidana Korupsi</li>
                    </ol>
                    <p>Apabila anda melihat atau mengetahui dugaan Tindak Pidana Korupsi yang dilakukan pegawai di lingkungan RS Jantung Dan Pembuluh Darah Harapan Kita. Silahkan melapor ke Satuan Pemeriksaan Internal (SPI) RSJPDHK.</p>
                    <p>Jika laporan anda memenuhi syarat/kriteria, maka akan diproses lebih lanjut.</p>
                    <p>Silahkan melapor ke Satuan Pemeriksaan Internal (SPI) RSJPDHK di telepon (021) 568 4085 ext. 1469 atau klik link dibawah ini.</p>
                ',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'pengaduan-narasi-masyarakat',
                'judul' => 'Narasi Pengaduan Pengaduan Masyarakat',
                'isi' => '
                    <p><strong>Selamat datang di Pengaduan Masyarakat RS Jantung dan Pembuluh Darah Harapan Kita (RSJPDHK).</strong></p>
                    <p>Pengaduan Masyarakat adalah informasi atau pemberitahuan yang disampaikan oleh masyarakat, baik perseorangan dan/atau keluarga yang berasal dari pelanggan RS Jantung dan Pembuluh Darah Harapan Kita (RSJPDHK) maupun masyarakat umum yang berisi ketidakpuasan terkait perilaku pegawai RSJPDHK.</p>
                    <p>Pengaduan Masyarakat merupakan bagian dari sistem penanganan aduan yang berfokus pada penanganan ketidak puasan pelayanan di RSJPDHK.<br />Hal ini merupakan salah satu bentuk peran serta masyarakat dalam pengawasan terhadap RS Jantung dan Pembuluh Darah Harapan Kita yang perlu mendapatkan tanggapan yang cepat, tepat dan dapat dipertanggung jawabkan oleh instansi.</p>
                    <h3>Kriteria Pelaporan</h3>
                    <ol>
                    <li>Ada dugaankesalahan pelaksanaan tugas dan fungsi pegawai RSJPDHK dalam memberikan pelayanan.</li>
                    <li>Menjelaskan dimana, kapan kasus tersebut dilakukan.</li>
                    <li>Siapa pejabat/pegawai RSJPDHK yang melakukan atau terlibat.</li>
                    <li>Bagaimana cara perbuatan tersebut dilakukan.</li>
                    <li>Dilengkapi dengan bukti permulaan (data, dokumen, gambar dan rekaman) yang mendukung/menjelaskan adanya dugaan kesalahan pelaksanaan tugas dan fungsi pegawai RSJPDHK dalam memberikan pelayanan.</li>
                    </ol>
                    <p>Bila anda mengetahui atau mengalami dan ingin melaporkan ketidakpuasan atas sikap petugas dalam memberikan pelayanan. di lingkungan RS Jantung dan Pembuluh Darah Harapan Kita.</p>
                    <p>Silahkan menggunakan fasilitas ini untuk melapor ke Instalasi Pemasaran dan Pelayan Pelanggan RSJPDHK melalui telepon (021) 568 4085 ext. 5001 atau klik link dibawah ini.</p>
                ',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'pengaduan-narasi-gratifikasi',
                'judul' => 'Narasi Pengaduan Gratifikasi',
                'isi' => '
                    <p><strong>Selamat datang di Pelaporan Gratifikasi Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita. (RSJPDHK)</strong></p>
                    <p>Pelaporan dari Pegawai RSJPDHK mengenai gratifikasi yang terjadi di Rumah Sakit Jantung Dan Pembuluh Darah Harapan Kita.</p>
                    <p>Pelaporan Gratifikasi merupakan bagian dari sistem penanganan pengaduan pegawai terpadu yang memfokuskan pada penanganan dugaan gratifikasi.<br />Pengaduan dari pegawai dan masyarakat adalah salah satu bentuk peran serta dalam pengawasan terhadap RS Jantung Dan Pembuluh Darah Rumah Sakit Harapan Kita yang perlu mendapatkan tanggapan dengan cepat, tepat dan dapat dipertanggungjawabkan oleh instansi.</p>
                    <h3>Kriteria Pelaporan Gratifikasi</h3>
                    <ol>
                    <li>Ada gratifikasi yang ingin dilaporkan.</li>
                    <li>Menjelaskan Dimana, Kapan gratifikasi tersebut terjadi.</li>
                    <li>Siapa pejabat/pegawai RSJPDHK yang melakukan atau terlibat.</li>
                    <li>Bagaimana cara perbuatan tersebut dilakukan.</li>
                    <li>Dilengkapi dengan bukti permulaan (data, dokumen, gambar dan rekaman) yang mendukung/menjelaskan adanya dugaan gratifikasi.</li>
                    </ol>
                    <p>Apabila anda mendapatkan barang atau uang yang diduga merupakan Gratifikasi di lingkungan RS Jantung Dan Pembuluh Darah Rumah Sakit Harapan Kita. Silahkan melapor ke Unit Pengendali Gratifikasi (UPG) RSJPDHK.</p>
                    <p>Jika laporan anda memenuhi syarat / kriteria, maka akan diproses lebih lanjut.<br />Silahkan melapor ke Unit Pengendali Gratifikasi (UPG) RSJPDHK atau klik link dibawah ini.</p>
                ',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'kunci' => 'pengaduan-narasi-sponsor',
                'judul' => 'Narasi Pengaduan Sponsor',
                'isi' => '
                    <p><strong>Selamat datang di Pelaporan Sponsor Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita. (RSJPDHK)</strong></p>
                    <p>Pelaporan dari Pegawai RSJPDHK mengenai sponsor yang terjadi di Rumah Sakit Jantung Dan Pembuluh Darah Harapan Kita.</p>
                    <p>Pelaporan sponsorsip merupakan bagian dari sistem penanganan pengaduan pegawai terpadu.<br />Pengaduan dari pegawai dan masyarakat adalah salah satu bentuk peran serta dalam pengawasan terhadap RS Jantung Dan Pembuluh Darah Rumah Sakit Harapan Kita.</p>
                    <h3>Kriteria Pelaporan Sponsor</h3>
                    <ol>
                    <li>Adanya kegiatan sponsorsip yang ingin dilaporkan.</li>
                    <li>Menjelaskan dimana dan kapan sponsor tersebut terjadi.</li>
                    <li>Adanya pejabat/pegawai RSJPDHK yang terlibat.</li>
                    <li>Bagaimana cara perbuatan tersebut dilakukan.</li>
                    <li>Dilengkapi dengan bukti (data, dokumen, gambar dan rekaman) yang mendukung/menjelaskan adanya sponsor.</li>
                    </ol>
                    <p>Apabila anda mendapatkan sponsor di lingkungan RS Jantung Dan Pembuluh Darah Rumah Sakit Harapan Kita. Silahkan melapor ke Unit Pengendali Gratifikasi (UPG) RSJPDHK.</p>
                    <p>Jika laporan anda memenuhi syarat / kriteria, maka akan diproses lebih lanjut.<br />Silahkan melapor ke Unit Pengendali Gratifikasi (UPG) RSJPDHK atau klik link dibawah ini.</p>
                ',
                'created_at' => date('Y-m-d H:i:s'),
            ],
        ];

        foreach ($settings as $setting) {
            $item = Setting::where('kunci', $setting['kunci'])->first();

            if(!$item){
                $set = new Setting();
                $set->fill($setting);
                $set->save();
            }
        }
    }
}
