<?php

use Illuminate\Database\Seeder;

class KuesionerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trans_kuesioner')->truncate();

        DB::table('trans_kuesioner')->insert([
            [
                'nama' => 'arfan',
                'email' => 'arfan.pragma@gmail.com',
                'profesi' => 'dokter',
                'kepuasan_layanan' => '1',
                'kepuasan_fasilitas_website' => '1',
                'feedback' => 'cukup',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => '1',
            ],
        ]);
    }
}
