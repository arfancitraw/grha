--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.8
-- Dumped by pg_dump version 9.6.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: log_jabatan_dokter; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_jabatan_dokter (id, id_jabatan_dokter, jabatan, deskripsi, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Kepala Spesialis Cardiologi', 'Kepala Spesialis Cardiologi', '2018-05-15 04:08:51', 1, NULL, NULL);
INSERT INTO public.log_jabatan_dokter (id, id_jabatan_dokter, jabatan, deskripsi, created_at, created_by, updated_at, updated_by) VALUES (2, 1, 'Dokter Spesialis', 'Dokter Spesialis', '2018-05-15 04:08:51', 1, '2018-05-15 04:20:15', 1);
INSERT INTO public.log_jabatan_dokter (id, id_jabatan_dokter, jabatan, deskripsi, created_at, created_by, updated_at, updated_by) VALUES (3, 2, 'Dokter Umum', 'Dokter Umum', '2018-05-15 04:20:27', 1, NULL, NULL);


--
-- Name: log_jabatan_dokter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_jabatan_dokter_id_seq', 3, true);


--
-- Data for Name: log_jenis_mutu; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_jenis_mutu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_jenis_mutu_id_seq', 1, false);


--
-- Data for Name: log_poli; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_poli_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_poli_id_seq', 1, false);


--
-- Data for Name: log_ref_kategori_artikel; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_ref_kategori_artikel (id, id_kategori_artikel, nama, deskripsi, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Berita', 'Berita', '2018-05-22 05:58:48', 1, NULL, NULL);


--
-- Name: log_ref_kategori_artikel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_ref_kategori_artikel_id_seq', 1, true);


--
-- Data for Name: log_ref_kuesioner_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (1, 1, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-29 22:50:27', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (2, 2, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-29 22:50:27', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (3, 3, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-29 22:50:27', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (4, 4, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-29 22:50:27', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (5, 5, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (6, 6, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (7, 7, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-31 04:25:33', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (8, 8, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-31 04:25:34', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (9, 9, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-31 04:25:34', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (10, 10, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-31 04:25:34', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (11, 11, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:25:34', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (12, 12, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:25:34', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (13, 7, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-31 04:25:33', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (14, 8, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-31 04:25:34', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (15, 9, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-31 04:25:34', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (16, 10, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-31 04:25:34', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (17, 11, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:25:34', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (18, 12, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:25:34', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (19, 13, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-31 04:30:50', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (20, 14, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-31 04:30:50', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (21, 15, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-31 04:30:50', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (22, 16, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-31 04:30:50', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (23, 17, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:30:50', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (24, 18, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:30:50', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (25, 13, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-31 04:30:50', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (26, 14, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-31 04:30:50', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (27, 15, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-31 04:30:50', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (28, 16, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-31 04:30:50', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (29, 17, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:30:50', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (30, 18, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:30:50', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (31, 19, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-31 04:33:23', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (32, 20, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-31 04:33:23', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (33, 21, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-31 04:33:23', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (34, 22, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-31 04:33:23', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (35, 23, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:33:23', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (36, 24, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:33:23', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (37, 1, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-29 22:50:27', 1, '2018-05-31 04:37:30', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (38, 2, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:37:30', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (39, 3, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:37:30', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (40, 4, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:37:30', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (41, 5, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:37:30', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (42, 6, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:37:30', 1, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (43, 1, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:00', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (44, 2, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:00', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (45, 3, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:00', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (46, 4, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:00', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (47, 5, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:00', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (48, 6, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:00', 1, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (49, 1, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:17', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (50, 2, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:17', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (51, 3, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:17', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (52, 4, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:17', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (53, 5, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:17', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (54, 6, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:17', 1, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (55, 1, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:41', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (56, 2, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:41', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (57, 3, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:41', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (58, 4, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:41', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (59, 5, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:41', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (60, 6, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:41', 1, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (61, 1, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:54', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (62, 2, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:54', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (63, 3, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:54', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (64, 4, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:54', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (65, 5, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:54', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (66, 6, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:54', 1, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (67, 1, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:58', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (68, 2, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:58', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (69, 3, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:58', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (70, 4, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:58', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (71, 5, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:58', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (72, 6, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:39:58', 1, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (73, 1, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-29 22:50:27', 1, '2018-05-31 04:40:14', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (74, 2, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:40:14', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (75, 3, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:40:14', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (76, 4, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:40:14', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (77, 5, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:40:14', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (78, 6, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:40:14', 1, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (79, 1, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:32', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (80, 2, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:32', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (81, 3, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:32', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (82, 4, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:32', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (83, 5, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:32', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (84, 6, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:32', 1, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (85, 1, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (86, 2, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (87, 3, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (88, 4, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (89, 5, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (90, 6, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (91, 25, 2, 'Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita sudah sesuai dengan harapan anda?', '2018-05-31 04:49:41', 1, NULL, NULL, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (92, 26, 2, 'Anda puas dengan pelayanan petugas kami ?', '2018-05-31 04:49:41', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (93, 27, 2, 'Informasi yang ada di website ini mudah anda pahami ?', '2018-05-31 04:49:41', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (94, 28, 2, 'Anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-31 04:49:41', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (95, 29, 2, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:49:41', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (96, 30, 2, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:49:41', 1, NULL, NULL, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (97, 25, 2, 'Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita sudah sesuai dengan harapan anda?', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (98, 26, 2, 'Anda puas dengan pelayanan petugas kami ?', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (99, 27, 2, 'Informasi yang ada di website ini mudah anda pahami ?', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (100, 28, 2, 'Anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, false);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (101, 29, 2, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, true);
INSERT INTO public.log_ref_kuesioner_detail (id, id_kuesioner_detail, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (102, 30, 2, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, true);


--
-- Name: log_ref_kuesioner_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_ref_kuesioner_detail_id_seq', 102, true);


--
-- Data for Name: log_ref_kuesioner_periode; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, NULL, NULL);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (2, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (3, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (4, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (5, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (6, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (7, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (8, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (9, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (10, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (11, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (12, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (13, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (14, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (15, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (16, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (17, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (18, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (19, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (20, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (21, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (22, 1, 'Semester 1 - 2018', 1, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (23, 2, 'Semester 2 - 2018', 0, '2018-05-31 04:49:41', 1, NULL, NULL);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (24, 2, 'Semester 2 - 2018', 1, '2018-05-31 04:49:41', 1, '2018-05-31 05:06:06', 1);
INSERT INTO public.log_ref_kuesioner_periode (id, id_kuesioner_periode, periode, status, created_at, created_by, updated_at, updated_by) VALUES (25, 2, 'Semester 2 - 2018', 1, '2018-05-31 04:49:41', 1, '2018-05-31 05:06:06', 1);


--
-- Name: log_ref_kuesioner_periode_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_ref_kuesioner_periode_id_seq', 25, true);


--
-- Data for Name: log_ref_link_footer; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Kemenkes', 'http://www.kemkes.go.id/', '2018-05-21 03:54:43', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (2, 2, 'Karir', 'karir.pjnhk.go.id', '2018-05-21 04:15:27', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (3, 3, 'Dinkes DKI', 'http://dinkes.jakarta.go.id/', '2018-05-21 05:48:21', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (4, 2, 'Karir', 'karir.pjnhk.go.id', '2018-05-21 04:15:27', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (5, 4, 'SIRS Online', 'http://telemedicine.pjnhk.go.id/daftar/', '2018-05-21 05:49:08', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (6, 5, 'AGD Dinkes DKI', 'https://agddinkes.jakarta.go.id/', '2018-05-21 05:49:46', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (7, 6, 'Siranap', 'http://yankes.kemkes.go.id/app/siranap/pages/rsvertikal', '2018-05-21 05:50:22', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (8, 7, 'LPSE Kemenkes', 'http://www.lpse.depkes.go.id/eproc4', '2018-05-21 05:50:36', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (9, 8, 'SISRUTE', 'https://sisrute.kemkes.go.id/', '2018-05-21 05:51:03', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (10, 9, 'SIRUP', 'https://sirup.lkpp.go.id/sirup', '2018-05-21 05:51:28', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (11, 10, 'BPJS', 'https://bpjs-kesehatan.go.id/bpjs/', '2018-05-21 05:51:53', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (12, 11, 'FK UI', 'http://fk.ui.ac.id/', '2018-05-21 05:52:22', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (13, 12, 'Yayasan Jantung Indonesia', 'http://www.inaheart.or.id/', '2018-05-21 05:53:04', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (14, 13, 'PERSI', 'https://www.persi.or.id/', '2018-05-21 05:53:45', 1, NULL, NULL);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (15, 12, 'PERKI', 'http://www.inaheart.or.id/', '2018-05-21 05:53:04', 1, '2018-05-21 05:53:55', 1);
INSERT INTO public.log_ref_link_footer (id, id_link_footer, nama, link, created_at, created_by, updated_at, updated_by) VALUES (16, 14, 'BIOS PPKBLU', 'http://bios.djpbn.kemenkeu.go.id/', '2018-05-21 05:54:14', 1, NULL, NULL);


--
-- Name: log_ref_link_footer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_ref_link_footer_id_seq', 16, true);


--
-- Data for Name: log_ref_sertifikat_footer; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_ref_sertifikat_footer (id, id_sertifikat_footer, nama, gambar, keterangan, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Joint Commission International', 'uploads/master/sertifikat-footer/LUAwJxWC29mNNh4hS9tvFeRwRGfMZKwxnAh9DCcO.png', 'Quality Approval from JCI', '2018-05-21 03:07:45', 1, NULL, NULL);
INSERT INTO public.log_ref_sertifikat_footer (id, id_sertifikat_footer, nama, gambar, keterangan, created_at, created_by, updated_at, updated_by) VALUES (2, 2, 'KARS', 'uploads/master/sertifikat-footer/dgYYSVpkklXuOMCkLLidotcpxl3nwHFSWgZvxNRf.png', 'KARS', '2018-05-21 03:08:02', 1, NULL, NULL);
INSERT INTO public.log_ref_sertifikat_footer (id, id_sertifikat_footer, nama, gambar, keterangan, created_at, created_by, updated_at, updated_by) VALUES (3, 3, 'Satria Brand Award', 'uploads/master/sertifikat-footer/daezJF8jPGrxFdfmcmqJ6D7sJEfpmkU5MhjoaaX1.png', 'Merek Pilihan', '2018-05-21 03:08:19', 1, NULL, NULL);
INSERT INTO public.log_ref_sertifikat_footer (id, id_sertifikat_footer, nama, gambar, keterangan, created_at, created_by, updated_at, updated_by) VALUES (4, 4, 'WBBM', 'uploads/master/sertifikat-footer/KiIk86vAndijBrTmCTSEyixKt12L90eANybGMZxv.png', 'Wilayah Birokrasi Bersih dan Melayani', '2018-05-21 03:08:40', 1, NULL, NULL);
INSERT INTO public.log_ref_sertifikat_footer (id, id_sertifikat_footer, nama, gambar, keterangan, created_at, created_by, updated_at, updated_by) VALUES (5, 2, 'KARS', 'uploads/dokter/list-dokter/6K3uF4UNAO9U00kpvGBwlyzQpzVnUPM8Ca2WJFHi.png', 'KARS', '2018-05-21 03:08:02', 1, '2018-05-24 07:08:33', 1);
INSERT INTO public.log_ref_sertifikat_footer (id, id_sertifikat_footer, nama, gambar, keterangan, created_at, created_by, updated_at, updated_by) VALUES (6, 3, 'Satria Brand Award', 'uploads/master/sertifikat-footer/daezJF8jPGrxFdfmcmqJ6D7sJEfpmkU5MhjoaaX1.png', 'Merek Pilihan', '2018-05-21 03:08:19', 1, NULL, NULL);
INSERT INTO public.log_ref_sertifikat_footer (id, id_sertifikat_footer, nama, gambar, keterangan, created_at, created_by, updated_at, updated_by) VALUES (7, 4, 'WBBM', 'uploads/master/sertifikat-footer/KiIk86vAndijBrTmCTSEyixKt12L90eANybGMZxv.png', 'Wilayah Birokrasi Bersih dan Melayani', '2018-05-21 03:08:40', 1, NULL, NULL);


--
-- Name: log_ref_sertifikat_footer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_ref_sertifikat_footer_id_seq', 7, true);


--
-- Data for Name: log_ref_sub_spesialisasi; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_ref_sub_spesialisasi (id, id_sub_spesialisasi, nama, spesialisasi_induk, keterangan, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Cardiologist', 1, 'Pembedahan Jantung', '2018-05-28 04:28:38', 1, NULL, NULL);


--
-- Name: log_ref_sub_spesialisasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_ref_sub_spesialisasi_id_seq', 1, true);


--
-- Data for Name: log_rumah_sakit; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_rumah_sakit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_rumah_sakit_id_seq', 1, false);


--
-- Data for Name: log_slider; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_slider (id, id_slider, judul, sub_judul, deskripsi, url, photo, posisi, status, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Akses Mudah Menuju Sehat', 'Android Apps PJNHK', NULL, 'http://pjnhk.go.id', 'uploads/master/slider/PYoHDfihDhuZFpWFrlSEKMDgkW8tsF6T8QqsUYwR.jpeg', 0, 1, '2018-05-21 09:22:12', 1, NULL, NULL);
INSERT INTO public.log_slider (id, id_slider, judul, sub_judul, deskripsi, url, photo, posisi, status, created_at, created_by, updated_at, updated_by) VALUES (2, 1, 'Akses Mudah Menuju Sehat', 'Android Apps PJNHK', NULL, 'http://pjnhk.go.id', 'uploads/master/slider/z4cNHybBxsrCKvPapgYOUaG8fsCbkLx2yM95rr66.jpeg', 0, 1, '2018-05-21 09:22:12', 1, '2018-05-21 09:52:08', 1);
INSERT INTO public.log_slider (id, id_slider, judul, sub_judul, deskripsi, url, photo, posisi, status, created_at, created_by, updated_at, updated_by) VALUES (3, 2, 'Event Lebaran', 'Lebaran', 'Selamat Hari Lebaran', 'http://pjnhk.me', 'uploads/master/slider/kOPrkY69sZJspL71HFu30c2Q8M9VNLHhy4yHEdF2.jpeg', 0, 1, '2018-05-22 07:17:58', 1, NULL, NULL);
INSERT INTO public.log_slider (id, id_slider, judul, sub_judul, deskripsi, url, photo, posisi, status, created_at, created_by, updated_at, updated_by) VALUES (4, 1, 'Akses Mudah Menuju Sehat', 'Android Apps PJNHK', NULL, 'http://pjnhk.go.id', 'uploads/master/slider/NfcRhJ86pJeG3mDKgK9Xp1g1Sws5iYW5K6Rh7Ooo.jpeg', 0, 1, '2018-05-21 09:22:12', 1, '2018-05-24 07:26:34', 1);


--
-- Name: log_slider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_slider_id_seq', 4, true);


--
-- Data for Name: log_spesialisasi; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_spesialisasi (id, id_spesialisasi, nama, deskripsi, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Cardiologi', 'Cardiologi', '2018-05-15 04:06:59', 1, NULL, NULL);
INSERT INTO public.log_spesialisasi (id, id_spesialisasi, nama, deskripsi, created_at, created_by, updated_at, updated_by) VALUES (2, 2, 'Bedah Dewasa', 'Bedah Khusus Dewasa', '2018-05-15 04:19:38', 1, NULL, NULL);


--
-- Name: log_spesialisasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_spesialisasi_id_seq', 2, true);


--
-- Data for Name: log_tautan_footer; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_tautan_footer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_tautan_footer_id_seq', 1, false);


--
-- Data for Name: log_tipe_media; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_tipe_media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_tipe_media_id_seq', 1, false);


--
-- Data for Name: log_tipe_pendaftar; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_tipe_pendaftar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_tipe_pendaftar_id_seq', 1, false);


--
-- Data for Name: log_trans_alur_pendaftaran; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_alur_pendaftaran_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_alur_pendaftaran_id_seq', 1, false);


--
-- Data for Name: log_trans_aturan_pengunjung; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_aturan_pengunjung_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_aturan_pengunjung_id_seq', 1, false);


--
-- Data for Name: log_trans_hasil_seleksi; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_hasil_seleksi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_hasil_seleksi_id_seq', 1, false);


--
-- Data for Name: log_trans_info_bed; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_info_bed_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_info_bed_id_seq', 1, false);


--
-- Data for Name: log_trans_info_diklat; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_info_diklat (id, id_info_diklat, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Info Diklat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc id libero sagittis, sagittis risus eget, accumsan magna. Pellentesque nec risus nec purus consectetur eleifend. In vel ante tellus. Fusce suscipit malesuada dui, sollicitudin gravida purus facilisis quis. Donec id quam neque. Nullam finibus nisi mi, sed molestie orci faucibus ut. In hac habitasse platea dictumst. Vestibulum sollicitudin arcu eu felis mattis tempor sed tristique dui. Nulla id nunc eget augue venenatis dictum. Proin tristique leo eu sem pretium, lobortis tristique nulla volutpat.

Aliquam nec lorem efficitur, blandit tellus non, mattis dui. Vestibulum feugiat lacus eu dolor auctor, at varius mi dapibus. Pellentesque viverra quis mi pretium lobortis. Pellentesque tristique nulla quis eros gravida ultricies. Praesent blandit elementum eros commodo scelerisque. Vivamus placerat, dui ac molestie tempor, neque dui posuere nisl, vitae luctus ante neque ac nibh. Vivamus dignissim massa leo, in varius neque fermentum quis. Mauris hendrerit tempor justo, eu pellentesque ligula. Duis tincidunt interdum nibh, in dictum ipsum fermentum sit amet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas finibus dictum placerat. In imperdiet sem vel eleifend ultrices. Vestibulum gravida ut metus et condimentum. Pellentesque mollis dolor id arcu dapibus rhoncus. Morbi blandit quis est a aliquet.

Nunc tempus cursus dui eu sodales. Cras convallis elit at pretium mattis. Fusce malesuada lectus malesuada odio vulputate, sed vulputate erat mattis. Aenean consequat rutrum fermentum. Proin finibus congue sodales. Vivamus hendrerit fringilla luctus. Maecenas at risus ac tellus rhoncus dignissim. Quisque vulputate, diam sit amet sagittis tempor, est dolor sollicitudin elit, quis tempus mi mi ut dui. Quisque at urna vitae nibh malesuada auctor. Nam mollis viverra efficitur. Donec ipsum urna, egestas ut pharetra hendrerit, blandit nec nibh. Phasellus tristique consectetur turpis non fringilla. Sed rutrum lacinia velit, sit amet gravida risus rutrum ut. Praesent vehicula arcu vulputate tortor dignissim, at condimentum turpis facilisis.', '2018-05-31 09:50:36', 1, NULL, NULL);


--
-- Name: log_trans_info_diklat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_info_diklat_id_seq', 1, true);


--
-- Data for Name: log_trans_info_litbang; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_info_litbang (id, id_info_litbang, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Info Litbang', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc id libero sagittis, sagittis risus eget, accumsan magna. Pellentesque nec risus nec purus consectetur eleifend. In vel ante tellus. Fusce suscipit malesuada dui, sollicitudin gravida purus facilisis quis. Donec id quam neque. Nullam finibus nisi mi, sed molestie orci faucibus ut. In hac habitasse platea dictumst. Vestibulum sollicitudin arcu eu felis mattis tempor sed tristique dui. Nulla id nunc eget augue venenatis dictum. Proin tristique leo eu sem pretium, lobortis tristique nulla volutpat.

Aliquam nec lorem efficitur, blandit tellus non, mattis dui. Vestibulum feugiat lacus eu dolor auctor, at varius mi dapibus. Pellentesque viverra quis mi pretium lobortis. Pellentesque tristique nulla quis eros gravida ultricies. Praesent blandit elementum eros commodo scelerisque. Vivamus placerat, dui ac molestie tempor, neque dui posuere nisl, vitae luctus ante neque ac nibh. Vivamus dignissim massa leo, in varius neque fermentum quis. Mauris hendrerit tempor justo, eu pellentesque ligula. Duis tincidunt interdum nibh, in dictum ipsum fermentum sit amet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas finibus dictum placerat. In imperdiet sem vel eleifend ultrices. Vestibulum gravida ut metus et condimentum. Pellentesque mollis dolor id arcu dapibus rhoncus. Morbi blandit quis est a aliquet.', '2018-05-31 09:53:25', 1, NULL, NULL);


--
-- Name: log_trans_info_litbang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_info_litbang_id_seq', 1, true);


--
-- Data for Name: log_trans_info_mutu; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_info_mutu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_info_mutu_id_seq', 1, false);


--
-- Data for Name: log_trans_info_pasien; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_info_pasien_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_info_pasien_id_seq', 1, false);


--
-- Data for Name: log_trans_info_pengaduan; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_info_pengaduan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_info_pengaduan_id_seq', 1, false);


--
-- Data for Name: log_trans_info_whistle; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_info_whistle (id, id_info_whistle, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Whistleblowing', 'Whistleblowing 
Etika dan Hukum', '2018-05-12 13:37:41', 1, NULL, NULL);
INSERT INTO public.log_trans_info_whistle (id, id_info_whistle, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (2, 1, 'Whistleblowing Etika dan Hukum', 'Untuk pengaduan silahkan kirim email 
         etikhukum.wbs@pjnhk.go.id', '2018-05-12 13:37:41', 1, '2018-05-12 13:39:32', 1);
INSERT INTO public.log_trans_info_whistle (id, id_info_whistle, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (3, 1, 'Whistleblowing Etika dan Hukum', 'Untuk pengaduan silahkan kirim email : 
         

etikhukum.wbs@pjnhk.go.id', '2018-05-12 13:37:41', 1, '2018-05-12 13:40:47', 1);
INSERT INTO public.log_trans_info_whistle (id, id_info_whistle, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (4, 1, 'Whistleblowing Etika dan Hukum', 'Untuk pengaduan silahkan kirim email : 
         

etikhukum.wbs@pjnhk.go.id', '2018-05-12 13:37:41', 1, '2018-05-12 13:40:47', 1);
INSERT INTO public.log_trans_info_whistle (id, id_info_whistle, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (5, 2, 'Whistleblowing Pelayanan Pelanggan', 'Untuk Pengaduan silahkan email :

 Yangan.wbs@pjnhk.go.id', '2018-05-12 13:42:01', 1, NULL, NULL);
INSERT INTO public.log_trans_info_whistle (id, id_info_whistle, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (6, 2, 'Whistleblowing Pelayanan Pelanggan', 'Untuk Pengaduan silahkan email :

 yangan.wbs@pjnhk.go.id', '2018-05-12 13:42:01', 1, '2018-05-12 13:42:11', 1);
INSERT INTO public.log_trans_info_whistle (id, id_info_whistle, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (7, 3, 'Whistleblowing SPI (Satuan Pemeriksaan Internal)', 'Untuk Pengaduan silahkan email : spi.wbs@pjnhk.go.id', '2018-05-12 13:43:18', 1, NULL, NULL);


--
-- Name: log_trans_info_whistle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_info_whistle_id_seq', 7, true);


--
-- Data for Name: log_trans_item_pengaduan; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_item_pengaduan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_item_pengaduan_id_seq', 1, false);


--
-- Data for Name: log_trans_item_whistle; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_item_whistle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_item_whistle_id_seq', 1, false);


--
-- Data for Name: log_trans_jadwal_diklat; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_jadwal_diklat (id, id_trans_jadwal_diklat, judul, konten, keterangan, gambar, tanggal, jam, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Diklat International', '<p>Diklat International<br></p>', 'Diklat International', 'uploads/diklat/KOvxjlh7jOngk27pE8GE6PfowVLMd6GGI5DCqh0v.jpeg', '2018-05-02', '13:00:00', '2018-05-31 09:51:48', 1, NULL, NULL);


--
-- Name: log_trans_jadwal_diklat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_jadwal_diklat_id_seq', 1, true);


--
-- Data for Name: log_trans_jadwal_dokter; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_jadwal_dokter (id, id_jadwal_dokter, list_dokter_id, hari_praktek, jam_mulai_praktek, jam_selesai_praktek, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 1, 1, '13:00', '9:00', '2018-05-15 04:15:45', 1, NULL, NULL);
INSERT INTO public.log_trans_jadwal_dokter (id, id_jadwal_dokter, list_dokter_id, hari_praktek, jam_mulai_praktek, jam_selesai_praktek, created_at, created_by, updated_at, updated_by) VALUES (2, 1, 1, 1, '13:00', '16:00', '2018-05-15 04:15:45', 1, '2018-05-15 04:15:53', 1);


--
-- Name: log_trans_jadwal_dokter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_jadwal_dokter_id_seq', 2, true);


--
-- Data for Name: log_trans_karir; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_karir_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_karir_id_seq', 1, false);


--
-- Data for Name: log_trans_kegiatan; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_kegiatan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_kegiatan_id_seq', 1, false);


--
-- Data for Name: log_trans_komite_etik; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_komite_etik (id, id_komite_etik, nama, jabatan, departemen, tanggal_efektif_awal, tanggal_efektif_akhir, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Dokter Bambang', 1, 'Pas', '2018-05-03', '2018-05-09', '2018-05-31 09:54:17', 1, NULL, NULL);


--
-- Name: log_trans_komite_etik_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_komite_etik_id_seq', 1, true);


--
-- Data for Name: log_trans_kuesioner; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_kuesioner_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_kuesioner_id_seq', 1, false);


--
-- Data for Name: log_trans_kuesioner_jawaban; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_kuesioner_jawaban (id, id_kuesioner_jawaban, nama, email, kuesioner_id, feedback, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 'Stef', 'stefanus.christian.c@gmail.com', 1, 'Test', '2018-05-29 23:04:18', 1, NULL, NULL);


--
-- Data for Name: log_trans_kuesioner_jawaban_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_kuesioner_jawaban_detail (id, id_kuesioner_jawaban_detail, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 1, 1, '4', '2018-05-29 23:04:18', 1, NULL, NULL);
INSERT INTO public.log_trans_kuesioner_jawaban_detail (id, id_kuesioner_jawaban_detail, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (2, 2, 1, 2, '4', '2018-05-29 23:04:18', 1, NULL, NULL);
INSERT INTO public.log_trans_kuesioner_jawaban_detail (id, id_kuesioner_jawaban_detail, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (3, 3, 1, 3, '5', '2018-05-29 23:04:18', 1, NULL, NULL);
INSERT INTO public.log_trans_kuesioner_jawaban_detail (id, id_kuesioner_jawaban_detail, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (4, 4, 1, 4, '5', '2018-05-29 23:04:18', 1, NULL, NULL);
INSERT INTO public.log_trans_kuesioner_jawaban_detail (id, id_kuesioner_jawaban_detail, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (5, 5, 1, 5, 'Tet', '2018-05-29 23:04:18', 1, NULL, NULL);
INSERT INTO public.log_trans_kuesioner_jawaban_detail (id, id_kuesioner_jawaban_detail, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (6, 6, 1, 6, 'tet', '2018-05-29 23:04:18', 1, NULL, NULL);


--
-- Name: log_trans_kuesioner_jawaban_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_kuesioner_jawaban_detail_id_seq', 33, true);


--
-- Name: log_trans_kuesioner_jawaban_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_kuesioner_jawaban_id_seq', 33, true);


--
-- Data for Name: log_trans_layanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (1, 1, 'Jam Besuk', '<p>Jam Besuk Rumah Sakit Pusat Jantung Nasional Harapan Kita&nbsp;</p>', 'Jam Besuk Pasien', 'uploads/layanan/layanan/NAW5bzqfgiwEiKJ4y46OeEYC7N8rmlQu7dmmBEpg.jpeg', '2018-05-12 10:35:27', 1, NULL, NULL, 0, 'jam-besuk', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (2, 2, 'KRITIK dan SARAN', '<table width="540" style="margin: 0px; padding: 0px; font-size: 13px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; border-spacing: 0px; max-width: 100%; color: rgb(96, 96, 96); height: 543px;"><tbody style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Lembar &nbsp; &nbsp;</strong></h3><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">O Kritik &nbsp;</strong></h3><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">O </strong>S<strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">aran</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;">&nbsp;</h3></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Nama Lengkap &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;">.........................................................................................................</span></h3></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Alamat &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &nbsp;&nbsp;</strong></h3><h3 style="margin-top: 0px; margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</strong></h3><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;">&nbsp;<span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">......................................................................................................</span></span></h3><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; line-height: 15.808px;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;">&nbsp;......................................................................................................</span></h3></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Phone - &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong></h3><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;">&nbsp;<span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">......................................................................................................</span></span></h3><p style="padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;">&nbsp;</span></p></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Email &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :</strong></h3><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">&nbsp; ......................................................................................................</span></span></h3></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Isi Masukan &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp; &nbsp; &nbsp; &nbsp;</strong></h3><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; line-height: 15.808px;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">&nbsp; ......................................................................................................</span></span></h3><p style="padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</p><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; line-height: 15.808px;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">&nbsp; ......................................................................................................</span></span></h3></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;Lain-lain &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br></strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; line-height: 15.808px;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">&nbsp; ......................................................................................................</span></span></h3><p style="padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</p><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; line-height: 15.808px;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">&nbsp; ......................................................................................................</span></span></h3></td></tr></tbody></table><address style="margin: 20px 0px; padding: 0px; font-size: 13px; font-style: inherit; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; letter-spacing: 1px; color: rgb(96, 96, 96);">&nbsp;</address>', 'Kritik dan Saran', 'uploads/layanan/layanan/lWAgV2wJfNv3ifees8XZSQ3mVhVhvTMacaA1PvzA.jpeg', '2018-05-12 10:47:02', 1, NULL, NULL, 0, 'kritik-dan-saran', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (3, 3, 'Penunjang Medis', '<h1 style="margin-bottom: 20px; font-size: 23px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">PENUNJANG MEDIS</em></h1>', 'Penunjang Medis', 'uploads/layanan/layanan/VoFgefcljwegytMFmQAsDFuNNf5YJ2cdrFkeLspv.png', '2018-05-12 11:01:03', 1, NULL, NULL, 0, 'penunjang-medis', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (4, 4, 'Rawat Gawat Darurat UGD', '<p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">SPGDT dan layanan ambulans menawarkan layanan-layanan berikut: 1. Umum / pelayanan pasien; termasuk transortation tanah dan evakuasi, Call Center Layanan Darurat Medis (proyek percontohan dari Pemerintah Daerah Khusus Ibukota Jakarta dan di tingkat nasional) sebagai tuan rumah dalam penanganan darurat jantung, tingkat Tele EKG nasional, dan layanan darurat jantung, dengan unggul pengobatan Sindrom Koroner akut dan manajemen. 2. tim dukungan kardiovaskular selama acara negara, dari provinsi, nasional ke tingkat internasional 3. tim dukungan kardiovaskular selama acara ormas / Lembaga Swasta tim 4. dukungan medis dalam bencana mengacu pada Rencana Discharge Bencana RSJPDHK itu.Siapkan program pelatihan dan pengembangan bagi dokter dan perawat dari rumah sakit umum dan Pusat Kesehatan Masyarakat di wilayah Jakarta (jaringan rumah sakit) yang berfokus pada pengobatan jantung darurat dan manajemen, terutama Sindrom Koroner Akut, dan melatih rekaman EKG dan penularan selama EKG tele nasional di bawah koordinasi Departemen Kesehatan.</p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">Berkontribusi terhadap penelitian berupa pengumpulan data dan analisis dari SKA Registry, data Call Center dan evakuasi dan ambulans jasa.Layanan yang ditawarkan:</p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">* layanan Pasien • pelayanan pasien 24 jam</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Intensif ambulans kardiovaskular (evakuasi dan transportasi)</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• konsultan darurat kardiovaskular</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Sistem Rujukan</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Call Center Layanan Darurat Medis (kamar kosong) Pilot Project (DKI)</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Tele EKG (tingkat nasional) / Nyata waktu</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Koordinator dalam keadaan darurat kardiovaskular, terutama ACSTim medis kardiovaskular menyediakan supportduring peristiwa negara, mulai dari:&nbsp;</em></strong><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-size: 12.16px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 1.3em;">&nbsp; Tingkat Provinsi • Nasional • Internasional • Swasta / Yayasan Meningkatkan kompetensi layanan Gadar ACS seluruh &nbsp;Jakarta.</span></em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</em></strong></p><h1 style="margin-top: 0px; margin-bottom: 20px; font-size: 23px; font-style: italic; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Unit IGD / Emergency Departement</em></h1><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">Gawat darurat kardiovaskular (ED) terletak di bangunan utama, di lantai dasar dan dapat diakses langsung dari pintu masuk utama rumah sakit. Pada tahun 2013, ada 11.210 pasien dirawat di ED, yang 3186 pasien (28%) memiliki sindrom koroner akut (ACS). Akut elevasi ST-segmen infark miokard (STEMI) didiagnosis pada 896 pasien (28% dari populasi ACS). Pada 2013, terapi reperfusi akut dengan PCI primer dilakukan pada 330 pasien. Waktu rata-rata door-to-balon itu 90 menit. Sejak 2011, kami telah membangun Sistem Kardiovaskular Jakarta Perawatan Satuan Jaringan dan tujuan utama membangun jaringan adalah bagaimana kita dapat membuat diagnosis awal dari suatu infark, dengan demikian, diikuti dengan terapi reperfusi sebelumnya. Setelah implementasi jaringan lokal untuk pasien dengan STEMI, ada secara signifikan lebih antar-rumah sakit rujukan kasus, prosedur PCI lebih utama, dan lebih banyak pasien mencapai waktu door-to-needle ≤30 menit, dibandingkan dengan periode sebelum pelaksanaan jaringan ini. Untuk meningkatkan STEMI networking berdasarkan pedoman baru-baru ini, ada pra-rumah sakit dan di rumah sakit protokol harus ditingkatkan. Dalam pengaturan pra-rumah sakit, kami telah membuat bentuk triase pra-rumah sakit dan komunikasi ambulans bentuk grafik yang harus diisi oleh penyedia layanan kesehatan dalam pengaturan pra-rumah sakit. Juga, 12-lead EKG harus dicatat dalam semua dicurigai pasien dan harus dikirimkan ke pusat kami sebagai tuan rumah jaringan menggunakan telepon, email atau sistem faksimili. Di-rumah sakit protokol ditingkatkan dengan mengintegrasikan semua proses administrasi di ED, mengurangi waktu tunda untuk PCI primer</p>', 'SPGDT (Sistem Darurat Terpadu) dan Ambulance Service', NULL, '2018-05-12 11:04:49', 1, NULL, NULL, 0, 'rawat-gawat-darurat-ugd', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (5, 4, 'Rawat Gawat Darurat UGD', '<p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">SPGDT dan layanan ambulans menawarkan layanan-layanan berikut: 1. Umum / pelayanan pasien; termasuk transortation tanah dan evakuasi, Call Center Layanan Darurat Medis (proyek percontohan dari Pemerintah Daerah Khusus Ibukota Jakarta dan di tingkat nasional) sebagai tuan rumah dalam penanganan darurat jantung, tingkat Tele EKG nasional, dan layanan darurat jantung, dengan unggul pengobatan Sindrom Koroner akut dan manajemen. 2. tim dukungan kardiovaskular selama acara negara, dari provinsi, nasional ke tingkat internasional 3. tim dukungan kardiovaskular selama acara ormas / Lembaga Swasta tim 4. dukungan medis dalam bencana mengacu pada Rencana Discharge Bencana RSJPDHK itu.Siapkan program pelatihan dan pengembangan bagi dokter dan perawat dari rumah sakit umum dan Pusat Kesehatan Masyarakat di wilayah Jakarta (jaringan rumah sakit) yang berfokus pada pengobatan jantung darurat dan manajemen, terutama Sindrom Koroner Akut, dan melatih rekaman EKG dan penularan selama EKG tele nasional di bawah koordinasi Departemen Kesehatan.</p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">Berkontribusi terhadap penelitian berupa pengumpulan data dan analisis dari SKA Registry, data Call Center dan evakuasi dan ambulans jasa.Layanan yang ditawarkan:</p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">* layanan Pasien • pelayanan pasien 24 jam</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Intensif ambulans kardiovaskular (evakuasi dan transportasi)</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• konsultan darurat kardiovaskular</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Sistem Rujukan</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Call Center Layanan Darurat Medis (kamar kosong) Pilot Project (DKI)</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Tele EKG (tingkat nasional) / Nyata waktu</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Koordinator dalam keadaan darurat kardiovaskular, terutama ACSTim medis kardiovaskular menyediakan supportduring peristiwa negara, mulai dari:&nbsp;</em></strong><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-size: 12.16px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 1.3em;">&nbsp; Tingkat Provinsi • Nasional • Internasional • Swasta / Yayasan Meningkatkan kompetensi layanan Gadar ACS seluruh &nbsp;Jakarta.</span></em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</em></strong></p><h1 style="margin-top: 0px; margin-bottom: 20px; font-size: 23px; font-style: italic; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Unit IGD / Emergency Departement</em></h1><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">Gawat darurat kardiovaskular (ED) terletak di bangunan utama, di lantai dasar dan dapat diakses langsung dari pintu masuk utama rumah sakit. Pada tahun 2013, ada 11.210 pasien dirawat di ED, yang 3186 pasien (28%) memiliki sindrom koroner akut (ACS). Akut elevasi ST-segmen infark miokard (STEMI) didiagnosis pada 896 pasien (28% dari populasi ACS). Pada 2013, terapi reperfusi akut dengan PCI primer dilakukan pada 330 pasien. Waktu rata-rata door-to-balon itu 90 menit. Sejak 2011, kami telah membangun Sistem Kardiovaskular Jakarta Perawatan Satuan Jaringan dan tujuan utama membangun jaringan adalah bagaimana kita dapat membuat diagnosis awal dari suatu infark, dengan demikian, diikuti dengan terapi reperfusi sebelumnya. Setelah implementasi jaringan lokal untuk pasien dengan STEMI, ada secara signifikan lebih antar-rumah sakit rujukan kasus, prosedur PCI lebih utama, dan lebih banyak pasien mencapai waktu door-to-needle ≤30 menit, dibandingkan dengan periode sebelum pelaksanaan jaringan ini. Untuk meningkatkan STEMI networking berdasarkan pedoman baru-baru ini, ada pra-rumah sakit dan di rumah sakit protokol harus ditingkatkan. Dalam pengaturan pra-rumah sakit, kami telah membuat bentuk triase pra-rumah sakit dan komunikasi ambulans bentuk grafik yang harus diisi oleh penyedia layanan kesehatan dalam pengaturan pra-rumah sakit. Juga, 12-lead EKG harus dicatat dalam semua dicurigai pasien dan harus dikirimkan ke pusat kami sebagai tuan rumah jaringan menggunakan telepon, email atau sistem faksimili. Di-rumah sakit protokol ditingkatkan dengan mengintegrasikan semua proses administrasi di ED, mengurangi waktu tunda untuk PCI primer</p>', 'SPGDT (Sistem Darurat Terpadu) dan Ambulance Service', NULL, '2018-05-12 11:04:49', 1, NULL, NULL, 0, 'rawat-gawat-darurat-ugd', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (6, 5, 'Rawat Gawat Darurat UGD', '<h1 style="margin-bottom: 20px; font-size: 23px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">SPGDT (Sistem Darurat Terpadu) dan Ambulance Service</strong></h1><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">SPGDT dan layanan ambulans menawarkan layanan-layanan berikut: 1. Umum / pelayanan pasien; termasuk transortation tanah dan evakuasi, Call Center Layanan Darurat Medis (proyek percontohan dari Pemerintah Daerah Khusus Ibukota Jakarta dan di tingkat nasional) sebagai tuan rumah dalam penanganan darurat jantung, tingkat Tele EKG nasional, dan layanan darurat jantung, dengan unggul pengobatan Sindrom Koroner akut dan manajemen. 2. tim dukungan kardiovaskular selama acara negara, dari provinsi, nasional ke tingkat internasional 3. tim dukungan kardiovaskular selama acara ormas / Lembaga Swasta tim 4. dukungan medis dalam bencana mengacu pada Rencana Discharge Bencana RSJPDHK itu.Siapkan program pelatihan dan pengembangan bagi dokter dan perawat dari rumah sakit umum dan Pusat Kesehatan Masyarakat di wilayah Jakarta (jaringan rumah sakit) yang berfokus pada pengobatan jantung darurat dan manajemen, terutama Sindrom Koroner Akut, dan melatih rekaman EKG dan penularan selama EKG tele nasional di bawah koordinasi Departemen Kesehatan.</p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">Berkontribusi terhadap penelitian berupa pengumpulan data dan analisis dari SKA Registry, data Call Center dan evakuasi dan ambulans jasa.Layanan yang ditawarkan:</p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">* layanan Pasien • pelayanan pasien 24 jam</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Intensif ambulans kardiovaskular (evakuasi dan transportasi)</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• konsultan darurat kardiovaskular</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Sistem Rujukan</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Call Center Layanan Darurat Medis (kamar kosong) Pilot Project (DKI)</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Tele EKG (tingkat nasional) / Nyata waktu</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Koordinator dalam keadaan darurat kardiovaskular, terutama ACSTim medis kardiovaskular menyediakan supportduring peristiwa negara, mulai dari:&nbsp;</em></strong><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-size: 12.16px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 1.3em;">&nbsp; Tingkat Provinsi • Nasional • Internasional • Swasta / Yayasan Meningkatkan kompetensi layanan Gadar ACS seluruh &nbsp;Jakarta.</span></em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</em></strong></p><h1 style="margin-top: 0px; margin-bottom: 20px; font-size: 23px; font-style: italic; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Unit IGD / Emergency Departement</em></h1><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">Gawat darurat kardiovaskular (ED) terletak di bangunan utama, di lantai dasar dan dapat diakses langsung dari pintu masuk utama rumah sakit. Pada tahun 2013, ada 11.210 pasien dirawat di ED, yang 3186 pasien (28%) memiliki sindrom koroner akut (ACS). Akut elevasi ST-segmen infark miokard (STEMI) didiagnosis pada 896 pasien (28% dari populasi ACS). Pada 2013, terapi reperfusi akut dengan PCI primer dilakukan pada 330 pasien. Waktu rata-rata door-to-balon itu 90 menit. Sejak 2011, kami telah membangun Sistem Kardiovaskular Jakarta Perawatan Satuan Jaringan dan tujuan utama membangun jaringan adalah bagaimana kita dapat membuat diagnosis awal dari suatu infark, dengan demikian, diikuti dengan terapi reperfusi sebelumnya. Setelah implementasi jaringan lokal untuk pasien dengan STEMI, ada secara signifikan lebih antar-rumah sakit rujukan kasus, prosedur PCI lebih utama, dan lebih banyak pasien mencapai waktu door-to-needle ≤30 menit, dibandingkan dengan periode sebelum pelaksanaan jaringan ini. Untuk meningkatkan STEMI networking berdasarkan pedoman baru-baru ini, ada pra-rumah sakit dan di rumah sakit protokol harus ditingkatkan. Dalam pengaturan pra-rumah sakit, kami telah membuat bentuk triase pra-rumah sakit dan komunikasi ambulans bentuk grafik yang harus diisi oleh penyedia layanan kesehatan dalam pengaturan pra-rumah sakit. Juga, 12-lead EKG harus dicatat dalam semua dicurigai pasien dan harus dikirimkan ke pusat kami sebagai tuan rumah jaringan menggunakan telepon, email atau sistem faksimili. Di-rumah sakit protokol ditingkatkan dengan mengintegrasikan semua proses administrasi di ED, mengurangi waktu tunda untuk PCI primer</p>', 'Alur Pasien IGD', 'uploads/layanan/layanan/1xCQW7dUWSbGIxVHSuIzpmPaHo2f632Mg5uXWikX.jpeg', '2018-05-12 11:09:46', 1, NULL, NULL, 0, 'rawat-gawat-darurat-ugd', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (7, 6, 'Arrytmia', '<p>Laboratorium Kateterisasi (Vaskuler)<br></p>', 'Laboratorium Kateterisasi (Vaskuler)', NULL, '2018-05-21 04:27:01', 1, NULL, NULL, 1, 'arrytmia', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (8, 6, 'Arrytmia', '<p>Laboratorium Kateterisasi (Vaskuler)<br></p>', 'Laboratorium Kateterisasi (Vaskuler)', 'uploads/layanan/layanan/AYm7XUzmp7Ip7pZ6jMkSjavEDpTKhcfnW6mVUdWu.png', '2018-05-21 04:27:01', 1, '2018-05-21 04:27:45', 1, 1, 'arrytmia', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (9, 6, 'Arrytmia', '<p>Laboratorium Kateterisasi (Vaskuler) mampu melayani  kasus aritmia fibrilasi atrium dan takikardia supraventrikular.</p>', 'Laboratorium Kateterisasi (Vaskuler)', 'uploads/layanan/layanan/AYm7XUzmp7Ip7pZ6jMkSjavEDpTKhcfnW6mVUdWu.png', '2018-05-21 04:27:01', 1, '2018-05-21 04:29:07', 1, 1, 'arrytmia', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (10, 6, 'Arrytmia', '<p>Laboratorium Kateterisasi (Vaskuler) mampu melayani  kasus aritmia fibrilasi atrium dan takikardia supraventrikular.</p>', 'Laboratorium Kateterisasi (Vaskuler) mampu melayani kasus aritmia fibrilasi atrium dan takikardia supraventrikular.', 'uploads/layanan/layanan/AYm7XUzmp7Ip7pZ6jMkSjavEDpTKhcfnW6mVUdWu.png', '2018-05-21 04:27:01', 1, '2018-05-21 04:29:17', 1, 1, 'arrytmia', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (11, 6, 'Arrytmia', '<p>Laboratorium Kateterisasi (Vaskuler) mampu melayani  kasus aritmia fibrilasi atrium dan takikardia supraventrikular.</p>', 'Laboratorium Kateterisasi melayani kasus aritmia fibrilasi atrium dan takikardia supraventrikular.', 'uploads/layanan/layanan/AYm7XUzmp7Ip7pZ6jMkSjavEDpTKhcfnW6mVUdWu.png', '2018-05-21 04:27:01', 1, '2018-05-21 04:29:33', 1, 1, 'arrytmia', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (12, 6, 'Arrytmia', '<p>Laboratorium Kateterisasi (Vaskuler) mampu melayani  kasus aritmia fibrilasi atrium dan takikardia supraventrikular.</p>', 'Melayani kasus aritmia fibrilasi atrium dan takikardia supraventrikular.', 'uploads/layanan/layanan/AYm7XUzmp7Ip7pZ6jMkSjavEDpTKhcfnW6mVUdWu.png', '2018-05-21 04:27:01', 1, '2018-05-21 04:29:49', 1, 1, 'arrytmia', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (13, 7, 'Medical Check-Up', '<p>Deteksi Dini Kardiovaskuler<br></p>', 'Deteksi Dini Kardiovaskuler', 'uploads/layanan/layanan/R6khls7OviGYaJ9w09IkSVlJBZMhPi4Biz9OKZs2.png', '2018-05-21 04:31:27', 1, NULL, NULL, 0, 'medical-check-up', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (14, 7, 'Medical Check-Up', '<p>Deteksi Dini Kardiovaskuler<br></p>', 'Deteksi Dini Kardiovaskuler', 'uploads/layanan/layanan/R6khls7OviGYaJ9w09IkSVlJBZMhPi4Biz9OKZs2.png', '2018-05-21 04:31:27', 1, '2018-05-21 04:31:35', 1, 1, 'medical-check-up', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (15, 8, 'Weekend Service', '<p>Melayani pada hari weekend<br></p>', 'Melayani pasien semaksimal mungkin, bahkan pada hari libur', 'uploads/layanan/layanan/5ebxTsWllDZJz7fJ9uiaU9hEf2W4f5QCOPdGpAkx.png', '2018-05-21 05:05:53', 1, NULL, NULL, 1, 'weekend-service', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (16, 8, 'Weekend Service', '<p>Melayani pada hari weekend<br></p>', 'Melayani pasien semaksimal mungkin, bahkan pada hari libur', 'uploads/layanan/layanan/Et1znIydX5OC37DNwK0K1EcW9kqVR57XjwsoOpej.png', '2018-05-21 05:05:53', 1, '2018-05-21 05:10:16', 1, 1, 'weekend-service', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (17, 7, 'Medical Check-Up', '<p>Deteksi Dini Kardiovaskuler<br></p>', 'Deteksi Dini Kardiovaskuler. Mencegah lebih baik dari pada mengobati.', 'uploads/layanan/layanan/R6khls7OviGYaJ9w09IkSVlJBZMhPi4Biz9OKZs2.png', '2018-05-21 04:31:27', 1, '2018-05-21 05:10:40', 1, 1, 'medical-check-up', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (18, 6, 'Arrytmia', '<p>Laboratorium Kateterisasi (Vaskuler) mampu melayani  kasus aritmia fibrilasi atrium dan takikardia supraventrikular.</p>', 'Melayani kasus aritmia fibrilasi atrium dan takikardia supraventrikular.', 'uploads/layanan/layanan/fuEEiTTTCLkFzEJHNSEUIXH0XumosUvzO5lWIvbL.png', '2018-05-21 04:27:01', 1, '2018-05-21 05:11:32', 1, 1, 'arrytmia', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (19, 7, 'Medical Check-Up', '<p>Deteksi Dini Kardiovaskuler<br></p>', 'Deteksi Dini Kardiovaskuler. Mencegah lebih baik dari pada mengobati.', 'uploads/layanan/layanan/EYPwfjgkAPboArX7WQfcafurTumtQbjaX9pf2PTF.png', '2018-05-21 04:31:27', 1, '2018-05-21 05:11:44', 1, 1, 'medical-check-up', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (20, 8, 'Weekend Service', '<p>Melayani pada hari weekend<br></p>', 'Melayani pasien semaksimal mungkin, bahkan pada hari libur', 'uploads/layanan/layanan/rPnvI9oA3TLtv3XvshqJso2n7lY41tSLbtJcCuFM.png', '2018-05-21 05:05:53', 1, '2018-05-21 05:12:00', 1, 1, 'weekend-service', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (21, 8, 'Weekend Service', '<p>Melayani pada hari weekend<br></p>', 'Melayani pasien semaksimal mungkin, bahkan pada hari libur', 'uploads/layanan/layanan/rPnvI9oA3TLtv3XvshqJso2n7lY41tSLbtJcCuFM.png', '2018-05-21 05:05:53', 1, '2018-05-21 05:12:00', 1, 1, 'weekend-service', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (22, 2, 'KRITIK dan SARAN', '<table width="540" style="margin: 0px; padding: 0px; font-size: 13px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; border-spacing: 0px; max-width: 100%; color: rgb(96, 96, 96); height: 543px;"><tbody style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Lembar &nbsp; &nbsp;</strong></h3><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">O Kritik &nbsp;</strong></h3><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">O </strong>S<strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">aran</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;">&nbsp;</h3></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Nama Lengkap &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;">.........................................................................................................</span></h3></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Alamat &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &nbsp;&nbsp;</strong></h3><h3 style="margin-top: 0px; margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</strong></h3><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;">&nbsp;<span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">......................................................................................................</span></span></h3><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; line-height: 15.808px;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;">&nbsp;......................................................................................................</span></h3></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Phone - &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong></h3><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;">&nbsp;<span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">......................................................................................................</span></span></h3><p style="padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;">&nbsp;</span></p></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Email &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :</strong></h3><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">&nbsp; ......................................................................................................</span></span></h3></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Isi Masukan &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp; &nbsp; &nbsp; &nbsp;</strong></h3><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; line-height: 15.808px;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">&nbsp; ......................................................................................................</span></span></h3><p style="padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</p><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; line-height: 15.808px;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">&nbsp; ......................................................................................................</span></span></h3></td></tr><tr style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;Lain-lain &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br></strong></h3></td><td style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border-width: 0px; border-style: initial; border-color: initial; font-family: inherit; outline: 0px; vertical-align: baseline;"><h3 style="margin-bottom: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; line-height: 15.808px;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">&nbsp; ......................................................................................................</span></span></h3><p style="padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</p><h3 style="margin-top: 0px; font-size: 16px; font-style: italic; text-align: inherit; background: transparent; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; line-height: 15.808px;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 15.808px;">&nbsp; ......................................................................................................</span></span></h3></td></tr></tbody></table><address style="margin: 20px 0px; padding: 0px; font-size: 13px; font-style: inherit; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; letter-spacing: 1px; color: rgb(96, 96, 96);">&nbsp;</address>', 'Kritik dan Saran', 'uploads/layanan/layanan/lWAgV2wJfNv3ifees8XZSQ3mVhVhvTMacaA1PvzA.jpeg', '2018-05-12 10:47:02', 1, NULL, NULL, 0, 'kritik-dan-saran', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (23, 9, 'Diagnostik Non Invasif & Intervensi Non Bedah', '<p>Pusat Aorta dan perifer dengan tindakan bedah tanpa bedah (TEVAR, EVAR, BENTALL, Prosedure, EVLT)<br></p>', 'Pusat Aorta dan perifer dengan tindakan bedah tanpa bedah (TEVAR, EVAR, BENTALL, Prosedure, EVLT)', 'uploads/layanan/layanan/ETbNnmxZtlSzxIrwAAeBEgHwVryHK6ZEVituzTPe.png', '2018-05-30 06:25:13', 1, NULL, NULL, 1, 'diagnostik-non-invasif--intervensi-non-bedah', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (24, 9, 'DINB', '<p>Diagnostik Non Invasif &amp; Intervensi Non Bedah, Pusat Aorta dan perifer dengan tindakan bedah tanpa bedah (TEVAR, EVAR, BENTALL, Prosedure, EVLT)<br></p>', 'Pusat Aorta dan perifer dengan tindakan bedah tanpa bedah (TEVAR, EVAR, BENTALL, Prosedure, EVLT)', 'uploads/layanan/layanan/ETbNnmxZtlSzxIrwAAeBEgHwVryHK6ZEVituzTPe.png', '2018-05-30 06:25:13', 1, '2018-05-31 04:01:36', 1, 1, 'diagnostik-non-invasif--intervensi-non-bedah', NULL);
INSERT INTO public.log_trans_layanan (id, id_layanan, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (25, 9, 'DINB', '<p>Diagnostik Non Invasif & Intervensi Non Bedah, Pusat Aorta dan perifer dengan tindakan bedah tanpa bedah (TEVAR, EVAR, BENTALL, Prosedure, EVLT)<br></p>', 'Pusat Aorta dan perifer dengan tindakan tanpa bedah (TEVAR, EVAR, BENTALL, Prosedure, EVLT)', 'uploads/layanan/layanan/ETbNnmxZtlSzxIrwAAeBEgHwVryHK6ZEVituzTPe.png', '2018-05-30 06:25:13', 1, '2018-05-31 04:02:03', 1, 1, 'diagnostik-non-invasif--intervensi-non-bedah', NULL);


--
-- Data for Name: log_trans_layanan_umum; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_layanan_umum_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_layanan_umum_id_seq', 1, false);


--
-- Name: log_trans_layanan_unggulan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_layanan_unggulan_id_seq', 25, true);


--
-- Data for Name: log_trans_list_artikel; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_list_artikel (id, id_list_artikel, judul, konten, created_at, created_by, updated_at, updated_by, slug, deskripsi, photo, kategori_id) VALUES (1, 1, 'Penyelenggaraan Bimbingan SNARS', '<p style="margin: 0px 0px 6px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; line-height: 19.32px; text-align: justify;">Rabu
 (25/4) RS Jantung dan Pembuluh Darah Harapan Kita menyelenggarakan 
bimbingan Standar Nasional Akreditasi Rumah Sakit (SNARS) edisi 1 di 
Ruang Rapat Direksi Gedung Utama RSJPDHK, yang dibimbing langsung <em><strong>dr. Nico A. Lumenta K. Nefro, MM., MH. Kes</strong></em> dari <em>Komisi Akreditasi Rumah Sakit (KARS</em>).
 Bimbingan ini dilakukan sebagai salah satu bentuk persiapan rumah sakit
 dalam menghadapi Re-Akreditasi Nasional KARS pada Oktober - November 
mendatang.</p>
<p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; line-height: 19.32px; text-align: justify;">Hari ini dan esok merupakan bimbing<span class="text_exposed_show" style="display: inline; font-family: inherit;">an
 gelombang pertama, dimana hal yang menjadi fokus pembahasan adalah 
mengenai Standar Pelayanan terhadap Pasien. Sedangkan untuk bimbingan 
kedua, akan diselenggarakan pada tanggal 7 - 9 Mei mendatang, membahas 
tentang Standar Manajemen yang sesuai dengan SNARS.</span></p>
<div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; line-height: 19.32px;">
<p style="margin: 0px 0px 6px; font-family: inherit; text-align: justify;">Kami
 berharap, melalui bimbingan ini rumah sakit dapat mengimplementasikan 
teori-teori yang didapat, memperbaiki kualitas pelayanan, sehingga 
berhasil lulus dalam Re-Akreditasi dan tentunya dapat memberikan 
pelayanan yang lebih lagi kepada pasien. <em>(Narasi :Nurhasanah / Hukormas)</em></p>
</div>', '2018-05-22 05:59:49', 1, NULL, NULL, 'bimbingan-snars', 'RSJPDHK menyelenggarakan bimbingan SNARS', NULL, 1);


--
-- Name: log_trans_list_artikel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_list_artikel_id_seq', 1, true);


--
-- Data for Name: log_trans_list_dokter; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_list_dokter (id, id_list_dokter, nama, spesialisasi_id, alamat, no_tlpn, deskripsi, photo, created_at, created_by, updated_at, updated_by, jabatan_dokter_id) VALUES (1, 1, 'dr ADE MEIDIAN AMBARI , SpJp', 1, 'Jakarta', '08123456789', NULL, 'uploads/dokter/list-dokter/wqY0fiA54874Zv4avLutNqaiOmEqNARpmOpMaaMa.jpeg', '2018-05-15 04:12:47', 1, NULL, NULL, 1);
INSERT INTO public.log_trans_list_dokter (id, id_list_dokter, nama, spesialisasi_id, alamat, no_tlpn, deskripsi, photo, created_at, created_by, updated_at, updated_by, jabatan_dokter_id) VALUES (2, 2, 'DR.dr. AMILIANA MARDIANI , Sp.JP(K)', 1, 'Jakarta', '08123456789', NULL, 'uploads/dokter/list-dokter/HZLCI6ZGw5XwoCi5DisSYcgsATdRH5iArzLpsyO9.jpeg', '2018-05-15 04:17:11', 1, NULL, NULL, 1);
INSERT INTO public.log_trans_list_dokter (id, id_list_dokter, nama, spesialisasi_id, alamat, no_tlpn, deskripsi, photo, created_at, created_by, updated_at, updated_by, jabatan_dokter_id) VALUES (3, 3, 'dr. AMIN TJUBANDI , Sp.BTKV', 1, 'Jakarta', '08123456789', NULL, 'uploads/dokter/list-dokter/He3XkxsOdYbUJL27Q8ymzmNQAnrVP3XANVTUpmlf.jpeg', '2018-05-15 04:18:41', 1, NULL, NULL, 1);
INSERT INTO public.log_trans_list_dokter (id, id_list_dokter, nama, spesialisasi_id, alamat, no_tlpn, deskripsi, photo, created_at, created_by, updated_at, updated_by, jabatan_dokter_id) VALUES (4, 4, 'dr. ANNA ULFA RAHAYOE , Sp.JP(K)', 1, 'Jakarta', '08123456789', NULL, 'uploads/dokter/list-dokter/8YPlvDrGNqhLC8OkMPTCgmIUEOXZPNXeMTAIVJpP.jpeg', '2018-05-15 04:21:56', 1, NULL, NULL, 1);
INSERT INTO public.log_trans_list_dokter (id, id_list_dokter, nama, spesialisasi_id, alamat, no_tlpn, deskripsi, photo, created_at, created_by, updated_at, updated_by, jabatan_dokter_id) VALUES (5, 2, 'DR.dr. AMILIANA MARDIANI , Sp.JP(K)', 1, 'Jakarta', '08123456789', NULL, 'uploads/dokter/list-dokter/HZLCI6ZGw5XwoCi5DisSYcgsATdRH5iArzLpsyO9.jpeg', '2018-05-15 04:17:11', 1, '2018-05-15 05:06:40', 1, 1);
INSERT INTO public.log_trans_list_dokter (id, id_list_dokter, nama, spesialisasi_id, alamat, no_tlpn, deskripsi, photo, created_at, created_by, updated_at, updated_by, jabatan_dokter_id) VALUES (6, 1, 'dr ADE MEIDIAN AMBARI , SpJp', 1, 'Jakarta', '08123456789', NULL, 'uploads/dokter/list-dokter/wqY0fiA54874Zv4avLutNqaiOmEqNARpmOpMaaMa.jpeg', '2018-05-15 04:12:47', 1, '2018-05-31 09:42:02', 1, 1);


--
-- Name: log_trans_list_dokter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_list_dokter_id_seq', 6, true);


--
-- Data for Name: log_trans_list_publikasi; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_list_publikasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_list_publikasi_id_seq', 1, false);


--
-- Data for Name: log_trans_pengumuman; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_pengumuman_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_pengumuman_id_seq', 1, false);


--
-- Data for Name: log_trans_profile; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (1, 1, 'Struktur Organisasi', '<p>Struktur Organisasi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita<br></p>', 'Struktur Organisasi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita', '2018-05-12 09:25:04', 1, NULL, NULL, 'struktur-organisasi', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (2, 2, 'Jajaran Direksi', '<p><span style="color: rgba(0, 0, 0, 0.87); font-size: 12px;">&nbsp;Jajaran Direksi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita</span><br></p>', 'Jajaran Direksi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita', '2018-05-12 09:46:29', 1, NULL, NULL, 'jajaran-direksi', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (3, 3, 'Sambutan', '<p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Putra Palembang yang terlahir pada 1 Januari 1966</span>&nbsp; merupakan seorang Dokter Spesialis Jantung dan Pembuluh Darah. Saat ini, dr. Iwan Dakota berpraktek di Rumah Sakit Jantung&nbsp;dan Pembuluh Darah&nbsp;Harapan Kita di Slipi, Jakarta Barat. Sebagai seorang dokter, juga sebagai Direktur Utama &nbsp;dan beliau telah mengenyam pendidikan di Universitas Sriwijaya tahun 1985-1992, untuk S1 Kedokteran Umum, dilanjutkan S2 Spesialias Kardiologi 1996-2001 dan S3 untuk Magister RS tahun 2012-2014 di Universitas Indonesia.</p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">Pertama-tama marilah kita mengucapkan Puji Syukur kehadirat Allah SWT, karena atas Rahmat dan Hidayah-Nya, kita dapat menyusun Annual Report Tahun 2015. Pada tahun 2015 Rumah Sakit Jantung Dan Pembuluh Darah Harapan Kita (RSJPDHK) telah berupaya meningkatkan mutu pelayanan melalui produk unggulan, dengan terwujudnya Akreditasi KARS dengan hasil Paripurna, yang merupakan prestasi dari seluruh kinerja Karyawan RSJPDHK. Ke depan RSJPDHK berkomitmen untuk mewujudkan akreditasi Internasional melalui Akreditasi Joint Commission International (JCI) yang menjadi wujud nyata peningkatan profesionalisme bagi Rumah Sakit. RSJPDHK adalah rumah sakit rujukan tersier yang mengutamakan pelayanan unggulan dengan prosedur tindakan yang didukung dengan alat berteknologi canggih, dan tenaga medis dan non-medis yang berkompeten dengan mengutamakan mutu pelayanan dan keselamatan pasien. RSJPDHK selalu berkerjasama dengan institusi ternama di luar negeri untuk meningkatkan profesionalitastenaga medis dan non medis untuk pelayanan kardiovaskular. Evaluasi kinerja tahunan setiap kali dilakukan secara konsisten dengan tujuan untuk mengetahui tingkat efiensi dan kualitas dari seluruh unit revenue center dalam memberikan kontribusi pelayanan terhadap pelanggan RSJPDHK, sebagai dasar untuk melakukan suatu langkah perbaikan dalam peningkatan mutu dan memperkecil faktor risiko yang timbul dari proses pelayanan tersebut. Apresiasi dan harapan dari manajemen dan stake holder dalam tahun berikutnya akan mewujudkan RSJPDHK yang memilki nilai dan budaya organisasi yang lebih baik. Manajemen mengucapkan terima kasih atas penghargaan setinggi-tingginya kepada seluruh karyawan RSJPDHK atas suksesnya penyelenggaraan pelayaan prima terhadap seluruh pelanggan RSJPDHK.</p>', 'Dr. dr. Iwan Dakota, Sp.JP(K) MARS,FACC, FESC', '2018-05-12 09:48:54', 1, NULL, NULL, 'sambutan', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (4, 4, 'Dewan Pengawas', '<p><span style="color: rgba(0, 0, 0, 0.87); font-size: 12px;">Dewan Pengawas Rumah Sakit Jantung dan Pembuluh Darah Harapan kita</span><br></p>', 'Dewan Pengawas Rumah Sakit Jantung dan Pembuluh Darah Harapan kita', '2018-05-12 09:52:57', 1, NULL, NULL, 'dewan-pengawas', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (5, 5, 'Makna dan Ketentuan Logo', '<h4 style="margin-bottom: 0px; font-size: 14px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="font-family: inherit; font-size: 23px; font-style: inherit; background: transparent; text-align: inherit; margin: 0px; padding: 0px; font-weight: bold; border: 0px; outline: 0px; vertical-align: baseline;">Pusat Jantung Nasional Harapan Kita</strong><br></h4>', 'Makna dan Ketentuan Logo', '2018-05-12 09:56:21', 1, NULL, NULL, 'makna-dan-ketentuan-logo-', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (6, 5, 'Makna dan Ketentuan Logo', '<h4 style="margin-bottom: 0px; font-size: 14px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="font-family: inherit; font-size: 23px; font-style: inherit; background: transparent; text-align: inherit; margin: 0px; padding: 0px; font-weight: bold; border: 0px; outline: 0px; vertical-align: baseline;">&nbsp; &nbsp; Pusat Jantung Nasional Harapan Kita</strong><br></h4>', 'Makna dan Ketentuan Logo', '2018-05-12 09:56:21', 1, '2018-05-12 09:59:21', 1, 'makna-dan-ketentuan-logo', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (7, 6, 'Tentang Kami', '<h2 style="margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><p class="MsoNormal" style="margin-bottom: 0.0001pt; padding: 0px; font-size: 13px; font-weight: 400; font-style: normal; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96); text-indent: 1cm; line-height: 19.5px;"><em style="font-family: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita (RSJPDHK) merupakan rumah sakit khusus yang menjadi Pusat Rujukan Nasional untuk penanganan penyakit jantung dan pembuluh darah (kardiovaskular). Rumah sakit ini didirikan oleh Yayasan Harapan Kita diatas tanah&nbsp;</span><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; text-decoration-line: underline; font-size: 12px;">seluas 22.389</span><span style="font-size: 12px; font-family: Arial;">&nbsp;m2</span></em></strong><span style="font-size: 12px; font-family: Arial;">&nbsp;di Jl. S. Parman kavling 87 Slipi, Jakarta Barat dan diresmikan pada tanggal</span><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; text-decoration-line: underline; font-size: 12px;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;9 Nopember 1985.</strong></em></span></span></em><br></p><p class="MsoNormal" style="margin-bottom: 0.0001pt; padding: 0px; font-size: 13px; font-weight: 400; font-style: normal; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96); text-indent: 1cm; line-height: 19.5px;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><br></strong></em></span></span></em><em style="font-family: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; font-size: 12px;">Pada tanggal 27 Maret 1985 Yayasan Harapan Kita melalui Surat Keputusan No.02/1985 menyerahkan kepemilikan rumah sakit ini kepada pemerintah dalam hal ini Departemen Kesehatan, tetapi pengelolaannya diserahkan kepada Yayasan Harapan Kita berdasarkan SK No. 57/Menkes/SK/II/1985. Pada tanggal 31 Juli 1997 Yayasan Harapan Kita menyerahkan kembali pengelolaan Rumah Sakit Jantung Harapan Kita kepada Departemen Kesehatan Republik Indonesia dan selanjutnya melalui Peraturan Pemerintah nomor 126 tahun 2000, status Rumah Sakit Jantung Harapan Kita pun berubah menjadi Perusahaan Jawatan dibawah naungan Kementerian BUMN.&nbsp;</span></em></p><p class="MsoNormal" style="margin-bottom: 0.0001pt; padding: 0px; font-size: 13px; font-weight: 400; font-style: normal; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96); text-indent: 1cm; line-height: 19.5px;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><br></span></em><em style="font-family: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; font-size: 12px;">Pada tanggal 13 Juni 2005, ditetapkan Peraturan Pemerintah nomor 23 tahun 2005 tentang Pola Pengelolaan Keuangan Badan Layanan Umum, yang menyebutkan perubahan status rumah sakit yang semula berstatus Perusahaan Jawatan (Badan Usaha Milik Negara) menjadi Badan Layanan Umum (pasal 37 ayat 2). Dengan demikian, Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita pun berubah statusnya menjadi BLU-RSJPD Harapan Kita, yang berada di bawah Kementerian Kesehatan RI sebagai Unit Pelaksanaan Teknis dengan menerapkan Pola Pengelolaan Keuangan Badan Layanan Umum (PPK-BLU).</span></em></p><p class="MsoNormal" style="margin-bottom: 0.0001pt; padding: 0px; font-size: 13px; font-weight: 400; font-style: normal; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96); text-indent: 1cm; line-height: 19.5px;"><em style="font-family: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><br></span></em><em style="font-family: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; font-size: 12px;">Sebagai Pusat Jantung Nasional (National Cardiovascular Centre), selain menyediakan pelayanan kesehatan jantung, RSJPDHK juga dikembangkan sebagai wahana pendidikan serta pelatihan, dan penelitian&nbsp;</span></em><span style="font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; font-size: 12pt; border: 0px; font-family: &quot;Times New Roman&quot;, serif; outline: 0px; vertical-align: baseline; line-height: 24px;"><span style="font-size: 12px; font-family: Arial;">dalam bidang kesehatan kardiovaskular. Berbagai upaya telah dilaksanakan untuk menciptakan&nbsp;</span><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; font-size: 12px;">Good Corporate Governance,&nbsp;</span><span style="font-size: 12px; font-family: Arial;">yakni: transparansi, kemandirian, akuntabilitas, pertanggung jawaban dan kewajaran. Salah satu wujud pelaksanaannya adalah senantiasa meningkatkan mutu layanan yang salah satu upaya dilakukan melalui program akreditasi baik tingkat Nasional maupun Internasional.</span></span></p><p class="MsoNormal" style="margin-bottom: 0.0001pt; padding: 0px; font-size: 13px; font-weight: 400; font-style: normal; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96); text-indent: 1cm; line-height: 19.5px;"><span style="font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; font-size: 12pt; border: 0px; font-family: &quot;Times New Roman&quot;, serif; outline: 0px; vertical-align: baseline; line-height: 24px;"><br></span><span style="font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background-color: transparent; font-family: Arial; font-size: 12px;">Akreditasi dibidang pendidikan juga dilakukan terkait dengan penyelenggaraan pedidikan sebagai salah satu rumah sakit pendidikan dibidang kardiovaskular. Adapun jenis akreditasi dimaksud yang telah dicapai diantaranya akreditasi rumah sakit pendidikan oleh Kementerian Kesehatan RI pada tahun 2014,&nbsp;</span><strong style="font-family: inherit; font-style: inherit; text-align: inherit; text-indent: 1cm; background: transparent; font-size: 12pt; margin: 0px; padding: 0px; font-weight: bold; border: 0px; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; text-decoration-line: underline; font-size: 12px;">Akreditasi KARS paripurna versi 2012</span><span style="font-size: 12px; font-family: Arial;">&nbsp;</span></em></strong><span style="font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background-color: transparent; font-family: Arial; font-size: 12px;">pada tahun 2015, serta&nbsp;</span><span style="font-family: Arial; font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; font-size: 12px; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Akreditasi Internasional Joint Commition International (JCI) pada tahun 2016</em></strong></span><span style="font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background-color: transparent; font-family: Arial; font-size: 12px;">.</span></p></h2>', 'Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita', '2018-05-12 10:06:44', 1, NULL, NULL, 'tentang-kami', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (8, 7, 'Visi Misi', '<p>Visi &amp; Misi Rumah Sakit Jantung dan Hati Harapan Kita</p>', 'Visi & Misi Rumah Sakit Jantung dan Hati Harapan Kita', '2018-05-12 10:11:16', 1, NULL, NULL, 'visi-misi', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (9, 8, 'Lokasi', '<p>Lokasi Rumah Sakit Pusat Jantung Nasional Harapan Kita</p>', 'Lokasi Rumah Sakit', '2018-05-12 10:16:36', 1, NULL, NULL, 'lokasi-', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (10, 9, 'Aksebilitas RS', '<h2 style="margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">1. Website : &nbsp;<a href="http://www.pjnhk.go.id/" style="margin: 0px; padding: 0px; font-style: inherit; text-align: inherit; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; outline: 0px; vertical-align: baseline; transition: all 0.15s ease; color: rgb(45, 132, 182); text-decoration: underline;">http://www.pjnhk.go.id</a></em></strong></h2><h2 style="margin-top: 0px; margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">2. Perjanjian Online via Android : (download di &nbsp;</strong><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: arial, sans-serif; outline: 0px; vertical-align: baseline; white-space: nowrap; color: rgb(0, 102, 33); line-height: 16px;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; white-space: normal;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><a href="https://play.google.com/store/apps" style="margin: 0px; padding: 0px; font-style: inherit; text-align: inherit; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; outline: 0px; vertical-align: baseline; transition: all 0.15s ease; color: rgb(45, 132, 182); text-decoration: underline;">https://play.google.com/store/apps</a></em></strong>&nbsp;atau&nbsp;</span><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 1.3em;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Google Playstore) : PJNHK</em></strong></h2><h2 style="margin-top: 0px; margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">3. SPGDT - IGD 24 Jam</strong></h2><h2 style="margin-top: 0px; margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">4. Layanan Poliklinik Umum dan Poliklinik Eksekutif</strong></h2><h2 style="margin-top: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">5. Contack Center 1500034</strong></h2>', 'Aksebilitas Rumah Sakit', '2018-05-12 10:21:02', 1, NULL, NULL, 'aksebilitas-rs', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (11, 9, 'Aksebilitas RS', '<h2 style="margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">1. Website : &nbsp;</span><a href="http://www.pjnhk.go.id/" style="margin: 0px; padding: 0px; font-style: inherit; text-align: inherit; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; outline: 0px; vertical-align: baseline; transition: all 0.15s ease; color: rgb(45, 132, 182); text-decoration: underline;"><span style="font-size: 12px;">http://www.pjnhk.go.id</span></a></em></strong></h2><h2 style="margin-top: 0px; margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">2. Perjanjian Online via Android : (download di &nbsp;</span></strong><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: arial, sans-serif; outline: 0px; vertical-align: baseline; white-space: nowrap; color: rgb(0, 102, 33); line-height: 16px;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; white-space: normal;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><a href="https://play.google.com/store/apps" style="margin: 0px; padding: 0px; font-style: inherit; text-align: inherit; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; outline: 0px; vertical-align: baseline; transition: all 0.15s ease; color: rgb(45, 132, 182); text-decoration: underline;"><span style="font-size: 12px;">https://play.google.com/store/apps</span></a></em></strong><span style="font-size: 12px;">&nbsp;atau&nbsp;</span></span><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 1.3em;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">Google Playstore) : PJNHK</span></em></strong></h2><h2 style="margin-top: 0px; margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">3. SPGDT - IGD 24 Jam</span></strong></h2><h2 style="margin-top: 0px; margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">4. Layanan Poliklinik Umum dan Poliklinik Eksekutif</span></strong></h2><h2 style="margin-top: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">5. Contack Center 1500034</span></strong></h2>', 'Aksebilitas Rumah Sakit', '2018-05-12 10:21:02', 1, '2018-05-12 10:22:43', 1, 'aksebilitas-rs', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (12, 1, 'Struktur Organisasi', '<p>Struktur Organisasi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita<br></p>', 'Struktur Organisasi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita', '2018-05-12 09:25:04', 1, '2018-05-25 04:05:40', 1, 'struktur-organisasi', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (13, 2, 'Jajaran Direksi', '<p><span style="color: rgba(0, 0, 0, 0.87); font-size: 12px;">Jajaran Direksi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita</span><br></p>', 'Jajaran Direksi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita', '2018-05-12 09:46:29', 1, '2018-05-28 04:25:35', 1, 'jajaran-direksi', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (14, 5, 'Makna dan Ketentuan Logo', '<h4 style="margin-bottom: 0px; font-size: 14px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="font-family: inherit; font-size: 23px; font-style: inherit; background: transparent; text-align: inherit; margin: 0px; padding: 0px; font-weight: bold; border: 0px; outline: 0px; vertical-align: baseline;">Pusat Jantung Nasional Harapan Kita</strong><br></h4>', 'Makna dan Ketentuan Logo', '2018-05-12 09:56:21', 1, '2018-05-28 04:25:49', 1, 'makna-dan-ketentuan-logo', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (15, 8, 'Lokasi', '<p>Lokasi Rumah Sakit Pusat Jantung Nasional Harapan Kita</p>', 'Lokasi Rumah Sakit', '2018-05-12 10:16:36', 1, NULL, NULL, 'lokasi-', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (16, 5, 'Makna dan Ketentuan Logo', '<h4 style="margin-bottom: 0px; font-size: 14px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="font-family: inherit; font-size: 23px; font-style: inherit; background: transparent; text-align: inherit; margin: 0px; padding: 0px; font-weight: bold; border: 0px; outline: 0px; vertical-align: baseline;">Pusat Jantung Nasional Harapan Kita</strong><br></h4>', 'Makna dan Ketentuan Logo', '2018-05-12 09:56:21', 1, '2018-05-28 04:25:49', 1, 'makna-dan-ketentuan-logo', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (17, 7, 'Visi Misi', '<h2>VISI</h2><p>visi kami adalah ...<br></p><h1>MISI</h1><p>misi kami adalah ...<br></p><h1>Nilai Budaya</h1><p>nilai budaya kami adalah ...<br></p>', 'Visi & Misi Rumah Sakit Jantung dan Hati Harapan Kita', '2018-05-12 10:11:16', 1, '2018-05-30 06:50:41', 1, 'visi-misi', NULL);
INSERT INTO public.log_trans_profile (id, id_trans_profile, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (18, 7, 'Visi Misi', '<h2>VISI</h2><p>visi kami adalah ...<br></p><h1>MISI</h1><p>misi kami adalah ...<br></p><h1>Nilai Budaya</h1><p>nilai budaya kami adalah ...<br></p>', 'Visi & Misi Rumah Sakit Jantung dan Hati Harapan Kita', '2018-05-12 10:11:16', 1, '2018-05-30 06:50:41', 1, 'visi-misi', NULL);


--
-- Name: log_trans_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_profile_id_seq', 18, true);


--
-- Data for Name: log_trans_rujukan_agenda; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_rujukan_agenda (id, id_trans_agenda, id_rujukan_nasional, nama, keterangan, tahun, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 3, 'Test', 'Test', '2018', '2018-05-25 04:55:31', 1, NULL, NULL);


--
-- Name: log_trans_rujukan_agenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_rujukan_agenda_id_seq', 1, true);


--
-- Data for Name: log_trans_rujukan_nasional; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_rujukan_nasional (id, id_trans_rujukan_nasional, judul, pengantar, profile, cara_merujuk, penelitian, jejaring, prosedur, alamat, no_tlpn, email, deskripsi, photo, status, created_at, created_by, updated_at, updated_by, akses_deskripsi, kordinat, fasilitas_nama, fasilitas_pelayanan) VALUES (1, 1, 'Kardiovaskuler', '<p>Test<br></p>', '<p>test<br></p>', '<p>test<br></p>', '<p>test<br></p>', '<p>tset<br></p>', NULL, 'test', '0123', 'test@test.com', 'test', 'uploads/rujukan-nasional/wHawhOI3nMcIePCEjhJMvK99KCQPnFkIhc4CnXLO.jpeg', 1, '2018-05-24 08:59:04', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.log_trans_rujukan_nasional (id, id_trans_rujukan_nasional, judul, pengantar, profile, cara_merujuk, penelitian, jejaring, prosedur, alamat, no_tlpn, email, deskripsi, photo, status, created_at, created_by, updated_at, updated_by, akses_deskripsi, kordinat, fasilitas_nama, fasilitas_pelayanan) VALUES (2, 2, 'Kardiovaskuler', '<p>Test<br></p>', '<p>test<br></p>', '<p>test<br></p>', '<p>test<br></p>', '<p>tset<br></p>', NULL, 'test', '0123', 'test@test.com', 'test', 'uploads/rujukan-nasional/qzZefqB1howt59cl67y12QDKq6emgnZvFdgGwOZ7.jpeg', 1, '2018-05-24 08:59:28', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.log_trans_rujukan_nasional (id, id_trans_rujukan_nasional, judul, pengantar, profile, cara_merujuk, penelitian, jejaring, prosedur, alamat, no_tlpn, email, deskripsi, photo, status, created_at, created_by, updated_at, updated_by, akses_deskripsi, kordinat, fasilitas_nama, fasilitas_pelayanan) VALUES (3, 1, 'Kardiovaskuler', '<p>Test<br></p>', '<p>test<br></p>', '<p>test<br></p>', '<p>test<br></p>', '<p>tset<br></p>', NULL, 'test', '0123', 'test@test.com', 'test', 'uploads/rujukan-nasional/wHawhOI3nMcIePCEjhJMvK99KCQPnFkIhc4CnXLO.jpeg', 1, '2018-05-24 08:59:04', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.log_trans_rujukan_nasional (id, id_trans_rujukan_nasional, judul, pengantar, profile, cara_merujuk, penelitian, jejaring, prosedur, alamat, no_tlpn, email, deskripsi, photo, status, created_at, created_by, updated_at, updated_by, akses_deskripsi, kordinat, fasilitas_nama, fasilitas_pelayanan) VALUES (4, 3, 'Pusat Jantung Terpadu', '<p>Test<br></p>', '<p>Test<br></p>', '<p>Test<br></p>', '<p>Test<br></p>', '<p>Test<br></p>', NULL, 'Test', '123', 'test@test.test', 'Test', 'uploads/rujukan-nasional/EAumXoM645tsFlt7atYXh7pJyiUnhMvDMyHph0ok.jpeg', 1, '2018-05-25 04:55:31', 1, NULL, NULL, NULL, '1212', 'Test', '<p>Test<br></p>');


--
-- Name: log_trans_rujukan_nasional_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_rujukan_nasional_id_seq', 4, true);


--
-- Data for Name: log_trans_rujukan_produk; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_rujukan_produk (id, id_trans_produk, id_rujukan_nasional, nama, keterangan, gambar, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 3, 'test', 'Test', 'uploads/rujukan-nasional-produk/8a9Fks5zB9pOcZtPbkbs2WXZe7K2oLTGBiDaTJhg.jpeg', '2018-05-25 04:55:31', 1, NULL, NULL);


--
-- Name: log_trans_rujukan_produk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_rujukan_produk_id_seq', 1, true);


--
-- Data for Name: log_trans_rujukan_tim; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.log_trans_rujukan_tim (id, id_trans_tim, id_rujukan_nasional, id_dokter, posisi, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 3, 1, '1', '2018-05-25 04:55:31', 1, NULL, NULL);


--
-- Name: log_trans_rujukan_tim_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_rujukan_tim_id_seq', 1, true);


--
-- Data for Name: log_trans_testimoni; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: log_trans_testimoni_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_trans_testimoni_id_seq', 1, false);


--
-- Data for Name: oauth_access_tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: oauth_auth_codes; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: oauth_clients; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: oauth_clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.oauth_clients_id_seq', 1, false);


--
-- Data for Name: oauth_personal_access_clients; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.oauth_personal_access_clients_id_seq', 1, false);


--
-- Data for Name: oauth_refresh_tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: ref_jabatan_dokter; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ref_jabatan_dokter (id, jabatan, deskripsi, created_at, created_by, updated_at, updated_by) VALUES (1, 'Dokter Spesialis', 'Dokter Spesialis', '2018-05-15 04:08:51', 1, '2018-05-15 04:20:15', 1);
INSERT INTO public.ref_jabatan_dokter (id, jabatan, deskripsi, created_at, created_by, updated_at, updated_by) VALUES (2, 'Dokter Umum', 'Dokter Umum', '2018-05-15 04:20:27', 1, NULL, NULL);


--
-- Name: ref_jabatan_dokter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_jabatan_dokter_id_seq', 2, true);


--
-- Data for Name: ref_jenis_mutu; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: ref_jenis_mutu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_jenis_mutu_id_seq', 1, false);


--
-- Data for Name: ref_kategori_artikel; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ref_kategori_artikel (id, nama, deskripsi, created_at, created_by, updated_at, updated_by) VALUES (1, 'Berita', 'Berita', '2018-05-22 05:58:48', 1, NULL, NULL);


--
-- Name: ref_kategori_artikel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_kategori_artikel_id_seq', 1, true);


--
-- Data for Name: ref_kuesioner_periode; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ref_kuesioner_periode (id, periode, status, created_at, created_by, updated_at, updated_by) VALUES (1, 'Semester 1 - 2018', 0, '2018-05-29 22:50:27', 1, '2018-05-31 04:25:00', 1);
INSERT INTO public.ref_kuesioner_periode (id, periode, status, created_at, created_by, updated_at, updated_by) VALUES (2, 'Semester 2 - 2018', 1, '2018-05-31 04:49:41', 1, '2018-05-31 05:06:06', 1);


--
-- Data for Name: ref_kuesioner_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (6, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, true);
INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (25, 2, 'Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita sudah sesuai dengan harapan anda?', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, false);
INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (26, 2, 'Anda puas dengan pelayanan petugas kami ?', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, false);
INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (27, 2, 'Informasi yang ada di website ini mudah anda pahami ?', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, false);
INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (28, 2, 'Anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, false);
INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (29, 2, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, true);
INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (30, 2, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda kecewa? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-31 04:49:41', 1, '2018-05-31 05:46:48', 1, true);
INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (1, 1, 'Apakah Pelayanan di Rumah Sakit Jantung dan Pembuluh Darah Harapan KIta sudah sesuai dengan harapan anda?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, false);
INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (2, 1, 'Apakah anda puas dengan pelayanan petugas kami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, false);
INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (3, 1, 'Apakah Informasi yang ada di website ini mudah anda pahami ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, false);
INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (4, 1, 'Apakah anda puas dengan informasi yang kami berikan pada website ini ?', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, false);
INSERT INTO public.ref_kuesioner_detail (id, kuesioner_id, pertanyaan, created_at, created_by, updated_at, updated_by, tipe) VALUES (5, 1, 'Saat anda di RSJPDHK, hal-hal apa saja yang membuat anda terkesan? (terkait pelayanan, petugas, keuangan, fasilitas dan lainnya)', '2018-05-29 22:50:27', 1, '2018-05-31 04:43:42', 1, true);


--
-- Name: ref_kuesioner_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_kuesioner_detail_id_seq', 30, true);


--
-- Name: ref_kuesioner_periode_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_kuesioner_periode_id_seq', 2, true);


--
-- Data for Name: ref_link_footer; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (1, 'Kemenkes', 'http://www.kemkes.go.id/', '2018-05-21 03:54:43', 1, NULL, NULL);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (3, 'Dinkes DKI', 'http://dinkes.jakarta.go.id/', '2018-05-21 05:48:21', 1, NULL, NULL);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (4, 'SIRS Online', 'http://telemedicine.pjnhk.go.id/daftar/', '2018-05-21 05:49:08', 1, NULL, NULL);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (5, 'AGD Dinkes DKI', 'https://agddinkes.jakarta.go.id/', '2018-05-21 05:49:46', 1, NULL, NULL);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (6, 'Siranap', 'http://yankes.kemkes.go.id/app/siranap/pages/rsvertikal', '2018-05-21 05:50:22', 1, NULL, NULL);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (7, 'LPSE Kemenkes', 'http://www.lpse.depkes.go.id/eproc4', '2018-05-21 05:50:36', 1, NULL, NULL);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (8, 'SISRUTE', 'https://sisrute.kemkes.go.id/', '2018-05-21 05:51:03', 1, NULL, NULL);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (9, 'SIRUP', 'https://sirup.lkpp.go.id/sirup', '2018-05-21 05:51:28', 1, NULL, NULL);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (10, 'BPJS', 'https://bpjs-kesehatan.go.id/bpjs/', '2018-05-21 05:51:53', 1, NULL, NULL);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (11, 'FK UI', 'http://fk.ui.ac.id/', '2018-05-21 05:52:22', 1, NULL, NULL);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (13, 'PERSI', 'https://www.persi.or.id/', '2018-05-21 05:53:45', 1, NULL, NULL);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (12, 'PERKI', 'http://www.inaheart.or.id/', '2018-05-21 05:53:04', 1, '2018-05-21 05:53:55', 1);
INSERT INTO public.ref_link_footer (id, nama, link, created_at, created_by, updated_at, updated_by) VALUES (14, 'BIOS PPKBLU', 'http://bios.djpbn.kemenkeu.go.id/', '2018-05-21 05:54:14', 1, NULL, NULL);


--
-- Name: ref_link_footer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_link_footer_id_seq', 14, true);


--
-- Data for Name: ref_poli; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: ref_poli_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_poli_id_seq', 1, false);


--
-- Data for Name: ref_rumah_sakit; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: ref_rumah_sakit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_rumah_sakit_id_seq', 1, false);


--
-- Data for Name: ref_sertifikat_footer; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ref_sertifikat_footer (id, nama, gambar, keterangan, created_at, created_by, updated_at, updated_by) VALUES (1, 'Joint Commission International', 'uploads/master/sertifikat-footer/LUAwJxWC29mNNh4hS9tvFeRwRGfMZKwxnAh9DCcO.png', 'Quality Approval from JCI', '2018-05-21 03:07:45', 1, NULL, NULL);
INSERT INTO public.ref_sertifikat_footer (id, nama, gambar, keterangan, created_at, created_by, updated_at, updated_by) VALUES (2, 'KARS', 'uploads/dokter/list-dokter/6K3uF4UNAO9U00kpvGBwlyzQpzVnUPM8Ca2WJFHi.png', 'KARS', '2018-05-21 03:08:02', 1, '2018-05-24 07:08:33', 1);


--
-- Name: ref_sertifikat_footer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_sertifikat_footer_id_seq', 4, true);


--
-- Data for Name: ref_slider; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ref_slider (id, judul, sub_judul, deskripsi, url, photo, posisi, status, created_at, created_by, updated_at, updated_by) VALUES (2, 'Event Lebaran', 'Lebaran', 'Selamat Hari Lebaran', 'http://pjnhk.me', 'uploads/master/slider/kOPrkY69sZJspL71HFu30c2Q8M9VNLHhy4yHEdF2.jpeg', 0, 1, '2018-05-22 07:17:58', 1, NULL, NULL);
INSERT INTO public.ref_slider (id, judul, sub_judul, deskripsi, url, photo, posisi, status, created_at, created_by, updated_at, updated_by) VALUES (1, 'Akses Mudah Menuju Sehat', 'Android Apps PJNHK', NULL, 'http://pjnhk.go.id', 'uploads/master/slider/NfcRhJ86pJeG3mDKgK9Xp1g1Sws5iYW5K6Rh7Ooo.jpeg', 0, 1, '2018-05-21 09:22:12', 1, '2018-05-24 07:26:34', 1);


--
-- Name: ref_slider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_slider_id_seq', 2, true);


--
-- Data for Name: ref_spesialisasi; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ref_spesialisasi (id, nama, deskripsi, created_at, created_by, updated_at, updated_by) VALUES (1, 'Cardiologi', 'Cardiologi', '2018-05-15 04:06:59', 1, NULL, NULL);
INSERT INTO public.ref_spesialisasi (id, nama, deskripsi, created_at, created_by, updated_at, updated_by) VALUES (2, 'Bedah Dewasa', 'Bedah Khusus Dewasa', '2018-05-15 04:19:38', 1, NULL, NULL);


--
-- Name: ref_spesialisasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_spesialisasi_id_seq', 2, true);


--
-- Data for Name: ref_sub_spesialisasi; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ref_sub_spesialisasi (id, nama, spesialisasi_induk, keterangan, created_at, created_by, updated_at, updated_by) VALUES (1, 'Cardiologist', 1, 'Pembedahan Jantung', '2018-05-28 04:28:38', 1, NULL, NULL);


--
-- Name: ref_sub_spesialisasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_sub_spesialisasi_id_seq', 1, true);


--
-- Data for Name: ref_tautan_footer; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: ref_tautan_footer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_tautan_footer_id_seq', 1, false);


--
-- Data for Name: ref_tipe_media; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: ref_tipe_media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_tipe_media_id_seq', 1, false);


--
-- Data for Name: ref_tipe_pendaftar; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: ref_tipe_pendaftar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ref_tipe_pendaftar_id_seq', 1, false);


--
-- Data for Name: sys_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sys_migrations (id, migration, batch) VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (3, '2016_06_01_000001_create_oauth_auth_codes_table', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (4, '2016_06_01_000002_create_oauth_access_tokens_table', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (6, '2016_06_01_000004_create_oauth_clients_table', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (8, '2018_05_03_041717_entrust_setup_tables', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (9, '2018_05_03_052630_create_ref_sejarah_tables', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (10, '2018_05_03_065604_create_table_ref_slider', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (11, '2018_05_03_071102_create_table_ref_tautan_footer', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (12, '2018_05_03_084510_create_table_ref_rumahsakit', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (13, '2018_05_03_095752_create_table_ref_tipe_pendaftar', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (14, '2018_05_03_100607_create_table_ref_tipe_media', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (15, '2018_05_03_101622_create_table_ref_visi_misi', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (16, '2018_05_03_103324_create_table_ref_spesialisasi', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (17, '2018_05_03_103751_create_table_ref_poli', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (18, '2018_05_03_111719_create_table_ref_jabatan_dokter', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (19, '2018_05_04_024615_create_table_ref_penelitian', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (20, '2018_05_04_025350_create_table_ref_sambutan', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (21, '2018_05_04_025703_create_ref_dewan_direksi', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (22, '2018_05_04_031046_create_table_ref_dewan_pengawas', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (23, '2018_05_04_041653_create_ref_struktrur_organisasi', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (24, '2018_05_04_050803_create_table_ref_profesi_keahlian', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (25, '2018_05_04_062129_create_ref_denah_lokasi', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (26, '2018_05_04_074013_create_table_ref_kerja_sama', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (27, '2018_05_04_075026_create_trans_list_dokter', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (28, '2018_05_04_093743_create_table_trans_jadwal_dokter', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (29, '2018_05_05_042535_create_trans_list_artikel', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (30, '2018_05_05_052327_create_trans_kegiatan', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (31, '2018_05_05_053630_create_table_trans_pengumuman', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (32, '2018_05_05_055157_add_slug_change_keterangan_lampiran_on_trans_list_artikel', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (33, '2018_05_05_080426_create_trans_alur_pendaftaran', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (34, '2018_05_05_083035_create_table_trans_karir', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (35, '2018_05_05_085233_create_trans_aturan_pengunjung', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (36, '2018_05_05_094224_create_trans_hasil_seleksis', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (37, '2018_05_07_042039_create_trans_info_whistle', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (38, '2018_05_07_054328_create_trans_item_whistle', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (39, '2018_05_07_064440_create_table_trans_rujuan_nasional', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (40, '2018_05_07_065304_create_trans_layanan_unggulan', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (41, '2018_05_07_065630_create_trans_layanan_umum', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (42, '2018_05_09_031257_change_keterangan_to_null_on_karir', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (43, '2018_05_09_042353_add_jabatan_on_list_dokter', 1);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (44, '2018_05_12_040407_drop_all_profile', 2);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (45, '2018_05_12_041227_add_attribut_trans_list_dokter', 2);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (46, '2018_05_12_042016_add_attribut_trans_list_dokter_new', 2);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (47, '2018_05_12_042411_create_trans_profile', 2);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (48, '2018_05_12_053056_add_attribut_trans_profile', 2);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (49, '2018_05_12_055806_add_trans_profile_slug', 2);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (50, '2018_05_12_062753_add_choice_layanan_trans_layanan_unggulan', 2);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (51, '2018_05_12_072509_costum_modify_table_layanan_trans_layanan_unggulan', 2);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (52, '2018_05_12_082838_create_trans_info_pasien', 2);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (53, '2018_05_11_055325_drop_spesialisasi_on_jabatan_dokter', 3);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (54, '2018_05_12_101529_edit_trans_info_pasien_and_profil', 3);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (55, '2018_05_12_102356_edit_trans_info_pasien_and_profil_past', 3);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (56, '2018_05_14_025725_create_trans_jadwal_diklat', 3);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (57, '2018_05_14_032825_add_files_upload_trans_layanan', 3);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (58, '2018_05_14_055717_create_trans_jadwal_diklat_tambah_kolom_jam', 3);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (59, '2018_05_14_062856_create_trans_testimoni', 3);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (60, '2018_05_14_063943_create_trans_rujukan_nasional_for_produk_rujukan', 4);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (61, '2018_05_14_064623_create_trans_rujukan_nasional_for_tim_rujukan', 4);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (62, '2018_05_14_065443_create_trans_rujukan_nasional_for_agenda_rujukan', 4);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (63, '2018_05_15_035743_create_table_dokter_bahasa_pendidikan_keahlian', 4);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (64, '2018_05_15_041353_edit_trans_rujukan_nasional', 4);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (65, '2018_05_15_042334_edit_trans_rujukan_nasional_attribut', 4);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (66, '2018_05_15_043502_edit_trans_rujukan_nasional_attribut_tlp', 5);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (67, '2018_05_15_044700_create_new_trans_rujukan_nasional', 5);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (68, '2018_05_16_042952_create_trans_item_pengaduan', 6);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (69, '2018_05_16_043008_create_trans_info_pengaduan', 6);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (70, '2018_05_16_060339_create_ref_sertifikat_footer', 6);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (71, '2018_05_16_060359_create_ref_link_footer', 6);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (72, '2018_05_17_074247_create_trans_info_litbang', 7);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (73, '2018_05_17_085952_create_trans_list_publikasi', 7);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (74, '2018_05_17_091741_create_trans_komite_etik', 7);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (75, '2018_05_17_101047_create_ref_info_bed', 8);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (76, '2018_05_18_023256_create_ref_jenis_mutu', 8);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (77, '2018_05_18_032231_create_trans_info_mutu', 8);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (78, '2018_05_18_063358_create_trans_info_bed', 8);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (79, '2018_05_18_065039_create_ref_sub_spesialisasi', 8);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (80, '2018_05_18_073138_drop_bahasa_dokter_list', 8);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (81, '2018_05_18_073259_create_ref_kategori_artikel', 8);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (82, '2018_05_18_073753_add_attr_artikel', 8);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (83, '2018_05_18_085604_drop_trans_rujukan_tim_attributs', 8);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (84, '2018_05_18_090050_add_attr_trans_rujukan_tim_attributs', 8);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (85, '2018_05_18_090706_creat_log_trans_atrtikel', 8);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (86, '2018_05_21_080502_create_trans_kuesioner', 9);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (119, '2018_05_24_044310_create_ref_kuesioner_periode', 10);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (120, '2018_05_24_062900_create_ref_kuesioner_detail', 10);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (121, '2018_05_24_071206_create_trans_kuesioner_jawaban', 10);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (122, '2018_05_24_033627_drop_three_table_rujukan', 11);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (123, '2018_05_24_034032_add_three_table_rujukan', 11);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (124, '2018_05_24_085439_create_trans_kuesioner_jawaban_detail', 11);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (125, '2018_05_28_061547_update_kuisioner_add_essay_feature', 12);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (126, '2018_05_28_071152_change_nullable_kuesioner', 13);
INSERT INTO public.sys_migrations (id, migration, batch) VALUES (127, '2018_05_31_040445_create_trans_info_diklat', 14);


--
-- Name: sys_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sys_migrations_id_seq', 127, true);


--
-- Data for Name: sys_password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sys_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sys_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sys_permission_role; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: sys_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sys_permissions_id_seq', 1, false);


--
-- Data for Name: sys_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sys_users (id, name, email, password, remember_token, created_at, updated_at) VALUES (1, 'Admin', 'admin@pragmainf.co.id', '$2y$10$nMWy2uoGNFWRSvQu/iIWZ.9wj2knl0aUp6LrT8SNulw8yz2B4dnT.', 'jF4GslYKuXHv4TMJmjNKn48H5muafb5jySimo8vv23tRWUDzImW476QxJ7Bj', '2018-05-11 12:14:58', NULL);


--
-- Data for Name: sys_role_user; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: sys_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sys_roles_id_seq', 1, false);


--
-- Name: sys_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sys_users_id_seq', 1, true);


--
-- Data for Name: trans_alur_pendaftaran; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_alur_pendaftaran_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_alur_pendaftaran_id_seq', 1, false);


--
-- Data for Name: trans_aturan_pengunjung; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_aturan_pengunjung_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_aturan_pengunjung_id_seq', 1, false);


--
-- Data for Name: trans_list_dokter; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_list_dokter (id, nama, spesialisasi_id, alamat, no_tlpn, deskripsi, photo, created_at, created_by, updated_at, updated_by, jabatan_dokter_id) VALUES (4, 'dr. ANNA ULFA RAHAYOE , Sp.JP(K)', 1, 'Jakarta', '08123456789', NULL, 'uploads/dokter/list-dokter/8YPlvDrGNqhLC8OkMPTCgmIUEOXZPNXeMTAIVJpP.jpeg', '2018-05-15 04:21:56', 1, NULL, NULL, 1);
INSERT INTO public.trans_list_dokter (id, nama, spesialisasi_id, alamat, no_tlpn, deskripsi, photo, created_at, created_by, updated_at, updated_by, jabatan_dokter_id) VALUES (3, 'dr. AMIN TJUBANDI , Sp.BTKV', 2, 'Jakarta', '08123456789', NULL, 'uploads/dokter/list-dokter/He3XkxsOdYbUJL27Q8ymzmNQAnrVP3XANVTUpmlf.jpeg', '2018-05-15 04:18:41', 1, NULL, NULL, 1);
INSERT INTO public.trans_list_dokter (id, nama, spesialisasi_id, alamat, no_tlpn, deskripsi, photo, created_at, created_by, updated_at, updated_by, jabatan_dokter_id) VALUES (2, 'DR.dr. AMILIANA MARDIANI , Sp.JP(K)', 1, 'Jakarta', '08123456789', NULL, 'uploads/dokter/list-dokter/HZLCI6ZGw5XwoCi5DisSYcgsATdRH5iArzLpsyO9.jpeg', '2018-05-15 04:17:11', 1, '2018-05-15 05:06:40', 1, 1);
INSERT INTO public.trans_list_dokter (id, nama, spesialisasi_id, alamat, no_tlpn, deskripsi, photo, created_at, created_by, updated_at, updated_by, jabatan_dokter_id) VALUES (1, 'dr ADE MEIDIAN AMBARI , SpJp', 1, 'Jakarta', '08123456789', NULL, 'uploads/dokter/list-dokter/wqY0fiA54874Zv4avLutNqaiOmEqNARpmOpMaaMa.jpeg', '2018-05-15 04:12:47', 1, '2018-05-31 09:42:02', 1, 1);


--
-- Data for Name: trans_dokter_keahlian; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_dokter_keahlian (id, list_dokter_id, nama, created_at, created_by, updated_at, updated_by) VALUES (1, 2, 'Kardiologi', '2018-05-15 05:06:40', 1, NULL, NULL);
INSERT INTO public.trans_dokter_keahlian (id, list_dokter_id, nama, created_at, created_by, updated_at, updated_by) VALUES (2, 2, 'Radiologi', '2018-05-15 05:06:40', 1, NULL, NULL);
INSERT INTO public.trans_dokter_keahlian (id, list_dokter_id, nama, created_at, created_by, updated_at, updated_by) VALUES (3, 1, 'Kardiografi', '2018-05-31 09:42:02', 1, NULL, NULL);
INSERT INTO public.trans_dokter_keahlian (id, list_dokter_id, nama, created_at, created_by, updated_at, updated_by) VALUES (4, 1, 'Metode Selang', '2018-05-31 09:42:02', 1, NULL, NULL);


--
-- Name: trans_dokter_keahlian_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_dokter_keahlian_id_seq', 4, true);


--
-- Data for Name: trans_dokter_pendidikan; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_dokter_pendidikan (id, list_dokter_id, nama, created_at, created_by, updated_at, updated_by) VALUES (1, 2, 'Universitas Indonesia', '2018-05-15 05:06:40', 1, NULL, NULL);
INSERT INTO public.trans_dokter_pendidikan (id, list_dokter_id, nama, created_at, created_by, updated_at, updated_by) VALUES (2, 1, 'Md', '2018-05-31 09:42:02', 1, NULL, NULL);


--
-- Name: trans_dokter_pendidikan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_dokter_pendidikan_id_seq', 2, true);


--
-- Data for Name: trans_karir; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: trans_hasil_seleksi; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_hasil_seleksi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_hasil_seleksi_id_seq', 1, false);


--
-- Data for Name: trans_info_bed; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_info_bed_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_info_bed_id_seq', 1, false);


--
-- Data for Name: trans_info_diklat; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_info_diklat (id, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (1, 'Info Diklat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc id libero sagittis, sagittis risus eget, accumsan magna. Pellentesque nec risus nec purus consectetur eleifend. In vel ante tellus. Fusce suscipit malesuada dui, sollicitudin gravida purus facilisis quis. Donec id quam neque. Nullam finibus nisi mi, sed molestie orci faucibus ut. In hac habitasse platea dictumst. Vestibulum sollicitudin arcu eu felis mattis tempor sed tristique dui. Nulla id nunc eget augue venenatis dictum. Proin tristique leo eu sem pretium, lobortis tristique nulla volutpat.

Aliquam nec lorem efficitur, blandit tellus non, mattis dui. Vestibulum feugiat lacus eu dolor auctor, at varius mi dapibus. Pellentesque viverra quis mi pretium lobortis. Pellentesque tristique nulla quis eros gravida ultricies. Praesent blandit elementum eros commodo scelerisque. Vivamus placerat, dui ac molestie tempor, neque dui posuere nisl, vitae luctus ante neque ac nibh. Vivamus dignissim massa leo, in varius neque fermentum quis. Mauris hendrerit tempor justo, eu pellentesque ligula. Duis tincidunt interdum nibh, in dictum ipsum fermentum sit amet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas finibus dictum placerat. In imperdiet sem vel eleifend ultrices. Vestibulum gravida ut metus et condimentum. Pellentesque mollis dolor id arcu dapibus rhoncus. Morbi blandit quis est a aliquet.

Nunc tempus cursus dui eu sodales. Cras convallis elit at pretium mattis. Fusce malesuada lectus malesuada odio vulputate, sed vulputate erat mattis. Aenean consequat rutrum fermentum. Proin finibus congue sodales. Vivamus hendrerit fringilla luctus. Maecenas at risus ac tellus rhoncus dignissim. Quisque vulputate, diam sit amet sagittis tempor, est dolor sollicitudin elit, quis tempus mi mi ut dui. Quisque at urna vitae nibh malesuada auctor. Nam mollis viverra efficitur. Donec ipsum urna, egestas ut pharetra hendrerit, blandit nec nibh. Phasellus tristique consectetur turpis non fringilla. Sed rutrum lacinia velit, sit amet gravida risus rutrum ut. Praesent vehicula arcu vulputate tortor dignissim, at condimentum turpis facilisis.', '2018-05-31 09:50:36', 1, NULL, NULL);


--
-- Name: trans_info_diklat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_info_diklat_id_seq', 1, true);


--
-- Data for Name: trans_info_litbang; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_info_litbang (id, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (1, 'Info Litbang', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc id libero sagittis, sagittis risus eget, accumsan magna. Pellentesque nec risus nec purus consectetur eleifend. In vel ante tellus. Fusce suscipit malesuada dui, sollicitudin gravida purus facilisis quis. Donec id quam neque. Nullam finibus nisi mi, sed molestie orci faucibus ut. In hac habitasse platea dictumst. Vestibulum sollicitudin arcu eu felis mattis tempor sed tristique dui. Nulla id nunc eget augue venenatis dictum. Proin tristique leo eu sem pretium, lobortis tristique nulla volutpat.

Aliquam nec lorem efficitur, blandit tellus non, mattis dui. Vestibulum feugiat lacus eu dolor auctor, at varius mi dapibus. Pellentesque viverra quis mi pretium lobortis. Pellentesque tristique nulla quis eros gravida ultricies. Praesent blandit elementum eros commodo scelerisque. Vivamus placerat, dui ac molestie tempor, neque dui posuere nisl, vitae luctus ante neque ac nibh. Vivamus dignissim massa leo, in varius neque fermentum quis. Mauris hendrerit tempor justo, eu pellentesque ligula. Duis tincidunt interdum nibh, in dictum ipsum fermentum sit amet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas finibus dictum placerat. In imperdiet sem vel eleifend ultrices. Vestibulum gravida ut metus et condimentum. Pellentesque mollis dolor id arcu dapibus rhoncus. Morbi blandit quis est a aliquet.', '2018-05-31 09:53:25', 1, NULL, NULL);


--
-- Name: trans_info_litbang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_info_litbang_id_seq', 1, true);


--
-- Data for Name: trans_info_mutu; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_info_mutu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_info_mutu_id_seq', 1, false);


--
-- Data for Name: trans_info_pasien; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_info_pasien_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_info_pasien_id_seq', 1, false);


--
-- Data for Name: trans_info_pengaduan; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_info_pengaduan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_info_pengaduan_id_seq', 1, false);


--
-- Data for Name: trans_info_whistle; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_info_whistle (id, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (1, 'Whistleblowing Etika dan Hukum', 'Untuk pengaduan silahkan kirim email : 
         

etikhukum.wbs@pjnhk.go.id', '2018-05-12 13:37:41', 1, '2018-05-12 13:40:47', 1);
INSERT INTO public.trans_info_whistle (id, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (2, 'Whistleblowing Pelayanan Pelanggan', 'Untuk Pengaduan silahkan email :

 yangan.wbs@pjnhk.go.id', '2018-05-12 13:42:01', 1, '2018-05-12 13:42:11', 1);
INSERT INTO public.trans_info_whistle (id, judul, konten, created_at, created_by, updated_at, updated_by) VALUES (3, 'Whistleblowing SPI (Satuan Pemeriksaan Internal)', 'Untuk Pengaduan silahkan email : spi.wbs@pjnhk.go.id', '2018-05-12 13:43:18', 1, NULL, NULL);


--
-- Name: trans_info_whistle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_info_whistle_id_seq', 3, true);


--
-- Data for Name: trans_item_pengaduan; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_item_pengaduan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_item_pengaduan_id_seq', 1, false);


--
-- Data for Name: trans_item_whistle; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_item_whistle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_item_whistle_id_seq', 1, false);


--
-- Data for Name: trans_jadwal_diklat; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_jadwal_diklat (id, judul, konten, keterangan, gambar, tanggal, jam, created_at, created_by, updated_at, updated_by) VALUES (1, 'Diklat International', '<p>Diklat International<br></p>', 'Diklat International', 'uploads/diklat/KOvxjlh7jOngk27pE8GE6PfowVLMd6GGI5DCqh0v.jpeg', '2018-05-02', '13:00:00', '2018-05-31 09:51:48', 1, NULL, NULL);


--
-- Name: trans_jadwal_diklat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_jadwal_diklat_id_seq', 1, true);


--
-- Data for Name: trans_jadwal_dokter; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_jadwal_dokter (id, list_dokter_id, hari_praktek, jam_mulai_praktek, jam_selesai_praktek, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 1, '13:00', '16:00', '2018-05-15 04:15:45', 1, '2018-05-15 04:15:53', 1);


--
-- Name: trans_jadwal_dokter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_jadwal_dokter_id_seq', 1, true);


--
-- Name: trans_karir_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_karir_id_seq', 1, false);


--
-- Data for Name: trans_kegiatan; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_kegiatan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_kegiatan_id_seq', 1, false);


--
-- Data for Name: trans_komite_etik; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_komite_etik (id, nama, jabatan, departemen, tanggal_efektif_awal, tanggal_efektif_akhir, created_at, created_by, updated_at, updated_by) VALUES (1, 'Dokter Bambang', 1, 'Pas', '2018-05-03', '2018-05-09', '2018-05-31 09:54:17', 1, NULL, NULL);


--
-- Name: trans_komite_etik_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_komite_etik_id_seq', 1, true);


--
-- Data for Name: trans_kuesioner; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_kuesioner_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_kuesioner_id_seq', 1, false);


--
-- Data for Name: trans_kuesioner_jawaban; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_kuesioner_jawaban (id, nama, email, kuesioner_id, feedback, created_at, created_by, updated_at, updated_by) VALUES (1, 'Stef', 'stefanus.christian.c@gmail.com', 1, 'Test', '2018-05-29 23:04:18', 1, NULL, NULL);


--
-- Data for Name: trans_kuesioner_jawaban_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_kuesioner_jawaban_detail (id, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (1, 1, 1, '4', '2018-05-29 23:04:18', 1, NULL, NULL);
INSERT INTO public.trans_kuesioner_jawaban_detail (id, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (2, 1, 2, '4', '2018-05-29 23:04:18', 1, NULL, NULL);
INSERT INTO public.trans_kuesioner_jawaban_detail (id, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (3, 1, 3, '5', '2018-05-29 23:04:18', 1, NULL, NULL);
INSERT INTO public.trans_kuesioner_jawaban_detail (id, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (4, 1, 4, '5', '2018-05-29 23:04:18', 1, NULL, NULL);
INSERT INTO public.trans_kuesioner_jawaban_detail (id, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (5, 1, 5, 'Tet', '2018-05-29 23:04:18', 1, NULL, NULL);
INSERT INTO public.trans_kuesioner_jawaban_detail (id, kuesioner_jawaban_id, kuesioner_detail_id, jawaban, created_at, created_by, updated_at, updated_by) VALUES (6, 1, 6, 'tet', '2018-05-29 23:04:18', 1, NULL, NULL);


--
-- Name: trans_kuesioner_jawaban_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_kuesioner_jawaban_detail_id_seq', 33, true);


--
-- Name: trans_kuesioner_jawaban_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_kuesioner_jawaban_id_seq', 33, true);


--
-- Data for Name: trans_layanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_layanan (id, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (1, 'Jam Besuk', '<p>Jam Besuk Rumah Sakit Pusat Jantung Nasional Harapan Kita&nbsp;</p>', 'Jam Besuk Pasien', 'uploads/layanan/layanan/NAW5bzqfgiwEiKJ4y46OeEYC7N8rmlQu7dmmBEpg.jpeg', '2018-05-12 10:35:27', 1, NULL, NULL, 0, 'jam-besuk', NULL);
INSERT INTO public.trans_layanan (id, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (3, 'Penunjang Medis', '<h1 style="margin-bottom: 20px; font-size: 23px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">PENUNJANG MEDIS</em></h1>', 'Penunjang Medis', 'uploads/layanan/layanan/VoFgefcljwegytMFmQAsDFuNNf5YJ2cdrFkeLspv.png', '2018-05-12 11:01:03', 1, NULL, NULL, 0, 'penunjang-medis', NULL);
INSERT INTO public.trans_layanan (id, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (5, 'Rawat Gawat Darurat UGD', '<h1 style="margin-bottom: 20px; font-size: 23px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">SPGDT (Sistem Darurat Terpadu) dan Ambulance Service</strong></h1><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">SPGDT dan layanan ambulans menawarkan layanan-layanan berikut: 1. Umum / pelayanan pasien; termasuk transortation tanah dan evakuasi, Call Center Layanan Darurat Medis (proyek percontohan dari Pemerintah Daerah Khusus Ibukota Jakarta dan di tingkat nasional) sebagai tuan rumah dalam penanganan darurat jantung, tingkat Tele EKG nasional, dan layanan darurat jantung, dengan unggul pengobatan Sindrom Koroner akut dan manajemen. 2. tim dukungan kardiovaskular selama acara negara, dari provinsi, nasional ke tingkat internasional 3. tim dukungan kardiovaskular selama acara ormas / Lembaga Swasta tim 4. dukungan medis dalam bencana mengacu pada Rencana Discharge Bencana RSJPDHK itu.Siapkan program pelatihan dan pengembangan bagi dokter dan perawat dari rumah sakit umum dan Pusat Kesehatan Masyarakat di wilayah Jakarta (jaringan rumah sakit) yang berfokus pada pengobatan jantung darurat dan manajemen, terutama Sindrom Koroner Akut, dan melatih rekaman EKG dan penularan selama EKG tele nasional di bawah koordinasi Departemen Kesehatan.</p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">Berkontribusi terhadap penelitian berupa pengumpulan data dan analisis dari SKA Registry, data Call Center dan evakuasi dan ambulans jasa.Layanan yang ditawarkan:</p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">* layanan Pasien • pelayanan pasien 24 jam</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Intensif ambulans kardiovaskular (evakuasi dan transportasi)</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• konsultan darurat kardiovaskular</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Sistem Rujukan</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Call Center Layanan Darurat Medis (kamar kosong) Pilot Project (DKI)</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Tele EKG (tingkat nasional) / Nyata waktu</em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">• Koordinator dalam keadaan darurat kardiovaskular, terutama ACSTim medis kardiovaskular menyediakan supportduring peristiwa negara, mulai dari:&nbsp;</em></strong><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-size: 12.16px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 1.3em;">&nbsp; Tingkat Provinsi • Nasional • Internasional • Swasta / Yayasan Meningkatkan kompetensi layanan Gadar ACS seluruh &nbsp;Jakarta.</span></em></strong></p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;</em></strong></p><h1 style="margin-top: 0px; margin-bottom: 20px; font-size: 23px; font-style: italic; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Unit IGD / Emergency Departement</em></h1><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">Gawat darurat kardiovaskular (ED) terletak di bangunan utama, di lantai dasar dan dapat diakses langsung dari pintu masuk utama rumah sakit. Pada tahun 2013, ada 11.210 pasien dirawat di ED, yang 3186 pasien (28%) memiliki sindrom koroner akut (ACS). Akut elevasi ST-segmen infark miokard (STEMI) didiagnosis pada 896 pasien (28% dari populasi ACS). Pada 2013, terapi reperfusi akut dengan PCI primer dilakukan pada 330 pasien. Waktu rata-rata door-to-balon itu 90 menit. Sejak 2011, kami telah membangun Sistem Kardiovaskular Jakarta Perawatan Satuan Jaringan dan tujuan utama membangun jaringan adalah bagaimana kita dapat membuat diagnosis awal dari suatu infark, dengan demikian, diikuti dengan terapi reperfusi sebelumnya. Setelah implementasi jaringan lokal untuk pasien dengan STEMI, ada secara signifikan lebih antar-rumah sakit rujukan kasus, prosedur PCI lebih utama, dan lebih banyak pasien mencapai waktu door-to-needle ≤30 menit, dibandingkan dengan periode sebelum pelaksanaan jaringan ini. Untuk meningkatkan STEMI networking berdasarkan pedoman baru-baru ini, ada pra-rumah sakit dan di rumah sakit protokol harus ditingkatkan. Dalam pengaturan pra-rumah sakit, kami telah membuat bentuk triase pra-rumah sakit dan komunikasi ambulans bentuk grafik yang harus diisi oleh penyedia layanan kesehatan dalam pengaturan pra-rumah sakit. Juga, 12-lead EKG harus dicatat dalam semua dicurigai pasien dan harus dikirimkan ke pusat kami sebagai tuan rumah jaringan menggunakan telepon, email atau sistem faksimili. Di-rumah sakit protokol ditingkatkan dengan mengintegrasikan semua proses administrasi di ED, mengurangi waktu tunda untuk PCI primer</p>', 'Alur Pasien IGD', 'uploads/layanan/layanan/1xCQW7dUWSbGIxVHSuIzpmPaHo2f632Mg5uXWikX.jpeg', '2018-05-12 11:09:46', 1, NULL, NULL, 0, 'rawat-gawat-darurat-ugd', NULL);
INSERT INTO public.trans_layanan (id, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (9, 'DINB', '<p>Diagnostik Non Invasif & Intervensi Non Bedah, Pusat Aorta dan perifer dengan tindakan bedah tanpa bedah (TEVAR, EVAR, BENTALL, Prosedure, EVLT)<br></p>', 'Pusat Aorta dan perifer dengan tindakan tanpa bedah (TEVAR, EVAR, BENTALL, Prosedure, EVLT)', 'uploads/layanan/layanan/ETbNnmxZtlSzxIrwAAeBEgHwVryHK6ZEVituzTPe.png', '2018-05-30 06:25:13', 1, '2018-05-31 04:02:03', 1, 1, 'diagnostik-non-invasif--intervensi-non-bedah', NULL);
INSERT INTO public.trans_layanan (id, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (6, 'Arrytmia', '<p>Laboratorium Kateterisasi (Vaskuler) mampu melayani  kasus aritmia fibrilasi atrium dan takikardia supraventrikular.</p>', 'Melayani kasus aritmia fibrilasi atrium dan takikardia supraventrikular.', 'uploads/layanan/layanan/fuEEiTTTCLkFzEJHNSEUIXH0XumosUvzO5lWIvbL.png', '2018-05-21 04:27:01', 1, '2018-05-21 05:11:32', 1, 1, 'arrytmia', NULL);
INSERT INTO public.trans_layanan (id, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (7, 'Medical Check-Up', '<p>Deteksi Dini Kardiovaskuler<br></p>', 'Deteksi Dini Kardiovaskuler. Mencegah lebih baik dari pada mengobati.', 'uploads/layanan/layanan/EYPwfjgkAPboArX7WQfcafurTumtQbjaX9pf2PTF.png', '2018-05-21 04:31:27', 1, '2018-05-21 05:11:44', 1, 1, 'medical-check-up', NULL);
INSERT INTO public.trans_layanan (id, judul, konten, keterangan, photo, created_at, created_by, updated_at, updated_by, layanan_unggulan, slug, files) VALUES (8, 'Weekend Service', '<p>Melayani pada hari weekend<br></p>', 'Melayani pasien semaksimal mungkin, bahkan pada hari libur', 'uploads/layanan/layanan/rPnvI9oA3TLtv3XvshqJso2n7lY41tSLbtJcCuFM.png', '2018-05-21 05:05:53', 1, '2018-05-21 05:12:00', 1, 1, 'weekend-service', NULL);


--
-- Data for Name: trans_layanan_umum; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_layanan_umum_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_layanan_umum_id_seq', 1, false);


--
-- Name: trans_layanan_unggulan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_layanan_unggulan_id_seq', 9, true);


--
-- Data for Name: trans_list_artikel; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_list_artikel (id, judul, konten, created_at, created_by, updated_at, updated_by, slug, deskripsi, photo, kategori_id) VALUES (1, 'Penyelenggaraan Bimbingan SNARS', '<p style="margin: 0px 0px 6px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; line-height: 19.32px; text-align: justify;">Rabu
 (25/4) RS Jantung dan Pembuluh Darah Harapan Kita menyelenggarakan 
bimbingan Standar Nasional Akreditasi Rumah Sakit (SNARS) edisi 1 di 
Ruang Rapat Direksi Gedung Utama RSJPDHK, yang dibimbing langsung <em><strong>dr. Nico A. Lumenta K. Nefro, MM., MH. Kes</strong></em> dari <em>Komisi Akreditasi Rumah Sakit (KARS</em>).
 Bimbingan ini dilakukan sebagai salah satu bentuk persiapan rumah sakit
 dalam menghadapi Re-Akreditasi Nasional KARS pada Oktober - November 
mendatang.</p>
<p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; line-height: 19.32px; text-align: justify;">Hari ini dan esok merupakan bimbing<span class="text_exposed_show" style="display: inline; font-family: inherit;">an
 gelombang pertama, dimana hal yang menjadi fokus pembahasan adalah 
mengenai Standar Pelayanan terhadap Pasien. Sedangkan untuk bimbingan 
kedua, akan diselenggarakan pada tanggal 7 - 9 Mei mendatang, membahas 
tentang Standar Manajemen yang sesuai dengan SNARS.</span></p>
<div class="text_exposed_show" style="display: inline; font-family: Helvetica, Arial, sans-serif; color: #1d2129; font-size: 14px; line-height: 19.32px;">
<p style="margin: 0px 0px 6px; font-family: inherit; text-align: justify;">Kami
 berharap, melalui bimbingan ini rumah sakit dapat mengimplementasikan 
teori-teori yang didapat, memperbaiki kualitas pelayanan, sehingga 
berhasil lulus dalam Re-Akreditasi dan tentunya dapat memberikan 
pelayanan yang lebih lagi kepada pasien. <em>(Narasi :Nurhasanah / Hukormas)</em></p>
</div>', '2018-05-22 05:59:49', 1, NULL, NULL, 'bimbingan-snars', 'RSJPDHK menyelenggarakan bimbingan SNARS', NULL, 1);


--
-- Name: trans_list_artikel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_list_artikel_id_seq', 1, true);


--
-- Name: trans_list_dokter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_list_dokter_id_seq', 4, true);


--
-- Data for Name: trans_list_publikasi; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_list_publikasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_list_publikasi_id_seq', 1, false);


--
-- Data for Name: trans_pengumuman; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_pengumuman_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_pengumuman_id_seq', 1, false);


--
-- Data for Name: trans_profile; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_profile (id, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (3, 'Sambutan', '<p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Putra Palembang yang terlahir pada 1 Januari 1966</span>&nbsp; merupakan seorang Dokter Spesialis Jantung dan Pembuluh Darah. Saat ini, dr. Iwan Dakota berpraktek di Rumah Sakit Jantung&nbsp;dan Pembuluh Darah&nbsp;Harapan Kita di Slipi, Jakarta Barat. Sebagai seorang dokter, juga sebagai Direktur Utama &nbsp;dan beliau telah mengenyam pendidikan di Universitas Sriwijaya tahun 1985-1992, untuk S1 Kedokteran Umum, dilanjutkan S2 Spesialias Kardiologi 1996-2001 dan S3 untuk Magister RS tahun 2012-2014 di Universitas Indonesia.</p><p style="padding: 0px; font-size: 13px; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96);">Pertama-tama marilah kita mengucapkan Puji Syukur kehadirat Allah SWT, karena atas Rahmat dan Hidayah-Nya, kita dapat menyusun Annual Report Tahun 2015. Pada tahun 2015 Rumah Sakit Jantung Dan Pembuluh Darah Harapan Kita (RSJPDHK) telah berupaya meningkatkan mutu pelayanan melalui produk unggulan, dengan terwujudnya Akreditasi KARS dengan hasil Paripurna, yang merupakan prestasi dari seluruh kinerja Karyawan RSJPDHK. Ke depan RSJPDHK berkomitmen untuk mewujudkan akreditasi Internasional melalui Akreditasi Joint Commission International (JCI) yang menjadi wujud nyata peningkatan profesionalisme bagi Rumah Sakit. RSJPDHK adalah rumah sakit rujukan tersier yang mengutamakan pelayanan unggulan dengan prosedur tindakan yang didukung dengan alat berteknologi canggih, dan tenaga medis dan non-medis yang berkompeten dengan mengutamakan mutu pelayanan dan keselamatan pasien. RSJPDHK selalu berkerjasama dengan institusi ternama di luar negeri untuk meningkatkan profesionalitastenaga medis dan non medis untuk pelayanan kardiovaskular. Evaluasi kinerja tahunan setiap kali dilakukan secara konsisten dengan tujuan untuk mengetahui tingkat efiensi dan kualitas dari seluruh unit revenue center dalam memberikan kontribusi pelayanan terhadap pelanggan RSJPDHK, sebagai dasar untuk melakukan suatu langkah perbaikan dalam peningkatan mutu dan memperkecil faktor risiko yang timbul dari proses pelayanan tersebut. Apresiasi dan harapan dari manajemen dan stake holder dalam tahun berikutnya akan mewujudkan RSJPDHK yang memilki nilai dan budaya organisasi yang lebih baik. Manajemen mengucapkan terima kasih atas penghargaan setinggi-tingginya kepada seluruh karyawan RSJPDHK atas suksesnya penyelenggaraan pelayaan prima terhadap seluruh pelanggan RSJPDHK.</p>', 'Dr. dr. Iwan Dakota, Sp.JP(K) MARS,FACC, FESC', '2018-05-12 09:48:54', 1, NULL, NULL, 'sambutan', NULL);
INSERT INTO public.trans_profile (id, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (4, 'Dewan Pengawas', '<p><span style="color: rgba(0, 0, 0, 0.87); font-size: 12px;">Dewan Pengawas Rumah Sakit Jantung dan Pembuluh Darah Harapan kita</span><br></p>', 'Dewan Pengawas Rumah Sakit Jantung dan Pembuluh Darah Harapan kita', '2018-05-12 09:52:57', 1, NULL, NULL, 'dewan-pengawas', NULL);
INSERT INTO public.trans_profile (id, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (6, 'Tentang Kami', '<h2 style="margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><p class="MsoNormal" style="margin-bottom: 0.0001pt; padding: 0px; font-size: 13px; font-weight: 400; font-style: normal; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96); text-indent: 1cm; line-height: 19.5px;"><em style="font-family: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita (RSJPDHK) merupakan rumah sakit khusus yang menjadi Pusat Rujukan Nasional untuk penanganan penyakit jantung dan pembuluh darah (kardiovaskular). Rumah sakit ini didirikan oleh Yayasan Harapan Kita diatas tanah&nbsp;</span><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; text-decoration-line: underline; font-size: 12px;">seluas 22.389</span><span style="font-size: 12px; font-family: Arial;">&nbsp;m2</span></em></strong><span style="font-size: 12px; font-family: Arial;">&nbsp;di Jl. S. Parman kavling 87 Slipi, Jakarta Barat dan diresmikan pada tanggal</span><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; text-decoration-line: underline; font-size: 12px;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">&nbsp;9 Nopember 1985.</strong></em></span></span></em><br></p><p class="MsoNormal" style="margin-bottom: 0.0001pt; padding: 0px; font-size: 13px; font-weight: 400; font-style: normal; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96); text-indent: 1cm; line-height: 19.5px;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><br></strong></em></span></span></em><em style="font-family: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; font-size: 12px;">Pada tanggal 27 Maret 1985 Yayasan Harapan Kita melalui Surat Keputusan No.02/1985 menyerahkan kepemilikan rumah sakit ini kepada pemerintah dalam hal ini Departemen Kesehatan, tetapi pengelolaannya diserahkan kepada Yayasan Harapan Kita berdasarkan SK No. 57/Menkes/SK/II/1985. Pada tanggal 31 Juli 1997 Yayasan Harapan Kita menyerahkan kembali pengelolaan Rumah Sakit Jantung Harapan Kita kepada Departemen Kesehatan Republik Indonesia dan selanjutnya melalui Peraturan Pemerintah nomor 126 tahun 2000, status Rumah Sakit Jantung Harapan Kita pun berubah menjadi Perusahaan Jawatan dibawah naungan Kementerian BUMN.&nbsp;</span></em></p><p class="MsoNormal" style="margin-bottom: 0.0001pt; padding: 0px; font-size: 13px; font-weight: 400; font-style: normal; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96); text-indent: 1cm; line-height: 19.5px;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><br></span></em><em style="font-family: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; font-size: 12px;">Pada tanggal 13 Juni 2005, ditetapkan Peraturan Pemerintah nomor 23 tahun 2005 tentang Pola Pengelolaan Keuangan Badan Layanan Umum, yang menyebutkan perubahan status rumah sakit yang semula berstatus Perusahaan Jawatan (Badan Usaha Milik Negara) menjadi Badan Layanan Umum (pasal 37 ayat 2). Dengan demikian, Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita pun berubah statusnya menjadi BLU-RSJPD Harapan Kita, yang berada di bawah Kementerian Kesehatan RI sebagai Unit Pelaksanaan Teknis dengan menerapkan Pola Pengelolaan Keuangan Badan Layanan Umum (PPK-BLU).</span></em></p><p class="MsoNormal" style="margin-bottom: 0.0001pt; padding: 0px; font-size: 13px; font-weight: 400; font-style: normal; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96); text-indent: 1cm; line-height: 19.5px;"><em style="font-family: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><br></span></em><em style="font-family: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: normal; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; font-size: 12px;">Sebagai Pusat Jantung Nasional (National Cardiovascular Centre), selain menyediakan pelayanan kesehatan jantung, RSJPDHK juga dikembangkan sebagai wahana pendidikan serta pelatihan, dan penelitian&nbsp;</span></em><span style="font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; font-size: 12pt; border: 0px; font-family: &quot;Times New Roman&quot;, serif; outline: 0px; vertical-align: baseline; line-height: 24px;"><span style="font-size: 12px; font-family: Arial;">dalam bidang kesehatan kardiovaskular. Berbagai upaya telah dilaksanakan untuk menciptakan&nbsp;</span><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; font-size: 12px;">Good Corporate Governance,&nbsp;</span><span style="font-size: 12px; font-family: Arial;">yakni: transparansi, kemandirian, akuntabilitas, pertanggung jawaban dan kewajaran. Salah satu wujud pelaksanaannya adalah senantiasa meningkatkan mutu layanan yang salah satu upaya dilakukan melalui program akreditasi baik tingkat Nasional maupun Internasional.</span></span></p><p class="MsoNormal" style="margin-bottom: 0.0001pt; padding: 0px; font-size: 13px; font-weight: 400; font-style: normal; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline; color: rgb(96, 96, 96); text-indent: 1cm; line-height: 19.5px;"><span style="font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; margin: 0px; padding: 0px; font-size: 12pt; border: 0px; font-family: &quot;Times New Roman&quot;, serif; outline: 0px; vertical-align: baseline; line-height: 24px;"><br></span><span style="font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background-color: transparent; font-family: Arial; font-size: 12px;">Akreditasi dibidang pendidikan juga dilakukan terkait dengan penyelenggaraan pedidikan sebagai salah satu rumah sakit pendidikan dibidang kardiovaskular. Adapun jenis akreditasi dimaksud yang telah dicapai diantaranya akreditasi rumah sakit pendidikan oleh Kementerian Kesehatan RI pada tahun 2014,&nbsp;</span><strong style="font-family: inherit; font-style: inherit; text-align: inherit; text-indent: 1cm; background: transparent; font-size: 12pt; margin: 0px; padding: 0px; font-weight: bold; border: 0px; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: Arial; outline: 0px; vertical-align: baseline; text-decoration-line: underline; font-size: 12px;">Akreditasi KARS paripurna versi 2012</span><span style="font-size: 12px; font-family: Arial;">&nbsp;</span></em></strong><span style="font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background-color: transparent; font-family: Arial; font-size: 12px;">pada tahun 2015, serta&nbsp;</span><span style="font-family: Arial; font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background: transparent; font-size: 12px; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; text-decoration-line: underline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;">Akreditasi Internasional Joint Commition International (JCI) pada tahun 2016</em></strong></span><span style="font-style: inherit; font-weight: inherit; text-align: inherit; text-indent: 1cm; background-color: transparent; font-family: Arial; font-size: 12px;">.</span></p></h2>', 'Rumah Sakit Jantung dan Pembuluh Darah Harapan Kita', '2018-05-12 10:06:44', 1, NULL, NULL, 'tentang-kami', NULL);
INSERT INTO public.trans_profile (id, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (9, 'Aksebilitas RS', '<h2 style="margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">1. Website : &nbsp;</span><a href="http://www.pjnhk.go.id/" style="margin: 0px; padding: 0px; font-style: inherit; text-align: inherit; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; outline: 0px; vertical-align: baseline; transition: all 0.15s ease; color: rgb(45, 132, 182); text-decoration: underline;"><span style="font-size: 12px;">http://www.pjnhk.go.id</span></a></em></strong></h2><h2 style="margin-top: 0px; margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">2. Perjanjian Online via Android : (download di &nbsp;</span></strong><span style="margin: 0px; padding: 0px; font-weight: inherit; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: arial, sans-serif; outline: 0px; vertical-align: baseline; white-space: nowrap; color: rgb(0, 102, 33); line-height: 16px;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; white-space: normal;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><a href="https://play.google.com/store/apps" style="margin: 0px; padding: 0px; font-style: inherit; text-align: inherit; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; outline: 0px; vertical-align: baseline; transition: all 0.15s ease; color: rgb(45, 132, 182); text-decoration: underline;"><span style="font-size: 12px;">https://play.google.com/store/apps</span></a></em></strong><span style="font-size: 12px;">&nbsp;atau&nbsp;</span></span><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline; line-height: 1.3em;"><em style="margin: 0px; padding: 0px; font-weight: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">Google Playstore) : PJNHK</span></em></strong></h2><h2 style="margin-top: 0px; margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">3. SPGDT - IGD 24 Jam</span></strong></h2><h2 style="margin-top: 0px; margin-bottom: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">4. Layanan Poliklinik Umum dan Poliklinik Eksekutif</span></strong></h2><h2 style="margin-top: 0px; font-size: 18px; font-style: italic; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-family: Roboto, Arial, Helvetica, sans-serif; outline: 0px; vertical-align: baseline;"><strong style="margin: 0px; padding: 0px; font-weight: bold; font-style: inherit; text-align: inherit; background: transparent; border: 0px; font-family: inherit; outline: 0px; vertical-align: baseline;"><span style="font-size: 12px;">5. Contack Center 1500034</span></strong></h2>', 'Aksebilitas Rumah Sakit', '2018-05-12 10:21:02', 1, '2018-05-12 10:22:43', 1, 'aksebilitas-rs', NULL);
INSERT INTO public.trans_profile (id, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (1, 'Struktur Organisasi', '<p>Struktur Organisasi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita<br></p>', 'Struktur Organisasi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita', '2018-05-12 09:25:04', 1, '2018-05-25 04:05:40', 1, 'struktur-organisasi', NULL);
INSERT INTO public.trans_profile (id, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (2, 'Jajaran Direksi', '<p><span style="color: rgba(0, 0, 0, 0.87); font-size: 12px;">Jajaran Direksi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita</span><br></p>', 'Jajaran Direksi Rumah Sakit Jantung dan Pembuluh Darah Harapan kita', '2018-05-12 09:46:29', 1, '2018-05-28 04:25:35', 1, 'jajaran-direksi', NULL);
INSERT INTO public.trans_profile (id, judul, konten, keterangan, created_at, created_by, updated_at, updated_by, slug, gambar) VALUES (7, 'Visi Misi', '<h2>VISI</h2><p>visi kami adalah ...<br></p><h1>MISI</h1><p>misi kami adalah ...<br></p><h1>Nilai Budaya</h1><p>nilai budaya kami adalah ...<br></p>', 'Visi & Misi Rumah Sakit Jantung dan Hati Harapan Kita', '2018-05-12 10:11:16', 1, '2018-05-30 06:50:41', 1, 'visi-misi', NULL);


--
-- Name: trans_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_profile_id_seq', 9, true);


--
-- Data for Name: trans_rujukan_nasional; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_rujukan_nasional (id, judul, pengantar, profile, cara_merujuk, penelitian, jejaring, prosedur, alamat, no_tlpn, email, deskripsi, photo, status, created_at, created_by, updated_at, updated_by, akses_deskripsi, kordinat, fasilitas_nama, fasilitas_pelayanan) VALUES (2, 'Kardiovaskuler', '<p>Test<br></p>', '<p>test<br></p>', '<p>test<br></p>', '<p>test<br></p>', '<p>tset<br></p>', NULL, 'test', '0123', 'test@test.com', 'test', 'uploads/rujukan-nasional/qzZefqB1howt59cl67y12QDKq6emgnZvFdgGwOZ7.jpeg', 1, '2018-05-24 08:59:28', 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.trans_rujukan_nasional (id, judul, pengantar, profile, cara_merujuk, penelitian, jejaring, prosedur, alamat, no_tlpn, email, deskripsi, photo, status, created_at, created_by, updated_at, updated_by, akses_deskripsi, kordinat, fasilitas_nama, fasilitas_pelayanan) VALUES (3, 'Pusat Jantung Terpadu', '<p>Test<br></p>', '<p>Test<br></p>', '<p>Test<br></p>', '<p>Test<br></p>', '<p>Test<br></p>', NULL, 'Test', '123', 'test@test.test', 'Test', 'uploads/rujukan-nasional/EAumXoM645tsFlt7atYXh7pJyiUnhMvDMyHph0ok.jpeg', 1, '2018-05-25 04:55:31', 1, NULL, NULL, NULL, '1212', 'Test', '<p>Test<br></p>');


--
-- Data for Name: trans_rujukan_agenda; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_rujukan_agenda (id, id_rujukan_nasional, nama, keterangan, tahun, created_at, created_by, updated_at, updated_by) VALUES (1, 3, 'Test', 'Test', '2018', '2018-05-25 04:55:31', 1, NULL, NULL);


--
-- Name: trans_rujukan_agenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_rujukan_agenda_id_seq', 1, true);


--
-- Name: trans_rujukan_nasional_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_rujukan_nasional_id_seq', 3, true);


--
-- Data for Name: trans_rujukan_produk; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_rujukan_produk (id, id_rujukan_nasional, nama, keterangan, gambar, created_at, created_by, updated_at, updated_by) VALUES (1, 3, 'test', 'Test', 'uploads/rujukan-nasional-produk/8a9Fks5zB9pOcZtPbkbs2WXZe7K2oLTGBiDaTJhg.jpeg', '2018-05-25 04:55:31', 1, NULL, NULL);


--
-- Name: trans_rujukan_produk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_rujukan_produk_id_seq', 1, true);


--
-- Data for Name: trans_rujukan_tim; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.trans_rujukan_tim (id, id_rujukan_nasional, id_dokter, posisi, created_at, created_by, updated_at, updated_by) VALUES (1, 3, 1, '1', '2018-05-25 04:55:31', 1, NULL, NULL);


--
-- Name: trans_rujukan_tim_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_rujukan_tim_id_seq', 1, true);


--
-- Data for Name: trans_testimoni; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: trans_testimoni_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trans_testimoni_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

