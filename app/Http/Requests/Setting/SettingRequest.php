<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\Request;

class SettingRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'kunci' => 'unique:trans_sukaman_setting,kunci'.$unique,
            'judul' => 'required',
            'isi' => 'required',
        ];
    }
}
