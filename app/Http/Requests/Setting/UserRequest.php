<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        if ($this->isMethod('PUT')) {
            return [
                'name' => 'required|unique:sys_users,name'.$unique,
                'email' => 'required|unique:sys_users,email'.$unique,
                'password' => 'sometimes|confirmed|min:6',
                'role_id' => 'required',
            ];
        }

        return [
            'name' => 'required|unique:sys_users,name',
            'email' => 'required|unique:sys_users,email',
            'password' => 'required|confirmed|min:6',
            'role_id' => 'required',
        ];
    }

    public function attributes()
    {
        return [
          'role_id' => 'Role & Permission',
        ];
    }
}
