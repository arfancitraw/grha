<?php

namespace App\Http\Requests\InfoPasien;

use App\Http\Requests\Request;

class TanyaJawabRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required',
            'email' => 'required',
            'rahasia' => 'required',
            'id_kategori' => 'required',
            'judul' => 'required|min:3|unique:trans_tanya_jawab,judul'.$unique,
            'konten' => 'required|min:3',
        ];
    }
}
