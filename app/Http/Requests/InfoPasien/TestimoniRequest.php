<?php

namespace App\Http\Requests\InfoPasien;

use App\Http\Requests\Request;

class TestimoniRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required',
            'email' => 'required',
            'layanan_yang_digunakan' => 'required',
            'testimoni' => 'required',
            'publik' => 'required|max:150'
        ];
    }
}
