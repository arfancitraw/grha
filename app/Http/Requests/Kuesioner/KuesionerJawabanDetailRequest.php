<?php

namespace App\Http\Requests\Kuesioner;

use App\Http\Requests\Request;

class KuesionerJawabanDetailRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'kuesioner_jawaban_id' => 'required',
            'kuesioner_detail_id' => 'required',
            'jawaban' => 'required',
        ];
    }
    public function authorize(){
        return true;
    }
}
