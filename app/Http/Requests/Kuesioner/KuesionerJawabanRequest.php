<?php

namespace App\Http\Requests\Kuesioner;

use App\Http\Requests\Request;

class KuesionerJawabanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required',
            'email' => 'required',
            'kuesioner_id' => 'required',
            'feedback' => 'required',
        ];
    }
    public function authorize(){
        return true;
    }
}
