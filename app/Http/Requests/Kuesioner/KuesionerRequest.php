<?php

namespace App\Http\Requests\Kuesioner;

use App\Http\Requests\Request;

class KuesionerRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'periode' => 'required|unique:ref_kuesioner_periode,periode'.$unique,
            'status' => 'required',
            'detail.*.pertanyaan.id'  => "required|string|distinct|min:5",
        ];
    }
    public function authorize(){
        return true;
    }
}
