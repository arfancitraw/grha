<?php

namespace App\Http\Requests\Pertanyaan;

use App\Http\Requests\Request;

class JawabanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'referensi' => 'required',
            'jawaban' => 'required',
        ];
    }
}
