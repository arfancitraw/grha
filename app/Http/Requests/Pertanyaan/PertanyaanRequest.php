<?php

namespace App\Http\Requests\Pertanyaan;

use App\Http\Requests\Request;

class PertanyaanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'id_tanya_jawab' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'judul' => 'required',
            'konten' => 'required'
        ];
    }
}
