<?php

namespace App\Http\Requests\Informasi;

use App\Http\Requests\Request;

class InformasiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'judul' => 'required|max:64|unique:trans_informasi,judul'.$unique,
            'konten' => 'required',
            'keterangan' => 'required|max:150'
        ];
    }
}
