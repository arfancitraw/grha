<?php

namespace App\Http\Requests\Mobile;

use App\Http\Requests\Request;

class DireksiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required|unique:trans_mobile_direksi,nama'.$unique,
            'jabatan' => 'required',
            'urutan' => 'required',
            'photo' => 'required',
        ];
    }
}
