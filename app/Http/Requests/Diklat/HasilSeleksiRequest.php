<?php

namespace App\Http\Requests\Diklat;

use App\Http\Requests\Request;

class HasilSeleksiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'karir_id' => 'required',
        ];
    }
}
