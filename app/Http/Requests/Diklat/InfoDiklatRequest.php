<?php

namespace App\Http\Requests\Diklat;

use App\Http\Requests\Request;

class InfoDiklatRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'judul' => 'required',
            'konten' => 'required',
        ];
    }
}
