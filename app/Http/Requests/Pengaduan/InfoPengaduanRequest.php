<?php

namespace App\Http\Requests\Pengaduan;

use App\Http\Requests\Request;

class InfoPengaduanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'judul' => 'required',
        ];
    }
}
