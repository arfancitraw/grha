<?php

namespace App\Http\Requests\Sukaman;

use App\Http\Requests\Request;

class DokterRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'id_dokter' => 'required|unique:trans_sukaman_dokter,id_dokter'.$unique,
        ];
    }
}
