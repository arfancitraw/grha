<?php

namespace App\Http\Requests\Sukaman;

use App\Http\Requests\Request;

class InformasiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'judul' => 'required|unique:trans_sukaman_informasi,judul'.$unique,
            'konten' => 'required',
        ];
    }
}
