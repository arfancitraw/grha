<?php

namespace App\Http\Requests\Sukaman;

use App\Http\Requests\Request;

class PricingDetailRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required',
            'id_tarif' => 'required',
            'check' => 'required',
        ];
    }
}
