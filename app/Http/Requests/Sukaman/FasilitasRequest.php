<?php

namespace App\Http\Requests\Sukaman;

use App\Http\Requests\Request;

class FasilitasRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'judul' => 'required|unique:trans_sukaman_fasilitas,judul'.$unique,
            'gambar' => 'required',
            'deskripsi' => 'required',
        ];
    }
}
