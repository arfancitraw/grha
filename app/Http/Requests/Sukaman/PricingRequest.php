<?php

namespace App\Http\Requests\Sukaman;

use App\Http\Requests\Request;

class PricingRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';
        return [
            'nama' => 'required|unique:trans_sukaman_tarif,nama'.$unique,
            'id_kategori' => 'required',
            'harga' => 'required',
        ];
    }
}
