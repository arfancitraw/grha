<?php

namespace App\Http\Requests\Dokter;

use App\Http\Requests\Request;

class ListDokterRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required',
            'nama_lengkap' => 'required',
            'spesialisasi_id' => 'required',
        ];
    }
}
