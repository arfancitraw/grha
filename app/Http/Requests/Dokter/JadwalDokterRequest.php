<?php

namespace App\Http\Requests\Dokter;

use App\Http\Requests\Request;

class JadwalDokterRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'list_dokter_id' => 'required',
            'hari_praktek' => 'required',
            'jam_mulai_praktek' => 'required',
            'jam_selesai_praktek' => 'required',
        ];
    }
}
