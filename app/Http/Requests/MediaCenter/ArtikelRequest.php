<?php

namespace App\Http\Requests\MediaCenter;

use App\Http\Requests\Request;

class ArtikelRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'judul' => 'required|unique:trans_list_artikel,judul'.$unique,
            'slug' => 'required',
            'deskripsi' => 'required | max:100',
            'kategori_id' => 'required',
            'photo' => 'max:5120',
        ];
    }
}
