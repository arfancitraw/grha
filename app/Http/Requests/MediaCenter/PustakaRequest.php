<?php

namespace App\Http\Requests\MediaCenter;

use App\Http\Requests\Request;

class PustakaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'judul' => 'required',
            'pengertian' => 'required',
            'gejala' => 'required',
            'penyebab' => 'required',
            'penanganan' => 'required',
            'pencegahan' => 'required'
        ];
    }
}
