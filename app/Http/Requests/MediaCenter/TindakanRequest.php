<?php

namespace App\Http\Requests\MediaCenter;

use App\Http\Requests\Request;

class TindakanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required|unique:trans_pustaka_tindakan,nama'.$unique,
            // 'indikasi' => 'required',
        ];
    }
}
