<?php

namespace App\Http\Requests\PasienDanPengunjung;

use App\Http\Requests\Request;

class AlurPendaftaranRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'judul' => 'required|unique:trans_alur_pendaftaran,judul'.$unique,
        ];
    }
}
