<?php

namespace App\Http\Requests\Litbang;

use App\Http\Requests\Request;

class ListPublikasiRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'judul' => 'required',
            'tahun' => 'required',
            'peneliti_utama' => 'required',
            'peneliti_pendamping' => 'required',
            'citasi_jurnal' => 'required',
            // 'spesialisasi_id' => 'required',
        ];
    }
}
