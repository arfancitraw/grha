<?php

namespace App\Http\Requests\Litbang;

use App\Http\Requests\Request;

class KomiteEtikRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required',
            'jabatan' => 'required',
            'departemen' => 'required',
            // 'spesialisasi_id' => 'required',
        ];
    }
}
