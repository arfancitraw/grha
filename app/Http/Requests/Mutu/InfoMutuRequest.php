<?php

namespace App\Http\Requests\Mutu;

use App\Http\Requests\Request;

class InfoMutuRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required',
            'jenis_mutu' => 'required',
            'periode' => 'required',
        ];
    }
}
