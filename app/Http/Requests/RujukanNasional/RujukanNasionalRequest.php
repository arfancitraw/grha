<?php

namespace App\Http\Requests\RujukanNasional;

use App\Http\Requests\Request;

class RujukanNasionalRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'judul' => 'required|max:64',
            'deskripsi' => 'required',
            // 'photo' => ($this->get('id')) ? '' : 'required',
            // 'pengantar' => 'required',
            // 'penelitian' => 'required',
            // 'profile' => 'required',
            // 'jejaring' => 'required',
             
            // 'produk.nama.*' => 'required',
            // 'produk.keterangan.*' => 'required',
            
            // 'agenda.nama.*' => 'required',
            // 'agenda.tahun.*' => 'required',
            // 'agenda.keterangan.*' => 'required',
            
            // 'produk' => 'required',
            // 'alamat' => 'required',
            // 'no_tlpn' => 'required|max:13',
            // 'email' => 'required',
            // 'akses_deskripsi' => 'required',
            // 'cara_merujuk' => 'required',

            // 'fasilitas_nama' => 'required|max:64',
            // 'fasilitas_pelayanan' => 'required',
            
        ];
    }
}
