<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class TipeMediaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'tipe_media' => 'required|unique:ref_tipe_media,tipe_media'.$unique,
            'deskripsi' => 'required',
        ];
    }
}
