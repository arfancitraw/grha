<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class RoleRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'display_name' => 'required|unique:sys_roles,display_name'.$unique,
            'description' => 'required',
        ];
    }

    public function attributes()
    {
        return [
          'display_name' => 'Nama Role',
          'description' => 'Deskripsi'
        ];
    }
}
