<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class FooterRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required|unique:ref_tautan_footer,nama'.$unique,
            'deskripsi' => 'required',
            'url' => 'required',
        ];
    }
}
