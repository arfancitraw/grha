<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class SertifikatFooterRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required',
            'keterangan' => 'required',
        ];
    }
}
