<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class KategoriTanyaJawabRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'nama' => 'required|unique:ref_kategori_tanya_jawab,nama'.$unique,
        ];
    }
}
