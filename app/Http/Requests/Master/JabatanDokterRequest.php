<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class JabatanDokterRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'jabatan' => 'required|unique:ref_jabatan_dokter,jabatan'.$unique,
            'deskripsi' => 'required',
            // 'spesialisasi_id' => 'required',
        ];
    }
}
