<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class SliderRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'judul' => 'required|unique:ref_slider,judul'.$unique,
            'sub_judul' => 'required',
            // 'url' => 'required',
            // 'posisi' => 'required',
        ];
    }
}
