<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;

class TipePendaftarRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        return [
            'tipe_pendaftar' => 'required|unique:ref_tipe_pendaftar,tipe_pendaftar'.$unique,
        ];
    }
}
