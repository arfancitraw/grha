<?php

namespace App\Http\Middleware;

use Closure;
use Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('mainMenu', function ($menu) {
            //DASHBOARD
            $menu->add('Dashboard', 'home')
                 ->data('icon', 'dashboard')
                 ->active('dashboard/*');

            $menu->add('Media Center')
                 ->data('icon', 'image')
                 ->active('backend/media-centers/*')
                 ->data('perms', [
                   'backend/media-center/artikel',
                   'backend/media-center/berita',
                   'backend/media-center/pustaka/penyakit',
                   'backend/media-center/pustaka/tindakan'
                 ]);
            $menu->mediaCenter->add('Artikel', 'backend/media-center/artikel')
                              ->active('backend/media-center/artikel/*');
            $menu->mediaCenter->add('Kegiatan', 'backend/media-center/kegiatan')
                              ->active('backend/media-center/kegiatan/*');
            $menu->mediaCenter->add('Berita', 'backend/media-center/pengumuman')
                              ->active('backend/media-center/pengumuman/*');
            $menu->mediaCenter->add('Pustaka', 'backend/media-center/pustaka')
                              ->active('backend/media-center/pustaka/*')
                              ->data('perms', [
                                'backend/media-center/pustaka/penyakit',
                                'backend/media-center/pustaka/tindakan'
                              ]);

            $menu->pustaka->add('Penyakit', 'backend/media-center/pustaka/penyakit')
                          ->active('backend/media-center/pustaka/penyakit/*');
            $menu->pustaka->add('Tindakan', 'backend/media-center/pustaka/tindakan')
                          ->active('backend/media-center/pustaka/tindakan/*');
            // $menu->mediaCenter->add('Pustaka', 'backend/media-center/pustaka');

            $menu->add('Profil', 'backend/profil')
                 ->data('icon', 'hospital')
                 ->active('backend/profil/*');

            //Layanan Umum
            $menu->add('Pelayanan', 'backend/layanan')
                 ->data('icon', 'ambulance')
                 ->active('backend/layanan/*');

            //tim dokter
            $menu->add('Tim Dokter', 'backend/dokter/list-dokter')
                 ->data('icon', 'users')
                 ->active('backend/dokter/*');
            // $menu->timDokter
            //      ->add('List Dokter', 'backend/dokter/list-dokter')
            //      ->active('backend/dokter/list-dokter');
            // $menu->timDokter
            //      ->add('Jadwal Dokter', 'backend/dokter/jadwal-dokter')
            //      ->active('backend/dokter/jadwal-dokter');

            // rujukan nasional
            $menu->add('Rujukan Nasional', 'backend/rujukan-nasional')
                 ->data('icon', 'first aid')
                 ->active('backend/rujukan-nasional/*');

            // diklat
            $menu->add('Diklat')
                 ->data('icon', 'certificate')
                 ->active('backend/diklat/*')
                 ->data('perms', [
                   'backend/diklat/info-diklat',
                   'backend/diklat/jadwal-diklat'
                 ]);

            $menu->diklat->add('Info Diklat', 'backend/diklat/info-diklat')
                         ->active('backend/diklat/info-diklat/*');
            $menu->diklat->add('Jadwal Diklat', 'backend/diklat/jadwal-diklat')
                         ->active('backend/diklat/jadwal-diklat/*');
            // $menu->diklat->add('Info Karir', 'backend/diklat/info');
            // $menu->diklat->add('Hasil Seleksi', 'backend/diklat/hasil-seleksi');

            // karir
            $menu->add('Litbang')
                 ->data('icon', 'lab')
                 ->active('backend/litbang/*')
                 ->data('perms', [
                   'backend/litbang/info-litbang',
                   'backend/litbang/komite-etik',
                   'backend/media-center/litbang/list-publikasi'
                 ]);

            $menu->litbang->add('Info Litbang', 'backend/litbang/info-litbang')
                          ->active('backend/litbang/info-litbang/*');
            $menu->litbang->add('Komite Etik', 'backend/litbang/komite-etik')
                          ->active('backend/litbang/komite-etik/*');
            $menu->litbang->add('List Publikasi', 'backend/litbang/list-publikasi')
                          ->active('backend/litbang/list-publikasi/*');

            $menu->add('Informasi', 'backend/informasi')
                 ->data('icon', 'info circle')
                 ->active('backend/informasi/*');

            $menu->add('Mutu', 'backend/mutu')
                 ->data('icon', 'circle check')
                 ->active('backend/mutu/*');

            //Pasien Prngunjung
            // $menu->add('Info Pasien')
            //      ->data('icon', 'user');
            // $menu->infoPasien->add('Info Pasien', 'backend/info-pasien/info');
            // $menu->infoPasien->add('Testimoni', 'backend/info-pasien/testimoni');
            // $menu->infoPasien->add('Komplain', 'backend/info-pasien/komplain');
            // $menu->infoPasien->add('Info Pengaduan', 'backend/pengaduan/info-pengaduan');
            // $menu->infoPasien->add('Kontak Pengaduan', 'backend/pengaduan/item-pengaduan');

            //Pasien Prngunjung
            // $menu->add('Whistleblowing')
            // ->data('icon', 'announcement');
            // $menu->whistleblowing->add('Info Whistle', 'backend/whistleblowing/info-whistle');
            // $menu->whistleblowing->add('Item Whistle', 'backend/whistleblowing/item-whistle');

            // karir
            $menu->add('Kuesioner', 'backend/kuesioner')
                 ->data('icon', 'edit')
                 ->active('backend/kuesioner/*');

            $menu->add('Testimoni', 'backend/testimoni')
                 ->data('icon', 'comment')
                 ->active('backend/testimoni/*');

            $menu->add('Pertanyaan', 'backend/pertanyaan')
                 ->data('icon', 'comments outline')
                 ->active('backend/pertanyaan/*');

            $menu->add('Pengaduan')
                 ->data('icon', 'bullhorn');
            $menu->pengaduan
                 ->add('Narasi', 'backend/pengaduan/narasi')
                 ->active('backend/pengaduan/narasi/*');
            $menu->pengaduan
                 ->add('Etik & Hukum', 'backend/pengaduan/etik-hukum')
                 ->active('backend/pengaduan/etik-hukum/*');
            $menu->pengaduan
                 ->add('Pengaduan Masyarakat', 'backend/pengaduan/masyarakat')
                 ->active('backend/pengaduan/masyarakat/*');
            $menu->pengaduan
                 ->add('Whistleblowing', 'backend/pengaduan/whistleblowing')
                 ->active('backend/pengaduan/whistleblowing/*');
            $menu->pengaduan
                 ->add('Gratifikasi', 'backend/pengaduan/gratifikasi')
                 ->active('backend/pengaduan/gratifikasi/*');
            $menu->pengaduan
                 ->add('Sponsor', 'backend/pengaduan/sponsor')
                 ->active('backend/pengaduan/sponsor/*');


            $menu->add('Paviliun Sukaman')
                 ->data('icon', 'hospital')
                 ->data('perms', [
                   'backend/sukaman/pelayanan',
                   'backend/sukaman/dokter',
                 ]);
            //Akan dikelompokan ke setting Website
            $menu->paviliunSukaman
                 ->add('Slider', 'backend/sukaman/slider')
                 ->active('backend/sukaman/slider/*');
            $menu->paviliunSukaman
                 ->add('Pelayanan', 'backend/sukaman/pelayanan')
                 ->active('backend/sukaman/pelayanan/*');
            $menu->paviliunSukaman
                 ->add('Dokter', 'backend/sukaman/dokter')
                 ->active('backend/sukaman/dokter/*');
            $menu->paviliunSukaman
                 ->add('Fasilitas', 'backend/sukaman/fasilitas')
                 ->active('backend/sukaman/fasilitas/*');
            $menu->paviliunSukaman
                 ->add('Informasi', 'backend/sukaman/informasi')
                 ->active('backend/sukaman/informasi/*');

            $menu->paviliunSukaman
                 ->add('Setting', 'backend/sukaman/setting')
                 ->active('backend/sukaman/setting/*');

            $menu->paviliunSukaman
                 ->add('Pricing', 'backend/sukaman/pricing-kategori')
                 ->active('backend/sukaman/pricing-kategori/*');

            $menu->paviliunSukaman
                 ->add('Kontak', 'backend/sukaman/kontak')
                 ->active('backend/sukaman/Kontak/*');
           
            // $menu->kuesioner->add('Jawaban', 'backend/kuesioner/kuesioner-jawaban');
            // $menu->kuesioner->add('Jawaban Detail', 'backend/kuesioner/kuesioner-jawaban-detail');

            $menu->add('Pengaturan Website')
                 ->data('icon', 'setting')
                 ->data('perms', [
                   'backend/master/slider',
                   'backend/master/sertifikat-footer',
                   'backend/master/media-sosial',
                   'backend/master/link-footer',
                   'backend/setting/user',
                   'backend/master/role-permission'
                 ]);
            //Akan dikelompokan ke setting Website
            $menu->pengaturanWebsite
                 ->add('Home Slider', 'backend/master/slider')
                 ->active('backend/master/slider/*');
            $menu->pengaturanWebsite
                 ->add('Pengaturan Gambar', 'backend/setting/jumbotron')
                 ->active('backend/setting/jumbotron/*');
            $menu->pengaturanWebsite
                 ->add('Sertifikat', 'backend/master/sertifikat-footer')
                 ->active('backend/master/sertifikat-footer/*');
            $menu->pengaturanWebsite
                 ->add('Media Sosial', 'backend/master/media-sosial')
                 ->active('backend/master/media-sosial/*');
            $menu->pengaturanWebsite
                 ->add('Link Footer', 'backend/master/link-footer')
                 ->active('backend/master/link-footer/*');
            $menu->pengaturanWebsite
                 ->add('Pengaturan Menu', 'backend/setting/menu')
                 ->active('backend/setting/menu/*');

            $menu->pengaturanWebsite
                 ->add('Setting', 'backend/setting/setting')
                 ->active('backend/setting/setting/*');
            $menu->pengaturanWebsite
                 ->add('Manajemen Pengguna', 'backend/setting/user')
                 ->active('backend/setting/user/*');
            $menu->pengaturanWebsite
                 ->add('Role & Permissions', 'backend/master/role-permission')
                 ->active('backend/master/role-permission/*');
           
            $menu->add('Pengaturan Mobile')
                 ->data('icon', 'mobile')
                 ->data('perms', [
                   'backend/setting/user',
                 ]);
            $menu->pengaturanMobile
                 ->add('Direksi', 'backend/mobile/direksi')
                 ->active('backend/mobile/direksi/*');
            $menu->pengaturanMobile
                 ->add('Dewan Pengawas', 'backend/mobile/dewan')
                 ->active('backend/mobile/dewan/*');

            $menu->add('Master')
                 ->data('icon', 'sidebar')
                 ->data('perms', [
                   'backend/master/poli',
                   'backend/master/jenis-mutu',
                   'backend/master/jabatan-dokter',
                   'backend/master/spesialisasi',
                   'backend/master/sub-spesialisasi',
                   'backend/master/kategori-artikel',
                   'backend/master/kategori-pengumuman',
                   'backend/master/kategori-tanya-jawab',
                   'backend/master/tipe-media',
                   'backend/master/tipe-pendaftar'
                 ]);
            // $menu->master->add('Info Bed', 'backend/master/info-bed');
            // $menu->master->add('Rumah Sakit', 'backend/master/rumah-sakit');
            $menu->master->add('Poli', 'backend/master/poli');
            $menu->master->add('Indikator Mutu', 'backend/master/jenis-mutu');
            $menu->master->add('Jabatan Dokter', 'backend/master/jabatan-dokter');
            $menu->master->add('Spesialisasi', 'backend/master/spesialisasi');
            $menu->master->add('Sub Spesialisasi', 'backend/master/sub-spesialisasi');
            $menu->master->add('Kategori Artikel', 'backend/master/kategori-artikel');
            $menu->master->add('Kategori Berita', 'backend/master/kategori-pengumuman');
            $menu->master->add('Kategori Tanya Jawab', 'backend/master/kategori-tanya-jawab');
            $menu->master->add('Tipe Media', 'backend/master/tipe-media');
            $menu->master->add('Tipe Pendaftar', 'backend/master/tipe-pendaftar');
            //Kuisioner yang bakal dipindah
            // $menu->master->add('Kuesioner', 'backend/master/kuesioner-periode');
            // $menu->master->add('Kuesioner Detail', 'backend/master/kuesioner-detail');
        });

        return $next($request);
    }
}
