<?php

namespace App\Http\Middleware;

use Closure;
use Menu;

class Bahasa
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(\Session::get('lang'));
        \App::setLocale(\Session::get('lang') == 'english' ? 'en' : 'id');
        
        return $next($request);
    }
}
