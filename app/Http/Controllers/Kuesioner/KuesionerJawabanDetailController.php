<?php

namespace App\Http\Controllers\kuesioner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Kuesioner\KuesionerJawabanDetail;
use App\Models\Master\KuesionerDetail;
use App\Models\Kuesioner\KuesionerJawaban;

use App\Http\Requests\Kuesioner\KuesionerJawabanDetailRequest;

use Datatables;

class KuesionerJawabanDetailController extends Controller
{
    //
  protected $link = 'backend/kuesioner/kuesioner-jawaban-detail/';
  protected $perms  = '';

  function __construct()
  {
    $this->setLink($this->link);
    $this->setPerms($this->perms);

    $this->setTitle("Kuesioner Jawaban Detail");
    $this->setSubtitle("Kuesioner Jawaban Detail");
    $this->setModalSize("tiny");
    $this->setBreadcrumb(['Kuesioner Jawaban Detail' => '#', 'Kuesioner Jawaban Detail' => '#']);
    $this->setTableStruct([
     [
       'data' => 'num',
       'name' => 'num',
       'label' => '#',
       'orderable' => false,
       'searchable' => false,
       'className' => "center aligned",
       'width' => '40px',
     ],
     /* --------------------------- */
     [
       'data' => 'kuesioner_jawaban_id',
       'name' => 'kuesioner_jawaban_id',
       'label' => 'Kuesioner Jawaban',
       'searchable' => false,
       'sortable' => true,
     ],
     [
       'data' => 'kuesioner_detail_id',
       'name' => 'kuesioner_detail_id',
       'label' => 'Kuesioner Detail',
       'searchable' => false,
       'sortable' => true,
     ],
     [
       'data' => 'jawaban',
       'name' => 'jawaban',
       'label' => 'Jawaban',
       'searchable' => false,
       'sortable' => true,
     ],
    [
     'data' => 'created_at',
     'name' => 'created_at',
     'label' => 'Tanggal Entry',
     'searchable' => false,
     'sortable' => true,
   ],
   [
     'data' => 'created_by',
     'name' => 'created_by',
     'label' => 'Oleh',
     'searchable' => false,
     'sortable' => true,
   ],
   [
     'data' => 'action',
     'name' => 'action',
     'label' => 'Aksi',
     'searchable' => false,
     'sortable' => false,
     'className' => "center aligned",
     'width' => '150px',
   ]
 ]);
  }

  public function grid(Request $request)
  {
    $records = KuesionerJawabanDetail::with('creator')
    ->select('*');
		//Init Sort
    if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
      $records->orderBy('created_at', 'desc');
    }

        //Filters
    if ($kuesioner_jawaban_id = $request->kuesioner_jawaban_id) {
      $records->where('kuesioner_jawaban_id', $kuesioner_jawaban_id);
    }

    if ($kuesioner_detail_id = $request->kuesioner_detail_id) {
      $records->where('kuesioner_detail_id', $kuesioner_detail_id);
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
      return $request->get('start');
    })
    ->addColumn('created_at', function ($record) {
      return $record->created_at->format('d F Y');
    })
    ->addColumn('created_by', function ($record) {
      return $record->creator->name;
    })
    ->addColumn('kuesioner_jawaban_id', function ($record) {
                // dd($record->spesialisasi);
      return $record->kuesionerjawaban['nama'];
    })

    ->addColumn('kuesioner_detail_id', function ($record) {
                // dd($record->spesialisasi);
      return $record->kuesionerdetail['kuesioner_id'];
    })

    ->addColumn('action', function ($record) {
      $btn = '';
                //Edit
      $btn .= $this->makeButton([
       'type' => 'edit-page',
       'id'   => $record->id
     ]);
                // Delete
      $btn .= $this->makeButton([
       'type' => 'delete',
       'id'   => $record->id
     ]);

      return $btn;
    })
    ->rawColumns(['action','feedback'])
    ->make(true);
  }

  public function index()
  {
    return $this->render('backend.kuesioner.kuesioner-jawaban-detail.index', ['mockup' => false]);
  }

  public function create()
  {
    return $this->render('backend.kuesioner.kuesioner-jawaban-detail.create');
  }

  public function store(KuesionerJawabanDetailRequest $request)
  {
   $jenis = new KuesionerJawabanDetail;
   $jenis->fill($request->all());
   $jenis->save();

   return response([
    'status' => true,
    'data'	=> $jenis
  ]);
 }

 public function show($id)
 {
  return KuesionerJawabanDetail::find($id);
}

public function edit($id)
{
 $record = KuesionerJawabanDetail::find($id);

 return $this->render('backend.kuesioner.kuesioner-jawaban-detail.edit', ['record' => $record]);
}

public function update(KuesionerJawabanDetailRequest $request, $id)
{
 $jenis = KuesionerJawabanDetail::find($id);
 $jenis->fill($request->all());
 $jenis->save();

 return response([
  'status' => true,
  'data'	=> $jenis
]);
}

public function destroy($id)
{
 $jenis = KuesionerJawabanDetail::find($id);
 $jenis->delete();

 return response([
  'status' => true,
]);
}
}
