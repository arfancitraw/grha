<?php

namespace App\Http\Controllers\kuesioner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Kuesioner\KuesionerJawaban;
use App\Models\Master\KuesionerPeriode;

use App\Http\Requests\Kuesioner\KuesionerJawabanRequest;

use Datatables;

class KuesionerJawabanController extends Controller
{
    //
  protected $link = 'backend/kuesioner/kuesioner-jawaban/';
  protected $perms  = '';

  function __construct()
  {
    $this->setLink($this->link);
    $this->setPerms($this->perms);

    $this->setTitle("Kuesioner Jawaban");
    $this->setSubtitle("Kuesioner Jawaban");
    $this->setModalSize("tiny");
    $this->setBreadcrumb(['Kuesioner Jawaban' => '#', 'Kuesioner Jawaban' => '#']);
    $this->setTableStruct([
     [
       'data' => 'num',
       'name' => 'num',
       'label' => '#',
       'orderable' => false,
       'searchable' => false,
       'className' => "center aligned",
       'width' => '40px',
     ],
     /* --------------------------- */
 [
      'data' => 'nama',
      'name' => 'nama',
      'label' => 'Nama',
      'searchable' => false,
      'sortable' => true,
    ],
     [
      'data' => 'email',
      'name' => 'email',
      'label' => 'Email',
      'searchable' => false,
      'sortable' => true,
    ],
     [
       'data' => 'kuesioner_id',
       'name' => 'kuesioner_id',
       'label' => 'Kuesioner Peridoe',
       'searchable' => false,
       'sortable' => true,
     ],
     [
       'data' => 'feedback',
       'name' => 'feedback',
       'label' => 'Feedback',
       'searchable' => false,
       'sortable' => true,
     ],
    [
     'data' => 'created_at',
     'name' => 'created_at',
     'label' => 'Tanggal Entry',
     'searchable' => false,
     'sortable' => true,
   ],
   [
     'data' => 'created_by',
     'name' => 'created_by',
     'label' => 'Oleh',
     'searchable' => false,
     'sortable' => true,
   ],
   [
     'data' => 'action',
     'name' => 'action',
     'label' => 'Aksi',
     'searchable' => false,
     'sortable' => false,
     'className' => "center aligned",
     'width' => '150px',
   ]
 ]);
  }

  public function grid(Request $request)
  {
    $records = KuesionerJawaban::with('creator')
    ->select('*');
		//Init Sort
    if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
      $records->orderBy('created_at', 'desc');
    }

        //Filters
    if ($kuesioner_id = $request->kuesioner_id) {
      $records->where('kuesioner_id', $kuesioner_id);
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
      return $request->get('start');
    })
    ->addColumn('status', function ($record) {
      $string = '';
      if ($record->status > 0) {
       $string .= '<div class="ui purple horizontal label">Aktif</div>';
     }else{
       $string .= '<div class="ui red horizontal label">Tidak Aktif</div>';
     }
     return $string;
   })
    ->addColumn('created_at', function ($record) {
      return $record->created_at->format('d F Y');
    })
    ->addColumn('created_by', function ($record) {
      return $record->creator->name;
    })
    ->addColumn('kuesioner_id', function ($record) {
                // dd($record->spesialisasi);
      return $record->kuesionerperiode['periode'];
    })
    ->addColumn('action', function ($record) {
      $btn = '';
                //Edit
      $btn .= $this->makeButton([
       'type' => 'edit-page',
       'id'   => $record->id
     ]);
                // Delete
      $btn .= $this->makeButton([
       'type' => 'delete',
       'id'   => $record->id
     ]);

      return $btn;
    })
    ->rawColumns(['action','feedback'])
    ->make(true);
  }

  public function index()
  {
    return $this->render('backend.kuesioner.kuesioner-jawaban.index', ['mockup' => false]);
  }

  public function create()
  {
    return $this->render('backend.kuesioner.kuesioner-jawaban.create');
  }

  public function store(KuesionerJawabanRequest $request)
  {
   $jenis = new KuesionerJawaban;
   $jenis->fill($request->all());
   $jenis->save();

   return response([
    'status' => true,
    'data'	=> $jenis
  ]);
 }

 public function show($id)
 {
  return KuesionerJawaban::find($id);
}

public function edit($id)
{
 $record = KuesionerJawaban::find($id);

 return $this->render('backend.kuesioner.kuesioner-jawaban.edit', ['record' => $record]);
}

public function update(KuesionerJawabanRequest $request, $id)
{
 $jenis = KuesionerJawaban::find($id);
 $jenis->fill($request->all());
 $jenis->save();

 return response([
  'status' => true,
  'data'	=> $jenis
]);
}

public function destroy($id)
{
 $jenis = KuesionerJawaban::find($id);
 $jenis->delete();

 return response([
  'status' => true,
]);
}
}
