<?php

namespace App\Http\Controllers\Kuesioner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Kuesioner\Periode;
use App\Models\Kuesioner\Detail;

use App\Http\Requests\Kuesioner\KuesionerRequest;

use Datatables;
use DB;

class KuesionerController extends Controller
{
    protected $link = 'backend/kuesioner/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Periode Kuesioner");
        $this->setSubtitle("Kuesioner");
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Kuesioner' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'periode',
                'name' => 'periode',
                'label' => 'Periode',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Periode::with('creator')
                          ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($periode = $request->periode) {
            $records->where('periode', 'ilike', '%' . $periode . '%');
        }
        if ($status = $request->status) {
            $records->where('status', $status);
        }

        $link = $this->link;

        return Datatables::of($records)
                         ->addColumn('num', function ($record) use ($request) {
                             return $request->get('start');
                         })
                         ->editColumn('status', function ($record) {
                             return $record->status
                                  ? '<div class="ui green horizontal label">Aktif</div>'
                                  : '<div class="ui red horizontal label">Non-aktif</div>';
                         })
                         ->addColumn('created_at', function ($record) {
                             return $record->created_at->format('d F Y');
                         })
                         ->addColumn('created_by', function ($record) {
                             return $record->creator->name;
                         })
                         ->addColumn('action', function ($record) use ($link){
                            $btn = '';
                            //Activate
                            $btn .= $this->makeButton([
                                'type' => 'modal',
                                'class' => 'green icon activate',
                                'label' => '<i class="checkmark icon"></i>',
                                'tooltip' => 'Aktifkan',
                                'disabled' => $record->status ? 'disabled' : '',
                                'id'   => $record->id
                            ]);
                            //Monitoring
                            $btn .= $this->makeButton([
                                'type' => 'url',
                                'class' => 'blue icon',
                                'label' => '<i class="clipboard icon"></i>',
                                'tooltip' => 'Monitoring Jawaban',
                                'target' => url($link.$record->id.'/monitoring')
                            ]);
                            //Edit
                            $btn .= $this->makeButton([
                                'type' => 'edit-page',
                                'id'   => $record->id
                            ]);
                            // Delete
                            $btn .= $this->makeButton([
                                'type' => 'delete',
                                'id'   => $record->id
                            ]);
                            return $btn;
                         })
                         ->rawColumns(['action','status'])
                         ->make(true);
    }

    public function index()
    {
        $this->setTitle('List Periode Kuisioner');
        $this->pushBreadcrumb(['List Periode' => '#']);
        return $this->render('backend.kuesioner.index', ['mockup' => false]);
    }

    public function create()
    {
        $this->setTitle('Tambah Periode Kuisioner');
        $this->pushBreadcrumb(['Tambah Periode Kuisioner' => '#']);
        return $this->render('backend.kuesioner.create');
    }

    public function store(KuesionerRequest $request)
    {
        $data = new Periode;
        $data->fill($request->all());
        $data->save();

        if($details = $request->detail){
            foreach ($details as $detail) {
                $kues = [];
                $kues['pertanyaan'] = $detail['pertanyaan']['id'];
                $kues['pertanyaan_en'] = $detail['pertanyaan']['en'];
                $kues['tipe'] = $detail['tipe'];
                $child = new Detail;
                $child->fill($kues);
                $data->detail()->save($child);
            }
        }

        return response([
            'status' => true,
            'data'	=> $data
        ]);
    }

    public function show($id)
    {
        return Periode::find($id);
    }

    public function edit($id)
    {
        $this->setTitle('Ubah Periode Kuisioner');
        $this->pushBreadcrumb(['Ubah Periode Kuisioner' => '#']);
        $record = Periode::with(['detail' => function($q){ $q->orderBy('id'); }])
                         ->find($id);

        return $this->render('backend.kuesioner.edit', ['record' => $record]);
    }

    public function update(KuesionerRequest $request, $id)
    {
        $data = Periode::find($id);
        $data->fill($request->all());
        $data->save();
        if($request->subid){
            $unlink = $data->detail()->whereNotIn('id', $request->subid)->get();
            if($unlink->count() > 0){
                foreach ($unlink as $key => $data) {
                    $data->delete();
                }
            }
        }

        if($details = $request->detail){
            foreach ($details as $detail) {
                if(!is_null($detail['pertanyaan'])){
                    $row = (array_key_exists('id', $detail) && $id = $detail['id']) ? Detail::find($id) : new Detail;
                    $kues = [];
                    $kues['pertanyaan'] = $detail['pertanyaan']['id'];
                    $kues['pertanyaan_en'] = $detail['pertanyaan']['en'];
                    $kues['tipe'] = $detail['tipe'];
                    $row->fill($kues);
                    $data->detail()->save($row);
                }
            }
        }

        return response([
            'status' => true,
            'data'	=> $data
        ]);
    }

    public function destroy($id)
    {
        try {
            $data = Periode::find($id);
            $data->delete();

            return response([
                'status' => true,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            return response([
                'status' => false,
                'message' => 'Data kuesioner sudah memiliki jawaban, tidak dapat dihapus.'
            ], 422);
        }
    }

    public function activate($id)
    {
        $affected = DB::table('ref_kuesioner_periode')->update(array('status' => 0));
        $data = Periode::find($id);
        $data->status = 1;
        $data->save();

        return response([
            'status' => true,
        ]);
    }

    public function monitoring($id)
    {
        $this->setTitle('Ubah Periode Kuisioner');
        $this->pushBreadcrumb(['Ubah Periode Kuisioner' => '#']);
        $record = Periode::with(['detail' => function($q){ $q->orderBy('id'); }])
                         ->find($id);

        return $this->render('backend.kuesioner.monitoring', ['record' => $record]);
    }
}
