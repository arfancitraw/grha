<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;
use App\Models\Setting\Menu;

use App\Models\Profil\Profil;

class ProfileController extends FrontEndController
{
    //
    protected $link = '/profile';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Profil");
		$this->setBreadcrumb(['Profil' => '#']);
	}

    public function index()
    {
        $menu = Profil::all();
        return $this->render('frontend.profile.index', ['mockup' => true, 'menu' => $menu]);
    }
    public function detail($slug)
    {
        $url = url($this->link.'/'.$slug);
        $menu = Menu::where('tautan', $url)->first();
        if(!is_null($menu)){
            $menu = $menu->parent->childs->sortBy('urutan');
        }else{
            $menu = Profil::orderBy('posisi','asc')->get();
        }
        $record = Profil::where('slug', $slug)->first();
        if($record){
            return $this->render('frontend.profile.detail', ['mockup' => true, 'menu' => $menu, 'record' => $record]);
        }
        return abort(404);
    }

    public function page($page)
    {
        return $this->render('frontend.profile.'.$page, ['mockup' => true]);
    }
}
