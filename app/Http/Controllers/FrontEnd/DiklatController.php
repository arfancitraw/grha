<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\Diklat\InfoDiklat;
use App\Models\Diklat\JadwalDiklat;

class DiklatController extends FrontEndController
{
    //
    protected $link = '/diklat';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Diklat");
		$this->setBreadcrumb(['Diklat' => '#']);
	}

    public function index()
    {
       $diklat = InfoDiklat::get();
       $jadwal = JadwalDiklat::orderBy('tanggal')
                             ->orderBy('jam')
                             ->get();

        return $this->render('frontend.diklat.index', [
            'mockup' => false, 
            'diklat' => $diklat, 
            'jadwal' => $jadwal
        ]);

        // $diklat = Diklat::orderBy('id', 'ASC')->get();
        // return $this->render('frontend.diklat.index', ['mockup' => false, 'diklat' => $diklat]);
    }
    public function detail($id)
    {
        $diklat = JadwalDiklat::find($id);
        
        return $this->render('frontend.diklat.detail', [
            'mockup' => false,
            'diklat' => $diklat
        ]);
    }
}
