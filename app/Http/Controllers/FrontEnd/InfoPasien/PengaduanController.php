<?php

namespace App\Http\Controllers\FrontEnd\InfoPasien;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Mail\Pengaduan as PengaduanMail;
use App\Mail\GratifikasiMail;
use App\Mail\SponsorMail;
use App\Models\Pengaduan\InfoPengaduan;
use App\Models\Pengaduan\ItemPengaduan;
use App\Models\Pengaduan\Pengaduan as Pengaduans;
use App\Models\Pengaduan\PengaduanBukti;
use App\Models\Pengaduan\PengaduanSaksi;
use App\Models\Pengaduan\PengaduanGratifikasi;
use App\Models\Pengaduan\PengaduanGratifikasiBukti;
use App\Models\Pengaduan\PengaduanSponsor;
use App\Models\Pengaduan\PengaduanSponsorBukti;
use App\Models\Setting\Setting;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

use Mail;

class PengaduanController extends FrontEndController
{
    //
    protected $link = 'info-pasien/pengaduan/';
    protected $perms  = '';
    protected $settings = null;

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Pengaduan");
		$this->setBreadcrumb(['Pengaduan' => '#']);
        $this->settings = Setting::all()
                                 ->mapWithKeys(function ($item) {
                                     return [$item->kunci => $item];
                                 });
	}

    public function index($tipe)
    {
        switch ($tipe) {
            case 'etik-hukum':
                return $this->render('frontend.pengaduan.etik-hukum.index', ['mockup' => false]);
            break;
            case 'whistleblowing':
                return $this->render('frontend.pengaduan.whistleblowing.index', ['mockup' => false]);
            break;
            case 'sponsor':
                return $this->render('frontend.pengaduan.sponsor.index', ['mockup' => false]);
            break;
            case 'gratifikasi':
                return $this->render('frontend.pengaduan.gratifikasi.index', ['mockup' => false]);
            break;
            default:
                return $this->render('frontend.pengaduan.masyarakat.index', ['mockup' => false]);
            break;
        }
        
        // $info = InfoPengaduan::first();
        // $record = ItemPengaduan::all();
        // return $this->render('frontend.pengaduan.index', [
        //                      'info' => $info,
        //                      'record' => $record,
        //                      'mockup' => true]);
    }

    public function etikHukum()
    {
        return $this->render('frontend.pengaduan.etik-hukum.index', ['mockup' => false]);
    }

    public function pengaduanMasyarakat()
    {
        return $this->render('frontend.pengaduan.masyarakat.index', ['mockup' => false]);
    }

    public function whistleblowing()
    {
        return $this->render('frontend.pengaduan.whistleblowing.index', ['mockup' => false]);
    }

    public function store(Request $request){
        $row = $request->all();
        $data = new Pengaduans;
        if($file = $request->bukti){
            $row['bukti'] = $file->store('uploads/pengaduan/', 'public');
        }
        $data->fill($row);
        $data->save();

        $to = [
            //'Etik & Hukum'
            $this->settings['pengaduan-email-etik']->isi,
            //'Whistleblowing' 
            $this->settings['pengaduan-email-wbs']->isi,
            //'Pengaduan Masyarakat'
            $this->settings['pengaduan-email-masyarakat']->isi,
        ]; 

        Mail::to($to[$data->tipe])
            // ->cc(['stefanus.pragma@gmail.com', 'budiha44@gmail.com'])
            ->queue(new PengaduanMail($data));
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function storeGratifikasi(Request $request){
        $row = $request->all();
        $data = new PengaduanGratifikasi;
        $data->fill($row);
        $data->save();
        
        if($file = $request->bukti){
            foreach ($request->bukti as $key => $bukti) {
                $path = $bukti->store('uploads/pengaduan/gratifikasi', 'public');
                $bukti = new PengaduanGratifikasiBukti;
                $bukti->id_pengaduan_gratifikasi = $data->id;
                $bukti->file = $path;
                // $bukti->nama = $bukti->getClientOriginalName();
                $bukti->save();
            }
        }

        $to = $this->settings['pengaduan-email-gratifikasi']->isi; 

        Mail::to($to)
            // ->cc(['stefanus.pragma@gmail.com', 'budiha44@gmail.com'])
            ->queue(new GratifikasiMail($data));
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function storeSponsor(Request $request){
        $row = $request->all();
        $data = new PengaduanSponsor;
        $data->fill($row);
        $data->save();
        
        if($file = $request->bukti){
            foreach ($request->bukti as $key => $bukti) {
                $path = $bukti->store('uploads/pengaduan/sponsor', 'public');
                $bukti = new PengaduanSponsorBukti;
                $bukti->id_pengaduan_sponsor = $data->id;
                $bukti->file = $path;
                // $bukti->nama = $bukti->getClientOriginalName();
                $bukti->save();
            }
        }

        $to = $this->settings['pengaduan-email-sponsor']->isi; 

        Mail::to($to)
            // ->cc(['stefanus.pragma@gmail.com', 'budiha44@gmail.com'])
            ->queue(new SponsorMail($data));
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
}
