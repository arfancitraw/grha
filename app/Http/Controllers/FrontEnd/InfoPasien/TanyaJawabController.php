<?php

namespace App\Http\Controllers\FrontEnd\InfoPasien;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;
use App\Models\Dokter\ListDokter;
use App\Models\Dokter\JadwalDokter;
use App\Models\TanyaJawab\TanyaJawab;
use App\Models\TanyaJawab\Detail;
use App\Models\TanyaJawab\Access;
use App\Models\Master\KategoriTanyaJawab;

use App\Http\Requests\InfoPasien\TanyaJawabRequest;

class TanyaJawabController extends FrontEndController
{
    //
    protected $link = '/dokter/tanya-jawab/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Tanya Jawab");
		$this->setBreadcrumb(['Tanya Jawab' => '#']);
	}

    public function index(Request $request)
    {
        $kategori = KategoriTanyaJawab::with([
            'pertanyaan' => function($q){
                $q->where('publish', true);
            }
        ])->get();

        return $this->render('frontend.dokter.tanya-jawab.index',[
            'kategori' => $kategori,
        ]);
    }

    public function detail(Request $request, $id)
    {
        $record = TanyaJawab::with(['detail', 'access'])->whereRaw('slugify(judul) = ?', $id)->first();

        if(is_null($record))
            return abort(404);

        $access = $record->access->where('ip', \Request::ip())->first();
        if(is_null($access)){
            $access = new Access;
        }
        
        $access->ip = \Request::ip();
        $record->access()->save($access);
        
        return $this->render('frontend.dokter.tanya-jawab.detail', [
            'record' => $record
        ]);
    }

    public function store(TanyaJawabRequest $request){
        $row = $request->all();

        $data = new TanyaJawab;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function update(TanyaJawabRequest $request, $tanya_jawab){
        $row = $request->all();
        $data = TanyaJawab::find($tanya_jawab);
        
        $detail = new Detail;
        $detail->fill($row);
        $data->detail()->save($detail);
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
}


