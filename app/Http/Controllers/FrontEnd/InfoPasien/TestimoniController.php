<?php

namespace App\Http\Controllers\FrontEnd\InfoPasien;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;
use App\Models\Testimoni\Testimoni;

class TestimoniController extends FrontEndController
{
    //
    protected $link = '/info-pasien/testimoni';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Testimoni");
		$this->setBreadcrumb(['Testimoni' => '#']);
	}

    public function index(Request $request)
    {
        $record = Testimoni::latest('created_at')
        ->where('publik', '>', 0)
        ->paginate(3);
        //dd($record);
        return $this->render('frontend.info-pasien.testimoni.index', ['mockup' => false, 'record' => $record]);
    }
}


