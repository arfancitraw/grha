<?php

namespace App\Http\Controllers\FrontEnd\InfoPasien;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;
use App\Models\Dokter\ListDokter;
use App\Models\Dokter\JadwalDokter;
use App\Models\TanyaJawab\TanyaJawab;

class TambahanTanyaJawabController extends FrontEndController
{
    //
    protected $link = '/info-pasien/tanya-jawab/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Tanya Jawab");
		$this->setBreadcrumb(['Tanya Jawab' => '#']);
	}

    public function store(Request $request){
        $row = $request->all();
        $data = new TanyaJawab;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
}


