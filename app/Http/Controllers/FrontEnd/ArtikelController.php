<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\MediaCenter\Artikel;

use DB;
use Carbon\Carbon;

class ArtikelController extends FrontEndController
{
    //
    protected $link = '/media/artikel';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $bulan = [];
        for($i = 0; $i<12; $i++){
            $bulan[] = Carbon::now()->subMonths($i);
        }

        $this->setTitle("Berita");
        $this->setBreadcrumb(['Beranda' => '#', 'Berita' => '#']);
        $this->setData([
            'bulan' => $bulan
        ]);
    }

    public function index()
    {
        $record = Artikel::latest('tanggal')->paginate(5);
        return $this->render('frontend.artikel.index', ['mockup' => false, 'record' => $record]);
    }

    public function month($bulan)
    {
        $record = Artikel::whereRaw("to_char(tanggal, 'YYYY-MM') = ?", [$bulan])->latest('tanggal')->paginate(5);
        return $this->render('frontend.artikel.index', ['mockup' => false, 'record' => $record]);
    }

    public function page($slug)
    {
        $latest = Artikel::latest('tanggal')->take(5)->get();
        $record = Artikel::where('slug', $slug)->first();
        if(!$record){
            return abort(404);
        }
        return $this->render('frontend.artikel.detail', [
            'mockup' => false,
            'latest' => $latest,
            'record' => $record
        ]);
    }
}
