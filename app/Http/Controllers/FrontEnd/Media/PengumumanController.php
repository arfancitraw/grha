<?php

namespace App\Http\Controllers\FrontEnd\Media;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;
use App\Models\MediaCenter\Pengumuman;

class PengumumanController extends FrontEndController
{
    //
    protected $link = '/pengumuman';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Pengumuman");
		$this->setBreadcrumb(['Beranda' => '#', 'Media' => '#', 'Pengumuman' => '#']);
	}

    public function index()
    {
        $record = Pengumuman::latest('created_at')->paginate(10);
        return $this->render('frontend.media.pengumuman.index', ['record' => $record] );
    }

    public function detail($id)
    {
        $record = Pengumuman::find($id);
        return $this->render('frontend.media.pengumuman.detail', ['record' => $record]);
    }

    public function page($page)
    {
        return $this->render('frontend.media.pengumuman.'.$page, ['mockup' => true]);
    }
}
