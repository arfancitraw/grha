<?php

namespace App\Http\Controllers\FrontEnd\Media;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;
use App\Models\MediaCenter\Kegiatan;

class KegiatanController extends FrontEndController
{
    //
    protected $link = '/kegiatan';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Kegiatan");
        $this->setBreadcrumb(['Beranda' => '#', 'Media' => '#', 'Kegiatan' => '#']);
    }

    public function index()
    {
        $record = Kegiatan::latest('created_at')->paginate(10);
        return $this->render('frontend.media.kegiatan.index', ['record' => $record] );
    }

    public function detail($id)
    {
        $record = Kegiatan::find($id);
        return $this->render('frontend.media.kegiatan.detail', ['record' => $record]);
    }

    public function page($page)
    {
        return $this->render('frontend.media.kegiatan.'.$page, ['mockup' => true]);
    }
}
