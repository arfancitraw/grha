<?php

namespace App\Http\Controllers\FrontEnd\Media;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;
use App\Models\MediaCenter\Artikel;

class ArtikelController extends FrontEndController
{
    //
    protected $link = '/artikel';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Artikel");
        $this->setBreadcrumb(['Beranda' => '#', 'Media' => '#', 'Artikel' => '#']);
    }

    public function index()
    {
        $record = Artikel::latest('created_at')->paginate(10);
        return $this->render('frontend.media.artikel.index', ['record' => $record] );
    }

    public function detail($id)
    {
        $record = Artikel::find($id);
        return $this->render('frontend.media.artikel.detail', ['record' => $record]);
    }

    public function page($page)
    {
        return $this->render('frontend.media.artikel.'.$page, ['mockup' => true]);
    }
}
