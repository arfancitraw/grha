<?php

namespace App\Http\Controllers\FrontEnd\Media;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;
use App\Models\MediaCenter\Pustaka;
use App\Models\MediaCenter\Penyakit;
use App\Models\MediaCenter\Tindakan;

class PustakaController extends FrontEndController
{
    //
    protected $link = '/pustaka';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Pustaka");
		$this->setBreadcrumb(['Beranda' => '#', 'Media' => '#', 'Pengumuman' => '#']);
	}

    public function index()
    {
        return $this->render('frontend.media.pustaka.index', ['mockup' => true]);
    }

    public function daftar($key)
    {
        $penyakit = Penyakit::whereRaw('LOWER(nama) LIKE ? ', strtolower($key).'%')
                            ->get();
        $tindakan = Tindakan::whereRaw('LOWER(nama) LIKE ? ', strtolower($key).'%')
                            ->get();

        return $this->render('frontend.media.pustaka.list', [
            'penyakit' => $penyakit,
            'tindakan' => $tindakan,
            'keyword'=>$key
        ]);
    }

    public function detail($tipe, $id)
    {
        if($tipe == 'penyakit'){
            $pustaka = Penyakit::find($id);
        
            return $this->render('frontend.media.pustaka.penyakit', [
                'mockup' => false,
                'pustaka' => $pustaka
            ]);
        }

        $pustaka = Tindakan::find($id);
        return $this->render('frontend.media.pustaka.tindakan', [
            'mockup' => false,
            'pustaka' => $pustaka
        ]);
    }

    public function page($page)
    {
        return $this->render('frontend.media.pustaka.'.$page, ['mockup' => true]);
    }
}
