<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\Informasi\Informasi;
use App\Models\Setting\Menu;

class InformasiController extends FrontEndController
{
    //
    protected $link = '/informasi';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Informasi");
		$this->setBreadcrumb(['Informasi' => '#']);
	}

    public function index()
    {
        $menu = Informasi::all();
        return $this->render('frontend.informasi.index', ['mockup' => true, 'menu' => $menu]);
    }

    public function detail($slug)
    {
        $url = url($this->link.'/'.$slug);
        $menu = Menu::where('tautan', $url)->first();
        // dd($url, $menu);
        if(!is_null($menu)){
            $menu = $menu->parent->childs->sortBy('urutan');
        }else{
            $menu = Informasi::orderBy('posisi','asc')->get();
        }
        $record = Informasi::where('slug', $slug)->first();
        return $this->render('frontend.informasi.detail', ['mockup' => true, 'menu' => $menu, 'record' => $record]);
    }

    public function page($page)
    {
        return $this->render('frontend.informasi.'.$page, ['mockup' => true]);
    }
}
