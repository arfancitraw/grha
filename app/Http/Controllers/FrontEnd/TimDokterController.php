<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;
use App\Models\Master\Spesialisasi;
use App\Models\Master\Poli;
use App\Models\Dokter\ListDokter;
use App\Models\Dokter\JadwalDokter;

class TimDokterController extends FrontEndController
{
    //
    protected $link = '/tim-dokter';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Beranda");
		$this->setBreadcrumb(['Tim Dokter' => '#']);
	}

    public function index(Request $request)
    {
        // dd($request->all());
        $spesialisasi = '';
        $nama = '';
        
        try {
            $record = ListDokter::with('creator')->active()->whereNotNull('nama_lengkap')->orderBy('nama');
            $sspesialisasi = Spesialisasi::orderBy('prioritas')
                                         ->whereHas('dokter', function($q){
                                            $q->active();
                                         })
                                         ->get();

            $data = $record->paginate(24);

            if($request->ajax()){
                if($spesialisasi = $request->spesialisasi){
                    $record->where('spesialisasi_id', $spesialisasi);
                }    
                if($nama = $request->nama){
                    $record->where('nama', 'ilike', '%'.$nama.'%');
                }
        
                return $this->render('frontend.tim-dokter.list', [
                    'record' => $record->paginate(24), 
                ]);       
            }
        } catch (\Illuminate\Database\QueryException $ex){ 
            $data = null;
        }

        return $this->render('frontend.tim-dokter.index', [
            'record' => $data, 
            'spesialisasi' => $spesialisasi, 
            'nama' => $nama, 
            'sspesialisasi' => $sspesialisasi
        ]);
    }

    public function detail($id)
    {
        $record = ListDokter::find($id);
        $poli   = Poli::with(['jadwal' => function($q) use ($id){
                          $q->where('list_dokter_id', $id);
                      }])
                      ->where('aktif', 1)
                      ->get();
        // $schedule = JadwalDokter::where('list_dokter_id', $id)
        //                         ->orderBy('jam_mulai_praktek', 'desc')
        //                         ->get();
        return $this->render('frontend.tim-dokter.detail', [
            'record' => $record, 
            'poli' => $poli 
            // 'schedule' => $schedule
        ]);
    }
}


