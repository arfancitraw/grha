<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Schedule\Schedules;
use App\Models\Schedule\SchedulesParticipant;

use Carbon\Carbon;
use Auth;

class TranslateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->key);
        if ($request->key == 'english') {
        
        $request->session()->put('lang', $request->key);
            
        }else{

        $request->session()->put('lang', $request->key);
            
        }
        // dd(\Session::get('lang'));
       // dd($request->session()->get('lang'));

        return response([
            'status' => true,
            'data' => $request->session()->get('lang'),
            'message' => $request->session()->get('lang'),
        ]);
    }
}
