<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\Layanan\Layanan;
use App\Models\Setting\Menu;

class PelayananController extends FrontEndController
{
    protected $link = '/pelayanan';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Beranda");
		$this->setBreadcrumb(['Beranda' => '#']);
	}

    public function index()
    {
        return $this->render('frontend.pelayanan.index', ['mockup' => true]);
    }
    public function detail($slug=null)
    {
        $url = url($this->link.'/'.$slug);
        $menu = Menu::where('tautan', $url)->first();
        if(!is_null($menu)){
            $menu = $menu->parent->childs->sortBy('urutan');
        }else{
            $menu = Layanan::where('layanan_unggulan', 0)->orderBy('posisi')->get();
        }

        $record = Layanan::where('slug', $slug)->first();
        if($record){
            return $this->render('frontend.pelayanan.detail', ['record' => $record, 'menu' => $menu]);
        }
        return abort(404);
    }
}
