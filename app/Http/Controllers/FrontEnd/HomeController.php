<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\Layanan\Layanan;
use App\Models\RujukanNasional\RujukanNasional;
use App\Models\Diklat\JadwalDiklat;
use App\Models\MediaCenter\Artikel;
use App\Models\MediaCenter\Kegiatan;
use App\Models\MediaCenter\Pengumuman;
use App\Models\MediaCenter\Pustaka;
// use App\Models\Master\SertifikatFooter;
use App\Models\Master\Slider;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class HomeController extends FrontEndController
{
    //
    protected $link = '/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Beranda");
		$this->setBreadcrumb(['Beranda' => '#']);
	}

    public function index()
    {
        // $mergedUnggulan = Layanan::where('layanan_unggulan', 1)->get();
        // $mergedUnggulan = $mergedUnggulan->merge(RujukanNasional::where('home', 1)->get());

        $homeLayanan = RujukanNasional::where('home', 1)->get();
        $layananUnggulan = RujukanNasional::where('home', 1)->get();
        // $layananUnggulan = $mergedUnggulan;
        $diklat = JadwalDiklat::all();
        $artikel = Artikel::orderBy('created_at', 'desc')->take(3)->get();
        $kegiatan = Kegiatan::orderBy('created_at', 'desc')->take(3)->get();
        $pengumuman = Pengumuman::orderBy('tanggal', 'desc')->take(3)->get();

        $news = collect($artikel->all());
        $news = $news->merge($kegiatan->all());
        $news = $news->merge($pengumuman->all());

        $pustaka = Pustaka::all();
        // $sertifikatFooter = SertifikatFooter::all();
        $slider = Slider::orderBy('posisi')->get();
        $infobed = $this->infobed();
        return $this->render('frontend.home.index', ['mockup' => false,
                                                     'homeLayanan' => $homeLayanan,
                                                     'layananUnggulan' => $layananUnggulan,
                                                     // 'sertifikatFooter' => $sertifikatFooter,
                                                     'artikel' => $artikel,
                                                     'kegiatan' => $kegiatan,
                                                     'pengumuman' => $pengumuman,
                                                     'news' => $news->sortByDesc('created_at')->take(3)->all(),
                                                     'pustaka' => $pustaka,
                                                     'diklat' => $diklat,
                                                     'infobed' => $infobed,
                                                     'slider' => $slider]);
    }

    public function infobed()
    {
        $url = config('app.api_url').'infobed';
        $client = new Client();
        try {
            $result = $client->get($url);
        } catch (GuzzleException $e) {
            return null;
        }

        return ($result) ? json_decode($result->getBody()) : null;
    }
}
