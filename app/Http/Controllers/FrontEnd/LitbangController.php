<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\Litbang\InfoLitbang;
use App\Models\Litbang\KomiteEtik;
use App\Models\Litbang\ListPublikasi;

class LitbangController extends FrontEndController
{
    //
    protected $link = '/litbang';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Litbang");
		$this->setBreadcrumb(['Litbang' => '#']);
	}

    public function index()
    {
        $info = InfoLitbang::all();
        $komiteEtik = KomiteEtik::all();
        $listPublikasi = ListPublikasi::all();
        return $this->render('frontend.litbang.index', ['mockup' => false,
                                                    'info' => $info,
                                                    'komiteEtik' => $komiteEtik,
                                                    'listPublikasi' => $listPublikasi]);
    }
}
