<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\RujukanNasional\RujukanNasional;
use App\Models\RujukanNasional\RujukanNasionalAgenda;
use App\Models\Dokter\ListDokter;
use DB;

class RujukanNasionalController extends FrontEndController
{
    //
    protected $link = '/rujukan-nasional';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Rujukan Nasional");
		$this->setBreadcrumb(['Beranda' => '#', 'Rujukan Nasional']);
	}

    public function index()
    {
        $record = RujukanNasional::orderBy('id', 'ASC')->get();
        return $this->render('frontend.rujukan-nasional.index', ['mockup' => false, 'record' => $record]);
    }

    public function detail($id) {
        $record = RujukanNasional::with(['dokter' => function($q){
                                    return $q->orderByRaw('trans_rujukan_tim.id asc');
                                 }])
                                 ->where('id', $id)
                                 ->first();
        return $this->render('frontend.rujukan-nasional.detail', ['mockup' => false, 'record' => $record]);
    }

    public function page($slug)
    {
        $record = RujukanNasional::with(['dokter' => function($q){
                                    return $q->orderByRaw('trans_rujukan_tim.id asc');
                                 }])
                                 ->whereRaw('slugify(judul) = ?', [$slug])
                                 ->first();
        return $this->render('frontend.rujukan-nasional.detail', ['mockup' => false, 'record' => $record]);
    }
}
