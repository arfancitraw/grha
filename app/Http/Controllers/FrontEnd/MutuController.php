<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\Master\JenisMutu;
use App\Models\Mutu\InfoMutu;

class MutuController extends FrontEndController
{
    //
    protected $link = '/mutu';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Mutu");
		$this->setBreadcrumb(['Mutu' => '#']);
	}

    public function index()
    {
        return $this->render('frontend.mutu.index', [
            'mockup' => false,
            'indikator' => JenisMutu::with('konten')->has('konten')->get()
        ]);
    }

    public function show(Request $request)
    {
        $periode = $request->periode ?: '';
        $indikator = InfoMutu::where('jenis_mutu', $request->id)
                             ->orderByRaw('periode DESC')
                             ->get();
                             
        return $this->render('frontend.mutu.indikator', ['indikator' => $indikator]);
    }
}
