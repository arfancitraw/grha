<?php

namespace App\Http\Controllers\FrontEnd\Diklat;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\Diklat\JadwalDiklat;

class JadwalDiklatController extends FrontEndController
{
    //
    protected $link = '/diklat/jadwal';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Beranda");
		$this->setBreadcrumb(['Beranda' => '#']);
	}

    public function index()
    {
        $record = JadwalDiklat::get();
        return $this->render('frontend.diklat.jadwal.index', ['record' => $record, 'mockup' => false]);
    }
}
