<?php

namespace App\Http\Controllers\FrontEnd\Diklat;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\Diklat\Info;
use App\Models\Diklat\HasilSeleksi;

class InfoDiklatController extends FrontEndController
{
    //
    protected $link = '/diklat/info-lowongan';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Beranda");
		$this->setBreadcrumb(['Beranda' => '#']);
	}

    public function index()
    {
        $record = Info::get();
        return $this->render('frontend.diklat.info.index', ['mockup' => false, 'record' => $record]);
    }

    public function hasil()
    {
        $record = HasilSeleksi::get();
        return $this->render('frontend.diklat.info.hasil', ['mockup' => false, 'record' => $record]);
    }

    public function detailHasil()
    {
       $record = HasilSeleksi::first();
        return $this->render('frontend.karir.info.hasil', ['mockup' => false, 'record'=> $record]);
    }
}
