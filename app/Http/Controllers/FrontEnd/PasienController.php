<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\Master\Poli;
use App\Models\Dokter\ListDokter as Dokter;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions as Options;
use Carbon\Carbon;

class PasienController extends FrontEndController
{
    protected $link = '/pasien';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Pasien");
		$this->setBreadcrumb(['Beranda' => '#']);
	}

    public function index(Request $request)
    {
        $kd_dr = $request->kd_dr ?: null;
        $kd_dep = $request->kd_dep ?: null;
        $hari = $request->hari ? $request->hari - 1 : null;
        $date = $request->tgllahir ? Carbon::createFromFormat('d/m/Y', $request->tgllahir) : null;
        $pasien = $date ? $this->pasien($request->nomr, $date->format('m/d/Y')) : null;
        // dd($pasien);
        $days = [1, 2, 3, 4, 5, 6, 0];
        $haris = ['SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU', 'MINGGU'];
        
        if(!is_null($hari)){
            unset($days[$hari]);
        }

        return $this->render('frontend.pasien.index', [
            'dokter' => $kd_dr,
            'poli' => $kd_dep,
            'disable_days' => !is_null($hari) ? implode(',', $days) : null,
            'hari' => !is_null($hari) ? $haris[$hari] : null,
            'pasien' => $pasien,
            'tanggal_lahir' => $date ?: null,
            'pilihanPoli'  => Poli::where('aktif', 1)->pluck('nama', 'kode'),
            'pilihanDokter'  => Dokter::pluck('nama', 'kode'),
        ]);
    }

    public function table(Request $request)
    {
        $days = ['SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU', 'MINGGU'];
        $kd_dr = $request->dokter ?: null;
        $kd_dep = $request->poli;
        $tgl_janji = $request->tanggal;
        $date = Carbon::createFromFormat('d/m/Y', $tgl_janji);
        $hari = $days[$date->format('N') - 1];

        $jadwal = $this->jadwal($hari, $kd_dep, $date->format('m/d/Y'), $kd_dr);

        return $this->render('frontend.pasien.table', ['jadwal' => $jadwal, 'hari'=> $hari]);
    }

    public function jadwal($hari, $kd_dep, $tgl_janji, $kd_dr=null)
    {
        $param = [
            'hari' => $hari,
            'kd_dep' => $kd_dep,
            'tgl_janji' => $tgl_janji
        ];
        if($kd_dr != null){
            $param['kd_dr'] = $kd_dr;
        }

        $url = config('app.api_url').'listdrquota';
        $client = new Client();
        $result = $client->post($url, [
            Options::JSON => $param
        ]);

        return ($result) ? json_decode($result->getBody()) : null;
    }

    public function pasien($nomr, $tgllahir)
    {
        $url = config('app.api_url').'pasien';
        $client = new Client();
        $result = $client->post($url, [
            Options::JSON => [
                'nomr' => $nomr,
                'tgllahir' => $tgllahir
            ]
        ]);

        return ($result) ? json_decode($result->getBody()) : null;
    }

    public function perjanjian(Request $request)
    {
        $row = $request->all();
        $date = Carbon::createFromFormat('d/m/Y', $row['tgl_janji']);
        $row['tgl_janji'] = $date->format('m/d/Y');
        unset($row['_token']);

        // return json_encode($row);
        $url = config('app.api_url').'simpan';
        $client = new Client();
        $result = $client->post($url, [
            Options::JSON => $row,
        ]);

        if($result){
            $data = json_decode($result->getBody(), false);
            if($data->status == 'false'){
                return response((array) $data, 422);
            }

            return response((array) $data);
        }

        return response('Gagal menyimpan perjanjian, Tidak dapat mengakses Server', 422);
    }
}
