<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\MediaCenter\Pengumuman;

use DB;
use Carbon\Carbon;

class PengumumanController extends FrontEndController
{
    //
    protected $link = '/media/artikel';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

        $bulan = [];
        for($i = 0; $i<12; $i++){
            $bulan[] = Carbon::now()->subMonths($i);
        }

		$this->setTitle("Berita");
		$this->setBreadcrumb(['Beranda' => '#', 'Berita' => '#']);
        $this->setData([
            'bulan' => $bulan
        ]);
	}

    public function index()
    {
        $record = Pengumuman::latest('created_at')->paginate(5);
        return $this->render('frontend.berita.index', ['mockup' => false, 'record' => $record]);
    }

    public function month($bulan)
    {
        $record = Pengumuman::whereRaw("to_char(tanggal, 'YYYY-MM') = ?", [$bulan])->latest('created_at')->paginate(5);
        return $this->render('frontend.berita.index', ['mockup' => false, 'record' => $record]);
    }

    public function page($slug)
    {
        $latest = Pengumuman::latest('created_at')->take(5)->get();
        $record = Pengumuman::where('slug', $slug)->first();
        if(!$record){
            return abort(404);
        }
        return $this->render('frontend.berita.detail', [
            'mockup' => false,
            'latest' => $latest,
            'record' => $record
        ]);
    }
}
