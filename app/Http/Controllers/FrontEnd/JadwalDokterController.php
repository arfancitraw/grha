<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\Dokter\ListDokter;
use App\Models\Dokter\JadwalDokter;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class JadwalDokterController extends FrontEndController
{
    //
    protected $link = '/jadwal-dokter';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Jadwal Dokter");
        $this->setBreadcrumb(['Beranda' => '#']);
    }

    public function index(Request $request)
    {
        $poli = JadwalDokter::select('kode_departemen', 'nama_departemen')->distinct()->get();
        // $jadwal = collect($this->realtime());
        // dd($jadwal->where('kd_dr', '9104')->all());
        $jadwal = JadwalDokter::when(($p = $request->poli), function ($query) use ($p) {
                                return $query->where('kode_departemen', $p);
                              })
                              ->when(($h = $request->hari), function ($query) use ($h) {
                                return $query->where('hari_praktek', $h-1);
                              })
                              ->when(($d = $request->dokter), function ($query) use ($d) {
                                return $query->whereHas('listdokter', function($q) use ($d){
                                    $q->whereRaw('LOWER(nama) LIKE ? ', '%'.strtolower($d).'%');
                                });
                              })
                              ->orderBy('hari_praktek')
                              ->orderBy('jam_mulai_praktek')
                              ->get();

        return $this->render('frontend.jadwal-dokter.index', [
            'mockup' => true, 
            'poli' => $poli, 
            'jadwal' => $jadwal,
            'selected' => [
                'poli' => $request->poli,
                'hari' => $request->hari,
                'dokter' => $request->dokter,
            ]
        ]);
    }

    public function realtime()
    {
        $url = config('app.api_url').'jadwal_dokter';
        $client = new Client();
        $result = $client->get($url);

        return ($result) ? json_decode($result->getBody(), true) : null;
    }
}
