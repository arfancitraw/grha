<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\MediaCenter\Kegiatan;

use DB;
use Carbon\Carbon;

class KegiatanController extends FrontEndController
{
    //
    protected $link = '/media/artikel';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

        $bulan = [];
        for($i = 0; $i<12; $i++){
            $bulan[] = Carbon::now()->subMonths($i);
        }

		$this->setTitle("Berita");
		$this->setBreadcrumb(['Beranda' => '#', 'Berita' => '#']);
        $this->setData([
            'bulan' => $bulan
        ]);
	}

    public function index()
    {
        $record = Kegiatan::latest('created_at')->paginate(5);
        return $this->render('frontend.kegiatan.index', ['mockup' => false, 'record' => $record]);
    }

    public function month($bulan)
    {
        $record = Kegiatan::whereRaw("to_char(tanggal, 'YYYY-MM') = ?", [$bulan])->latest('created_at')->paginate(5);
        return $this->render('frontend.kegiatan.index', ['mockup' => false, 'record' => $record]);
    }

    public function page($slug)
    {
        $latest = Kegiatan::latest('created_at')->take(5)->get();
        $record = Kegiatan::where('slug', $slug)->first();
        if(!$record){
            return abort(404);
        }
        return $this->render('frontend.kegiatan.detail', [
            'mockup' => false,
            'latest' => $latest,
            'record' => $record
        ]);
    }
}
