<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;

use App\Models\Master\Spesialisasi;
use App\Models\Master\Poli;
use App\Models\Dokter\ListDokter;
use App\Models\Sukaman\Kontak;
use App\Models\Sukaman\Informasi;
use App\Models\Sukaman\Slider;
use App\Models\Sukaman\Setting;
use App\Models\Sukaman\PricingKategori;
use App\Models\Sukaman\PricingDetail;
use App\Models\Sukaman\Pricing;
use App\Models\Sukaman\Fasilitas;
use App\Models\Sukaman\Pelayanan;

class SukamanController extends FrontEndController
{
    protected $link = '/';
    protected $perms  = '';
    protected $code  = '20012';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Beranda");
        $this->setBreadcrumb(['Beranda' => '#']);
    }

    public function index()
    {
        $poli = $this->code;
        $pricing = PricingKategori::with(['tarif' => function($q){
                                            return $q->orderBy('id');
                                        }, 'tarif.detail' => function($q){
                                            return $q->orderBy('urutan')
                                                     ->orderBy('id');
                                        }])
                                  ->orderBy('prioritas','asc')
                                  ->get();
        $info = Informasi::orderBy('created_at', 'desc')->get();
        $slider = Slider::orderBy('urutan', 'asc')->get();
        $dokter = Spesialisasi::with(['dokter' => function($q){
                                return $q->active()->orderBy('nama');
                              }])
                              ->whereHas('dokter', function($q){
                                return $q->active();
                              })
                              ->whereHas('dokter.jadwal', function($q) use ($poli){
                                return $q->where('kode_departemen', $poli);
                              })
                              ->orderBy('prioritas')
                              ->get();
        $setting = Setting::all()
                          ->mapWithKeys(function ($item) {
                              return [$item->kunci => $item];
                          });
        $fasilitas = Fasilitas::orderBy('created_at', 'desc')->get();
        $pelayanan = Pelayanan::orderBy('created_at', 'desc')->get();

        return $this->render('sukaman.home.index', [
            'pricing' => $pricing,
            'info' => $info,
            'slider' => $slider,
            'pelayanan' => $pelayanan,
            'dokter' => $dokter,
            'setting' => $setting,
            'fasilitas' => $fasilitas,
            'poli' => $poli
        ]);
    }

    public function contact(Request $request)
    {
        // dd($request->all());
        $data = new Kontak;
        $data->fill($request->all());
        $data->save();


        return response([
            'status' => true,
        ]);

    }

    public function dokter($id)
    {
        $record = ListDokter::with('keahlian', 'pendidikan', 'spesialisasi')->find($id);
        $poli   = Poli::with(['jadwal' => function($q) use ($id){
                          $q->where('list_dokter_id', $id);
                      }])
                      ->where('aktif', 1)
                      ->where('nama', 'POLI EKSEKUTIF')
                      ->first();

        return $this->render('sukaman.home.modals.dokter', [
            'record' => $record, 
            'poli' => $poli 
        ]);
    }

    public function info($id)
    {
        $record = Informasi::find($id);
        # return $this->render('sukaman.home.info-detail', ['mockup'=>false, 'record' => $record]);
        return $record;
    }
    
    public function service($id)
    {
        $record = Pelayanan::find($id);
        # return $this->render('sukaman.home.info-detail', ['mockup'=>false, 'record' => $record]);
        return $record;
    }

}
