<?php

namespace App\Http\Controllers\FrontEnd\Pasien;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;
use App\Models\PasienDanPengunjung\AlurPendaftaran;
use App\Models\PasienDanPengunjung\AturanPengunjung;

class PasienController extends FrontEndController
{
    //
    protected $link = '/pasien';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Beranda");
		$this->setBreadcrumb(['Beranda' => '#']);
	}

    public function index()
    {
        $record = AlurPendaftaran::get();
        return $this->render('frontend.pasien.index', ['record' => $record]);
    }

    public function rules()
    {
        $record = AturanPengunjung::get();
        return $this->render('frontend.pasien.rules', ['record' => $record]);
    }

    public function page($page)
    {
        return $this->render('frontend.pasien.'.$page, ['mockup' => true]);
    }
}
