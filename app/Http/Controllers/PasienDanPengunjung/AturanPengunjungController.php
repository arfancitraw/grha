<?php

namespace App\Http\Controllers\PasienDanPengunjung;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PasienDanPengunjung\AturanPengunjung;

use App\Http\Requests\PasienDanPengunjung\AturanPengunjungRequest;

use Datatables;

class AturanPengunjungController extends Controller
{
    //
    protected $link = 'backend/pasien-pengunjung/aturan-pengunjung/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Aturan Pengunjung");
		$this->setModalSize("");
		$this->setBreadcrumb(['Pasien dan Pengunjung' => '#', 'Pasien dan Pengunjung' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'judul',
			    'name' => 'judul',
			    'label' => 'Judul',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'keterangan',
			    'name' => 'keterangan',
			    'label' => 'Keterangan',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = AturanPengunjung::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($keterangan = $request->keterangan) {
            $records->where('keterangan', 'ilike', '%' . $keterangan . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.pasien-pengunjung.aturan-pengunjung.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.pasien-pengunjung.aturan-pengunjung.create');
    }

    public function store(AturanPengunjungRequest $request)
    {
    	$jenis = new AturanPengunjung;
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        return AturanPengunjung::find($id);
    }

    public function edit($id)
    {
    	$record = AturanPengunjung::find($id);

        return $this->render('backend.pasien-pengunjung.aturan-pengunjung.edit', ['record' => $record]);
    }

    public function update(AturanPengunjungRequest $request, $id)
    {
    	$jenis = AturanPengunjung::find($id);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function destroy($id)
    {
    	$jenis = AturanPengunjung::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
