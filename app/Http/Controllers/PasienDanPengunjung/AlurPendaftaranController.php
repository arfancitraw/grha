<?php

namespace App\Http\Controllers\PasienDanPengunjung;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PasienDanPengunjung\AlurPendaftaran;

use App\Http\Requests\PasienDanPengunjung\AlurPendaftaranRequest;

use Datatables;

class AlurPendaftaranController extends Controller
{
    //
    protected $link = 'backend/pasien-pengunjung/alur-pendaftaran/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Alur Pendaftaran");
        $this->setModalSize("");
        $this->setBreadcrumb(['Pasien dan Pengunjung' => '#', 'Alur Pendaftaran' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul',
                'searchable' => false,
                'sortable' => true,
                'className'=> 'center aligned'
            ],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
                'className'=> 'center aligned'
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className'=> 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className'=> 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = AlurPendaftaran::with('creator')
                           ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('gambar', function ($record) {
                $string ='';
                if($record->photo){
                    $string = '<img src="'.asset('storage').'/'.$record->photo.'" alt="icon" width="80">';
                }
                return $string;
            })
            ->addColumn('posisi', function ($record) {
                $string ='Kiri';
                if($record->posisi==1){
                    $string = 'Kanan';
                }
                return $string;
            })
            ->addColumn('status', function ($record) {
                $string ='<span class="ui fluid orange label">Draft</span>';
                if($record->status==1){
                    $string = '<span class="ui fluid green label">Published</span>';
                }
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['gambar','action','status'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.pasien-pengunjung.alur-pendaftaran.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.pasien-pengunjung.alur-pendaftaran.create');
    }

    public function store(AlurPendaftaranRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/pasien-pengunjung/alur-pendaftaran', 'public');
            $row['photo'] = $path;
        }

        $data = new AlurPendaftaran;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return AlurPendaftaran::find($id);
    }

    public function edit($id)
    {
        $record = AlurPendaftaran::find($id);

        return $this->render('backend.pasien-pengunjung.alur-pendaftaran.edit', ['record' => $record]);
    }

    public function update(AlurPendaftaranRequest $request, $id)
    {
        $row = $request->all();
        
        $data = AlurPendaftaran::find($id);

        $row['photo'] = $data->photo;

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/pasien-pengunjung/alur-pendaftaran', 'public');
            $row['photo'] = $path;
        }

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
        $jenis = AlurPendaftaran::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
