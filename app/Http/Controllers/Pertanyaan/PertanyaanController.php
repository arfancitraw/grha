<?php

namespace App\Http\Controllers\Pertanyaan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\TanyaJawab\TanyaJawab;
use App\Models\TanyaJawab\Detail;
use App\Http\Requests\Pertanyaan\PertanyaanRequest;

use Datatables;

class PertanyaanController extends Controller
{
    protected $link = 'backend/pertanyaan/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("List Pertanyaan");
        $this->setSubtitle("List Pertanyaan");
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Pertanyaan' => '#', 'List Pertanyaan' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'kategori.nama',
                'name' => 'kategori.nama',
                'label' => 'Kategori',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'konten',
                'name' => 'konten',
                'label' => 'Konten',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'rahasia',
                'name' => 'rahasia',
                'label' => 'Identitas',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'publish',
                'name' => 'publish',
                'label' => 'Publikasi',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '100px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = TanyaJawab::with('creator', 'kategori')
                             ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($kategori = $request->kategori) {
            $records->where('id_kategori', $kategori);
        }

        return Datatables::of($records)
                ->addColumn('num', function ($record) use ($request) {
                    return $request->get('start');
                })
                ->addColumn('created_at', function ($record) {
                    return $record->created_at->format('d/m/Y');
                })
                ->editColumn('rahasia', function($record){
                    return $record->rahasia
                         ? '<div class="ui tiny red horizontal label">Dirahasiakan</div>'
                         : '<div class="ui tiny blue horizontal label">Ditampilkan</div>';
                })
                ->editColumn('publish', function($record){
                    return $record->publish
                         ? '<div class="ui tiny green horizontal label">Ditampilkan</div>'
                         : '<div class="ui tiny orange horizontal label">Disembunyikan</div>';
                })
                ->addColumn('action', function ($record) {
                    $btn = '';
                    if(!$record->publish){
                        //Publish
                        $btn .= $this->makeButton([
                            'type' => 'modal',
                            'class' => 'blue publish icon',
                            'label' => '<i class="upload icon"></i>',
                            'tooltip' => 'Tampilkan ke Publik',
                            'id'   => $record->id
                        ]);
                    }else {
                        //Unpublish
                        $btn .= $this->makeButton([
                            'type' => 'modal',
                            'class' => 'orange unpublish icon',
                            'label' => '<i class="download icon"></i>',
                            'tooltip' => 'Sembunyikan dari Publik',
                            'id'   => $record->id
                        ]);
                    }
                    //Edit
                    $btn .= $this->makeButton([
                        'type' => 'url',
                        'class' => 'teal icon',
                        'label' => '<i class="file text icon"></i>',
                        'tooltip' => 'Detail Tanya Jawab',
                        'target'   => url($this->link.$record->id.'/detail')
                    ]);
                    // Delete
                    $btn .= $this->makeButton([
                        'type' => 'delete',
                        'id'   => $record->id
                    ]);

                    return $btn;
                })
                ->rawColumns(['rahasia', 'publish', 'action'])
                ->make(true);
    }

    public function index()
    {
        return $this->render('backend.pertanyaan.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.pertanyaan.create');
    }

    public function store(PertanyaanRequest $request)
    {
        $data = new Detail;
        $data->fill($request->all());
        $data->internal = 1;
        $data->publish = 1;
        $data->rahasia = 0;
        $data->save();

        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return TanyaJawab::find($id);
    }

    public function updateJawaban(PertanyaanRequest $request)
    {
        $data = Pertanyaan::find($request->id);
        $data->fill($request->all());
        $data->save();

        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function detail($id)
    {
        $record = TanyaJawab::find($id);

        return $this->render('backend.pertanyaan.details', [
            'record' => $record
        ]);
    }

    public function jawaban($id,$ans)
    {

        $record = TanyaJawab::find($id);
        $jawaban = Pertanyaan::where('id',$ans)->first();
        return $this->render('backend.pertanyaan.details-answer', ['record' => $record,'jawaban' => $jawaban]);
    }

    public function update(PertanyaanRequest $request, $id)
    {
        $data = new Pertanyaan;
        $data->fill($request->all());
        $data->save();

        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
        $data = TanyaJawab::find($id);
        $data->delete();

        return response([
            'status' => true,
        ]);
    }

    public function publish($id)
    {
        $status = request()->status;

        $data = TanyaJawab::find($id);
        $data->publish = $status;
        $data->save();

        return response([
            'status' => true,
        ]);
    }

    public function deleteJawaban($id)
    {
        $data = Pertanyaan::find($id);
        $data->delete();

        return response([
            'status' => true,
        ]);
    }

}
