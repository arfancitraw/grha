<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Models\Profil\Profil;
use App\Models\Layanan\Layanan;
use App\Models\Informasi\Informasi;
use App\Models\Master\LinkFooter;
use App\Models\Master\MediaSosial;
use App\Models\Master\SertifikatFooter;
use App\Models\Kuesioner\KuesionerJawaban;
use App\Models\Testimoni\TestimoniPelanggan;
use App\Models\Dokter\JadwalDokter;
use App\Models\Master\Poli;
use App\Models\RujukanNasional\RujukanNasional;
use App\Models\Setting\Menu;
use App\Models\Setting\Setting;

use App\Models\Kuesioner\Periode;

class FrontEndController extends BaseController
{

    private $breadcrumb = ["Home" => "#"];
    private $perms = "";
    private $link = "";
    private $title = "Title";
    private $subtitle = " ";
    private $data = [];

    public function setBreadcrumb($value=[])
    {
        $this->breadcrumb = $value;
    }

    public function pushBreadCrumb($value=[])
    {
        $this->breadcrumb = array_merge($this->breadcrumb, $value);
    }

    public function getBreadcrumb()
    {
        return $this->breadcrumb;
    }

    public function setTableStruct($value=[])
    {
    	$this->tableStruct = $value;
    }

    public function getTableStruct()
    {
    	return $this->tableStruct;
    }

    public function setTitle($value="")
    {
    	$this->title = $value;
    }

    public function getTitle()
    {
    	return $this->title;
    }

    public function setSubtitle($value="")
    {
    	$this->subtitle = $value;
    }

    public function getSubtitle()
    {
    	return $this->subtitle;
    }

    public function setLink($value="")
    {
        $this->link = $value;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setPerms($value="")
    {
        $this->perms = $value;
    }

    public function getPerms()
    {
        return $this->perms;
    }

    public function setData($value=[])
    {
    	$this->data = $value;
    }

    public function getData()
    {
    	return $this->data;
    }

    public function render($view, $additional=[])
    {
        // $mergedUnggulan = Layanan::where('layanan_unggulan', 1)->get();
        // $mergedUnggulan = $mergedUnggulan->merge(RujukanNasional::where('home', 1)->get());
        $poli = JadwalDokter::select('kode_departemen', 'nama_departemen')->distinct()->pluck('nama_departemen', 'kode_departemen');
        $data = array_merge([
            'profil' => Profil::orderBy('posisi','asc')->get(),
            'layananUnggulan' => RujukanNasional::where('home', 1)->orderBy('created_at','asc')->get(),
            // 'layananUnggulan' => $mergedUnggulan,
            'informasi' => Informasi::orderBy('posisi','asc')->get(),
            'layanan' => Layanan::orderBy('posisi','asc')->get(),
            'linkFooter' => LinkFooter::get(),
            'mediaSosial' => MediaSosial::get(),
            'sertifikatFooter' => SertifikatFooter::get(),
            'kuesionerDetail' => Periode::with(['detail' => function($q){ $q->orderBy('id', 'ASC'); }])->where('status', 1)->first(),
            'pilihanPoli'  => $poli, // Poli::where('aktif', 1)->pluck('nama', 'kode'),
            'mainMenu' => Menu::generate(),

    		'breadcrumb' => $this->breadcrumb,
    		'title'		 => $this->title,
            'subtitle'   => $this->subtitle,
            'pageUrl'    => $this->link,
            'pagePerms'  => $this->perms,
            'mockup'     => true,
            'settings'   => Setting::all()
                                   ->mapWithKeys(function ($item) {
                                       return [$item->kunci => $item];
                                   })
    	], $this->data);

        return view($view, array_merge($data, $additional));
    }

    public function nilaiKuesioner(Request $request){
        $row = $request->all();
       // dd($row);
        $data = new KuesionerJawaban;
        $data->fill($request->all());
        $data->save();
        $data->savekepuasanLayanan($data->id,$request->all());
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function nilaiTestimoni(Request $request){
        $row = $request->all();
        //dd($row);
        $data = new TestimoniPelanggan;
        $data->fill($request->all());
        $data->save();

        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

}
