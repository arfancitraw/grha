<?php

namespace App\Http\Controllers\Diklat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Diklat\JadwalDiklat;
use App\Models\Model;

use App\Http\Requests\Diklat\JadwalDiklatRequest;

use Datatables;

class JadwalDiklatController extends Controller
{
    protected $link = 'backend/diklat/jadwal-diklat/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Jadwal Diklat");
        $this->setSubtitle("Diklat");
        $this->setModalSize("small");
        $this->setBreadcrumb(['Diklat' => '#', 'Jadwal Diklat' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "one wide column center aligned",
            ],
            /* --------------------------- */
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned',
            ],
            [
                'data' => 'konten',
                'name' => 'konten',
                'label' => 'Konten',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned',
            ],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned"
            ],
            [
                'data' => 'tanggal',
                'name' => 'tanggal',
                'label' => 'Tanggal',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => 'two wide column center aligned',
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'two wide column center aligned',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = JadwalDiklat::with('creator')->select('*');
        // dd($records);
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
        // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($konten = $request->konten) {
            $records->where('konten', 'ilike', '%' . $konten . '%');
        }

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {

            return $request->get('start');
        })
        ->addColumn('konten', function ($record) {
            $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->konten).'</span>';
            return $string;
        })
        ->addColumn('tanggal', function ($record) {
            $string = strtotime($record->tanggal);
            return date('d F Y', $string);
        })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->format('d F Y');
        })
        ->addColumn('created_by', function ($record) {
            return $record->creator->name;
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            //Edit
            $btn .= $this->makeButton([
                'type' => 'edit-page',
                'id'   => $record->id
            ]);
            // Delete
            $btn .= $this->makeButton([
                'type' => 'delete',
                'id'   => $record->id
            ]);

            return $btn;
        })
        ->rawColumns(['action','konten'])
        ->make(true);
    }

    public function index()
    {
        return $this->render('backend.diklat.jadwal-diklat.index',['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.diklat.jadwal-diklat.create');
    }

    public function store(JadwalDiklatRequest $request)
    {

        $row = $request->all();
        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/diklat', 'public');
            $row['gambar'] = $path;
        }
        $data = new JadwalDiklat;
        $data->fill($row);
        $data->save();

        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return JadwalDiklat::find($id);
    }

    public function edit($id)
    {
        $record = JadwalDiklat::find($id);
        return $this->render('backend.diklat.jadwal-diklat.edit', ['record' => $record]);
    }

    public function update(JadwalDiklatRequest $request, $id)
    {
        $row = $request->all();
        $data = JadwalDiklat::find($id);
        $row['gambar'] = $data->gambar;
        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/diklat', 'public');
            $row['gambar'] = $path;
        }
        $data->fill($row);
        $data->save();

        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
        $diklat = JadwalDiklat::find($id);
        $diklat->fileDelete();
        $diklat->delete();

        return response([
            'status' => true,
        ]);
    }
}
