<?php

namespace App\Http\Controllers\InfoPasien;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\InfoPasien\InfoPasien;
use App\Models\Model;

use App\Http\Requests\InfoPasien\InfoPasienRequest;

use Datatables;

class InfoPasienController extends Controller
{
    //
    protected $link = 'backend/info-pasien/info/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Info Pasien");
        $this->setSubtitle("Info Pasien");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Info Pasien' => '#', 'info' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'judul',
			    'name' => 'judul',
			    'label' => 'Judul',
			    'searchable' => false,
			    'sortable' => true,
                'className' => "left aligned",
                'width' => '100px',
			],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'className' => "left aligned",
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
    {
        $records = InfoPasien::with('creator')->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($keterangan = $request->keterangan) {
            $records->where('keterangan', 'ilike', '%' . $keterangan . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['action','konten'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.info-pasien.info.index',['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.info-pasien.info.create');
    }

    public function store(InfoPasienRequest $request)
    {
        // dd($request->all());
        $row = $request->all();
        $row['gambar'] = $request->photo;
        
        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/info-pasien', 'public');
            $row['gambar'] = $path;
        }

        $data = new InfoPasien;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return InfoPasien::find($id);
    }

    public function edit($id)
    {
        // dd($id);
        $record = InfoPasien::find($id);
        // dd($record);
        return $this->render('backend.info-pasien.info.edit', ['record' => $record]);
    }

    public function update(InfoPasienRequest $request, $id)
    {

        // dd($request->all());
        $row = $request->all();
        $data = InfoPasien::find($id);
        $row['gambar'] = $data->gambar;
        $row['slug'] = $this->setSlugify($request->judul);
        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/info-pasien', 'public');
            $row['gambar'] = $path;
        }

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
        $pasien = InfoPasien::find($id);
        $pasien->fileDelete();
        $pasien->delete();

        return response([
            'status' => true,
        ]);
    }
}
