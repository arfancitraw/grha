<?php

namespace App\Http\Controllers\InfoPasien;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\InfoPasien\Testimoni;
use App\Models\Model;

use App\Http\Requests\InfoPasien\TestimoniRequest;

use Datatables;

class TestimoniController extends Controller
{
    //
    protected $link = 'backend/info-pasien/testimoni/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Testimoni");
        $this->setSubtitle("Info Pasien");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Info Pasien' => '#', 'testimoni' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "one wide column center aligned",
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'layanan_yang_digunakan',
                'name' => 'layanan_yang_digunakan',
                'label' => 'Layanan',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'publik',
                'name' => 'publik',
                'label' => 'Publik',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'two wide column center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'two wide column center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
    {
        $records = Testimoni::with('creator')->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($publik = $request->publik) {
            $records->where('publik', 'ilike', '%' . $email . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('publik', function ($record) {
                if($record->publik == 1) {
                    $string = "<span class='ui green label'>Publik</span>";
                }else{
                    $string = "<span class='ui red label'>Private</span>";
                }
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('layanan_yang_digunakan', function ($record) {
                return $record->Layanan->judul;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['action','konten', 'publik'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.info-pasien.testimoni.index',['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.info-pasien.testimoni.create');
    }

    public function store(TestimoniRequest $request)
    {
        // dd($request->all());
        $row = $request->all();

        $data = new Testimoni;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return Testimoni::find($id);
    }

    public function edit($id)
    {
        // dd($id);
        $record = Testimoni::find($id);
        // dd($record);
        return $this->render('backend.info-pasien.testimoni.edit', ['record' => $record]);
    }

    public function update(TestimoniRequest $request, $id)
    {
        $row = $request->all();
        $data = Testimoni::find($id);

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
        $pasien = Testimoni::find($id);
        $pasien->fileDelete();
        $pasien->delete();

        return response([
            'status' => true,
        ]);
    }
}
