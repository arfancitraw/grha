<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Setting\Menu;
use App\Models\Profil\Profil;
use App\Models\Layanan\Layanan;
use App\Models\Informasi\Informasi;
// use App\Http\Requests\Setting\MenuRequest;
use Datatables;

class MenuController extends Controller
{
    protected $link = 'backend/setting/menu/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Pengaturan Menu");
        $this->setSubtitle("Pengaturan Menu pada Website");
        $this->setBreadcrumb(['Pengaturan Website' => '#', 'Pengaturan Menu' => '#']);
    }

    public function index()
    {
        $data['halaman'] = [
            [
                'judul' => 'Layanan Unggulan',
                'tautan' => 'layanan-unggulan',
            ],
            [
                'judul' => 'Indikator Mutu',
                'tautan' => url('mutu'),
            ],
            [
                'judul' => 'Tim Dokter',
                'tautan' => url('tim-dokter'),
            ],
            [
                'judul' => 'Jadwal Dokter',
                'tautan' => url('jadwal-dokter'),
            ],
            [
                'judul' => 'Rujukan Nasional',
                'tautan' => url('rujukan-nasional'),
            ],
            [
                'judul' => 'Diklat',
                'tautan' => url('diklat'),
            ],
            [
                'judul' => 'Litbang',
                'tautan' => url('litbang'),
            ],
            [
                'judul' => 'Tanya Jawab',
                'tautan' => url('dokter/tanya-jawab'),
            ],
            [
                'judul' => 'Pengaduan',
                'tautan' => url('info-pasien/pengaduan'),
            ],
            [
                'judul' => 'Testimoni',
                'tautan' => url('info-pasien/testimoni'),
            ],
        ];
        $data['profil']  = Profil::all();
        $data['layanan'] = Layanan::all();
        $data['informasi'] = Informasi::all();
        $data['record']   = Menu::generate();

        return $this->render('backend.setting.menu.index', $data);
    }

    public function store(Request $request)
    {
        try {
            $data = new Menu;
            $data->fill($request->all());
            $data->urutan = Menu::findPlace();
            $data->save();
        } catch (QueryException $e) {
            return response([
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        }

        return response(['data' => $data]);
    }

    public function show($id)
    {
        $data = Menu::find($id);
        
        return response(['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        try {
            $data = Menu::find($id);
            $data->fill($request->all());
            $data->save();
        } catch (QueryException $e) {
            return response([
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 
        
        return response(['data' => $data]);
    }

    public function destroy($id)
    {
        try {
            $data = Menu::find($id);
            $data->delete();
        } catch (QueryException $e) {
            return response([
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response(['data' => $data]);
    }

    public function change($id)
    {
        try {
            $data = Menu::find($id);
            $data->right = !$data->right;
            $data->save();
        } catch (QueryException $e) {
            return response([
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response(['data' => $data]);
    }

    public function save(Request $request)
    {
        $this->insert($request->data);
        return response(['message'=>'success']);
    }

    public function load()
    {
        $this->data['data']   = Menu::generate();
        return $this->render('backend.setting.menu.list', $this->data);
    }

    private function insert($data, $induk_id=null)
    {
        foreach ($data as $idx => $row) {
            $menu = Menu::find($row['id']);
            $menu->urutan = $idx + 1;
            $menu->induk_id = $induk_id;
            $menu->save();

            if(array_key_exists('children', $row)){
                $this->insert($row['children'], $menu->id);
            }
        }
    }

}
