<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Http\Requests\Setting\UserRequest;
use Datatables;

class UserController extends Controller
{
    protected $link = 'backend/setting/user/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Manajemen Pengguna");
        $this->setSubtitle("Pengguna");
        $this->setModalSize("small");
        $this->setBreadcrumb(['Pengaturan Website' => '#', 'Manajemen Pengguna' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'name',
                'name' => 'name',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '70px'
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = User::select('*');

        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->sort();
            $records->orderBy('created_at');
        }

        // Filters
        if ($nama = $request->nama) {
            $records->where('name', 'ilike', '%'.$nama.'%');
        }

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->addColumn('created_at', function ($record) {
            return is_null($record->created_at) 
                 ? '-'
                 : $record->created_at->format('d F Y');
        })
        ->addColumn('action', function ($record) {
            $btn = '';
            //Edit
            $btn .= $this->makeButton([
                'type' => 'edit',
                'id'   => $record->id
            ]);
            // Delete
            $btn .= $this->makeButton([
                'type' => 'delete',
                'id'   => $record->id
            ]);

            return $btn;
        })
        ->rawColumns(['gambar','action','status'])
        ->make(true);
    }

    public function index()
    {
        return $this->render('backend.setting.user.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.setting.user.create');
    }

    public function store(UserRequest $request)
    {
        $row = $request->all();

        $data = new User;
        $row['password'] = bcrypt($request->password);
        $data->fill($row);
        $data->save();

        $data->attachRole($request->role_id);

        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function show($id)
    {
        return User::find($id);
    }

    public function edit($id)
    {
        $record = User::find($id);

        return $this->render('backend.setting.user.edit', ['record' => $record]);
    }

    public function update(Request $request, $id)
    {
        if ($request->password == null) {
          
            $row = $request->all();
            $data = User::find($id);
            $row['password'] = $data->password;
            $data->fill($row);
            $data->save();

        }else{
            $row = $request->all();
            $data = User::find($id);
            $row['password'] = bcrypt($request->password);
            $data->fill($row);
            $data->save();

            }

        $data->roles()->sync([$request->role_id]);

        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
        $jenis = User::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
