<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Setting\Setting;

use App\Http\Requests\Setting\JumbotronRequest;

use Datatables;
use Storage;

class JumbotronController extends Controller
{
    //
    protected $link = 'backend/setting/jumbotron/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Pengaturan Gambar");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Pengaturan Website' => '#', 'Pengaturan Gambar' => '#']);
    }

    public function index()
    {
        $seting = Setting::orderBy('id', 'asc')->where('kunci', 'like', 'jumbotron-%')->get();
        $settings = $seting->mapWithKeys(function ($item) {
                               return [$item->kunci => $item];
                           });
        
        return $this->render('backend.setting.jumbotron.index', ['mockup' => false,
            'seting' => $seting,
            'settings' => $settings
        ]);
    }

    public function create()
    {
        return $this->render('backend.setting.jumbotron.create');
    }

    public function store(Request $request)
    {
        foreach ($request->setting as $id => $value) {
            if(!is_null($value['isi'])){
                $path = $value['isi']->store('uploads/settings/jumbotron', 'public');

                $row = Setting::find($id);
                $row->isi = $path;
                $row->save();
            }
        }
        
        return response([
            'status' => true
        ]);

    }

}
