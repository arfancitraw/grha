<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
// use App\Http\Requests\Setting\ProfileRequest;
use Datatables;

class ProfileController extends Controller
{
    protected $link = '/profile';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Profil Pengguna");
        $this->setSubtitle("Pengaturan Profil Pengguna");
        $this->setBreadcrumb(['Profil Pengguna' => '#']);
    }

    public function index()
    {
        $record = auth()->user();
        return $this->render('backend.setting.profile.index', compact('record'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'                  => 'required',
            'email'                 => 'required|email',
            'password'              => 'confirmed',
            'password_confirmation' => 'same:password'
        ]);

        $row = $request->all();

        try {
            $user = auth()->user();
            $user->fill($row);

            if($request->hasFile('photo')){
                $path = $request->file('photo')->store('uploads/users/', 'public');
                $user->photo = $path;
            }

            if($pass = $request->old_password){
                if(\Hash::check($pass, $user->password)){
                    $user->password = bcrypt($request->password);
                }else{
                    return response([
                        'old_password' => [
                            'Password yang dimasukkan tidak sesuai'
                        ]
                    ], 422);
                }
            }
            
            $user->save();
        } catch (QueryException $e) {
            return response([
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        }

        return response(['user' => $user]);
    }

}
