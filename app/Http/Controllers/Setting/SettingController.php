<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Setting\Setting;

use App\Http\Requests\Setting\SettingRequest;

use Datatables;
use Storage;

class SettingController extends Controller
{
    //
    protected $link = 'backend/setting/setting/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Pengaturan Website");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Pengaturan Website' => '#', 'Setting' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'kunci',
                'name' => 'kunci',
                'label' => 'Kunci',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'isi',
                'name' => 'isi',
                'label' => 'Isi',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
                'width' => '20%'
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }

    public function index()
    {
        $seting = Setting::orderBy('id', 'asc')->get();
        $settings = Setting::all()
                           ->mapWithKeys(function ($item) {
                               return [$item->kunci => $item];
                           });
        return $this->render('backend.setting.setting.index', ['mockup' => false,
            'seting' => $seting,
            'settings' => $settings
        ]);
    }

    public function create()
    {
        return $this->render('backend.setting.setting.create');
    }

    public function store(Request $request)
    {
        foreach ($request->setting as $id => $value) {
            // dd($value);
            $row = Setting::find($id);
            $row->fill($value);
            $row->save();
        }
        
        return response([
            'status' => true
        ]);

    }

}
