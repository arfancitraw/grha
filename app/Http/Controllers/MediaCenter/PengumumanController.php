<?php

namespace App\Http\Controllers\MediaCenter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\MediaCenter\Pengumuman;

use App\Http\Requests\MediaCenter\PengumumanRequest;

use Datatables;

class PengumumanController extends Controller
{
    //
    protected $link = 'backend/media-center/pengumuman/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Berita");
		$this->setModalSize("");
		$this->setBreadcrumb(['Media Center' => '#', 'Berita' => url($this->link)]);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => 'No.',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'kategori.nama',
                'name' => 'kategori.nama',
                'label' => 'Kategori',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'tanggal',
                'name' => 'tanggal',
                'label' => 'Tanggal Berita',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '70px',
			]
		]);
	}

	public function grid(Request $request)
    {
        $records = Pengumuman::with('creator', 'kategori')
                           ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderByRaw('tanggal desc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($keterangan = $request->keterangan) {
            $records->where('keterangan', 'ilike', '%' . $keterangan . '%');
        }
        if ($kategori = $request->kategori) {
            $records->where('kategori_id', $kategori);
        }

        $link = $this->link;

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('tanggal', function ($record) {
                return $record->tanggal ? $record->tanggal->format('d F Y') : '-' ;
            })
            ->editColumn('kategori.nama', function ($record) {
                return $record->kategori ? $record->kategori->nama : '-' ;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            // ->addColumn('konten', function ($record) {
            //     $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->konten).'</span>';
            //     return $string;
            // })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) use ($link) {
                $btn = '';
                //Download
                // if ($record->lampiran != null && $record->lampiran != '' ) {
                //     $btn .= $this->makeButton([
                //         'type' => 'url',
                //         'class' => 'green icon',
                //         'label' => '<i class="download icon"></i>',
                //         'target' => url('storage/'.$record->lampiran),

                //     ]);
                //      # code...
                //  }else{
                //     $btn .= $this->makeButton([
                //         'type' => 'url',
                //         'class' => 'grey icon disabled',
                //         'label' => '<i class="download icon"></i>',
                //         'target' => url('#'),
                //     ]);
                //  }
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['gambar','action','status','konten'])
            ->make(true);
    }

    public function index()
    {
        $this->setTitle('List Berita');
        return $this->render('backend.media-center.pengumuman.index', ['mockup' => false]);
    }

    public function create()
    {
        $this->setTitle('Tambah Berita');
        $this->pushBreadcrumb(['Tambah Berita' => '#']);
        return $this->render('backend.media-center.pengumuman.create');
    }

    public function store(PengumumanRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/media-center/pengumuman', 'public');
            $row['photo'] = $path;
        }

        if($request->hasFile('lampiran')){
            $path = $request->file('lampiran')->store('uploads/media-center/pengumuman', 'public');
            $row['lampiran'] = $path;
        }

        $data = new Pengumuman;
        $data->fill($row);
        $data->save();

        return redirect($this->link);
    }

    public function show($id)
    {
        return Pengumuman::find($id);
    }

    public function edit($id)
    {
        $this->setTitle('Ubah Berita');
        $this->pushBreadcrumb(['Ubah Berita' => '#']);
        $record = Pengumuman::find($id);

        return $this->render('backend.media-center.pengumuman.edit', ['record' => $record]);
    }

    public function update(PengumumanRequest $request, $id)
    {
        $row = $request->all();

        $data = Pengumuman::find($id);

        $row['lampiran'] = $data->lampiran;
        $row['photo'] = $data->photo;

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/media-center/pengumuman', 'public');
            $row['photo'] = $path;
        }

        if($request->hasFile('lampiran')){
            $path = $request->file('lampiran')->store('uploads/media-center/pengumuman', 'public');
            $row['lampiran'] = $path;
        }

        $data->fill($row);
        $data->save();

        return redirect($this->link);
    }

    public function destroy($id)
    {
        $jenis = Pengumuman::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
