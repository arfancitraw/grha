<?php

namespace App\Http\Controllers\MediaCenter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\MediaCenter\Artikel;

use App\Http\Requests\MediaCenter\ArtikelRequest;

use Datatables;
use Storage;

class ArtikelController extends Controller
{
    //
    protected $link = 'backend/media-center/artikel/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Artikel");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Media Center' => '#', 'Artikel' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul Artikel',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'kategori_artikel.nama',
                'name' => 'KategoriArtikel.nama',
                'label' => 'Kategori Artikel',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
            ],
            // [
            //     'data' => 'slug',
            //     'name' => 'slug',
            //     'label' => 'Slug',
            //     'searchable' => false,
            //     'sortable' => true,
            //     'width' => '120px',
            // ],
            // [
            //     'data' => 'konten',
            //     'name' => 'konten',
            //     'label' => 'Konten',
            //     'searchable' => false,
            //     'sortable' => true,
            //     'className' => "left aligned",
            //     'width' => '20%'
            // ],
            [
                'data' => 'tanggal',
                'name' => 'tanggal',
                'label' => 'Tanggal Artikel',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
            ],
            // [
            //     'data' => 'photo',
            //     'name' => 'photo',
            //     'label' => 'Photo',
            //     'searchable' => false,
            //     'sortable' => true,
            // ],
                // 'className' => 'center aligned'
            // ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Artikel::with('creator','KategoriArtikel')
                           ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('kategori_artikel.nama', function ($record) {
                return $record->KategoriArtikel ? $record->KategoriArtikel->nama : '-';
            })
            ->editColumn('tanggal', function ($record) {
                return $record->tanggal ? $record->tanggal->format('d F Y') : '-' ;
            })

            ->addColumn('konten', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->konten).'</span>';
                return $string;
            })

            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['gambar','action','status','konten'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.media-center.artikel.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.media-center.artikel.create');
    }

    public function store(ArtikelRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/media-center/artikel', 'public');
            $row['photo'] = $path;
        }

        $data = new Artikel;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return Artikel::find($id);
    }

    public function edit($id)
    {
        $record = Artikel::find($id);

        return $this->render('backend.media-center.artikel.edit', ['record' => $record]);
    }

    public function update(ArtikelRequest $request, $id)
    {
        $row = $request->all();
        
        $data = Artikel::find($id);

        $row['photo'] = $data->photo;

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/media-center/artikel', 'public');
            $row['photo'] = $path;
        }
        $data->fill($row);
        $data->save();
        
        return redirect($this->link);


    }

public function removeImage($id)
{
    $data = Artikel::find($id);
    Storage::delete($data->photo);
    $data->photo = null;
    $data->save();

    return response([
    'status' => true,
    'data'  => $data
    ]);
}
    public function destroy($id)
    {
        $jenis = Artikel::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
