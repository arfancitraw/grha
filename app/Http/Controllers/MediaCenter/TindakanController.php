<?php

namespace App\Http\Controllers\MediaCenter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\MediaCenter\Tindakan;

use App\Http\Requests\MediaCenter\TindakanRequest;

use Datatables;

class TindakanController extends Controller
{
    //
    protected $link = 'backend/media-center/pustaka/tindakan/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Tindakan");
        $this->setModalSize("");
        $this->setBreadcrumb(['Media Center' => '#', 'Pustaka' => '#', 'Tindakan' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => 'No.',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Tindakan',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'indikasi',
                'name' => 'indikasi',
                'label' => 'Indikasi',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Tindakan::with('creator')
                           ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($indikasi = $request->indikasi) {
            $records->where('indikasi', 'ilike', '%' . $indikasi . '%');
        }

        $link = $this->link;

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('indikasi', function ($record) {
                $text = strip_tags($record->indikasi);
                $text = trim(preg_replace('/\s+/', ' ', $text));
                return substr($text, 0, 200).'...';
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('konten', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->konten).'</span>';
                return $string;
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) use ($link) {
                $btn = '';
                //Download
                if ($record->lampiran != null && $record->lampiran != '' ) {
                    $btn .= $this->makeButton([
                        'type' => 'url',
                        'class' => 'green icon',
                        'label' => '<i class="download icon"></i>',
                        'target' => url('storage/'.$record->lampiran),
                      
                    ]);
                     # code...
                 }else{
                    $btn .= $this->makeButton([
                        'type' => 'url',
                        'class' => 'grey icon disabled',
                        'label' => '<i class="download icon"></i>',
                        'target' => url('#'),
                    ]);
                 }
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'url',
                    'class' => 'orange icon',
                    'label' => '<i class="edit icon"></i>',
                    'target' => url($link. $record->id . '/edit'),
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['gambar','action','status','konten'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.media-center.tindakan.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.media-center.tindakan.create');
    }

    public function store(TindakanRequest $request)
    {
        $row = $request->all();
        
        if($request->hasFile('lampiran')){
            $path = $request->file('lampiran')->store('uploads/media-center/tindakan', 'public');
            $row['lampiran'] = $path;
        }

        $data = new Tindakan;
        $data->fill($row);
        $data->save();
        
        return redirect($this->link);
    }

    public function show($id)
    {
        return Tindakan::find($id);
    }

    public function edit($id)
    {
        $record = Tindakan::find($id);

        return $this->render('backend.media-center.tindakan.edit', ['record' => $record]);
    }

    public function update(TindakanRequest $request, $id)
    {
        $row = $request->all();
        
        $data = Tindakan::find($id);

        $row['lampiran'] = $data->lampiran;

        if($request->hasFile('lampiran')){
            $path = $request->file('lampiran')->store('uploads/media-center/tindakan', 'public');
            $row['lampiran'] = $path;
        }

        $data->fill($row);
        $data->save();
        
        return redirect($this->link);
    }

    public function destroy($id)
    {
        $jenis = Tindakan::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
