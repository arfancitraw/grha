<?php

namespace App\Http\Controllers\MediaCenter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\MediaCenter\Kegiatan;
use App\Models\Master\TipeMedia;

use App\Http\Requests\MediaCenter\KegiatanRequest;

use Datatables;

class KegiatanController extends Controller
{
    //
    protected $link = 'backend/media-center/kegiatan/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Kegiatan");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Media Center' => '#', 'Kegiatan' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'judul',
			    'name' => 'judul',
			    'label' => 'Judul Kegiatan',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
            [
                'data' => 'tanggal',
                'name' => 'tanggal',
                'label' => 'Tanggal Kegiatan',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '70px',
            ]
		]);
	}

	public function grid(Request $request)
	{
		$records = Kegiatan::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($keterangan = $request->keterangan) {
            $records->where('keterangan', 'ilike', '%' . $keterangan . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('tanggal', function ($record) {
                return $record->tanggal ? $record->tanggal->format('d F Y') : '-' ;
            })
            ->addColumn('keterangan', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->keterangan).'</span>';
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit-page',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['keterangan','action'])
            ->make(true);
	}

    public function index()
    {
        $this->setTitle('List Kegiatan');
        return $this->render('backend.media-center.kegiatan.index', ['mockup' => false]);
    }

    public function create()
    {
        $this->setTitle('Tambah Kegiatan');
        $this->pushBreadcrumb(['Tambah Kegiatan' => '#']);
        return $this->render('backend.media-center.kegiatan.create');
    }

    public function store(KegiatanRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/media-center/kegiatan', 'public');
            $row['photo'] = $path;
        }

        $data = new Kegiatan;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function show($id)
    {
        return Kegiatan::find($id);
    }

    public function edit($id)
    {
        $this->setTitle('Ubah Kegiatan');
        $this->pushBreadcrumb(['Ubah Kegiatan' => '#']);
    	$record = Kegiatan::find($id);

        return $this->render('backend.media-center.kegiatan.edit', ['record' => $record]);
    }

    public function update(KegiatanRequest $request, $id)
    {
        $row = $request->all();
        
        $data = Kegiatan::find($id);

        $row['photo'] = $data->photo;

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/media-center/pengumuman', 'public');
            $row['photo'] = $path;
        }

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
    	$jenis = Kegiatan::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
