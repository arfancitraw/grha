<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\Models\TanyaJawab\TanyaJawab;
use App\Models\MediaCenter\Artikel;
use App\Models\MediaCenter\Kegiatan;
use App\Models\MediaCenter\Pengumuman;
use App\Models\Kuesioner\Periode;
use App\Models\Pengaduan\Pengaduan;
use App\Models\Pengaduan\PengaduanGratifikasi;
use App\Models\Pengaduan\PengaduanSponsor;

class HomeController extends Controller
{
    protected $link = 'home/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Dashboard Website");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = TanyaJawab::orderBy('created_at')
                                ->get();
        $kuesioner = Periode::with(['isian' => function($q){
                                return $q->orderBy('created_at');
                            }])
                            ->where('status', 1)
                            ->first();
        $pengaduan  = Pengaduan::orderBy('created_at')
                              ->get();
        $pengaduanGrat = PengaduanGratifikasi::orderBy('created_at')
                                             ->get();
        $pengaduanSpon = PengaduanSponsor::orderBy('created_at')
                                         ->get();
        $counts = [
            'perjanjian' => '?',
            'tanya-jawab' => $pertanyaan->where('publish', false)->count(),
            'kuesioner' => 0,
            'pengaduan' => $pengaduan->where('created_at', '>', Carbon::now()->subDays(7))
                                     ->count() + 
                           $pengaduanGrat->where('created_at', '>', Carbon::now()->subDays(7))
                                         ->count() +
                           $pengaduanSpon->where('created_at', '>', Carbon::now()->subDays(7))
                                         ->count(),
        ];

        $allPengaduan = collect($pengaduan->merge($pengaduanGrat))->merge($pengaduanSpon);
        return $this->render('home', [
            'counts' => $counts,
            'tanyajawab' => $pertanyaan,
            'kuesioner' => $kuesioner,
            'pengaduan' => $allPengaduan->sortByDesc('created_at'),
            'pengaduanGrat' => $pengaduanGrat,
            'pengaduanSpon' => $pengaduanSpon,
        ]);
    }
}
