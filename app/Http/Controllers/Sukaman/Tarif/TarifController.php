<?php

namespace App\Http\Controllers\Sukaman\Tarif;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Sukaman\Pricing;

use App\Http\Requests\Sukaman\PricingRequest;

use Datatables;
use Storage;

class TarifController extends Controller
{
    protected $link = 'backend/sukaman/pricing/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Kategori Pricing Paviliun Sukaman");
        
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Paviliun Sukaman' => '#', 'Pricing' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
            ],
            [
                'data' => 'prioritas',
                'name' => 'prioritas',
                'label' => 'Prioritas',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
            ],

            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }
    
    public function index()
    {
        return $this->render('backend.sukaman.pricing-kategori.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.sukaman.pricing-kategori.create');
    }

    public function pricing()
    {        
        $this->setTitle("Pricing di Paviliun Sukaman");
        return $this->render('backend.sukaman.pricing-kategori.pricing');
    }
    
    public function tarif()
    {        
        $record = 1;

        return $this->render('backend.sukaman.pricing-kategori.tambah-tarif', ['record' => $record]);
    }

    public function store(TarifRequest $request)
    {
        $row = $request->all();

        $data = new Tarif;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function storeTarif(PricingRequest $request)
    {
        $row = $request->all();

        $data = new Pricing;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function edit($id)
    {
        $this->setTitle("Detail Kategori Pricing Sukaman");
        $record = Tarif::find($id);

        return $this->render('backend.sukaman.pricing-kategori.edit', ['record' => $record]);
    }

    public function update(TarifRequest $request, $id)
    {
        $row = $request->all();
        
        $data = Tarif::find($id);

        $row['icon'] = $data->icon;

        if($request->hasFile('icon')){
            $path = $request->file('icon')->store('uploads/sukaman/artikel', 'public');
            $row['icon'] = $path;
        }
        $data->fill($row);
        $data->save();
        
        return redirect($this->link);


    }
    public function destroy($id)
    {
        $jenis = Tarif::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
