<?php

namespace App\Http\Controllers\Sukaman;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Sukaman\PricingKategori;
use App\Models\Sukaman\Pricing;

use App\Http\Requests\Sukaman\PricingKategoriRequest;
use App\Http\Requests\Sukaman\PricingRequest;

use Datatables;
use Storage;

class KategoriController extends Controller
{
    protected $link = 'backend/sukaman/pricing-kategori/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Kategori Pricing Paviliun Sukaman");
        
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Paviliun Sukaman' => '#', 'Pricing' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
            ],
            [
                'data' => 'prioritas',
                'name' => 'prioritas',
                'label' => 'Prioritas',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
            ],

            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = PricingKategori::with('creator')
                           ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);
                return $btn;
            })
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.sukaman.pricing-kategori.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.sukaman.pricing-kategori.create');
    }

    public function pricing()
    {        
        $this->setTitle("Pricing di Paviliun Sukaman");
        return $this->render('backend.sukaman.pricing-kategori.pricing');
    }
    
    public function tarif()
    {        
        $record = 1;

        return $this->render('backend.sukaman.pricing-kategori.tambah-tarif', ['record' => $record]);
    }

    public function store(PricingKategoriRequest $request)
    {
        $row = $request->all();

        $data = new PricingKategori;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function storeTarif(PricingRequest $request)
    {
        $row = $request->all();

        $data = new Pricing;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function edit($id)
    {
        $this->setTitle("Detail Kategori Pricing Sukaman");
        $record = PricingKategori::find($id);

        return $this->render('backend.sukaman.pricing-kategori.edit', ['record' => $record]);
    }

    public function update(PricingKategoriRequest $request, $id)
    {
        $row = $request->all();
        
        $data = PricingKategori::find($id);

        $row['icon'] = $data->icon;

        if($request->hasFile('icon')){
            $path = $request->file('icon')->store('uploads/sukaman/artikel', 'public');
            $row['icon'] = $path;
        }
        $data->fill($row);
        $data->save();
        
        return redirect($this->link);


    }
    public function destroy($id)
    {
        $jenis = PricingKategori::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
