<?php

namespace App\Http\Controllers\Sukaman;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Sukaman\Kontak;

use App\Http\Requests\Sukaman\KontakRequest;

use Datatables;
use Storage;

class KontakController extends Controller
{
    //
    protected $link = 'backend/sukaman/kontak/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Paviliun Sukaman Kontak");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Paviliun Sukaman' => '#', 'Kontak' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'perihal',
                'name' => 'perihal',
                'label' => 'Perihal',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
                'width' => '20%'
            ],
            [
                'data' => 'pesan',
                'name' => 'pesan',
                'label' => 'Pesan',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
                'width' => '20%'
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Kontak::with('creator')
        ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })

        ->addColumn('created_at', function ($record) {
            return $record->created_at->format('d F Y');
        })

        ->addColumn('action', function ($record) {
            $btn = '';
                //Edit
            $btn .= $this->makeButton([
                'type' => 'details-page',
                'id'   => $record->id
            ]);
                // Delete
            $btn .= $this->makeButton([
                'type' => 'delete',
                'id'   => $record->id
            ]);

            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function index()
    {
        return $this->render('backend.sukaman.kontak.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.sukaman.kontak.create');
    }

    public function store(KontakRequest $request)
    {
        $row = $request->all();
        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/sukaman/kontak', 'public');
            $row['gambar'] = $path;
        }

        $data = new Informasi;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return Kontak::find($id);
    }

    public function edit($id)
    {
        $record = Kontak::find($id);

        return $this->render('backend.sukaman.kontak.edit', ['record' => $record]);
    }

    public function update(Request $request, $id)
    {
        $row = $request->all();
        
        $data = Kontak::find($id);

        $row['icon'] = $data->icon;

        if($request->hasFile('icon')){
            $path = $request->file('icon')->store('uploads/sukaman/kontak', 'public');
            $row['icon'] = $path;
        }
        $row['gambar'] = $data->gambar;

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/sukaman/kontak', 'public');
            $row['gambar'] = $path;
        }
        $data->fill($row);
        $data->save();
        
        return redirect($this->link);


    }

    public function details($id)
    {
     $record = Kontak::find($id);
       // dd($record);
     return $this->render('backend.sukaman.kontak.details', ['record' => $record]);
 }

 public function destroy($id)
 {
    $jenis = Kontak::find($id);
    $jenis->delete();

    return response([
        'status' => true,
    ]);
}
}
