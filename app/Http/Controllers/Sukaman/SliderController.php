<?php

namespace App\Http\Controllers\Sukaman;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Sukaman\SliderRequest;
use App\Models\Sukaman\Slider;

use Datatables;

class SliderController extends Controller
{
    //
    protected $link = 'backend/sukaman/slider/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Slider Web Paviliun Sukaman");
        $this->setSubtitle("Sukaman");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Paviliun Sukaman' => '#', 'Slider' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'judul',
			    'name' => 'judul',
			    'label' => 'Judul',
			    'searchable' => false,
                'className' => "center aligned",
			    'sortable' => true,
			],
			[
			    'data' => 'gambar',
			    'name' => 'gambar',
			    'label' => 'Gambar',
			    'searchable' => false,
                'className' => "center aligned",
                
			    'sortable' => true,
			],
            [
                'data' => 'urutan',
                'name' => 'urutan',
                'label' => 'Urutan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
                'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => true,
                'width' => '100px',
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
                'className' => "center aligned",
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '70px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Slider::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%'.$judul.'%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('gambar', function ($record) {
                $string ='';
                if($record->gambar){
                    $string = '<img src="'.asset('storage').'/'.$record->gambar.'" alt="icon" width="80">';
                }
                return $string;
            })
            ->addColumn('status', function ($record) {
                $string ='<span class="ui fluid orange label">Draft</span>';
                if($record->status==1){
                    $string = '<span class="ui fluid green label">Published</span>';
                }
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['gambar','action','status'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.sukaman.slider.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.sukaman.slider.create');
    }

    public function store(SliderRequest $request)
    {
    	$row = $request->all();

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/sukaman/slider', 'public');
            $row['gambar'] = $path;
        }

        $data = new Slider;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return Slider::find($id);
    }

    public function edit($id)
    {
    	$record = Slider::find($id);

        return $this->render('backend.sukaman.slider.edit', ['record' => $record]);
    }

    public function update(SliderRequest $request, $id)
    {
    	$row = $request->all();
        
        $data = Slider::find($id);

        $row['gambar'] = $data->gambar;

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/sukaman/slider', 'public');
            $row['gambar'] = $path;
        }

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
    	$jenis = Slider::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
