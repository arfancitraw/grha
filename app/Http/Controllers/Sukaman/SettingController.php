<?php

namespace App\Http\Controllers\Sukaman;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Sukaman\Setting;

use App\Http\Requests\Sukaman\SettingRequest;

use Datatables;
use Storage;

class SettingController extends Controller
{
    //
    protected $link = 'backend/sukaman/setting/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Paviliun Sukaman Setting");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Paviliun Sukaman' => '#', 'Setting' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'kunci',
                'name' => 'kunci',
                'label' => 'Kunci',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'isi',
                'name' => 'isi',
                'label' => 'Isi',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
                'width' => '20%'
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }

    public function index()
    {
        $seting = Setting::orderBy('id', 'asc')->get();
        $settings = Setting::all()
                           ->mapWithKeys(function ($item) {
                               return [$item->kunci => $item];
                           });
        return $this->render('backend.sukaman.setting.index', ['mockup' => false,
            'seting' => $seting,
            'settings' => $settings
        ]);
    }

    public function create()
    {
        return $this->render('backend.sukaman.setting.create');
    }

    public function store(Request $request)
    {
        foreach ($request->setting as $id => $value) {
            // dd($value);
            $row = Setting::find($id);
            $row->fill($value);
            $row->save();
        }
        
        return response([
            'status' => true
        ]);

    }

}
