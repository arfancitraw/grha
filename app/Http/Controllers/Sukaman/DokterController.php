<?php

namespace App\Http\Controllers\Sukaman;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Sukaman\Dokter;
use App\Models\Dokter\ListDokter;

use App\Http\Requests\Sukaman\DokterRequest;

use Datatables;
use Storage;

class DokterController extends Controller
{
    //
    protected $link = 'backend/sukaman/dokter/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Paviliun Sukaman Dokter");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Paviliun Sukaman' => '#', 'Dokter' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'id_dokter',
                'name' => 'id_dokter',
                'label' => 'Dokter',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
                'width' => '20%'
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Dokter::select('*');

        if($search = request()->get('search')){
            $records->whereHas('detail', function($q) use ($search){
                $q->where('nama', 'ilike', '%'.$search.'%');
            });
        }
        
        return $this->render('backend.sukaman.dokter.grid', [
            'data' => $records->get()
        ]);
    }

    public function index()
    {
        return $this->render('backend.sukaman.dokter.index', ['mockup' => false]);
    }

    public function create()
    {
        $records = ListDokter::doesnthave('sukaman')
                             ->whereNotNull('nama_lengkap')
                             ->get();

        return $this->render('backend.sukaman.dokter.create', [
            'records' => $records
        ]);
    }

    public function store(Request $request)
    {
        $row = $request->all();

        foreach ($request->dokter as $val) {
            $data = new Dokter;
            $data->id_dokter = $val;
            $data->save();
        }
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return Dokter::find($id);
    }

    public function edit($id)
    {
        $record = Dokter::find($id);

        return $this->render('backend.sukaman.dokter.edit', ['record' => $record]);
    }

    public function update(DokterRequest $request, $id)
    {
        $row = $request->all();
        
        $data = Dokter::find($id);

        $row['icon'] = $data->icon;

        if($request->hasFile('icon')){
            $path = $request->file('icon')->store('uploads/sukaman/artikel', 'public');
            $row['icon'] = $path;
        }
        $data->fill($row);
        $data->save();
        
        return redirect($this->link);


    }

public function removeImage($id)
{
    $data = Dokter::find($id);
    Storage::delete($data->photo);
    $data->photo = null;
    $data->save();

    return response([
    'status' => true,
    'data'  => $data
    ]);
}
    public function destroy($id)
    {
        $jenis = Dokter::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
