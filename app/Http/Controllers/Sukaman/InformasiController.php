<?php

namespace App\Http\Controllers\Sukaman;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Sukaman\Informasi;

use App\Http\Requests\Sukaman\InformasiRequest;

use Datatables;
use Storage;

class InformasiController extends Controller
{
    //
    protected $link = 'backend/sukaman/informasi/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Paviliun Sukaman Informasi");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Paviliun Sukaman' => '#', 'Informasi' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'gambar',
                'name' => 'gambar',
                'label' => 'Gambar',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'konten',
                'name' => 'konten',
                'label' => 'Konten',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
                'width' => '20%'
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Informasi::with('creator')
                           ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('gambar', function ($record) {
                $string ='';
                if($record->gambar){
                    $string = '<img src="'.asset('storage').'/'.$record->gambar.'" alt="gambar" width="80">';
                }
                return $string;
            })
            ->addColumn('konten', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->konten).'</span>';
                return $string;
            })

            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['gambar','action','konten'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.sukaman.informasi.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.sukaman.informasi.create');
    }

    public function store(InformasiRequest $request)
    {
        $row = $request->all();
        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/sukaman/informasi', 'public');
            $row['gambar'] = $path;
        }

        $data = new Informasi;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return Informasi::find($id);
    }

    public function edit($id)
    {
        $record = Informasi::find($id);

        return $this->render('backend.sukaman.informasi.edit', ['record' => $record]);
    }

    public function update(Request $request, $id)
    {
        $row = $request->all();
        
        $data = Informasi::find($id);

        $row['icon'] = $data->icon;

        if($request->hasFile('icon')){
            $path = $request->file('icon')->store('uploads/sukaman/informasi', 'public');
            $row['icon'] = $path;
        }
        $row['gambar'] = $data->gambar;

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/sukaman/informasi', 'public');
            $row['gambar'] = $path;
        }
        $data->fill($row);
        $data->save();
        
        return redirect($this->link);


    }

public function removeImage($id)
{
    $data = Informasi::find($id);
    Storage::delete($data->photo);
    $data->photo = null;
    $data->save();

    return response([
    'status' => true,
    'data'  => $data
    ]);
}
    public function destroy($id)
    {
        $jenis = Informasi::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
