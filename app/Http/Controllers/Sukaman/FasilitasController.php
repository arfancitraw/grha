<?php

namespace App\Http\Controllers\Sukaman;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Sukaman\Fasilitas;

use App\Http\Requests\Sukaman\FasilitasRequest;

use Datatables;
use Storage;

class FasilitasController extends Controller
{
    //
    protected $link = 'backend/sukaman/fasilitas/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Paviliun Sukaman Fasilitas");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Paviliun Sukaman' => '#', 'Fasilitas' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'gambar',
                'name' => 'gambar',
                'label' => 'Gambar',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
                'width' => '20%'
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Fasilitas::with('creator')
                           ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('gambar', function ($record) {
                $string ='';
                if($record->gambar){
                    $string = '<img src="'.asset('storage').'/'.$record->gambar.'" alt="gambar" width="80">';
                }
                return $string;
            })
            ->addColumn('deskripsi', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->deskripsi).'</span>';
                return $string;
            })

            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['gambar','action','deskripsi'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.sukaman.fasilitas.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.sukaman.fasilitas.create');
    }

    public function store(FasilitasRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/sukaman/fasilitas', 'public');
            $row['gambar'] = $path;
        }

        $data = new Fasilitas;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return Fasilitas::find($id);
    }

    public function edit($id)
    {
        $record = Fasilitas::find($id);

        return $this->render('backend.sukaman.fasilitas.edit', ['record' => $record]);
    }

    public function update(Request $request, $id)
    {
        $row = $request->all();
        
        $data = Fasilitas::find($id);

        $row['gambar'] = $data->gambar;

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/sukaman/fasilitas', 'public');
            $row['gambar'] = $path;
        }
        $data->fill($row);
        $data->save();
        
        return redirect($this->link);


    }

public function removeImage($id)
{
    $data = Fasilitas::find($id);
    Storage::delete($data->photo);
    $data->photo = null;
    $data->save();

    return response([
    'status' => true,
    'data'  => $data
    ]);
}
    public function destroy($id)
    {
        $jenis = Fasilitas::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
