<?php

namespace App\Http\Controllers\Sukaman;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Sukaman\PricingDetail;
use App\Models\Sukaman\PricingKategori;
use App\Models\Sukaman\Pricing;

use App\Http\Requests\Sukaman\PricingDetailRequest;
use App\Http\Requests\Sukaman\PricingKategoriRequest;
use App\Http\Requests\Sukaman\PricingRequest;

use Datatables;
use Storage;

class PricingKategoriController extends Controller
{
    //
    protected $link = 'backend/sukaman/pricing-kategori/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Kategori Pricing Paviliun Sukaman");
        
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Paviliun Sukaman' => '#', 'Pricing' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
            ],
            [
                'data' => 'prioritas',
                'name' => 'prioritas',
                'label' => 'Prioritas',
                'searchable' => false,
                'sortable' => true,
                'className' => "right aligned",
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],

            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => 'center aligned'
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "two wide column center aligned",
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = PricingKategori::with('creator')
                           ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('status', function ($record) {
                return $record->status 
                     ? 'Aktif'
                     : 'Non-Aktif';
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);
                return $btn;
            })
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.sukaman.pricing-kategori.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.sukaman.pricing-kategori.create');
    }

    public function pricing()
    {        
        $this->setTitle("Pricing di Paviliun Sukaman");
        return $this->render('backend.sukaman.pricing-kategori.pricing');
    }

    public function tarif($id)
    {        
        $record = PricingKategori::find($id);
        return $this->render('backend.sukaman.pricing-kategori.tarif.create', ['record' => $record]);
    }

    public function salin($id)
    {        
        $record = Pricing::find($id);
        return $this->render('backend.sukaman.pricing-kategori.tarif.salin', ['record' => $record]);
    }

    public function Detailtarif($id)
    {        
        $record = Pricing::find($id);
        return $this->render('backend.sukaman.pricing-kategori.detail.create', ['record' => $record]);
    }

    public function store(PricingKategoriRequest $request)
    {
        $row = $request->all();

        $data = new PricingKategori;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function storeTarif(PricingRequest $request)
    {
        $row = $request->all();

        $data = new Pricing;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function salinTarif(PricingRequest $request)
    {
        $source = Pricing::find($request->sumber);
        $row = $request->all();

        $data = new Pricing;
        $data->fill($row);
        $data->save();

        foreach ($source->detail as $row) {
            $detail = new PricingDetail;
            $detail->fill($row->toArray());
            $data->detail()->save($detail);
        }
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function storeDetailTarif(PricingDetailRequest $request)
    {
        $row = $request->all();
        $data = new PricingDetail;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function edit($id)
    {
        $this->setTitle("Detail Kategori Pricing Sukaman");
        $record = PricingKategori::with(['tarif' => function($q){
                                            return $q->orderBy('id');
                                        }, 'tarif.detail' => function($q){
                                            return $q->orderBy('urutan')
                                                     ->orderBy('id');
                                        }])
                                 ->find($id);
        // $record = PricingDetailKategori::find($id);

        return $this->render('backend.sukaman.pricing-kategori.edit', ['record' => $record]);
    }
    public function editTarif($id)
    {
        $this->setTitle("Edit Tarif Pricing Sukaman");
        $record = Pricing::find($id);

        return $this->render('backend.sukaman.pricing-kategori.tarif.edit', ['record' => $record]);
    }
    public function editDetailTarif($id)
    {
        // dd($id);
        $this->setTitle("Edit Tarif Pricing Sukaman");
        $record = PricingDetail::find($id);

        return $this->render('backend.sukaman.pricing-kategori.detail.edit', ['record' => $record]);
    }

    public function update(PricingKategoriRequest $request, $id)
    {
        $row = $request->all();
        
        $data = PricingKategori::find($id);

        $data->fill($row);
        $data->save();
        return redirect($this->link);
    }
    public function updateTarif(Request $request)
    {
        $row = $request->all();
        $data = Pricing::find($request->id);

        $data->fill($row);
        $data->save();
        return redirect($this->link);
    }
    public function updateDetailTarif(Request $request)
    {
        // dd($request->all());
        $row = $request->all();
        $data = PricingDetail::find($request->id);

        $data->fill($row);
        $data->save();
        return redirect($this->link);
    }

    public function destroy($id)
    {
        $jenis = PricingKategori::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
    public function destroyTarif($id)
    {
        $pricing = Pricing::find($id);
        $pricing->detail()->delete();
        $pricing->delete();

        return response([
            'status' => true,
        ]);
    }
    public function destroyDetail($id)
    {
        $detail = PricingDetail::find($id);
        $detail->delete();

        return response([
            'status' => true,
        ]);
    }
}
