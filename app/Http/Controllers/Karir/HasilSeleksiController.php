<?php

namespace App\Http\Controllers\Karir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Karir\HasilSeleksi;

use App\Http\Requests\Karir\HasilSeleksiRequest;

use Datatables;

class HasilSeleksiController extends Controller
{
    //
    protected $link = 'backend/karir/hasil-seleksi/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Hasil Seleksi");
        $this->setSubtitle("Diklat");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Karir' => '#', 'Hasil Seleksi' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'karir_id',
			    'name' => 'karir_id',
			    'label' => 'Karir',
			    'searchable' => false,
			    'sortable' => true,
			],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'lampiran',
                'name' => 'lampiran',
                'label' => 'Lampiran',
                'searchable' => false,
                'sortable' => true,
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = HasilSeleksi::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('karir_id', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('karir_id', 'ilike', '%' . $karir_id . '%');
        }
        if ($keterangan = $request->keterangan) {
            $records->where('keterangan', 'ilike', '%' . $keterangan . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('karir_id', function ($record) {
                return $record->karir->judul;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.diklat.hasil-seleksi.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.diklat.hasil-seleksi.create');
    }

    public function store(HasilSeleksiRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('lampiran')){
            $path = $request->file('lampiran')->store('uploads/karir/hasil-seleksi', 'public');
            $row['lampiran'] = $path;
        }

        $data = new HasilSeleksi;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function show($id)
    {
        return HasilSeleksi::find($id);
    }

    public function edit($id)
    {
    	$record = HasilSeleksi::find($id);

        return $this->render('backend.diklat.hasil-seleksi.edit', ['record' => $record]);
    }

    public function update(HasilSeleksiRequest $request, $id)
    {
        $row = $request->all();
        
        $data = HasilSeleksi::find($id);

        $row['lampiran'] = $data->lampiran;

        if($request->hasFile('lampiran')){
            $path = $request->file('lampiran')->store('uploads/karir/hasil-seleksi', 'public');
            $row['lampiran'] = $path;
        }

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
    	$jenis = HasilSeleksi::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
