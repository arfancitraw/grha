<?php

namespace App\Http\Controllers\Karir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Karir\Info;

use App\Http\Requests\Karir\InfoRequest;

use Datatables;

class InfoController extends Controller
{
    //
    protected $link = 'backend/karir/info/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Info Karir");
        $this->setSubtitle("Diklat");
        $this->setModalSize("");
        $this->setBreadcrumb(['Karir' => '#', 'Info' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul',
                'searchable' => false,
                'sortable' => true,
                'width' => '200px'
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",

            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Info::with('creator')
                           ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('posisi', function ($record) {
                $string ='Kiri';
                if($record->posisi==1){
                    $string = 'Kanan';
                }
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.diklat.info.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.diklat.info.create');
    }

    public function store(InfoRequest $request)
    {
        $row = $request->all();

        $data = new Info;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return Info::find($id);
    }

    public function edit($id)
    {
        $record = Info::find($id);

        return $this->render('backend.diklat.info.edit', ['record' => $record]);
    }

    public function update(InfoRequest $request, $id)
    {
        $row = $request->all();
        $data = Info::find($id);

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
        $jenis = Info::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
