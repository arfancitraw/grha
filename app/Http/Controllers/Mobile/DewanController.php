<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Mobile\DireksiRequest;
use App\Models\Mobile\Dewan;

use Datatables;

class DewanController extends Controller
{
    //
    protected $link = 'backend/mobile/dewan/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Mobile Dewan");
        $this->setSubtitle("Mobile");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Mobilr Dewan' => '#', 'Dewan' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
                'className' => "center aligned",
			    'sortable' => true,
			],
			[
			    'data' => 'jabatan',
			    'name' => 'jabatan',
			    'label' => 'Jabatan',
			    'searchable' => false,
                'className' => "center aligned",
                
			    'sortable' => true,
			],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'photo',
                'name' => 'photo',
                'label' => 'Photo',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'urutan',
                'name' => 'urutan',
                'label' => 'Urutan',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
                'className' => "center aligned",
			    'searchable' => false,
			    'sortable' => true,
                'width' => '100px',
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
                'className' => "center aligned",
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '70px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Dewan::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%'.$nama.'%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('photo', function ($record) {
                $string ='';
                if($record->photo){
                    $string = '<img src="'.asset('storage').'/'.$record->photo.'" alt="icon" width="80">';
                }
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['photo','action'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.mobile.dewan.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.mobile.dewan.create');
    }

    public function store(DireksiRequest $request)
    {
    	$row = $request->all();

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/sukaman/dewan', 'public');
            $row['photo'] = $path;
        }

        $data = new Dewan;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return Dewan::find($id);
    }

    public function edit($id)
    {
    	$record = Dewan::find($id);

        return $this->render('backend.mobile.dewan.edit', ['record' => $record]);
    }

    public function update(Request $request, $id)
    {
    	$row = $request->all();
        
        $data = Dewan::find($id);

        $row['photo'] = $data->photo;

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/mobile/dewan', 'public');
            $row['photo'] = $path;
        }

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
    	$jenis = Dewan::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
