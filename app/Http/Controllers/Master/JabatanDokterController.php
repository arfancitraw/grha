<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\JabatanDokter;

use App\Http\Requests\Master\JabatanDokterRequest;

use Datatables;

class JabatanDokterController extends Controller
{
    //
    protected $link = 'backend/master/jabatan-dokter/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Jabatan Dokter");
        $this->setSubtitle("Master");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Master' => '#', 'Jabatan Dokter' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'jabatan',
			    'name' => 'jabatan',
			    'label' => 'Jabatan',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'three wide column'
			],
			[
			    'data' => 'deskripsi',
			    'name' => 'deskripsi',
			    'label' => 'Deskripsi',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'five wide column'
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = JabatanDokter::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($jabatan = $request->jabatan) {
            $records->where('jabatan', 'ilike', '%' . $jabatan . '%');
        }
        // if ($keterangan = $request->keterangan) {
        //     $records->where('deskripsi', 'ilike', '%' . $deskripsi . '%');
        // }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('deskripsi', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->deskripsi).'</span>';
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['action','deskripsi'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.master.jabatan-dokter.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.jabatan-dokter.create');
    }

    public function store(JabatanDokterRequest $request)
    {
    	$jenis = new JabatanDokter;
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        return JabatanDokter::find($id);
    }

    public function edit($id)
    {
    	$record = JabatanDokter::find($id);

        return $this->render('backend.master.jabatan-dokter.edit', ['record' => $record]);
    }

    public function update(JabatanDokterRequest $request, $id)
    {
    	$jenis = JabatanDokter::find($id);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function destroy($id)
    {
        $jenis = JabatanDokter::find($id);
        try {
            $jenis->delete();
        } catch (\Exception $e) {
            return response([
                'status' => false,
                'message' => 'Data sudah digunakan di table lain'
            ], 422);
        }

        return response([
            'status' => true,
        ]);
    }
}
