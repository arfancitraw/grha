<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\KuesionerDetail;
use App\Models\Master\KuesionerPeriode;

use App\Http\Requests\Master\KuesionerDetailRequest;

use Datatables;

class KuesionerDetailController extends Controller
{
    //
  protected $link = 'backend/master/kuesioner-detail/';
  protected $perms  = '';

  function __construct()
  {
    $this->setLink($this->link);
    $this->setPerms($this->perms);

    $this->setTitle("Kuesioner Detail");
    $this->setSubtitle("Kuesioner Detail");
    $this->setModalSize("tiny");
    $this->setBreadcrumb(['Kuesioner Detail' => '#', 'Kuesioner Detail' => '#']);
    $this->setTableStruct([
     [
       'data' => 'num',
       'name' => 'num',
       'label' => '#',
       'orderable' => false,
       'searchable' => false,
       'className' => "center aligned",
       'width' => '40px',
     ],
     /* --------------------------- */
     [
       'data' => 'kuesioner_id',
       'name' => 'kuesioner_id',
       'label' => 'Kuesioner Peridoe',
       'searchable' => false,
       'sortable' => true,
     ],
     [
      'data' => 'pertanyaan',
      'name' => 'pertanyaan',
      'label' => 'Pertanyaan',
      'searchable' => false,
      'sortable' => true,
      'className'=> "center aligned",
    ],
    [
     'data' => 'created_at',
     'name' => 'created_at',
     'label' => 'Tanggal Entry',
     'searchable' => false,
     'sortable' => true,
   ],
   [
     'data' => 'created_by',
     'name' => 'created_by',
     'label' => 'Oleh',
     'searchable' => false,
     'sortable' => true,
   ],
   [
     'data' => 'action',
     'name' => 'action',
     'label' => 'Aksi',
     'searchable' => false,
     'sortable' => false,
     'className' => "center aligned",
     'width' => '150px',
   ]
 ]);
  }

  public function grid(Request $request)
  {
    $records = KuesionerDetail::with('creator')
    ->select('*');
		//Init Sort
    if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
      $records->orderBy('created_at', 'desc');
    }

        //Filters
    if ($kuesioner_id = $request->kuesioner_id) {
      $records->where('kuesioner_id', $kuesioner_id);
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
      return $request->get('start');
    })
    ->addColumn('status', function ($record) {
      $string = '';
      if ($record->status > 0) {
       $string .= '<div class="ui purple horizontal label">Aktif</div>';
     }else{
       $string .= '<div class="ui red horizontal label">Tidak Aktif</div>';
     }
     return $string;
   })
    ->addColumn('created_at', function ($record) {
      return $record->created_at->format('d F Y');
    })
    ->addColumn('created_by', function ($record) {
      return $record->creator->name;
    })
    ->addColumn('kuesioner_id', function ($record) {
                // dd($record->spesialisasi);
      return $record->kuesionerperiode['periode'];
    })
    ->addColumn('action', function ($record) {
      $btn = '';
                //Edit
      $btn .= $this->makeButton([
       'type' => 'edit-page',
       'id'   => $record->id
     ]);
                // Delete
      $btn .= $this->makeButton([
       'type' => 'delete',
       'id'   => $record->id
     ]);

      return $btn;
    })
    ->rawColumns(['action','status'])
    ->make(true);
  }

  public function index()
  {
    return $this->render('backend.master.kuesioner-detail.index', ['mockup' => false]);
  }

  public function create()
  {
    return $this->render('backend.master.kuesioner-detail.create');
  }

  public function store(KuesionerDetailRequest $request)
  {
   $jenis = new KuesionerDetail;
   $jenis->fill($request->all());
   $jenis->save();

   return response([
    'status' => true,
    'data'	=> $jenis
  ]);
 }

 public function show($id)
 {
  return KuesionerDetail::find($id);
}

public function edit($id)
{
 $record = KuesionerDetail::find($id);

 return $this->render('backend.master.kuesioner-detail.edit', ['record' => $record]);
}

public function update(KuesionerDetailRequest $request, $id)
{
 $jenis = KuesionerDetail::find($id);
 $jenis->fill($request->all());
 $jenis->save();

 return response([
  'status' => true,
  'data'	=> $jenis
]);
}

public function destroy($id)
{
 $jenis = KuesionerDetail::find($id);
 $jenis->delete();

 return response([
  'status' => true,
]);
}
}
