<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\JenisMutu;

use App\Http\Requests\Master\JenisMutuRequest;

use Datatables;

class JenisMutuController extends Controller
{
    //
    protected $link = 'backend/master/jenis-mutu/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Jenis Mutu");
        $this->setSubtitle("Master");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Master' => '#', 'Jenis Mutu' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "one wide column center aligned",
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'four wide column left aligned'
			],
			[
			    'data' => 'keterangan',
			    'name' => 'keterangan',
			    'label' => 'Keterangan',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'five wide column left aligned'
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = JenisMutu::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($keterangan = $request->keterangan) {
            $records->where('keterangan', 'ilike', '%' . $keterangan . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('keterangan', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->keterangan).'</span>';
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['action','keterangan'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.master.jenis-mutu.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.jenis-mutu.create');
    }

    public function store(JenisMutuRequest $request)
    {
    	$jenis = new JenisMutu;
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        return JenisMutu::find($id);
    }

    public function edit($id)
    {
    	$record = JenisMutu::find($id);

        return $this->render('backend.master.jenis-mutu.edit', ['record' => $record]);
    }

    public function update(JenisMutuRequest $request, $id)
    {
    	$jenis = JenisMutu::find($id);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function destroy($id)
    {
    	$jenis = JenisMutu::find($id);
        try {
            $jenis->delete();
        } catch (\Exception $e) {
            return response([
                'status' => false,
                'message' => 'Data sudah digunakan di table lain'
            ], 422);
        }

    	return response([
    		'status' => true,
    	]);
    }
}
