<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master\InfoBed;
use App\Http\Requests\Master\InfoBedRequest;
use Datatables;

class InfoBedController extends Controller
{
    //
    protected $link = 'backend/master/info-bed/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Info Bed");
        $this->setSubtitle("Master");
		$this->setModalSize("medium");
		$this->setBreadcrumb(['Master' => '#', 'Info Bed' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
            [
                'data' => 'gambar',
                'name' => 'gambar',
                'label' => 'Gambar',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'jumlah_total',
                'name' => 'jumlah_total',
                'label' => 'Jumlah Total',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'isi',
                'name' => 'isi',
                'label' => 'Isi',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = InfoBed::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('gambar', function ($record) {
                $string ='';
                if($record->gambar){
                    $string = '<img src="'.asset('storage').'/'.$record->gambar.'" alt="icon" width="80">';
                }
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                 $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;

            })
            ->rawColumns(['gambar','action','status'])
            ->make(true);

	}

    public function index()
    {
        return $this->render('backend.master.info-bed.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.info-bed.create');
    }

    public function store(InfoBedRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/master/info-bed', 'public');
            $row['gambar'] = $path;
        }

        $data = new InfoBed;
        $data->fill($row);
        $data->save();
        
        return redirect($this->link);
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function show($id)
    {
        return InfoBed::find($id);
    }

    public function edit($id)
    {
    	$record = InfoBed::find($id);
        return $this->render('backend.master.info-bed.edit', ['record' => $record]);
    }

    public function update(InfoBedRequest $request, $id)
    {
        $row = $request->all();
        
        $data = InfoBed::find($id);

        $row['gambar'] = $data->gambar;

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/master/info-bad', 'public');
            $row['gambar'] = $path;
        }

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
    	$jenis = InfoBed::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
