<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\MediaSosial;

use App\Http\Requests\Master\MediaSosialRequest;

use Datatables;

class MediaSosialController extends Controller
{
    //
  protected $link = 'backend/master/media-sosial/';
  protected $perms  = '';

  function __construct()
  {
    $this->setLink($this->link);
    $this->setPerms($this->perms);

    $this->setTitle("Media Sosial ");
    $this->setSubtitle("Master");
    $this->setModalSize("tiny");
    $this->setBreadcrumb(['Media Sosial' => '#', 'Media Sosial' => '#']);
    $this->setTableStruct([
     [
       'data' => 'num',
       'name' => 'num',
       'label' => '#',
       'orderable' => false,
       'searchable' => false,
       'className' => "center aligned",
       'width' => '40px',
     ],
     /* --------------------------- */
     [
      'data' => 'icon',
      'name' => 'icon',
      'label' => 'Icon',
      'searchable' => false,
      'sortable' => true,
      'className' => "center aligned",
    ],
    [
     'data' => 'link',
     'name' => 'link',
     'label' => 'Link',
     'searchable' => false,
     'sortable' => true,
     'className' => 'center aligned'
   ],
   [
     'data' => 'created_at',
     'name' => 'created_at',
     'label' => 'Tanggal Entry',
     'searchable' => false,
     'sortable' => true,
     'className' => 'center aligned'
   ],
   [
     'data' => 'created_by',
     'name' => 'created_by',
     'label' => 'Oleh',
     'searchable' => false,
     'sortable' => true,
     'className' => 'center aligned'
   ],
   [
     'data' => 'action',
     'name' => 'action',
     'label' => 'Aksi',
     'searchable' => false,
     'sortable' => false,
     'className' => "two wide column center aligned",
   ]
 ]);
  }

  public function grid(Request $request)
  {
    $records = MediaSosial::with('creator')
    ->select('*');
		//Init Sort
    if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
      $records->orderBy('nama', 'asc');
    }

        // Filters
    if ($link = $request->link) {
      $records->where('keterangan', $link);
    }
    if ($nama = $request->nama) {
      $records->where('nama', $nama );
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
      return $request->get('start');
    })
    ->addColumn('gambar', function ($record) {
      $string ='';
      if($record->gambar){
        $string = '<img src="'.asset('storage').'/'.$record->gambar.'" alt="icon" width="80">';
      }
      return $string;
    })
    ->addColumn('created_at', function ($record) {
      return $record->created_at->format('d F Y');
    })
    ->addColumn('created_by', function ($record) {
      return $record->creator->name;
    })
    ->addColumn('action', function ($record) {
      $btn = '';
                //Edit
      $btn .= $this->makeButton([
       'type' => 'edit',
       'id'   => $record->id
     ]);
                // Delete
      $btn .= $this->makeButton([
       'type' => 'delete',
       'id'   => $record->id
     ]);

      return $btn;
    })
    ->rawColumns(['gambar','action','status'])
    ->make(true);
  }

  public function index()
  {
    return $this->render('backend.master.media-sosial.index', ['mockup' => false]);
  }

  public function create()
  {
    return $this->render('backend.master.media-sosial.create');
  }

  public function store(MediaSosialRequest $request)
  {
    $row = $request->all();
    if($request->hasFile('gambar')){
      $path = $request->file('gambar')->store('uploads/master/media-sosial', 'public');
      $row['gambar'] = $path;
    }

    $data = new MediaSosial;
    $data->fill($row);
    $data->save();

    return response([
      'status' => true,
      'data'  => $data
    ]);
  }
  public function show($id)
  {
    return MediaSosial::find($id);
  }

  public function edit($id)
  {
   $record = MediaSosial::find($id);

   return $this->render('backend.master.media-sosial.edit', ['record' => $record]);
 }

 public function update(MediaSosialRequest $request, $id)
 {
  $row = $request->all();

  $data = MediaSosial::find($id);
  $row['gambar'] = $data->gambar;

  if($request->hasFile('gambar')){
    $path = $request->file('gambar')->store('uploads/master/media-sosial', 'public');
    $row['gambar'] = $path;
  }
  $data->fill($row);
  $data->save();

  return response([
    'status' => true,
    'data'  => $data
  ]);
}

public function destroy($id)
{
 $jenis = MediaSosial::find($id);
 $jenis->delete();

 return response([
  'status' => true,
]);
}
}
