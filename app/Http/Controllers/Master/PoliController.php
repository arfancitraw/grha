<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\Poli;

use App\Http\Requests\Master\PoliRequest;

use Datatables;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Storage;

class PoliController extends Controller
{
    //
    protected $link = 'backend/master/poli/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Poli");
        $this->setSubtitle("Master");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Master' => '#', 'Poli' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
                'data' => 'kode',
                'name' => 'kode',
                'label' => 'Kode',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true
            ],
            [
			    'data' => 'aktif',
			    'name' => 'aktif',
			    'label' => 'Status',
			    'searchable' => false,
			    'sortable' => true,
                'className' => "center aligned",
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Poli::with('creator')
					   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('kode', 'asc');
        }

        //Filters
        if ($kode = $request->kode) {
            $records->where('kode', 'ilike', '%' . $kode . '%');
        }
        if ($nama = $request->nama) {
            $records->whereRaw('LOWER(nama) LIKE ? ', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('aktif', function ($record) {
                return $record->aktif 
                     ? '<span class="ui green label">Aktif</span>'
                     : '<span class="ui red label">Tidak</span>';
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['aktif', 'action'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.master.poli.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.poli.create');
    }

    public function store(PoliRequest $request)
    {
    	$jenis = new Poli;
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        return Poli::find($id);
    }

    public function edit($id)
    {
    	$record = Poli::find($id);

        return $this->render('backend.master.poli.edit', ['record' => $record]);
    }

    public function update(PoliRequest $request, $id)
    {
    	$jenis = Poli::find($id);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function destroy($id)
    {
    	$jenis = Poli::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }

    public function sync()
    {
        $url = config('app.api_url').'kd_dep';
        $client = new Client();
        $result = $client->get($url);

        if($result){
            $data = json_decode($result->getBody());
            foreach ($data as $poli) {
                $cek = Poli::where('kode', $poli->kd_dep)->first();
                $row = $cek ?: new Poli();

                $row->kode = $poli->kd_dep;
                $row->nama = $poli->ds_dep;
                $row->aktif = ($poli->aktif == "true");
                $row->save();
            }

            return response([
                'success' => true,
            ]);
        }

        return response([
            'success' => false,
            'message' => 'Gagal mengakses web-service, silahkan periksa koneksi anda'
        ], 500);
    }
}
