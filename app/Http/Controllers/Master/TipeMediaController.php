<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\TipeMedia;

use App\Http\Requests\Master\TipeMediaRequest;

use Datatables;

class TipeMediaController extends Controller
{
    //
    protected $link = 'backend/master/tipe-media/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Tipe Media");
        $this->setSubtitle("Master");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Master' => '#', 'Tipe Media' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "one wide column center aligned",
			],
			/* --------------------------- */
			[
			    'data' => 'tipe_media',
			    'name' => 'tipe_media',
			    'label' => 'Tipe Media',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'three wide column'
			],
			[
			    'data' => 'deskripsi',
			    'name' => 'deskripsi',
			    'label' => 'Deskripsi',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'five wide column'
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = TipeMedia::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('tipe_media', 'asc');
        }

        //Filters
        if ($tipe_media = $request->tipe_media) {
            $records->where('tipe_media', 'ilike', '%' . $tipe_media . '%');
        }
        // if ($keterangan = $request->keterangan) {
        //     $records->where('deskripsi', 'ilike', '%' . $deskripsi . '%');
        // }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('deskripsi', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->deskripsi).'</span>';
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['deskripsi', 'action'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.master.tipe-media.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.tipe-media.create');
    }

    public function store(TipeMediaRequest $request)
    {
    	$jenis = new TipeMedia;
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        return TipeMedia::find($id);
    }

    public function edit($id)
    {
    	$record = TipeMedia::find($id);

        return $this->render('backend.master.tipe-media.edit', ['record' => $record]);
    }

    public function update(TipeMediaRequest $request, $id)
    {
    	$jenis = TipeMedia::find($id);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function destroy($id)
    {
    	$jenis = TipeMedia::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
