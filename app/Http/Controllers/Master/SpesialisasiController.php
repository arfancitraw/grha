<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\Spesialisasi;

use App\Http\Requests\Master\SpesialisasiRequest;

use Datatables;

class SpesialisasiController extends Controller
{
    //
    protected $link = 'backend/master/spesialisasi/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Spesialisasi");
        $this->setSubtitle("Master");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Master' => '#', 'Spesialisasi' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'three wide column'
			],
            [
                'data' => 'prioritas',
                'name' => 'prioritas',
                'label' => 'Prioritas',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'searchable' => false,
                'sortable' => true,
                'className' => 'five wide column'
            ],

			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Spesialisasi::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('prioritas', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . strtolower($nama) . '%');
        }
        // if ($keterangan = $request->keterangan) {
        //     $records->where('deskripsi', 'ilike', '%' . $deskripsi . '%');
        // }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('deskripsi', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->deskripsi).'</span>';
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['deskripsi','action'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.master.spesialisasi.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.spesialisasi.create');
    }

    public function store(SpesialisasiRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('icon')){
            $path = $request->file('icon')->store('uploads/master/spesialisasi', 'public');
            $row['icon'] = $path;
        }

    	$data = new Spesialisasi;
    	$data->fill($row);
    	$data->save();

    	return response([
    		'status' => true,
    		'data'	=> $data
    	]);
    }

    public function show($id)
    {
        return Spesialisasi::find($id);
    }

    public function edit($id)
    {
    	$record = Spesialisasi::find($id);

        return $this->render('backend.master.spesialisasi.edit', ['record' => $record]);
    }

    public function update(SpesialisasiRequest $request, $id)
    {
        $row = $request->all();
        $data = Spesialisasi::find($id);
        $row['icon'] = $data->icon;

        if($request->hasFile('icon')){
            $path = $request->file('icon')->store('uploads/master/spesialisasi', 'public');
            $row['icon'] = $path;
        }

    	$data->fill($row);
        $data->save();

    	return response([
    		'status' => true,
    		'data'	=> $data
    	]);
    }

    public function destroy($id)
    {
        $jenis = Spesialisasi::find($id);
        try {
            $jenis->delete();
        } catch (\Exception $e) {
            return response([
                'status' => false,
                'message' => 'Data sudah digunakan di table lain'
            ], 422);
        }

        return response([
            'status' => true,
        ]);
        
    }
}
