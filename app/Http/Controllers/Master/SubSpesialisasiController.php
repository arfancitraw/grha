<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master\SubSpesialisasi;
use App\Http\Requests\Master\SubSpesialisasiRequest;

use Datatables;

class SubSpesialisasiController extends Controller
{
    //
    protected $link = 'backend/master/sub-spesialisasi/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Sub Spesialisasi");
        $this->setSubtitle("Master");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Master' => '#', 'Sub Spesialisasi' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "one column wide center aligned",
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'three wide column'
			],
            [
                'data' => 'spesialisasi.nama',
                'name' => 'spesialisasi.nama',
                'label' => 'Spesialisasi Induk',
                'searchable' => false,
                'sortable' => true,
                'className' => 'three wide column'
            ],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
                'className' => 'four wide column'
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = SubSpesialisasi::with('creator','spesialisasi')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($spesialisasi_induk = $request->spesialisasi_induk) {
            $records->where('spesialisasi_induk', $spesialisasi_induk);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('keterangan', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->keterangan).'</span>';
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['keterangan','action'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.master.sub-spesialisasi.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.sub-spesialisasi.create');
    }

    public function store(SubSpesialisasiRequest $request)
    {
    	$jenis = new SubSpesialisasi;
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        return SubSpesialisasi::find($id);
    }

    public function edit($id)
    {
    	$record = SubSpesialisasi::find($id);

        return $this->render('backend.master.sub-spesialisasi.edit', ['record' => $record]);
    }

    public function update(SubSpesialisasiRequest $request, $id)
    {
    	$jenis = SubSpesialisasi::find($id);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function destroy($id)
    {
    	$jenis = SubSpesialisasi::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
