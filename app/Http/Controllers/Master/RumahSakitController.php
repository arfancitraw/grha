<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\RumahSakit;

use App\Http\Requests\Master\RumahSakitRequest;

use Datatables;

class RumahSakitController extends Controller
{
    //
    protected $link = 'backend/master/rumah-sakit/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Rumah Sakit");
        $this->setSubtitle("Master");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Master' => '#', 'Rumah Sakit' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'alamat',
			    'name' => 'alamat',
			    'label' => 'Alamat',
			    'searchable' => false,
			    'sortable' => true,
			],
            [
                'data' => 'no_tlpn',
                'name' => 'no_tlpn',
                'label' => 'No. Telepon',
                'searchable' => false,
                'sortable' => true,
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = RumahSakit::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($keterangan = $request->keterangan) {
            $records->where('keterangan', 'ilike', '%' . $keterangan . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.master.rumah-sakit.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.rumah-sakit.create');
    }

    public function store(RumahSakitRequest $request)
    {
    	$jenis = new RumahSakit;
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        return RumahSakit::find($id);
    }

    public function edit($id)
    {
    	$record = RumahSakit::find($id);

        return $this->render('backend.master.rumah-sakit.edit', ['record' => $record]);
    }

    public function update(RumahSakitRequest $request, $id)
    {
    	$jenis = RumahSakit::find($id);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function destroy($id)
    {
    	$jenis = RumahSakit::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
