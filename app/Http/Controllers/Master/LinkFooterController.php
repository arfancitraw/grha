<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\LinkFooter;

use App\Http\Requests\Master\LinkFooterRequest;

use Datatables;

class LinkFooterController extends Controller
{
    //
    protected $link = 'backend/master/link-footer/';
    protected $perms  = '';

    function __construct()
    {
      $this->setLink($this->link);
      $this->setPerms($this->perms);

      $this->setTitle("Link Footer ");
      $this->setSubtitle("Master");
      $this->setModalSize("tiny");
      $this->setBreadcrumb(['Link Footer' => '#', 'Link Footer' => '#']);
      $this->setTableStruct([
       [
           'data' => 'num',
           'name' => 'num',
           'label' => '#',
           'orderable' => false,
           'searchable' => false,
           'className' => "center aligned",
           'width' => '40px',
       ],
       /* --------------------------- */
       [
           'data' => 'nama',
           'name' => 'nama',
           'label' => 'Nama',
           'searchable' => false,
           'sortable' => true,
           'className' => 'center aligned'
       ],
        [
           'data' => 'link',
           'name' => 'link',
           'label' => 'Link Website',
           'searchable' => false,
           'sortable' => true,
           'className' => 'center aligned'
       ],
    [
       'data' => 'created_at',
       'name' => 'created_at',
       'label' => 'Tanggal Entry',
       'searchable' => false,
       'sortable' => true,
       'className' => 'center aligned'
   ],
   [
       'data' => 'created_by',
       'name' => 'created_by',
       'label' => 'Oleh',
       'searchable' => false,
       'sortable' => true,
       'className' => 'center aligned'
   ],
   [
       'data' => 'action',
       'name' => 'action',
       'label' => 'Aksi',
       'searchable' => false,
       'sortable' => false,
       'className' => "two wide column center aligned",
   ]
]);
  }

  public function grid(Request $request)
  {
      $records = LinkFooter::with('creator')
      ->select('*');
		//Init Sort
      if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
        $records->orderBy('nama', 'asc');
    }

        // Filters
    if ($link = $request->link) {
        $records->where('link', $link);
    }
    if ($nama = $request->nama) {
        $records->where('nama', $nama );
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
        return $request->get('start');
    })
    ->addColumn('created_at', function ($record) {
        return $record->created_at->format('d F Y');
    })
    ->addColumn('created_by', function ($record) {
        return $record->creator->name;
    })
    ->addColumn('action', function ($record) {
        $btn = '';
                //Edit
        $btn .= $this->makeButton([
         'type' => 'edit',
         'id'   => $record->id
     ]);
                // Delete
        $btn .= $this->makeButton([
         'type' => 'delete',
         'id'   => $record->id
     ]);

        return $btn;
    })
    ->make(true);
}

public function index()
{
    return $this->render('backend.master.link-footer.index', ['mockup' => false]);
}

public function create()
{
    return $this->render('backend.master.link-footer.create');
}

public function store(LinkFooterRequest $request)
{
    $row = $request->all();

    if($request->hasFile('photo')){
        $path = $request->file('photo')->store('uploads/master/link-footer', 'public');
        $row['photo'] = $path;
    }

    $data = new LinkFooter;
    $data->fill($row);
    $data->save();

    return response([
        'status' => true,
        'data'  => $data
    ]);
}
public function show($id)
{
    return LinkFooter::find($id);
}

public function edit($id)
{
 $record = LinkFooter::find($id);

 return $this->render('backend.master.link-footer.edit', ['record' => $record]);
}

public function update(LinkFooterRequest $request, $id)
{
    $row = $request->all();

    $data = LinkFooter::find($id);

    $data->fill($row);
    $data->save();

    return response([
        'status' => true,
        'data'  => $data
    ]);
}

public function destroy($id)
{
 $jenis = LinkFooter::find($id);
 $jenis->delete();

 return response([
  'status' => true,
]);
}
}
