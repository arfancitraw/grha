<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\TipePendaftar;

use App\Http\Requests\Master\TipePendaftarRequest;

use Datatables;

class TipePendaftarController extends Controller
{
    //
    protected $link = 'backend/master/tipe-pendaftar/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Tipe Pendaftar");
        $this->setSubtitle("Master");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Master' => '#', 'Tipe Pendaftar' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "one wide column center aligned",
			],
			/* --------------------------- */
			[
			    'data' => 'tipe_pendaftar',
			    'name' => 'tipe_pendaftar',
			    'label' => 'Tipe Pendaftar',
                'className' => "three wide column",

			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'deskripsi',
			    'name' => 'deskripsi',
			    'label' => 'Deskripsi',
                'className' => "five wide column",

			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
                'className' => "center aligned",

			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
                'className' => "center aligned",
                
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = TipePendaftar::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('tipe_pendaftar', 'asc');
        }

        //Filters
        if ($tipe_pendaftar = $request->tipe_pendaftar) {
            $records->where('tipe_pendaftar', 'ilike', '%' . $tipe_pendaftar . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('deskripsi', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->deskripsi).'</span>';
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['deskripsi', 'action'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.master.tipe-pendaftar.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.tipe-pendaftar.create');
    }

    public function store(TipePendaftarRequest $request)
    {
    	$jenis = new TipePendaftar;
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        return TipePendaftar::find($id);
    }

    public function edit($id)
    {
    	$record = TipePendaftar::find($id);

        return $this->render('backend.master.tipe-pendaftar.edit', ['record' => $record]);
    }

    public function update(TipePendaftarRequest $request, $id)
    {
    	$jenis = TipePendaftar::find($id);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function destroy($id)
    {
    	$jenis = TipePendaftar::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
