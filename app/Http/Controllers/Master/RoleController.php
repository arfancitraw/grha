<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Entrust\Role;
use App\Models\Entrust\Permission;

use App\Http\Requests\Master\RoleRequest;

use Datatables;

class RoleController extends Controller
{
    //
    protected $link = 'backend/master/role-permission/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Role & Permissions");
        $this->setSubtitle("Master");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Master' => '#', 'Role & Permissions' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'display_name',
			    'name' => 'display_name',
			    'label' => 'Nama Role',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'three wide column'
			],
			[
			    'data' => 'description',
			    'name' => 'description',
			    'label' => 'Deskripsi',
			    'searchable' => false,
			    'sortable' => true,
          'className' => 'five wide column'
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Role::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($jabatan = $request->jabatan) {
            $records->where('display_name', 'ilike', '%' . $jabatan . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('description', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->description).'</span>';
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return !is_null($record->creator) ? $record->creator->name : '[System]';
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                  'type' => 'url',
                	'class' => 'green icon',
                  'label' => '<i class="book icon"></i>',
                  'tooltip' => 'Kelola Permission',
                	'id'   => $record->id,
                  'target' => url($this->link.$record->id)
                ]);

                return $btn;
            })
            ->rawColumns(['action','description'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.master.role.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.role.create');
    }

    public function store(RoleRequest $request)
    {
    	$jenis = new Role;
      $request['name'] = str_slug($request->display_name);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        $record = Role::find($id);
        $permission = Permission::getArray();

        return $this->render('backend.master.role.detail', ['record' => $record, 'permission' => $permission]);
    }

    public function edit($id)
    {
  	   $record = Role::find($id);

        return $this->render('backend.master.role.edit', ['record' => $record]);
    }

    public function update(RoleRequest $request, $id)
    {
    	$jenis = Role::find($id);
      $request['name'] = str_slug($request->display_name);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function changePermission(Request $request)
    {
    	$role = Role::find($request->id);
        $role->perms()->sync($request->permission);

    	return redirect($this->link);
    }

    public function destroy($id)
    {
        $jenis = JabatanDokter::find($id);
        try {
            $jenis->delete();
        } catch (\Exception $e) {
            return response([
                'status' => false,
                'message' => 'Data sudah digunakan di table lain'
            ], 422);
        }

        return response([
            'status' => true,
        ]);
    }
}
