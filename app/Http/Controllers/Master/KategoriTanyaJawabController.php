<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\KategoriTanyaJawab;

use App\Http\Requests\Master\KategoriTanyaJawabRequest;

use Datatables;

class KategoriTanyaJawabController extends Controller
{
    //
    protected $link = 'backend/master/kategori-tanya-jawab/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Kategori Tanya Jawab");
        $this->setSubtitle("Master");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Master' => '#', 'Kategori Tanya Jawab' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "one wide column center aligned",
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
			],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
            ],
			[
			    'data' => 'pernyataan',
			    'name' => 'pernyataan',
			    'label' => 'Butuh Pernyataan?',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned',
                'width' => '120px'
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = KategoriTanyaJawab::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($keterangan = $request->keterangan) {
            $records->where('keterangan', 'ilike', '%' . $keterangan . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('keterangan', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->keterangan).'</span>';
                return $string;
            })
            ->addColumn('pernyataan', function ($record) {
                return $record->pernyataan
                     ? '<span class="ui green label">Ya</span>'
                     : '<span class="ui red label">Tidak</span>';
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['keterangan', 'action', 'pernyataan'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.master.kategori-tanya-jawab.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.kategori-tanya-jawab.create');
    }

    public function store(KategoriTanyaJawabRequest $request)
    {
    	$jenis = new KategoriTanyaJawab;
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        return KategoriTanyaJawab::find($id);
    }

    public function edit($id)
    {
    	$record = KategoriTanyaJawab::find($id);

        return $this->render('backend.master.kategori-tanya-jawab.edit', ['record' => $record]);
    }

    public function update(KategoriTanyaJawabRequest $request, $id)
    {
    	$jenis = KategoriTanyaJawab::find($id);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function destroy($id)
    {
    	$jenis = KategoriTanyaJawab::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
