<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\Footer;

use App\Http\Requests\Master\FooterRequest;

use Datatables;

class FooterController extends Controller
{
    //
    protected $link = 'backend/master/footer/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Tautan Footer");
        $this->setSubtitle("Master");
		$this->setModalSize("mini");
		$this->setBreadcrumb(['Master' => '#', 'Tautan Footer' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
                'className' => "center aligned",

			    'searchable' => false,
			    'sortable' => true,
			],
            [
                'data' => 'url',
                'name' => 'url',
                'label' => 'Url',
                'className' => "center aligned",

                'searchable' => false,
                'sortable' => true,
            ],
			[
			    'data' => 'deskripsi',
			    'name' => 'deskripsi',
			    'label' => 'Deskripsi',
                'className' => "center aligned",

			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
                'className' => "center aligned",

			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
                'className' => "center aligned",
                
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = Footer::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($url = $request->url) {
            $records->where('url', 'ilike', '%' . $url . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.master.footer.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.master.footer.create');
    }

    public function store(FooterRequest $request)
    {
    	$jenis = new Footer;
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        return Footer::find($id);
    }

    public function edit($id)
    {
    	$record = Footer::find($id);

        return $this->render('backend.master.footer.edit', ['record' => $record]);
    }

    public function update(FooterRequest $request, $id)
    {
    	$jenis = Footer::find($id);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function destroy($id)
    {
    	$jenis = Footer::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
