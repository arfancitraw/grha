<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\KuesionerPeriode;

use App\Http\Requests\Master\KuesionerPeriodeRequest;

use Datatables;

class KuesionerPeriodeController extends Controller
{
    //
    protected $link = 'backend/master/kuesioner-periode/';
    protected $perms  = '';

    function __construct()
    {
      $this->setLink($this->link);
      $this->setPerms($this->perms);

      $this->setTitle("Kuesioner Periode");
      $this->setSubtitle("Kuesioner Periode");
      $this->setModalSize("tiny");
      $this->setBreadcrumb(['Kuesioner Periode' => '#', 'Kuesioner Periode' => '#']);
      $this->setTableStruct([
       [
           'data' => 'num',
           'name' => 'num',
           'label' => '#',
           'orderable' => false,
           'searchable' => false,
           'className' => "center aligned",
           'width' => '40px',
       ],
       /* --------------------------- */
       [
           'data' => 'periode',
           'name' => 'periode',
           'label' => 'Periode',
           'searchable' => false,
           'sortable' => true,
       ],
       [
        'data' => 'status',
        'name' => 'status',
        'label' => 'Status',
        'searchable' => false,
        'sortable' => true,
        'className'=> "center aligned",
    ],
    [
       'data' => 'created_at',
       'name' => 'created_at',
       'label' => 'Tanggal Entry',
       'searchable' => false,
       'sortable' => true,
   ],
   [
       'data' => 'created_by',
       'name' => 'created_by',
       'label' => 'Oleh',
       'searchable' => false,
       'sortable' => true,
   ],
   [
       'data' => 'action',
       'name' => 'action',
       'label' => 'Aksi',
       'searchable' => false,
       'sortable' => false,
       'className' => "center aligned",
       'width' => '150px',
   ]
]);
  }

  public function grid(Request $request)
  {
      $records = KuesionerPeriode::with('creator')
      ->select('*');
		//Init Sort
      if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
        $records->orderBy('created_at', 'desc');
    }

        //Filters
    if ($periode = $request->periode) {
        $records->where('periode', 'ilike', '%' . $periode . '%');
    }
 
    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
        return $request->get('start');
    })
    ->addColumn('status', function ($record) {
        $string = '';
        if ($record->status > 0) {
           $string .= '<div class="ui purple horizontal label">Aktif</div>';
        }else{
           $string .= '<div class="ui red horizontal label">Tidak Aktif</div>';
        }
        return $string;
    })
    ->addColumn('created_at', function ($record) {
        return $record->created_at->format('d F Y');
    })
    ->addColumn('created_by', function ($record) {
        return $record->creator->name;
    })
    ->addColumn('action', function ($record) {
        $btn = '';
                //Edit
        $btn .= $this->makeButton([
         'type' => 'edit-page',
         'id'   => $record->id
     ]);
                // Delete
        $btn .= $this->makeButton([
         'type' => 'delete',
         'id'   => $record->id
     ]);

        return $btn;
    })
    ->rawColumns(['action','status'])
    ->make(true);
}

public function index()
{
    return $this->render('backend.master.kuesioner-periode.index', ['mockup' => false]);
}

public function create()
{
    return $this->render('backend.master.kuesioner-periode.create');
}

public function store(KuesionerPeriodeRequest $request)
{
 $jenis = new KuesionerPeriode;
 $jenis->fill($request->all());
 $jenis->save();

 return response([
  'status' => true,
  'data'	=> $jenis
]);
}

public function show($id)
{
    return KuesionerPeriode::find($id);
}

public function edit($id)
{
 $record = KuesionerPeriode::find($id);

 return $this->render('backend.master.kuesioner-periode.edit', ['record' => $record]);
}

public function update(KuesionerPeriodeRequest $request, $id)
{
 $jenis = KuesionerPeriode::find($id);
 $jenis->fill($request->all());
 $jenis->save();

 return response([
  'status' => true,
  'data'	=> $jenis
]);
}

public function destroy($id)
{
 $jenis = KuesionerPeriode::find($id);
 $jenis->delete();

 return response([
  'status' => true,
]);
}
}
