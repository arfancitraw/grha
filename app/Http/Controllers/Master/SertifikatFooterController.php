<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\SertifikatFooter;

use App\Http\Requests\Master\SertifikatFooterRequest;

use Datatables;

class SertifikatFooterController extends Controller
{
    //
  protected $link = 'backend/master/sertifikat-footer/';
  protected $perms  = '';

  function __construct()
  {
    $this->setLink($this->link);
    $this->setPerms($this->perms);

    $this->setTitle("Sertifikat Footer ");
    $this->setSubtitle("Master");
    $this->setModalSize("small");
    $this->setBreadcrumb(['Sertifikat Footer' => '#', 'Sertifikat Footer' => '#']);
    $this->setTableStruct([
     [
       'data' => 'num',
       'name' => 'num',
       'label' => '#',
       'orderable' => false,
       'searchable' => false,
       'className' => "center aligned",
       'width' => '40px',
     ],
     /* --------------------------- */
     [
       'data' => 'nama',
       'name' => 'nama',
       'label' => 'Nama',
       'searchable' => false,
       'sortable' => true,
       'className' => 'center aligned'
     ],
     [
      'data' => 'gambar',
      'name' => 'gambar',
      'label' => 'Gambar',
      'searchable' => false,
      'sortable' => true,
      'className' => "center aligned",
    ],
    [
     'data' => 'keterangan',
     'name' => 'keterangan',
     'label' => 'Keterangan',
     'searchable' => false,
     'sortable' => true,
     'className' => 'center aligned'
   ],
   [
     'data' => 'created_at',
     'name' => 'created_at',
     'label' => 'Tanggal Entry',
     'searchable' => false,
     'sortable' => true,
     'className' => 'center aligned'
   ],
   [
     'data' => 'created_by',
     'name' => 'created_by',
     'label' => 'Oleh',
     'searchable' => false,
     'sortable' => true,
     'className' => 'center aligned'
   ],
   [
     'data' => 'action',
     'name' => 'action',
     'label' => 'Aksi',
     'searchable' => false,
     'sortable' => false,
     'className' => "two wide column center aligned",
   ]
 ]);
  }

  public function grid(Request $request)
  {
    $records = SertifikatFooter::with('creator')
    ->select('*');
		//Init Sort
    if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
      $records->orderBy('nama', 'asc');
    }

        // Filters
    if ($link = $request->link) {
      $records->where('keterangan', $link);
    }
    if ($nama = $request->nama) {
      $records->where('nama', $nama );
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
      return $request->get('start');
    })
    ->addColumn('gambar', function ($record) {
      $string ='';
      if($record->gambar){
        $string = '<img src="'.asset('storage').'/'.$record->gambar.'" alt="icon" width="80">';
      }
      return $string;
    })
    ->addColumn('created_at', function ($record) {
      return $record->created_at->format('d F Y');
    })
    ->addColumn('created_by', function ($record) {
      return $record->creator->name;
    })
    ->addColumn('action', function ($record) {
      $btn = '';
                //Edit
      $btn .= $this->makeButton([
       'type' => 'edit',
       'id'   => $record->id
     ]);
                // Delete
      $btn .= $this->makeButton([
       'type' => 'delete',
       'id'   => $record->id
     ]);

      return $btn;
    })
    ->rawColumns(['gambar','action','status'])
    ->make(true);
  }

  public function index()
  {
    return $this->render('backend.master.sertifikat-footer.index', ['mockup' => false]);
  }

  public function create()
  {
    return $this->render('backend.master.sertifikat-footer.create');
  }

  public function store(SertifikatFooterRequest $request)
  {
    $row = $request->all();
    if($request->hasFile('gambar')){
      $path = $request->file('gambar')->store('uploads/master/sertifikat-footer', 'public');
      $row['gambar'] = $path;
    }

    $data = new SertifikatFooter;
    $data->fill($row);
    $data->save();

    return response([
      'status' => true,
      'data'  => $data
    ]);
  }
  public function show($id)
  {
    return SertifikatFooter::find($id);
  }

  public function edit($id)
  {
   $record = SertifikatFooter::find($id);

   return $this->render('backend.master.sertifikat-footer.edit', ['record' => $record]);
 }

 public function update(SertifikatFooterRequest $request, $id)
 {
  $row = $request->all();

  $data = SertifikatFooter::find($id);
  $row['gambar'] = $data->gambar;

  if($request->hasFile('gambar')){
    $path = $request->file('gambar')->store('uploads/dokter/list-dokter', 'public');
    $row['gambar'] = $path;
  }
  $data->fill($row);
  $data->save();

  return response([
    'status' => true,
    'data'  => $data
  ]);
}

public function destroy($id)
{
 $jenis = SertifikatFooter::find($id);
 $jenis->delete();

 return response([
  'status' => true,
]);
}
}
