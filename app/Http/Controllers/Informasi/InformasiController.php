<?php

namespace App\Http\Controllers\Informasi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Informasi\Informasi;
use App\Models\Informasi\Lampiran;
use App\Models\Model;

use App\Http\Requests\Informasi\InformasiRequest;

use Datatables;
use Storage;

class InformasiController extends Controller
{
    //
    protected $link = 'backend/informasi/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("List Konten Informasi");
        $this->setSubtitle("List Konten Informasi");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Informasi' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "one wide column center aligned",
			],
			/* --------------------------- */
			[
			    'data' => 'judul',
			    'name' => 'judul',
			    'label' => 'Judul',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'left aligned',
			],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
            ],
            [
                'data' => 'lampiran',
                'name' => 'lampiran',
                'label' => 'Jumlah Lampiran',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned',
                'width' => '100px'
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'two wide column center aligned',
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'two wide column center aligned',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
    {
        $records = Informasi::with('creator')->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            // $records->orderBy('judul', 'asc');
            $records->orderBy('posisi');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($keterangan = $request->keterangan) {
            $records->where('keterangan', 'ilike', '%' . $keterangan . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            // ->addColumn('konten', function ($record) {
            //     $string ='<span class="ccount more" data-ccount="118">'.strip_tags($record->konten).'</span>';
            //     return $string;
            // })
            ->addColumn('lampiran', function ($record) {
                return count($record->lampiran);
            })

            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['action','konten'])
            ->make(true);
    }

    public function index()
    {
        $this->pushBreadCrumb(['List Informasi' => '#']);
        return $this->render('backend.informasi.informasi.index',[
            'data' => Informasi::orderBy('posisi')->get(),
            'mockup' => false,
        ]);
    }

    public function create()
    {
		$this->setTitle("Tambah Konten Informasi");
        $this->pushBreadCrumb(['Tambah Informasi' => '#']);
        return $this->render('backend.informasi.informasi.create');
    }

    public function store(InformasiRequest $request)
    {
        // dd($request->all());
        $row = $request->all();
        $row['gambar'] = $request->photo;
        
        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/informasi', 'public');
            $row['gambar'] = $path;
        }

        $data = new Informasi;
        $data->fill($row);
        $data->save();

        if($lampiran = $request->lampiran){
            foreach ($lampiran as $sub) {
                $file = new Lampiran;
                $path = $sub['file']->store('uploads/informasi/'.$data->slug, 'public');
                $file->judul = $sub['judul'];
                $file->file = $path;
                $data->lampiran()->save($file);
            }
        }
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return Informasi::find($id);
    }

    public function edit($id)
    {
		$this->setTitle("Ubah Konten Informasi");
        $this->pushBreadCrumb(['Ubah Informasi' => '#']);
        // dd($id);
        $record = Informasi::find($id);
        // dd($record);
        return $this->render('backend.informasi.informasi.edit', ['record' => $record]);
    }

    public function update(InformasiRequest $request, $id)
    {
        $row = $request->all();
        $data = Informasi::find($id);
        $row['gambar'] = $data->gambar;
        $row['slug'] = $this->setSlugify($request->judul);
        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/informasi', 'public');
            $row['gambar'] = $path;
        }
        // dd($row);
        $data->fill($row);
        $data->save();

        if($request->subid){
            $unlink = $data->lampiran()->whereNotIn('id', $request->subid)->get();
            if($unlink->count() > 0){
                foreach ($unlink as $key => $image) {
                    Storage::delete($image->file);
                    $image->delete();
                }
            }
        }else{
            if($data->lampiran->count() > 0){
                foreach ($data->lampiran as $image) {
                    Storage::delete($image->file);
                    $image->delete();
                }
            }
        }

        if($lampirans = $request->lampiran){
            foreach ($lampirans as $lampiran) {
                if(!is_null($lampiran['judul'])){
                    $row = (array_key_exists('id', $lampiran) && $id = $lampiran['id']) ? Lampiran::find($id) : new Lampiran;
                    $row->judul = $lampiran['judul'];
                    if(!is_null($lampiran['file'])){
                        if($unlink = $row->file){
                            Storage::disk('public')->delete($unlink);
                        }
                        $path = $lampiran['file']->store('uploads/informasi/'.$data->slug, 'public');
                        $row->file = $path;
                    }
                    $data->lampiran()->save($row);
                }
            }
        }
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function removeImage($id)
    {
        $data = Informasi::find($id);
        Storage::delete($data->gambar);
        $data->gambar = null;
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function destroy($id)
    {
        $informasi = Informasi::find($id);
        $informasi->fileDelete();
        $informasi->delete();

        return response([
            'status' => true,
        ]);
    }

    public function sort(Request $request)
    {
        if($request->sorting){
            foreach ($request->sorting as $position => $id) {
                $informasi = Informasi::find($id);
                $informasi->posisi = $position;
                $informasi->save();
            }
                
            return response([
                'status' => true,
            ]);
        }

        return response([
            'status' => false,
        ], 422);
    }
}
