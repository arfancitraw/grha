<?php

namespace App\Http\Controllers\Pengaduan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Pengaduan\PengaduanSponsor;

use Datatables;
use Storage;

class PengaduanSponsorController extends Controller
{
    //
    protected $link = 'backend/pengaduan/sponsor/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Pengaduan Sponsor");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Pengaduan' => '#', 'Sponsor' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama_kegiatan',
                'name' => 'nama_kegiatan',
                'label' => 'Nama Kegiatan',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'nip_nopeg',
                'name' => 'nip_nopeg',
                'label' => 'Nip / Nopeg',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'jabatan',
                'name' => 'jabatan',
                'label' => 'Jabatan',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'alamat',
                'name' => 'alamat',
                'label' => 'Alamat',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '70px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = PengaduanSponsor::select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }


        $link = $this->link;
        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
           return $request->get('start');
       })
        ->addColumn('created_at', function ($record) {
           return $record->created_at->format('d F Y');
       })
        ->addColumn('action', function ($record) use ($link){
            $btn = '';
                            //detail
            $btn .= $this->makeButton([
                'type' => 'url',
                'class' => 'blue icon',
                'label' => '<i class="clipboard icon"></i>',
                'tooltip' => 'Detail PengaduanSponsor',
                'target' => url($link.$record->id)
            ]);

                            // Delete
            $btn .= $this->makeButton([
                'type' => 'delete',
                'id'   => $record->id
            ]);
            return $btn;
        })
        ->make(true);
    }

    public function index()
    {
        return $this->render('backend.pengaduan.sponsor.index', ['mockup' => false]);
    }

    public function show($id)
    {
        $record = PengaduanSponsor::find($id);
        return $this->render('backend.pengaduan.sponsor.detail', ['record' => $record]);
    }


    public function destroy($id)
    {
        $jenis = PengaduanSponsor::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
