<?php

namespace App\Http\Controllers\Pengaduan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Pengaduan\Pengaduan;

use Datatables;
use Storage;

class PengaduanEtikHukumController extends Controller
{
    //
    protected $link = 'backend/pengaduan/etik-hukum/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Pengaduan Etik & Hukum");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Pengaduan' => '#', 'Etik & Hukum' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'perihal',
                'name' => 'perihal',
                'label' => 'Perihal',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'telepon',
                'name' => 'telepon',
                'label' => 'Telepon',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '70px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Pengaduan::select('*')->where('tipe',0);
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        if($perihal = $request->perihal){
            $records->where('perihal', 'ilike', '%' . $perihal . '%');
        }

        if($nama = $request->nama){
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        if($telepon = $request->telepon){
            $records->where('telepon', 'ilike', '%' . $telepon . '%');
        }

        if($email = $request->email){
            $records->where('email', 'ilike', '%' . $email . '%');
        }


        $link = $this->link;
        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
           return $request->get('start');
       })
        ->addColumn('created_at', function ($record) {
           return $record->created_at->format('d F Y');
       })
        ->addColumn('action', function ($record) use ($link){
            $btn = '';
                            //detail
            $btn .= $this->makeButton([
                'type' => 'url',
                'class' => 'blue icon',
                'label' => '<i class="clipboard icon"></i>',
                'tooltip' => 'Detail Pengaduan',
                'target' => url($link.$record->id)
            ]);

                            // Delete
            $btn .= $this->makeButton([
                'type' => 'delete',
                'id'   => $record->id
            ]);
            return $btn;
        })
        ->make(true);
    }

    public function index()
    {
        return $this->render('backend.pengaduan.etik-hukum.index', ['mockup' => false]);
    }

    public function show($id)
    {
        $record = Pengaduan::find($id);
        return $this->render('backend.pengaduan.etik-hukum.detail', ['record' => $record]);
    }


    public function destroy($id)
    {
        $jenis = Pengaduan::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
