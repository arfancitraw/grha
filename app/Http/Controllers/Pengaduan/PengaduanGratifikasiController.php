<?php

namespace App\Http\Controllers\Pengaduan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Pengaduan\PengaduanGratifikasi;

use Datatables;
use Storage;

class PengaduanGratifikasiController extends Controller
{
    //
    protected $link = 'backend/pengaduan/gratifikasi/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Pengaduan Gratifikasi");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Pengaduan' => '#', 'Gratifikasi' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'jenis_bentuk_penerimaan',
                'name' => 'jenis_bentuk_penerimaan',
                'label' => 'Jenis / Bentuk Penerimaan',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'nip_nopeg',
                'name' => 'nip_nopeg',
                'label' => 'Nip / Nopeg',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'jabatan_unit_kerja',
                'name' => 'jabatan_unit_kerja',
                'label' => 'Jabatan / Unit Kerja',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'alamat_telepon_email',
                'name' => 'alamat_telepon_email',
                'label' => 'Alamat / Telepon / Email',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '70px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = PengaduanGratifikasi::select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }


        $link = $this->link;
        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
           return $request->get('start');
       })
        ->addColumn('created_at', function ($record) {
           return $record->created_at->format('d F Y');
       })
        ->addColumn('action', function ($record) use ($link){
            $btn = '';
                            //detail
            $btn .= $this->makeButton([
                'type' => 'url',
                'class' => 'blue icon',
                'label' => '<i class="clipboard icon"></i>',
                'tooltip' => 'Detail PengaduanGratifikasi',
                'target' => url($link.$record->id)
            ]);

                            // Delete
            $btn .= $this->makeButton([
                'type' => 'delete',
                'id'   => $record->id
            ]);
            return $btn;
        })
        ->make(true);
    }

    public function index()
    {
        return $this->render('backend.pengaduan.gratifikasi.index', ['mockup' => false]);
    }

    public function show($id)
    {
        $record = PengaduanGratifikasi::find($id);
        return $this->render('backend.pengaduan.gratifikasi.detail', ['record' => $record]);
    }


    public function destroy($id)
    {
        $jenis = PengaduanGratifikasi::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
