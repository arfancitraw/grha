<?php

namespace App\Http\Controllers\Pengaduan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Setting\Setting;

use Datatables;
use Storage;

class NarasiController extends Controller
{
    //
    protected $link = 'backend/pengaduan/narasi/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);
        $this->setTitle("Narasi Pengaduan");
        
        $this->setModalSize("large");
        $this->setBreadcrumb(['Pengaduan' => '#', 'Narasi' => '#']);
    }

    public function index()
    {
        $settings = Setting::all()
                           ->mapWithKeys(function ($item) {
                               return [$item->kunci => $item];
                           });

        return $this->render('backend.pengaduan.narasi', ['mockup' => false,
            'settings' => $settings
        ]);
    }

    public function store(Request $request)
    {
        foreach ($request->setting as $id => $value) {
            // dd($value);
            $row = Setting::find($id);
            $row->fill($value);
            $row->save();
        }
        
        return response([
            'status' => true
        ]);
    }

}
