<?php

namespace App\Http\Controllers\Mutu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Mutu\InfoMutu;

use App\Http\Requests\Mutu\InfoMutuRequest;

use Datatables;
use Storage;

class InfoMutuController extends Controller
{
    //
    protected $link = 'backend/mutu/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Info Mutu");
        $this->setSubtitle("Mutu");
		$this->setModalSize("medium");
		$this->setBreadcrumb(['Mutu' => '#', 'Info Mutu' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
            [
                'data' => 'jenis_mutu',
                'name' => 'jenis_mutu',
                'label' => 'Jenis Mutu',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'gambar',
                'name' => 'gambar',
                'label' => 'Gambar',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'periode',
                'name' => 'periode',
                'label' => 'Periode',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = InfoMutu::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('gambar', function ($record) {
                $string ='';
                if($record->gambar){
                    $string = '<img src="'.asset('storage').'/'.$record->gambar.'" alt="icon" width="80">';
                }
                return $string;
            })
            ->addColumn('deskripsi', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->deskripsi).'</span>';
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('jenis_mutu', function ($record) {
                return $record->jenismutu->nama;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                 $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;

            })
            ->rawColumns(['gambar','action','status', 'deskripsi'])
            ->make(true);

	}

    public function index()
    {
        return $this->render('backend.mutu.info-mutu.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.mutu.info-mutu.create');
    }

    public function store(InfoMutuRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/mutu/info-mutu', 'public');
            $row['gambar'] = $path;
        }

        $data = new InfoMutu;
        $data->fill($row);
        $data->save();
        
        return redirect($this->link);
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function show($id)
    {
        return InfoMutu::find($id);
    }

    public function edit($id)
    {
    	$record = InfoMutu::find($id);
        return $this->render('backend.mutu.info-mutu.edit', ['record' => $record]);
    }

    public function update(InfoMutuRequest $request, $id)
    {
        $row = $request->all();
        
        $data = InfoMutu::find($id);

        $row['gambar'] = $data->gambar;

        if($request->hasFile('gambar')){
            $path = $request->file('gambar')->store('uploads/mutu/info-mutu', 'public');
            $row['gambar'] = $path;
        }

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function removeImage($id)
    {
        $data = InfoMutu::find($id);
        Storage::delete($data->gambar);
        $data->gambar = null;
        $data->save();

        return response([
        'status' => true,
        'data'  => $data
        ]);
    }
    public function destroy($id)
    {
    	$jenis = InfoMutu::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
