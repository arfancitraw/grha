<?php

namespace App\Http\Controllers\API\MediaCenter;

use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;
use App\Models\MediaCenter\Pustaka;
use App\Models\MediaCenter\Penyakit;
use App\Models\MediaCenter\Tindakan;
use App\Http\Requests\MediaCenter\PustakaRequest;


class PustakaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new QueryBuilder(new Pustaka, $request);

        if($page = $request->page){
            return $data->build()->paginate();
        }

        return response()->json([
            'status' => true,
            'data' => $data->build()->get()
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function penyakit(Request $request)
    {
        $data = new QueryBuilder(new Penyakit, $request);

        if($page = $request->page){
            return $data->build()->paginate();
        }

        return response()->json([
            'status' => true,
            'data' => $data->build()->get()
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tindakan(Request $request)
    {
        $data = new QueryBuilder(new Tindakan, $request);

        if($page = $request->page){
            return $data->build()->paginate();
        }

        return response()->json([
            'status' => true,
            'data' => $data->build()->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PustakaRequest $request)
    {
        try {
            $data = new Pustaka;
            $data->fill($request->all());
            $data->save();
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Pustaka::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PustakaRequest $request, $id)
    {
        try {
            $data = Pustaka::find($id);
            $data->fill($request->all());
            $data->save();
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 
        
        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = Pustaka::find($id);
            $data->delete();    
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response()->json([
            'status' => true
        ]);
    }
}
