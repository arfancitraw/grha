<?php

namespace App\Http\Controllers\API\MediaCenter;

use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;
use App\Models\MediaCenter\Artikel;
use App\Http\Requests\MediaCenter\PengumumanRequest;


class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Artikel::whereRaw('LOWER("judul") ILIKE ? ',['%'.trim(strtolower($request->judul)).'%']);

        // if($page = $request->page){
        //     return $data->build()->paginate();
        // }

        return response()->json([
            'status' => true,
            'data' => $data->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PengumumanRequest $request)
    {
        try {
            $data = new Artikel;
            $data->fill($request->all());
            $data->save();
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Artikel::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PengumumanRequest $request, $id)
    {
        try {
            $data = Artikel::find($id);
            $data->fill($request->all());
            $data->save();
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 
        
        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = Artikel::find($id);
            $data->delete();    
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response()->json([
            'status' => true
        ]);
    }
}
