<?php

namespace App\Http\Controllers\API\Dokter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Unlu\Laravel\Api\QueryBuilder;

use App\Models\TanyaJawab\TanyaJawab;
use App\Models\TanyaJawab\Detail;
use App\Models\TanyaJawab\Access;
use App\Models\Master\KategoriTanyaJawab;

use App\Http\Requests\InfoPasien\TanyaJawabRequest;


class TanyaJawabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new QueryBuilder(new KategoriTanyaJawab, $request);

        if($page = $request->page){
            return $data->build()->paginate();
        }

        return response()->json([
            'status' => true,
            'data' => $data->build()->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TanyaJawabRequest $request)
    {
        try {
            $data = new TanyaJawab;
            $data->fill($request->all());
            $data->save();
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $record = TanyaJawab::with(['detail', 'access'])->find($id);
        
        if(is_null($record))
            return response([
                'status' => false
            ], 404);

        $access = $record->access->where('ip', \Request::ip())->first();
        if(is_null($access)){
            $access = new Access;
        }
        
        $access->ip = \Request::ip();
        $record->access()->save($access);
        
        return response()->json([
            'status' => true,
            'data' => $record
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $row = $request->all();
            $data = TanyaJawab::find($id);
            
            $detail = new Detail;
            $detail->fill($row);
            $data->detail()->save($detail);
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 
        
        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = TanyaJawab::find($id);
            $data->delete();    
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response()->json([
            'status' => true
        ]);
    }
}
