<?php

namespace App\Http\Controllers\API\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\SertifikatFooter;
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;

class SertifikatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new QueryBuilder(new SertifikatFooter, $request);

        if($page = $request->page){
            return $data->build()->paginate();
        }

        return response()->json([
            'status' => true,
            'data' => $data->build()->get()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return SertifikatFooter::find($id);
    }
}
