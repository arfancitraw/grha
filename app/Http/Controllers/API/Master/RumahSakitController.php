<?php

namespace App\Http\Controllers\API\Master;

use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;
use App\Models\Master\RumahSakit;
use App\Http\Requests\Master\RumahSakitRequest;


class RumahSakitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new QueryBuilder(new RumahSakit, $request);

        if($page = $request->page){
            return $data->build()->paginate();
        }

        return response()->json([
            'status' => true,
            'data' => $data->build()->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RumahSakitRequest $request)
    {
        try {
            $data = new RumahSakit;
            $data->fill($request->all());
            $data->save();
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return RumahSakit::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RumahSakitRequest $request, $id)
    {
        try {
            $data = RumahSakit::find($id);
            $data->fill($request->all());
            $data->save();
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 
        
        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = RumahSakit::find($id);
            $data->delete();    
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response()->json([
            'status' => true
        ]);
    }
}
