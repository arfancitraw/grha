<?php

namespace App\Http\Controllers\API\Master;

use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;
use App\Models\Layanan\Layanan;
use App\Http\Requests\Layanan\LayananRequest;


class LayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new QueryBuilder(new Layanan, $request);

        if($page = $request->page){
            $resp = $data->build()->paginate();
            $resp->getCollection()->transform(function ($item) {
                return [
                    'id' => $item->id,
                    'judul' => $item->judul,
                    'photo' => $item->photo,
                    'photo_url' => $item->photo_url,
                    'keterangan' => $item->keterangan,
                    'slug' => $item->slug,
                    'posisi' => $item->posisi,
                ];
            });
            return $resp;
        }

        $resp = $data->build()->get()->map(function($item){
            return [
                'id' => $item->id,
                'judul' => $item->judul,
                'photo' => $item->photo,
                'photo_url' => $item->photo_url,
                'keterangan' => $item->keterangan,
                'slug' => $item->slug,
                'posisi' => $item->posisi,
            ];
        });
        return response()->json([
            'status' => true,
            'data' => $resp
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LayananRequest $request)
    {
        try {
            $data = new Layanan;
            $data->fill($request->all());
            $data->save();
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Layanan::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LayananRequest $request, $id)
    {
        try {
            $data = Layanan::find($id);
            $data->fill($request->all());
            $data->save();
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 
        
        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = Layanan::find($id);
            $data->delete();    
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response()->json([
            'status' => true
        ]);
    }
}
