<?php

namespace App\Http\Controllers\API\Kuesioner;

use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use App\Http\Controllers\Controller;
use App\Models\Kuesioner\Periode;
use App\Models\Kuesioner\KuesionerJawaban;
use App\Models\Kuesioner\Detail;
use App\Models\Kuesioner\KuesionerJawabanDetail;
use App\Http\Requests\Kuesioner\KuesionerRequest;


class KuesionerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Periode::with(['detail' => function($q){
                            $q->orderBy('id', 'ASC');
                        }])
                       ->where('status', 1)
                       ->first();

        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $row = $request->all();
       //dd($row);
        $data = new KuesionerJawaban;
        $data->fill($request->all());
        $data->save();
        $data->savekepuasanLayanan($data->id,$request->all());

        return response()->json([
            'status' => true,
            'data'  => $data
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Periode::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KuesionerRequest $request, $id)
    {
        try {
            $data = Kuesioner::find($id);
            $data->fill($request->all());
            $data->save();
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 
        
        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = Kuesioner::find($id);
            $data->delete();    
        } catch (QueryException $e) {
            return response([
                'status' => false,
                'errors' => [
                    'message' => $e->errorInfo[2],
                    'code' => $e->errorInfo[1],
                ]
            ], 400);
        } 

        return response()->json([
            'status' => true
        ]);
    }

    public function nilaiKuesioner(Request $request){
        $row = $request->all();
       //dd($row);
        $data = new KuesionerJawaban;
        $data->fill($request->all());
        $data->save();
        $data->savekepuasanLayanan($data->id,$request->all());
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
}
