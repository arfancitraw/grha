<?php

namespace App\Http\Controllers\RujukanNasional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\RujukanNasional\RujukanNasional;

use App\Http\Requests\RujukanNasional\RujukanNasionalRequest;

use Datatables;
use Storage;

class RujukanNasionalController extends Controller
{
    //
    protected $link = 'backend/rujukan-nasional/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("List Rujukan Nasional");
        $this->setSubtitle("Rujukan Nasional");
		$this->setModalSize("");
		$this->setBreadcrumb(['Rujukan Nasional' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'judul',
			    'name' => 'judul',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'photo',
			    'name' => 'photo',
			    'label' => 'Photo',
			    'searchable' => false,
			    'sortable' => true,
                'className' => "center aligned",
			],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'home',
                'name' => 'home',
                'label' => 'Tampil di <i class="home icon"></i>',
                'searchable' => false,
                'sortable' => true,
                'className' => "one wide column center aligned"
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'className' => "center aligned",
                'searchable' => false,
                'sortable' => true,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = RujukanNasional::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('judul', 'ilike', '%' . $nama . '%');
        }
        if ($status = $request->status) {
            $records->where('status',$status);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('photo', function ($record) {
                $string ='';
                if($record->photo){
                    $string = '<img src="'.asset('storage').'/'.$record->photo.'" alt="icon" width="80">';
                }
                return $string;
            })
           
            ->addColumn('status', function ($record) {
                $string ='<span class="ui fluid orange label">Draft</span>';
                if($record->status==1){
                    $string = '<span class="ui fluid green label">Published</span>';
                }
                return $string;
            })
            ->editColumn('home', function ($record) {
                return $record->home 
                     ? '<span class="ui green label">Tampil</span>'
                     : '<span class="ui red label">Tidak</span>';
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit-page',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['photo','action','status', 'home'])
            ->make(true);
	}

    public function index()
    {
        $this->pushBreadcrumb(['List Rujukan Nasional' => '#']);
        return $this->render('backend.rujukan-nasional.index', ['mockup' => false]);
    }

    public function create()
    {
        $this->setTitle('Tambah Rujukan Nasional');
        $this->pushBreadcrumb(['Tambah Rujukan Nasional' => '#']);
        return $this->render('backend.rujukan-nasional.create');
    }

    public function store(RujukanNasionalRequest $request)
    {
        
        $row = $request->all();
        // dd($row);
        // $row['photo'] = $request->photo;
        // $row['icon'] = $request->icon;
        
        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/rujukan-nasional', 'public');
            $row['photo'] = $path;
        }
        if($request->hasFile('icon')){
            $path = $request->file('icon')->store('uploads/rujukan-nasional', 'public');
            $row['icon'] = $path;
        }

        $data = new RujukanNasional;
        $data->fill($row);
    	$data->save();

        if($tim = $request->dokter){
            $dokter = [];
            foreach ($tim as $sub) {
                $dokter[$sub['id_dokter']] = [
                    'posisi' => $sub['posisi'], 
                    'created_by' => auth()->user()->id,
                    'updated_by' => auth()->user()->id
                ];
            }
            $data->dokter()->sync($dokter);
        }
        
        // $id = $data->id;
        // $data->RujukanProduk($id, $row);
        // if(!is_null($request->id_dokter )){
        // $data->RujukanTim($id, $row);
        // }
        // $data->RujukanAgenda($id, $row);
    	return response([
    		'status' => true,
    		'data'	=> $data
    	]);
    }

    public function show($id)
    {
        return RujukanNasional::find($id);
    }

    public function edit($id)
    {

    	$record = RujukanNasional::with(['dokter' => function($q){
            return $q->orderByRaw('trans_rujukan_tim.id asc');
        }])->find($id);
        
        return $this->render('backend.rujukan-nasional.edit', [
            'record' => $record,
        ]);
    }

    public function update(RujukanNasionalRequest $request, $id)
    {
        // dd($request->all());
        $row = $request->all();
    	$rujukan = RujukanNasional::find($id);
        $row['photo'] = $rujukan->photo;        
        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/rujukan-nasional', 'public');
            $row['photo'] = $path;
        }
        $row['icon'] = $rujukan->icon;        
        if($request->hasFile('icon')){
            $path = $request->file('icon')->store('uploads/rujukan-nasional', 'public');
            $row['icon'] = $path;
        }

        $rujukan->fill($row);
        $rujukan->save();

        if($tim = $request->dokter){
            $dokter = [];
            foreach ($tim as $sub) {
                if(!is_null($sub['id_dokter'])){
                    $dokter[$sub['id_dokter']] = [
                        'posisi' => $sub['posisi'], 
                        'created_by' => auth()->user()->id,
                        'updated_by' => auth()->user()->id
                    ];
                }
            }

            if(count($dokter) > 1){
                $rujukan->dokter()->sync($dokter);
            }
        }
        // $id = $rujukan->id;
        // $rujukan->RujukanProdukEdit($id, $row);
        // $rujukan->RujukanTimEdit($id, $row);
        // $rujukan->RujukanAgendaEdit($id, $row);

    	return response([
    		'status' => true,
    		'data'	=> $rujukan
    	]);
    }

    public function removeImage($id)
    {
        $data = RujukanNasional::find($id);
        Storage::delete($data->photo);
        $data->photo = null;
        $data->save();

        return response([
        'status' => true,
        'data'  => $data
        ]);
    }
    
    public function destroy($id)
    {
    	$jenis = RujukanNasional::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }

    public function download($id)
    {
        $check = RujukanNasional::find($id);
        if ($check->photo == true){
             if(file_exists(public_path('storage/'.$check->photo)))
        {
          return response()->download(public_path('storage/'.$check->photo), $check->filename);
        }

        return $this->render('failed.file');
        }
       
    }
}
