<?php

namespace App\Http\Controllers\Dokter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Dokter\JadwalDokter;

use App\Http\Requests\Dokter\JadwalDokterRequest;

use Datatables;

class JadwalDokterController extends Controller
{
    //
    protected $link = 'backend/dokter/jadwal-dokter/';
    protected $perms  = '';

    function __construct()
    {
      $this->setLink($this->link);
      $this->setPerms($this->perms);

      $this->setTitle("Jadwal Dokter");
      $this->setSubtitle("Tim Dokter");
      $this->setModalSize("tiny");
      $this->setBreadcrumb(['Tim Dokter' => '#', 'Jadwal Dokter' => '#']);
      $this->setTableStruct([
       [
           'data' => 'num',
           'name' => 'num',
           'label' => '#',
           'orderable' => false,
           'searchable' => false,
           'className' => "center aligned",
           'width' => '40px',
       ],
       /* --------------------------- */
       [
           'data' => 'list_dokter_id',
           'name' => 'list_dokter_id',
           'label' => 'Nama Dokter',
           'searchable' => false,
           'sortable' => true,
           'className' => 'center aligned'
       ],
       [
        'data' => 'hari_praktek',
        'name' => 'hari_praktek',
        'label' => 'Hari Praktek',
        'searchable' => false,
        'sortable' => true,
        'className' => 'center aligned'
    ],
    [
        'data' => 'jam_mulai_praktek',
        'name' => 'jam_mulai_praktek',
        'label' => 'Jam Mulai Praktek',
        'searchable' => false,
        'sortable' => true,
        'className' => 'center aligned'
    ],
    [
        'data' => 'jam_selesai_praktek',
        'name' => 'jam_selesai_praktek',
        'label' => 'Jam Selesai Praktek',
        'searchable' => false,
        'sortable' => true,
        'className' => 'center aligned'
    ],
    [
       'data' => 'created_at',
       'name' => 'created_at',
       'label' => 'Tanggal Entry',
       'searchable' => false,
       'sortable' => true,
       'className' => 'center aligned'
   ],
   [
       'data' => 'created_by',
       'name' => 'created_by',
       'label' => 'Oleh',
       'searchable' => false,
       'sortable' => true,
       'className' => 'center aligned'
   ],
   [
       'data' => 'action',
       'name' => 'action',
       'label' => 'Aksi',
       'searchable' => false,
       'sortable' => false,
       'className' => "two wide column center aligned",
   ]
]);
  }

  public function grid(Request $request)
  {
      $records = JadwalDokter::with('creator')
      ->select('*');
		//Init Sort
      if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
        $records->orderBy('list_dokter_id', 'asc');
    }

        // Filters
    if ($dokter = $request->dokter) {
        $records->where('list_dokter_id', $dokter);
    }
    if ($hari_praktek = $request->hari_praktek) {
        $records->where('hari_praktek', $hari_praktek );
    }

    return Datatables::of($records)
    ->addColumn('num', function ($record) use ($request) {
        return $request->get('start');
    })
    ->addColumn('created_at', function ($record) {
        return $record->created_at->format('d F Y');
    })
    ->addColumn('created_by', function ($record) {
        return $record->creator->name;
    })
    ->addColumn('list_dokter_id', function ($record) {
        return $record->listdokter->nama;
    })
    ->addColumn('hari_praktek', function ($record) {
        switch ($record->hari_praktek) {
            case 1:
            return "Senin";
            break;
            case 2:
            return "Selasa";
            break;
            case 3:
            return "Rabu";
            break;
            case 4:
            return "Kamis";
            break;
            case 5:
            return "Jumat";
            break;
            case 6:
            return "Sabtu";
            break;
            case 7:
            return "Minggu";
            break;
            default:
        }
    })
    ->addColumn('action', function ($record) {
        $btn = '';
                //Edit
        $btn .= $this->makeButton([
         'type' => 'edit',
         'id'   => $record->id
     ]);
                // Delete
        $btn .= $this->makeButton([
         'type' => 'delete',
         'id'   => $record->id
     ]);

        return $btn;
    })
    ->make(true);
}

public function index()
{
    return $this->render('backend.dokter.jadwal-dokter.index', ['mockup' => false]);
}

public function create()
{
    return $this->render('backend.dokter.jadwal-dokter.create');
}

public function store(JadwalDokterRequest $request)
{
    $row = $request->all();

    if($request->hasFile('photo')){
        $path = $request->file('photo')->store('uploads/dokter/jadwal-dokter', 'public');
        $row['photo'] = $path;
    }

    $data = new JadwalDokter;
    $data->fill($row);
    $data->save();

    return response([
        'status' => true,
        'data'  => $data
    ]);
}
public function show($id)
{
    return JadwalDokter::find($id);
}

public function edit($id)
{
 $record = JadwalDokter::find($id);

 return $this->render('backend.dokter.jadwal-dokter.edit', ['record' => $record]);
}

public function update(JadwalDokterRequest $request, $id)
{
    $row = $request->all();

    $data = JadwalDokter::find($id);

    $row['photo'] = $data->photo;

    if($request->hasFile('photo')){
        $path = $request->file('photo')->store('uploads/dokter/jadwal-dokter', 'public');
        $row['photo'] = $path;
    }

    $data->fill($row);
    $data->save();

    return response([
        'status' => true,
        'data'  => $data
    ]);
}

public function destroy($id)
{
 $jenis = JadwalDokter::find($id);
 $jenis->delete();

 return response([
  'status' => true,
]);
}
}
