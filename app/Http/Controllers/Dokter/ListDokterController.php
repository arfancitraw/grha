<?php

namespace App\Http\Controllers\Dokter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\Spesialisasi;
use App\Models\Dokter\ListDokter;
use App\Models\Dokter\JadwalDokter;
use App\Models\Dokter\PendidikanDokter;
use App\Models\Dokter\KeahlianDokter;

use App\Http\Requests\Dokter\ListDokterRequest;

use Datatables;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Storage;

class ListDokterController extends Controller
{
    //
    protected $link = 'backend/dokter/list-dokter/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("List Dokter");
        $this->setSubtitle("Tim Dokter");
		$this->setModalSize("medium");
		$this->setBreadcrumb(['Tim Dokter' => '#', 'List Dokter' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true,
            ],
			[
			    'data' => 'nama_lengkap',
			    'name' => 'nama_lengkap',
			    'label' => 'Nama Lengkap',
			    'searchable' => false,
			    'sortable' => true,
			],
            [
                'data' => 'spesialisasi_id',
                'name' => 'spesialisasi_id',
                'label' => 'Spesialisasi',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'aktif',
                'name' => 'aktif',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
        $records = ListDokter::with('creator')
                           ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('aktif', 'desc')
                    ->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($spesialisasi = $request->spesialisasi) {
            $records->where('spesialisasi_id', $spesialisasi);
        }
        if ($jabatan = $request->jabatan) {
            $records->where('jabatan_dokter_id', $jabatan);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->editColumn('aktif', function ($record) {
                return $record->aktif 
                     ? "Aktif"
                     : "Non-aktif";
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('spesialisasi_id', function ($record) {
                // dd($record->spesialisasi);
                return $record->spesialisasi['nama'];
            })
            ->addColumn('jabatan_dokter_id', function ($record) {
                return $record->jabatandokter['jabatan'];
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                    'class'=> 'orange icon edit-tab',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.dokter.list-dokter.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.dokter.list-dokter.create');
    }

    public function store(ListDokterRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/dokter/list-dokter', 'public');
            $row['photo'] = $path;
        }

        $data = new ListDokter;
        $data->fill($row);
        $data->save();

        if($spesialisasi = $request->spesialisasi){
            foreach ($spesialisasi as $key => $row) {
                $new = new KeahlianDokter();
                $new->nama = $row['nama'];
                $new->tipe = 0;
                $data->keahlian()->save($new);
            }
        }
        if($prosedur = $request->prosedur){
            foreach ($prosedur as $key => $row) {
                $new = new KeahlianDokter();
                $new->nama = $row['nama'];
                $new->tipe = 1;
                $data->keahlian()->save($new);
            }
        }

        if($gelar = $request->gelar){
            foreach ($gelar as $key => $row) {
                $new = new PendidikanDokter();
                $new->nama = $row['nama'];
                $new->tipe = 0;
                $data->pendidikan()->save($new);
            }
        }
        if($pelatihan = $request->pelatihan){
            foreach ($pelatihan as $key => $row) {
                $new = new PendidikanDokter();
                $new->nama = $row['nama'];
                $new->tipe = 2;
                $data->pendidikan()->save($new);
            }
        }
        if($internasional = $request->internasional){
            foreach ($internasional as $key => $row) {
                $new = new PendidikanDokter();
                $new->nama = $row['nama'];
                $new->tipe = 3;
                $data->pendidikan()->save($new);
            }
        }
        if($keanggotaan = $request->keanggotaan){
            foreach ($keanggotaan as $key => $row) {
                $new = new PendidikanDokter();
                $new->nama = $row['nama'];
                $new->tipe = 1;
                $data->pendidikan()->save($new);
            }
        }

        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function show($id)
    {
        return ListDokter::find($id);
    }

    public function edit($id)
    {
    	$record = ListDokter::find($id);

        return $this->render('backend.dokter.list-dokter.edit', ['record' => $record]);
    }

    public function update(ListDokterRequest $request, $id)
    {
        $row = $request->all();
        
        $data = ListDokter::find($id);

        $row['photo'] = $data->photo;

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/dokter/list-dokter', 'public');
            $row['photo'] = $path;
        }

        $data->fill($row);
        $data->save();

        if($request->keahlian){
            $unlink = $data->keahlian()->whereNotIn('id', $request->keahlian)->get();
            if($unlink->count() > 0){
                foreach ($unlink as $key => $rows) {
                    $rows->delete();
                }
            }
        }
        if($request->pendidikan){
            $unlink = $data->pendidikan()->whereNotIn('id', $request->pendidikan)->get();
            if($unlink->count() > 0){
                foreach ($unlink as $key => $rows) {
                    $rows->delete();
                }
            }
        }

        if($spesialisasi = $request->spesialisasi){
            foreach ($spesialisasi as $key => $row) {
                if($row['nama'] != ''){
                    if(array_key_exists('id', $row) && $id = $row['id']){
                        $new = KeahlianDokter::find($id);
                    }else{
                        $new = new KeahlianDokter;
                    }
                    $new->nama = $row['nama'];
                    $new->tipe = 0;
                    $data->keahlian()->save($new);
                }
            }
        }
        if($prosedur = $request->prosedur){
            foreach ($prosedur as $key => $row) {
                if($row['nama'] != ''){
                    if(array_key_exists('id', $row) && $id = $row['id']){
                        $new = KeahlianDokter::find($id);
                    }else{
                        $new = new KeahlianDokter;
                    }
                    $new->nama = $row['nama'];
                    $new->tipe = 1;
                    $data->keahlian()->save($new);
                }
            }
        }

        if($gelar = $request->gelar){
            foreach ($gelar as $key => $row) {
                if($row['nama'] != ''){
                    if(array_key_exists('id', $row) && $id = $row['id']){
                        $new = PendidikanDokter::find($id);
                    }else{
                        $new = new PendidikanDokter;
                    }
                    $new->nama = $row['nama'];
                    $new->tipe = 0;
                    $data->pendidikan()->save($new);
                }
            }
        }
        if($pelatihan = $request->pelatihan){
            foreach ($pelatihan as $key => $row) {
                if($row['nama'] != ''){
                    if(array_key_exists('id', $row) && $id = $row['id']){
                        $new = PendidikanDokter::find($id);
                    }else{
                        $new = new PendidikanDokter;
                    }
                    $new->nama = $row['nama'];
                    $new->tipe = 2;
                    $data->pendidikan()->save($new);
                }
            }
        }
        if($internasional = $request->internasional){
            foreach ($internasional as $key => $row) {
                if($row['nama'] != ''){
                    if(array_key_exists('id', $row) && $id = $row['id']){
                        $new = PendidikanDokter::find($id);
                    }else{
                        $new = new PendidikanDokter;
                    }
                    $new->nama = $row['nama'];
                    $new->tipe = 3;
                    $data->pendidikan()->save($new);
                }
            }
        }
        if($keanggotaan = $request->keanggotaan){
            foreach ($keanggotaan as $key => $row) {
                if($row['nama'] != ''){
                    if(array_key_exists('id', $row) && $id = $row['id']){
                        $new = PendidikanDokter::find($id);
                    }else{
                        $new = new PendidikanDokter;
                    }
                    $new->nama = $row['nama'];
                    $new->tipe = 1;
                    $data->pendidikan()->save($new);
                }
            }
        }
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
    	$jenis = ListDokter::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }

    public function sync()
    {
        $url = config('app.api_url').'jadwal_dokter';
        $client = new Client();
        $result = $client->get($url);

        if($result){
            $data = json_decode($result->getBody());
            $kodes = array_column($data, 'kd_dr');
            $cek = ListDokter::whereNotIn('kode', $kodes)
                             ->update(['aktif' => 0]);

            foreach ($data as $dokter) {
                $cek = ListDokter::where('kode', $dokter->kd_dr)->first();
                $row = $cek ?: new ListDokter();
                //Cek Spesialis
                $cek = Spesialisasi::whereRaw('LOWER(nama) LIKE ? ', '%'.strtolower($dokter->spesialis).'%')->first();
                if($cek){
                    $spec = $cek;
                    // dump($dokter->nama_dr, $spec->id);
                }else{
                    $spec = new Spesialisasi;
                    $spec->nama = ucfirst($dokter->spesialis);
                    $spec->save();
                }
                //EO Spesialis
                //Foto
                // $path = null;
                // $url = $dokter->foto;
                // if($contents = @file_get_contents($url)){
                //     $name = substr($url, strrpos($url, '/') + 1);
                //     Storage::disk('public')->put('uploads/dokter/'.$name, $contents);
                //     $path = 'uploads/dokter/'.$name;
                // }
                //EO Foto

                $row->kode = $dokter->kd_dr;
                $row->nama = $dokter->nama_dr;
                $row->nama_lengkap = $dokter->nama_dr_lengkap;
                $row->spesialisasi_id = $spec->id;
                $row->aktif = 1;
                // $row->spesialisasi_id = $row->spesialisasi_id ?: $spec->id;
                // $row->photo = $path;
                $row->save();

                if($dokter->jadwal){
                    $row->jadwal()->delete();
                    foreach ($dokter->jadwal as $jadwal) {
                        $days = ['SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU', 'MINGGU'];
                        $cek = JadwalDokter::where('list_dokter_id', $row->id)
                                           ->where('hari_praktek', array_search($jadwal->hari, $days))
                                           ->where('jam_mulai_praktek', $jadwal->jam_dtg)
                                           ->first();
                        $sub = $cek ?: new JadwalDokter();
                        $sub->list_dokter_id = $row->id;
                        $sub->hari_praktek = array_search($jadwal->hari, $days);
                        $sub->jam_mulai_praktek = $jadwal->jam_dtg;
                        $sub->jam_selesai_praktek = $jadwal->jam_plg;
                        $sub->kode_departemen = $jadwal->kd_dep;
                        $sub->nama_departemen = $jadwal->ds_dep;
                        $sub->nama_departemen = $jadwal->ds_dep;
                        $sub->status_jam = $jadwal->sts_jam;
                        $sub->status_ue = $jadwal->sts_ue;
                        $sub->quota_janji = $jadwal->quota_janji;

                        $sub->save();
                    }
                }
            }

            return response([
                'success' => true,
            ]);
        }

        return response([
            'success' => false,
            'message' => 'Gagal mengakses web-service, silahkan periksa koneksi anda'
        ], 500);
    }
}
