<?php

namespace App\Http\Controllers\Profil;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Profil\ProfesiKeahlian;

use App\Http\Requests\Profil\ProfesiKeahlianRequest;

use Datatables;

class ProfesiKeahlianController extends Controller
{
    //
    protected $link = 'backend/profil/profesi-keahlian/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Profesi Keahlian");
		$this->setModalSize("");
		$this->setBreadcrumb(['Profil' => '#', 'Profesi Keahlian' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'judul',
			    'name' => 'judul',
			    'label' => 'Judul',
			    'searchable' => false,
			    'sortable' => true,
                'width' => '100px',
			],
            [
                'data' => 'konten',
                'name' => 'konten',
                'label' => 'Konten',
                'searchable' => false,
                'sortable' => true,
                'className' => "justify aligned",

            ],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
                
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
    {
        $records = ProfesiKeahlian::with('creator')
                           ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            // ->addColumn('gambar', function ($record) {
            //     $string ='';
            //     if($record->photo){
            //         $string = '<img src="'.asset('storage').'/'.$record->photo.'" alt="icon" width="80">';
            //     }
            //     return $string;
            // })
            // ->addColumn('posisi', function ($record) {
            //     $string ='Kiri';
            //     if($record->posisi==1){
            //         $string = 'Kanan';
            //     }
            //     return $string;
            // })
            ->addColumn('status', function ($record) {
                $string ='<span class="ui fluid orange label">Draft</span>';
                if($record->status==1){
                    $string = '<span class="ui fluid green label">Published</span>';
                }
                return $string;
            })
            ->addColumn('konten', function ($record) {
                $string ='<div class="ccount more" data-ccount="80" style="text-align: justify;">'.$record->konten.'</div>';
                return $string;
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['gambar','action','status','konten'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.profil.profesi-keahlian.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.profil.profesi-keahlian.create');
    }

    public function store(ProfesiKeahlianRequest $request)
    {
        // dd($request->all());
        $row = $request->all();

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/profil/profesi-keahlian', 'public');
            $row['photo'] = $path;
        }

        $data = new ProfesiKeahlian;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return ProfesiKeahlian::find($id);
    }

    public function edit($id)
    {
        $record = ProfesiKeahlian::find($id);

        return $this->render('backend.profil.profesi-keahlian.edit', ['record' => $record]);
    }

    public function update(ProfesiKeahlianRequest $request, $id)
    {
        $row = $request->all();
        
        $data = ProfesiKeahlian::find($id);

        $row['photo'] = $data->photo;

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/profil/profesi-keahlian', 'public');
            $row['photo'] = $path;
        }

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
        $jenis = ProfesiKeahlian::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
