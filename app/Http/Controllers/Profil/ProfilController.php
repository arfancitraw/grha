<?php

namespace App\Http\Controllers\Profil;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Profil\Profil;
use App\Models\Model;

use App\Http\Requests\Profil\ProfilRequest;

use Datatables;
use Storage;

class ProfilController extends Controller
{
    //
    protected $link = 'backend/profil/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("List Konten Profil");
        $this->setSubtitle("List Konten Profil");
		$this->setModalSize("small");
		$this->setBreadcrumb(['Profil' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "one wide column center aligned",
			],
			/* --------------------------- */
			[
			    'data' => 'judul',
			    'name' => 'judul',
			    'label' => 'Judul',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'left aligned',
                'width' => '15%'
			],
            [
                'data' => 'konten',
                'name' => 'konten',
                'label' => 'Konten',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
                'width' => '25%'
            ],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
                'width' => '20%'
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'two wide column center aligned',
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'two wide column center aligned',
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
    {
        $records = Profil::with('creator')->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            // $records->orderBy('judul', 'asc');
            $records->orderBy('posisi');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($keterangan = $request->keterangan) {
            $records->where('keterangan', 'ilike', '%' . $keterangan . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('konten', function ($record) {
                $string ='<span class="ccount more" data-ccount="118">'.strip_tags($record->konten).'</span>';
                return $string;
            })

            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                    'type' => 'edit-page',
                    'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['action','konten'])
            ->make(true);
    }

    public function index()
    {
        $this->pushBreadCrumb(['List Profil' => '#']);
        return $this->render('backend.profil.profil.index',[
            'data' => Profil::orderBy('posisi')->get(),
            'mockup' => false,
        ]);
    }

    public function create()
    {
		$this->setTitle("Tambah Konten Profil");
        $this->pushBreadCrumb(['Tambah Profil' => '#']);
        return $this->render('backend.profil.profil.create');
    }

    public function store(ProfilRequest $request)
    {
        // dd($request->all());
        $row = $request->all();
        $row['gambar'] = $request->photo;
        
        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/profil', 'public');
            $row['gambar'] = $path;
        }

        $data = new Profil;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function show($id)
    {
        return Profil::find($id);
    }

    public function edit($id)
    {
		$this->setTitle("Ubah Konten Profil");
        $this->pushBreadCrumb(['Ubah Profil' => '#']);
        // dd($id);
        $record = Profil::find($id);
        // dd($record);
        return $this->render('backend.profil.profil.edit', ['record' => $record]);
    }

    public function update(ProfilRequest $request, $id)
    {
        $row = $request->all();
        $data = Profil::find($id);
        $row['gambar'] = $data->gambar;
        $row['slug'] = $this->setSlugify($request->judul);
        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/profil', 'public');
            $row['gambar'] = $path;
        }
        // dd($row);
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function removeImage($id)
    {
        $data = Profil::find($id);
        Storage::delete($data->gambar);
        $data->gambar = null;
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function destroy($id)
    {
        $profil = Profil::find($id);
        $profil->fileDelete();
        $profil->delete();

        return response([
            'status' => true,
        ]);
    }

    public function sort(Request $request)
    {
        if($request->sorting){
            foreach ($request->sorting as $position => $id) {
                $profil = Profil::find($id);
                $profil->posisi = $position;
                $profil->save();
            }
                
            return response([
                'status' => true,
            ]);
        }

        return response([
            'status' => false,
        ], 422);
    }
}
