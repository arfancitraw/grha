<?php

namespace App\Http\Controllers\Pengaduan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Pengaduan\ItemPengaduan;

use App\Http\Requests\Pengaduan\ItemPengaduanRequest;

use Datatables;

class ItemPengaduanController extends Controller
{
    //
    protected $link = 'backend/pengaduan/item-pengaduan/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Item Pengaduan");
        $this->setSubtitle("Info Pasien");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Pengaduan Blowing' => '#', 'Item Pengaduan' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'judul',
			    'name' => 'judul',
			    'label' => 'Judul',
			    'searchable' => false,
			    'sortable' => true,
			],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'photo',
                'name' => 'photo',
                'label' => 'Photo',
                'searchable' => false,
                'sortable' => true,
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = ItemPengaduan::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('judul', 'asc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($konten = $request->konten) {
            $records->where('konten', 'ilike', '%' . $konten . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })

            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.pengaduan.item-pengaduan.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.pengaduan.item-pengaduan.create');
    }

    public function store(ItemPengaduanRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/pengaduan/item-pengaduan', 'public');
            $row['photo'] = $path;
        }

        $data = new ItemPengaduan;
        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }
    public function show($id)
    {
        return ItemPengaduan::find($id);
    }

    public function edit($id)
    {
    	$record = ItemPengaduan::find($id);

        return $this->render('backend.pengaduan.item-pengaduan.edit', ['record' => $record]);
    }

    public function update(ItemPengaduanRequest $request, $id)
    {
        $row = $request->all();
        
        $data = ItemPengaduan::find($id);

        $row['photo'] = $data->photo;

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/pengaduan/item-pengaduan', 'public');
            $row['photo'] = $path;
        }

        $data->fill($row);
        $data->save();
        
        return response([
            'status' => true,
            'data'  => $data
        ]);
    }

    public function destroy($id)
    {
    	$jenis = ItemPengaduan::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
