<?php

namespace App\Http\Controllers\Litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Litbang\ListPublikasi;

use App\Http\Requests\Litbang\ListPublikasiRequest;

use Datatables;

class ListPublikasiController extends Controller
{
//
    protected $link = 'backend/litbang/list-publikasi/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("List Publikasi");
        $this->setSubtitle("Litbang");
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Litbang' => '#', 'List Publikasi' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'tahun',
                'name' => 'tahun',
                'label' => 'Tahun',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'peneliti_utama',
                'name' => 'peneliti_utama',
                'label' => 'Peneliti Utama',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'peneliti_pendamping',
                'name' => 'peneliti_pendamping',
                'label' => 'Peneliti Pendamping',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Oleh',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '150px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = ListPublikasi::with('creator')
                                ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($tahun = $request->tahun) {
            $records->where('tahun', $tahun);
        }

        if ($peneliti = $request->peneliti) {
            $records->where('peneliti_utama', 'ilike', '%' . $peneliti . '%')
                    ->orWhere('peneliti_pendamping', 'ilike', '%' . $peneliti . '%');
        }

        return Datatables::of($records)
                ->addColumn('num', function ($record) use ($request) {
                    return $request->get('start');
                })
                ->addColumn('created_at', function ($record) {
                    return $record->created_at->format('d F Y');
                })
                ->addColumn('created_by', function ($record) {
                    return $record->creator->name;
                })
                ->addColumn('action', function ($record) {
                    $btn = '';
                    //Edit
                    $btn .= $this->makeButton([
                        'type' => 'edit-page',
                        'id'   => $record->id
                    ]);
                    // Delete
                    $btn .= $this->makeButton([
                        'type' => 'delete',
                        'id'   => $record->id
                    ]);

                    return $btn;
                })
                ->rawColumns(['action','deskripsi'])
                ->make(true);
    }

    public function index()
    {
        return $this->render('backend.litbang.list-publikasi.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.litbang.list-publikasi.create');
    }

    public function store(ListPublikasiRequest $request)
    {
        $row = $request->all();

        if($request->hasFile('file')){
            $path = $request->file('file')->store('uploads/libtan/publikasi', 'public');
            $row['file'] = $path;
        }

        $jenis = new ListPublikasi;
        $jenis->fill($row);
        $jenis->save();

        return response([
            'status' => true,
            'data'	=> $jenis
        ]);
    }

    public function show($id)
    {
        return ListPublikasi::find($id);
    }

    public function edit($id)
    {
        $record = ListPublikasi::find($id);

        return $this->render('backend.litbang.list-publikasi.edit', ['record' => $record]);
    }

    public function update(ListPublikasiRequest $request, $id)
    {
        $row = $request->all();

        $jenis = ListPublikasi::find($id);
        if($request->hasFile('file')){
            $path = $request->file('file')->store('uploads/litbang/publikasi', 'public');
            $row['file'] = $path;
        }
        $jenis->fill($row);
        $jenis->save();

        return response([
            'status' => true,
            'data'	=> $jenis
        ]);
    }

    public function destroy($id)
    {
        $jenis = ListPublikasi::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
}
