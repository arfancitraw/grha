<?php

namespace App\Http\Controllers\Litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Litbang\InfoLitbang;

use App\Http\Requests\Litbang\InfoLitbangRequest;

use Datatables;

class InfoLitbangController extends Controller
{
    //
    protected $link = 'backend/litbang/info-litbang/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Info Litbang");
        $this->setSubtitle("Litbang");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Master' => '#', 'Info Litbang' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'judul',
			    'name' => 'judul',
			    'label' => 'Judul',
			    'searchable' => false,
			    'sortable' => true,
			],
            [
                'data' => 'konten',
                'name' => 'konten',
                'label' => 'Konten',
                'searchable' => false,
                'sortable' => true,
                'className' => "left aligned",
                'width' => '20%'
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "center aligned",
			    'width' => '150px',
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = InfoLitbang::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($judul = $request->judul) {
            $records->where('judul', 'ilike', '%' . $judul . '%');
        }
        if ($konten = $request->konten) {
            $records->where('konten', 'ilike', '%' . $konten . '%');
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('konten', function ($record) {
                $string ='<span class="ccount more" data-ccount="80">'.strip_tags($record->konten).'</span>';
                return $string;
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit-page',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->rawColumns(['action','konten'])
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.litbang.info-litbang.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.litbang.info-litbang.create');
    }

    public function store(InfoLitbangRequest $request)
    {
    	$jenis = new InfoLitbang;
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function show($id)
    {
        return InfoLitbang::find($id);
    }

    public function edit($id)
    {
    	$record = InfoLitbang::find($id);

        return $this->render('backend.litbang.info-litbang.edit', ['record' => $record]);
    }

    public function update(InfoLitbangRequest $request, $id)
    {
    	$jenis = InfoLitbang::find($id);
    	$jenis->fill($request->all());
    	$jenis->save();

    	return response([
    		'status' => true,
    		'data'	=> $jenis
    	]);
    }

    public function destroy($id)
    {
    	$jenis = InfoLitbang::find($id);
    	$jenis->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
