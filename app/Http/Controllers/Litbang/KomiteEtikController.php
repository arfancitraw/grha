<?php

namespace App\Http\Controllers\Litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Litbang\KomiteEtik;

use App\Http\Requests\Litbang\KomiteEtikRequest;

use Datatables;

class KomiteEtikController extends Controller
{
    //
    protected $link = 'backend/litbang/komite-etik/';
    protected $perms  = '';

	function __construct()
	{
		$this->setLink($this->link);
        $this->setPerms($this->perms);

		$this->setTitle("Komite Etik");
        $this->setSubtitle("Litbang");
		$this->setModalSize("tiny");
		$this->setBreadcrumb(['Litbang' => '#', 'Komite Etik' => '#']);
		$this->setTableStruct([
			[
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
			    'searchable' => false,
			    'className' => "center aligned",
			    'width' => '40px',
			],
			/* --------------------------- */
			[
			    'data' => 'nama',
			    'name' => 'nama',
			    'label' => 'Nama',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
            [
                'data' => 'jabatan',
                'name' => 'jabatan',
                'label' => 'Jabatan',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'departemen',
                'name' => 'departemen',
                'label' => 'Departemen',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'tanggal_efektif_awal',
                'name' => 'tanggal_efektif_awal',
                'label' => 'Tanggal efektif awal',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
            [
                'data' => 'tanggal_efektif_akhir',
                'name' => 'tanggal_efektif_akhir',
                'label' => 'Tanggal efektif akhir',
                'searchable' => false,
                'sortable' => true,
                'className' => 'center aligned'
            ],
			[
			    'data' => 'created_at',
			    'name' => 'created_at',
			    'label' => 'Tanggal Entry',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'created_by',
			    'name' => 'created_by',
			    'label' => 'Oleh',
			    'searchable' => false,
			    'sortable' => true,
                'className' => 'center aligned'
			],
			[
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Aksi',
			    'searchable' => false,
			    'sortable' => false,
			    'className' => "two wide column center aligned",
			]
		]);
	}

	public function grid(Request $request)
	{
		$records = KomiteEtik::with('creator')
						   ->select('*');
		//Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('created_at', 'desc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        if ($jabatan = $request->jabatan) {
            $records->where('jabatan', $jabatan);
        }

        return Datatables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('tanggal_efektif_awal', function ($record) {
                return $record->tanggal_efektif_awal->format('d F Y');
            })
            ->addColumn('tanggal_efektif_akhir', function ($record) {
                return $record->tanggal_efektif_akhir->format('d F Y');
            })
            ->addColumn('created_at', function ($record) {
                return $record->created_at->format('d F Y');
            })
            ->addColumn('created_by', function ($record) {
                return $record->creator->name;
            })            
            ->addColumn('jabatan', function ($record) {
                // dd($record->spesialisasi);
                return $record->jabatandokter['jabatan'];
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                $btn .= $this->makeButton([
                	'type' => 'edit-page',
                	'id'   => $record->id
                ]);
                // Delete
                $btn .= $this->makeButton([
                	'type' => 'delete',
                	'id'   => $record->id
                ]);

                return $btn;
            })
            ->make(true);
	}

    public function index()
    {
        return $this->render('backend.litbang.komite-etik.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.litbang.komite-etik.create');
    }

    public function store(KomiteEtikRequest $request)
    {
    	$data = new KomiteEtik;
    	$data->fill($request->all());
    	$data->save();

    	return response([
    		'status' => true,
    		'data'	=> $data
    	]);
    }

    public function show($id)
    {
        return KomiteEtik::find($id);
    }

    public function edit($id)
    {
    	$record = KomiteEtik::find($id);

        return $this->render('backend.litbang.komite-etik.edit', ['record' => $record]);
    }

    public function update(KomiteEtikRequest $request, $id)
    {
    	$data = KomiteEtik::find($id);
    	$data->fill($request->all());
    	$data->save();

    	return response([
    		'status' => true,
    		'data'	=> $data
    	]);
    }

    public function destroy($id)
    {
    	$data = KomiteEtik::find($id);
    	$data->delete();

    	return response([
    		'status' => true,
    	]);
    }
}
