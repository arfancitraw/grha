<?php

namespace App\Http\Controllers\Testimoni;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Testimoni\Testimoni;

use App\Http\Requests\Testimoni\TestimoniRequest;

use Datatables;
use DB;

class TestimoniController extends Controller
{
    protected $link = 'backend/testimoni/';
    protected $perms  = '';

    function __construct()
    {
        $this->setLink($this->link);
        $this->setPerms($this->perms);

        $this->setTitle("Periode Testimoni");
        $this->setSubtitle("Testimoni");
        $this->setModalSize("tiny");
        $this->setBreadcrumb(['Testimoni' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "center aligned",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'pekerjaan',
                'name' => 'pekerjaan',
                'label' => 'Pekerjaan',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'testimoni',
                'name' => 'testimoni',
                'label' => 'Testimoni',
                'searchable' => false,
                'sortable' => true,
            ],
            [
                'data' => 'publik',
                'name' => 'publik',
                'label' => 'Tampil di Publik',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Tanggal Entry',
                'searchable' => false,
                'sortable' => true,
                'className' => "center aligned",
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'className' => "center aligned",
                'width' => '70px',
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Testimoni::with('creator')
        ->select('*');
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
            // $records->->sort();
            $records->orderBy('nama', 'asc');
        }

        //Filters
        if ($nama = $request->nama) {
            $records->where('nama', 'ilike', '%' . $nama . '%');
        }
        return Datatables::of($records)
        ->addColumn('num', function ($record) use ($request) {
            return $request->get('start');
        })
        ->editColumn('publik', function ($record) {
            return $record->publik
           ? '<div class="ui green horizontal label">Tampil</div>'
           : '<div class="ui red horizontal label">Tersembunyi</div>';
       })
        ->addColumn('created_at', function ($record) {
            return $record->created_at->format('d F Y');
        })
        ->addColumn('action', function ($record) {
            $btn = '';
                //publik
            if($record->publik){
                $btn .= $this->makeButton([
                    'type' => 'modal',
                    'class' => 'orange icon activate',
                    'label' => '<i class="low vision icon"></i>',
                    'tooltip' => 'Sembunyikan',
                    'disabled' => $record->status ? 'disabled' : '',
                    'id'   => $record->id
                ]);

            }else{
                $btn .= $this->makeButton([
                    'type' => 'modal',
                    'class' => 'green icon activate',
                    'label' => '<i class="eye icon"></i>',
                    'tooltip' => 'Tampilkan',
                    'disabled' => $record->status ? 'disabled' : '',
                    'id'   => $record->id
                ]);         
            }

                // Delete
            $btn .= $this->makeButton([
                'type' => 'delete',
                'id'   => $record->id
            ]);

            return $btn;
        })
        ->rawColumns(['action','publik'])
        ->make(true);
    }

    public function index()
    {
        return $this->render('backend.testimoni.testimoni.index', ['mockup' => false]);
    }

    public function create()
    {
        return $this->render('backend.testimoni.testimoni.create');
    }

    public function store(TestimoniRequest $request)
    {
        $jenis = new Testimoni;
        $jenis->fill($request->all());
        $jenis->save();

        return response([
            'status' => true,
            'data'  => $jenis
        ]);
    }

    public function show($id)
    {
        return Testimoni::find($id);
    }

    public function edit($id)
    {
        $record = Testimoni::find($id);

        return $this->render('backend.testimoni.testimoni.edit', ['record' => $record]);
    }

    public function update(TestimoniRequest $request, $id)
    {
        $jenis = Testimoni::find($id);
        $jenis->fill($request->all());
        $jenis->save();

        return response([
            'status' => true,
            'data'  => $jenis
        ]);
    }

    public function destroy($id)
    {
        $jenis = Testimoni::find($id);
        $jenis->delete();

        return response([
            'status' => true,
        ]);
    }
    public function activate($id)
    {
        $data = Testimoni::find($id);
        $data->publik = !$data->publik;
        $data->save();

        return response([
            'publik' => true,
        ]);
    }
}
