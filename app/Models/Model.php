<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Base;
use App\Models\Traits\EntryBy;
use App\Models\Traits\RaidModel;
use App\Models\Traits\Utilities;

class Model extends Base
{
    use RaidModel, Utilities, EntryBy;

    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray($append=true)
    {
    	if(!$append){
	        return $this->attributes;
    	}
        return array_merge($this->attributesToArray(), $this->relationsToArray());
    }

    public function slugify($text)
    {
      // replace non letter or digits by -
      $text = preg_replace('~[^\pL\d]+~u', '-', $text);

      // transliterate
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

      // remove unwanted characters
      $text = preg_replace('~[^-\w]+~', '', $text);

      // trim
      $text = trim($text, '-');

      // remove duplicate -
      $text = preg_replace('~-+~', '-', $text);

      // lowercase
      $text = strtolower($text);

      if (empty($text)) {
        return 'n-a';
      }

      return $text;
    }

    public function fileDelete()
    {   
        // dd($this->gambar == false);
        if($this->gambar == true)
        {
                if(file_exists(public_path('storage/'.$this->gambar)))
                {
                    unlink(public_path('storage/'.$this->gambar));
                }
                $this->delete();
        }
    }
}
