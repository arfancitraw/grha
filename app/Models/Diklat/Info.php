<?php

namespace App\Models\Diklat;

use App\Models\Model;

class Info extends Model
{
    protected $table 		= 'trans_karir';
    protected $log_table 	= 'log_trans_karir';
    protected $log_table_fk	= 'id_karir';

    protected $fillable 	= ['judul','konten','keterangan','lampiran'];
}
