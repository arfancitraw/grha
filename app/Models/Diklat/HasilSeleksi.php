<?php

namespace App\Models\Diklat;

use App\Models\Model;

class HasilSeleksi extends Model
{
    protected $table 		= 'trans_hasil_seleksi';
    protected $log_table 	= 'log_trans_hasil_seleksi';
    protected $log_table_fk	= 'id_hasil_seleksi';

    protected $fillable 	= ['karir_id','keterangan','lampiran'];

    public function karir()
    {
        return $this->belongsTo(Info::class,'karir_id');
    }
}
