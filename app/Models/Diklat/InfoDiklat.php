<?php

namespace App\Models\Diklat;

use App\Models\Model;

class InfoDiklat extends Model
{
    protected $table 		= 'trans_info_diklat';
    protected $log_table 	= 'log_trans_info_diklat';
    protected $log_table_fk	= 'id_info_diklat';

    protected $fillable 	= ['judul', 'konten','judul_en','konten_en'];
}
