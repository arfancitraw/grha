<?php

namespace App\Models\Diklat;

use App\Models\Model;
use App\Models\User;

class JadwalDiklat extends Model
{
    protected $table 		= 'trans_jadwal_diklat';
    protected $log_table 	= 'log_trans_jadwal_diklat';
    protected $log_table_fk	= 'id_trans_jadwal_diklat';
    protected $fillable 	= ['judul', 'konten', 'keterangan', 'tanggal', 'jam','gambar','judul_en','konten_en','keterangan_en'];
    protected $dates		= ['tanggal'];
}
