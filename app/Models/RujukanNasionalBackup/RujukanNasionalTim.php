<?php

namespace App\Models\RujukanNasionalBackup;

use App\Models\Model;

class RujukanNasionalTim extends Model
{
    protected $table 		= 'trans_rujukan_tim';
    protected $log_table 	= 'log_trans_rujukan_tim';
    protected $log_table_fk	= 'id_trans_tim';
    protected $fillable 	= ['id_rujukan_nasional', 'id_dokter', 'posisi'];
}
