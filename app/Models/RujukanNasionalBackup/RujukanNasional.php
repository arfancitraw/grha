<?php

namespace App\Models\RujukanNasionalBackup;

use App\Models\Model;
use App\Models\Dokter\ListDokter as Dokter;
use App\Models\RujukanNasional\RujukanNasionalAgenda as Agenda;
use App\Models\RujukanNasional\RujukanNasionalTim as TIM;

class RujukanNasional extends Model
{
    protected $table 		= 'trans_rujukan_nasional';
    protected $log_table 	= 'log_trans_rujukan_nasional';
    protected $log_table_fk	= 'id_trans_rujukan_nasional';
    protected $fillable 	= [
        'judul', 'photo', 'status','home','icon','deskripsi',
        'pengantar', 'profile',  'produk',  'tim',  'fasilitas',  'penelitian',  'jejaring',  'agenda',  'aksesibilitas',  'cara_merujuk'
    ];

    public function produk(){
        return $this->hasMany(RujukanNasionalProduk::class, 'id_rujukan_nasional');
    }
    public function dokter(){
        return $this->belongsToMany(Dokter::class, 'trans_rujukan_tim', 'id_rujukan_nasional', 'id_dokter');
    }
    public function agenda(){
        return $this->hasMany(Agenda::class, 'id_rujukan_nasional');
    }

    public function tim(){
        return $this->hasMany(TIM::class, 'id_rujukan_nasional');
    }

    public function getTimDokter()
    {

        if($this->dokter->count() > 0)
        {
            foreach($this->dokter as $att)
            {
                $timdokter[] = ''.$att->id_dokter.'';
            }
        }else{
            $timdokter[] ='';
        }
        return json_encode($timdokter);
    }
}
