<?php

namespace App\Models\RujukanNasionalBackup;

use App\Models\Model;
use App\Models\Dokter\ListDokter as Dokter;
use App\Models\RujukanNasional\RujukanNasionalAgenda as Agenda;
use App\Models\RujukanNasional\RujukanNasionalTim as TIM;

class RujukanNasionalBackup extends Model
{
    protected $table 		= 'trans_rujukan_nasional';
    protected $log_table 	= 'log_trans_rujukan_nasional';
    protected $log_table_fk	= 'id_trans_rujukan_nasional';
    protected $fillable 	= [
        'judul', 'photo', 'status','home','icon','deskripsi',
        'pengantar', 'profile',  'produk',  'tim',  'fasilitas',  'penelitian',  'jejaring',  'agenda',  'aksesibilitas',  'cara_merujuk'
    ];

    public function RujukanProduk($id='',$data=''){
        if (($id != '')&&($data['produk'] != '')){
            foreach ($data['produk']['nama'] as $Nilaivalue => $value) {
                $dataProduk['id_rujukan_nasional'] = $id;
                $dataProduk['nama'] = $value;
                $dataProduk['keterangan'] = $data['produk']['keterangan'][$Nilaivalue];
                $file = $data['produk']['gambar'][$Nilaivalue];
                if($file){
                    $path = $file->store('uploads/rujukan-nasional-produk', 'public');
                    $dataProduk['gambar'] = $path;
                }
                $save = new RujukanNasionalProduk;
                $save->fill($dataProduk);
                $save->save();
// $data['gambar'] = $data['produk']['gambar'][$Nilaivalue];
            }
        }
    }

    public function RujukanTim($id='',$data=''){
// dd($data);
        if (($id != '')&&($data['tim'] != '')){
            foreach ($data['tim']['id_dokter'] as $Nilaivalue => $value) {
                $dataTim['id_rujukan_nasional'] = $id;
                $dataTim['id_dokter'] = $value;
                $dataTim['posisi'] = $data['tim']['posisi'][$Nilaivalue];

                $save = new RujukanNasionalTim;
                $save->fill($dataTim);
                $save->save();
// $data['gambar'] = $data['produk']['gambar'][$Nilaivalue];
            }
        }
    }

    public function RujukanAgenda($id='',$data=''){
        if (($id != '')&&($data['agenda'] != '')){
            foreach ($data['agenda']['nama'] as $Nilaivalue => $value) {
                $dataAgenda['id_rujukan_nasional'] = $id;
                $dataAgenda['nama'] = $value;
                $dataAgenda['tahun'] = $data['agenda']['tahun'][$Nilaivalue];
                $dataAgenda['keterangan'] = $data['agenda']['keterangan'][$Nilaivalue];

                $save = new RujukanNasionalAgenda;
                $save->fill($dataAgenda);
                $save->save();
            }
        }
    }

// FIND EDIT

    public function RujukanProdukEdit($id='',$data=''){

        RujukanNasionalProduk::where('id_rujukan_nasional',$id)->delete();
        if (($id != '')&&(isset($data['produk']))){
            $arras = array();
            $dataProduk = array();
// foreach ($data['produk']['nama'] as $Nilaivalue => $value) {
// foreach ($data['produk']['gambar_asli'] as $NilaiGmbr => $valueGmbr){
// if ($Nilaivalue == $NilaiGmbr){
//   $arras[$NilaiGmbr]['nama'] = $value;
//   $arras[$NilaiGmbr]['gambar'] = $valueGmbr;
//   $arras[$NilaiGmbr]['keterangan'] = $data['produk']['keterangan'][$NilaiGmbr];
// }
//   } 
// }
            $nilai = count($arras);
            $hasilNama = array_slice($data['produk']['nama'], $nilai);
            $hasilKet = array_slice($data['produk']['keterangan'], $nilai);
            foreach ($hasilNama as $Nilais => $valNam) {
                $dataProduk[$Nilais]['nama'] = $valNam;
                $dataProduk[$Nilais]['keterangan'] = $hasilKet[$Nilais];
                $file = $data['produk']['gambar'][$Nilais];
                if($file){
                    $path = $file->store('uploads/rujukan-nasional-produk', 'public');
                    $dataProduk[$Nilais]['gambar'] = $path;
                }
            }
            $hasils = array_merge($arras,$dataProduk);

            foreach ($hasils as $Savevalue) {
// dd($Savevalue);
                $hasilss['id_rujukan_nasional'] = $id;
                $hasilss['nama'] = $Savevalue['nama'];
                $hasilss['gambar'] = $Savevalue['gambar'];
                $hasilss['keterangan'] = $Savevalue['keterangan'];
                $new = new RujukanNasionalProduk;
                $new->fill($hasilss);
                $new->save();
// break;
            }
        }
    }

    public function RujukanTimEdit($id='',$data=''){
        RujukanNasionalTim::where('id_rujukan_nasional',$id)->delete();
        if (($id != '')&&(isset($data['tim']))){
            foreach ($data['tim']['id_dokter'] as $Nilaivalue => $value) {
                $dataTim['id_rujukan_nasional'] = $id;
                $dataTim['id_dokter'] = $value;
                $dataTim['posisi'] = $data['tim']['posisi'][$Nilaivalue];

                $save = new RujukanNasionalTim;
                $save->fill($dataTim);
                $save->save();    
            }
        }
    }

    public function RujukanAgendaEdit($id='',$data=''){
        RujukanNasionalAgenda::where('id_rujukan_nasional',$id)->delete();
        if (($id != '')&&(isset($data['agenda']))){
            foreach ($data['agenda']['nama'] as $Nilaivalue => $value) {
                $dataAgenda['id_rujukan_nasional'] = $id;
                $dataAgenda['nama'] = $value;
                $dataAgenda['tahun'] = $data['agenda']['tahun'][$Nilaivalue];
                $dataAgenda['keterangan'] = $data['agenda']['keterangan'][$Nilaivalue];

                $save = new RujukanNasionalAgenda;

// foreach ($save as $Savevalue) {
                $save->fill($dataAgenda);
                $save->save();
                break;
// }

            }
        }
    }

    public function produk(){
        return $this->hasMany(RujukanNasionalProduk::class, 'id_rujukan_nasional');
    }
    public function dokter(){
        return $this->belongsToMany(Dokter::class, 'trans_rujukan_tim', 'id_rujukan_nasional', 'id_dokter');
    }
    public function agenda(){
        return $this->hasMany(Agenda::class, 'id_rujukan_nasional');
    }

    public function tim(){
        return $this->hasMany(TIM::class, 'id_rujukan_nasional');
    }

    public function getTimDokter()
    {

        if($this->dokter->count() > 0)
        {
            foreach($this->dokter as $att)
            {
                $timdokter[] = ''.$att->id_dokter.'';
            }
        }else{
            $timdokter[] ='';
        }
        return json_encode($timdokter);
    }
}
