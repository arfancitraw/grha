<?php

namespace App\Models\RujukanNasionalBackup;

use App\Models\Model;

class RujukanNasionalProduk extends Model
{
    protected $table 		= 'trans_rujukan_produk';
    protected $log_table 	= 'log_trans_rujukan_produk';
    protected $log_table_fk	= 'id_trans_produk';
    protected $fillable 	= ['id_rujukan_nasional', 'nama', 'keterangan', 'gambar'];
}
