<?php

namespace App\Models\RujukanNasionalBackup;

use App\Models\Model;

class RujukanNasionalAgenda extends Model
{
    protected $table 		= 'trans_rujukan_agenda';
    protected $log_table 	= 'log_trans_rujukan_agenda';
    protected $log_table_fk	= 'id_trans_agenda';
    protected $fillable 	= ['id_rujukan_nasional', 'nama', 'keterangan', 'tahun'];
}
