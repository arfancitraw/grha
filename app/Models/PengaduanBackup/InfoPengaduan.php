<?php

namespace App\Models\Pengaduan;

use App\Models\Model;

class InfoPengaduan extends Model
{
    protected $table 		= 'trans_info_pengaduan';
    protected $log_table 	= 'log_trans_info_pengaduan';
    protected $log_table_fk	= 'id_info_pengaduan';

    protected $fillable 	= ['judul','konten'];


}
