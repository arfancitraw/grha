<?php

namespace App\Models\Pengaduan;

use App\Models\Model;

class PengaduanSaksi extends Model
{
    protected $table 		= 'trans_pengaduan_saksi';
    protected $log_table 	= 'log_trans_pengaduan_saksi';
    protected $log_table_fk	= 'id_pengaduan_saksi';

    protected $fillable 	= ['id_pengaduan','nama','alamat','telp'];
    
}
