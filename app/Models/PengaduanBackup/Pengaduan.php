<?php

namespace App\Models\PengaduanBackup;

use App\Models\Model;

class Pengaduan extends Model
{
    protected $table 		= 'trans_pengaduan';
    protected $log_table 	= 'log_trans_pengaduan';
    protected $log_table_fk	= 'id_pengaduan';

    protected $fillable 	= ['nama','alamat','telp_rumah','telp_kantor','telp_seluler','email','hari','pukul_awal','pukul_akhir','no_id','pangkat','jabatan','jabatan','tgl_kerja','divisi','no_sk','rahasia','alasan_rahasia','kronologi','pihak_terkait','lokasi_kejadian','tanggal_kejadian','penyebab','dampak','alasan'];

    public function saksi()
    {
        return $this->hasMany(PengaduanSaksi::class, 'id_pengaduan');
    }
        public function bukti()
    {
        return $this->hasMany(PengaduanBukti::class, 'id_pengaduan');
    }
}
