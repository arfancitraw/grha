<?php

namespace App\Models\Pengaduan;

use App\Models\Model;

class ItemPengaduan extends Model
{
    protected $table 		= 'trans_item_pengaduan';
    protected $log_table 	= 'log_trans_item_pengaduan';
    protected $log_table_fk	= 'id_item_pengaduan';

    protected $fillable 	= ['judul','keterangan','photo'];


}
