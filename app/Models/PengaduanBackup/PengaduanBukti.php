<?php

namespace App\Models\Pengaduan;

use App\Models\Model;

class PengaduanBukti extends Model
{
    protected $table 		= 'trans_pengaduan_bukti';
    protected $log_table 	= 'log_trans_pengaduan_bukti';
    protected $log_table_fk	= 'id_pengaduan_bukti';

    protected $fillable 	= ['id_pengaduan','nama','jumlah','file'];

    
}
