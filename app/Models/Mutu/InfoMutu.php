<?php

namespace App\Models\Mutu;

use App\Models\Model;
use App\Models\Master\JenisMutu;

class InfoMutu extends Model
{
    protected $table 		= 'trans_info_mutu';
    protected $log_table 	= 'log_trans_info_mutu';
    protected $log_table_fk	= 'id_info_mutu';

    protected $fillable 	= ['nama','jenis_mutu','deskripsi','gambar','periode','nama_en','deskripsi_en'];

    public function JenisMutu()
    {
        return $this->belongsTo(JenisMutu::class,'jenis_mutu');
    }

}
