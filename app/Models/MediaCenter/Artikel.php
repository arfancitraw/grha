<?php

namespace App\Models\MediaCenter;

use App\Models\Model;
use App\Models\Master\KategoriArtikel;

class Artikel extends Model
{
    protected $table 		= 'trans_list_artikel';
    protected $log_table 	= 'log_trans_list_artikel';
    protected $log_table_fk	= 'id_list_artikel';

    protected $fillable 	= ['judul','kategori_id','slug','konten','deskripsi','photo', 'tanggal','judul_en','konten_en','deskripsi_en'];
    protected $dates		= ['tanggal'];
    protected $appends		= ['tautan'];

 	public function KategoriArtikel()
 	{
 		return $this->belongsTo(KategoriArtikel::class,'kategori_id');
 	}

	public function getTautanAttribute()
    {
    	return url('artikel/'.$this->slug);
    }
}
