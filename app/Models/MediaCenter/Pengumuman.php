<?php

namespace App\Models\MediaCenter;

use App\Models\Model;
use App\Models\Master\KategoriPengumuman as Kategori;

class Pengumuman extends Model
{
    protected $table 		= 'trans_pengumuman';
    protected $log_table 	= 'log_trans_pengumuman';
    protected $log_table_fk	= 'id_pengumuman';

    protected $fillable 	= ['judul', 'slug', 'konten', 'keterangan', 'lampiran', 'tanggal', 'kategori_id', 'photo','judul_en','konten_en','keterangan_en'];
    protected $dates		= ['tanggal'];
    protected $appends		= ['tautan'];

    public function kategori()
    {
    	return $this->belongsTo(Kategori::class, 'kategori_id');
    }

	public function getTautanAttribute()
    {
    	return url('berita/'.$this->slug);
    }
}
