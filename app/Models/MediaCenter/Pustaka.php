<?php

namespace App\Models\MediaCenter;

use App\Models\Model;

class Pustaka extends Model
{
    protected $table 		= 'trans_pustaka';
    protected $log_table 	= 'log_trans_pustaka';
    protected $log_table_fk	= 'id_pustaka';

    protected $fillable 	= ['judul','konten','keterangan','lampiran', 'pengertian', 'gejala', 'penyebab', 'penanganan', 'pencegahan'];
}
