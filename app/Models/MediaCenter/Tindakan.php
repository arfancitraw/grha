<?php

namespace App\Models\MediaCenter;

use App\Models\Model;

class Tindakan extends Model
{
    protected $table 		= 'trans_pustaka_tindakan';
    protected $log_table 	= 'log_trans_pustaka_tindakan';
    protected $log_table_fk	= 'id_tindakan';

    protected $fillable 	= ['nama', 'indikasi', 'tujuan', 'tatacara', 'risiko', 'alternatif', 'prognosis_dilakukan', 'prognosis_tidak', 'perluasan', 'persiapan', 'penjelasan', 'lampiran','nama_en','indikasi_en','tujuan_en', 'tatacara_en', 'risiko_en', 'alternatif_en', 'prognosis_dilakukan_en', 'prognosis_tidak_en', 'perluasan_en', 'persiapan_en', 'penjelasan_en'];
}
