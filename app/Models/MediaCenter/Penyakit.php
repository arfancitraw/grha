<?php

namespace App\Models\MediaCenter;

use App\Models\Model;

class Penyakit extends Model
{
    protected $table 		= 'trans_pustaka_penyakit';
    protected $log_table 	= 'log_trans_pustaka_penyakit';
    protected $log_table_fk	= 'id_penyakit';

    protected $fillable 	= ['nama', 'pengertian', 'gejala', 'penyebab', 'penanganan', 'pencegahan', 'lampiran'];
}
