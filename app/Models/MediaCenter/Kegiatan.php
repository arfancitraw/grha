<?php

namespace App\Models\MediaCenter;

use App\Models\Model;

class Kegiatan extends Model
{
    protected $table 		= 'trans_kegiatan';
    protected $log_table 	= 'log_trans_kegiatan';
    protected $log_table_fk	= 'id_kegiatan';

    protected $fillable 	= ['judul', 'slug', 'konten', 'keterangan', 'tanggal', 'photo','judul_en','konten_en','keterangan_en'];
    protected $dates		= ['tanggal'];
    protected $appends		= ['tautan'];

	public function getTautanAttribute()
    {
    	return url('kegiatan/'.$this->slug);
    }
}
