<?php

namespace App\Models\Pengaduan;

use App\Models\Model;

class PengaduanGratifikasi extends Model
{
    protected $table 		= 'trans_pengaduan_gratifikasi';
    protected $log_table 	= 'log_trans_pengaduan_gratifikasi';
    protected $log_table_fk	= 'id_pengaduan_gratifikasi';

    protected $fillable 	= ['nip_nopeg', 'nama', 'jabatan_unit_kerja', 'alamat_telepon_email', 'jenis_bentuk_penerimaan', 'harga_nilai_nominal', 'kaitan_peristiwa_penerimaan', 'tempat_penerimaan', 'tanggal_penerimaan','nama_pemberi','pekerjaan_jabatan','hubungan_penerima','alasan_pemberian','kronologi','catatan'];

    protected $dates		= ['tanggal_penerimaan'];

    public function bukti()
	{
	    return $this->hasMany(PengaduanGratifikasiBukti::class, 'id_pengaduan_gratifikasi','id');
	}
}
