<?php

namespace App\Models\Pengaduan;

use App\Models\Model;

class PengaduanSponsorBukti extends Model
{
    protected $table 		= 'trans_pengaduan_sponsor_bukti';
    protected $log_table 	= 'log_trans_pengaduan_sponsor_bukti';
    protected $log_table_fk	= 'id_pengaduan_sponsor_bukti';

    protected $fillable 	= ['id_pengaduan_sponsor', 'file'];

    public function sponsor()
	{
	    return $this->belongsTo(PengaduanSponsor::class, 'id_pengaduan_sponsor','id');
	}
}
