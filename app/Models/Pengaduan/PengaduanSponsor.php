<?php

namespace App\Models\Pengaduan;

use App\Models\Model;

class PengaduanSponsor extends Model
{
    protected $table 		= 'trans_pengaduan_sponsor';
    protected $log_table 	= 'log_trans_pengaduan_sponsor';
    protected $log_table_fk	= 'id_pengaduan_sponsor';

    protected $fillable 	= ['nip_nopeg', 'nama', 'jabatan', 'alamat', 'nama_pemberi', 'nama_kegiatan', 'nama_perusahaan', 'tanggal', 'tempat_kegiatan','peranan','pemberi_sponsor','registrasi','akomodasi','transportasi','honor','total'];
    protected $dates		= ['tanggal'];

    public function bukti()
	{
	    return $this->hasMany(PengaduanSponsorBukti::class, 'id_pengaduan_sponsor','id');
	}
}
