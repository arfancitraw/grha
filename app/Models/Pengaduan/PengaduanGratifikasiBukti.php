<?php

namespace App\Models\Pengaduan;

use App\Models\Model;

class PengaduanGratifikasiBukti extends Model
{
    protected $table 		= 'trans_pengaduan_gratifikasi_bukti';
    protected $log_table 	= 'log_trans_pengaduan_gratifikasi_bukti';
    protected $log_table_fk	= 'id_pengaduan_gratifikasi_bukti';

    protected $fillable 	= ['id_pengaduan_gratifikasi', 'file'];

    public function gratifikasi()
	{
	    return $this->belongsTo(PengaduanGratifikasi::class, 'id_pengaduan_gratifikasi','id');
	}
}
