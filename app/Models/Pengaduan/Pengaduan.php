<?php

namespace App\Models\Pengaduan;

use App\Models\Model;

class Pengaduan extends Model
{
    protected $table 		= 'trans_pengaduan';
    protected $log_table 	= 'log_trans_pengaduan';
    protected $log_table_fk	= 'id_pengaduan';

    protected $fillable 	= ['tipe', 'nama', 'status', 'telepon', 'email', 'perihal', 'uraian', 'bukti', 'keterangan'];
    protected $tipes        = ['Etik & Hukum', 'Whistleblowing', 'Pengaduan Masyarakat'];
    protected $appends      = ['tipe_label'];

    public function getTipeLabelAttribute()
    {
        return array_key_exists('tipe', $this->attributes) ? $this->tipes[$this->attributes['tipe']] : '';
    }
}
