<?php

namespace App\Models\Profil;

use App\Models\Model;
use App\Models\User;

class Profil extends Model
{
    protected $table 		= 'trans_profile';
    protected $log_table 	= 'log_trans_profile';
    protected $log_table_fk	= 'id_trans_profile';
    protected $fillable 	= ['judul', 'konten', 'keterangan', 'gambar', 'slug', 'posisi','judul_en','konten_en','keterangan_en'];
}
