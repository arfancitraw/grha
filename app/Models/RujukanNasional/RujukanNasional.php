<?php

namespace App\Models\RujukanNasional;

use App\Models\Model;
use App\Models\Dokter\ListDokter as Dokter;
use App\Models\RujukanNasional\RujukanNasionalAgenda as Agenda;
use App\Models\RujukanNasional\RujukanNasionalTim as TIM;

class RujukanNasional extends Model
{
    protected $table 		= 'trans_rujukan_nasional';
    protected $log_table 	= 'log_trans_rujukan_nasional';
    protected $log_table_fk	= 'id_trans_rujukan_nasional';
    protected $fillable 	= [
        'judul', 'photo', 'status','home','icon','deskripsi',
        'pengantar', 'profile',  'produk',  'tim',  'fasilitas',  'penelitian',  'jejaring',  'agenda',  'aksesibilitas',  'cara_merujuk',
        'judul_en', 'pengantar_en', 'profile_en', 'cara_merujuk_en', 'penelitian_en', 'jejaring_en', 'prosedur_en', 'deskripsi_en', 'produk_en', 'tim_en', 'fasilitas_en', 'agenda_en', 'aksesibilitas_en',
    ];
    protected $appends      = ['photo_url', 'icon_url'];
    
    public function dokter(){
        return $this->belongsToMany(Dokter::class, 'trans_rujukan_tim', 'id_rujukan_nasional', 'id_dokter')
                    ->withTimestamps()
                    ->withPivot('posisi', 'created_by', 'updated_by');
    }

    public function getPhotoUrlAttribute()
    {
        return !is_null($this->attributes['photo']) ? url('storage/'.$this->attributes['photo']) : null;
    }

    public function getIconUrlAttribute()
    {
        return !is_null($this->attributes['icon']) ? url('storage/'.$this->attributes['icon']) : null;
    }
}
