<?php

namespace App\Models\Litbang;

use App\Models\Model;
use App\Models\Master\JabatanDokter;

class KomiteEtik extends Model
{
    protected $table 		= 'trans_komite_etik';
    protected $log_table 	= 'log_trans_komite_etik';
    protected $log_table_fk	= 'id_komite_etik';

    protected $fillable 	= ['nama','jabatan','departemen', 'tanggal_efektif_awal', 'tanggal_efektif_akhir'];
    protected $dates		= ['tanggal_efektif_awal', 'tanggal_efektif_akhir'];
    
    public function jabatandokter()
    {
        return $this->belongsTo(JabatanDokter::class,'jabatan');
    }
}
