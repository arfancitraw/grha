<?php

namespace App\Models\Litbang;

use App\Models\Model;

class InfoLitbang extends Model
{
    protected $table 		= 'trans_info_litbang';
    protected $log_table 	= 'log_trans_info_litbang';
    protected $log_table_fk	= 'id_info_litbang';

    protected $fillable 	= ['judul', 'konten','judul_en','konten_en'];
}
