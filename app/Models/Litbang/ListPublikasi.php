<?php

namespace App\Models\Litbang;

use App\Models\Model;

class ListPublikasi extends Model
{
    protected $table 		= 'trans_list_publikasi';
    protected $log_table 	= 'log_trans_list_publikasi';
    protected $log_table_fk	= 'id_list_publikasi';

    protected $fillable 	= ['judul', 'tahun', 'peneliti_utama', 'peneliti_pendamping', 'citasi_jurnal', 'link', 'file', 'deskripsi'];
}
