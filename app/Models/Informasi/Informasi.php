<?php

namespace App\Models\Informasi;

use App\Models\Model;

class Informasi extends Model
{
    protected $table 		= 'trans_informasi';
    protected $log_table 	= 'log_trans_informasi';
    protected $log_table_fk	= 'id_informasi';

    protected $fillable 	= ['judul', 'slug', 'konten', 'keterangan', 'gambar', 'posisi','judul_en','keterangan_en','konten_en'];

    public function lampiran()
    {
    	return $this->hasMany(Lampiran::class, 'informasi_id');
    }
}
