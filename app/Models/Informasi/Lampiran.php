<?php

namespace App\Models\Informasi;

use App\Models\Model;

class Lampiran extends Model
{
    protected $table 		= 'trans_informasi_lampiran';
    protected $log_table 	= 'log_trans_informasi_lampiran';
    protected $log_table_fk	= 'lampiran_id';

    protected $fillable 	= ['informasi_id', 'judul', 'file'];

    public function informasi()
    {
    	return $this->belongsTo(Informasi::class, 'informasi_id');
    }
}
