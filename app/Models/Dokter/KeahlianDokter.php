<?php

namespace App\Models\Dokter;

use App\Models\Model;

class KeahlianDokter extends Model
{
    protected $table 		= 'trans_dokter_keahlian';
    protected $fillable 	= ['list_dokter_id', 'nama', 'tipe'];

    public function listdokter()
	{
	    return $this->belongsTo(ListDokter::class, 'list_dokter_id','id');
	}
}
