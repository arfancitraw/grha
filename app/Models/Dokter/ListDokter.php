<?php

namespace App\Models\Dokter;

use App\Models\Model;
use App\Models\Master\Spesialisasi;
use App\Models\Master\JabatanDokter;
use App\Models\Sukaman\Dokter;

class ListDokter extends Model
{
    protected $table 		= 'trans_list_dokter';
    protected $log_table 	= 'log_trans_list_dokter';
    protected $log_table_fk	= 'id_list_dokter';

    protected $fillable 	= ['nama','spesialisasi_id','alamat','no_tlpn','deskripsi','photo','jabatan_dokter_id', 'kode', 'nama_lengkap', 'aktif'];

    public function spesialisasi()
    {
        return $this->belongsTo(Spesialisasi::class,'spesialisasi_id');
    }

    public function jabatandokter()
    {
        return $this->belongsTo(JabatanDokter::class,'jabatan_dokter_id');
    }

    public function bahasa()
    {
        return $this->hasMany(BahasaDokter::class,'list_dokter_id');
    }

    public function pendidikan()
    {
        return $this->hasMany(PendidikanDokter::class,'list_dokter_id');
    }

    public function keahlian()
    {
        return $this->hasMany(KeahlianDokter::class,'list_dokter_id');
    }

    public function jadwal()
    {
        return $this->hasMany(JadwalDokter::class, 'list_dokter_id');
    }

    public function sukaman()
    {
        return $this->hasOne(Dokter::class, 'id_dokter');
    }

    public function scopeActive($query)
    {
        return $query->where('aktif', true);
    }
}
