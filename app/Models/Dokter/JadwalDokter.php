<?php

namespace App\Models\Dokter;

use App\Models\Model;
use App\Models\Master\Poli;

class JadwalDokter extends Model
{
    protected $table 		= 'trans_jadwal_dokter';
    protected $log_table 	= 'log_trans_jadwal_dokter';
    protected $log_table_fk	= 'id_jadwal_dokter';

    protected $fillable 	= ['list_dokter_id','hari_praktek','jam_mulai_praktek','jam_selesai_praktek', 'kode_departemen', 'nama_departemen', 'status_jam', 'status_ue', 'quota_janji'];
    protected $appends	 	= ['display_hari'];

    public function getDisplayHariAttribute()
    {
        $days = ['SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU', 'MINGGU'];
        return $days[$this->hari_praktek];
    }

    public function listdokter()
	{
	    return $this->belongsTo(ListDokter::class, 'list_dokter_id','id');
	}

    public function poli()
    {
        return $this->belongsTo(Poli::class, 'kode_departemen', 'kode');
    }
}
