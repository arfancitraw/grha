<?php

namespace App\Models\Pertanyaan;

use App\Models\Model;

class Pertanyaan extends Model
{
    protected $table 		= 'trans_jawaban_pertanyaan';
    protected $log_table 	= 'log_trans_jawaban_pertanyaan';
    protected $log_table_fk	= 'id_trans_tanya_jawab';

    protected $fillable 	= ['id_trans_tanya_jawab','referensi','jawaban'];
}
