<?php

namespace App\Models\Entrust;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    public static function getArray() {
        $permission = Static::get();
        if($permission->count() > 0)
        {
            foreach($permission as $p)
            {
                $prefix = explode('/', $p->name);

                if($prefix[1] == 'master')
                {
                    switch($prefix[2]) {
                        case 'slider':
                            if(isset($prefix[3]))
                            {
                              $return['pengaturan-website'][$prefix[2]][$prefix[3]] = $p->id;
                            }else{
                              $return['pengaturan-website'][$prefix[2]]['view'] = $p->id;
                            }
                        break;
                        case 'sertifikat-footer':
                            if(isset($prefix[3]))
                            {
                              $return['pengaturan-website'][$prefix[2]][$prefix[3]] = $p->id;
                            }else{
                              $return['pengaturan-website'][$prefix[2]]['view'] = $p->id;
                            }
                        break;
                        case 'media-sosial':
                            if(isset($prefix[3]))
                            {
                              $return['pengaturan-website'][$prefix[2]][$prefix[3]] = $p->id;
                            }else{
                              $return['pengaturan-website'][$prefix[2]]['view'] = $p->id;
                            }
                        break;
                        case 'link-footer':
                            if(isset($prefix[3]))
                            {
                              $return['pengaturan-website'][$prefix[2]][$prefix[3]] = $p->id;
                            }else{
                              $return['pengaturan-website'][$prefix[2]]['view'] = $p->id;
                            }
                        break;
                        case 'role-permission':
                            if(isset($prefix[3]))
                            {
                              $return['pengaturan-website'][$prefix[2]][$prefix[3]] = $p->id;
                            }else{
                              $return['pengaturan-website'][$prefix[2]]['view'] = $p->id;
                            }
                        break;
                        default :
                            if(isset($prefix[3]))
                            {
                              $return['master'][$prefix[2]][$prefix[3]] = $p->id;
                            }else{
                              $return['master'][$prefix[2]]['view'] = $p->id;
                            }
                    }
                }else{
                  if(isset($prefix[4]))
                  {
                      $return[$prefix[1]][$prefix[2]][$prefix[3]][$prefix[4]] = $p->id;
                  }else if(isset($prefix[3])){
                      switch($prefix[3]) {
                          case 'create':
                                $return[$prefix[1]][$prefix[2]][$prefix[3]] = $p->id;
                          break;
                          case 'edit':
                                $return[$prefix[1]][$prefix[2]][$prefix[3]] = $p->id;
                          break;
                          case 'delete':
                                $return[$prefix[1]][$prefix[2]][$prefix[3]] = $p->id;
                          break;
                          default:
                          $return[$prefix[1]][$prefix[2]][$prefix[3]]['view'] = $p->id;
                      }
                    }else if(isset($prefix[2])){
                        switch($prefix[2]) {
                            case 'create':
                                  $return[$prefix[1]][$prefix[2]] = $p->id;
                            break;
                            case 'edit':
                                  $return[$prefix[1]][$prefix[2]] = $p->id;
                            break;
                            case 'delete':
                                  $return[$prefix[1]][$prefix[2]] = $p->id;
                            break;
                            default:
                            $return[$prefix[1]][$prefix[2]]['view'] = $p->id;
                        }
                    }else{
                        switch($prefix[1]) {
                            case 'create':
                                  $return[$prefix[1]] = $p->id;
                            break;
                            case 'edit':
                                  $return[$prefix[1]] = $p->id;
                            break;
                            case 'delete':
                                  $return[$prefix[1]] = $p->id;
                            break;
                            default:
                            $return[$prefix[1]]['view'] = $p->id;
                        }
                    }
                }
            }
        }

        return $return;
    }
}
