<?php
namespace App\Models\Entrust;

use Zizaco\Entrust\EntrustRole;
use App\Models\Entrust\Permission;
use App\Models\Traits\EntryBy;
use App\Models\Traits\RaidModel;
use App\Models\Traits\Utilities;

class Role extends EntrustRole
{
    use RaidModel, Utilities, EntryBy;

    protected $fillable 	= ['name', 'display_name', 'description'];

    // public function setNameAttribute($value){
    //     $this->attributes['name'] = strtolower($this->attributes['display_name']);
    // }

    public function getChecked($perms) {
        foreach($this->perms()->get() as $permission)
        {
            if($permission->name == $perms)
            {
              return 'checked';
            }
        }
        return '';
    }

}
