<?php

namespace App\Models\Sukaman;

use App\Models\Model;

class Pricing extends Model
{
    protected $table 		= 'trans_sukaman_tarif';
    protected $log_table 	= 'log_trans_sukaman_tarif';
    protected $log_table_fk	= 'id_tarif';

    protected $fillable 	= ['id_kategori','nama','deskripsi','harga','warna','prioritas'];
    
    public function kategori()
    {
        return $this->belongsTo(PricingKategori::class,'id_kategori');
    }
    
    public function detail()
    {
        return $this->hasMany(PricingDetail::class,'id_tarif');
    }

}
