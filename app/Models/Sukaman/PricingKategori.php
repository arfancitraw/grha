<?php

namespace App\Models\Sukaman;

use App\Models\Model;
use App\Models\Sukaman\Pricing;

class PricingKategori extends Model
{
    protected $table 		= 'trans_sukaman_kategori_tarif';
    protected $log_table 	= 'log_trans_sukaman_kategori_tarif';
    protected $log_table_fk	= 'id_kategori';

    protected $fillable 	= ['nama','deskripsi','prioritas','status'];

    public function tarif()
    {
        return $this->hasMany(Pricing::class,'id_kategori');
    } 
}

