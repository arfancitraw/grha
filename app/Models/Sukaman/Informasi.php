<?php

namespace App\Models\Sukaman;

use App\Models\Model;

class Informasi extends Model
{
    protected $table 		= 'trans_sukaman_informasi';
    protected $log_table 	= 'log_trans_sukaman_informasi';
    protected $log_table_fk	= 'id_sukaman_informasi';

    protected $fillable 	= ['judul','gambar','konten'];

}
