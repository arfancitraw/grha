<?php

namespace App\Models\Sukaman;

use App\Models\Model;

class Fasilitas extends Model
{
    protected $table 		= 'trans_sukaman_fasilitas';
    protected $log_table 	= 'log_trans_sukaman_fasilitas';
    protected $log_table_fk	= 'id_sukaman_fasilitas';

    protected $fillable 	= ['judul','gambar','deskripsi'];

}
