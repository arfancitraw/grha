<?php

namespace App\Models\Sukaman;

use App\Models\Model;
use App\Models\Dokter\ListDokter;

class Dokter extends Model
{
    protected $table 		= 'trans_sukaman_dokter';
    protected $log_table 	= 'log_trans_sukaman_dokter';
    protected $log_table_fk	= 'id_sukaman_dokter';

    protected $fillable 	= ['id_dokter'];

    public function detail()
    {
        return $this->belongsTo(ListDokter::class,'id_dokter');
    }
}
