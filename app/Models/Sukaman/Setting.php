<?php

namespace App\Models\Sukaman;

use App\Models\Model;

class Setting extends Model
{
    protected $table 		= 'trans_sukaman_setting';

    protected $fillable 	= ['kunci','judul','isi'];

}
