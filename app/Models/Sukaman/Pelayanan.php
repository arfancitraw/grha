<?php

namespace App\Models\Sukaman;

use App\Models\Model;

class Pelayanan extends Model
{
    protected $table 		= 'trans_sukaman_pelayanan';
    protected $log_table 	= 'log_trans_sukaman_pelayanan';
    protected $log_table_fk	= 'id_sukaman_pelayanan';

    protected $fillable 	= ['judul','konten','icon','gambar'];

}
