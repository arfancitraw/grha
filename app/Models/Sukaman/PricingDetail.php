<?php

namespace App\Models\Sukaman;

use App\Models\Model;

class PricingDetail extends Model
{
    protected $table 		= 'trans_sukaman_tarif_detail';
    protected $log_table 	= 'log_trans_sukaman_tarif_detail';
    protected $log_table_fk	= 'id_tarif_detail';

    protected $fillable 	= ['id_tarif','nama','check','urutan'];

    public function setUrutanAttribute($value)
    {
        $this->attributes['urutan'] = is_null($value) ? 999 : $value;
    }
}