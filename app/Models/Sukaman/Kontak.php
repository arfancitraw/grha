<?php

namespace App\Models\Sukaman;

use App\Models\Model;

class Kontak extends Model
{
    protected $table 		= 'trans_sukaman_kontak';
    protected $log_table 	= 'log_trans_sukaman_kontak';
    protected $log_table_fk	= 'id_kontak';
    protected $fillable 	= ['nama', 'email', 'perihal', 'pesan', 'status'];
}
