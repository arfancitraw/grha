<?php

namespace App\Models\Sukaman;

use App\Models\Model;

class Slider extends Model
{
    protected $table 		= 'trans_sukaman_slider';
    protected $log_table 	= 'log_trans_sukaman_slider';
    protected $log_table_fk	= 'id_slider';
    protected $fillable 	= ['judul', 'konten', 'gambar', 'urutan', 'status'];
}
