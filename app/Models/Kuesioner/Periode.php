<?php

namespace App\Models\Kuesioner;

use App\Models\Model;

class Periode extends Model
{
    protected $table 		= 'ref_kuesioner_periode';
    protected $log_table 	= 'log_ref_kuesioner_periode';
    protected $log_table_fk	= 'id_kuesioner_periode';

    protected $fillable 	= ['periode', 'status'];

    public function detail()
    {
        return $this->hasMany(Detail::class, 'kuesioner_id');
    }

	public function isian(){
		return $this->hasMany(KuesionerJawaban::class, 'kuesioner_id');
	}
}
