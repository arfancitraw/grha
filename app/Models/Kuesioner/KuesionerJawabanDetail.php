<?php

namespace App\Models\Kuesioner;

use App\Models\Model;
use App\Models\Kuesioner\KuesionerJawaban;
use App\Models\Master\KuesionerDetail;

class KuesionerJawabanDetail extends Model
{
    protected $table 		= 'trans_kuesioner_jawaban_detail';
    protected $log_table 	= 'log_trans_kuesioner_jawaban_detail';
    protected $log_table_fk	= 'id_kuesioner_jawaban_detail';

    protected $fillable 	= ['kuesioner_jawaban_id','kuesioner_detail_id','jawaban'];

    public function kuesionerjawaban()
    {
        return $this->belongsTo(KuesionerJawaban::class,'kuesioner_jawaban_id');
    }

    public function kuesionerdetail()
    {
        return $this->belongsTo(KuesionerDetail::class,'kuesioner_detail_id');
    }
}
