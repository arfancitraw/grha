<?php

namespace App\Models\Kuesioner;

use App\Models\Model;

class Kuesioner extends Model
{
    protected $table 		= 'trans_kuesioner';
    protected $log_table 	= 'log_trans_kuesioner';
    protected $log_table_fk	= 'id_kuesioner';

    protected $fillable 	= ['nama','email','profesi', 'kepuasan_layanan', 'kepuasan_fasilitas_website', 'feedback'];
}
