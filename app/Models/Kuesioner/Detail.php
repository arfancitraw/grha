<?php

namespace App\Models\Kuesioner;

use App\Models\Model;

class Detail extends Model
{
    protected $table 		= 'ref_kuesioner_detail';
    protected $log_table 	= 'log_ref_kuesioner_detail';
    protected $log_table_fk	= 'id_kuesioner_detail';

    protected $fillable 	= ['kuesioner_id', 'pertanyaan', 'tipe','pertanyaan_en'];

    public function periode()
    {
        return $this->belongsTo(Periode::class, 'kuesioner_id');
    }

    public function jawaban()
    {
    	return $this->hasMany(KuesionerJawabanDetail::class, 'kuesioner_detail_id');
    }
}
