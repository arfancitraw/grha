<?php

namespace App\Models\Kuesioner;

use App\Models\Model;

use App\Models\Master\KuesionerPeriode;

class KuesionerJawaban extends Model
{
	protected $table 		= 'trans_kuesioner_jawaban';
	protected $log_table 	= 'log_trans_kuesioner_jawaban';
	protected $log_table_fk	= 'id_kuesioner_jawaban';

	protected $fillable 	= ['nama','email','kuesioner_id','feedback'];

	public function detail(){
		return $this->hasMany(KuesionerJawabanDetail::class,'kuesioner_jawaban_id');
	}

	public function kuesionerperiode()
	{
		return $this->belongsTo(KuesionerPeriode::class,'kuesioner_id');
	}

	public function savekepuasanLayanan($id="", $data="")
	{
		if(isset($data)){

			foreach ($data['kepuasan-layanan'] as $Nilaivalue => $value) {
				$kuesionerjawabandetail['kuesioner_jawaban_id'] = $id;
				$kuesionerjawabandetail['kuesioner_detail_id'] = $Nilaivalue;
				$kuesionerjawabandetail['jawaban'] = $value;

				$save = new KuesionerJawabanDetail;
				$save->fill($kuesionerjawabandetail);
				$save->save();
			}
		}
	}
}
