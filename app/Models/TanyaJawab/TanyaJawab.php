<?php

namespace App\Models\TanyaJawab;

use App\Models\Model;
use App\Models\Master\KategoriTanyaJawab;

class TanyaJawab extends Model
{
    protected $table 		= 'trans_tanya_jawab';
    protected $log_table 	= 'log_trans_tanya_jawab';
    protected $log_table_fk	= 'id_tanya_jawab';

    protected $fillable 	= ['id_kategori', 'nama', 'email', 'judul', 'konten', 'rahasia', 'publish'];

    public function kategori()
    {
        return $this->belongsTo(KategoriTanyaJawab::class, 'id_kategori');
    }

    public function detail()
    {
        return $this->hasMany(Detail::class, 'id_tanya_jawab');
    }

    public function access()
    {
        return $this->hasMany(Access::class, 'id_tanya_jawab');
    }

    public function scopePublished($query)
    {
        return $query->where('publish', true);
    }

}
