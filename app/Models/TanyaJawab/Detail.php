<?php

namespace App\Models\TanyaJawab;

use App\Models\Model;

class Detail extends Model
{
    protected $table 		= 'trans_tanya_jawab_detail';
    protected $log_table 	= 'log_trans_tanya_jawab_detail';
    protected $log_table_fk	= 'id_tanya_jawab_detail';

    protected $fillable 	= ['id_tanya_jawab', 'nama', 'email', 'referensi', 'judul', 'konten', 'internal', 'rahasia', 'publish'];

    public function pertanyaan()
    {
        return $this->belongsTo(TanyaJawab::class, 'id_tanya_jawab');
    }

    public function scopePublished($query)
    {
        return $query->where('publish', true);
    }
}
