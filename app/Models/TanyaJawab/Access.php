<?php

namespace App\Models\TanyaJawab;

use App\Models\Model;

class Access extends Model
{
    protected $table 		= 'trans_tanya_jawab_access';

    protected $fillable 	= ['id_tanya_jawab', 'ip'];

    public function pertanyaan()
    {
        return $this->belongsTo(TanyaJawab::class, 'id_tanya_jawab');
    }

    public function scopeUniqueCount()
    {
    	return $this->groupBy('ip');
    }
}
