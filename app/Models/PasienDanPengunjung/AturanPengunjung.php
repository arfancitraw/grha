<?php

namespace App\Models\PasienDanPengunjung;

use App\Models\Model;

class AturanPengunjung extends Model
{
    protected $table 		= 'trans_aturan_pengunjung';
    protected $log_table 	= 'log_trans_aturan_pengunjung';
    protected $log_table_fk	= 'id_aturan_pengunjung';
    protected $fillable 	= ['judul', 'konten', 'keterangan'];
}
