<?php

namespace App\Models\PasienDanPengunjung;

use App\Models\Model;

class AlurPendaftaran extends Model
{
    protected $table 		= 'trans_alur_pendaftaran';
    protected $log_table 	= 'log_trans_alur_pendaftaran';
    protected $log_table_fk	= 'id_alur_pendaftaran';

    protected $fillable 	= ['judul','photo','keterangan'];
}
