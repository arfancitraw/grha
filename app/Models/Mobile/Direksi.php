<?php

namespace App\Models\Mobile;

use App\Models\Model;

class Direksi extends Model
{
    protected $table 		= 'trans_mobile_direksi';
    protected $log_table 	= 'log_trans_mobile_direksi';
    protected $log_table_fk	= 'id_direksi';

    protected $appends		= ['photo_url'];

    protected $fillable 	= ['nama', 'jabatan', 'deskripsi', 'photo', 'urutan'];
    
    public function getPhotoUrlAttribute()
    {
        return !is_null($this->attributes['photo']) ? url('storage/'.$this->attributes['photo']) : null;
    }
}
