<?php

namespace App\Models\Mobile;

use App\Models\Model;

class Dewan extends Model
{
    protected $table 		= 'trans_mobile_pengawas';
    protected $log_table 	= 'log_trans_mobile_pengawas';
    protected $log_table_fk	= 'id_pengawas';
 	protected $appends		= ['photo_url'];

    protected $fillable 	= ['nama', 'jabatan', 'deskripsi', 'photo', 'urutan'];

    public function getPhotoUrlAttribute()
    {
        return !is_null($this->attributes['photo']) ? url('storage/'.$this->attributes['photo']) : null;
    }
}
