<?php

namespace App\Models\Testimoni;

use App\Models\Model;

class Testimoni extends Model
{
	protected $table 		= 'trans_testimoni';
	protected $log_table 	= 'log_trans_testimoni';
	protected $log_table_fk	= 'id_trans_testimoni';

	protected $fillable 	= ['nama','email','testimoni','publik','rating','pekerjaan'];
}
