<?php

namespace App\Models\Layanan;

use App\Models\Model;

class Layanan extends Model
{
    protected $table 		= 'trans_layanan';
    protected $log_table 	= 'log_trans_layanan';
    protected $log_table_fk	= 'id_layanan';

    protected $fillable 	= ['judul','konten','keterangan','photo','files','layanan_unggulan','slug', 'home', 'icon', 'posisi'];
    protected $appends		= ['photo_url'];

    public function getPhotoUrlAttribute()
    {
        return !is_null($this->attributes['photo']) ? url('storage/'.$this->attributes['photo']) : null;
    }
}
