<?php

namespace App\Models\InfoPasien;

use App\Models\Model;
use App\Models\User;
use App\Models\Layanan\Layanan;

class Testimoni extends Model
{
    protected $table 		= 'trans_testimoni';
    protected $log_table 	= 'log_trans_testimoni';
    protected $log_table_fk	= 'id_trans_testimoni';
    protected $fillable 	= ['nama', 'email', 'testimoni', 'publik', 'pekerjaan', 'rating'];
    public function layanan()
    {
        return $this->belongsTo(Layanan::class,'layanan_yang_digunakan');
    }
}