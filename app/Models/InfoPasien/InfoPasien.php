<?php

namespace App\Models\InfoPasien;

use App\Models\Model;
use App\Models\User;

class InfoPasien extends Model
{
    protected $table 		= 'trans_info_pasien';
    protected $log_table 	= 'log_trans_info_pasien';
    protected $log_table_fk	= 'id_trans_info_pasien';
    protected $fillable 	= ['judul', 'konten', 'keterangan', 'gambar', 'slug'];
}
