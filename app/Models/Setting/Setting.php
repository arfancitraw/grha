<?php

namespace App\Models\Setting;

use App\Models\Model;

class Setting extends Model
{
    protected $table 		= 'ref_setting';
    protected $fillable 	= ['kunci','judul','isi'];
}
