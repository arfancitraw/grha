<?php

namespace App\Models\Setting;

use App\Models\Model;

class Menu extends Model
{
	protected $table 		= 'trans_menu';
	protected $log_table 	= 'log_trans_menu';
	protected $log_table_fk	= 'id_menu';

	protected $fillable 	= ['judul', 'tautan', 'urutan', 'induk_id', 'right'];

    public function parent()
    {
    	return $this->belongsTo(static::class, 'induk_id');
    }

    public function childs()
    {
    	return $this->hasMany(static::class, 'induk_id');
    }

    public static function generate($induk_id=null)
    {
        $menus = static::where('induk_id', $induk_id)
                       ->orderBy('right')
                       ->orderBy('urutan')
        			   ->get();
        $ret = [];
        foreach ($menus as $menu) {
            $res = $menu->toArray();
            if(count($menu->childs) > 0){
                $res['childs'] = static::generate($menu->id);
            }
            array_push($ret, $res);
        }

        return $ret;
    }

    public static function findPlace($parent=null)
    {
        $count = static::count();
        if(!is_null($parent))
            $count = static::where('induk_id', $parent)->count();

        return $count + 1;
    }

}
