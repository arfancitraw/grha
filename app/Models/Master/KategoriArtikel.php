<?php

namespace App\Models\Master;

use App\Models\Model;

class KategoriArtikel extends Model
{
 	protected $table		= 'ref_kategori_artikel';
 	protected $log_table	= 'log_ref_kategori_artikel';
    protected $log_table_fk	= 'id_kategori_artikel';
 	protected $fillable		= ['nama','deskripsi'];

}