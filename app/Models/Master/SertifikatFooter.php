<?php

namespace App\Models\Master;

use App\Models\Model;

class SertifikatFooter extends Model
{
    protected $table 		= 'ref_sertifikat_footer';
    protected $log_table 	= 'log_ref_sertifikat_footer';
    protected $log_table_fk	= 'id_sertifikat_footer';

    protected $fillable 	= ['nama', 'gambar', 'keterangan', 'link'];
}
