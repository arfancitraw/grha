<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\KuesionerPeriode;

class KuesionerDetail extends Model
{
    protected $table 		= 'ref_kuesioner_detail';
    protected $log_table 	= 'log_ref_kuesioner_detail';
    protected $log_table_fk	= 'id_kuesioner_detail';

    protected $fillable 	= ['kuesioner_id','pertanyaan'];

    public function kuesionerperiode()
    {
        return $this->belongsTo(KuesionerPeriode::class,'kuesioner_id');
    }
}
