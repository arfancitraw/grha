<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Dokter\ListDokter;
use App\Models\Sukaman\Dokter;

class Spesialisasi extends Model
{
 	protected $table		= 'ref_spesialisasi';
 	protected $log_table	= 'log_spesialisasi';
    protected $log_table_fk	= 'id_spesialisasi';
 	protected $fillable		= ['nama','prioritas','deskripsi', 'icon'];
 	protected $appends		= ['icon_url'];

 	public function jabatanDokter()
 	{
 		return $this->hasMany(JabatanDokter::class,'spesialisasi_id');
 	}

 	public function subspesialisasi() {
 		return $this->hasMany(SubSpesialisasi::class,'spesialisasi_induk');
 	}

 	public function dokter()
 	{
 		return $this->hasMany(ListDokter::class, 'spesialisasi_id');
 	}

 	public function sukaman()
 	{
 		return $this->hasManyThrough(Dokter::class, ListDokter::class, 'spesialisasi_id', 'id_dokter');
 	}

    public function getIconUrlAttribute()
    {
        return !is_null($this->attributes['icon']) ? url('storage/'.$this->attributes['icon']) : null;
    }
}