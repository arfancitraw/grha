<?php

namespace App\Models\Master;

use App\Models\Model;

class KuesionerPeriode extends Model
{
    protected $table 		= 'ref_kuesioner_periode';
    protected $log_table 	= 'log_ref_kuesioner_periode';
    protected $log_table_fk	= 'id_kuesioner_periode';

    protected $fillable 	= ['periode','status'];

    public function kuesionerdetail()
    {
        return $this->hasMany(kuesionerdetail::class,'kuesioner_id');
    }
}
