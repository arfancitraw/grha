<?php

namespace App\Models\Master;

use App\Models\Model;

class JabatanDokter extends Model
{
    protected $table 		= 'ref_jabatan_dokter';
    protected $log_table 	= 'log_jabatan_dokter';
    protected $log_table_fk	= 'id_jabatan_dokter';

    protected $fillable 	= ['jabatan', 'deskripsi'];
}
