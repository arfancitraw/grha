<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Dokter\JadwalDokter;

class Poli extends Model
{
    protected $table 		= 'ref_poli';
    protected $log_table 	= 'log_poli';
    protected $log_table_fk	= 'id_poli';

    protected $fillable 	= ['kode', 'nama', 'deskripsi'];

    public function jadwal()
    {
    	return $this->hasMany(JadwalDokter::class, 'kode_departemen', 'kode');
    }
}
