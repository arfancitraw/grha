<?php

namespace App\Models\Master;

use App\Models\Model;

class LinkFooter extends Model
{
    protected $table 		= 'ref_link_footer';
    protected $log_table 	= 'log_ref_link_footer';
    protected $log_table_fk	= 'id_link_footer';

    protected $fillable 	= ['nama', 'link'];
}
