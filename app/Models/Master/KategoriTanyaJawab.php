<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\TanyaJawab\TanyaJawab;

class KategoriTanyaJawab extends Model
{
 	protected $table		= 'ref_kategori_tanya_jawab';
 	protected $log_table	= 'log_ref_kategori_tanya_jawab';
    protected $log_table_fk	= 'id_kategori_tanya_jawab';
 	protected $fillable		= ['nama','keterangan','pernyataan'];

    public function pertanyaan()
    {
        return $this->hasMany(TanyaJawab::class,'id_kategori');
    }
}