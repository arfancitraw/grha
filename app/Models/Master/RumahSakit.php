<?php

namespace App\Models\Master;

use App\Models\Model;

class RumahSakit extends Model
{
    protected $table 		= 'ref_rumah_sakit';
    protected $log_table 	= 'log_rumah_sakit';
    protected $log_table_fk	= 'id_rumah_sakit';

    protected $fillable 	= ['nama', 'alamat', 'no_tlpn'];
}
