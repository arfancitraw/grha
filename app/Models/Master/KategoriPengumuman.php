<?php

namespace App\Models\Master;

use App\Models\Model;

class KategoriPengumuman extends Model
{
 	protected $table		= 'ref_kategori_pengumuman';
 	protected $log_table	= 'log_ref_kategori_pengumuman';
    protected $log_table_fk	= 'id_kategori_pengumuman';
 	protected $fillable		= ['nama','deskripsi'];

}