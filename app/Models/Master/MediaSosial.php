<?php

namespace App\Models\Master;

use App\Models\Model;

class MediaSosial extends Model
{
    protected $table 		= 'ref_media_sosial';
    protected $log_table 	= 'log_ref_media_sosial';
    protected $log_table_fk	= 'id_media_sosial';

    protected $fillable 	= ['nama', 'icon', 'link'];
}
