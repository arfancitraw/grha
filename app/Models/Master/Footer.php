<?php

namespace App\Models\Master;

use App\Models\Model;

class Footer extends Model
{
    protected $table 		= 'ref_tautan_footer';
    protected $log_table 	= 'log_tautan_footer';
    protected $log_table_fk	= 'id_tautan_footer';

    protected $fillable 	= ['nama', 'url', 'deskripsi'];
}
