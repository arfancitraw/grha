<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\InfoBed;

class InfoBed extends Model
{
    protected $table 		= 'trans_info_bed';
    protected $log_table 	= 'log_trans_info_bed';
    protected $log_table_fk	= 'id_info_bed';

    protected $fillable 	= ['nama','gambar','jumlah_total','isi'];

}
