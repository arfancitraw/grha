<?php

namespace App\Models\Master;

use App\Models\Model;

class Slider extends Model
{
    protected $table 		= 'ref_slider';
    protected $log_table 	= 'log_slider';
    protected $log_table_fk	= 'id_slider';

    protected $fillable 	= ['judul', 'sub_judul', 'deskripsi','photo', 'url', 'posisi', 'status', 'android'];
 	protected $appends		= ['photo_url'];

    public function getPhotoUrlAttribute()
    {
        return !is_null($this->attributes['photo']) ? url('storage/'.$this->attributes['photo']) : null;
    }
}
