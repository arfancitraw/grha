<?php

namespace App\Models\Master;

use App\Models\Model;

class TipePendaftar extends Model
{
    protected $table 		= 'ref_tipe_pendaftar';
    protected $log_table 	= 'log_tipe_pendaftar';
    protected $log_table_fk	= 'id_tipe_pendaftar';

    protected $fillable 	= ['tipe_pendaftar','deskripsi'];
}
