<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Mutu\InfoMutu;

class JenisMutu extends Model
{
    protected $table 		= 'ref_jenis_mutu';
    protected $log_table 	= 'log_jenis_mutu';
    protected $log_table_fk	= 'id_jenis_mutu';

    protected $fillable 	= ['nama', 'keterangan'];

    public function konten()
    {
        return $this->hasMany(InfoMutu::class, 'jenis_mutu');
    }
}
