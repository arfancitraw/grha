<?php

namespace App\Models\Master;

use App\Models\Master\Spesialisasi;
use App\Models\Model;

class SubSpesialisasi extends Model
{
 	protected $table		= 'ref_sub_spesialisasi';
 	protected $log_table	= 'log_ref_sub_spesialisasi';
    protected $log_table_fk	= 'id_sub_spesialisasi';
 	protected $fillable		= ['nama','keterangan','spesialisasi_induk'];

 	public function spesialisasi()
 	{
 		return $this->belongsTo(Spesialisasi::class,'spesialisasi_induk');
 	}
}