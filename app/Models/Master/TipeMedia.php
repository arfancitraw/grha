<?php

namespace App\Models\Master;

use App\Models\Model;

class TipeMedia extends Model
{
    protected $table 		= 'ref_tipe_media';
    protected $log_table 	= 'log_tipe_media';
    protected $log_table_fk	= 'id_tipe_media';

    protected $fillable 	= ['tipe_media', 'deskripsi'];
}
