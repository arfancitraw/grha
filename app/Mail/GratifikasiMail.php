<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GratifikasiMail extends Mailable
{
    use Queueable, SerializesModels;

    public $parameter;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $parameter = $this->parameter;

        // return $this->from($from)
        return $this->from('info@pjnhk.go.id')
                    ->markdown('emails.gratifikasi')
                    ->with('parameter', $parameter);
    }
}
