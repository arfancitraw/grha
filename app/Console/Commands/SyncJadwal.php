<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Dokter\ListDokter;
use App\Models\Dokter\JadwalDokter;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SyncJadwal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:jadwal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sinkronisasi Jadwal Dokter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = config('app.api_url').'jadwal_dokter';
        $client = new Client();
        $result = $client->get($url);

        if($result){
            $data = json_decode($result->getBody());
            // $kodes = array_column($data, 'kd_dr');
            // $cek = ListDokter::whereNotIn('kode', $kodes)
            //                  ->update(['aktif' => 0]);
                             
            foreach ($data as $dokter) {
                $row = ListDokter::where('kode', $dokter->kd_dr)->first();

                if($row && $dokter->jadwal){
                    $row->jadwal()->delete();
                    foreach ($dokter->jadwal as $jadwal) {
                        $days = ['SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU', 'MINGGU'];
                        $cek = JadwalDokter::where('list_dokter_id', $row->id)
                                           ->where('hari_praktek', array_search($jadwal->hari, $days))
                                           ->where('jam_mulai_praktek', $jadwal->jam_dtg)
                                           ->first();
                        $sub = $cek ?: new JadwalDokter();
                        $sub->list_dokter_id = $row->id;
                        $sub->hari_praktek = array_search($jadwal->hari, $days);
                        $sub->jam_mulai_praktek = $jadwal->jam_dtg;
                        $sub->jam_selesai_praktek = $jadwal->jam_plg;
                        $sub->kode_departemen = $jadwal->kd_dep;
                        $sub->nama_departemen = $jadwal->ds_dep;
                        $sub->nama_departemen = $jadwal->ds_dep;
                        $sub->status_jam = $jadwal->sts_jam;
                        $sub->status_ue = $jadwal->sts_ue;
                        $sub->quota_janji = $jadwal->quota_janji;

                        $sub->save();
                    }
                }
            }

            $this->info('Data jadwal dokter berhasil ter-sinkron');
            die();
        }

        $this->error('Data jadwal dokter gagal ter-sinkron');
    }
}
